<?php

Class MY_Controller extends CI_Controller {

    public $page_id;
    public $page_title;
    public $menu_id;
    public $sub_menu_id;
    public $add_message;
    public $user_id;
    public $role;

    public function __construct() {
        parent::__construct();
        $this->load->model('Common_model');
        $this->page_id = '';
        $this->page_title = '';
        $this->menu_id = '';
        $this->sub_menu_id = '';
        $this->sabhasad_id = $this->session->userdata(SITE_NAME . '_sabhasad') != NULL ? $this->session->userdata(SITE_NAME . '_sabhasad')['sabhasad_id'] : '';
        $this->admin_id = $this->session->userdata(SITE_NAME . '_admin') != NULL ? $this->session->userdata(SITE_NAME . '_admin')['user_id'] : '';
        $this->role = $this->session->userdata(SITE_NAME . '_admin') != NULL ? $this->session->userdata(SITE_NAME . '_admin')['role'] : '';

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.weborative.com',
            'smtp_port' => 465,
            'smtp_user' => 'info@weborative.com',
            'smtp_pass' => 'info@123#',
            'smtp_crypto' => 'ssl',
            'mailtype' => 'html',
            'smtp_timeout' => '100',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
    }

    public function load_view($view, $data, $flag = true) {
        $data['add_message'] = $this->session->flashdata('message');
        if ($flag) {
            $this->load->view('frontend/header', $data);
        }
        $this->load->view($view, $data);
        if ($flag) {
            $this->load->view('frontend/footer', $data);
        }
    }

    public function load_admin_view($view, $data, $flag = true) {
        $data['add_message'] = $this->session->flashdata('message');
        $data['user_info'] = $this->Common_model->getDataById2('user_information', 'id', $this->admin_id, 'Live');
        if ($flag) {
            $this->load->view('admin/home/header', $data);
            $this->load->view('admin/home/side_menu', $data);
        }
        $this->load->view($view, $data);
        if ($flag) {
            $this->load->view('admin/home/footer', $data);
        }
    }

    public function is_login($login_type) {
        if ($login_type == 'sabhasad') {
            if (isset($this->sabhasad_id) && !empty($this->sabhasad_id)) {
                return true;
            }
        } else if ($login_type == 'admin') {
            if (isset($this->admin_id) && !empty($this->admin_id)) {
                return true;
            }
        }
        return false;
    }

    public function _show_message($message, $type = 'message') {
        $message = 'toastr["' . $type . '"]("' . $message . '");';
        $this->session->set_flashdata('message', $message);
    }

    /**
     * For JSON Encode
     * @var _json    -- return array
     */
    public function safe_json_encode($value) {
        if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
            $encoded = json_encode($value, JSON_PRETTY_PRINT);
        } else {
            $encoded = json_encode($value);
        }
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return $encoded;
            case JSON_ERROR_DEPTH:
                return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_STATE_MISMATCH:
                return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_CTRL_CHAR:
                return 'Unexpected control character found';
            case JSON_ERROR_SYNTAX:
                return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_UTF8:
                $clean = $this->utf8ize($value);
                return safe_json_encode($clean);
            default:
                return 'Unknown error'; // or trigger_error() or throw new Exception()
        }
    }

    function utf8ize($mixed) {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = utf8ize($value);
            }
        } else if (is_string($mixed)) {
            return utf8_encode($mixed);
        }
        return $mixed;
    }

    /**
     * For Ajax Request Output Buffer
     * @var _json    -- return array
     */
    public function output_json($_json) {
        $_json = $this->safe_json_encode($_json);
        $this->output->set_content_type('application/json')->set_output($_json);
    }

}
