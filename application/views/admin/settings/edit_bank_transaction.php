<!-- Bank debit credit manage -->

<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Bank Transaction
        </h1>
        <div class="d-flex mr-0">
            <a href="<?php echo base_url('admin/Csettings/index') ?>" class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed"><i class="ti-align-justify"> </i> <?php echo ('Add New Bank') ?> </a>
            <a href="<?php echo base_url('admin/Csettings/manage_bank_transaction') ?>" class="btn btn-primary bg-trans-gradient ml-1 waves-effect waves-themed"><i class="ti-align-justify"> </i>  <?php echo ('Manage Bank') ?> Transactions </a>
        </div>
    </div>

    <!-- New bank -->
    <div class="row">
        <div class="col-sm-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Csettings/update_bank_transaction/' . $id, array('class' => '', 'id' => 'validate')) ?>
                    <div class="panel-content">
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="bank_name" class="form-label"><?php echo ('Bank Name') ?> <i class="text-danger">*</i></label>
                                    <select class="form-control select2" name="bank_id" required="">
                                        <option value=""></option>
                                        <?php foreach ($bank_list as $key => $val) { ?>
                                            <option value="<?= $val['bank_id'] ?>" <?= ($val['bank_id'] == $transaction[0]->bank_id ? 'selected' : ''); ?>><?= $val['bank_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="date" class="form-label"><?php echo ('date') ?> <i class="text-danger">*</i></label>
                                    <?php $date = date('Y-m-d'); ?>
                                    <input type="date" class="form-control" name="date" id="date" required="" placeholder="<?php echo ('date') ?>" value="<?php echo $date ?>"/>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="account_type" class="form-label">Entry Type <i class="text-danger">*</i></label>
                                    <select class="form-control select2" id="account_type" name="account_type" data-placeholder="-- Select Type --" required="">
                                        <option value=""></option>
                                        <option value="1" <?= ($transaction[0]->ac_type == 1 ? 'selected' : ''); ?>>Credit (+)</option>
                                        <option value="2" <?= ($transaction[0]->ac_type == 2 ? 'selected' : ''); ?>>Debit (-)</option>
                                    </select>
                                    <span></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="withdraw_deposite_id" class="form-label">Payer / Receiver Name <i class="text-danger">*</i></label>
                                    <input type="text" class="form-control" value="<?= $transaction[0]->deposite_id ?>" name="withdraw_deposite_id" id="withdraw_deposite_id" required="" placeholder="<?php echo ('withdraw_deposite_id') ?>"/>
                                    <span></span>
                                </div>
                            </div>        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="ammount" class="form-label"><?php echo ('ammount') ?> <i class="text-danger">*</i></label>
                                    <input type="number" class="form-control" name="ammount" id="ammount" value="<?= $transaction[0]->amount ?>" required="" placeholder="<?php echo ('ammount') ?>"/>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description" class="form-label"><?php echo ('description') ?> </label>
                                    <textarea class="form-control" rows="3" placeholder="Write transaction description here..." name="description"><?= $transaction[0]->description ?></textarea>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row" style="gap:5px;">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-deposit" id="add-deposit" value="Save" ><span class="fal fa-check mr-1"></span>Save</button>
                        <a href="<?= base_url() ?>admin/Csettings/manage_bank_transaction"  class="btn btn-secondary btn-large">Cancel</a>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $("#validate").validate({
        errorPlacement: function (error, element) {
            error.insertAfter(element.next());
        }
    });
</script>
<!-- Bank debit credit manage -->

