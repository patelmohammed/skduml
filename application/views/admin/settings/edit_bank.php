

<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit new bank
        </h1>
        <div class="d-flex mr-3">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Csettings/manage_bank_transaction/<?php echo $bank_list[0]['bank_id'] ?>">Manage Bank transaction</a>
        </div>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Csettings/bank_list">Manage Bank</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Csettings/add_new_bank', array('class' => '', 'id' => 'validate', 'name' => 'validate')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="product_name"> Bank Name  <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="bank_name" id="bank_name" required="" placeholder="<?php echo ('bank_name') ?>" value="<?php echo $bank_list[0]['bank_name'] ?>">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="category_id">A/C Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="ac_name" id="ac_name" required="" placeholder="<?php echo ('ac_name') ?>" value="<?php echo $bank_list[0]['ac_name'] ?>">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="sell_price">A/C Number  <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="ac_no" id="ac_no" required="" placeholder="<?php echo ('ac_no') ?>" value="<?php echo $bank_list[0]['ac_number'] ?>">
                               
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="sell_price">Branch <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="branch" id="branch" required="" placeholder="<?php echo ('branch') ?>" value="<?php echo $bank_list[0]['branch'] ?>">
                               
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="sell_price">IFSC Code <span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="IFSC_code" id="IFSC_code" placeholder="IFSC Code"  value="<?php echo $bank_list[0]['IFSC_code'] ?>" >
                               
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="image">Signature Picture </label>
                                <input type="file" class="form-control" name="signature_pic" id="signature_pic" placeholder="<?php echo ('signature_pic') ?>" >
                                 <img src="<?php echo $bank_list[0]['signature_pic'] ?>"  height="80" width="80">
                            </div>
                            
                        </div>

                        
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" id="add-deposit" class="btn btn-danger ml-auto waves-effect waves-themed" name="save" value="<?php echo ('save_changes') ?>"><span class="fal fa-check mr-1"></span>Save</button>
                        <!--<button type="reset" class="btn btn-warning ml-1" name="RESET" value="Resest"><span class="fa fa-times pr-1" ></span>Reset</button>-->
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).ready(function () {
        $('#validate').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                bank_name: {
                    remote: '<?= base_url() ?>admin/Csettings/checkBank/<?= $bank_list[0]['bank_id']?>'
                }
            },
            messages: {
                bank_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
//</script>