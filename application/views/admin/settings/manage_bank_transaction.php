<style>
    .text-success{
        color: #15cd15 !important;
    }
</style>

<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Bank Transactions
        </h1>
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <a href="<?php echo base_url('admin/Csettings/bank_transaction') ?>" class="btn btn-success btn-large mr-1"><i class="ti-plus"> </i> Add Bank Transaction </a>
                    <a href="<?php echo base_url('admin/Csettings/bank_list') ?>" class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed mr-1"><i class="ti-align-justify"> </i> Manage Bank </a>
                </div>
            </div>
        </div>
    </div>
    <?php if (isset($bank_id) && !empty($bank_id)) { ?>
        <div class="row">
            <div class="col-sm-12">
                <div id="panel-1" class="panel">
                    <div class="panel-container show">
                        <div class="panel-header">
                            <div class="col-md-12 my-3 px-3">
                                <h4>Bank Account Details </h4>
                            </div>
                        </div>
                        <div class="panel-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p style="font-weight: 600;" class="fw-bold mb-1">Bank Name :</p>
                                            </div>
                                            <div class="col">
                                                <p class="fst-italic text-400 mb-0"><?= isset($bank_info['bank_name']) && !empty($bank_info['bank_name']) ? $bank_info['bank_name'] : null ?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p style="font-weight: 600;" class="fw-semi-bold mb-1">Account No :</p>
                                            </div>
                                            <div class="col">
                                                <p class="fst-italic text-400 mb-0"><?= isset($bank_info['ac_number']) && !empty($bank_info['ac_number']) ? $bank_info['ac_number'] : null ?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p style="font-weight: 600;" class="fw-semi-bold mb-1">Account Holder Name :</p>
                                            </div>
                                            <div class="col">
                                                <p class="fst-italic text-400 mb-0"><?= isset($bank_info['ac_name']) && !empty($bank_info['ac_name']) ? $bank_info['ac_name'] : null ?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p style="font-weight: 600;" class="fw-semi-bold mb-1">Branch :</p>
                                            </div>
                                            <div class="col">
                                                <p class="fst-italic text-400 mb-0"><?= isset($bank_info['branch']) && !empty($bank_info['branch']) ? $bank_info['branch'] : null ?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p style="font-weight: 600;" class="fw-semi-bold mb-0">IFSC :</p>
                                            </div>
                                            <div class="col">
                                                <p class="fst-italic text-400 mb-0"><?= isset($bank_info['IFSC_code']) && !empty($bank_info['IFSC_code']) ? $bank_info['IFSC_code'] : null ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <p class="m-t-0" style="">Available Balance</p>
                                        </div>
                                        <div class="col-md-12 text-right">
                                            <h2 class="m-t-0 text-primary" style="font-weight: bold;">&#8377; <?= isset($available_balance) && !empty($available_balance) ? number_format($available_balance, 2) : number_format(0, 2) ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="transaction_data" class="table table-hover table-striped w-100" style="margin-top:2px;" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>

                                    <th class="text-center">SN</th>
                                    <?php if (empty($bank_id)) { ?>
                                        <th>Bank Name</th>
                                        <th>A/C</th>
                                    <?php } ?>
                                    <th>Date</th>
                                    <th>Payer/Receiver Name</th>
                                    <th>Description</th>
                                    <th>Credit</th>
                                    <th>Debit</th>
                                    <?php if (isset($bank_id) && !empty($bank_id)) { ?>
                                        <th>Balance</th>
                                    <?php } ?>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- Delete Category ajax code -->
<script type="text/javascript">
    var bank_id = '<?= $bank_id ?>';
    var order_arr = [0, (bank_id != '' && bank_id != null ? 7 : 8)];

    $(document).ready(function () {
        var _transaction_data = $('#transaction_data').DataTable({
            dom: "<'row '<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'lf><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                    extend: 'pdfHtml5',
                    text: '<i class="fal fa-file-pdf "></i>',
                    titleAttr: 'Generate PDF',
                    className: 'btn-outline-danger btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'excelHtml5',
                    text: '<i class="fal fa-file-excel "></i>',
                    titleAttr: 'Generate Excel',
                    className: 'btn-outline-success btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'csvHtml5',
                    text: '<i class="fal fa-file-csv "></i>',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-primary btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    }
                }, {
                    extend: 'copyHtml5',
                    text: '<i class="fal fa-copy "></i>',
                    titleAttr: 'Copy to clipboard',
                    className: 'btn-outline-info btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'print',
                    text: '<i class="fal fa-print "></i>',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-dark btn-sm hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-dark-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }
            ],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            },
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": order_arr,
                    "orderable": false
                }, {
                    "targets": order_arr,
                    "className": 'text-center'
                }],
            "ajax": {
                "url": "<?= base_url() ?>admin/Csettings/get_transaction_list/" + bank_id,
                "type": "POST"
            }

        });
        $("#transaction_data thead input").on('keyup change', function () {
            _transaction_data.column($(this).parent().index() + ':visible').search(this.value).draw();
        });

    });

    $(document).on('click', '.DeleteTransaction', function () {
        var transaction_id = $(this).attr('name');
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: '<?php echo base_url('admin/Csettings/delete_bank_transaction') ?>',
                            data: {transaction_id: transaction_id},
                            cache: false,
                            success: function () {
                                location.reload();
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });
</script>
