<!-- Bank debit credit manage -->

<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Bank Transaction
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-success btn-large mr-1" href="<?php echo base_url() ?>admin/Csettings/index">Add Bank</a>
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Csettings/manage_bank_transaction">Manage Bank Transaction</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Csettings/bank_debit_credit_manage_add', array('class' => '', 'id' => 'validate')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label for="bank_name" class="form-label"><?php echo ('Bank Name') ?> <i class="text-danger">*</i></label>
                                    <select class="form-control select2" name="bank_id" data-placeholder="-- Select Bank --" required="">
                                        <option value="" selected="">--Select Bank--</option>
                                        <?php
                                        if (isset($bank_list) && !empty($bank_list)) {
                                            foreach ($bank_list as $key => $value) {
                                                ?>
                                                <option value="<?= isset($value['bank_id']) && !empty($value['bank_id']) ? $value['bank_id'] : null ?>"><?= isset($value['bank_name']) && !empty($value['bank_name']) ? $value['bank_name'] : null ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <span></span>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label for="date" class="form-label"><?php echo ('Date') ?> <i class="text-danger">*</i></label>
                                    <?php $date = date('d-m-Y'); ?>
                                    <input type="date" class="form-control" name="date" id="date" required="" placeholder="<?php echo ('date') ?>" value="<?php echo $date; ?>" />
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label for="account_type" class="form-label">Entry Type <i class="text-danger">*</i></label>
                                    <select class="form-control select2" id="account_type" data-placeholder="-- Select Type --" name="account_type" required="">
                                        <option value="" selected="">Entry Type</option>
                                        <option value="1">Credit (+)</option>
                                        <option value="2">Debit (-)</option>
                                    </select>
                                    <span></span>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label for="withdraw_deposite_id" class="form-label">Payer / Receiver Name <i class="text-danger">*</i></label>
                                    <input type="text" class="form-control" name="withdraw_deposite_id" id="withdraw_deposite_id" required="" placeholder="Payer/Receiver Name" />
                                    <span></span>
                                </div>
                            </div>        
                            <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label for="ammount" class="form-label"><?php echo ('Amount') ?> <i class="text-danger">*</i></label>
                                    <input type="number" class="form-control" name="ammount" id="ammount" required="" placeholder="<?php echo ('Amount') ?>" />
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <label for="description" class="form-label"><?php echo ('Description') ?> </label>
                                    <textarea class="form-control" placeholder="<?php echo ('Description') ?>" name="description" ></textarea>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row" style="gap:5px;">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-bank-transaction" value="Save"><span class="fal fa-check mr-1"></span>Save</button>
                        <a href="<?= base_url() ?>admin/Csettings/manage_bank_transaction"  class="btn btn-secondary btn-large">Cancel</a>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#validate').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>



