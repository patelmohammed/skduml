<page backtop="22mm" backbottom="20mm" backleft="5mm" backright="5mm">
    <page_header style="height:20%;">
        <table style="width: 100%; border-bottom: solid 1px blue;line-height: 18pt;">
            <tr>
                <td style="text-align: left;width: 40%;">
                    <img src="<?= $company_detail['company_logo'] != '' ? $company_detail['company_logo'] : '' ?>" style="width:150px;"/>
                </td>
                <td style="text-align: right;width: 60%;">
                    <span style="font-size: 18px;">Sale Order</span><br/>
                    <span style="font-size: 22px;"><b><?= $company_detail['company_name'] != '' ? $company_detail['company_name'] : '' ?></b></span><br>
                    <span style="font-size: 15px;margin-top: 5px;"><?= $company_detail['company_gst_no'] != '' ? 'GST No : ' . $company_detail['company_gst_no'] : '' ?></span>
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer>
        <table style="width:100%;border-bottom: solid 1px blue;line-height: 18pt;">
            <tr>
                <td style="width:33%"></td>
                <td style="width:33%; text-align: center;"><barcode dimension="1D" type="C128" value="<?= $product_sale['PSNO'] ?>" label="label" style="width:60mm; height:6mm; color: #000000; font-size: 2mm"></barcode></td>
            <td style="width:34%; text-align: right;"><i>page [[page_cu]]/[[page_nb]]</i></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center;font-size: 9pt;">
                    <img src="<?= base_url('my-assets/image/address.png') ?>" style="height: 20px;width: 20px;">
                    <?php
                    $add = '';
                    if (!empty($company_detail['address'])) {
                        $add .= $company_detail['address'];
                    }
                    if (!empty($company_detail['company_city'])) {
                        $add .= ', ' . $company_detail['company_city'];
                    }
                    if (!empty($company_detail['company_state'])) {
                        $add .= ', ' . $company_detail['company_state'];
                    }
                    if (!empty($company_detail['company_country'])) {
                        $add .= ', ' . $company_detail['company_country'];
                    }
                    if (!empty($company_detail['company_pincode'])) {
                        $add .= ' - ' . $company_detail['company_pincode'];
                    }
                    echo $add;
                    if (!empty($company_detail['mobile'])) {
                        ?>
                        <img src="<?= base_url('my-assets/image/phone.png') ?>" style="height:15px;width: 15px;">
                        <?php
                        echo $company_detail['mobile'];
                    }
                    if (!empty($company_detail['email'])) {
                        ?>
                        &nbsp;<img src="<?= base_url('my-assets/image/email.png') ?>" style="height: 18px;width: 18px;">
                        <?php
                        echo $company_detail['email'];
                    }
                    ?>
                </td>
            </tr>
        </table>

    </page_footer>
    <table style="width: 100%;line-height: 16pt;padding-top: 2pt;border-bottom: solid 1px blue;line-height: 18pt;">
        <tr>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;line-height: 16pt;">
                    <tr>
                        <td style="width: 100%;vertical-align: top;">
                            <b style="font-size: 14pt;color:blue;"><i>From,</i></b><br/>
                            <?php
                            $add = '';
                            if (!empty($company_detail['company_name'])) {
                                $add .= '<b>' . $company_detail['company_name'] . '</b><br>';
                            }
                            if (!empty($company_detail['address'])) {
                                $add .= $company_detail['address'];
                            }
                            if (!empty($company_detail['company_city'])) {
                                $add .= ', ' . $company_detail['company_city'];
                            }
                            if (!empty($company_detail['company_state'])) {
                                $add .= ', <br>' . $company_detail['company_state'];
                            }
                            if (!empty($company_detail['company_country'])) {
                                $add .= ', ' . $company_detail['company_country'];
                            }
                            if (!empty($company_detail['company_pincode'])) {
                                $add .= ' - ' . $company_detail['company_pincode'];
                            }
                            $q = trim($add, ', ');
                            $q = trim($q, ', ');
                            echo $q;
                            ?><br/>
                            <?= ($customer_detail['customer_mobile'] != '' ? $customer_detail['customer_mobile'] : ''); ?><br/>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;width: 100%;vertical-align: top;">Sale Order No.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;"><b><?= $product_sale['PSNO'] ?></b></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;vertical-align: top;">Sale Order Date.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;"><b><?= date('d/m/Y H:i', strtotime($product_sale['DateTime'])); ?></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;line-height: 16pt;padding-top: 2pt;border-bottom: solid 1px blue;line-height: 18pt;">
        <tr>
            <td style="width: 50%;vertical-align: top;">
                <b style="font-size: 14pt;color:blue;"><i>Bill To:</i></b><br/>
                <?php
                $add = '';
                if (!empty($product_sale['BillingCusName'])) {
                    $add .= '<b>' . $product_sale['BillingCusName'] . '</b><br>';
                }
                if (!empty($product_sale['BillingAddr1'])) {
                    $add .= $product_sale['BillingAddr1'];
                }
                if (!empty($product_sale['BillingCity'])) {
                    $add .= ', ' . $product_sale['BillingCity'];
                }
                if (!empty($product_sale['BillingState'])) {
                    $add .= ', <br>' . $product_sale['BillingState'];
                }
                if (!empty($product_sale['BillingCountry'])) {
                    $add .= ' - ' . $product_sale['BillingCountry'];
                }
                if (!empty($product_sale['BillingPincode'])) {
                    $add .= ' - ' . $product_sale['BillingPincode'];
                }
                if (!empty($product_sale['BillingPhoneNumber'])) {
                    $add .= '<br>' . $product_sale['BillingPhoneNumber'];
                }
                $q = '';
                $q = trim($add, ', ');
                $q = trim($q, ', ');
                echo $q;
                ?>
            </td>
            <td style="width: 50%;vertical-align: top;">
                <b style="font-size: 14pt;color:blue;"><i>Ship To:</i></b><br/>
                <?php
                $add = '';
                if (!empty($product_sale['ShippingCusName'])) {
                    $add .= '<b>' . $product_sale['ShippingCusName'] . '</b><br>';
                }
                if (!empty($product_sale['ShippingAddr1'])) {
                    $add .= $product_sale['ShippingAddr1'];
                }
                if (!empty($product_sale['ShippingCity'])) {
                    $add .= ', ' . $product_sale['ShippingCity'];
                }
                if (!empty($product_sale['ShippingState'])) {
                    $add .= ', <br>' . $product_sale['ShippingState'];
                }
                if (!empty($product_sale['ShippingCountry'])) {
                    $add .= ' - ' . $product_sale['ShippingCountry'];
                }
                if (!empty($product_sale['ShippingPincode'])) {
                    $add .= ' - ' . $product_sale['ShippingPincode'];
                }
                if (!empty($product_sale['ShippingPhoneNumber'])) {
                    $add .= '<br>' . $product_sale['ShippingPhoneNumber'];
                }
                $q = '';
                $q = trim($add, ', ');
                $q = trim($q, ', ');
                echo $q;
                ?><br/>
            </td>
        </tr>
    </table>
    <table style="width: 100%;line-height: 16pt;padding-top: 2pt;">
        <tr>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;line-height: 16pt;padding-top: 2pt;">
                    <tr>
                        <td>
                            <b>Payment Terms : </b><?= $product_sale['PaymentTerms'] != '' ? $product_sale['PaymentTerms'] : '' ?><br/>
                            <b>Shipping Terms : </b><?= $product_sale['DeliveryTerms'] != '' ? $product_sale['DeliveryTerms'] : '' ?>
                            <br/>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;line-height: 16pt;padding-top: 2pt;">
                    <tr>
                        <td>
                            <b>Ship Via :</b> <?= $product_sale['ShipVia'] != '' ? $product_sale['ShipVia'] : '' ?> <br/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;padding-top: 2pt;margin-top: 10pt;" cellspacing="0">
        <thead>
            <tr style="margin-bottom: 10pt">
                <td style="text-align:center; width:5%; border-top:1px solid blue;border-bottom: 1px solid blue;color: blue;font-size: 9pt;">SN</td>
                <td style="text-align:left; width:26%; border-top:1px solid blue;border-bottom: 1px solid blue;color: blue;font-size: 9pt;">Product</td>
                <td style="text-align:right; width:13%; border-top:1px solid blue;border-bottom: 1px solid blue;color: blue;font-size: 9pt;">Price</td>
                <td style="text-align:right; width:11%; border-top:1px solid blue;border-bottom: 1px solid blue;color: blue;font-size: 9pt;">Quantity</td>
                <td style="text-align:right; width:11%; border-top:1px solid blue;border-bottom: 1px solid blue;color: blue;font-size: 9pt;">Discount</td>
                <td style="text-align:right; width:10%; border-top:1px solid blue;border-bottom: 1px solid blue;color: blue;font-size: 9pt;">Tax</td>
                <td style="text-align:right; width:12%; border-top:1px solid blue;border-bottom: 1px solid blue;color: blue;font-size: 9pt;">Total Tax</td>
                <td style="text-align:right; width:12%; border-top:1px solid blue;border-bottom: 1px solid blue;color: blue;font-size: 9pt;">Total</td>
            </tr>
        </thead>
        <tbody style="font-size: 9pt;">
            <?php
            $i = 0;
            if (count($sale_product_list) > 0) {
                foreach ($sale_product_list as $items):
                    ?>
                    <tr style="line-height: 11pt;">
                        <td style="width:5%;text-align:center;padding-top:2pt;vertical-align: top;"><?= $i + 1 ?></td>
                        <td style="width:26%;text-align:left;padding-top:2pt;vertical-align: top;"><?= getProductNameById($items->refProductID) ?></td>
                        <td style="text-align:right;width:13%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                            <?= number_format($items->Unit_amt, 6) ?>
                        </td>
                        <td style="text-align:right;width:11%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">

                            <?= number_format($items->Qty, 6) ?>
                        </td>
                        <td style="text-align:right;width:11%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                            <?= number_format((isset($items->Discount) ? $items->Discount : 0), 6) ?>
                        </td>
                        <td style="text-align:right;width:10%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                            <?= isset($items->tax_name) ? $items->tax_name : '' ?>
                        </td>
                        <td style="text-align:right;width:12%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                            <b><?php echo number_format((isset($items->TaxAmt) ? $items->TaxAmt : 0), 6); ?></b>
                        </td>
                        <?php $total = $items->total_amt + $items->TaxAmt; ?>
                        <td style="text-align:right;width:12%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                            <b><?php echo number_format($total, 6); ?></b>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
            }
            ?>
        </tbody>
    </table>
    <end_last_page end_height="30mm">
        <table style="width: 100%;padding-top: 2pt;margin-top: 10pt;" cellspacing="0">
            <tr>
                <td style="width: 70%;border-top: 1px solid blue;border-bottom: 1px solid blue;"></td>
                <td style="width:30%;vertical-align: top;border-top: 1px solid blue;border-bottom: 1px solid blue;">
                    <table  style="width: 100%;" cellspacing="0">
                        <tr>
                            <td style="text-align:left;width:40%;color: blue;">Base Total</td>
                            <td style="text-align:right;width:60%;color: blue;"><?= $product_sale['BasicTotal'] ?></td>
                        </tr>
                        <tr>
                            <td style="text-align:left;width:40%;color: blue;">Discount</td>
                            <td style="text-align:right;width:60%;color: blue;"><?= !empty($product_sale['Discount']) ? $product_sale['Discount'] : 0 ?></td>
                        </tr>
                        <?php
                        if (!empty($tax_detail) && isset($tax_detail)) {
                            foreach ($tax_detail as $k2 => $val2) {
                                ?>
                                <tr>
                                    <td style="text-align:left;width:40%;color: blue;"><?= $val2->taxname ?>: </td>
                                    <td style="text-align:right;width:60%;color: blue;"><?= $val2->totaltax ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr>
                            <td style="text-align:left;width:40%;color: blue;">Grand Total: </td>
                            <td style="text-align:right;width:60%;color: blue;"><?= $product_sale['GrandTotal'] ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </end_last_page>
</page>
<?php if ($product_sale['t_n_c'] != '' && !empty($product_sale['t_n_c'])) { ?>
    <page backtop="22mm" backbottom="20mm" backleft="5mm" backright="5mm">
        <page_header style="height:20%;">
            <table style="width: 100%; border-bottom: solid 1px blue;line-height: 18pt;">
                <tr>
                    <td style="text-align: left;width: 40%;">
                        <img src="<?= $company_detail['company_logo'] != '' ? $company_detail['company_logo'] : '' ?>" style="width:150px;"/>
                    </td>
                    <td style="text-align: right;width: 60%;">
                        <span style="font-size: 18px;">Sale Order</span><br/>
                        <span style="font-size: 22px;"><b><?= $company_detail['company_name'] != '' ? $company_detail['company_name'] : '' ?></b></span><br>
                        <span style="font-size: 15px;margin-top: 5px;"><?= $company_detail['company_gst_no'] != '' ? 'GST No : ' . $company_detail['company_gst_no'] : '' ?></span>
                    </td>
                </tr>
            </table>
        </page_header>
        <page_footer>
            <table style="width:100%;border-bottom: solid 1px blue;line-height: 18pt;">
                <tr>
                    <td style="width:33%"></td>
                    <td style="width:33%; text-align: center;"><barcode dimension="1D" type="C128" value="<?= $product_sale['PSNO'] ?>" label="label" style="width:60mm; height:6mm; color: #000000; font-size: 2mm"></barcode></td>
                <td style="width:34%; text-align: right;"><i>page [[page_cu]]/[[page_nb]]</i></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center;font-size: 9pt;">
                        <img src="<?= base_url('my-assets/image/address.png') ?>" style="height: 20px;width: 20px;">
                        <?php
                        $add = '';
                        if (!empty($company_detail['address'])) {
                            $add .= $company_detail['address'];
                        }
                        if (!empty($company_detail['company_city'])) {
                            $add .= ', ' . $company_detail['company_city'];
                        }
                        if (!empty($company_detail['company_state'])) {
                            $add .= ', ' . $company_detail['company_state'];
                        }
                        if (!empty($company_detail['company_country'])) {
                            $add .= ', ' . $company_detail['company_country'];
                        }
                        if (!empty($company_detail['company_pincode'])) {
                            $add .= ' - ' . $company_detail['company_pincode'];
                        }
                        echo $add;
                        if (!empty($company_detail['mobile'])) {
                            ?>
                            <img src="<?= base_url('my-assets/image/phone.png') ?>" style="height:15px;width: 15px;">
                            <?php
                            echo $company_detail['mobile'];
                        }
                        if (!empty($company_detail['email'])) {
                            ?>
                            &nbsp;<img src="<?= base_url('my-assets/image/email.png') ?>" style="height: 18px;width: 18px;">
                            <?php
                            echo $company_detail['email'];
                        }
                        ?>
                    </td>
                </tr>
            </table>

        </page_footer>
        <table style="width: 100%;padding-top: 2pt;border-bottom: solid 1px blue;line-height: 18pt;">
            <tr>
                <td style="width: 50%;vertical-align: top;">
                    <table style="width: 100%;line-height: 16pt;">
                        <tr>
                            <td style="width: 100%;vertical-align: top;">
                                <b style="font-size: 14pt;color:blue;"><i>From,</i></b><br/>
                                <?php
                                $add = '';
                                if (!empty($company_detail['company_name'])) {
                                    $add .= '<b>' . $company_detail['company_name'] . '</b><br>';
                                }
                                if (!empty($company_detail['address'])) {
                                    $add .= $company_detail['address'];
                                }
                                if (!empty($company_detail['company_city'])) {
                                    $add .= ', ' . $company_detail['company_city'];
                                }
                                if (!empty($company_detail['company_state'])) {
                                    $add .= ', <br>' . $company_detail['company_state'];
                                }
                                if (!empty($company_detail['company_country'])) {
                                    $add .= ', ' . $company_detail['company_country'];
                                }
                                if (!empty($company_detail['company_pincode'])) {
                                    $add .= ' - ' . $company_detail['company_pincode'];
                                }
                                $q = trim($add, ', ');
                                $q = trim($q, ', ');
                                echo $q;
                                ?><br/>
                                <?= ($customer_detail['customer_mobile'] != '' ? $customer_detail['customer_mobile'] : ''); ?><br/>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 50%;vertical-align: top;">
                    <table style="width: 100%;">
                        <tr>
                            <td style="text-align: right;width: 100%;vertical-align: top;">Sale Order No.</td>
                        </tr>
                        <tr>
                            <td style="text-align: right;width: 100%;"><b><?= $product_sale['PSNO'] ?></b></td>
                        </tr>
                        <tr>
                            <td style="text-align: right;width: 100%;vertical-align: top;">Sale Order Date.</td>
                        </tr>
                        <tr>
                            <td style="text-align: right;width: 100%;"><b><?= date('d/m/Y H:i', strtotime($product_sale['DateTime'])); ?></b></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="width: 100%;line-height: 16pt;padding-top: 2pt;border-bottom: solid 1px blue;line-height: 18pt;">
            <tr>
                <td style="width: 50%;vertical-align: top;">
                    <b style="font-size: 14pt;color:blue;"><i>Bill To:</i></b><br/>
                    <?php
                    $add = '';
                    if (!empty($product_sale['BillingCusName'])) {
                        $add .= '<b>' . $product_sale['BillingCusName'] . '</b><br>';
                    }
                    if (!empty($product_sale['BillingAddr1'])) {
                        $add .= $product_sale['BillingAddr1'];
                    }
                    if (!empty($product_sale['BillingCity'])) {
                        $add .= ', ' . $product_sale['BillingCity'];
                    }
                    if (!empty($product_sale['BillingState'])) {
                        $add .= ', <br>' . $product_sale['BillingState'];
                    }
                    if (!empty($product_sale['BillingCountry'])) {
                        $add .= ' - ' . $product_sale['BillingCountry'];
                    }
                    if (!empty($product_sale['BillingPincode'])) {
                        $add .= ' - ' . $product_sale['BillingPincode'];
                    }
                    if (!empty($product_sale['BillingPhoneNumber'])) {
                        $add .= '<br>' . $product_sale['BillingPhoneNumber'];
                    }
                    $q = '';
                    $q = trim($add, ', ');
                    $q = trim($q, ', ');
                    echo $q;
                    ?>
                </td>
                <td style="width: 50%;vertical-align: top;">
                    <b style="font-size: 14pt;color:blue;"><i>Ship To:</i></b><br/>
                    <?php
                    $add = '';
                    if (!empty($product_sale['ShippingCusName'])) {
                        $add .= '<b>' . $product_sale['ShippingCusName'] . '</b><br>';
                    }
                    if (!empty($product_sale['ShippingAddr1'])) {
                        $add .= $product_sale['ShippingAddr1'];
                    }
                    if (!empty($product_sale['ShippingCity'])) {
                        $add .= ', ' . $product_sale['ShippingCity'];
                    }
                    if (!empty($product_sale['ShippingState'])) {
                        $add .= ', <br>' . $product_sale['ShippingState'];
                    }
                    if (!empty($product_sale['ShippingCountry'])) {
                        $add .= ' - ' . $product_sale['ShippingCountry'];
                    }
                    if (!empty($product_sale['ShippingPincode'])) {
                        $add .= ' - ' . $product_sale['ShippingPincode'];
                    }
                    if (!empty($product_sale['ShippingPhoneNumber'])) {
                        $add .= '<br>' . $product_sale['ShippingPhoneNumber'];
                    }
                    $q = '';
                    $q = trim($add, ', ');
                    $q = trim($q, ', ');
                    echo $q;
                    ?><br/>
                </td>
            </tr>
        </table>
        <table style="width: 100%;line-height: 16pt;padding-top: 2pt;">
            <tr>
                <td colspan="2" style="padding:2px; background:#fff;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" width="50%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
                                <b>Remarks: </b><br>
                                <?php echo str_replace('</p>', '<br>', str_replace('<p>', '', $product_sale['t_n_c'])); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </page>
<?php } ?>