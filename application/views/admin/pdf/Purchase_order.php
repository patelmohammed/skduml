<page backtop="30mm" backbottom="20mm" backleft="5mm" backright="5mm">
    <page_header style="">
        <table style="width: 100%; border-bottom: solid 1px blue;line-height: 18pt;">
            <tr>
                <td style="text-align: left;width: 40%;">
                    <img src="<?= $company_detail['company_logo'] != '' ? $company_detail['company_logo'] : '' ?>" style="width:150px;"/>
                </td>
                <td style="text-align: right;width: 60%;">
                    <span style="font-size: 18px;">Purchase Order</span><br/>
                    <span style="font-size: 22px;"><b><?= $company_detail['company_name'] != '' ? $company_detail['company_name'] : '' ?></b></span><br>
                    <span style="font-size: 15px;margin-top: 5px;"><?= $company_detail['company_gst_no'] != '' ? 'GST No : ' . $company_detail['company_gst_no'] : '' ?></span>
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer>
        <table style="width:100%;border-bottom: solid 1px blue;line-height: 18pt;">
            <tr>
                <td style="width:33%"></td>
                <td style="width:33%; text-align: center;"><barcode dimension="1D" type="C128" value="<?= $product_pomst['PONo'] ?>" label="label" style="width:60mm; height:6mm; color: #000000; font-size: 2mm"></barcode></td>
            <td style="width:34%; text-align: right;"><i>page [[page_cu]]/[[page_nb]]</i></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center;font-size: 9pt;">
                    <img src="<?= base_url('my-assets/image/address.png') ?>" style="height: 20px;width: 20px;">
                    <?php
                    $add = '';
                    if (!empty($company_detail['address'])) {
                        $add .= $company_detail['address'];
                    }
                    if (!empty($company_detail['company_city'])) {
                        $add .= ', ' . $company_detail['company_city'];
                    }
                    if (!empty($company_detail['company_state'])) {
                        $add .= ', ' . $company_detail['company_state'];
                    }
                    if (!empty($company_detail['company_country'])) {
                        $add .= ', ' . $company_detail['company_country'];
                    }
                    if (!empty($company_detail['company_pincode'])) {
                        $add .= ' - ' . $company_detail['company_pincode'];
                    }
                    echo $add;
                    if (!empty($company_detail['mobile'])) {
                        ?>
                        <img src="<?= base_url('my-assets/image/phone.png') ?>" style="height:15px;width: 15px;">
                        <?php
                        echo $company_detail['mobile'];
                    }
                    if (!empty($company_detail['email'])) {
                        ?>
                        &nbsp;<img src="<?= base_url('my-assets/image/email.png') ?>" style="height: 18px;width: 18px;">
                        <?php
                        echo $company_detail['email'];
                    }
                    ?>
                </td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%;padding: 0pt;border-bottom: solid 1px blue;line-height: 18pt;margin-bottom: 0px;margin-top: 0px;">
        <tr>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;line-height: 16pt;">
                    <tr>
                        <td style="width: 100%;vertical-align: top;">
                            <b style="font-size: 14pt;color:blue;"><i>To,</i></b><br/>
                            <?php
                            $add = '';
                            if (!empty($supplier_detail['supplier_name'])) {
                                $add .= $supplier_detail['supplier_name'] . '<br>';
                            }
                            if (!empty($supplier_detail['address'])) {
                                $add .= $supplier_detail['address'] . '.';
                            }
                            if (!empty($supplier_detail['mobile'])) {
                                $add .= '<br>' . $supplier_detail['mobile'];
                            }
                            $q = '';
                            $q = trim($add, ', ');
                            $q = trim($q, ', ');
                            echo $q;
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;width: 100%;vertical-align: top;">Purchase Order No.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;"><b><?= $product_pomst['PONo'] ?></b></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;vertical-align: top;">Purchase Order Date.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;"><b><?= date('d/m/Y H:i', strtotime($product_pomst['DateTime'])); ?></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;line-height: 16pt;padding-top: 2pt;border-bottom: solid 1px blue;line-height: 18pt;">
        <tr>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;line-height: 16pt;padding-top: 2pt;">
                    <tr>
                        <td>
                            <b style="font-size: 14pt;color:blue;"><i>Bill To:</i></b><br/>
                            <?php
                            $add = '';
                            if (!empty($product_pomst['BillingCompName'])) {
                                $add .= '<b>' . $product_pomst['BillingCompName'] . '</b><br>';
                            }
                            if (!empty($product_pomst['BillingAddr1'])) {
                                $add .= $product_pomst['BillingAddr1'];
                            }
                            if (!empty($product_pomst['BillingCity'])) {
                                $add .= ', ' . $product_pomst['BillingCity'];
                            }
                            if (!empty($product_pomst['BillingState'])) {
                                $add .= ', <br>' . $product_pomst['BillingState'];
                            }
                            if (!empty($product_pomst['BillingCountry'])) {
                                $add .= ' - ' . $product_pomst['BillingCountry'];
                            }
                            if (!empty($product_pomst['BillingPincode'])) {
                                $add .= ' - ' . $product_pomst['BillingPincode'];
                            }
                            if (!empty($product_pomst['BillingPhoneNumber'])) {
                                $add .= '<br>' . $product_pomst['BillingPhoneNumber'];
                            }
                            $q = '';
                            $q = trim($add, ', ');
                            $q = trim($q, ', ');
                            echo $q;
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;line-height: 16pt;padding-top: 2pt;">
                    <tr>
                        <td>
                            <b style="font-size: 14pt;color:blue;"><i>Ship To:</i></b><br/>
                            <?php
                            $add = '';
                            if (!empty($product_pomst['ShippingCompName'])) {
                                $add .= '<b>' . $product_pomst['ShippingCompName'] . '</b><br>';
                            }
                            if (!empty($product_pomst['ShippingAddr1'])) {
                                $add .= $product_pomst['ShippingAddr1'];
                            }
                            if (!empty($product_pomst['ShippingCity'])) {
                                $add .= ', ' . $product_pomst['ShippingCity'];
                            }
                            if (!empty($product_pomst['ShippingState'])) {
                                $add .= ', <br>' . $product_pomst['ShippingState'];
                            }
                            if (!empty($product_pomst['ShippingCountry'])) {
                                $add .= ' - ' . $product_pomst['ShippingCountry'];
                            }
                            if (!empty($product_pomst['ShippingPincode'])) {
                                $add .= ' - ' . $product_pomst['ShippingPincode'];
                            }
                            if (!empty($product_pomst['ShippingPhoneNumber'])) {
                                $add .= '<br>' . $product_pomst['ShippingPhoneNumber'];
                            }
                            $q = '';
                            $q = trim($add, ', ');
                            $q = trim($q, ', ');
                            echo $q;
                            ?>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;line-height: 16pt;padding-top: 2pt;">
        <tr>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;line-height: 16pt;padding-top: 2pt;">
                    <tr>
                        <td>
                            <b>Payment Terms : </b><?= $product_pomst['PaymentTerms'] != '' ? $product_pomst['PaymentTerms'] : '' ?><br/>
                            <b>Shipping Terms : </b><?= $product_pomst['DeliveryTerms'] != '' ? $product_pomst['DeliveryTerms'] : '' ?>
                            <br/>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;line-height: 16pt;padding-top: 2pt;">
                    <tr>
                        <td>
                            <b>Ship Via :</b> <?= $product_pomst['ShipVia'] != '' ? $product_pomst['ShipVia'] : '' ?> <br/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;padding-top: 2pt;margin-top: 5pt;" cellspacing="0">
        <thead>
            <tr>
                <td style="text-align:center; width:5%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">SN</td>
                <td style="text-align:left; width:26%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Product</td>
                <td style="text-align:right; width:13%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Price</td>
                <td style="text-align:right; width:11%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Quantity</td>
                <td style="text-align:right; width:11%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Discount</td>
                <td style="text-align:right; width:10%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Tax</td>
                <td style="text-align:right; width:12%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Total Tax</td>
                <td style="text-align:right; width:12%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Total</td>
            </tr>
        </thead>
        <tbody style="font-size: 9pt;">
            <?php
            $i = 0;
            if (count($pomst_product_list) > 0) {
                foreach ($pomst_product_list as $items):
                    ?>
                    <tr>
                        <td style="width:5%;text-align:center;padding-top:2pt;vertical-align: top;"><?= $i + 1 ?></td>
                        <td style="width:26%;text-align:left;padding-top:2pt;vertical-align: top;"><?= getProductNameById($items->refProductID) ?></td>
                        <td style="text-align:right;width:13%;padding-top:2pt;vertical-align: top;">
                            <?= number_format($items->Unit_amt, 6) ?>
                        </td>
                        <td style="text-align:right;width:11%;padding-top:2pt;vertical-align: top;">

                            <?= number_format($items->Qty, 6) ?>
                        </td>
                        <td style="text-align:right;width:11%;padding-top:2pt;vertical-align: top;">
                            <?= number_format((isset($items->Discount) ? $items->Discount : 0), 6) ?>
                        </td>
                        <td style="text-align:right;width:10%;padding-top:2pt;vertical-align: top;">
                            <?= isset($items->tax_name) ? $items->tax_name : '' ?>
                        </td>
                        <td style="text-align:right;width:12%;padding-top:2pt;vertical-align: top;">
                            <b><?php echo number_format((isset($items->TaxAmt) ? $items->TaxAmt : 0), 6); ?></b>
                        </td>
                        <?php $total = $items->total_amt + $items->TaxAmt; ?>
                        <td style="text-align:right;width:12%;padding-top:2pt;vertical-align: top;">
                            <b><?php echo number_format($total,6); ?></b>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
            }
            ?>
        </tbody>
    </table>
    <end_last_page end_height="30mm">
        <table style="width: 100%;padding-top: 2pt;margin-top: 10pt;" cellspacing="0">
            <tr>
                <td style="width: 70%;border-top: 1px solid blue;border-bottom: 1px solid blue;"></td>
                <td style="width:30%;vertical-align: top;border-top: 1px solid blue;border-bottom: 1px solid blue;">
                    <table  style="width: 100%;" cellspacing="0">
                        <tr>
                            <td style="text-align:left;width:40%;color: blue;">Base Total</td>
                            <td style="text-align:right;width:60%;color: blue;"><?= $product_pomst['BasicTotal'] ?></td>
                        </tr>
                        <tr>
                            <td style="text-align:left;width:40%;color: blue;">Discount</td>
                            <td style="text-align:right;width:60%;color: blue;"><?= !empty($product_pomst['Discount']) ? $product_pomst['Discount'] : 0 ?></td>
                        </tr>
                        <?php
                        if (!empty($tax_detail) && isset($tax_detail)) {
                            foreach ($tax_detail as $k2 => $val2) {
                                ?>
                                <tr>
                                    <td style="text-align:left;width:40%;color: blue;"><?= $val2->taxname ?>: </td>
                                    <td style="text-align:right;width:60%;color: blue;"><?= $val2->totaltax ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr>
                            <td style="text-align:left;width:40%;color: blue;">Grand Total: </td>
                            <td style="text-align:right;width:60%;color: blue;"><?= $product_pomst['GrandTotal'] ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </end_last_page>
</page>