<page backtop="30mm" backbottom="20mm" backleft="5mm" backright="5mm">
    <page_header style="height:20%;">
        <table style="width: 100%; border-bottom: solid 1px blue;line-height: 18pt;">
            <tr>
                <td style="text-align: left;width: 40%;">
                    <img src="<?= $company_detail['company_logo'] != '' ? $company_detail['company_logo'] : '' ?>" style="width:150px;"/>
                </td>
                <td style="text-align: right;width: 60%;">
                    <span style="font-size: 18px;">Sale Outward</span><br/>
                    <span style="font-size: 22px;"><b><?= $company_detail['company_name'] != '' ? $company_detail['company_name'] : '' ?></b></span><br>
                    <span style="font-size: 15px;margin-top: 5px;"><?= $company_detail['company_gst_no'] != '' ? 'GST No : ' . $company_detail['company_gst_no'] : '' ?></span>
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer>
        <table style="width:100%;border-bottom: solid 1px blue;line-height: 18pt;">
            <tr>
                <td style="width:33%"></td>
                <td style="width:33%; text-align: center;"><barcode dimension="1D" type="C128" value="<?= $sale_outward['SONO'] ?>" label="label" style="width:60mm; height:6mm; color: #000000; font-size: 2mm"></barcode></td>
            <td style="width:34%; text-align: right;"><i>page [[page_cu]]/[[page_nb]]</i></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center;font-size: 9pt;">
                    <img src="<?= base_url('my-assets/image/address.png') ?>" style="height: 20px;width: 20px;">
                    <?php
                    $add = '';
                    if (!empty($company_detail['address'])) {
                        $add .= $company_detail['address'];
                    }
                    if (!empty($company_detail['company_city'])) {
                        $add .= ', ' . $company_detail['company_city'];
                    }
                    if (!empty($company_detail['company_state'])) {
                        $add .= ', ' . $company_detail['company_state'];
                    }
                    if (!empty($company_detail['company_country'])) {
                        $add .= ', ' . $company_detail['company_country'];
                    }
                    if (!empty($company_detail['company_pincode'])) {
                        $add .= ' - ' . $company_detail['company_pincode'] . '<br>';
                    }
                    echo $add;
                    if (!empty($company_detail['mobile'])) {
                        ?>
                        <img src="<?= base_url('my-assets/image/phone.png') ?>" style="height:15px;width: 15px;">
                        <?php
                        echo $company_detail['mobile'];
                    }
                    if (!empty($company_detail['email'])) {
                        ?>
                        &nbsp;<img src="<?= base_url('my-assets/image/email.png') ?>" style="height: 18px;width: 18px;">
                        <?php
                        echo $company_detail['email'];
                    }
                    ?>
                </td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%;line-height: 16pt;">
        <tr>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;line-height: 16pt;">
                    <tr>
                        <td style="width: 100%;vertical-align: top;">
                            <b style="font-size: 14pt;color:blue;"><i>To,</i></b><br/>
                            <?php
                            $add = '';
                            if (!empty($customer_detail['customer_name'])) {
                                $add .= '<b>' . $customer_detail['customer_name'] . '</b><br>';
                            }
                            if (!empty($customer_detail['customer_address'])) {
                                $add .= $customer_detail['customer_address'];
                            }
                            if (!empty($customer_detail['customer_city'])) {
                                $add .= ', ' . $customer_detail['customer_city'];
                            }
                            if (!empty($customer_detail['customer_state'])) {
                                $add .= ', <br>' . $customer_detail['customer_state'];
                            }
                            if (!empty($customer_detail['customer_country'])) {
                                $add .= ', ' . $customer_detail['customer_country'];
                            }
                            if (!empty($customer_detail['customer_pincode'])) {
                                $add .= ' - ' . $customer_detail['customer_pincode'];
                            }
                            if (!empty($customer_detail['customer_mobile'])) {
                                $add .= '<br>' . $customer_detail['customer_mobile'];
                            }
                            $q = trim($add, ', ');
                            $q = trim($q, ', ');
                            echo $q;
                            ?><br/>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;width: 100%;vertical-align: top;">Sale Outward No.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;"><b><?= $sale_outward['SONO'] ?></b></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;vertical-align: top;">Sale Outward Date.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;"><b><?= date('d/m/Y H:i', strtotime($sale_outward['DateTime'])); ?></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;padding-top:10pt;margin-top: 2pt;" cellspacing="0">
        <thead>
            <tr>
                <td style="text-align:center; width:5%; border-top:1px solid blue;color: blue;font-size: 9pt;">SN</td>
                <td style="text-align:left; width:26%; border-top:1px solid blue;color: blue;font-size: 9pt;">Product</td>
                <td style="text-align:right; width:13%; border-top:1px solid blue;color: blue;font-size: 9pt;">Price</td>
                <td style="text-align:right; width:11%; border-top:1px solid blue;color: blue;font-size: 9pt;">Quantity</td>
                <td style="text-align:right; width:11%; border-top:1px solid blue;color: blue;font-size: 9pt;">Discount</td>
                <td style="text-align:right; width:10%; border-top:1px solid blue;color: blue;font-size: 9pt;">Tax</td>
                <td style="text-align:right; width:12%; border-top:1px solid blue;color: blue;font-size: 9pt;">Total Tax</td>
                <td style="text-align:right; width:12%; border-top:1px solid blue;color: blue;font-size: 9pt;">Total</td>
            </tr>
        </thead>
        <tbody style="font-size: 9pt;">
            <?php
            $i = 0;
            if (count($outward_sale_list) > 0) {
                foreach ($outward_sale_list as $items):
                    ?>
                    <tr style="border-top:1px solid blue;line-height: 11pt;">
                        <td style="border-top:1px solid blue;width:5%;text-align:center;padding-top:2pt;vertical-align: top;"><?= $i + 1 ?></td>
                        <td style="border-top:1px solid blue;width:26%;text-align:left;padding-top:2pt;vertical-align: top;">
                            <?= getProductNameById($items->refProductID) ?><br>
                            <span style="font-size: 10px;margin-left: 5px;"><b>Order No : <?= 'SO-' . $company_code . '-' . sprintf('%06d', $items->PSNO) ?></b></span>
                        </td>
                        <td style="border-top:1px solid blue;text-align:right;width:13%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                            <?= number_format($items->UnitAmt, 6) ?>
                        </td>
                        <td style="border-top:1px solid blue;text-align:right;width:11%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">

                            <?= number_format($items->Qty, 6) ?>
                        </td>
                        <td style="border-top:1px solid blue;text-align:right;width:11%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                            <?= number_format((isset($items->Discount) ? $items->Discount : 0), 6) ?>
                        </td>
                        <td style="border-top:1px solid blue;text-align:right;width:10%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                            <?= isset($items->tax_name) ? $items->tax_name : '' ?>
                        </td>
                        <td style="border-top:1px solid blue;text-align:right;width:12%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                            <b><?php echo number_format((isset($items->TaxAmt) ? $items->TaxAmt : 0), 6); ?></b>
                        </td>
                        <?php
                        $tax = $items->TaxAmt != '' && $items->TaxAmt != ' ' ? $items->TaxAmt : 0;
                        $total = $items->TotalAmt + $tax;
                        ?>
                        <td style="border-top:1px solid blue;text-align:right;width:12%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                            <b><?= number_format($total, 6) ?></b>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
            }
            ?>
        </tbody>
    </table>
    <end_last_page end_height="60mm">
        <table style="width: 100%;padding-top: 10pt;margin-top: 2pt;" cellspacing="0">
            <tr>
                <td style="width: 70%;border-top: 1px solid blue;border-bottom: 1px solid blue;"></td>
                <td style="width:30%;vertical-align: top;border-top: 1px solid blue;border-bottom: 1px solid blue;">
                    <table  style="width: 100%;" cellspacing="0">
                        <tr>
                            <td style="text-align:left;width:40%;color: blue;">Base Total</td>
                            <td style="text-align:right;width:60%;color: blue;"><?= $sale_outward['BasicTotal'] ?></td>
                        </tr>
                        <tr>
                            <td style="text-align:left;width:40%;color: blue;">Discount</td>
                            <td style="text-align:right;width:60%;color: blue;"><?= !empty($sale_outward['Discount']) ? $sale_outward['Discount'] : 0 ?></td>
                        </tr>
                        <?php
                        if (!empty($tax_detail) && isset($tax_detail)) {
                            foreach ($tax_detail as $k2 => $val2) {
                                ?>
                                <tr>
                                    <td style="text-align:left;width:40%;color: blue;"><?= $val2->taxname ?>: </td>
                                    <td style="text-align:right;width:60%;color: blue;"><?= $val2->totaltax ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr>
                            <td style="text-align:left;width:40%;color: blue;">Grand Total: </td>
                            <td style="text-align:right;width:60%;color: blue;"><?= $sale_outward['GrandTotal'] ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </end_last_page>
</page>