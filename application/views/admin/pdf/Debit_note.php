<page backtop="70mm" backbottom="30mm" backleft="5mm" backright="5mm" pagegroup="new"> 
    <style>
        th, td{
            padding-left: 4pt;
            padding-right: 4pt;
            padding-bottom: 4pt;
            font-size: 9pt;
        }
        .prod-table td{
            padding-left: 2pt;
            padding-right: 2pt;
        }
        .prod-table th{
            padding-bottom: 0pt;
        }
    </style>
    <?php
    $logo_url = parse_url($company_detail['company_logo'])['path'];
    ?>
    <page_header>
        <table style="width: 95%;vertical-align: top;border-collapse: collapse;margin-left: 5mm;margin-bottom: 0pt;padding-bottom: 0pt;">
            <tr>
                <td style="width: 33%;padding: 0;font-size: 14pt;vertical-align: top;">
                    &nbsp;<br>
                    <b>%%File_type%%</b>
                    <br>
                    <br>
            <barcode dimension="1D" type="C128" value="<?= filesupportname($debit_note_data['debit_no']) ?>" label="none" style="width:60mm; height:6mm; color: #000000; font-size: 2mm"></barcode>
            </td>
            <td style="width: 33%;padding: 0;font-size: 14pt;vertical-align: top;text-align: center;font-weight: bold;"><span style="font-size: 9pt;">|| Shree Ganeshay Namah: ||</span><br/><br/><br/>Credit Note</td>
            <td style="width: 34%;padding: 0;vertical-align: top;text-align: right;">
                <img src="<?= isset($company_detail['company_logo']) && !empty($company_detail['company_logo']) && file_exists($_SERVER['DOCUMENT_ROOT'] . $logo_url) ? $company_detail['company_logo'] : '' ?>"> <br/>&nbsp;<br/>&nbsp;
            </td>
            </tr>
        </table>
        <table style="width: 95%;border-collapse: collapse;vertical-align: top;margin-left: 5mm;margin-top: -10pt;margin-bottom: -12pt;">
            <tr>
                <td style="width:70%;padding:0;border: 1px solid #d3d3d3;vertical-align: top;">
                    <table style="width: 100%;vertical-align: top;border-collapse: collapse;height:100%;">
                        <tr>
                            <th style="border: 1px solid #d3d3d3;width: 50%;background-color: #d3d3d3;padding: 0;">Bill To: </th>
                            <th style="border: 1px solid #d3d3d3;width: 50%;background-color: #d3d3d3;padding: 0;">Ship To: </th>
                        </tr>
                        <tr>
                            <td style="width: 50%;vertical-align: top;border-bottom: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;padding-top: 0pt;padding-right: 0pt;padding-bottom: 8pt;padding-left: 2pt;height: 51pt;">
                                <?php
                                if (isset($customer_detail) && !empty($customer_detail) && $customer_detail != 0) {
                                    $add = '';
                                    if (!empty($customer_detail['company__name'])) {
                                        $add .= '<b>' . $customer_detail['company__name'] . '</b><br>';
                                    }
                                    if (!empty($customer_detail['customer_address'])) {
                                        $add .= $customer_detail['customer_address'];
                                    }
                                    if (!empty($customer_detail['customer_city'])) {
                                        $add .= ', ' . $customer_detail['customer_city'];
                                    }
                                    if (!empty($customer_detail['customer_state'])) {
                                        $add .= ',<br>' . $customer_detail['customer_state'];
                                    }
                                    if (!empty($customer_detail['customer_country'])) {
                                        $add .= ', ' . $customer_detail['customer_country'];
                                    }
                                    if (!empty($customer_detail['customer_pincode'])) {
                                        $add .= ' - ' . $customer_detail['customer_pincode'];
                                    }
                                    if (!empty($customer_detail['customer_mobile'])) {
                                        $add .= '<br>' . $customer_detail['customer_mobile'];
                                    }
                                    $q = trim($add, ', ');
                                    $q = trim($q, ', ');
                                    echo $q;
                                } elseif (isset($supplier_detail) && !empty($supplier_detail) && $supplier_detail != 0) {
                                    $add = '';
                                    if (isset($supplier_detail['supplier_name']) && !empty($supplier_detail['supplier_name'])) {
                                        $add .= '<b>' . $supplier_detail['supplier_name'] . '</b><br>';
                                    }
                                    if (isset($supplier_detail['address']) && !empty($supplier_detail['address'])) {
                                        $add .= $supplier_detail['address'];
                                    }
                                    if (isset($supplier_detail['mobile']) && !empty($supplier_detail['mobile'])) {
                                        $add .= '<br>' . $supplier_detail['mobile'];
                                    }
                                    $q = trim($add, ', ');
                                    $q = trim($q, ', ');
                                    echo $q;
                                } else {
                                    echo '&nbsp;';
                                }
                                ?>
                            </td>
                            <td style="width: 50%;vertical-align: top;border-bottom: 1px solid #d3d3d3;padding-top: 0pt;padding-right: 0pt;padding-bottom: 8pt;padding-left: 2pt;height: 51pt;"><!--line-height: 11pt;-->
                                <?php
                                if (isset($customer_detail) && !empty($customer_detail) && $customer_detail != 0) {
                                    $add = '';
                                    if (!empty($customer_detail['company__name'])) {
                                        $add .= '<b>' . $customer_detail['company__name'] . '</b><br>';
                                    }
                                    if (!empty($customer_detail['customer_address'])) {
                                        $add .= $customer_detail['customer_address'];
                                    }
                                    if (!empty($customer_detail['customer_city'])) {
                                        $add .= ', ' . $customer_detail['customer_city'];
                                    }
                                    if (!empty($customer_detail['customer_state'])) {
                                        $add .= ',<br>' . $customer_detail['customer_state'];
                                    }
                                    if (!empty($customer_detail['customer_country'])) {
                                        $add .= ', ' . $customer_detail['customer_country'];
                                    }
                                    if (!empty($customer_detail['customer_pincode'])) {
                                        $add .= ' - ' . $customer_detail['customer_pincode'];
                                    }
                                    if (!empty($customer_detail['customer_mobile'])) {
                                        $add .= '<br>' . $customer_detail['customer_mobile'];
                                    }
                                    $q = trim($add, ', ');
                                    $q = trim($q, ', ');
                                    echo $q;
                                } elseif (isset($supplier_detail) && !empty($supplier_detail) && $supplier_detail != 0) {
                                    $add = '';
                                    if (isset($supplier_detail['supplier_name']) && !empty($supplier_detail['supplier_name'])) {
                                        $add .= '<b>' . $supplier_detail['supplier_name'] . '</b><br>';
                                    }
                                    if (isset($supplier_detail['address']) && !empty($supplier_detail['address'])) {
                                        $add .= $supplier_detail['address'];
                                    }
                                    if (isset($supplier_detail['mobile']) && !empty($supplier_detail['mobile'])) {
                                        $add .= '<br>' . $supplier_detail['mobile'];
                                    }
                                    $q = trim($add, ', ');
                                    $q = trim($q, ', ');
                                    echo $q;
                                } else {
                                    echo '&nbsp;';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 100%;vertical-align: top;padding: 0pt;height: 35pt;">
                                <table style="width: 100%;vertical-align: top;border-collapse: collapse;border: 0;">
                                    <?php
                                    if (isset($customer_detail) && !empty($customer_detail) && $customer_detail != 0) {
                                        echo '<tr>';
                                        echo '<td style="width: 40%;padding-bottom: 0pt;">GST No: ' . (isset($customer_detail['customer_gst_no']) && !empty($customer_detail['customer_gst_no']) ? $customer_detail['customer_gst_no'] : '') . '</td>';
                                        echo '<td style="width: 20%;padding-bottom: 0pt;">State Code: ' . (isset($customer_detail['state_code_str']) && !empty($customer_detail['state_code_str']) ? $customer_detail['state_code_str'] : '') . '</td>';
                                        echo '<td style="width: 40%;padding-bottom: 0pt;">State Name: ' . (isset($customer_detail['state_name']) && !empty($customer_detail['state_name']) ? $customer_detail['state_name'] : '') . '</td>';
                                        echo '</tr>';
                                    } elseif (isset($supplier_detail) && !empty($supplier_detail) && $supplier_detail != 0) {
                                        echo '<tr>';
                                        echo '<td style="width: 40%;padding-bottom: 0pt;">GST No: ' . (isset($supplier_detail['supplier_gst_no']) && !empty($supplier_detail['supplier_gst_no']) ? $supplier_detail['supplier_gst_no'] : '') . '</td>';
                                        echo '<td style="width: 20%;padding-bottom: 0pt;">State Code: ' . (isset($supplier_detail['state_code_str']) && !empty($supplier_detail['state_code_str']) ? $supplier_detail['state_code_str'] : '') . '</td>';
                                        echo '<td style="width: 40%;padding-bottom: 0pt;">State Name: ' . (isset($supplier_detail['state_name']) && !empty($supplier_detail['state_name']) ? $supplier_detail['state_name'] : '') . '</td>';
                                        echo '</tr>';
                                    } else {
                                        echo '<tr>';
                                        echo '<td style="width: 40%;padding-bottom: 0pt;">GST No: &nbsp;</td>';
                                        echo '<td style="width: 20%;padding-bottom: 0pt;">State Code: &nbsp;</td>';
                                        echo '<td style="width: 40%;padding-bottom: 0pt;">State Name: &nbsp;</td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                    <?php if (isset($customer_detail) && !empty($customer_detail) && $customer_detail != 0) { ?>
                                        <tr>
                                            <td style="width: 60%;padding-bottom: 0pt;" colspan="2">
                                                <b>Contact Person: </b><?= isset($customer_detail['customer_name']) && !empty($customer_detail['customer_name']) ? $customer_detail['customer_name'] : '' ?>&nbsp;
                                            </td>
                                            <td style="width: 40%;padding-bottom: 0pt;">
                                                <b>Phone: </b><?= isset($customer_detail['customer_mobile']) && !empty($customer_detail['customer_mobile']) ? $customer_detail['customer_mobile'] : '' ?>&nbsp;
                                            </td>
                                        </tr>
                                    <?php } elseif (isset($supplier_detail) && !empty($supplier_detail) && $supplier_detail != 0) { ?>
                                        <tr>
                                            <td style="width: 60%;padding-bottom: 0pt;" colspan="2">
                                                <b>Contact Person: </b><?= isset($supplier_detail['contact_person']) && !empty($supplier_detail['contact_person']) ? $supplier_detail['contact_person'] : '' ?>&nbsp;
                                            </td>
                                            <td style="width: 40%;padding-bottom: 0pt;">
                                                <b>Phone: </b><?= isset($supplier_detail['mobile']) && !empty($supplier_detail['mobile']) ? $supplier_detail['mobile'] : '' ?>&nbsp;
                                            </td>
                                        </tr>
                                    <?php } else { ?>
                                        <tr>
                                        <td style="width: 60%;padding-bottom: 0pt;" colspan="2">
                                            <b>Contact Person: </b>&nbsp;
                                        </td>
                                        <td style="width: 40%;padding-bottom: 0pt;">
                                            <b>Phone: </b>&nbsp;
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width:30%;border: 1px solid #d3d3d3;padding: 0;">
                    <table style="width: 100%;vertical-align: top;border-collapse: collapse;">
                        <tr>
                            <td style="border: 1px solid #d3d3d3;width: 100%;padding-left: 0; padding-right: 0;">&nbsp;CN. No : <b><?= $debit_note_data['debit_no'] ?></b></td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #d3d3d3;width: 100%;padding-left: 0; padding-right: 0;">&nbsp;Inv. Dt : <b><?= date('d/m/Y', strtotime($debit_note_data['debit_date'])); ?></b></td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #d3d3d3;width: 100%;padding-left: 0; padding-right: 0;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #d3d3d3;width: 100%;padding-left: 0; padding-right: 0;">&nbsp;Valid Dt : <b><?= date('d/m/Y', strtotime($debit_note_data['valid_date'])); ?></b></td>
                        </tr>
                        <tr>
                            <td style="width: 100%;padding-left: 0; padding-right: 0;border: 1px solid #d3d3d3;">&nbsp;E-Way No: <b><?= isset($debit_note_data['EwayBillNo']) && !empty($debit_note_data['EwayBillNo']) ? $debit_note_data['EwayBillNo'] : '' ?></b></td>
                        </tr>
                        <tr>
                            <td style="width: 100%;padding-left: 0;padding-right: 0;border-top: 1px solid #d3d3d3;height: 35pt;">&nbsp;Sales Person : <b><?= isset($debit_note_data['agent_name']) && !empty($debit_note_data['agent_name']) ? $debit_note_data['agent_name'] : '' ?></b></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer  style="">
        <table style="width:100%;border-collapse: collapse;margin-left: 10pt;">
            <tr>
                <td style="width: 27%;font-size: 8pt;vertical-align: top;">Created By: <?= $CurrentUser['first_name'] . " " . $CurrentUser['last_name'] ?></td>
                <td style="width: 28%;font-size: 8pt;vertical-align: top;">Phone No: <?= $CurrentUser['mobile'] ?></td>
                <td style="width: 30%;font-size: 8pt;vertical-align: top;">Email ID: <?= $CurrentUser['email'] ?></td>
                <td style="width: 15%;font-size: 8pt;vertical-align: top;text-align: left;"><i>page [[page_cu]]/[[page_nb]]</i></td>
            </tr>
            <tr>
                <td style="width: 100%;font-size: 8pt;vertical-align: top;" colspan="4">------------------------------------------------------------------------------------------------: Regd. Office :--------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td style="width: 100%;vertical-align: top;" colspan="4">
                    <table style="width: 100%;border-collapse: collapse;">
                        <tr>
                            <td style="width: 30%;vertical-align: top;border-right: 1px dashed black;">
                                <table style="width: 100%;border-collapse: collapse;">
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;"><?= $company_detail['company_city'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;">PAN : <?= $company_detail['pan_number'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;">GST : <?= $company_detail['company_gst_no'] ?></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 40%;vertical-align: top;border-left: 1px dashed black;border-right: 1px dashed black;">
                                <table style="width: 100%;border-collapse: collapse;">
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;"><?= str_replace("\n", '', $company_detail['address']) ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;"><?= $company_detail['company_city'] ?>, <?= $company_detail['company_state'] ?> – <?= $company_detail['company_pincode'] ?>. <?= $company_detail['company_country'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;"></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 30%;vertical-align: top;border-left: 1px dashed black;">
                                <table style="width: 100%;border-collapse: collapse;">
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;">Phone : <?= $company_detail['mobile'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;">E-mail : <?= $company_detail['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;">Website : <?= $company_detail['website'] ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </page_footer>

    <table style="width: 100%;border-collapse: collapse;vertical-align: top;page-break-inside: avoid;">
        <thead>
            <tr>
                <th style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">No.</th>
                <th style="width: 38%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Product Description</th>
                <th style="width: 10%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">HSN/SAC</th>
                <th style="width: 10%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Qty.</th>
                <th style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">UNT</th>
                <th style="width: 10%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Price</th>
                <th style="width: 10%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Taxes</th>
                <th style="width: 12%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Base Total</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $blnk_tbl = FALSE;
            $max_cnt = $min_cnt = 0;
            $i = 0;
            $j = count($debit_note_detail_data);
            $blank_row = $j > 0 ? 5 - ($j % 5) : 5;
            $base_amt = 0;
            $tax_amt = 0;
            $discount = 0;
            $grand_total = 0;
            $tax = array();
            $total_line = 0;
            $tr_data = [];
            if (!empty($debit_note_detail_data) && isset($debit_note_detail_data)) {
                foreach ($debit_note_detail_data as $key => $product_cart_list) {

                    $prd_print_string = $srno_print_string = $po_print_string = $challan_print_string = $dept_print_string = $rmk_print_string = $tkt_print_string = $pr_prd_print_string = $pr_srno_print_string = $pr_ast_print_string = $pr_dept_print_string = $wday_print_string = $shipto_print_string = '';
                    $fin_str_cnt = 0;
                    $total = $product_cart_list['quantity'] * $product_cart_list['unit_price'];
                    $dis = $product_cart_list['discount_amt'];
                    $base_amt = $base_amt + $total;
                    $discount = $discount + $dis;
                    $tax_amt = $tax_amt + $product_cart_list['tax_amt'];
                    $grand_total = ($grand_total + $total + $product_cart_list['tax_amt']) - ($product_cart_list['discount_amt']);
                    $round = (ceil($grand_total)) - $grand_total;
                    $i++;
                    $dm_prd_str = getProductNameById2($product_cart_list['ref_product_id']);
                    $trm_prd_str = preg_replace('/\s+/', ' ', $dm_prd_str);
                    $prd_print_string = product_string($trm_prd_str, 40, 40);
                    $fin_str_cnt += $prd_print_string[1];

                    $hsn_str = getProductHSNById($product_cart_list['ref_product_id']);

                    $total_line += $fin_str_cnt;
                    $td_desc = '<b>' . trim($prd_print_string[0]) . '</b>';

                    $tr_data[] = [
                        0 => $i,
                        1 => $td_desc,
                        2 => $hsn_str,
                        3 => number_format($product_cart_list['quantity'], 2),
                        4 => number_format($product_cart_list['unit_price'], 2),
                        5 => number_format($total, 2),
                        6 => number_format($product_cart_list['tax_percentage'], 2) . " %",
                        7 => $total_line,
                        8 => $fin_str_cnt
                    ];
                }
                if (!empty($tr_data)) {
                    foreach ($tr_data as $tr_key => $tr_val) {
                        if ($max_cnt >= 24) {
                            $blnk_tbl = TRUE;
                            $min_cnt = 0;
                            if (($max_cnt + (isset($tr_data[$tr_key + 1][8]) ? $tr_data[$tr_key + 1][8] : 0)) > 45) {
                                $max_cnt = 0;
                            }
                        } else {
                            $min_cnt += $tr_val[8];
                        }
                        $max_cnt += $tr_val[8];
                        ?>
                        <tr>
                            <td style="width: 5%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;"><?= $tr_val[0] ?></td>
                            <td style="width: 38%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;"><?= $tr_val[1] ?></td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;text-align: center;"><?= $tr_val[2] ?></td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;text-align: center;"><?= $tr_val[3] ?></td>
                            <td style="width: 5%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;text-align: center;">KG</td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;text-align: right;"><?= $tr_val[4] ?></td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;text-align: center;"><?= $tr_val[6] ?></td>
                            <td style="width: 12%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;text-align: right;"><?= $tr_val[5] ?></td>
                        </tr>
                        <?php
                    }
                }
                if ($min_cnt > 0) {
                    for ($k = $min_cnt; $k < 25; $k++) {
                        $blnk_tbl = FALSE;
                        ?>
                        <tr>
                            <td style="width: 5%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 38%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 5%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 12%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                        </tr>
                        <?php
                    }
                }
                if ($max_cnt > 24 && $max_cnt <= 45) {
                    for ($k1 = $max_cnt; $k1 < 46; $k1++) {
                        $blnk_tbl = TRUE;
                        ?>
                        <tr>
                            <td style="width: 5%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 38%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 5%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                            <td style="width: 12%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                        </tr>
                        <?php
                    }
                }
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <td style="width: 5%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 0px;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                <td style="width: 38%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 0px;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                <td style="width: 10%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 0px;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                <td style="width: 10%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 0px;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                <td style="width: 5%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 0px;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                <td style="width: 10%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 0px;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                <td style="width: 10%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 0px;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
                <td style="width: 12%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 0px;vertical-align: top;padding-bottom: 0pt;">&nbsp;</td>
            </tr>
        </tfoot>
    </table>
    <?php
    if ($blnk_tbl) {
        ?>
        <div style="page-break-after:always;"></div>
        <table style="width: 100%;border-collapse: collapse;vertical-align: top;">
            <thead>
                <tr>
                    <th style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">No.</th>
                    <th style="width: 38%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Product Description</th>
                    <th style="width: 10%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">HSN/SAC</th>
                    <th style="width: 10%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Qty.</th>
                    <th style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Qty.</th>
                    <th style="width: 10%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Unit Price <img src="<?= base_url("assets/cur/inr.png"); ?>" style="height:10pt;"></th>
                    <th style="width: 10%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Taxes</th>
                    <th style="width: 12%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;">Base Total <img src="<?= base_url("assets/cur/inr.png"); ?>" style="height:10pt;"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($k1 = 0; $k1 < 25; $k1++) {
                    $border_bottom = $k1 == 24 ? 'border-bottom:1px solid #d3d3d3;' : '';
                    ?>
                    <tr>
                        <td style="width: 5%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;<?= $border_bottom ?>">&nbsp;</td>
                        <td style="width: 38%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;<?= $border_bottom ?>">&nbsp;</td>
                        <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;<?= $border_bottom ?>">&nbsp;</td>
                        <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;<?= $border_bottom ?>">&nbsp;</td>
                        <td style="width: 5%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;<?= $border_bottom ?>">&nbsp;</td>
                        <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;<?= $border_bottom ?>">&nbsp;</td>
                        <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;<?= $border_bottom ?>">&nbsp;</td>
                        <td style="width: 12%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;<?= $border_bottom ?>">&nbsp;</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }
    ?>
    <end_last_page end_height="75mm">
        <div>
            <table style="width: 100%;border-collapse: collapse;padding-bottom: 0;">
                <tr>
                    <td style="width: 60%;border-collapse: collapse;padding: 0pt;vertical-align: top;">
                        <table style="width: 100%;border-collapse: collapse;padding-bottom: 0;">
                            <tr>
                                <td style="width: 30%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: left;font-weight: bold;">Bank</td>
                                <td style="width: 70%;border: 1px solid #d3d3d3;font-size: 8pt;text-align: left;"><?= isset($bank_data['bank_name']) && !empty($bank_data['bank_name']) ? $bank_data['bank_name'] : '' ?></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: left;font-weight: bold;">A/C No</td>
                                <td style="width: 70%;border: 1px solid #d3d3d3;font-size: 8pt;text-align: left;"><?= isset($bank_data['ac_number']) && !empty($bank_data['ac_number']) ? $bank_data['ac_number'] : '' ?></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: left;font-weight: bold;">IFSC</td>
                                <td style="width: 70%;border: 1px solid #d3d3d3;font-size: 8pt;text-align: left;"><?= isset($bank_data['IFSC_code']) && !empty($bank_data['IFSC_code']) ? $bank_data['IFSC_code'] : '' ?></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width: 100%;border: 1px solid #d3d3d3;font-size: 8pt;text-align: justify;text-justify: inter-word;line-height: 11.5pt;"><b><u>Declaration</u></b><br/>1. Goods once sold will not be taken back or exchanged.<br/>2. Payment Terms : ON BOOKING<br/>3. If Delay in payment interest & other charges will be charged @24%. Our risk and responsibility ceases as soon as the goods leave our premises. "Subject to 'BARDOLI' Jurisdiction only. E.B.0.E"</td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 40%;border-collapse: collapse;padding: 0pt;vertical-align: top;">
                        <table style="width: 100%;border-collapse: collapse;padding-bottom: 0;">
                            <tr>
                                <td style="width: 40%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: left;font-weight: bold;padding-right: 0pt;">Total Before Tax</td>
                                <td style="width: 60%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: right;font-weight: bold;"><img src="<?= base_url("assets/cur/inr.png"); ?>" style="height:12px;"><?= number_format($base_amt, 2) ?></td>
                            </tr>
                            <tr>
                                <td style="width: 40%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: left;">Discount</td>
                                <td style="width: 60%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: right;"><img src="<?= base_url("assets/cur/inr.png"); ?>" style="height:12px;"><?= ($discount ? '-' : '') . number_format($discount, 2) ?></td>
                            </tr>
                            <?php if ((isset($debit_note_data['supp_state_id']) && !empty($debit_note_data['supp_state_id']) && $debit_note_data['supp_state_id'] == 23) || (isset($debit_note_data['cust_state_id']) && !empty($debit_note_data['cust_state_id']) && $debit_note_data['cust_state_id'] == 23)) { ?>
                                <tr>
                                    <td style="width: 40%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;font-size: 9pt;text-align: left;height: 6pt;">CGST - <?php echo number_format(($product_cart_list['tax_percentage'] / 2), 2) ?> %</td>
                                    <td style="width: 60%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;font-size: 9pt;text-align: right;height: 6pt;"><img src="<?= base_url("assets/cur/inr.png"); ?>" style="height:12px;"><?php echo number_format(($tax_amt / 2), 2) ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 40%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;font-size: 9pt;text-align: left;height: 6pt;">SGST - <?php echo number_format(($product_cart_list['tax_percentage'] / 2), 2) ?> %</td>
                                    <td style="width: 60%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;font-size: 9pt;text-align: right;height: 6pt;"><img src="<?= base_url("assets/cur/inr.png"); ?>" style="height:12px;"><?php echo number_format(($tax_amt / 2), 2) ?></td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td style="width: 40%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;font-size: 9pt;text-align: left;height: 6pt;">IGST - <?php echo number_format(($product_cart_list['tax_percentage']), 2) ?> %</td>
                                    <td style="width: 60%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;font-size: 9pt;text-align: right;height: 6pt;"><img src="<?= base_url("assets/cur/inr.png"); ?>" style="height:12px;"><?php echo number_format(($tax_amt), 2) ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 40%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;font-size: 9pt;text-align: left;height: 6pt;">&nbsp;</td>
                                    <td style="width: 60%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;font-size: 9pt;text-align: right;height: 6pt;">&nbsp;</td>
                                </tr>
                            <?php } ?>
                            <?php
//                            if (isset($debit_note_data['FrightCharges']) && !empty($debit_note_data['FrightCharges']) && $debit_note_data['FrightCharges'] != 0) {
//                                $grand_total = $grand_total + $debit_note_data['FrightCharges'];
//                            }
                            ?>  
                            <tr>
                                <td style="width: 40%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: left;">&nbsp;</td>
                                <td style="width: 60%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: right;">&nbsp;</td>
                            </tr>
                            <?php
//                            if (isset($debit_note_data['RoundUpAmt']) && !empty($debit_note_data['RoundUpAmt']) && $debit_note_data['RoundUpAmt'] != 0) {
//                                $grand_total = $grand_total + $debit_note_data['RoundUpAmt'];
//                            }
                            ?>
                            <tr>
                                <td style="width: 40%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: left;">&nbsp;</td>
                                <td style="width: 60%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: right;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 40%;border: 1px solid #d3d3d3;font-size: 9pt;text-align: left;font-weight: bold;height: 8pt;padding-top: 3pt;">CN. Total:</td>
                                <td style="width: 60%;border: 1px solid #d3d3d3;font-size: 12pt;text-align: right;font-weight: bold;height: 8pt;padding-top: 3pt;"><img src="<?= base_url("assets/cur/inr.png"); ?>" style="height:12px;"><?= number_format($grand_total, 2) ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%;border-collapse: collapse;padding: 0pt;vertical-align: top;" colspan="2">
                        <table style="width: 100%;border-collapse: collapse;padding-bottom: 0;">
                            <tr>
                                <td style="width: 60%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-top: 1px solid #d3d3d3;font-size: 8pt;font-weight: bold;text-align: left;vertical-align: top;height: 62pt;">Receiver’s Signature: </td>
                                <td style="width: 40%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-top: 1px solid #d3d3d3;font-size: 8pt;font-weight: bold;text-align: left;vertical-align: top;height: 62pt;" rowspan="2">
                                    For, <?= ucfirst($company_detail['company_name']) ?>
                                    <br>

                                </td>
                            </tr>
                            <tr>
                                <td style="width: 60%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;text-align: center;vertical-align: top;"></td>
                            </tr>
                            <tr>
                                <td style="width: 60%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;font-size: 8pt;font-weight: bold;text-align: left;vertical-align: top;">Seal</td>
                                <td style="width: 40%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;font-size: 8pt;font-weight: bold;text-align: left;vertical-align: top;">Authorised Sign & Stamp&nbsp;&nbsp;E. & O.E.</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </end_last_page>
</page>