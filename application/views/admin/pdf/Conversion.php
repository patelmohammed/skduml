<page backtop="30mm" backbottom="20mm" backleft="5mm" backright="5mm">
    <page_header style="height:20%;">
        <table style="width: 100%; border-bottom: solid 1px blue;line-height: 18pt;">
            <tr>
                <td style="text-align: left;width: 40%;">
                    <img src="<?= $company_detail['company_logo'] != '' ? $company_detail['company_logo'] : '' ?>" style="width:150px;"/>
                </td>
                <td style="text-align: right;width: 60%;">
                    <span style="font-size: 18px;">Conversion</span><br/>
                    <span style="font-size: 22px;"><b><?= $company_detail['company_name'] != '' ? $company_detail['company_name'] : '' ?></b></span><br>
                    <span style="font-size: 15px;margin-top: 5px;"><?= $company_detail['company_gst_no'] != '' ? 'GST No : ' . $company_detail['company_gst_no'] : '' ?></span>
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer>
        <table style="width:100%;border-bottom: solid 1px blue;line-height: 18pt;">
            <tr>
                <td style="width:33%"></td>
                <td style="width:33%; text-align: center;"><barcode dimension="1D" type="C128" value="<?= $conversion_data[0]->ConversionNumber ?>" label="label" style="width:60mm; height:6mm; color: #000000; font-size: 2mm"></barcode></td>
            <td style="width:34%; text-align: right;"><i>page [[page_cu]]/[[page_nb]]</i></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center;font-size: 9pt;">
                    <img src="<?= base_url('my-assets/image/address.png') ?>" style="height: 20px;width: 20px;">
                    <?php
                    $add = '';
                    if (!empty($company_detail['address'])) {
                        $add .= $company_detail['address'];
                    }
                    if (!empty($company_detail['company_city'])) {
                        $add .= ', ' . $company_detail['company_city'];
                    }
                    if (!empty($company_detail['company_state'])) {
                        $add .= ', ' . $company_detail['company_state'];
                    }
                    if (!empty($company_detail['company_country'])) {
                        $add .= ', ' . $company_detail['company_country'];
                    }
                    if (!empty($company_detail['company_pincode'])) {
                        $add .= ' - ' . $company_detail['company_pincode'] . '<br>';
                    }
                    echo $add;
                    if (!empty($company_detail['mobile'])) {
                        ?>
                        <img src="<?= base_url('my-assets/image/phone.png') ?>" style="height:15px;width: 15px;">
                        <?php
                        echo $company_detail['mobile'];
                    }
                    if (!empty($company_detail['email'])) {
                        ?>
                        &nbsp;<img src="<?= base_url('my-assets/image/email.png') ?>" style="height: 18px;width: 18px;">
                        <?php
                        echo $company_detail['email'];
                    }
                    ?>
                </td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%;line-height: 16pt;">
        <tr>
            <td style="width: 50%;vertical-align: top;text-align: left;">
                Conversion No.<br>
                <b><?= $conversion_data[0]->ConversionNumber ?></b>
            </td>
            <td style="width: 50%;vertical-align: top;text-align: right;">
                Conversion Date.<br>
                <b><?= date('d/m/Y H:i', strtotime($conversion_data[0]->conversion_datetime)); ?></b>
            </td>
        </tr>
    </table>
    <table style="width: 100%;padding-top:10pt;margin-top: 10pt;" cellspacing="0">
        <thead>
            <tr>
                <td colspan="6" style="text-align:left; width:100%; color: red;font-size: 9pt;font-style: italic;">From</td>
            </tr>
            <tr>
                <td style="text-align:center; width:5%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">SN</td>
                <td style="text-align:left; width:35%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Product</td>
                <td style="text-align:left; width:24%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Supplier</td>
                <td style="text-align:right; width:13%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Price</td>
                <td style="text-align:right; width:11%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Quantity</td>
                <td style="text-align:right; width:12%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Total</td>
            </tr>
        </thead>
        <tbody style="font-size: 9pt;">
            <?php
            $i = 0;
            if (count($conversion_data) > 0) {
                foreach ($conversion_data as $items) {
                    if ($items->type == 'from') {
                        ?>
                        <tr style="border-top:1px solid blue;line-height: 11pt;">
                            <td style="width:5%;text-align:center;padding-top:2pt;vertical-align: top;"><?= $i + 1 ?></td>
                            <td style="width:35%;text-align:left;padding-top:2pt;vertical-align: top;">
                                <?= getProductNameById($items->ref_product_id) ?>
                            </td>
                            <td style="width:24%;text-align:left;padding-top:2pt;vertical-align: top;">
                                <?= getSupplierNameById($items->ref_supplier_id) ?>
                            </td>
                            <td style="text-align:right;width:13%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                                <?= number_format($items->quantity, 6) ?>
                            </td>
                            <td style="text-align:right;width:11%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                                <?= number_format($items->amount, 6) ?>
                            </td>
                            <td style="text-align:right;width:12%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                                <b><?= number_format($items->total, 6); ?></b>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
            }
            ?>
        </tbody>
    </table>
    <table style="width: 100%;padding-top:10pt;margin-top: 10pt;" cellspacing="0">
        <thead>
            <tr>
                <td colspan="6" style="text-align:left; width:100%; color: red;font-size: 9pt;font-style: italic;">To</td>
            </tr>
            <tr>
                <td style="text-align:center; width:5%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">SN</td>
                <td style="text-align:left; width:35%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Product</td>
                <td style="text-align:left; width:24%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Supplier</td>
                <td style="text-align:right; width:13%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Price</td>
                <td style="text-align:right; width:11%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Quantity</td>
                <td style="text-align:right; width:12%; border-top:1px solid blue;border-bottom:1px solid blue;color: blue;font-size: 9pt;">Total</td>
            </tr>
        </thead>
        <tbody style="font-size: 9pt;">
            <?php
            $i = 0;
            if (count($conversion_data) > 0) {
                foreach ($conversion_data as $items) {
                    if ($items->type == 'to') {
                        ?>
                        <tr style="border-top:1px solid blue;line-height: 11pt;">
                            <td style="width:5%;text-align:center;padding-top:2pt;vertical-align: top;"><?= $i + 1 ?></td>
                            <td style="width:35%;text-align:left;padding-top:2pt;vertical-align: top;">
                                <?= getProductNameById($items->ref_product_id) ?>
                            </td>
                            <td style="width:24%;text-align:left;padding-top:2pt;vertical-align: top;">
                                <?= getSupplierNameById($items->ref_supplier_id) ?>
                            </td>
                            <td style="text-align:right;width:13%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                                <?= number_format($items->quantity, 6) ?>
                            </td>
                            <td style="text-align:right;width:11%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                                <?= number_format($items->amount, 6) ?>
                            </td>
                            <td style="text-align:right;width:12%;padding-right: 2pt;padding-top:2pt;vertical-align: top;">
                                <b><?= number_format($items->total, 6); ?></b>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
            }
            ?>
        </tbody>
    </table>
    <end_last_page end_height="60mm">
    </end_last_page>
</page>