<page backtop="32mm" backbottom="20mm" backleft="5mm" backright="5mm" pagegroup="new">
    <?php
    $logo_url = parse_url($company_detail['company_logo'])['path'];
    ?>
    <page_header>
        <table style="width: 95%;vertical-align: top;margin-left: 5mm;margin-bottom: 0pt;padding-bottom: 0pt;border-bottom: 1px solid black;">
            <tr>
                <td style="width: 33%;padding: 0;font-size: 14pt;vertical-align: top;">
                    &nbsp;<br>
                    <b>%%File_type%%</b>
                    <br>
                    <br>
            <barcode dimension="1D" type="C128" value="<?= filesupportname($production_data[0]->production_lot_no) ?>" label="none" style="width:60mm; height:6mm; color: #000000; font-size: 2mm"></barcode>
            </td>
            <td style="width: 33%;padding: 0;font-size: 14pt;vertical-align: top;text-align: center;font-weight: bold;"><span style="font-size: 9pt;">|| Shree Ganeshay Namah: ||</span><br/><br/><br/>Production Details</td>
            <td style="width: 34%;padding: 0;vertical-align: top;text-align: right;">
                <img src="<?= isset($company_detail['company_logo']) && !empty($company_detail['company_logo']) && file_exists($_SERVER['DOCUMENT_ROOT'] . $logo_url) ? $company_detail['company_logo'] : '' ?>"> <br/>&nbsp;<br/>&nbsp;
            </td>
            </tr>
        </table>
    </page_header>
<!--    <page_header style="height:20%;">
        <table style="width: 100%; border-bottom: solid 1px black;line-height: 18pt;">
            <tr>
                <td style="text-align: left;width: 40%;">
                    <img src="<?= $company_detail['company_logo'] != '' ? $company_detail['company_logo'] : '' ?>" style="width:150px;"/>
                </td>
                <td style="text-align: right;width: 60%;">
                    <span style="font-size: 18px;">Production</span><br/>
                    <span style="font-size: 22px;"><b><?= $company_detail['company_name'] != '' ? $company_detail['company_name'] : '' ?></b></span><br>
                    <span style="font-size: 15px;margin-top: 5px;"><?= $company_detail['company_gst_no'] != '' ? 'GST No : ' . $company_detail['company_gst_no'] : '' ?></span>
                </td>
            </tr>
        </table>
    </page_header>-->
    <page_footer>
        <div style="text-align: right; margin-bottom: 5px;"><i>page [[page_cu]]/[[page_nb]]</i></div>
        <table style="width:100%;border-top: solid 1px black;line-height: 18pt;">
<!--            <tr>
                <td style="width:33%"></td>
                <td style="width:33%; text-align: center;">
            <barcode dimension="1D" type="C128" value="<?= $production_data[0]->production_lot_no ?>" label="label" style="width:60mm; height:6mm; color: #000000; font-size: 2mm"></barcode>
            </td>
            <td style="width:34%; text-align: right;"><i>page [[page_cu]]/[[page_nb]]</i></td>
            </tr>-->
            <tr>
                <td colspan="3" style="text-align: center;font-size: 9pt; width: 100%;">
                    <img src="<?= base_url('my-assets/image/address.png') ?>" style="height: 20px;width: 20px;">
                    <?php
                    $add = '';
                    if (!empty($company_detail['address'])) {
                        $add .= $company_detail['address'];
                    }
                    if (!empty($company_detail['company_city'])) {
                        $add .= ', ' . $company_detail['company_city'];
                    }
                    if (!empty($company_detail['company_state'])) {
                        $add .= ', ' . $company_detail['company_state'];
                    }
                    if (!empty($company_detail['company_country'])) {
                        $add .= ', ' . $company_detail['company_country'];
                    }
                    if (!empty($company_detail['company_pincode'])) {
                        $add .= ' - ' . $company_detail['company_pincode'] . '<br>';
                    }
                    echo $add;
                    if (!empty($company_detail['mobile'])) {
                        ?>
                        <img src="<?= base_url('my-assets/image/phone.png') ?>" style="height:15px;width: 15px;">
                        <?php
                        echo $company_detail['mobile'];
                    }
                    if (!empty($company_detail['email'])) {
                        ?>
                        &nbsp;<img src="<?= base_url('my-assets/image/email.png') ?>" style="height: 18px;width: 18px;">
                        <?php
                        echo $company_detail['email'];
                    }
                    ?>
                </td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%;line-height: 16pt;padding-top: 10pt;">
        <tr>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;line-height: 16pt;">
                    <tr>
                        <td style="width: 100%;vertical-align: top;">
                            <b style="font-size: 14pt;color:black;"><i>Assigned,</i></b><br/>

                            <?php
                            $add = '';
                            if (!empty($production_data[0]->emp_name)) {
                                $add .= '<b>' . $production_data[0]->emp_name . '</b>';
                            }
                            if (!empty($production_data[0]->email)) {
                                $add .= '<br>' . $production_data[0]->email;
                            }
                            $q = '';
                            $q = trim($add, ', ');
                            $q = trim($q, ', ');
                            echo $q;
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50%;vertical-align: top;">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;width: 100%;vertical-align: top;">Production Lot No.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;"><b><?= $production_data[0]->production_lot_no ?></b></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;vertical-align: top;">Production Date.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;width: 100%;"><b><?= date('d/m/Y H:i', strtotime($production_data[0]->production_start_datetime)); ?></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;border-collapse: collapse; vertical-align: top;page-break-inside: avoid; margin-top: 10px;">
        <thead>
            <tr>
                <th style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;padding: 5px 0;">No.</th>
                <th style="width: 45%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;padding: 5px 0;" colspan="2">Product</th>
                <th style="width: 35%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;padding: 5px 0;" colspan="2">Unit</th>
                <th style="width: 15%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3;padding: 5px 0;">Quantity</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 0;
            if (count($production_data) > 0) {
                foreach ($production_data as $product_cart_list) {
                    ?>
                    <tr>
                        <td style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;padding: 5px 3px;border-bottom: none;"><?= $i + 1 ?></td>
                        <td style="width: 45%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt; padding: 5px 3px;" colspan="2"><b><?= $product_cart_list->product_name ?></b></td>
                        <td style="width: 35%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;text-align: center;padding: 5px 3px;" colspan="2"><?= $product_cart_list->unit ?></td>
                        <td style="width: 15%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding-bottom: 0pt;text-align: center;padding: 5px 3px;"><?= $product_cart_list->product_qty ?></td>
                    </tr>
                    <?php
                    if (!empty($product_cart_list->raw_product) && isset($product_cart_list->raw_product)) {
                        ?>
                        <tr>
                            <td style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;border-bottom: none;"></td>
                            <td style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;"><b>No.</b></td>
                            <td style="width: 40%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;"><b>Total Product</b></td>
                            <td style="width: 13%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;"><b>Unit</b></td>
                            <td style="width: 22%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;"><b>Consumption</b></td>
                            <td style="width: 15%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;"><b>Total Quantity</b></td>
                        </tr>
                        <?php
                        foreach ($product_cart_list->raw_product as $key => $value) {
                            $key++;
                            ?>
                            <tr>
                                <td style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: start;border-bottom: none;"></td>
                                <td style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: start;border-bottom: none;"><?= $key ?></td>
                                <td style="width: 40%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: start;border-bottom: none;"><?= $value->product_name ?></td>
                                <td style="width: 13%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;border-bottom: none;"><?= $value->unit ?></td>
                                <td style="width: 22%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;border-bottom: none;"><?= $value->consumption ?></td>
                                <td style="width: 15%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;border-bottom: none;"><?= $value->total_qty ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    <?php
                    $i++;
                }
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <td style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: start;"></td>
                <td style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: start;"></td>
                <td style="width: 40%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: start;"></td>
                <td style="width: 13%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;"></td>
                <td style="width: 22%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;"></td>
                <td style="width: 15%;border:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding:5px 3px;text-align: center;"></td>
            </tr>
        </tfoot>
    </table>
<!--    <table style="width: 100%;padding-top:10pt;margin-top: 10pt;" cellspacing="0">
        <thead>
            <tr>
                <td style="text-align:center; width:3%;border-top:1px solid black;color: black;font-size: 9pt;">SN</td>
                <td colspan="2" style="text-align:left; width:45%; border-top:1px solid black;color: black;font-size: 9pt;">Product</td>
                <td colspan="2" style="text-align:left; width:34%; border-top:1px solid black;color: black;font-size: 9pt;">Unit</td>
                <td style="text-align:right; width:18%; border-top:1px solid black;color: black;font-size: 9pt;">Quantity</td>
            </tr>
        </thead>
        <tbody style="font-size: 9pt;">
    <?php
    $i = 0;
    if (count($production_data) > 0) {
        foreach ($production_data as $product_cart_list) {
            ?>
                                                                                                    <tr>
                                                                                                        <td style="border-top:1px solid black;width:3%;text-align:center;padding-top:2pt;vertical-align: top;padding-top: 8px;"><?= $i + 1 ?></td>
                                                                                                        <td colspan="2" style="border-top:1px solid black;width:45%;text-align:left;padding-top:2pt;vertical-align: top;padding-top: 8px;">
            <?= $product_cart_list->product_name ?>
                                                                                                        </td>
                                                                                                        <td colspan="2" style="border-top:1px solid black;width:34%;text-align:left;padding-top:2pt;vertical-align: top;padding-top: 8px;">
            <?= $product_cart_list->unit ?>
                                                                                                        </td>
                                                                                                        <td style="border-top:1px solid black;width:18%;text-align:right;padding-top:2pt;vertical-align: top;padding-top: 8px;">
            <?= $product_cart_list->product_qty ?>
                                                                                                        </td>
                                                                                                    </tr>
            <?php
            if (!empty($product_cart_list->raw_product) && isset($product_cart_list->raw_product)) {
                ?>
                                                                                                                                                <tr>
                                                                                                                                                    <td style="text-align:center; width:3%;border-top:1px solid black;color: black;font-size: 9pt;"></td>
                                                                                                                                                    <td style="text-align:center; width:3%;border-top:1px solid black;color: black;font-size: 9pt;">SN</td>
                                                                                                                                                    <td style="text-align:left; width:42%;border-top:1px solid black;color: black;font-size: 9pt;">Raw product</td>
                                                                                                                                                    <td style="text-align:left; width:17%;border-top:1px solid black;color: black;font-size: 9pt;">Unit</td>
                                                                                                                                                    <td style="text-align:right; width:17%;border-top:1px solid black;color: black;font-size: 9pt;">Consumption</td>
                                                                                                                                                    <td style="text-align:right; width:18%;border-top:1px solid black;color: black;font-size: 9pt;">Total Quantity</td>
                                                                                                                                                </tr>
                <?php
                foreach ($product_cart_list->raw_product as $key => $value) {
                    $key++;
                    ?>
                                                                                                                                                                                            <tr>
                                                                                                                                                                                                <td style="border-top:1px solid black;width:3%;text-align:center;padding-top:2pt;vertical-align: top;"></td>
                                                                                                                                                                                                <td style="border-top:1px solid black;width:3%;text-align:center;padding-top:2pt;vertical-align: top;"><?= $key ?></td>
                                                                                                                                                                                                <td style="border-top:1px solid black;width:42%;text-align:left;padding-top:2pt;vertical-align: top;"><?= $value->product_name ?></td>
                                                                                                                                                                                                <td style="border-top:1px solid black;width:17%;text-align:left;padding-top:2pt;vertical-align: top;"><?= $value->unit ?></td>
                                                                                                                                                                                                <td style="border-top:1px solid black;width:17%;text-align:right;padding-top:2pt;vertical-align: top;"><?= $value->consumption ?></td>
                                                                                                                                                                                                <td style="border-top:1px solid black;width:18%;text-align:right;padding-top:2pt;vertical-align: top;"><?= $value->total_qty ?></td>
                                                                                                                                                                                            </tr>
                    <?php
                }
            }
            ?>
            <?php
            $i++;
        }
    }
    ?>
    </tbody>
</table>-->
    <end_last_page end_height="60mm">
    </end_last_page>
</page>