<page backtop="70mm" backbottom="30mm" backleft="5mm" backright="5mm" pagegroup="new"> 
    <style>
        th, td{
            padding-left: 4pt;
            padding-right: 4pt;
            padding-bottom: 4pt;
            font-size: 9pt;
        }
        .prod-table td{
            padding-left: 2pt;
            padding-right: 2pt;
        }
        .prod-table th{
            padding-bottom: 0pt;
        }
    </style>
    <?php
    $logo_url = parse_url($company_detail['company_logo'])['path'];
    ?>
    <page_header>
        <table style="width: 95%;vertical-align: top;border-collapse: collapse;margin-left: 5mm;margin-bottom: 0pt;padding-bottom: 0pt;">
            <tr>
                <td style="width: 33%;padding: 0;font-size: 14pt;vertical-align: top;">
                    &nbsp;<br>
                    <b>%%File_type%%</b>
                    <br>
                    <br>
            <!--<barcode dimension="1D" type="C128" value="<?php //  filesupportname(isset($purchase_invoice_data['SINO']) && !empty($purchase_invoice_data['SINO']) ? $purchase_invoice_data['SINO'] : '')                    ?>" label="none" style="width:60mm; height:6mm; color: #000000; font-size: 2mm"></barcode>-->
                </td>
                <td style="width: 33%;padding: 0;font-size: 14pt;vertical-align: top;text-align: center;font-weight: bold;"><span style="font-size: 9pt;">|| Shree Ganeshay Namah: ||</span><br/><br/><br/>Customer Ledger</td>
                <td style="width: 34%;padding: 0;vertical-align: top;text-align: right;">
                    <img src="<?= isset($company_detail['company_logo']) && !empty($company_detail['company_logo']) && file_exists($_SERVER['DOCUMENT_ROOT'] . $logo_url) ? $company_detail['company_logo'] : '' ?>"> <br/>&nbsp;<br/>&nbsp;
                </td>
            </tr>
        </table>
        <table style="width: 95%;border-collapse: collapse;vertical-align: top;margin-left: 5mm;margin-top: -10pt;margin-bottom: -12pt;">
            <tr>
                <td style="width:100%;padding:0;border: 1px solid #d3d3d3;vertical-align: top;">
                    <table style="width: 100%;vertical-align: top;border-collapse: collapse;height:100%;">
                        <tr>
                            <th style="border: 1px solid #d3d3d3;width: 50%;background-color: #d3d3d3;padding: 5px 5px;">Supplier: </th>
                        </tr>
                        <tr>
                            <td style="width: 100%;vertical-align: top;border-bottom: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;padding-top: 5px;padding-right: 0pt;padding-bottom: 8pt;padding-left: 5px;height: 20pt;">
                                <?php
                                $add = '';
                                if (!empty($customer_detail['company__name'])) {
                                    $add .= '<b>' . $customer_detail['company__name'] . '</b><br>';
                                }
                                if (!empty($customer_detail['customer_address'])) {
                                    $add .= $customer_detail['customer_address'];
                                }
                                if (!empty($customer_detail['customer_city'])) {
                                    $add .= ', ' . $customer_detail['customer_city'];
                                }
                                if (!empty($customer_detail['customer_state'])) {
                                    $add .= ',<br>' . $customer_detail['customer_state'];
                                }
                                if (!empty($customer_detail['customer_country'])) {
                                    $add .= ', ' . $customer_detail['customer_country'];
                                }
                                if (!empty($customer_detail['customer_pincode'])) {
                                    $add .= ' - ' . $customer_detail['customer_pincode'];
                                }
                                if (!empty($customer_detail['customer_mobile'])) {
                                    $add .= '<br>' . $customer_detail['customer_mobile'];
                                }
                                $q = trim($add, ', ');
                                $q = trim($q, ', ');
                                echo $q;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;vertical-align: top;padding: 0pt;height: 45pt;">
                                <table style="width: 100%;vertical-align: top;border-collapse: collapse;border: 0;padding-top: 5px;">
                                    <?php
                                    echo '<tr>';
                                    echo '<td style="width: 40%;padding-bottom: 0pt;">GST No: ' . (isset($customer_detail['customer_gst_no']) && !empty($customer_detail['customer_gst_no']) ? $customer_detail['customer_gst_no'] : '') . '</td>';
                                    echo '<td style="width: 25%;padding-bottom: 0pt;">State Code: ' . (isset($customer_detail['state_code_str']) && !empty($customer_detail['state_code_str']) ? $customer_detail['state_code_str'] : '') . '</td>';
                                    echo '<td style="width: 35%;padding-bottom: 0pt;">State Name: ' . (isset($customer_detail['state_name']) && !empty($customer_detail['state_name']) ? $customer_detail['state_name'] : '') . '</td>';
                                    echo '</tr>';
                                    ?>
                                    <tr>
                                        <td style="width: 65%;padding-bottom: 0pt;" colspan="2">
                                            <b>Contact Person: </b><?= isset($customer_detail['customer_name']) && !empty($customer_detail['customer_name']) ? $customer_detail['customer_name'] : '' ?>&nbsp;
                                        </td>
                                        <td style="width: 35%;padding-bottom: 0pt;">
                                            <b>Phone: </b><?= isset($customer_detail['customer_mobile']) && !empty($customer_detail['customer_mobile']) ? $customer_detail['customer_mobile'] : '' ?>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer  style="">
        <table style="width:100%;border-collapse: collapse;margin-left: 10pt;">
            <tr>
                <td style="width: 27%;font-size: 8pt;vertical-align: top;">Created By: <?= isset($CurrentUser['first_name']) && !empty($CurrentUser['last_name']) && isset($CurrentUser['last_name']) && !empty($CurrentUser['first_name']) ? $CurrentUser['first_name'] . " " . $CurrentUser['last_name'] : '' ?></td>
                <td style="width: 28%;font-size: 8pt;vertical-align: top;">Phone No: <?= isset($CurrentUser['mobile']) && !empty($CurrentUser['mobile']) ? $CurrentUser['mobile'] : '' ?></td>
                <td style="width: 30%;font-size: 8pt;vertical-align: top;">Email ID: <?= isset($CurrentUser['email']) && !empty($CurrentUser['email']) ? $CurrentUser['email'] : '' ?></td>
                <td style="width: 15%;font-size: 8pt;vertical-align: top;text-align: left;"><i>page [[page_cu]]/[[page_nb]]</i></td>
            </tr>
            <tr>
                <td style="width: 100%;font-size: 8pt;vertical-align: top;" colspan="4">------------------------------------------------------------------------------------------------: Regd. Office :--------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td style="width: 100%;vertical-align: top;" colspan="4">
                    <table style="width: 100%;border-collapse: collapse;">
                        <tr>
                            <td style="width: 30%;vertical-align: top;border-right: 1px dashed black;">
                                <table style="width: 100%;border-collapse: collapse;">
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;"><?= isset($company_detail['company_city']) && !empty($company_detail['company_city']) ? $company_detail['company_city'] : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;">PAN : <?= isset($company_detail['pan_number']) && !empty($company_detail['pan_number']) ? $company_detail['pan_number'] : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;">GST : <?= isset($company_detail['company_gst_no']) && !empty($company_detail['company_gst_no']) ? $company_detail['company_gst_no'] : '' ?></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 40%;vertical-align: top;border-left: 1px dashed black;border-right: 1px dashed black;">
                                <table style="width: 100%;border-collapse: collapse;">
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;"><?= isset($company_detail['address']) && !empty($company_detail['address']) ? str_replace("\n", '', $company_detail['address']) : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;"><?= isset($company_detail['company_city']) && !empty($company_detail['company_city']) ? $company_detail['company_city'] : '' ?>, <?= isset($company_detail['company_state']) && !empty($company_detail['company_state']) ? $company_detail['company_state'] : '' ?>, <?= isset($company_detail['company_country']) && !empty($company_detail['company_country']) ? $company_detail['company_country'] : '' ?> – <?= isset($company_detail['company_pincode']) && !empty($company_detail['company_pincode']) ? $company_detail['company_pincode'] : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;"></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 30%;vertical-align: top;border-left: 1px dashed black;">
                                <table style="width: 100%;border-collapse: collapse;">
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;">Phone : <?= isset($company_detail['mobile']) && !empty($company_detail['mobile']) ? $company_detail['mobile'] : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;">E-mail : <?= isset($company_detail['email']) && !empty($company_detail['email']) ? $company_detail['email'] : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size:8pt;vertical-align: top;">Website : <?= isset($company_detail['website']) && !empty($company_detail['website']) ? $company_detail['website'] : '' ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%;border-collapse: collapse;vertical-align: top;page-break-inside: avoid;">
        <thead>
            <tr>
                <th style="width: 5%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3; padding: 5px 0;">No.</th>
                <th style="width: 11%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3; padding: 5px 0;">Date</th>
                <th style="width: 10%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3; padding: 5px 0;">Invoice No</th>
                <th style="width: 13%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3; padding: 5px 0;">Receipt No</th>
                <th style="width: 22%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3; padding: 5px 0;">Description</th>
                <th style="width: 13%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3; padding: 5px 0;">Credit Amt</th>
                <th style="width: 13%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3; padding: 5px 0;">Debit Amt</th>
                <th style="width: 13%;border:1px solid #d3d3d3;font-size: 9pt;text-align: center;vertical-align: top;background-color: #d3d3d3; padding: 5px 0;">Balance</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 0;
            $debit = $credit = $balance = 0;
            if (!empty($customer_ledger_data) && isset($customer_ledger_data)) {
                foreach ($customer_ledger_data as $key => $value) {

                    if ($value['d_c'] == 'c') {
                        $credit += $value['amount'];
                    } else {
                        $credit += '0.00';
                    }

                    if ($value['d_c'] == 'd') {
                        $debit += $value['amount'];
                    } else {
                        $debit += '0.00';
                    }

                    $balance = $credit - $debit;

                    $i++;
                    ?>
                    <tr>
                        <td style="width: 5%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding: 2px 5px;"><?= $i ?></td>
                        <td style="width: 11%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding: 2px 2px;text-align: center;"><?= isset($value['date']) && !empty($value['date']) ? date('d-m-Y', strtotime($value['date'])) : '' ?></td>
                        <td style="width: 10%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding: 2px 2px;text-align: center;"><?= isset($value['SINO']) && !empty($value['SINO']) ? $value['SINO'] : (isset($value['SRNO']) && !empty($value['SRNO']) ? $value['SRNO'] : '') ?></td>
                        <td style="width: 13%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding: 2px 2px;text-align: center;"><?= isset($value['receipt_no']) && !empty($value['receipt_no']) ? $value['receipt_no'] : '' ?></td>
                        <td style="width: 22%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding: 2px 2px;"><?= isset($value['description']) && !empty($value['description']) ? $value['description'] : '' ?></td>
                        <td style="width: 13%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding: 2px 2px;text-align: center;"><?= isset($value['amount']) && !empty($value['amount']) && $value['d_c'] == 'c' ? number_format($value['amount'], 2, '.', ',') : '' ?></td>
                        <td style="width: 13%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding: 2px 2px;text-align: center;"><?= isset($value['amount']) && !empty($value['amount']) && $value['d_c'] == 'd' ? number_format($value['amount'], 2, '.', ',') : '' ?></td>
                        <td style="width: 13%;border-left:1px solid #d3d3d3;border-right:1px solid #d3d3d3;font-size: 9pt;vertical-align: top;padding: 2px 5px;text-align: right;"><?= isset($balance) && !empty($balance) ? number_format($balance, 2, '.', ',') : number_format(0, 2) ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <td style="width: 5%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 1px solid #d3d3d3;vertical-align: top;padding: 2px 5px;">&nbsp;</td>
                <td style="width: 11%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 1px solid #d3d3d3;vertical-align: top;padding: 2px 2px;">&nbsp;</td>
                <td style="width: 10%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 1px solid #d3d3d3;vertical-align: top;padding: 2px 2px;">&nbsp;</td>
                <td style="width: 13%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 1px solid #d3d3d3;vertical-align: top;padding: 2px 2px;">&nbsp;</td>
                <td style="width: 22%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 1px solid #d3d3d3;vertical-align: top;padding: 2px 5px; text-align: right;"><b>Total</b></td>
                <td style="width: 13%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 1px solid #d3d3d3;vertical-align: top;padding: 2px 2px; text-align: center;"><b><?= isset($credit) && !empty($credit) ? number_format($credit, 2, '.', ',') : number_format(0, 2) ?></b></td>
                <td style="width: 13%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 1px solid #d3d3d3;vertical-align: top;padding: 2px 2px; text-align: center;"><b><?= isset($debit) && !empty($debit) ? number_format($debit, 2, '.', ',') : number_format(0, 2) ?></b></td>
                <td style="width: 13%;border-left: 1px solid #d3d3d3;border-right: 1px solid #d3d3d3;border-bottom: 1px solid #d3d3d3;border-top: 1px solid #d3d3d3;vertical-align: top;padding: 2px 5px; text-align: right;"><b><?= isset($balance) && !empty($balance) ? number_format($balance, 2, '.', ',') : number_format(0, 2) ?></b></td>
            </tr>
        </tfoot>
    </table>
</page>