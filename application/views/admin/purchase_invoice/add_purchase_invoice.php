<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Create Purchase Invoice
        </h1>

        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Purchase_invoice">Manage Purchase Invoice</a>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Purchase_invoice/insertPurchaseInvoice', array('class' => '', 'id' => 'insert_purchase_invoice', 'name' => 'insert_purchase_invoice')) ?>
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <div class="form-group">

                                    <label for="datetime">Invoice Date <i class="text-danger">*</i></label>
                                    <div class="input-group date mb-3" id="invoice_date_div">
                                        <?php $date = date('d-m-Y'); ?>
                                        <input type="text" class="form-control datepicker" name="date" value="<?php echo $date; ?>" id="date" required />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="form-group">
                                    <label for="invoice_no">Invoice No. <i class="text-danger">*</i></label>
                                    <input type="text" class="form-control" id="invoice_no" name="invoice_no" required="">
                                </div>
                            </div>

                            <div class="col-md-3 mb-3">
                                <div class="form-group">
                                    <label for="supplier_id">Supplier <i class="text-danger">*</i></label>
                                    <select name="supplier_id" class="form-control select2" id="supplier_id" data-placeholder="-- Select supplier --" required="">
                                        <option value="" data-address=""> Select Supplier</option>
                                        <?php
                                        if ($supplier) {
                                            foreach ($supplier as $k => $spval) {
                                                ?>
                                                <option value="<?= $spval['supplier_id'] ?>" data-address="<?= $spval['address'] ?>"><?= $spval['supplier_name'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3 mb-3">
                                <div class="form-group">
                                    <label for="supplier_address" class="col-form-label" >Supplier Address</label><br>
                                    <sapn id="supplier_address"></sapn>
                                    <input type="hidden" value="" name="hidden_supplier_address" id="hidden_supplier_address">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="datetime">Order Date <i class="text-danger">*</i></label>
                                    <div class="input-group date" id="order_date_div">
                                        <input type="text" class="form-control" id="order_date" name="order_date" data-type="date" required="">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="datetime">Delivery Date <i class="text-danger">*</i></label>
                                    <div class="input-group date" id="delivery_date_div">
                                        <input type="text" class="form-control" id="delivery_date" name="delivery_date" data-type="date" required="">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="invoice_description">Invoice Description</label>
                                    <textarea type="text" rows="3" class="form-control mb-3" id="invoice_description" name="invoice_description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="products">Products <i class="text-danger">*</i></label>
                                    <select name="product[]" class="form-control mb-3" id="product_id" tabindex="8">
                                        <option value="" data-address=""> select Product</option>
<?php
if ($product_list) {
    foreach ($product_list as $k => $spval) {
        ?>
        <!--                                                <option value="<?php echo $spval['product_id'] . "|" . $spval['product_name'] . "|" . $spval['supplier_price'] . "|" . $spval['tax_name'] . "|" . $spval['tax'] . "|" . $spval['tax_id'] . "|" . $spval['PIID'] ?> "><?= $spval['product_name'] ?></option>-->
                                                <option value="<?php echo $spval['id'] . "|" . $spval['product_name'] . "|" . $spval['supplier_price'] . "|" . $spval['tax_name'] . "|" . $spval['tax'] . "|" . $spval['tax_id'] ?> "><?= $spval['product_name'] ?></option>
        <?php
    }
}
?>
                                    </select>
                                </div>
                            </div>
                            <!--                            <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="product_barcode">Product Barcode</label>
                                                                <input type="text" tabindex="" class="form-control" id="product_barcode" name="product_barcode" placeholder="Product Barcode" />
                                                            </div>
                                                        </div>-->
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive" id="product_cart">          
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th width="1%">SN</th>
                                                <th width="22%">Products</th>
                                                <th width="13%">Product Price</th>
                                                <th width="13%">Quantity</th>
                                                <th width="13%">Discount (%)</th>
                                                <th width="9%">Tax</th>
                                                <th width="9%">Tax Amount</th>
                                                <th width="15%">Total</th>
                                                <th width="5%">Action</th>  
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                </div> 
                                <div class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group">
                                            <label for="fright_charges">Fright Charges</label>
                                            <input type="text" class="form-control desimalcheck" id="fright_charges" name="fright_charges" placeholder="Fright Charges" onkeyup="calculateAll();" />
                                        </div>
                                    </div>
                                    <div class="col-md-9"></div>
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group">
                                            <label for="round_up_amt">Round Up</label>
                                            <input type="text" class="form-control desimalcheck" id="round_up_amt" name="round_up_amt" placeholder="Round Up Amount" onkeyup="calculateAll();" />
                                        </div>
                                    </div>
                                    <div class="col-md-9"></div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="g_total">Grand Total</label>
                                            <input type="text" class="form-control" id="g_total" name="g_total" placeholder="Grand Total" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row mt-2">
                            <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-purchase-invoice" value="Save"><span class="fal fa-check mr-1"></span>Save</button>
                            <button type="submit" class="btn btn-warning ml-1 waves-effect waves-themed" name="add-purchase-invoice-another" value="Save & Add Another"><span class="fal fa-check mr-1"></span>Save & Add Another</button>
                            <a href="<?= base_url() ?>admin/Purchase_invoice"  class="btn btn-secondary btn-large ml-1">Cancel</a>
                        </div>
                    </div>
<?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>



<script>
    var company_code = '<?= $company_code ?>';
    var product_id_container = [];
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger ml-r mr-2"
        },
        buttonsStyling: false
    });

    $(function () {

        var suffix = 0;
        var supp_selcted_or_not = 1;
        $(document).on('change', '#product_id', function () {
//            var supplier_id = $('#supplier_id').val();
            var product_details = $('#product_id').val();
            if (product_details != '') {

                var product_details_array = product_details.split('|');
                var index = product_id_container.indexOf(product_details_array[0]);
                var product_id = product_details_array[0];
                var product_name = product_details_array[1];
                var product_price = product_details_array[2];
                var tax_name = product_details_array[3];
                var tax_per = product_details_array[4];
                var tax_id = product_details_array[5];
//                var PIID = product_details_array[6].split(',');
                if (supplier_id == '' || supplier_id == ' ' || supplier_id == undefined) {
                    swalWithBootstrapButtons.fire({
                        title: "Alert!",
                        text: "Please select supplier first.",
                        confirmButtonText: 'Ok',
                        confirmButtonColor: '#3c8dbc'
                    });
                    $('#product_id').val('').change();
                    return false;
                }

                if (index > -1) {
                    swalWithBootstrapButtons.fire({
                        title: "Alert!",
                        text: "Product already ramain in cart.",
                        confirmButtonText: 'Ok',
                        confirmButtonColor: '#3c8dbc'
                    });
                    $('#product_id').val('').change();
                    return false;
                }

//                fullScreenLoader();

                $.ajax({
                    type: "POST",
                    url: '<?= base_url('admin/Purchase_invoice/getPurchaseInwardProductVise') ?>',
                    data: {product_id: product_id},
                    success: function (returnData) {
//                        HoldOn.close();
                        var data = JSON.parse(returnData);




                        if (data.result == true) {

                            if (data.product_detail.length <= 0) {
                                swalWithBootstrapButtons.fire({
                                    title: "Alert!",
                                    text: "No data available related to this product.",
                                    confirmButtonText: 'Ok',
                                    confirmButtonColor: '#3c8dbc'
                                });
                                $('#product_id').val('').change();
                                return false;
                            }

                            var product_detail = data.product_detail;
                            suffix++;
                            var product_row = '';
                            product_row += '<tr class="rowCount" data-id="' + suffix + '" id="row_' + suffix + '">';
                            product_row += '<td style="padding-left: 10px;"><p id="sl_' + suffix + '">' + suffix + '</p></td>';
                            product_row += '<td><span style="padding-bottom: 5px;">' + product_name + '</span></td>';
                            product_row += '<input type="hidden" id="product_id_' + suffix + '" name="product_id[]" value="' + product_id + '"/>';
                            product_row += '<input type="hidden" id="tax_per_' + suffix + '" name="tax_per[]" value="' + tax_per + '"/>';
                            product_row += '<input type="hidden" id="tax_per_amt_' + suffix + '" name="tax_per_amt[]" value=""/>';
                            product_row += '<input type="hidden" id="total_tax_' + suffix + '" name="total_tax[]" value=""/>';
                            product_row += '<input type="hidden" id="discount_ind_' + suffix + '" name="discount_ind[]" value=""/>';
                            product_row += '<input type="hidden" id="BasicTotal_' + suffix + '" name="BasicTotal[]" value=""/>';
                            product_row += '<input type="hidden" id="Tax_id_' + suffix + '" name="Tax_id[]" value="' + tax_id + '"/>';
                            product_row += '<input type="hidden" id="total_amt_' + suffix + '" name="total_amt[]" value=""/>';
                            product_row += '<input type="hidden" id="tax_name_' + suffix + '" name="tax_name[]" value="' + tax_name + '"/>';
                            product_row += '<input type="hidden" id="single_tax_amt_' + suffix + '" name="single_tax_amt[]" value=""/>';
//                            product_row += '<td>';
//                            product_row += '<select name="product_inward_no[]" class="form-control select222" id="pomst_id_' + suffix + '" required="" data-product_id="' + product_id + '" data-supplier_id="' + supplier_id + '" onchange="getPurchaseOrderData(' + suffix + ')">';
//                            product_row += '<option value="" data-address="">Select Inwrd No</option>';
//                            if (product_detail.length > 0) {
//                                product_detail.forEach(function (val, key) {
//                                    product_row += '<option value="' + val.PIID + '">PI-' + company_code + '-' + val.PINO + '</option>';
//                                });
//                            }
//
//                            product_row += '</select>';
//                            product_row += '</td>';
                            product_row += '<td><input type="text" id="unit_price_' + suffix + '" name="unit_price[]" onfocus="this.select();" class="form-control desimalcheck aligning" placeholder="Unit Price" value="' + product_price + '" onkeyup="return calculateAll();" required /><span class="label_aligning"></span></td>';
                            product_row += '<td><input type="text" data-countID="' + suffix + '" id="quantity_amount_' + suffix + '" name="quantity_amount[]" onfocus="this.select();" class="form-control desimalcheck aligning countID"  placeholder="Quantity" onkeyup="return calculateAll();" required data-purchase_inward_qty="0" onblur="checkStock(' + suffix + ')"></td>';
                            product_row += '<td><input type="text" data-countID="' + suffix + '" id="discount_' + suffix + '" name="discount[]" onfocus="this.select();" class="form-control desimalcheck aligning countID discount_percentage"  placeholder="Discount" onkeyup="return calculateAll();" ></td>';
                            product_row += '<td><span id="tax_display_' + suffix + '">' + tax_name + '</span></td>';
                            product_row += '<td><input type="text" id="tax_amount_' + suffix + '" name="tax_amount[]" class="form-control" placeholder="Tax Amount" readonly /><span class="label_aligning"></span></td>';
                            product_row += '<td><input type="text" id="total_' + suffix + '" name="total[]" class="form-control aligning" placeholder="Total" readonly /><span class="label_aligning"></span></td>';
                            product_row += '<td><button class="btn btn-danger red valid" type="button" value="Delete" onclick="deleter(' + suffix + ',\'' + product_id + '\')" tabindex="10" autocomplete="off" aria-invalid="false"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                            product_row += '</tr>';
                            $('#product_cart tbody').append(product_row);
                            $('.select222').css({"width": "100%"}).select2();
                            product_id_container.push(product_details_array[0]);
                            $('#product_id').val('').change();
                            calculateAll();
                        } else {
                            swalWithBootstrapButtons.fire({
                                title: "Alert!",
                                text: "No product available for purchase.",
                                confirmButtonText: 'Ok',
                                confirmButtonColor: '#3c8dbc'
                            });
                            $('#product_id').val('').change();
                            return false;
                        }
                    }
                });
            }
        });
    });

    $(document).on('change', '.discount_percentage', function () {
        let qty = parseFloat($(this).val());
        if (qty > 100) {
            $(this).val(null);
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Discount must be less than or equal to 100%.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
        } else if (qty <= 0) {
            $(this).val(null);
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Discount must be more than 0%.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
        } else {
//                alert('ok');
        }
    });

    function deleter(suffix, product_id) {
        swalWithBootstrapButtons.fire({
            title: "Alert!",
            text: "Are you sure?",
            confirmButtonColor: '#3c8dbc',
            cancelButtonText: 'cancel',
            confirmButtonText: 'Ok',
            showCancelButton: true
        }).then(function (result) {
            if (result.value) {
                $("#row_" + suffix).remove();
                $("#paid").val('');
                var ingredient_id_container_new = [];
                for (var i = 0; i < product_id_container.length; i++) {
                    if (product_id_container[i] != product_id) {
                        ingredient_id_container_new.push(product_id_container[i]);
                    }
                }
                product_id_container = ingredient_id_container_new;
                calculateAll();
            }
        });
    }

    function calculateAll() {
        var subtotal = 0;
        var i = 1;
        $(".rowCount").each(function () {
            var id = $(this).attr("data-id");
            var unit_price = $("#unit_price_" + id).val();
            var tax = $("#tax_per_" + id).val().split(',');
            var temp = "#sl_" + id;
            var product_id = $("#product_id_" + id).val();
            var quantity = $("#quantity_amount_" + id).val();
            var disc = $("#discount_" + id).val();
            var disc_amt = $("#discount_ind" + id).val();
            var disc_type = '';
            var temp_amount = '';
            var total_tax = 0;
            var total_amt = 0;

            $(temp).html(i);
            i++;
            var quantity_amount = $("#quantity_amount_" + id).val();
            if ($.trim(unit_price) == "" || $.isNumeric(unit_price) == false || unit_price == '' || unit_price == null || unit_price == NaN || unit_price == undefined) {
                unit_price = 0;
            }
            if ($.trim(quantity_amount) == "" || $.isNumeric(quantity_amount) == false || quantity_amount == '' || quantity_amount == null || quantity_amount == NaN || quantity_amount == undefined) {
                quantity_amount = 0;
            }
            if ($.trim(disc_amt) == "" || $.isNumeric(disc_amt) == false || disc_amt == '' || disc_amt == null || disc_amt == NaN || disc_amt == undefined) {
                disc_amt = 0;
            }
            if ($.trim(disc) == '' || $.trim(disc) == '%' || $.trim(disc) == '%%' || $.trim(disc) == '%%%' || $.trim(disc) == '%%%%') {
                disc = 0;
            }
            if (disc.length > 0) {
                disc_type = 'percentage';
            }
//              else {
//                disc_type = 'plain';
//            }
            var quantity_amount_and_unit_price = parseFloat($.trim(unit_price)) * parseFloat($.trim(quantity_amount));

            if (disc_type == 'percentage') {
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                temp_amount = quantity_amount_and_unit_price * disc;
                temp_amount = temp_amount / 100;
                $('#discount_ind_' + id).val(temp_amount);
                quantity_amount_and_unit_price = quantity_amount_and_unit_price - temp_amount;
            }
//            else {
//                $('#discount_ind_' + id).val(disc);
//                quantity_amount_and_unit_price = quantity_amount_and_unit_price - disc;
//            }

            if (tax.length > 0) {
                var tax_product = 0;
                var single_tax_amt = [];
                tax.forEach(function (key, val) {
                    var tax1 = quantity_amount_and_unit_price;
                    var tax2 = (tax1 * key) / 100;
                    total_tax += tax2;
                    tax_product += tax2;
                    single_tax_amt.push(tax2);
                });
                $('#single_tax_amt_' + id).val(single_tax_amt.join(','));
                quantity_amount_and_unit_price = quantity_amount_and_unit_price + tax_product;
            }

            var BasicTotal = quantity * unit_price;
            $('#BasicTotal_' + id).val(BasicTotal);

            disc_amt = temp_amount != '' && temp_amount != null && temp_amount != undefined && temp_amount != NaN ? parseFloat(temp_amount) : 0;
            total_amt = parseFloat(BasicTotal) - parseFloat(disc_amt);
            $('#total_amt_' + id).val(parseFloat(total_amt).toFixed(2));

            $('#total_tax_' + id).val(total_tax);
            $('#tax_amount_' + id).val(total_tax);
            $("#total_" + id).val(quantity_amount_and_unit_price.toFixed(2));
            subtotal += parseFloat($.trim($("#total_" + id).val()));
        });

        if (isNaN(subtotal)) {
            subtotal = 0;
        }
        $("#subtotal").val(subtotal);
        var other = parseFloat($.trim($("#other").val()));
        if ($.trim(other) == "" || $.isNumeric(other) == false) {
            other = 0;
        }

        var fright_charges = $('#fright_charges').val();
        var round_up_amt = $('#round_up_amt').val();

        var grand_total = parseFloat(subtotal) + parseFloat(other) + (fright_charges != '' && fright_charges != null && fright_charges != undefined ? parseFloat(fright_charges) : 0) + parseFloat(round_up_amt != '' && round_up_amt != null && round_up_amt != undefined ? parseFloat(round_up_amt) : 0);
        grand_total = grand_total.toFixed(2);
        $("#g_total").val(grand_total);
        var paid = $("#paid").val();
        if ($.trim(paid) == "" || $.isNumeric(paid) == false) {
            paid = 0;
        }

        var due = parseFloat(grand_total) - parseFloat(paid);

        $("#due").val(due.toFixed(2));
    }

    function updateRowNo() {
        var numRows = $("#product_cart tbody tr").length;
        for (var r = 0; r < numRows; r++) {
            $("#product_cart tbody tr").eq(r).find("td:first p").text(r + 1);
        }
    }

    $(document).on('change', '#supplier_id', function () {
        var address = $(this).find(':selected').attr('data-address');
        if (address != '' && address != undefined) {
            $('#supplier_address').text(address);
            $('#hidden_supplier_address').val(address);
        } else {
            $('#supplier_address').text('');
            $('#hidden_supplier_address').val('');
        }
    });

    function checkStock(suffix) {
        var quantity_amount = $('#quantity_amount_' + suffix).val();
        var quantity_sale_order = $('#quantity_amount_' + suffix).attr('data-purchase_inward_qty');
        quantity_amount = parseFloat(quantity_amount);
        quantity_sale_order = parseFloat(quantity_sale_order);

        if ($.trim(quantity_amount) == "" || quantity_amount == undefined || isNaN(quantity_amount)) {
            quantity_amount = 0;
        }
        if ($.trim(quantity_sale_order) == "" || quantity_sale_order == undefined || isNaN(quantity_sale_order)) {
            quantity_sale_order = 0;
        }

        if (quantity_amount <= 0) {
            $('#quantity_amount_' + suffix).val('');
            calculateAll();
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Quantity must be more than 0.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
            return false;
        }
    }

    function checkDue() {
        let grand_total = $("#g_total").val();
        let paid_amount = $("#paid_amount").val();
        let due_amount = parseFloat(grand_total) - parseFloat(paid_amount);
        $('#due_amount').val(due_amount);
    }

    $(document).ready(function () {
        $('#insert_purchase_invoice').validate({
            submitHandler: function (form) {
                if ($('.rowCount').length == 0) {
                    swalWithBootstrapButtons.fire('Alert!', 'Add at least one product.', 'warning');
                    return false;
                }
                form.submit();
            }
        });
    });

</script>
