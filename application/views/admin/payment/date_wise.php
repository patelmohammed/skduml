<!-- Person ledger start -->
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        document.body.style.marginTop = "0px";
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<!-- Person Start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('cashflow') ?></h1>
            <small><?php echo display('cashflow') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('cashflow') ?></a></li>
                <li class="active"><?php echo display('cashflow') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">
        <!-- Manage Product report -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body"> 
                        <div class="row">
                            <?php echo form_open('Cpayment/search_datewise/', array('class' => 'form-vertical',)) ?>
                            <?php $today = dateprint(date('Y-m-d')); ?>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="select"><?php echo display('from') ?></label>
                                    <input type="text" name="from_date"  value="<?php echo $today; ?>" class="datepicker form-control"/>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="select"><?php echo display('to') ?></label>
                                    <input type="text" name="to_date" value="<?php echo $today; ?>" class="datepicker form-control"/>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="select">&nbsp;</label><br>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search-plus" aria-hidden="true"></i>
                                        <?php echo display('search') ?></button>
                                </div>
                            </div>

                            <?php echo form_close() ?>            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="printableArea" style="margin-left:2px;">
                            <div class="table-responsive" style="margin-top: 10px;">
                                <p style="font-size: 17px; color: black; font-weight:bold">
                                    <?php
//                                    echo display('company_name');
//                                    echo "<br>";
//
//                                    echo "<br>";
                                    echo 'Date' . ':' . date('Y-m-d');
                                    ?>  <span id="time"></span><?php echo date(" a", time()); ?></p>

                                <table id="dataTableExample5" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo display('sl') ?></th>
                                            <th>Date of Transaction</th>
                                            <th class="text-center"><?php echo display('name') ?></th>
                                            <th class="text-center"><?php echo display('account_name') ?></th>
                                            <th class="text-center">Description</th>
                                            <th class="text-center"><?php echo display('receipt_amount') ?></th>
                                            <th class="text-center"><?php echo display('paid_amount') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        if ($ledger) {
                                            $sl = 0;
                                            $debit = $credit = $balance = 0;
                                            foreach ($ledger as $single) {
                                                $sl++
                                                ?>
                                                <tr>
                                                    <td><?php echo $sl; ?></td>
                                                    <td><?= $single['date_of_transaction'] ?></td>
                                                    <td>
                                                        <?php
                                                        if ($single['supplier_name']) {
                                                            ?>
                                                            <a href="<?php echo base_url() . 'Csupplier/supplier_details/' . $single['supplier_id']; ?>"><?= $single['supplier_name'] ?></a>
                                                            <?php
                                                        } elseif ($single['customer_name']) {
                                                            ?>
                                                            <a href="<?php echo base_url() . 'Ccustomer/customerledger/' . $single['customer_id']; ?>"><?= $single['customer_name'] ?></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php
                                                        $tran_cat = $single['transaction_category'];
                                                        if ($tran_cat == 1) {
                                                            echo "supplier";
                                                        } elseif ($tran_cat == 2) {
                                                            echo "customer";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td class="<?= $single['description'] == '' ? 'text-center' : '' ?>"><?= $single['description'] == '' ? '-' : $single['description']; ?></td>
                                                    <td align="right">
                                                        <?php
                                                        if ($single['receipt_amount']) {
                                                            echo (($position == 0) ? "$currency " : " $currency");
                                                            echo number_format($single['receipt_amount'], '2', '.', ',');
                                                            $debit += $single['receipt_amount'];
                                                        } else {
                                                            $debit += '0.00';
                                                        }
                                                        ?>
                                                    </td>
                                                    <td align="right">
                                                        <?php
                                                        if ($single['pay_amount']) {
                                                            echo (($position == 0) ? "$currency " : " $currency");
                                                            echo number_format($single['pay_amount'], '2', '.', ',');
                                                            $credit += $single['pay_amount'];
                                                        } else {
                                                            $credit += '0.00';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>

                                    <tfoot>
                                        <tr  align="right">
                                            <td colspan="5"  align="right"><b>Total:</b></td>
                                            <td align="right"><b>
                                                    <?php
                                                    echo (($position == 0) ? "$currency " : "$currency");
                                                    echo number_format(@$debit, '2', '.', ',');
                                                    ?></b>
                                            </td>
                                            <td align="right"><b>
                                                    <?php
                                                    echo (($position == 0) ? "$currency " : "$currency");
                                                    echo number_format(@$credit, '2', '.', ',');
                                                    ?></b>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="text-right"><?php echo $links ?></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

    function report_popup(transection_category)
    {
        fullScreenLoader();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('Cpayment/today_details'); ?>",
            data: "transection_category=" + transection_category,
            success: function (response) {
                HoldOn.close();
                $(".displaycontent").html(response);

            }
        });
        HoldOn.close();
    }
</script>

<div class="modal fade displaycontent" id="myModal">
    <script type="text/javascript">
        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            // add a zero in front of numbers<10
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
            t = setTimeout(function () {
                startTime()
            }, 500);
        }
        startTime();
    </script>