<style type="text/css">
    .hide {
        display: none;
    }
    .show {
        display: block;
    }
</style>

<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Update Payment
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Cpayment/manage_payment">Manage Transaction</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Cpayment/transaction_entry/' . $transaction_id, array('id' => 'validate')) ?>
                    <input type="hidden" name="transaction_type" value="1" />
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="_date" class="form-label"><?php echo ('Date') ?> <i class="text-danger">*</i></label>
                                <input class="form-control" name="date" id="_date" type="date" placeholder="Date"  required="" value="<?= isset($date_of_transaction) && !empty($date_of_transaction) ? date('Y-m-d', strtotime($date_of_transaction)) : '' ?>" >
                                <span></span>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="transaction_category" class="form-label"><?php echo ('Transaction Category') ?>  <i class="text-danger">*</i></label>
                                <select id="selId" class="form-control select2" name="transaction_category" onchange="myFunction()" data-placeholder="-- Select category --" required="">
                                    <option value="">--Select Type--</option>
                                    <option value="1" <?= $transaction_category == 1 ? 'selected' : '' ?>>Supplier</option>
                                    <option value="2" <?= $transaction_category == 2 ? 'selected' : '' ?>>Customer</option>
                                </select>
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="description" class="form-label"><?php echo ('Description') ?></label>
                                <textarea name="description" class="form-control" placeholder="Please Enter some description"></textarea> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="transaction" class="form-label"><span id="demo"></span></label>    
                                <div id="test1" class="hidid ">
                                    <select id="supp_id" name="supplier_id" class="form-control select2" data-placeholder="-- Select supplier --" required="">
                                        <option value=""><?php echo ('Select supplier') ?></option>
                                        <?php
                                        if (isset($supplier) && !empty($supplier)) {
                                            foreach ($supplier as $key => $val) {
                                                $sel_supp = ($val['supplier_id'] == $relation_id && $transaction_category == 1 ? 'selected=""' : '');
                                                echo '<option value="' . $val['supplier_id'] . '" ' . $sel_supp . '>' . $val['supplier_name'] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <span></span>
                                </div>
                                <div id="test2" class="hidid">
                                    <select id="cust_id" name="customer_id" class="form-control select2" data-placeholder="-- Select customer --" required="">
                                        <option value=""><?php echo ('Select Customer') ?></option>
                                        <?php
                                        if (isset($customer) && !empty($customer)) {
                                            foreach ($customer as $key => $val) {
                                                $sel_cus = ($val['customer_id'] == $relation_id && $transaction_category == 2 ? 'selected=""' : '');
                                                echo '<option value="' . $val['customer_id'] . '" ' . $sel_cus . '>' . $val['company__name'] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <span></span>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="payment" class="form-label"><?php echo ('Payment amount') ?><i class="text-danger">*</i></label>
                                <input class="form-control" name="pay_amount" id="payment" type="text" placeholder="Payment Amount" value="<?= $pay_amount ?>" required="">
                                <span></span>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="payment_type" class="form-label">Transaction Mode <i class="text-danger">*</i></label>
                                <select onchange="bank_info_show(this)" name="payment_type" id="payment_type" class="form-control select2" data-placeholder="-- Select mode --" required="">
                                    <option value="">--Select Type--</option>
                                    <?php
                                    if (isset($payment_mode_list) && !empty($payment_mode_list)) {
                                        foreach ($payment_mode_list as $key => $val) {
                                            $sel_mode = ($val->payment_mode_id == $transaction_mode ? 'selected=""' : '');
                                            echo '<option value="' . $val->payment_mode_id . '" ' . $sel_mode . '>' . $val->payment_mode_name . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                                <span></span>
                            </div>
                        </div>
                        <div class="row <?= $transaction_mode == CHEQUE_PAYMENT_METHOD_ID || $transaction_mode == PO_PAYMENT_METHOD_ID ? '' : "hide" ?>" id="bank_info_hide">
                            <div class="col-md-6 mb-3">
                                <label for="bank_name" class="form-label"><?php echo ('Bank Name') ?> <i class="text-danger">*</i></label>
                                <select name="bank_name" id="bank_name"  class="form-control select2" data-placeholder="-- Select state --" required="">
                                    <option value="">--Select Type--</option>
                                    <?php
                                    if (isset($bank) && !empty($bank)) {
                                        foreach ($bank as $key => $val) {
                                            $sel_bank = ($val->bank_id == $bank_id ? 'selected=""' : '');
                                            echo '<option value="' . $val->bank_id . '" ' . $sel_bank . '>' . $val->bank_name . '</option>';
                                        }
                                    }
                                    ?>   
                                </select> 
                                <span></span>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="cheque_or_pay_order_no" class="form-label"><?php echo ('Cheque or Pay Order No') ?> <i class="text-danger">*</i></label>
                                <input type="test" id="cheque_or_pay_order_no" class="form-control"  name="cheque_no" placeholder="<?php echo ('cheque or pay order no') ?>" value="<?= $cheque_no ?>" required=""/>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="cheque_mature_date" class="form-label">Cheque / Pay Order Issue date <i class="text-danger">*</i></label>
                                <input type="date" name="cheque_mature_date" id="cheque_mature_date"  class="form-control" value="<?= $chq_given_date ?>" required=""/>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-customer" value="Save"><span class="fal fa-check mr-1"></span>Submit </button>
                    </div> 
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

-->
<script>
    $(document).ready(function () {
        $('#validate').validate({
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        $("#selId").on("change", function () {
            var selectId = $(this).children("option").filter(":selected").text();
            console.log(selectId);
            $(".hidid").hide();
            if (selectId === "Supplier")
            {
                $("#test1").show();
            } else if (selectId == "Customer")
            {
                $("#test2").show();
            } else if (selectId == "Office")
            {
                $("#test3").show();
            } else if (selectId == "salary")
            {
                $("#test3").show();
            } else if (selectId == "Loan")
            {
                $("#test4").show();
            }
        }).change();
    });

    function myFunction() {
        var x = document.getElementById("selId").value;
        if (x == 1) {
            x = 'Supplier';
            $('#cust_id').val('');
        }
        if (x == 2) {
            x = 'Customer';
            $('#supp_id').val('');
        }
        if (x == 3) {
            x = 'Office'
        }
        if (x == 4) {
            x = 'Loan'
        }
        if (x != 0) {
            document.getElementById("demo").innerHTML = "Select " + x + '<i class="text-danger">*</i>';
        }
    }

    function bank_info_show(payment_type) {
        if (payment_type.value == <?= PO_PAYMENT_METHOD_ID ?> || payment_type.value == <?= CHEQUE_PAYMENT_METHOD_ID ?>) {
            $('#bank_info_hide').removeClass('hide');
        } else {
            $('#bank_info_hide').addClass('hide');
        }
    }
</script>