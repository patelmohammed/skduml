 

<link href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.min.css" rel="stylesheet" type="text/css"/>

<div class="modal-dialog modal-md" role="document">


    <div class="modal-content">
        <div class="modal-header">
            <center><strong><?php echo display('report') ?></strong></center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p><?php
                echo display('daily_reports') . display('account_name') . ':' . $Account_Type;
                echo '<br/>Date: ' . dateprint(date('Y-m-d'));
                ?></p>
            <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center"><?php echo display('account') ?></th>
                        <th class="text-center"><?php echo display('description') ?></th>
                        <th class="text-center"><?php echo display('receipt_amount') ?></th>
                        <th class="text-center"><?php echo display('paid_amount') ?></th>
                    </tr>
                </thead>

                <tbody>

                    <?php
                    if ($marks) {
                        ?>
                        <?php foreach ($marks as $row) { ?>
                            <tr>
                                <td  align="center">
                                    <?php
                                    if (isset($row['customer_name']) && isset($row['customer_id'])) {
                                        ?>
                                        <a href="<?php echo base_url() . 'Ccustomer/customerledger/' . $row['customer_id']; ?>"><?= $row['customer_name'] ?></a>
                                        <?php
                                    } elseif (isset($row['supplier_name']) && isset($row['supplier_id'])) {
                                        ?>
                                        <a href="<?php echo base_url() . 'Csupplier/supplier_details/' . $row['supplier_id']; ?>"><?= $row['supplier_name'] ?></a>
                                        <?php
                                    }
                                    if ($row['customer_name'] == '' && $row['supplier_name'] == '') {
                                        echo $row['relation_id'];
                                    }
                                    ?>
                                </td>

                                <td><?php
                                    echo $row['description'];
                                    ?></td>
                                <td style="text-align: right;"><?php
                                    echo $row['receipt_amount'];
                                    ?></td>
                                <td align="right">
                                    <?php
                                    echo $row['pay_amount'];
                                    ?></td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                </tbody>

                <tfoot>
                    <tr></tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</div>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.min.js" type="text/javascript"></script>

