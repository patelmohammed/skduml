    
<!-- Manage Payment start -->
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        document.body.style.marginTop = "0px";
        $('table tr').find('td:eq(7)').hide();
        $('table tr').find('th:eq(7)').hide();
        $('table tr').find('tfoot:eq(6)').hide();

        window.print();
        document.body.innerHTML = originalContents;
    }
</script>



<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Manage transaction
        </h1>
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <a href="<?php echo base_url('admin/Cpayment') ?>" class="btn btn-success btn-large mr-1"><i class="ti-plus"> </i> Payment </a>
                    <a href="<?php echo base_url('admin/Cpayment/receipt_transaction') ?>" class="btn btn-warning btn-large"><i class="ti-align-justify"> </i> Receipt </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <div class="row mb-3">
                            <div class="col-md-1"><b>Start Date</b></div>
                            <div class="col-md-2"><input type="date" name="from_date" class="form-control" id="from_date" value="<?= date('Y-m-d', strtotime(FIN_YEAR_STRT_DATE)) ?>" /></div>
                            <div class="col-md-1"><b>End Date</b></div>
                            <div class="col-md-2"><input type="date" name="to_date" value="<?= date('Y-m-d', strtotime(FIN_YEAR_END_DATE)) ?>" class="form-control" id="to_date"/></div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" id="date_filter_btn">Search</button>
                            </div>
                        </div>
                        <table id="payment_data" class="table table-hover table-striped w-100" style="margin-top:2px;" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-center">SN</th>
                                    <th class="">Transaction Date</th>
                                    <th class="">Transaction Category</th>
                                    <th class="">Payee/Receiver Name</th>
                                    <th class="">Description</th>
                                    <th class="">Transaction Mode</th>
                                    <th class="">Receipt Amount</th>
                                    <th class="">Paid Amount</th>
                                    <th class="">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<script type="text/javascript">
    $(document).ready(function () {
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });

        getTransctionList();

        $(document).on('click', '#date_filter_btn', function () {
            $('#payment_data').DataTable().destroy();
            getTransctionList();
        });

        $('body').on('click', '.deletePayment', function () {
            var id = $(this).data('id');
            swalWithBootstrapButtons.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this record!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value == true && id != '' && id != null) {
                    $.ajax({
                        type: 'POST',
                        url: '<?= base_url('admin/Cpayment/payment_delete') ?>',
                        dataType: 'json',
                        data: {id: id},
                        success: function (returnData) {
                            if (returnData.result == true) {
                                window.location.reload();
                            } else {
                                swalWithBootstrapButtons.fire(
                                        "Oops!",
                                        returnData.msg,
                                        "error"
                                        );
                            }
                            return false;
                        }
                    });
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    swalWithBootstrapButtons.fire(
                            "Cancelled",
                            "Your record is safe :)",
                            "success"
                            );
                } else {
                    swalWithBootstrapButtons.fire(
                            "Oops!",
                            "Something went wrong, Try again!",
                            "error"
                            );
                }
            });
        });
    });

    function getTransctionList() {
        var _payment_data = $('#payment_data').DataTable({
            dom: "<'row '<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'lf><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                    extend: 'pdfHtml5',
                    text: '<i class="fal fa-file-pdf "></i>',
                    titleAttr: 'Generate PDF',
                    className: 'btn-outline-danger btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'excelHtml5',
                    text: '<i class="fal fa-file-excel "></i>',
                    titleAttr: 'Generate Excel',
                    className: 'btn-outline-success btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'csvHtml5',
                    text: '<i class="fal fa-file-csv "></i>',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-primary btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    }
                }, {
                    extend: 'copyHtml5',
                    text: '<i class="fal fa-copy "></i>',
                    titleAttr: 'Copy to clipboard',
                    className: 'btn-outline-info btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'print',
                    text: '<i class="fal fa-print "></i>',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-dark btn-sm hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-dark-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }
            ],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            },
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": [0, 8],
                    "orderable": false
                }, {
                    "targets": [0],
                    "className": 'text-center'
                }],
            "ajax": {
                "url": "<?= base_url() ?>admin/Cpayment/get_payment_list",
                "type": "POST",
                "data": function (d) {
                    d.start_date = $('#from_date').val();
                    d.end_date = $('#to_date').val();
                }
            }

        });

        $("#payment_data thead input").on('keyup change', function () {
            _payment_data.column($(this).parent().index() + ':visible').search(this.value).draw();
        });
    }


</script>