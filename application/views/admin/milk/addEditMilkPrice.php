<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> <?= isset($milk_price_data->milk_price_id) && !empty($milk_price_data->milk_price_id) ? 'Edit' : 'Add' ?> Milk Price
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Milk">Milk Price</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Milk/addEditMilkPrice/' . (isset($milk_price_data->milk_price_id) && !empty($milk_price_data->milk_price_id) ? $milk_price_data->milk_price_id : ''), $arrayName = array('id' => 'addEditMilkPrice', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="ref_milk_id">Milk <span class="text-danger">*</span></label>
                                <select tabindex="7" class="select2 form-control" name="ref_milk_id" id="ref_milk_id" required="" <?= isset($milk_price_data->milk_price_id) && !empty($milk_price_data->milk_price_id) ? 'disabled=""' : '' ?>>
                                    <option></option>
                                    <?php
                                    if (isset($milk_data) && !empty($milk_data)) {
                                        foreach ($milk_data as $key1 => $value1) {
                                            ?>
                                            <option value="<?= $value1->milk_id ?>" <?= isset($milk_price_data->ref_milk_id) && !empty($milk_price_data->ref_milk_id) ? set_selected($milk_price_data->ref_milk_id, $value1->milk_id) : '' ?>><?= isset($value1->milk_name) && !empty($value1->milk_name) ? $value1->milk_name : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Milk Required
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="milk_price_date">Date <span class="text-danger">*</span></label>
                                <input tabindex="1" type="text" class="form-control" name="milk_price_date" id="milk_price_date" placeholder="Date" required value="<?= isset($milk_price_data->milk_price_date) && !empty($milk_price_data->milk_price_date) ? date('d-m-Y', strtotime($milk_price_data->milk_price_date)) : '' ?>" readonly="">
                                <div class="invalid-feedback">
                                    Date Required
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="milk_price">Price <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="milk_price" id="milk_price" placeholder="Price" required value="<?= isset($milk_price_data->milk_price) && !empty($milk_price_data->milk_price) ? $milk_price_data->milk_price : '' ?>">
                                <div class="invalid-feedback">
                                    Price Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var milk_price_id = '<?= isset($milk_price_data->milk_price_id) && !empty($milk_price_data->milk_price_id) ? $milk_price_data->milk_price_id : '' ?>';
    $(document).ready(function () {
        $('#milk_price_date').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            orientation: "bottom left",
            templates: controls,
            autoclose: true,
            onClose: function () {
                $(this).valid();
            }
        });

        $("#ref_milk_id").select2({
            placeholder: "Select milk",
            allowClear: true,
            width: '100%'
        });

        $('#addEditMilkPrice').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>