<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Profile
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Dashboard">Dashboard</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Dashboard/addEditProfile', $arrayName = array('id' => 'addEditProfile', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="user_name">Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="user_name" id="user_name" placeholder="Name" required value="<?= isset($user_data->user_name) && !empty($user_data->user_name) ? $user_data->user_name : '' ?>">
                                <div class="invalid-feedback">
                                    Name Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="old_password">Old Password</label>
                                <input tabindex="2" type="password" class="form-control" name="old_password" id="old_password" placeholder="Old Password" value="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="new_password">New Password</label>
                                <input tabindex="2" type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password" value="">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="confirm_new_password">Confirm New Password</label>
                                <input tabindex="2" type="password" class="form-control" name="confirm_new_password" id="confirm_new_password" placeholder="Confirm New Password" value="">
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditProfile').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>