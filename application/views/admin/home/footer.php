<div class="page-content-overlay" data-action="toggle" data-class="mobile-nav-on"></div> <!-- END Page Content -->
<!-- BEGIN Page Footer -->
<footer class="page-footer" role="contentinfo">
    <div class="d-flex align-items-center flex-1 text-muted">
        <span class="hidden-md-down fw-700"><?= '2021' . ' - ' . date('Y', strtotime('+1 year')) ?> © <?= COMPANY_NAME ?> powered by&nbsp;<a href='https://www.weborative.com' class='text-primary fw-500' title='weborative.com' target='_blank'>weborative.com</a></span>
    </div>
</footer>
<!-- END Page Footer -->
<!-- BEGIN Color profile -->
<!-- this area is hidden and will not be seen on screens or screen readers -->
<!-- we use this only for CSS color refernce for JS stuff -->
<p id="js-color-profile" class="d-none">
    <span class="color-primary-50"></span>
    <span class="color-primary-100"></span>
    <span class="color-primary-200"></span>
    <span class="color-primary-300"></span>
    <span class="color-primary-400"></span>
    <span class="color-primary-500"></span>
    <span class="color-primary-600"></span>
    <span class="color-primary-700"></span>
    <span class="color-primary-800"></span>
    <span class="color-primary-900"></span>
    <span class="color-info-50"></span>
    <span class="color-info-100"></span>
    <span class="color-info-200"></span>
    <span class="color-info-300"></span>
    <span class="color-info-400"></span>
    <span class="color-info-500"></span>
    <span class="color-info-600"></span>
    <span class="color-info-700"></span>
    <span class="color-info-800"></span>
    <span class="color-info-900"></span>
    <span class="color-danger-50"></span>
    <span class="color-danger-100"></span>
    <span class="color-danger-200"></span>
    <span class="color-danger-300"></span>
    <span class="color-danger-400"></span>
    <span class="color-danger-500"></span>
    <span class="color-danger-600"></span>
    <span class="color-danger-700"></span>
    <span class="color-danger-800"></span>
    <span class="color-danger-900"></span>
    <span class="color-warning-50"></span>
    <span class="color-warning-100"></span>
    <span class="color-warning-200"></span>
    <span class="color-warning-300"></span>
    <span class="color-warning-400"></span>
    <span class="color-warning-500"></span>
    <span class="color-warning-600"></span>
    <span class="color-warning-700"></span>
    <span class="color-warning-800"></span>
    <span class="color-warning-900"></span>
    <span class="color-success-50"></span>
    <span class="color-success-100"></span>
    <span class="color-success-200"></span>
    <span class="color-success-300"></span>
    <span class="color-success-400"></span>
    <span class="color-success-500"></span>
    <span class="color-success-600"></span>
    <span class="color-success-700"></span>
    <span class="color-success-800"></span>
    <span class="color-success-900"></span>
    <span class="color-fusion-50"></span>
    <span class="color-fusion-100"></span>
    <span class="color-fusion-200"></span>
    <span class="color-fusion-300"></span>
    <span class="color-fusion-400"></span>
    <span class="color-fusion-500"></span>
    <span class="color-fusion-600"></span>
    <span class="color-fusion-700"></span>
    <span class="color-fusion-800"></span>
    <span class="color-fusion-900"></span>
</p>
<!-- END Color profile -->
</div>
</div>
</div>
<!-- END Page Wrapper -->
<!-- BEGIN Quick Menu -->
<!-- to add more items, please make sure to change the variable '$menu-items: number;' in your _page-components-shortcut.scss -->
<nav class="shortcut-menu d-none d-sm-block">
    <a href="#" class="menu-item btn hover-effect-dot waves-effect" data-toggle="tooltip" data-placement="left" title="Scroll Top" data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
        <i class="fal fa-arrow-up"></i>
    </a>
</nav>
<!-- END Quick Menu -->
<style>
    .select2-container--open{
        z-index:9999999;
    }
</style>
<!--<audio id="myAlertAudio">
    <source src="<?= base_url('assets/admin/sound/noti.mp3') ?>" type="audio/mpeg">
</audio>-->

<script src="<?= base_url() ?>assets/admin/js/app.bundle.js"></script>
<!-- The order of scripts is irrelevant. Please check out the plugin pages for more details about these plugins below: -->
<script src="<?= base_url() ?>assets/admin/js/dependency/moment/moment.js"></script>
<script src="<?= base_url() ?>assets/admin/js/miscellaneous/fullcalendar/fullcalendar.bundle.js"></script>
<script src="<?= base_url() ?>assets/admin/js/statistics/sparkline/sparkline.bundle.js"></script>
<script src="<?= base_url() ?>assets/admin/js/statistics/easypiechart/easypiechart.bundle.js"></script>
<script src="<?= base_url() ?>assets/admin/js/statistics/flot/flot.bundle.js"></script>
<script src="<?= base_url() ?>assets/admin/js/miscellaneous/jqvmap/jqvmap.bundle.js"></script>

<script src="<?= base_url() ?>assets/admin/js/dependency/moment/moment-timezone-with-data-1970-2030.js"></script>
<script src="<?= base_url() ?>assets/admin/js/formplugins/select2/select2.bundle.js"></script>
<script src="<?= base_url() ?>assets/admin/js/formplugins/jquery-validate/jquery.validate.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/formplugins/jquery-validate/additional-methods.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/datagrid/datatables/datatables.bundle.js"></script>
<script src="<?= base_url() ?>assets/admin/js/datagrid/datatables/datatables.export.js"></script>
<script src="<?= base_url() ?>assets/admin/js/formplugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script src="<?= base_url() ?>assets/admin/js/notifications/toastr/toastr.js"></script>

<!-- CK Editor -->
<script src="<?php echo base_url() ?>assets/admin/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

<script type="text/javascript">
    $.fn.dataTable.ext.errMode = 'throw';
    
    Array.prototype.uniqueK = function () {
        var n = {}, r = [];
        for (var i = 0; i < this.length; i++) {
            if (!n[this[i]]) {
                n[this[i]] = true;
                r.push(this[i]);
            }
        }
        return r;
    };
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary mr-2",
            cancelButton: "btn btn-danger"
        },
        buttonsStyling: false
    });
    var today = new Date();
    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    };
    $(document).on('keydown', '.numbersonly', function (e) {
        var key = e.keyCode ? e.keyCode : e.which;
        if (!([8, 9, 13, 27, 46].indexOf(key) !== -1 ||
                (key == 65 && (e.ctrlKey || e.metaKey)) ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
                (key >= 96 && key <= 105) ||
                (key == 190 && e.shiftKey)
                )) {
            e.preventDefault();
        } else if (key == 190 && e.shiftKey) {
            e.preventDefault();
        } else {
            var str = $(this).val();
            if (str.includes(".") && (key === 190 || key === 110)) {
                e.preventDefault();
            }
        }
    });

    $(document).on('keydown', '.contactnumber', function (e) {
        var key = e.keyCode ? e.keyCode : e.which;
        if (!([8, 9, 13, 27, 46, 32, 107, 187].indexOf(key) !== -1 ||
                (key == 65 && (e.ctrlKey || e.metaKey)) ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
                (key >= 96 && key <= 105) ||
                (key == 190 && e.shiftKey)
                )) {
            e.preventDefault();
        } else if (key == 190 && e.shiftKey) {
            e.preventDefault();
        } else {
            var str = $(this).val();
            if (str.includes(".") && (key === 190 || key === 110)) {
                e.preventDefault();
            }
        }
    });

    $(document).on('keydown', '.price', function (e) {
        var key = e.keyCode ? e.keyCode : e.which;
        if (!([8, 9, 13, 27, 46, 190, 110].indexOf(key) !== -1 ||
                (key == 65 && (e.ctrlKey || e.metaKey)) ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
                (key >= 96 && key <= 105) ||
                (key == 190 && e.shiftKey)
                )) {
            e.preventDefault();
        } else if (key == 190 && e.shiftKey) {
            e.preventDefault();
        } else {
            var str = $(this).val();
            if (str.includes(".") && (key === 190 || key === 110)) {
                e.preventDefault();
            }
        }
    });

    $(document).on('blur', '.contact_number', function () {
        var strlenght = $(this).val().length;
        if ((strlenght > 10 || strlenght < 10) && strlenght > 0) {
            $(this).val('');
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Phone Number must be 10 digit!",
                    "error"
                    );
        }
    });

    $(document).on('keypress', '.textonly', function (e) {
        var keyCode = e.keyCode || e.which;
        var regex = /^[A-Za-z0-9 ]+$/;
        var isValid = regex.test(String.fromCharCode(keyCode));
        return isValid;
    });

    $(document).on('keypress', '.address', function (e) {
        var keyCode = e.keyCode || e.which;
        var regex = /^[A-Za-z0-9:,-./ ]+$/;
        var isValid = regex.test(String.fromCharCode(keyCode));
        return isValid;
    });

    $(document).on('keypress', '.decimalcheck', function (e) {

    });

    $(document).on('keypress', '.product_code', function (e) {
        var keyCode = e.keyCode || e.which;
        var regex = /^[A-Za-z0-9- ]+$/;
        var isValid = regex.test(String.fromCharCode(keyCode));
        return isValid;
    });

    $(document).on('keypress', '.email', function (e) {
        var keyCode = e.keyCode || e.which;
        var regex = /^[A-Za-z0-9@ ]+$/;
        var isValid = regex.test(String.fromCharCode(keyCode));
        return isValid;
    });

    /* Activate smart panels */
    $('#js-page-content').smartPanel();
    $('.select2').css({"width": "100%"}).select2({
        placeholder: $(this).data('placeholder') != '' && $(this).data('placeholder') != null ? $(this).data('placeholder') : '-- Select option --'
    });
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": 300,
        "hideDuration": 100,
        "timeOut": 5000,
        "extendedTimeOut": 1000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    $(document).on('keyup', '.integerchk', function (e) {
        var input = $(this).val();
        var ponto = input.split('.').length;
        var slash = input.split('-').length;
        if (ponto > 2)
            $(this).val(input.substr(0, (input.length) - 1));
        $(this).val(input.replace(/[^0-9]/, ''));
        if (slash > 2)
            $(this).val(input.substr(0, (input.length) - 1));
        if (ponto == 2)
            $(this).val(input.substr(0, (input.indexOf('.') + 5)));
        if (input == '.')
            $(this).val("");
    });

    $(document).on('click', '.delete', function (e) {
        e.preventDefault();
        var linkURL = this.href;
        warnBeforeRedirect(linkURL);
    });

    function warnBeforeRedirect(linkURL) {
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!"
        }).then(function (result) {
            if (result.value) {
                window.location.href = linkURL;
            }
        });
    }
    var table = $('#datatable').dataTable({
        responsive: true,
        select: true,
        dom: "<'row '<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'lf><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        buttons: [{
                extend: 'pdfHtml5',
                text: '<i class="fal fa-file-pdf "></i>',
                titleAttr: 'Generate PDF',
                className: 'btn-outline-danger btn-sm mr-1 hover-effect-dot',
                attr: {
                    "data-toggle": 'tooltip',
                    "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'
                },
                exportOptions: {
                    columns: ':not(.notexport)'
                },
                title: $('#datatable').data('title'),
                messageTop: $('#datatable').data('msgtop')
            }, {
                extend: 'excelHtml5',
                text: '<i class="fal fa-file-excel "></i>',
                titleAttr: 'Generate Excel',
                className: 'btn-outline-success btn-sm mr-1 hover-effect-dot',
                attr: {
                    "data-toggle": 'tooltip',
                    "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'
                },
                exportOptions: {
                    columns: ':not(.notexport)'
                },
                title: $('#datatable').data('title'),
                messageTop: $('#datatable').data('msgtop')
            }, {
                extend: 'csvHtml5',
                text: '<i class="fal fa-file-csv "></i>',
                titleAttr: 'Generate CSV',
                className: 'btn-outline-primary btn-sm mr-1 hover-effect-dot',
                attr: {
                    "data-toggle": 'tooltip',
                    "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'
                },
                exportOptions: {
                    columns: ':not(.notexport)'
                }
            }, {
                extend: 'copyHtml5',
                text: '<i class="fal fa-copy "></i>',
                titleAttr: 'Copy to clipboard',
                className: 'btn-outline-info btn-sm mr-1 hover-effect-dot',
                attr: {
                    "data-toggle": 'tooltip',
                    "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'
                },
                exportOptions: {
                    columns: ':not(.notexport)'
                },
                title: $('#datatable').data('title'),
                messageTop: $('#datatable').data('msgtop')
            }, {
                extend: 'print',
                text: '<i class="fal fa-print "></i>',
                titleAttr: 'Print Table',
                className: 'btn-outline-dark btn-sm hover-effect-dot',
                attr: {
                    "data-toggle": 'tooltip',
                    "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-dark-500"></div></div>'
                },
                exportOptions: {
                    columns: ':not(.notexport)'
                },
                title: $('#datatable').data('title'),
                messageTop: $('#datatable').data('msgtop')
            }
        ],
        drawCallback: function (settings) {
            $('[data-toggle="tooltip"]').tooltip();
        },
        columnDefs: [{orderable: false, targets: "no-sort"}],
        "language": {
            "lengthMenu": "_MENU_"
        }
    });
    table.on('responsive-display.dt', function (e, datatable, row, showHide, update) {
        console.log('Details for row ' + row.index() + ' ' + (showHide ? 'shown' : 'hidden'));
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(document).ready(function () {
        $(".dynamic_tag_select2").select2({
            tags: true,
            tokenSeparators: [',', ' '],
            width: 'resolve'
        });
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom left",
            templates: controls,
            autoclose: true,
//            endDate: "nextday",
            maxDate: today
        });

        $('.datetime').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            orientation: "bottom left",
            templates: controls,
            autoclose: true,
            endDate: "nextday",
            maxDate: today
        });

<?= $add_message; ?>
        console.log('<?= $add_message; ?>');
    });
</script>

</body>
</html>