<?php $profile_photo = $this->session->userdata(SITE_NAME . '_admin')['profile_photo']; ?>
<aside class="page-sidebar">
    <div class="page-logo">
        <a href="javascript:void(0);" class="page-logo-link press-scale-down d-flex align-items-center position-relative">
            <span class="page-logo-text mr-1 text-center"><?= COMPANY_NAME ?></span>
            <span class="position-absolute text-white opacity-50 small pos-top pos-right mr-2 mt-n2"></span>
            <!--<i class="fal fa-angle-down d-inline-block ml-1 fs-lg color-primary-300"></i>-->
        </a>
    </div>
    <!-- BEGIN PRIMARY NAVIGATION -->
    <nav id="js-primary-nav" class="primary-nav" role="navigation">
        <div class="nav-filter">
            <div class="position-relative">
                <input type="text" id="nav_filter_input" placeholder="Filter menu" class="form-control" tabindex="0">
                <a href="#" onclick="return false;" class="btn-primary btn-search-close js-waves-off" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar">
                    <i class="fal fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="info-card">
            <img src="<?= base_url() . (isset($profile_photo) && !empty($profile_photo) && file_exists($profile_photo) ? $profile_photo : 'assets/admin/img/demo/avatars/avatar-admin-new.png') ?>" class="profile-image rounded-circle" alt="<?= $this->session->userdata(SITE_NAME . '_admin')['user_name'] ?>">
            <div class="info-card-text">
                <a href="#" class="d-flex align-items-center text-white">
                    <span class="text-truncate text-truncate-sm d-inline-block">
                        <?= $this->session->userdata(SITE_NAME . '_admin')['user_name'] ?>
                    </span>
                </a>
                <span class="d-inline-block text-truncate text-truncate-sm"><?= $this->session->userdata(SITE_NAME . '_admin')['user_name'] ?></span>
            </div>
            <img src="<?= base_url() ?>assets/admin/img/card-backgrounds/cover-2-lg.png" class="cover" alt="cover">
            <!--            <a href="#" onclick="return false;" class="pull-trigger-btn" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar" data-focus="nav_filter_input">
                            <i class="fal fa-angle-down"></i>
                        </a>-->
        </div>
        <ul id="js-nav-menu" class="nav-menu">
            <?php
            $side_menu = $this->session->userdata(SITE_NAME . '_admin')['side_menu'];
            if (isset($side_menu) && !empty($side_menu)) {
                foreach ($side_menu as $key => $value) {
                    if ($value->ref_menu_id == NULL) {
                        $side_menu_class = ($this->page_id == $value->menu_constant ? 'active open' : '');
                        ?>
                        <li class="<?= $side_menu_class ?>">
                            <a href="<?= isset($value->menu_url) && !empty($value->menu_url) && $value->menu_url != '#' ? base_url($value->menu_url) : 'javascript:void(0);' ?>" title="" data-filter-tags="<?= $value->menu_name ?>"><i class="fal <?= $value->menu_icon ?>"></i><span class="nav-link-text"><?= $value->menu_name ?></span></a>
                            <?php
                            if ($value->menu_url == '#') {
                                ?>
                                <ul>
                                    <?php
                                    $menu_id = $value->menu_id;
                                    foreach ($side_menu as $k => $v) {
                                        if ($v->ref_menu_id == $menu_id) {
                                                ?>
                                                <li class="<?= $this->menu_id == $v->menu_constant ? 'active' : '' ?>"><a href="<?= base_url($v->menu_url) ?>"><span class="nav-link-text"><?= $v->menu_name ?></span></a></li>
                                                <?php
                                            
                                        }
                                    }
                                    ?>
                                </ul>
                            <?php } ?>
                        </li>  
                        <?php
                    }
                }
            }
            ?>
        </ul>
        <div class="filter-message js-filter-message bg-success-600"></div>
    </nav>
    <!-- END PRIMARY NAVIGATION -->
    <!-- NAV FOOTER -->
    <div class="nav-footer shadow-top">
        <a href="#" onclick="return false;" data-action="toggle" data-class="nav-function-minify" class="hidden-md-down">
            <i class="ni ni-chevron-right"></i>
            <i class="ni ni-chevron-right"></i>
        </a>
        <ul class="list-table m-auto nav-footer-buttons">
            <li>
                <a href="tel:+919737559737" data-toggle="tooltip" data-placement="top" title="Pilot Service">
                    <i class="fal fa-comments"></i>
                </a>
            </li>
            <li>
                <a href="tel:+919737559737" data-toggle="tooltip" data-placement="top" title="Support Call">
                    <i class="fal fa-life-ring"></i>
                </a>
            </li>
            <li>
                <a href="tel:+919737559737" data-toggle="tooltip" data-placement="top" title="Tech Person call">
                    <i class="fal fa-phone"></i>
                </a>
            </li>
        </ul>
    </div>
</aside>
<div class="page-content-wrapper">
    <!-- BEGIN Page Header -->
    <header class="page-header" role="banner">
        <!-- we need this logo when user switches to nav-function-top -->
        <div class="page-logo">
            <a href="<?= base_url() ?>" class="page-logo-link press-scale-down d-flex align-items-center position-relative">
                <img src="<?= base_url() ?>assets/admin/img/logo.png" alt="<?= COMPANY_NAME ?>" aria-roledescription="logo">
                <span class="page-logo-text mr-1"><?= COMPANY_NAME ?></span>
                <span class="position-absolute text-white opacity-50 small pos-top pos-right mr-2 mt-n2"></span>
                <i class="fal fa-angle-down d-inline-block ml-1 fs-lg color-primary-300"></i>
            </a>
        </div>
        <!-- DOC: nav menu layout change shortcut -->
        <div class="hidden-md-down dropdown-icon-menu position-relative">
            <a href="#" class="header-btn btn js-waves-off" data-action="toggle" data-class="nav-function-hidden" title="Hide Navigation">
                <i class="ni ni-menu"></i>
            </a>
            <ul>
                <li>
                    <a href="#" class="btn js-waves-off" data-action="toggle" data-class="nav-function-minify" title="Minify Navigation">
                        <i class="ni ni-minify-nav"></i>
                    </a>
                </li>
                <li>
                    <a href="#" class="btn js-waves-off" data-action="toggle" data-class="nav-function-fixed" title="Lock Navigation">
                        <i class="ni ni-lock-nav"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- DOC: mobile button appears during mobile width -->
        <div class="hidden-lg-up">
            <a href="#" class="header-btn btn press-scale-down" data-action="toggle" data-class="mobile-nav-on">
                <i class="ni ni-menu"></i>
            </a>
        </div>
        <div class="ml-auto d-flex">
            <!-- app shortcuts -->
            <div>
                <a href="#" class="header-icon" data-toggle="dropdown" title="My Apps">
                    <i class="fal fa-cube"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-animated w-auto h-auto">
                    <div class="dropdown-header bg-trans-gradient d-flex justify-content-center align-items-center rounded-top">
                        <h4 class="m-0 text-center color-white">
                            Quick Shortcut
                            <small class="mb-0 opacity-80">User Applications</small>
                        </h4>
                    </div>
                    <div class="custom-scroll h-100">
                        <ul class="app-list">
                            <li>
                                <a href="<?= base_url('admin/Sabhasad/addEditPaymentCredit') ?>" class="app-list-item hover-white">
                                    <span class="icon-stack">
                                        <i class="base-7 icon-stack-3x color-info-500"></i>
                                        <i class="base-7 icon-stack-2x color-info-700"></i>
                                        <i class="ni ni-graph icon-stack-1x text-white"></i>
                                    </span>
                                    <span class="app-list-name">
                                        Add Credit
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('admin/Sabhasad/addEditPayment') ?>" class="app-list-item hover-white">
                                    <span class="icon-stack">
                                        <i class="base-18 icon-stack-3x color-info-700"></i>
                                        <span class="position-absolute pos-top pos-left pos-right color-white fs-md mt-2 fw-400"><?= date('d') ?></span>
                                    </span>
                                    <span class="app-list-name">
                                        Add Debit
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('admin/Sabhasad/addEditSabhasad') ?>" class="app-list-item hover-white">
                                    <span class="icon-stack">
                                        <i class="base-2 icon-stack-3x color-primary-400"></i>
                                        <i class="base-10 text-white icon-stack-1x"></i>
                                        <i class="ni md-profile color-primary-800 icon-stack-2x"></i>
                                    </span>
                                    <span class="app-list-name">
                                        Add Sabhasad
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- app message -->
            <!-- app user menu -->
            <div>
                <a href="#" data-toggle="dropdown" title="<?= $this->session->userdata(SITE_NAME . '_admin')['user_email'] ?>" class="header-icon d-flex align-items-center justify-content-center ml-2">
                    <img src="<?= base_url() . (isset($profile_photo) && !empty($profile_photo) && file_exists($profile_photo) ? $profile_photo : 'assets/admin/img/demo/avatars/avatar-admin-new.png') ?>" class="rounded-circle profile-image" alt="<?= $this->session->userdata(SITE_NAME . '_admin')['user_name'] ?>">
                </a>
                <div class="dropdown-menu dropdown-menu-animated dropdown-lg">
                    <div class="dropdown-header bg-trans-gradient d-flex flex-row py-4 rounded-top">
                        <div class="d-flex flex-row align-items-center mt-1 mb-1 color-white">
                            <span class="mr-2">
                                <img src="<?= base_url() . (isset($profile_photo) && !empty($profile_photo) && file_exists($profile_photo) ? $profile_photo : 'assets/admin/img/demo/avatars/avatar-admin-new.png') ?>" class="rounded-circle profile-image" alt="<?= $this->session->userdata(SITE_NAME . '_admin')['user_name'] ?>">
                            </span>
                            <div class="info-card-text">
                                <div class="fs-lg text-truncate text-truncate-lg"><?= $this->session->userdata(SITE_NAME . '_admin')['user_name'] ?></div>
                                <span class="text-truncate text-truncate-md opacity-80"><?= $this->session->userdata(SITE_NAME . '_admin')['user_email'] ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-divider m-0"></div>
                    <div class="dropdown-divider m-0"></div>
                    <a href="<?= base_url() ?>admin/Dashboard/addEditProfile" class="dropdown-item">
                        <span data-i18n="drpdwn.settings">Reset Password</span>
                    </a>
                    <a href="#" class="dropdown-item" data-action="app-fullscreen">
                        <span data-i18n="drpdwn.fullscreen">Fullscreen</span>
                        <i class="float-right text-muted fw-n">F11</i>
                    </a>
                    <div class="dropdown-divider m-0"></div>
                    <a class="dropdown-item fw-500 pt-3 pb-3" href="<?= base_url() ?>admin/Auth/logout">
                        <span data-i18n="drpdwn.page-logout">Logout</span>
                        <span class="float-right fw-n">&commat;<?= $this->session->userdata(SITE_NAME . '_admin')['user_name'] ?></span>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <!-- END Page Header -->