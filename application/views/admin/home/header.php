<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>
            <?= isset($this->page_title) && !empty($this->page_title) ? $this->page_title : '' ?>
        </title>
        <meta name="description" content="<?= COMPANY_NAME ?>">
        <meta name="author" content="Weborative IT Consultancy LLP">
        <meta property="og:type" content="website">
        <meta property="og:title" content="<?= COMPANY_NAME ?>" />
        <meta property="og:description" content="" />
        <!--<meta property="og:image" content="http://fdmaster.foodmohalla.in/assets/admin/images/logo_640.png" />-->
        <meta property="og:site_name" content="<?= COMPANY_NAME ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>favicon-16x16.png">
        <!--        <link rel="manifest" href="/site.webmanifest">
                <link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">-->

        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/miscellaneous/reactions/reactions.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/miscellaneous/fullcalendar/fullcalendar.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/miscellaneous/jqvmap/jqvmap.bundle.css">

        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/fa-solid.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/formplugins/select2/select2.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/datagrid/datatables/datatables.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/formplugins/bootstrap-datepicker/bootstrap-datepicker.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/notifications/toastr/toastr.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/notifications/sweetalert2/sweetalert2.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/formplugins/summernote/summernote.css">
        <style>
            .subheader {
                margin-bottom: 0.5rem !important;
            }
            .page-content {
                padding: 1rem 1rem 3rem 1rem;
            }
            .font_papyrus{
                font-family: 'Papyrus';
            }
            .error{
                color: #fd3995;
            }
        </style>
        <script src="<?= base_url() ?>assets/admin/js/vendors.bundle.js"></script>
        <script src="<?= base_url() ?>assets/admin/js/notifications/sweetalert2/sweetalert2.bundle.js"></script>
    </head>
    <body class="mod-bg-1 mod-nav-link header-function-fixed nav-function-fixed">
        <!-- DOC: script to save and load page settings -->
        <script>
            /**
             *	This script should be placed right after the body tag for fast execution 
             *	Note: the script is written in pure javascript and does not depend on thirdparty library
             **/
            'use strict';

            var classHolder = document.getElementsByTagName("BODY")[0],
                    /** 
                     * Load from localstorage
                     **/
                    themeSettings = (localStorage.getItem('posthemeSettings')) ? JSON.parse(localStorage.getItem('posthemeSettings')) :
                    {},
                    themeURL = themeSettings.themeURL || '',
                    themeOptions = themeSettings.themeOptions || '';
            /** 
             * Load theme options
             **/
            if (themeSettings.themeOptions) {
                classHolder.className = themeSettings.themeOptions;
                console.log("%c✔ Theme settings loaded", "color: #148f32");
            } else {
                console.log("Heads up! Theme settings is empty or does not exist, loading default settings...");
            }
            if (themeSettings.themeURL && !document.getElementById('mytheme')) {
                var cssfile = document.createElement('link');
                cssfile.id = 'mytheme';
                cssfile.rel = 'stylesheet';
                cssfile.href = themeURL;
                document.getElementsByTagName('head')[0].appendChild(cssfile);
            }
            /** 
             * Save to localstorage 
             **/
            var saveSettings = function () {
                themeSettings.themeOptions = String(classHolder.className).split(/[^\w-]+/).filter(function (item) {
                    return /^(nav|header|mod|display)-/i.test(item);
                }).join(' ');
                if (document.getElementById('mytheme')) {
                    themeSettings.themeURL = document.getElementById('mytheme').getAttribute("href");
                }
                ;
                localStorage.setItem('posthemeSettings', JSON.stringify(themeSettings));
            }
            /** 
             * Reset settings
             **/
            var resetSettings = function () {
                localStorage.setItem("posthemeSettings", "");
            }

        </script>
        <!-- BEGIN Page Wrapper -->
        <div class="page-wrapper">
            <div class="page-inner">