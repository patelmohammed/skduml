<style>
    .bg-info-gradient{
        background-image:linear-gradient(100deg, rgba(9, 96, 165, 0.7), transparent);
    }
    .custom_redirect{
        cursor: pointer;
    }
</style>
<main id="js-page-content" role="main" class="page-content">
    <ol class="breadcrumb page-breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">SKDUML</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
        <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
    </ol>
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-chart-area'></i> Marketing <span class='fw-300'>Dashboard</span>
        </h1>
        <div class="d-flex mr-0">
            <div class="mr-2">
                <span class="peity-donut" data-peity="{ &quot;fill&quot;: [&quot;#2196F3&quot;, &quot;#9acffa&quot;],  &quot;innerRadius&quot;: 14, &quot;radius&quot;: 20 }">3/10</span>
            </div>
            <div>
                <label class="fs-sm mb-0 mt-2 mt-md-0">Milk Price</label>
                <h4 class="font-weight-bold mb-0"><?= isset($count_data->milk_price) && !empty($count_data->milk_price) ? $count_data->milk_price : 0 ?></h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-xl-3 custom_redirect redirect_sabhasad">
            <div class="p-3 bg-primary-300 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($count_data->sabhasad_count) && !empty($count_data->sabhasad_count) ? $count_data->sabhasad_count : 0 ?>
                        <small class="m-0 l-h-n">Total Sabhasad</small>
                    </h3>
                </div>
                <i class="fal fa-user position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="p-3 bg-warning-400 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($count_data->milk_sold_count) && !empty($count_data->milk_sold_count) ? $count_data->milk_sold_count : 0 ?>
                        <small class="m-0 l-h-n">Total MILK sold Ltrs</small>
                    </h3>
                </div>
                <i class="fal fa-gem position-absolute pos-right pos-bottom opacity-15  mb-n1 mr-n4" style="font-size: 6rem;"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 custom_redirect redirect_wallet_credit">
            <div class="p-3 bg-success-200 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($count_data->total_credit_amount) && !empty($count_data->total_credit_amount) ? $count_data->total_credit_amount : 0 ?>
                        <small class="m-0 l-h-n">Total Sabhasad Credit Amount</small>
                    </h3>
                </div>
                <i class="fal fa-lightbulb position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 custom_redirect redirect_wallet_debit">
            <div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?= isset($count_data->total_debit_amount) && !empty($count_data->total_debit_amount) ? $count_data->total_debit_amount : 0 ?>
                        <small class="m-0 l-h-n">Total Sabhasad Debit Amount </small>
                    </h3>
                </div>
                <i class="fal fa-globe position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        Marketing
                    </h2>
                </div>
                <div class="panel-container show">
                    <div class="panel-content bg-subtlelight-fade">
                        <div id="flot-toggles" class="w-100 mt-4" style="height: 300px"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div id="panel-4" class="panel">
                <div class="panel-hdr">
                    <h2>
                        Sabhasad <span class="fw-300"><i>Under 100</i></span>
                    </h2>
                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                            <thead class="bg-warning-200">
                                <tr>
                                    <th>Sabhasad Name</th>
                                    <th>Sabhasad Code</th>
                                    <th>Sabhasad Contact</th>
                                    <th>Sabhasad Address</th>
                                    <th>Credit</th>
                                    <th>Debit</th>
                                    <th>Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($less_balance_sabhasad) && !empty($less_balance_sabhasad)) {
                                    foreach ($less_balance_sabhasad as $key => $value) {
                                        ?>
                                        <tr>
                                            <td><?= isset($value->sabhasad_name) && !empty($value->sabhasad_name) ? $value->sabhasad_name : '' ?></td>
                                            <td><?= isset($value->sabhasad_code) && !empty($value->sabhasad_code) ? $value->sabhasad_code : '' ?></td>
                                            <td><?= isset($value->sabhasad_contact) && !empty($value->sabhasad_contact) ? $value->sabhasad_contact : '' ?></td>
                                            <td><?= isset($value->sabhasad_address) && !empty($value->sabhasad_address) ? $value->sabhasad_address : '' ?></td>
                                            <td><?= isset($value->total_credit) && !empty($value->total_credit) ? $value->total_credit : '' ?></td>
                                            <td><?= isset($value->total_debit) && !empty($value->total_debit) ? $value->total_debit : '' ?></td>
                                            <td><?= number_format(($value->total_credit - $value->total_debit), 2) ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Sabhasad Name</th>
                                    <th>Sabhasad Code</th>
                                    <th>Sabhasad Contact</th>
                                    <th>Sabhasad Address</th>
                                    <th>Credit</th>
                                    <th>Debit</th>
                                    <th>Balance</th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- datatable end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="<?= base_url() ?>assets/admin/js/statistics/peity/peity.bundle.js"></script>
<script src="<?= base_url() ?>assets/admin/js/statistics/flot/flot.bundle.js"></script>
<script src="<?= base_url() ?>assets/admin/js/statistics/easypiechart/easypiechart.bundle.js"></script>

<script>

    $(document).ready(function () {
        selectMonth(12);
        /* init datatables */
        $('#dt-basic-example').dataTable({
            responsive: true,
            dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    extend: 'csvHtml5',
                    text: 'CSV',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-default'
                },
                {
                    extend: 'copyHtml5',
                    text: 'Copy',
                    titleAttr: 'Copy to clipboard',
                    className: 'btn-outline-default'
                },
                {
                    extend: 'print',
                    text: '<i class="fal fa-print"></i>',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-default'
                }
            ]
        });

        /* flot toggle example */
//        var flot_toggle = function () {
//            var data = [{
//                    label: "Target Profit",
//                    data: dataTargetProfit,
//                    color: color.info._400,
//                    bars: {
//                        show: true,
//                        align: "center",
//                        barWidth: 30 * 30 * 60 * 1000 * 80,
//                        lineWidth: 0,
//                        /*fillColor: {
//                         colors: [color.primary._500, color.primary._900]
//                         },*/
//                        fillColor: {
//                            colors: [{
//                                    opacity: 0.9
//                                }, {
//                                    opacity: 0.1
//                                }]
//                        }
//                    },
//                    highlightColor: 'rgba(255,255,255,0.3)',
//                    shadowSize: 0
//                }]
//
//            var options = {
//                grid: {
//                    hoverable: true,
//                    clickable: true,
//                    tickColor: '#f2f2f2',
//                    borderWidth: 1,
//                    borderColor: '#f2f2f2'
//                },
//                tooltip: true,
//                tooltipOpts: {
//                    cssClass: 'tooltip-inner',
//                    defaultTheme: false
//                },
//                xaxis: {
//                    mode: "time",
//                },
//                yaxes: {
//                    tickFormatter: function (val, axis)
//                    {
//                        return "$" + val;
//                    },
//                    max: 1200
//                }
//
//            };
//            var d = [];
//            d.push(data[0]);
//            $.plot($("#flot-toggles"), d, options);
//        }
//        flot_toggle();
        /* flot toggle example -- end*/
    });

    $('.redirect_sabhasad').on('click', function () {
        window.location.href = '<?= base_url('admin/Sabhasad') ?>';
    });
    $('.redirect_wallet_credit').on('click', function () {
        window.location.href = '<?= base_url('admin/Sabhasad/paymentCredit') ?>';
    });
    $('.redirect_wallet_debit').on('click', function () {
        window.location.href = '<?= base_url('admin/Sabhasad/payment') ?>';
    });

    function  selectMonth(value) {
        $.ajax({
            url: '<?= base_url() ?>admin/Dashboard/sale_report_ajax_get',
            type: 'get',
            datatype: 'json',
            data: {months: value},
            success: function (response) {
                var json = response;
                $.plot("#flot-toggles", [json.data], {
                    series: {
                        bars: {
                            show: true,
                            barWidth: 0.6,
                            align: "center",
                            color: '#39a1f4',
                            fillColor: {
                                colors: [{
                                        opacity: 0.9
                                    },
                                    {
                                        opacity: 0.1
                                    }]
                            }
                        },
                        highlightColor: 'rgba(255,255,255,0.3)',
                        shadowSize: 0
                    },
                    xaxis: {
                        mode: "categories",
                        showTicks: false,
                        gridLines: false
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: '#f2f2f2',
                        borderWidth: 1,
                        borderColor: '#f2f2f2'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        cssClass: 'tooltip-inner',
                        defaultTheme: false,
                        content: getTooltip,
                    },
                    colors: ["#39a1f4"]
                });
                function getTooltip(label, x, y) {
                    return "Your sales for " + json.xlabel[x] + " was ₹" + y;
                }
            }
        });
    }
</script>