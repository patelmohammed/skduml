<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Admin User
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Dashboard/user">Admin User</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Dashboard/addEditUser/' . $encrypted_id, $arrayName = array('id' => 'addEditUser', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="user_name">User Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="user_name" id="user_name" placeholder="User Name" required value="<?= isset($user_data->user_name) && !empty($user_data->user_name) ? $user_data->user_name : '' ?>">
                                <div class="invalid-feedback">
                                    User Name Required / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="user_email">User Email <span class="text-danger">*</span></label>
                                <input tabindex="2" type="email" class="form-control" name="user_email" id="user_email" placeholder="User Email" required value="<?= isset($user_data->user_email) && !empty($user_data->user_email) ? $user_data->user_email : '' ?>">
                                <div class="invalid-feedback">
                                    User Email Required / Already Exist
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="password">Password <span class="text-danger">*</span></label>
                                <input tabindex="2" type="password" class="form-control" name="password" id="password" placeholder="Password">
                                <div class="invalid-feedback">
                                    Password Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="confirm_password">Confirm Password <span class="text-danger">*</span></label>
                                <input tabindex="2" type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                                <div class="invalid-feedback">
                                    Confirm Password Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="mobile">Contact Number <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control contactnumber" name="mobile" id="mobile" placeholder="Contact Number" required value="<?= isset($user_data->mobile) && !empty($user_data->mobile) ? $user_data->mobile : '' ?>">
                                <div class="invalid-feedback">
                                    Contact Number Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="admin_code">Admin Code <span class="text-danger">*</span></label>
                                <input tabindex="1" type="text" class="form-control contactnumber" name="admin_code" id="admin_code" placeholder="Admin Code" required value="<?= isset($user_data->admin_code) && !empty($user_data->admin_code) ? $user_data->admin_code : '' ?>">
                                <div class="invalid-feedback">
                                    Admin Code Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="admin_pin">Pin <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input tabindex="5" type="text" class="form-control contactnumber" name="admin_pin" id="admin_pin" placeholder="Pin" maxlength="4" <?= isset($user_data->admin_pin) && !empty($user_data->admin_pin) ? '' : 'required' ?>>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" onclick="generatePassword()">Generate</button>
                                    </div>
                                </div>
                                <div class="invalid-feedback">
                                    Pin Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="confirm_admin_pin">Confirm Pin <span class="text-danger">*</span></label>
                                <input tabindex="6" type="text" class="form-control contactnumber" name="confirm_admin_pin" id="confirm_admin_pin" placeholder="Confirm Pin" maxlength="4" <?= isset($user_data->admin_pin) && !empty($user_data->admin_pin) ? '' : 'required' ?>>
                                <div class="invalid-feedback">
                                    Confirm Pin Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="address">Address <span class="text-danger">*</span></label>
                                <textarea class="form-control address" name="address" id="address" required rows="5"><?= isset($user_data->address) && !empty($user_data->address) ? $user_data->address : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Address Required
                                </div>
                            </div>
                        </div>
                        <hr style="margin: 0px;margin-bottom: 0px;padding: 0px;margin-bottom: 6px;">
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="checkall_side_menu">Side Menu Access</label>
                                <div class="frame-wrap">
                                    <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="checkall_side_menu">
                                        <label class="custom-control-label" for="checkall_side_menu">Select All</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Menu</th>
                                            <th>Full Access</th>
                                            <th>View</th>
                                            <th>Add</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($side_menu)) {
                                            foreach ($side_menu as $key => $value) {
                                                if ($value->menu_url == '#') {
                                                    ?>
                                                    <tr>
                                                        <td colspan="6"><?= $value->menu_name ?></td>
                                                    </tr>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td><?= $value->menu_name ?></td>
                                                        <td>
                                                            <div class="frame-wrap">
                                                                <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                    <input type="checkbox" class="custom-control-input check_full_access check_all" id="full_access_<?= $value->menu_id ?>" name="full_access[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>" <?= isset($value->full_access) && $value->full_access == 1 ? 'checked=""' : '' ?>>
                                                                    <label class="custom-control-label" for="full_access_<?= $value->menu_id ?>"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="frame-wrap">
                                                                <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                    <input type="checkbox" class="custom-control-input check_child" id="view_<?= $value->menu_id ?>" name="view[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>" <?= isset($value->view_right) && $value->view_right == 1 ? 'checked=""' : '' ?>>
                                                                    <label class="custom-control-label" for="view_<?= $value->menu_id ?>"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="frame-wrap">
                                                                <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                    <input type="checkbox" class="custom-control-input check_child" id="add_<?= $value->menu_id ?>" name="add[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>" <?= isset($value->add_right) && $value->add_right == 1 ? 'checked=""' : '' ?>>
                                                                    <label class="custom-control-label" for="add_<?= $value->menu_id ?>"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="frame-wrap">
                                                                <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                    <input type="checkbox" class="custom-control-input check_child" id="edit_<?= $value->menu_id ?>" name="edit[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>" <?= isset($value->edit_right) && $value->edit_right == 1 ? 'checked="checked"' : '' ?>>
                                                                    <label class="custom-control-label" for="edit_<?= $value->menu_id ?>"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="frame-wrap">
                                                                <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                    <input type="checkbox" class="custom-control-input check_child" id="delete_<?= $value->menu_id ?>" name="delete[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>" <?= isset($value->delete_right) && $value->delete_right == 1 ? 'checked="checked"' : '' ?>>
                                                                    <label class="custom-control-label" for="delete_<?= $value->menu_id ?>"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                if (!empty($value->sub_menu)) {
                                                    $data['subMenuData'] = $value->sub_menu;
                                                    echo $this->load->view('admin/user/edit_sub_menu_rights', $data, true);
                                                }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        var id = '<?= isset($user_data->id) && !empty($user_data->id) ? $user_data->id : '' ?>';
        $('#addEditUser').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                user_name: {
                    remote: {
                        url: "<?= base_url('/admin/Dashboard/checkUserName/') ?>" + id,
                        type: "get"
                    }
                },
                user_email: {
                    remote: {
                        url: "<?= base_url('/admin/Dashboard/checkUserEmail/') ?>" + id,
                        type: "get"
                    }
                }
            },
            messages: {
                user_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                user_email: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });

        $(document).on('change', '.check_full_access', function () {
            var i = $(this).data('id');
            if ($(this).is(':checked')) {
                $("#view_" + i).prop("checked", true);
                $("#add_" + i).prop("checked", true);
                $("#edit_" + i).prop("checked", true);
                $("#delete_" + i).prop("checked", true);
            } else {
                $("#view_" + i).prop("checked", false);
                $("#add_" + i).prop("checked", false);
                $("#edit_" + i).prop("checked", false);
                $("#delete_" + i).prop("checked", false);
            }
        });

        $(document).on('change', '#checkall_side_menu', function () {
            if ($(this).is(':checked')) {
                $(".check_all").prop("checked", true);
                $(".check_child").prop("checked", true);
            } else {
                $(".check_all").prop("checked", false);
                $(".check_child").prop("checked", false);
            }
        });

        $(document).on('change', '.check_child', function () {
            var i = $(this).data('id');
            if ($("#view_" + i).is(':checked') && $("#add_" + i).is(':checked') && $("#edit_" + i).is(':checked') && $("#delete_" + i).is(':checked')) {
                $("#full_access_" + i).prop("checked", true);
            } else {
                $("#full_access_" + i).prop("checked", false);
            }
        });
    });

    function generatePassword() {
        var length = 4,
                charset = "0123456789",
                retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        $('#admin_pin').val(retVal);
    }
</script>