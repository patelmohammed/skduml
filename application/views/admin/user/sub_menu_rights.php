<?php
if (!empty($subMenuData)) {
    foreach ($subMenuData as $key1 => $value1) {
        if ($value1->menu_url == '#') {
            ?>
            <tr>
                <td>&emsp;&emsp;<b><?= $value1->menu_name ?></b></td>
            </tr>
            <?php
        } else {
            ?>
            <tr>
                <td>&emsp;&emsp;<?= $value1->menu_name ?></td>
                <td>
                    <div class="frame-wrap">
                        <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                            <input type="checkbox" class="custom-control-input check_full_access check_all" id="full_access_<?= $value1->menu_id ?>" name="full_access[<?= $value1->menu_id ?>]" data-id="<?= $value1->menu_id ?>">
                            <label class="custom-control-label" for="full_access_<?= $value1->menu_id ?>"></label>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="frame-wrap">
                        <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                            <input type="checkbox" class="custom-control-input check_child" id="view_<?= $value1->menu_id ?>" name="view[<?= $value1->menu_id ?>]" data-id="<?= $value1->menu_id ?>">
                            <label class="custom-control-label" for="view_<?= $value1->menu_id ?>"></label>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="frame-wrap">
                        <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                            <input type="checkbox" class="custom-control-input check_child" id="add_<?= $value1->menu_id ?>" name="add[<?= $value1->menu_id ?>]" data-id="<?= $value1->menu_id ?>">
                            <label class="custom-control-label" for="add_<?= $value1->menu_id ?>"></label>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="frame-wrap">
                        <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                            <input type="checkbox" class="custom-control-input check_child" id="edit_<?= $value1->menu_id ?>" name="edit[<?= $value1->menu_id ?>]" data-id="<?= $value1->menu_id ?>">
                            <label class="custom-control-label" for="edit_<?= $value1->menu_id ?>"></label>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="frame-wrap">
                        <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                            <input type="checkbox" class="custom-control-input check_child" id="delete_<?= $value1->menu_id ?>" name="delete[<?= $value1->menu_id ?>]" data-id="<?= $value1->menu_id ?>">
                            <label class="custom-control-label" for="delete_<?= $value1->menu_id ?>"></label>
                        </div>
                    </div>
                </td>
            </tr>
            <?php
        }
        if (!empty($value1->sub_menu)) {
            $data['subMenuData'] = $value1->sub_menu;
            echo $this->load->view('user/sub_menu_rights', $data, true);
        }
    }
}
?>