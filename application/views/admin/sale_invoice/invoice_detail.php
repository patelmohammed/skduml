<?php
$CI = & get_instance();
$CI->load->model('Web_settings');
$Web_settings = $CI->Web_settings->retrieve_setting_editdata();
?>

<!-- Printable area start -->
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        // document.body.style.marginTop="-45px";
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
<!-- Printable area end -->

<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Sale Invoice
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Sale_invoice">Sale Invoice</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <div id="printableArea">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <img src="<?= isset($Web_settings[0]['invoice_logo']) && !empty($Web_settings[0]['invoice_logo']) ? $Web_settings[0]['invoice_logo'] : '' ?>" class="" alt="" style="margin-bottom:20px">
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <h2 class="m-0">Sale Invoice - <?= isset($sale_invoice_data['InvoiceType']) && !empty($sale_invoice_data['InvoiceType']) && $sale_invoice_data['InvoiceType'] == 1 ? 'GST' : 'Non-GST' ?></h2>
                                        <div><b>Invoice No:</b> <?= isset($sale_invoice_data['SINO']) && !empty($sale_invoice_data['SINO']) ? $sale_invoice_data['SINO'] : null ?></div>
                                        <div class="m-b-15"><b>Invoice Date:</b> <?= isset($sale_invoice_data['DateTime']) && !empty($sale_invoice_data['DateTime']) ? date('d/m/Y h:i A', strtotime($sale_invoice_data['DateTime'])) : null ?></div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row mb-2">
                                    <div class="col-sm-6">
                                        <?php
                                        if (isset($company_info) && !empty($company_info)) {
                                            ?>
                                            <span class="label label-success-outline m-r-15 p-10" >Billing From:</span>
                                            <address style="margin-top:10px">
                                                <strong style="font-size: 20px; "><?= $company_info['company_name'] ?></strong><br>
                                                <?= $company_info['address'] ?><br>
                                                <abbr><b>Mobile:</b></abbr> 
                                                <?= $company_info['mobile'] ?><br>
                                                <abbr><b>Email:</b></abbr> 
                                                <?= $company_info['email'] ?><br>
                                                <abbr><b>Website:</b></abbr> 
                                                <?= $company_info['website'] ?>
                                            </address>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="col-sm-6 text-left">
                                        <span class = "label label-success-outline mr-2">Billing To:</span>
                                        <address style="margin-top:10px;">  
                                            <strong style="font-size: 20px; "><?= isset($sale_invoice_data['company__name']) && !empty($sale_invoice_data['company__name']) ? $sale_invoice_data['company__name'] : null ?> </strong><br>
                                            <?php
                                            if (isset($sale_invoice_data['RefCustomerAddress']) && !empty($sale_invoice_data['RefCustomerAddress'])) {
                                                echo $sale_invoice_data['RefCustomerAddress'];
                                            } else {
                                                echo '';
                                            }

                                            if (isset($sale_invoice_data['RefCustomerPhone']) && !empty($sale_invoice_data['RefCustomerPhone'])) {
                                                ?>
                                                <br>
                                                <abbr><b>Mobile:</b></abbr>
                                                <?php
                                                echo $sale_invoice_data['RefCustomerPhone'];
                                            } else {
                                                echo '';
                                            }

                                            if (isset($sale_invoice_data['RefCustomerEmail']) && !empty($sale_invoice_data['RefCustomerEmail'])) {
                                                ?>
                                                <br>
                                                <abbr><b>Email:</b></abbr> 
                                                <?= $sale_invoice_data['RefCustomerEmail']; ?>
                                            <?php } ?>
                                        </address>
                                    </div>
                                </div>
                                <div class="table-responsive m-b-20">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">SN</th>
                                                <th class="text-center">Product Name</th>
                                                <th class="text-center">Rate</th>
                                                <th class="text-center">Qty</th>
                                                <th class="text-center">Discount </th>
                                                <th class="text-center">Tax Amount</th>
                                                <th class="text-center">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sl = 0;
                                            $total_qty = 0;
                                            if (isset($sale_invoice_details_data) && !empty($sale_invoice_details_data)) {
                                                foreach ($sale_invoice_details_data as $key => $value) {
                                                    $sl++;
                                                    $total_qty += $value->Qty;
                                                    $total = $value->TotalAmt + $value->TaxAmt;
                                                    ?>
                                                    <tr>
                                                        <td class="text-center"><?= $sl ?></td>
                                                        <td>
<!--                                                            <a href="<?php echo base_url() . 'admin/Cproduct/product_details/' . $value->refProductID; ?>">-->
                                                                <?= getProductNameById($value->refProductID) ?>
                                                            <!--</a>-->
                                                        </td>
                                                        <td align="center"><?= isset($value->UnitAmt) && !empty($value->UnitAmt) ? "$currency " . number_format($value->UnitAmt, 2) : null ?></td>
                                                        <td align="center"><?= isset($value->Qty) && !empty($value->Qty) ? ($value->Qty) : null ?></td>
                                                        <td align="center"><?= isset($value->DiscountAmt) && !empty($value->DiscountAmt) ? "$currency " . $value->DiscountAmt : "$currency " . number_format(0, 2) ?></td>
                                                        <td align="center"><?= isset($value->TaxAmt) && !empty($value->TaxAmt) ? "$currency " . $value->TaxAmt : "$currency " . number_format(0, 2) ?></td>
                                                        <td align="center"><?= isset($total) && !empty($total) ? "$currency " . number_format($total, 2) : "$currency " . number_format(0, 2) ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <tr>
                                                <td class="text-right" colspan="3" style="border: 0px"><b>Total Qty :</b></td>
                                                <td align="center"  style="border: 0px"><b><?= number_format($total_qty, 2) ?></b></td>
                                                <td style="border: 0px"></td>
                                                <td style="border: 0px"></td>
                                                <td align="center"  style="border: 0px"><b><?= "$currency " . number_format($subtotal, 2) ?></b></td>
                                            </tr>
                                        </tbody>
                                        <!--                                    <tfoot>
    
                                        </tfoot>-->
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8" style="display: inline-block;width: 66%">
                                        &nbsp;
                                    </div>
                                    <div class="col-xs-4" style="display: inline-block;">
                                        <table class="table">
                                            <tr>
                                                <th style="border-top: 0;">Sub Total :</th>
                                                <td style="border-top: 0;"><?= "$currency " . number_format($subtotal, 2) ?></td>
                                            </tr>
                                            <tr>
                                                <th style="border-top: 0; border-bottom: 0;">Fright Charges : </th>
                                                <td style="border-top: 0; border-bottom: 0;"><?= "$currency " . $sale_invoice_data['FrightCharges']; ?></td>
                                            </tr>
                                            <tr>
                                                <th style="border-top: 0; border-bottom: 0;">Round Up : </th>
                                                <td style="border-top: 0; border-bottom: 0;"><?= "$currency " . $sale_invoice_data['RoundUpAmt']; ?></td>
                                            </tr>
                                            <tr>
                                                <th class="grand_total">Grand Total :</th>
                                                <td class="grand_total text-success"><b><?= "$currency " . number_format($sale_invoice_data['GrandTotal'], 2) ?></b></td>
                                            </tr>			 
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-right">
                            <a  class="btn btn-danger" href="<?php echo base_url('admin/Sale_invoice'); ?>">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>



