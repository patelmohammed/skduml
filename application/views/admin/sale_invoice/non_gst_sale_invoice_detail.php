<!-- Manage Product Start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1>Manage Non-GST Sale Invoice</h1>
            <small>Manage Your Non-GST Sale Invoice</small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#">Sale Invoice</a></li>
                <li class="active">Manage Non-GST Sale Invoice</li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <?php
                    if ($menu_rights['add_right'] == 1) {
                        ?>
                        <a href="<?php echo base_url('Sale_invoice/addNonGSTSaleInvoice') ?>" class="btn btn-info m-b-5 m-r-2"><i class="ti-plus"> </i>Add Non-GST Sale Invoice </a>
                    <?php } ?>

                        <!--<a href="<?php // echo base_url('Cproduct/add_product_csv')                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ?>" class="btn btn-primary m-b-5 m-r-2"><i class="ti-plus"> </i>  <?php // echo display('add_product_csv')                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ?> </a>-->

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body"> 
                        <div class="form-inline">   
                            <div class="form-group m-r-15">
                                <label class="m-r-5" for="from_date"><?php echo display('start_date') ?></label>
                                <input type="text" name="from_date" class="form-control datepicker" id="from_date" placeholder="DD-MM-YYYY" value="<?= date_format(date_create(FIN_YEAR_STRT_DATE),'d-m-Y') ?>">
                            </div> 
                            <div class="form-group m-r-15">
                                <label class="m-r-5" for="to_date"><?php echo display('end_date') ?></label>
                                <input type="text" name="to_date" class="form-control datepicker" id="to_date" placeholder="DD-MM-YYYY" value="<?= date_format(date_create(FIN_YEAR_END_DATE),'d-m-Y') ?>">
                            </div>  
                            <button type="submit" class="btn btn-success" id="date_filter_btn"><?php echo display('search') ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Manage Product report -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Manage Non-GST Sale Invoice</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="non_gst_sale_invoice_data" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th><?php echo display('sl') ?></th>
                                        <th>Date</th>
                                        <th>Sale Invoice No.</th>
                                        <th>Customer Name</th>
                                        <th>Total Product</th>
                                        <th>Grand Total</th>
                                        <th style="width : 170px !important;"><?php echo display('action') ?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Manage Product End -->

<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">  
    <div class="modal-dialog" role="document" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-2x">×</i></span></button>
                <h4 class="modal-title" id="myModalLabel">Print Sale Invoice</h4>
            </div>
            <div class="modal-body" id="product-invoice-model">

            </div>
            <div class="modal-footer">
<!--                <a href="javascript:void(0);" target="_blanck" type="button" class="btn btn-primary" id="pdfSaleInvoice">
                    <i class="fa fa-save"></i> Print
                </a>-->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mailSaleInvoiceDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-2x">×</i></span></button>
                    <h4 class="modal-title" id="myModalLabel">Mail Sale Invoice</h4>
                </div>
                <form class="form-vertical">
                    <div class="row" style="padding-top: 10px;">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="mail_to" class="col-form-label">Mail To<i class="text-danger"> *</i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        <select name="mail_to" class="form-control dynamic_tag_select2 select2-container" id="mail_to" required multiple="">
                                            <?php
                                            if (!empty($employee_list)) {
                                                foreach ($employee_list as $k1 => $v1) {
                                                    ?>
                                                    <option value="<?= $v1['email'] ?>"><?= !empty($v1['email']) ? $v1['email'] : '' ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="mail_subject" class="col-form-label">Mail Subject</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="mail_subject" name="mail_subject" placeholder="Mail Subject"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="hidden_siid" id="hidden_siid" value="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="mail_body" class="col-form-label">Mail Body</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <textarea class="form-control ckeditor" id="mail_body" name="mail_body" placeholder="Mail Body"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        &nbsp;
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="checkbox" name="mail_send_me" id="mail_send_me">  Send Me
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="mailSaleInvoiceData">
                    <i class="fa fa-envelope"></i> Mail</button>
            </div>
        </div>
    </div>
</div>

<!-- Delete Product ajax code -->
<script type="text/javascript">

    $(document).ready(function () {
        getSaleInvoiceList();
        
        $(document).on('click', '#date_filter_btn', function () {
            $('#non_gst_sale_invoice_data').DataTable().destroy();
            getSaleInvoiceList();
        });
        

        $(document).on('click', '.deleteSaleInvoice', function (e) {
            var sale_invoice_id = $(this).attr('name');
            var csrf_test_name = $("[name=csrf_test_name]").val();
            swal({
                title: 'Are you sure,Want to Delete?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }, function () {
                fullScreenLoader();
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url('Sale_invoice/saleInvoiceDelete') ?>',
                    data: {sale_invoice_id: sale_invoice_id, csrf_test_name: csrf_test_name},
                    cache: false,
                    success: function (datas) {
                        HoldOn.close();
                        location.reload();
                    }
                });
                HoldOn.close();
            });
        });

        $(document).on('click', '.print_sale_invoice_pdf', function (e) {
            fullScreenLoader();
            e.preventDefault();
            var sale_invoice_id = $(this).attr('name');
            var SINO = $(this).attr('data-sino');
            $.ajax({
                type: "POST",
                url: '<?php echo base_url('Sale_invoice/printSaleInvoicePdf') ?>',
                data: {sale_invoice_id: sale_invoice_id},
                cache: false,
                success: function (datas) {
                    $('#pdfSaleInvoice').attr('href', '<?php echo base_url('Sale_invoice/printPdfSaleInvoice') ?>/' + sale_invoice_id + '/' + SINO);
                    $('#product-invoice-model').html(datas);
                    HoldOn.close();
                    $('#invoiceModal').modal('show');
                }
            });
        });

        $(document).on('click', '.mailSaleInvoiceForm', function (e) {
            fullScreenLoader();
            e.preventDefault();
            var SIID = $(this).attr('name');
            $('#hidden_siid').val(SIID);
            document.getElementById("mailSaleInvoiceData").disabled = false;
            HoldOn.close();
            $('#mailSaleInvoiceDetail').modal('show');
        });

        $(document).on('click', '#mailSaleInvoiceData', function (e) {
            fullScreenLoader();
            e.preventDefault();
            document.getElementById("mailSaleInvoiceData").disabled = true;
            var SIID = $('#hidden_siid').val();
            var mail_to = $('#mail_to').val();
            var mail_subject = $('#mail_subject').val();
            var mail_body = CKEDITOR.instances['mail_body'].getData()
            var send_me = $('#mail_send_me').prop("checked");
            $.ajax({
                type: "POST",
                url: '<?php echo base_url('Sale_invoice/mailSaleInvoiceDetail') ?>',
                data: {SIID: SIID, mail_to: mail_to, mail_subject: mail_subject, mail_body: mail_body, send_me: send_me},
                cache: false,
                success: function (returnData) {
                    HoldOn.close();
                    var data = JSON.parse(returnData);
                    if (data.result == true) {
                        $('#mailSaleInvoiceDetail').modal('hide');
                    }
                }
            });
        });
    });

    function getSaleInvoiceList() {
        var non_gst_sale_invoice_data = $('#non_gst_sale_invoice_data').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>t<'bottom'><'row'<'col-sm-5'i>p>",
            "oTableTools": {
                "aButtons": [
                ]
            },
            buttons: [
                {extend: 'excel', title: 'manage_sale_invoice', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3, 4, 5], modifier: {page: 'current'}}},
                {extend: 'print', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3, 4, 5], modifier: {page: 'current'}}}
            ],
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": [0, 6],
                    "orderable": false
                }, {
                    "targets": [0, 6],
                    "className": 'text-center'
                }],
            "ajax": {
                "url": "<?= base_url() ?>Sale_invoice/get_non_gst_sale_invoice_list",
                "type": "POST",
                "data": function (d) {
                    d.start_date = $('#from_date').val();
                    d.end_date = $('#to_date').val();
                }
            }
        });
        $("#non_gst_sale_invoice_data thead input").on('keyup change', function () {
            non_gst_sale_invoice_data.column($(this).parent().index() + ':visible').search(this.value).draw();
        });
    }
</script>