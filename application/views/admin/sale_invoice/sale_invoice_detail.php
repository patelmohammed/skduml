<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Manage GST Sale Invoice
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Sale_invoice/addSaleInvoice">Add GST Sale Invoice</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <div class="row mb-3">
                            <div class="col-md-1">
                                <label class="form-label">Start Date</label>
                            </div>
                            <div class="col-md-2">
                                <input type="date" name="from_date" class="form-control" id="from_date" placeholder="DD-MM-YYYY" value="<?= date('Y-m-d', strtotime(FIN_YEAR_STRT_DATE)) ?>">
                            </div>
                            <div class="col-md-1">
                                <label class="form-label">End Date</label>
                            </div>
                            <div class="col-md-2">
                                <input type="date" name="to_date" class="form-control" id="to_date" placeholder="DD-MM-YYYY" value="<?= date('Y-m-d', strtotime(FIN_YEAR_END_DATE)) ?>">
                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-primary" id="date_filter_btn">Search</button>
                            </div>
                        </div>
                        <table id="sale_invoice_data" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Date</th>
                                    <th>Sale Invoice No.</th>
                                    <th>Customer Name</th>
                                    <th>Total Product</th>
                                    <th>Grand Total</th>
                                    <th>Created By</th>
                                    <th>Updated By</th>
                                    <th class="text-center notexport">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">  
    <div class="modal-dialog" role="document" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-2x">×</i></span></button>
                <h4 class="modal-title" id="myModalLabel">Print Sale Invoice</h4>
            </div>
            <div class="modal-body" id="product-invoice-model">

            </div>
            <div class="modal-footer">
                <!--                <a href="javascript:void(0);" target="_blanck" type="button" class="btn btn-primary" id="pdfSaleInvoice">
                                    <i class="fa fa-save"></i> Print
                                </a>-->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mailSaleInvoiceDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-2x">×</i></span></button>
                    <h4 class="modal-title" id="myModalLabel">Mail Sale Invoice</h4>
                </div>
                <form class="form-vertical">
                    <div class="row" style="padding-top: 10px;">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="mail_to" class="col-form-label">Mail To<i class="text-danger"> *</i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        <select name="mail_to" class="form-control dynamic_tag_select2 select2-container" id="mail_to" required multiple="">
                                            <?php
                                            if (!empty($employee_list)) {
                                                foreach ($employee_list as $k1 => $v1) {
                                                    ?>
                                                    <option value="<?= $v1['user_email'] ?>"><?= !empty($v1['user_email']) ? $v1['user_email'] : '' ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="mail_subject" class="col-form-label">Mail Subject</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="mail_subject" name="mail_subject" placeholder="Mail Subject"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="hidden_siid" id="hidden_siid" value="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="mail_body" class="col-form-label">Mail Body</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <textarea class="form-control ckeditor" id="mail_body" name="mail_body" placeholder="Mail Body"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        &nbsp;
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="checkbox" name="mail_send_me" id="mail_send_me">  Send Me
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="mailSaleInvoiceData">
                    <i class="fa fa-envelope"></i> Mail</button>
            </div>
        </div>
    </div>
</div>

<!-- Delete Product ajax code -->
<script type="text/javascript">
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary mr-2",
            cancelButton: "btn btn-danger"
        },
    });

    $(document).ready(function () {
<?php if (isset($last_inserted_sale_invoice_id) && !empty($last_inserted_sale_invoice_id)) {
    ?>
            window.open('<?= base_url('admin/Sale_invoice/printPdfSaleInvoice/' . $last_inserted_sale_invoice_id) ?>', '_blank');
<?php }
?>
        getSaleInvoiceList();

        $(document).on('click', '#date_filter_btn', function () {
            $('#sale_invoice_data').DataTable().destroy();
            getSaleInvoiceList();
        });


        $(document).on('click', '.deleteSaleInvoice', function (e) {
            var sale_invoice_id = $(this).attr('name');
            var csrf_test_name = $("[name=csrf_test_name]").val();
            swalWithBootstrapButtons.fire({
                title: 'Are you sure,Want to Delete?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url('admin/Sale_invoice/saleInvoiceDelete') ?>',
                        data: {sale_invoice_id: sale_invoice_id, csrf_test_name: csrf_test_name},
                        cache: false,
                        success: function (datas) {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('click', '.print_sale_invoice_pdf', function (e) {

            e.preventDefault();
            var sale_invoice_id = $(this).attr('name');
            var SINO = $(this).attr('data-sino');
            $.ajax({
                type: "POST",
                url: '<?php echo base_url('Sale_invoice/printSaleInvoicePdf') ?>',
                data: {sale_invoice_id: sale_invoice_id},
                cache: false,
                success: function (datas) {
                    $('#pdfSaleInvoice').attr('href', '<?php echo base_url('admin/Sale_invoice/printPdfSaleInvoice') ?>/' + sale_invoice_id + '/' + SINO);
                    $('#product-invoice-model').html(datas);

                    $('#invoiceModal').modal('show');
                }
            });
        });

        $(document).on('click', '.mailSaleInvoiceForm', function (e) {

            e.preventDefault();
            var SIID = $(this).attr('name');
            $('#hidden_siid').val(SIID);
            document.getElementById("mailSaleInvoiceData").disabled = false;

            $('#mailSaleInvoiceDetail').modal('show');
        });

        $(document).on('click', '#mailSaleInvoiceData', function (e) {

            e.preventDefault();
            document.getElementById("mailSaleInvoiceData").disabled = true;
            var SIID = $('#hidden_siid').val();
            var mail_to = $('#mail_to').val();
            var mail_subject = $('#mail_subject').val();
            var mail_body = CKEDITOR.instances['mail_body'].getData()
            var send_me = $('#mail_send_me').prop("checked");
            $.ajax({
                type: "POST",
                url: '<?php echo base_url('Sale_invoice/mailSaleInvoiceDetail') ?>',
                data: {SIID: SIID, mail_to: mail_to, mail_subject: mail_subject, mail_body: mail_body, send_me: send_me},
                cache: false,
                success: function (returnData) {

                    var data = JSON.parse(returnData);
                    if (data.result == true) {
                        $('#mailSaleInvoiceDetail').modal('hide');
                    }
                }
            });
        });
    }
    );

    function getSaleInvoiceList() {
        var _sale_invoice_data = $('#sale_invoice_data').DataTable({
            dom: "<'row '<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'lf><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            "oTableTools": {
                "aButtons": [
                ]
            },
            buttons: [{
                    extend: 'pdfHtml5',
                    text: '<i class="fal fa-file-pdf "></i>',
                    titleAttr: 'Generate PDF',
                    className: 'btn-outline-danger btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'excelHtml5',
                    text: '<i class="fal fa-file-excel "></i>',
                    titleAttr: 'Generate Excel',
                    className: 'btn-outline-success btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'csvHtml5',
                    text: '<i class="fal fa-file-csv "></i>',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-primary btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    }
                }, {
                    extend: 'copyHtml5',
                    text: '<i class="fal fa-copy "></i>',
                    titleAttr: 'Copy to clipboard',
                    className: 'btn-outline-info btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'print',
                    text: '<i class="fal fa-print "></i>',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-dark btn-sm hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-dark-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            },
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": [0, 8],
                    "orderable": false
                }, {
                    "targets": [0],
                    "className": 'text-center'
                }],
            "ajax": {
                "url": "<?= base_url() ?>admin/Sale_invoice/get_sale_invoice_list",
                "type": "POST",
                "data": function (d) {
                    d.start_date = $('#from_date').val();
                    d.end_date = $('#to_date').val();
                }
            }
        });
        $("#sale_invoice_data thead input").on('keyup change', function () {
            _sale_invoice_data.column($(this).parent().index() + ':visible').search(this.value).draw();
        });
    }
</script>