<script src="<?php echo base_url() ?>my-assets/js/admin_js/json/product.js" type="text/javascript"></script>
<!-- Add Product Start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1>Update Non-GST Sale Invoice</h1>
            <small>Edit Your Non GST Sale Invoice</small>
            <ol class="breadcrumb">
                <li><a href="index.html"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#">Sale Invoice</a></li>
                <li class="active">Update Sale Invoice</li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">

                    <a href="<?php echo base_url('Sale_invoice/manageNonGSTSaleInvoice') ?>" class="btn btn-primary m-b-5 m-r-2"><i class="ti-align-justify"> </i>Manage Non-GST Sale Invoice</a>

                </div>
            </div>
        </div>

        <!-- Add Product -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Update Sale Invoice</h4>
                        </div>
                    </div>
                    <?php echo form_open_multipart('Sale_invoice/updateNonGSTSaleInvoice/' . $SIID, array('class' => 'form-vertical', 'id' => 'update_non_gst_sale_invoice', 'name' => 'update_non_gst_sale_invoice')) ?>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="sale_invoice_no">Invoice No. <i class="text-danger">*</i></label>
                                    <input type="text" class="form-control" id="sale_invoice_no" name="sale_invoice_no" value="<?= isset($sale_invoice['SINO']) && !empty($sale_invoice['SINO']) ? $sale_invoice['SINO'] : null ?>" required readonly="">
                                    <input type="hidden" class="form-control" id="sale_invoice_id" name="sale_invoice_id" value="<?= isset($SIID) && !empty($SIID) ? $SIID : null ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="datetime">Invoice Date <i class="text-danger">*</i></label>
                                    <div class="input-group date" id="invoice_date_div">
                                        <input type="text" class="form-control" id="invoice_date" name="invoice_date" data-type="date" value="<?= isset($sale_invoice['DateTime']) && !empty($sale_invoice['DateTime']) ? date('d-m-Y', strtotime($sale_invoice['DateTime'])) : null ?>" required>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="datetime">Due Date <i class="text-danger">*</i></label>
                                    <div class="input-group date" id="due_date_div">
                                        <input type="text" class="form-control" id="due_date" name="due_date" data-type="date" value="<?= isset($sale_invoice['DueDate']) && !empty($sale_invoice['DueDate']) ? date('d-m-Y', strtotime($sale_invoice['DueDate'])) : null ?>" required>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="invoice_receievd_name">Invoice Receiver Name</label>
                                    <input type="text" class="form-control" id="invoice_receievd_name" name="invoice_receievd_name" value="<?= isset($sale_invoice['ReceiverName']) && !empty($sale_invoice['ReceiverName']) ? $sale_invoice['ReceiverName'] : null ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="customer_id">Customer <i class="text-danger">*</i></label>
                                    <select name="customer_id" class="form-control select2222" id="customer_id" required="" data-placeholder="-- Select Customer --">
                                        <?php
                                        if (isset($customer_list) && !empty($customer_list)) {
                                            foreach ($customer_list as $key => $value) {
                                                ?>
                                                <option value="<?= $value['customer_id'] ?>" <?= isset($sale_invoice['RefCustomerID']) && !empty($sale_invoice['RefCustomerID']) && $sale_invoice['RefCustomerID'] == $value['customer_id'] ? 'selected' : null ?> data-name="<?= isset($value['company__name']) && !empty($value['company__name']) ? $value['company__name'] : null ?>" data-address="<?= (isset($value['customer_address']) && !empty($value['customer_address']) ? $value['customer_address'] : null) . (isset($value['customer_city']) && !empty($value['customer_city']) ? ', ' . $value['customer_city'] : null) . (isset($value['state_name']) && !empty($value['state_name']) ? ', ' . $value['state_name'] : null) . (isset($value['customer_country']) && !empty($value['customer_country']) ? ', ' . $value['customer_country'] : null) . (isset($value['customer_pincode']) && !empty($value['customer_pincode']) ? ' - ' . $value['customer_pincode'] : (isset($value['customer_address']) && !empty($value['customer_address']) ? '.' : null)) ?>" data-phonenumber="<?= $value['customer_mobile'] ?>" data-email="<?= $value['customer_email'] ?>"><?= $value['company__name'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group" id="customer_all_detail">
                                    <label for="customer_detail" class="col-form-label" >Billing Address<a style="margin-left: 11px; color: black;" id="edit_billing_address_btn" href="javascript:void(0);"><i class="fa fa-pencil" style="font-size: 18px;" title="Edit"></i></a></label><br>
                                    <textarea class="form-control valid" style="resize:vertical;" rows="3" name="hidden_customer_address" id="hidden_customer_address" readonly=""><?= isset($sale_invoice['RefCustomerAddress']) && !empty($sale_invoice['RefCustomerAddress']) ? $sale_invoice['RefCustomerAddress'] : '' ?></textarea>

<!--                                    <sapn id="customer_address"><?= isset($sale_invoice['RefCustomerAddress']) && !empty($sale_invoice['RefCustomerAddress']) ? '<b>Address : </b>' . $sale_invoice['RefCustomerAddress'] . '<br>' : '' ?></sapn>
                                    <input type="hidden" name="hidden_customer_address" id="hidden_customer_address" value="<?= isset($sale_invoice['RefCustomerAddress']) && !empty($sale_invoice['RefCustomerAddress']) ? $sale_invoice['RefCustomerAddress'] : '' ?>">-->

                                    <!--<sapn id="customer_phonenumber"><?= isset($sale_invoice['RefCustomerPhone']) && !empty($sale_invoice['RefCustomerPhone']) ? '<b>Phone : </b>' . $sale_invoice['RefCustomerPhone'] . '<br>' : '' ?></sapn>-->
                                    <input type="hidden" name="hidden_customer_phonenumber" id="hidden_customer_phonenumber" value="<?= isset($sale_invoice['RefCustomerPhone']) && !empty($sale_invoice['RefCustomerPhone']) ? $sale_invoice['RefCustomerPhone'] : '' ?>">

                                    <!--<sapn id="customer_email"><?= isset($sale_invoice['RefCustomerEmail']) && !empty($sale_invoice['RefCustomerEmail']) ? '<b>Email : </b>' . $sale_invoice['RefCustomerEmail'] . '<br>' : '' ?></sapn>-->
                                    <input type="hidden" name="hidden_customer_email" id="hidden_customer_email" value="<?= isset($sale_invoice['RefCustomerEmail']) && !empty($sale_invoice['RefCustomerEmail']) ? $sale_invoice['RefCustomerEmail'] : '' ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="agent_id">Agent</label>
                                    <select name="agent_id" class="form-control select222" id="agent_id" data-placeholder="-- Select Agent --">
                                        <option value=""></option>
                                        <?php
                                        if (isset($agent_list) && !empty($agent_list)) {
                                            foreach ($agent_list as $key2 => $value2) {
                                                ?>
                                                <option value="<?= isset($value2->agent_id) && !empty($value2->agent_id) ? $value2->agent_id : null ?>" <?= isset($sale_invoice['RefAgentId']) && !empty($sale_invoice['RefAgentId']) && $sale_invoice['RefAgentId'] == $value2->agent_id ? 'selected' : null ?> data-name="<?= isset($value2->agent_name) && !empty($value2->agent_name) ? $value2->agent_name : null ?>" data-address="<?= isset($value2->agent_address) && !empty($value2->agent_address) ? $value2->agent_address : null ?>" data-phonenumber="<?= isset($value2->agent_mobile) && !empty($value2->agent_mobile) ? $value2->agent_mobile : null ?>" data-email="<?= isset($value2->agent_email) && !empty($value2->agent_email) ? $value2->agent_email : null ?>"><?= isset($value2->agent_name) && !empty($value2->agent_name) ? $value2->agent_name : null ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group" id="agent_all_detail">
                                    <label for="agent_detail" class="col-form-label" >Agent Detail</label><br>

                                    <sapn id="agent_address"><?= isset($sale_invoice['RefAgentAddress']) && !empty($sale_invoice['RefAgentAddress']) ? '<b>Address : </b>' . $sale_invoice['RefAgentAddress'] . '<br>' : '' ?></sapn>
                                    <input type="hidden" name="hidden_agent_address" id="hidden_agent_address" value="<?= isset($sale_invoice['RefAgentAddress']) && !empty($sale_invoice['RefAgentAddress']) ? $sale_invoice['RefAgentAddress'] : '' ?>">

                                    <sapn id="agent_phonenumber"><?= isset($sale_invoice['RefAgentPhone']) && !empty($sale_invoice['RefAgentPhone']) ? '<b>Phone : </b>' . $sale_invoice['RefAgentPhone'] . '<br>' : '' ?></sapn>
                                    <input type="hidden" name="hidden_agent_phonenumber" id="hidden_agent_phonenumber" value="<?= isset($sale_invoice['RefAgentPhone']) && !empty($sale_invoice['RefAgentPhone']) ? $sale_invoice['RefAgentPhone'] : '' ?>">

                                    <sapn id="agent_email"><?= isset($sale_invoice['RefAgentEmail']) && !empty($sale_invoice['RefAgentEmail']) ? '<b>Email : </b>' . $sale_invoice['RefAgentEmail'] . '<br>' : '' ?></sapn>
                                    <input type="hidden" name="hidden_agent_email" id="hidden_agent_email" value="<?= isset($sale_invoice['RefAgentEmail']) && !empty($sale_invoice['RefAgentEmail']) ? $sale_invoice['RefAgentEmail'] : '' ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row" id="commission_div" style="<?= isset($sale_invoice['RefAgentId']) && !empty($sale_invoice['RefAgentId']) ? 'display:block' : 'display:none' ?>">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="commission_rate_id">Commission Type</label>
                                    <select name="commission_rate_id" class="form-control select2222" id="commission_rate_id" data-placeholder="-- Select Commission --">
                                        <option value="" selected="" disabled=""></option>
                                        <?php
                                        if (isset($commision_rate_list) && !empty($commision_rate_list)) {
                                            foreach ($commision_rate_list as $key1 => $value1) {
                                                if ($sale_invoice['RefCommissionRateId'] == $value1->commission_id) {
                                                    $sel_com = 'selected';
                                                } else {
                                                    $sel_com = '';
                                                }
                                                ?>
                                                <option value="<?= isset($value1->commission_id) && !empty($value1->commission_id) ? $value1->commission_id : null ?>" <?= $sel_com ?> ><?= isset($value1->commission_name) && !empty($value1->commission_name) ? $value1->commission_name : null ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4" id="commission_rate_type_div">
                                <div class="form-group">
                                    <label for="commission_rate_type">Rate Type</label>
                                    <select name="commission_rate_type" class="form-control select2222" id="commission_rate_type" data-placeholder="-- Select Type --">
                                        <option value="" selected="" disabled=""></option>
                                        <option value="1" <?= isset($sale_invoice['RefCommissionRateType']) && !empty($sale_invoice['RefCommissionRateType']) && $sale_invoice['RefCommissionRateType'] == 1 ? 'selected' : '' ?> >Fix Per KG</option>
                                        <option value="2" <?= isset($sale_invoice['RefCommissionRateType']) && !empty($sale_invoice['RefCommissionRateType']) && $sale_invoice['RefCommissionRateType'] == 2 ? 'selected' : '' ?> >Percentage</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4" id="commission_rate_div">
                                <div class="form-group">
                                    <label for="commission_rate">Commission Rate</label>
                                    <input type="text" class="form-control" id="commission_rate" name="commission_rate" value="<?= isset($sale_invoice['RefCommissionRate']) && !empty($sale_invoice['RefCommissionRate']) ? $sale_invoice['RefCommissionRate'] : null ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="eway_bill_no">E-way Bill No.</label>
                                    <input type="text" class="form-control" id="eway_bill_no" name="eway_bill_no" value="<?= isset($sale_invoice['EwayBillNo']) && !empty($sale_invoice['EwayBillNo']) ? $sale_invoice['EwayBillNo'] : null ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="datetime">Delivery Date <i class="text-danger">*</i></label>
                                    <div class="input-group date" id="delivery_date_div">
                                        <input type="text" class="form-control" id="delivery_date" name="delivery_date" data-type="date" value="<?= isset($sale_invoice['DeliveryDate']) && !empty($sale_invoice['DeliveryDate']) ? date('d-m-Y', strtotime($sale_invoice['DeliveryDate'])) : null ?>" required>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="vehicle_id">Vehicle</label>
                                    <select name="vehicle_id" class="form-control select222" id="vehicle_id" data-placeholder="-- Select Vehicle --">
                                        <option value="">-- Select Vehicle --</option>
                                        <?php
                                        if (isset($vehicle_list) && !empty($vehicle_list)) {
                                            foreach ($vehicle_list as $key4 => $value4) {
                                                if ($sale_invoice['RefVehicleId'] == $value4->vehicle_id) {
                                                    $sel_veh = 'selected';
                                                } else {
                                                    $sel_veh = '';
                                                }
                                                ?>
                                                <option value="<?= isset($value4->vehicle_id) && !empty($value4->vehicle_id) ? $value4->vehicle_id : null ?>" <?= $sel_veh ?> ><?= isset($value4->vehicle_name) && !empty($value4->vehicle_name) ? $value4->vehicle_name . ' - (' . $value4->vehicle_type . ')' : null ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="sale_invoice_remark">Remarks</label>
                                    <input type="text" class="form-control" id="sale_invoice_remark" name="sale_invoice_remark" value="<?= isset($sale_invoice['Remarks']) && !empty($sale_invoice['Remarks']) ? $sale_invoice['Remarks'] : null ?>" />
                                </div>
                            </div>
                        </div>
                        <!--                        <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="sale_invoice_tc">Terms & Conditions</label>
                                                            <textarea type="text" rows="5" class="form-control" id="sale_invoice_tc" name="sale_invoice_tc"><?= isset($sale_invoice['TermsAndConditions']) && !empty($sale_invoice['TermsAndConditions']) ? $sale_invoice['TermsAndConditions'] : null ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>-->
                        <hr>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="products">Products <i class="text-danger">*</i></label>
                                    <select name="product[]" class="form-control select2222" id="product_id" data-placeholder="-- Select Product --">
                                        <option value="" data-address=""> select Product</option>
                                        <?php
                                        if ($product_list) {
                                            foreach ($product_list as $k => $spval) {
                                                ?>
                                                <option value="<?php echo $spval['id'] . "|" . $spval['product_name'] . "|" . $spval['supplier_price'] . "|" . $spval['tax_name'] . "|" . $spval['tax'] . "|" . $spval['tax_id'] ?> "><?= $spval['product_name'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!--                            <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="product_barcode">Product Barcode</label>
                                                                <input type="text" class="form-control" id="product_barcode" name="product_barcode" placeholder="Product Barcode" />
                                                            </div>
                                                        </div>-->
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive" id="product_cart">          
                                    <table class="table">
                                        <thead>
                                            <tr class="text-nowrap">
                                                <th style="min-width: 40px;">SN</th>
                                                <th style="min-width: 260px;">Products</th>
                                                <th style="min-width: 130px;">Product Price</th>
                                                <th style="min-width: 130px;">Quantity</th>
                                                <th style="min-width: 130px">Discount</th>
<!--                                                <th style="min-width: 100px;">Tax</th>
                                                <th style="min-width: 130px;">Tax Amount</th>-->
                                                <th style="min-width: 150px;">Total</th>
                                                <th style="min-width: 70px;">Action</th>   
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($invoice_sale_list) && $invoice_sale_list != '' && isset($invoice_sale_list)) {
                                                foreach ($invoice_sale_list as $k => $product_value) {
                                                    $k++;
                                                    $product_row = '';
                                                    $product_row .= '<tr class="text-nowrap rowCount" data-id="' . $k . '" id="row_' . $k . '">';
                                                    $product_row .= '<td style="padding-left: 10px;"><p id="sl_' . $k . '">' . $k . '</p></td>';
                                                    $product_row .= '<td><span style="padding-bottom: 5px;">' . getProductNameById($product_value->refProductID) . '</span></td>';

                                                    $product_row .= '<input type="hidden" id="product_id_' . $k . '" name="product_id[]" value="' . $product_value->refProductID . '"/>';
                                                    $product_row .= '<input type="hidden" id="tax_per_' . $k . '" name="tax_per[]" value="' . $product_value->tax_per . '"/>';
                                                    $product_row .= '<input type="hidden" id="tax_per_amt_' . $k . '" name="tax_per_amt[]" value=""/>';
                                                    $product_row .= '<input type="hidden" id="total_tax_' . $k . '" name="total_tax[]" value="' . $product_value->TaxAmt . '"/>';
                                                    $product_row .= '<input type="hidden" id="discount_ind_' . $k . '" name="discount_ind[]" value="' . $product_value->DiscountAmt . '"/>';
                                                    $product_row .= '<input type="hidden" id="BasicTotal_' . $k . '" name="BasicTotal[]" value="' . $product_value->UnitAmt * $product_value->Qty . '"/>';
                                                    $product_row .= '<input type="hidden" id="Tax_id_' . $k . '" name="Tax_id[]" value="' . $product_value->Tax_id . '"/>';

                                                    $product_row .= '<input type="hidden" id="total_amt_' . $k . '" name="total_amt[]" value="' . $product_value->TotalAmt . '"/>';
                                                    $product_row .= '<input type="hidden" id="tax_name_' . $k . '" name="tax_name[]" value="' . $product_value->tax_name . '"/>';
                                                   
                                                    $product_row .= '<td><input type="text" id="unit_price_' . $k . '" name="unit_price[]" onfocus="this.select();" class="form-control desimalcheck aligning" placeholder="Unit Price" value="' . $product_value->UnitAmt . '" onkeyup="return calculateAll();" required /><span class="label_aligning"></span></td>';
                                                    $product_row .= '<td><input type="text" data-countID="' . $k . '" id="quantity_amount_' . $k . '" name="quantity_amount[]" onfocus="this.select();" class="form-control integerchkforqua aligning countID"  placeholder="Quantity" onkeyup="return calculateAll();" value="' . $product_value->Qty . '" required data-sale_outward_qty="" onblur="checkStock(' . $k . ')"></td>';
                                                    $product_row .= '<td><input type="text" data-countID="' . $k . '" id="discount_' . $k . '" name="discount[]" onfocus="this.select();" class="form-control integerchkforqua aligning countID" value="' . $product_value->DiscountPer . '" placeholder="Discount" onkeyup="return calculateAll();" ></td>';
//                                                    $product_row .= '<td><span id="tax_display_' . $k . '">' . $product_value->tax_name . '</span></td>';
//                                                    $product_row .= '<td><input type="text" id="tax_amount_' . $k . '" name="tax_amount[]" class="form-control" value="' . $product_value->TaxAmt . '" placeholder="Tax Amount" readonly /><span class="label_aligning"></span></td>';

                                                    $total = floatval($product_value->TotalAmt) + floatval($product_value->TaxAmt);
                                                    $product_row .= '<td><input type="text" id="total_' . $k . '" name="total[]" class="form-control aligning" value="' . number_format(floatval($total), 2) . '" placeholder="Total" readonly /><span class="label_aligning"></span></td>';
                                                    $product_row .= '<td><button class="btn btn-danger red valid" type="button" value="Delete" onclick="deleter(' . $k . ', \'' . $product_value->refProductID . '\')" autocomplete="off" aria-invalid="false"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                                                    $product_row .= '</tr>';
                                                    echo $product_row;
                                                }
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div> 
                            </div>
                        </div>

                        <br>

                        <div class="row">
                            <div class="col-md-9"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="fright_charges">Fright Charges</label>
                                    <input type="text" class="form-control desimalcheck" id="fright_charges" name="fright_charges" placeholder="Fright Charges" onkeyup="calculateAll();" value="<?= isset($sale_invoice['FrightCharges']) && !empty($sale_invoice['FrightCharges']) ? $sale_invoice['FrightCharges'] : null ?>" />
                                </div>
                            </div>
                            <div class="col-md-9"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="round_up_amt">Round Up</label>
                                    <input type="text" class="form-control desimalcheck" id="round_up_amt" name="round_up_amt" placeholder="Round Up Amount" onkeyup="calculateAll();" value="<?= isset($sale_invoice['RoundUpAmt']) && !empty($sale_invoice['RoundUpAmt']) ? $sale_invoice['RoundUpAmt'] : null ?>" />
                                </div>
                            </div>
                            <div class="col-md-9"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="g_total">Grand Total</label>
                                    <input type="text" class="form-control" id="g_total" name="g_total" placeholder="Grand Total" value="<?= $sale_invoice['GrandTotal'] ?>" readonly />
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="submit" id="add-sale-invoice" class="btn btn-primary btn-large" name="add-sale-invoice" value="<?php echo display('save') ?>" />
                                <input type="submit" value="<?php echo display('save_and_add_another') ?>" name="add-sale-invoice-another" class="btn btn-large btn-success" id="add-sale-invoice-another">
                                <a href="<?= base_url() ?>Sale_invoice/manageNonGSTSaleInvoice"  class="btn btn-danger btn-large">Cancel</a>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Add Product End -->

<script>
    var company_code = '<?= $company_code ?>';

<?php
$ingredient_id_container = "[";
if ($invoice_sale_list && !empty($invoice_sale_list)) {
    foreach ($invoice_sale_list as $pi) {
        $ingredient_id_container .= '"' . $pi->refProductID . '",';
    }
}
$ingredient_id_container = substr($ingredient_id_container, 0, -1);
$ingredient_id_container .= "]";
?>
    var product_id_container = <?= $ingredient_id_container ?>;

    $(function () {

        var suffix =
<?php
if (isset($invoice_sale_list)) {
    echo count($invoice_sale_list);
} else {
    echo 0;
}
?>;

        var supp_selcted_or_not = 1;
        $(document).on('change', '#product_id', function () {
            var customer_id = $('#hidden_customer_id').val();
            var product_details = $('#product_id').val();
            if (product_details != '' && product_details != null) {

                var product_details_array = product_details.split('|');

                var index = product_id_container.indexOf(product_details_array[0]);

                var product_id = product_details_array[0];
                var product_name = product_details_array[1];
                var product_price = product_details_array[2];
                var tax_name = product_details_array[3];
                var tax_per = product_details_array[4];
                var tax_id = product_details_array[5];
//                var SOID = product_details_array[6].split(',');

//                if (SOID.length <= 0 || SOID == '' || SOID == ' ' || SOID == undefined || SOID == null) {
//                    swal({
//                        title: "Alert!",
//                        text: "This product has no sale outward.",
//                        confirmButtonText: 'Ok',
//                        confirmButtonColor: '#3c8dbc'
//                    });
//                    $('#product_id').val('').change();
//                    return false;
//                }

//                if (index > -1) {
//                    swal({
//                        title: "Alert!",
//                        text: "Product already ramain in cart.",
//                        confirmButtonText: 'Ok',
//                        confirmButtonColor: '#3c8dbc'
//                    });
//                    $('#product_id').val('').change();
//                    return false;
//                }

                fullScreenLoader();

                $.ajax({
                    type: "POST",
                    url: '<?= base_url('Sale_invoice/getProductInformationByProductId') ?>',
                    data: {product_id: product_id, customer_id: customer_id},
                    success: function (returnData) {
                        HoldOn.close();
//                        console.log(returnData);
                        var data = JSON.parse(returnData);
                        if (data.result == true) {
                            var sale_outward_detail = data.sale_outward_detail;

                            suffix++;
                            var product_row = '';
                            product_row += '<tr class="rowCount" data-id="' + suffix + '" id="row_' + suffix + '">';
                            product_row += '<td style="padding-left: 10px;"><p id="sl_' + suffix + '">' + suffix + '</p></td>';
                            product_row += '<td><span style="padding-bottom: 5px;">' + product_name + '</span></td>';

                            product_row += '<input type="hidden" id="product_id_' + suffix + '" name="product_id[]" value="' + product_id + '"/>';
                            product_row += '<input type="hidden" id="tax_per_' + suffix + '" name="tax_per[]" value="0"/>';
                            product_row += '<input type="hidden" id="tax_per_amt_' + suffix + '" name="tax_per_amt[]" value=""/>';
                            product_row += '<input type="hidden" id="total_tax_' + suffix + '" name="total_tax[]" value=""/>';
                            product_row += '<input type="hidden" id="discount_ind_' + suffix + '" name="discount_ind[]" value=""/>';
                            product_row += '<input type="hidden" id="BasicTotal_' + suffix + '" name="BasicTotal[]" value=""/>';
                            product_row += '<input type="hidden" id="Tax_id_' + suffix + '" name="Tax_id[]" value="' + tax_id + '"/>';

                            product_row += '<input type="hidden" id="total_amt_' + suffix + '" name="total_amt[]" value=""/>';
                            product_row += '<input type="hidden" id="tax_name_' + suffix + '" name="tax_name[]" value="' + tax_name + '"/>';

                            product_row += '<td><input type="text" id="unit_price_' + suffix + '" name="unit_price[]" onfocus="this.select();" class="form-control desimalcheck aligning" placeholder="Unit Price" value="' + product_price + '" onkeyup="return calculateAll();" required /><span class="label_aligning"></span></td>';
                            product_row += '<td><input type="text" data-countID="' + suffix + '" id="quantity_amount_' + suffix + '" name="quantity_amount[]" onfocus="this.select();" class="form-control integerchkforqua aligning countID"  placeholder="Quantity" onkeyup="return calculateAll();" required data-sale_outward_qty="0" onblur="checkStock(' + suffix + ')"></td>';
                            product_row += '<td><input type="text" data-countID="' + suffix + '" id="discount_' + suffix + '" name="discount[]" onfocus="this.select();" class="form-control integerchkforqua aligning countID"  placeholder="Discount" onkeyup="return calculateAll();" ></td>';
//                            product_row += '<td><span id="tax_display_' + suffix + '">' + tax_name + '</span></td>';
//                            product_row += '<td><input type="text" id="tax_amount_' + suffix + '" name="tax_amount[]" class="form-control" placeholder="Tax Amount" readonly /><span class="label_aligning"></span></td>';
                            product_row += '<td><input type="text" id="total_' + suffix + '" name="total[]" class="form-control aligning" placeholder="Total" readonly /><span class="label_aligning"></span></td>';
                            product_row += '<td><button class="btn btn-danger red valid" type="button" value="Delete" onclick="deleter(' + suffix + ',\'' + product_id + '\')" autocomplete="off" aria-invalid="false"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                            product_row += '</tr>';

                            $('#product_cart tbody').append(product_row);
                            $('.select222').css({"width": "100%"}).select2({
                                allowClear: false,
                                placeholder: $(this).data('placeholder') != '' && $(this).data('placeholder') != null ? $(this).data('placeholder') : '-- Select Option --'
                            });
                            product_id_container.push(product_details_array[0]);
                            $('#product_id').val('').change();
                            calculateAll();
                        } else {
                            swal({
                                title: "Alert!",
                                text: "This customer have no sale outward.",
                                confirmButtonText: 'Ok',
                                confirmButtonColor: '#3c8dbc'
                            });
                            $('#product_id').val('').change();
                            return false;
                        }
                    }
                });
            }
        });
    });

    function getSaleOrderData(so_id) {
        fullScreenLoader();
        var prod_id = $('#pomst_id_' + so_id).attr("data-product_id");
        var cus_id = $('#pomst_id_' + so_id).attr("data-customer_id");
        var sale_outward_no = $('#pomst_id_' + so_id).val();

        $.ajax({
            type: "POST",
            url: '<?= base_url('Sale_invoice/getSaleOutwardDataBySOID') ?>',
            data: {product_id: prod_id, customer_id: cus_id, sale_outward_no: sale_outward_no},
            success: function (returnData) {
                HoldOn.close();
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    var Unit_amt = data.sale_outward_detail[0]['UnitAmt'];
                    var tax = data.sale_outward_detail[0]['tax'];
                    var tax_name = data.sale_outward_detail[0]['tax_name'];
                    var sale_outward_qty = data.sale_outward_detail[0]['sale_outward_qty'];
                    if (Unit_amt < 1) {
                        $('#unit_price_' + so_id).val('');
                        $('#quantity_amount_' + so_id).val('');
                        $('#discount_' + so_id).val('');
                    } else {
                        $('#unit_price_' + so_id).val(Unit_amt);
                    }
                    $('#quantity_amount_' + so_id).attr('data-sale_outward_qty', sale_outward_qty);
                    if (tax < 1) {
                        $("#tax_per_" + so_id).val('');
                    } else {
                        $("#tax_per_" + so_id).val(tax);
                    }
                    if (tax_name < 1) {
                        $("#tax_name_" + so_id).html('');
                        $("#tax_display_" + so_id).html('');
                    } else {
                        $("#tax_name_" + so_id).html(tax_name);
                        $("#tax_display_" + so_id).html(tax_name);
                    }
                    calculateAll();
                }
            }
        });
    }

    function deleter(suffix, product_id) {
        swal({
            title: "Alert!",
            text: "Are you sure?",
            confirmButtonColor: '#3c8dbc',
            cancelButtonText: 'cancel',
            confirmButtonText: 'Ok',
            showCancelButton: true
        }, function () {
            $("#row_" + suffix).remove();
            $("#paid").val('');
            var ingredient_id_container_new = [];
            for (var i = 0; i < product_id_container.length; i++) {
                if (product_id_container[i] != product_id) {
                    ingredient_id_container_new.push(product_id_container[i]);
                }
            }
            product_id_container = ingredient_id_container_new;
            calculateAll();
        });
    }

    function calculateAll() {
        var subtotal = 0;
        var i = 1;
        $(".rowCount").each(function () {
            var id = $(this).attr("data-id");
            var unit_price = $("#unit_price_" + id).val();
            var tax = $("#tax_per_" + id).val().split(',');
            var temp = "#sl_" + id;
            var product_id = $("#product_id_" + id).val();
            var quantity = $("#quantity_amount_" + id).val();
            var disc = $("#discount_" + id).val();
            var disc_type = '';
            var temp_amount = 0;
            var total_tax = 0;

            $(temp).html(i);
            i++;
            var quantity_amount = $("#quantity_amount_" + id).val();
            if ($.trim(unit_price) == "" || $.isNumeric(unit_price) == false || unit_price == '' || unit_price == null || unit_price == NaN || unit_price == undefined) {
                unit_price = 0;
            }
            if ($.trim(quantity_amount) == "" || $.isNumeric(quantity_amount) == false || quantity_amount == '' || quantity_amount == null || quantity_amount == NaN || quantity_amount == undefined) {
                quantity_amount = 0;
            }
            if ($.trim(disc) == '' || $.trim(disc) == '%' || $.trim(disc) == '%%' || $.trim(disc) == '%%%' || $.trim(disc) == '%%%%') {
                disc = 0;
            }
            if (disc.length > 0) {
                disc_type = 'percentage';
            }
//            else {
//                disc_type = 'plain';
//            }

            var quantity_amount_and_unit_price = parseFloat($.trim(unit_price)) * parseFloat($.trim(quantity_amount));

            if (disc_type == 'percentage') {
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                temp_amount = quantity_amount_and_unit_price * disc;
                temp_amount = temp_amount / 100;
                $('#discount_ind_' + id).val(temp_amount);
                quantity_amount_and_unit_price = quantity_amount_and_unit_price - temp_amount;
            }
//            else {
//                $('#discount_ind_' + id).val(disc);
//                quantity_amount_and_unit_price = quantity_amount_and_unit_price - disc;
//            }

            if (tax.length > 0) {
                var tax_product = 0;
                tax.forEach(function (key, val) {
                    var tax1 = quantity_amount_and_unit_price;
                    var tax2 = (tax1 * key) / 100;
                    total_tax += tax2;
                    tax_product += tax2;
                });
                quantity_amount_and_unit_price = quantity_amount_and_unit_price + tax_product;
            }

            var BasicTotal = quantity * unit_price;
            $('#BasicTotal_' + id).val(BasicTotal);

            var total_amt = parseFloat(BasicTotal) - parseFloat(temp_amount);
            $('#total_amt_' + id).val(parseFloat(total_amt).toFixed(2));

            $('#total_tax_' + id).val(total_tax);
            $('#tax_amount_' + id).val(total_tax);
            $("#total_" + id).val(quantity_amount_and_unit_price.toFixed(2));
            subtotal += parseFloat($.trim($("#total_" + id).val()));
        });

        if (isNaN(subtotal)) {
            subtotal = 0;
        }
        $("#subtotal").val(subtotal);
        var other = parseFloat($.trim($("#other").val()));
        if ($.trim(other) == "" || $.isNumeric(other) == false) {
            other = 0;
        }

        var fright_charges = $('#fright_charges').val();
        var round_up_amt = $('#round_up_amt').val();

        var grand_total = parseFloat(subtotal) + parseFloat(other) + (fright_charges != '' && fright_charges != null && fright_charges != undefined ? parseFloat(fright_charges) : 0) + parseFloat(round_up_amt != '' && round_up_amt != null && round_up_amt != undefined ? parseFloat(round_up_amt) : 0);
        grand_total = grand_total.toFixed(2);
        $("#g_total").val(grand_total);

        var paid = $("#paid").val();
        if ($.trim(paid) == "" || $.isNumeric(paid) == false) {
            paid = 0;
        }

        var due = parseFloat(grand_total) - parseFloat(paid);

        $("#due").val(due.toFixed(2));
    }

    function updateRowNo() {
        var numRows = $("#product_cart tbody tr").length;
        for (var r = 0; r < numRows; r++) {
            $("#product_cart tbody tr").eq(r).find("td:first p").text(r + 1);
        }
    }

    $(document).on('change', '#supplier_id', function () {
        var address = $(this).find(':selected').attr('data-address');
        if (address != '' && address != undefined) {
            $('#supplier_address').text(address);
            $('#hidden_supplier_address').val(address);
        } else {
            $('#supplier_address').text('');
            $('#hidden_supplier_address').val('');
        }
    });

    function checkStock(suffix) {
        var quantity_amount = $('#quantity_amount_' + suffix).val();
        var quantity_sale_order = $('#quantity_amount_' + suffix).attr('data-sale_outward_qty');
        quantity_amount = parseFloat(quantity_amount);
        quantity_sale_order = parseFloat(quantity_sale_order);

        if ($.trim(quantity_amount) == "" || quantity_amount == undefined || isNaN(quantity_amount)) {
            quantity_amount = 0;
        }
        if ($.trim(quantity_sale_order) == "" || quantity_sale_order == undefined || isNaN(quantity_sale_order)) {
            quantity_sale_order = 0;
        }

        if (quantity_amount <= 0) {
            $('#quantity_amount_' + suffix).val(null);
            calculateAll();
            swal({
                title: "Alert!",
                text: "Quantity must be more than 0.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
            return false;
        }

        var stored_qty = $('#stored_qty_td_' + suffix).text();
        var stored_qty_val = stored_qty != '' && stored_qty != null ? stored_qty.split(' ')[0] : null;
//        console.log(stored_qty_val);

        if (stored_qty_val != '' && stored_qty_val != ' ' && stored_qty_val != null) {
            if (quantity_amount > parseFloat(stored_qty_val)) {
                $('#quantity_amount_' + suffix).val(null);
                calculateAll();
                swal({
                    title: "Alert!",
                    text: "Quantity must be less than or equal to stored qty.",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
                return false;
            }
        }
    }

    $(document).on('change', '#customer_id', function () {
        fullScreenLoader();
        var same_as_billing = $('#sameasbilling').prop("checked");

        $('#customer_all_detail').html('');
        var customer_all_detail = '<label for="customer_detail" class="col-form-label" >Billing Address<a style="margin-left: 11px; color: black;" id="edit_billing_address_btn" href="javascript:void(0);"><i class="fa fa-pencil" style="font-size: 18px;" title="Edit"></i></a></label><br>';
        var customer_name = $(this).find(':selected').attr('data-name');
        var address = $(this).find(':selected').attr('data-address');
        var phone_number = $(this).find(':selected').attr('data-phonenumber');
        var email_address = $(this).find(':selected').attr('data-email');

        if (address != '' && address != undefined) {
            customer_all_detail += '<div id="customer_address">';
            customer_all_detail += '<textarea class="form-control" style="resize:vertical;" rows="3" name="hidden_customer_address" id="hidden_customer_address" readonly>' + address + '</textarea>';
            customer_all_detail += '</div>';
//            customer_all_detail += '<input type="hidden" value="' + address + '" name="hidden_customer_address" id="hidden_customer_address">';
            $('#hidden_customer_address').val(address);

            if (same_as_billing == true) {
                $('#si_address_1').val(address);
            }
            $('#bi_address_1').val(address);

        } else {
            if (same_as_billing == true) {
                $('#si_address_1').val('');
            }
            $('#customer_address').text('');
            $('#bi_address_1').val('');
            $('#hidden_customer_address').val('');
        }

        if (phone_number != '' && phone_number != undefined) {
//            customer_all_detail += '<span id="customer_phonenumber"><b>Phone : </b>' + phone_number + '</span><br>';
            customer_all_detail += '<input type="hidden" value="' + phone_number + '" name="hidden_customer_phonenumber" id="hidden_customer_phonenumber">';
            $('#hidden_customer_phonenumber').val(phone_number);

            if (same_as_billing == true) {
                $('#si_phone').val(phone_number);
            }
            $('#bi_phone').val(phone_number);
        } else {
            if (same_as_billing == true) {
                $('#si_phone').val('');
            }
            $('#customer_phonenumber').text('');
            $('#bi_phone').val('');
            $('#hidden_customer_phonenumber').val('');
        }

        if (email_address != '' && email_address != undefined) {
//            customer_all_detail += '<span id="customer_email"><b>Email : </b>' + email_address + '</span><br>';
            customer_all_detail += '<input type="hidden" value="' + email_address + '" name="hidden_customer_email" id="hidden_customer_email">';
            $('#hidden_customer_email').val(email_address);
        } else {
            $('#customer_email').text('');
            $('#hidden_customer_email').val('');
        }

        if (customer_name != '' && customer_name != undefined) {
            if (same_as_billing == true) {
                $('#si_customer_name').val(customer_name);
            }
            $('#bi_customer_name').val(customer_name);
        } else {
            if (same_as_billing == true) {
                $('#si_customer_name').val('');
            }
            $('#bi_customer_name').val('');
        }
        HoldOn.close();
        $('#customer_all_detail').append(customer_all_detail);
    });

    $(document).on('change', '#agent_id', function () {
        fullScreenLoader();

        let agent_id = $(this).find(':selected').val();
        var same_as_billing = $('#sameasbilling').prop("checked");

        $('#agent_all_detail').html('');

        if (agent_id != '' && agent_id != null) {
            var agent_all_detail = '<label for="agent_detail" class="col-form-label" >Agent Detail</label><br>';
            var agent_name = $(this).find(':selected').attr('data-name');
            var agent_address = $(this).find(':selected').attr('data-address');
            var agent_phone_number = $(this).find(':selected').attr('data-phonenumber');
            var agent_email_address = $(this).find(':selected').attr('data-email');

            if (agent_address != '' && agent_address != undefined) {
                agent_all_detail += '<span id="agent_address"><b>Address : </b>' + agent_address + '</span><br>';
                agent_all_detail += '<input type="hidden" value="' + agent_address + '" name="hidden_agent_address" id="hidden_agent_address">';
                $('#hidden_agent_address').val(agent_address);

                if (same_as_billing == true) {
                    $('#si_address_1').val(agent_address);
                }
                $('#bi_address_1').val(agent_address);

            } else {
                if (same_as_billing == true) {
                    $('#si_address_1').val('');
                }
                $('#agent_address').text('');
                $('#bi_address_1').val('');
                $('#hidden_agent_address').val('');
            }

            if (agent_phone_number != '' && agent_phone_number != undefined) {
                agent_all_detail += '<span id="agent_phonenumber"><b>Phone : </b>' + agent_phone_number + '</span><br>';
                agent_all_detail += '<input type="hidden" value="' + agent_phone_number + '" name="hidden_agent_phonenumber" id="hidden_agent_phonenumber">';
                $('#hidden_agent_phonenumber').val(agent_phone_number);

                if (same_as_billing == true) {
                    $('#si_phone').val(agent_phone_number);
                }
                $('#bi_phone').val(agent_phone_number);
            } else {
                if (same_as_billing == true) {
                    $('#si_phone').val('');
                }
                $('#agent_phonenumber').text('');
                $('#bi_phone').val('');
                $('#hidden_agent_phonenumber').val('');
            }

            if (agent_email_address != '' && agent_email_address != undefined) {
                agent_all_detail += '<span id="agent_email"><b>Email : </b>' + agent_email_address + '</span><br>';
                agent_all_detail += '<input type="hidden" value="' + agent_email_address + '" name="hidden_agent_email" id="hidden_agent_email">';
                $('#hidden_agent_email').val(agent_email_address);
            } else {
                $('#agent_email').text('');
                $('#hidden_agent_email').val('');
            }

            if (agent_name != '' && agent_name != undefined) {
                if (same_as_billing == true) {
                    $('#si_agent_name').val(agent_name);
                }
                $('#bi_agent_name').val(agent_name);
            } else {
                if (same_as_billing == true) {
                    $('#si_agent_name').val('');
                }
                $('#bi_agent_name').val('');
            }
            $('#agent_all_detail').append(agent_all_detail);
            $('#commission_rate_id').prop('required', true);
            $('#commission_rate_type').prop('required', true);
            $('#commission_rate').prop('required', true);
            $('#commission_div').show();
        } else {
            $('#commission_rate_id').prop('required', false);
            $('#commission_rate_type').prop('required', false);
            $('#commission_rate').prop('required', false);
            $('#commission_rate_id').val('').change();
            $('#commission_rate_type').val('').change();
            $('#commission_rate').val(null);
            $('#commission_div').hide();
        }
        HoldOn.close();
    });


    $(document).ready(function () {
        $('.select2222').select2();

        $('#update_non_gst_sale_invoice').validate({
            errorPlacement: function (error, element) {
                if (element.is("select")) {
                    error.appendTo(element.parent());
                } else if (element.data('type') == "date") {
                    error.appendTo(element.parent().parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                sale_invoice_no: {
                    remote: {
                        url: "<?= base_url('Sale_invoice/checkSaleInvoiceNo') ?>",
                        type: "POST",
                        data: {
                            sale_invoice_id: function () {
                                return $("#sale_invoice_id").val();
                            }
                        }
                    }
                }
            },
            messages: {
                sale_invoice_no: {
                    remote: "Sale invoice number is already in use!"
                }
            },
            submitHandler: function (form) {
                if ($('.rowCount').length == 0) {
                    swal('Alert!', 'Add at least one product.', 'warning');
                    return false;
                }
                form.submit();
            }
        });

        $('.select2222').select2();

        $("#invoice_date_div").datetimepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayBtn: true,
            startView: "month",
            minView: "month",
            pickerPosition: "bottom-left"
        });

        $("#due_date_div").datetimepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayBtn: true,
            startView: "month",
            minView: "month",
            pickerPosition: "bottom-left"
        });

        $("#delivery_date_div").datetimepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayBtn: true,
            startView: "month",
            minView: "month",
            pickerPosition: "bottom-left"
        });

        $(document).on('change', '.discount_percentage', function () {
            let qty = parseFloat($(this).val());
            if (qty > 100) {
                $(this).val(null);
                swal({
                    title: "Alert!",
                    text: "Discount must be less than or equal to 100%.",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
            } else if (qty <= 0) {
                $(this).val(null);
                swal({
                    title: "Alert!",
                    text: "Discount must be more than 0%.",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
            } else {
//                alert('ok');
            }
        });

        $(document).on('change', '.sup_no', function () {
            let dataId = $(this).data('id');
            let supplier_id = $(this).find(':selected').val();
            let product_id = $('#product_id_' + dataId).val();
            if (supplier_id != '' && supplier_id != null) {
                $("#lot_no_" + dataId).empty();
                $("#lot_no_" + dataId).css({"width": "100%"}).select2({
                    placeholder: $(this).data('placeholder') != '' && $(this).data('placeholder') != null ? $(this).data('placeholder') : '-- Select Option --'
                });
                $("#lot_no_" + dataId).val([]).trigger("change");

                $("#storage_tank_" + dataId).empty();
                $("#storage_tank_" + dataId).css({"width": "100%"}).select2({
                    placeholder: $(this).data('placeholder') != '' && $(this).data('placeholder') != null ? $(this).data('placeholder') : '-- Select Option --'
                });
                $("#storage_tank_" + dataId).val([]).trigger("change");

                $('#stored_qty_td_' + dataId).text('');

                $.ajax({
                    method: 'POST',
                    url: '<?= base_url('Sale_invoice/getProductionBatchNoBySupplier') ?>',
                    data: {supplier_id: supplier_id, product_id: product_id},
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (data.status == true) {

                            var option = data.lot_no_options;
                            $("#lot_no_" + dataId).append(option);

                            $("#lot_no_" + dataId).css({"width": "100%"}).select2({
                                placeholder: $(this).data('placeholder') != '' && $(this).data('placeholder') != null ? $(this).data('placeholder') : '-- Select option --'
                            });

                            $("#lot_no_" + dataId).val([]).trigger("change");
                        } else {
                            swal('Oops...!', data.message, 'error');
                        }
                    }
                });
            }
        });

        $(document).on('change', '.lot_no', function () {
            let dataId = $(this).data('id');
            let production_id = $(this).val();
            let supplier_id = $('#sup_no_' + dataId).find(':selected').val();
            let product_id = $('#product_id_' + dataId).val();

            if (production_id != '' && production_id != null) {
                $("#storage_tank_" + dataId).empty();
                $("#storage_tank_" + dataId).css({"width": "100%"}).select2({
                    placeholder: $(this).data('placeholder') != '' && $(this).data('placeholder') != null ? $(this).data('placeholder') : 'Select option...'
                });
                $("#storage_tank_" + dataId).val([]).trigger("change");

                $('#stored_qty_td_' + dataId).text('');

                $.ajax({
                    method: 'POST',
                    url: '<?= base_url('Sale_invoice/getStorageTankDataByProductionId') ?>',
                    data: {supplier_id: supplier_id, product_id: product_id, production_id: production_id},
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (data.status == true) {

                            var option = data.storage_options;
                            $("#storage_tank_" + dataId).append(option);

                            $("#storage_tank_" + dataId).css({"width": "100%"}).select2({
                                placeholder: $(this).data('placeholder') != '' && $(this).data('placeholder') != null ? $(this).data('placeholder') : '-- Select option --'
                            });

                            $("#storage_tank_" + dataId).val([]).trigger("change");
                        } else {
                            swal('Oops...!', data.message, 'error');
                        }
                    }
                });
            }
        });

        var cnt = 0;

        $(document).on('change', '.storage_tank', function () {
            let dataId = $(this).data('id');
            let product_id = $('#product_id_' + dataId).val();
            let supplier_id = $('#sup_no_' + dataId).find(':selected').val();
            let production_id = $('#lot_no_' + dataId).find(':selected').val();
            let storage_tank_id = $(this).find(":selected").val();

            if (storage_tank_id != '' && storage_tank_id != null) {
                $.ajax({
                    method: 'POST',
                    url: '<?= base_url('Sale_invoice/getProductionStorageDataByStorageId') ?>',
                    data: {supplier_id: supplier_id, product_id: product_id, production_id: production_id, storage_tank_id: storage_tank_id},
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (data.status == true) {
                            $('#stored_qty_td_' + dataId).text(parseFloat(data.stock_data.stock).toFixed(2));
                        } else {
                            swal('Oops...!', data.message, 'error');
                        }
                    }
                });
            }
        });

        $(document).on('click', '#edit_billing_address_btn', function (e) {
            e.preventDefault();
            let address = $('#hidden_customer_address').text();
            if (address != '' && address != null) {
                $("#hidden_customer_address").attr("readonly", false);
            } else {
                $("#hidden_customer_address").attr("readonly", true);
            }
        });

        $(document).on('change', '#commission_rate_id', function () {
            $('#commission_rate_type').val('').change();
            $('#commission_rate').val(null);
        });

        $(document).on('change', '#commission_rate_type', function () {
            let commision_id = $('#commission_rate_id').find(':selected').val();
            let rate_type = $(this).val();

            if (rate_type != '' && rate_type != null) {
                if (commision_id == '' || commision_id == null) {
                    swal('Oops...!', 'Please select commission type first!', 'warning');
                    $(this).val('').change();
                    $('#commission_rate').val(null);
                    return false;
                }

                if (rate_type == '' || rate_type == null) {
                    swal('Oops...!', 'Please select rate type!', 'warning');
                    $('#commission_rate').val(null);
                    return false;
                }

                if (commision_id != '' && commision_id != null && rate_type != '' && rate_type != null) {
                    $.ajax({
                        method: 'POST',
                        url: '<?= base_url('Sale_invoice/getAgentCommisionRate') ?>',
                        data: {commision_id: commision_id, rate_type: rate_type},
                        dataType: 'json',
                        success: function (data, textStatus, jqXHR) {
                            if (data.status == true) {
                                $('#commission_rate').val(data.rate);
                            } else {
                                swal('Oops...!', data.message, 'error');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal('Oops...!', 'Something went wrong, Try again!', 'error');
                        }
                    });
                }
            }
        });
    });

</script>



