<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Update Sale Invoice
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Sale_invoice">Manage Sale Invoice</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Sale_invoice/updateSaleInvoice/' . $SIID, array('class' => '', 'id' => 'update_sale_invoice', 'name' => 'update_sale_invoice')) ?>
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-8 mb-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="sale_invoice_no" class="form-label">Invoice No. <i class="text-danger">*</i></label>
                                            <input type="text" class="form-control" id="sale_invoice_no" name="sale_invoice_no" value="<?= isset($sale_invoice['SINO']) && !empty($sale_invoice['SINO']) ? $sale_invoice['SINO'] : null ?>" required="">
                                            <input type="hidden" class="form-control" id="sale_invoice_id" name="sale_invoice_id" value="<?= isset($SIID) && !empty($SIID) ? $SIID : null ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="date" class="form-label">Invoice Date <i class="text-danger">*</i></label>
                                            <div class="input-group date mb-3" id="invoice_date_div">
                                                <input type="text" class="form-control datepicker" id="invoice_date" name="invoice_date" data-type="date" value="<?= isset($sale_invoice['DateTime']) && !empty($sale_invoice['DateTime']) ? date('d-m-Y', strtotime($sale_invoice['DateTime'])) : null ?>" required>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div style="display: flex;gap: 10px;">
                                            <div class="form-group" style="flex-grow: 1;">
                                                <label for="customer_id" class="form-label">Customer <i class="text-danger">*</i></label>
                                                <select name="customer_id" class="form-control select222" id="customer_id" data-placeholder="-- Select customer --" required="">
                                                    <option value="" data-address=""> Select Customer</option>
                                                    <?php
                                                    if (isset($customer_list) && !empty($customer_list)) {
                                                        foreach ($customer_list as $key => $value) {
                                                            ?>
                                                            <option value="<?= $value['customer_id'] ?>" <?= isset($sale_invoice['RefCustomerID']) && !empty($sale_invoice['RefCustomerID']) && $sale_invoice['RefCustomerID'] == $value['customer_id'] ? 'selected' : null ?> data-name="<?= isset($value['company__name']) && !empty($value['company__name']) ? $value['company__name'] : null ?>" data-address="<?= (isset($value['customer_address']) && !empty($value['customer_address']) ? $value['customer_address'] : null) . (isset($value['customer_city']) && !empty($value['customer_city']) ? ', ' . $value['customer_city'] : null) . (isset($value['state_name']) && !empty($value['state_name']) ? ', ' . $value['state_name'] : null) . (isset($value['customer_country']) && !empty($value['customer_country']) ? ', ' . $value['customer_country'] : null) . (isset($value['customer_pincode']) && !empty($value['customer_pincode']) ? ' - ' . $value['customer_pincode'] : (isset($value['customer_address']) && !empty($value['customer_address']) ? '.' : null)) ?>" data-phonenumber="<?= $value['customer_mobile'] ?>" data-email="<?= $value['customer_email'] ?>"><?= $value['company__name'] ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="customer_all_detail">
                                            <label for="customer_detail" class="form-label">Customer Address<a style="margin-left: 11px;" class="btn btn-primary btn-icon btn-sm" id="edit_billing_address_btn" href="javascript:void(0);" data-toggle="tooltip" data-template="<div class=&quot;tooltip&quot; role=&quot;tooltip&quot;><div class=&quot;tooltip-inner bg-primary-500&quot;></div></div>" title="Edit Address"><i class="fa fa-pencil"></i></a></label><br>
                                            <textarea class="form-control" style="resize:vertical;" rows="3" name="hidden_customer_address" id="hidden_customer_address" readonly=""><?= isset($sale_invoice['RefCustomerAddress']) && !empty($sale_invoice['RefCustomerAddress']) ? $sale_invoice['RefCustomerAddress'] : '' ?></textarea>
                                            <input type="hidden" name="hidden_customer_phonenumber" id="hidden_customer_phonenumber" value="<?= isset($sale_invoice['RefCustomerPhone']) && !empty($sale_invoice['RefCustomerPhone']) ? $sale_invoice['RefCustomerPhone'] : '' ?>">
                                            <input type="hidden" name="hidden_customer_email" id="hidden_customer_email" value="<?= isset($sale_invoice['RefCustomerEmail']) && !empty($sale_invoice['RefCustomerEmail']) ? $sale_invoice['RefCustomerEmail'] : '' ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label for="sale_invoice_remark" class="form-label">Invoice Description</label>
                                    <textarea rows="7" class="form-control" id="sale_invoice_remark" name="sale_invoice_remark" placeholder="Write about invoice here..."><?= isset($sale_invoice['Remarks']) && !empty($sale_invoice['Remarks']) ? $sale_invoice['Remarks'] : null ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-6 mb-3">
                                <div class="form-group">
                                    <label for="products" class="form-label">Products <i class="text-danger">*</i></label>
                                    <select name="product[]" class="form-control select222" id="product_id" data-placeholder="-- Select product --">
                                        <option value=""> Select Product</option>
                                        <?php
                                        if (isset($product_list) && !empty($product_list)) {
                                            foreach ($product_list as $key3 => $value3) {
                                                ?>
                                                <option value="<?php echo $value3['id'] . "|" . $value3['product_name'] . "|" . $value3['price'] . "|" . $value3['tax_name'] . "|" . $value3['tax'] . "|" . $value3['tax_id'] ?> "><?= $value3['product_name'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 mb-3">
                                <div class="form-group">
                                    <label for="product_barcode" class="form-label">Product Barcode</label>
                                    <input type="text" class="form-control digitonly" id="product_barcode" name="product_barcode" placeholder="Product Barcode"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive mb-4" id="product_cart">          
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th style="min-width: 40px;">SN</th>
                                                <th style="min-width: 260px;">Products</th>
                                                <th style="min-width: 130px;">Product Price</th>
                                                <th style="min-width: 130px;">Quantity</th>
                                                <th style="min-width: 130px">Discount (%)</th>
                                                <th style="min-width: 100px;">Tax</th>
                                                <th style="min-width: 130px;">Tax Amount</th>
                                                <th style="min-width: 150px;">Total</th>
                                                <th style="min-width: 70px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($invoice_sale_list) && $invoice_sale_list != '' && isset($invoice_sale_list)) {
                                                foreach ($invoice_sale_list as $k => $product_value) {
                                                    $k++;
                                                    $product_row = '';
                                                    $product_row .= '<tr class="text-nowrap rowCount" data-id="' . $k . '" id="row_' . $k . '">';
                                                    $product_row .= '<td style="padding-left: 10px;"><p id="sl_' . $k . '">' . $k . '</p></td>';
                                                    $product_row .= '<td><span style="padding-bottom: 5px;">' . getProductNameById($product_value->refProductID) . '</span></td>';

                                                    $product_row .= '<input type="hidden" id="product_id_' . $k . '" name="product_id[]" value="' . $product_value->refProductID . '"/>';
                                                    $product_row .= '<input type="hidden" id="tax_per_' . $k . '" name="tax_per[]" value="' . $product_value->tax_per . '"/>';
                                                    $product_row .= '<input type="hidden" id="tax_per_amt_' . $k . '" name="tax_per_amt[]" value=""/>';
                                                    $product_row .= '<input type="hidden" id="total_tax_' . $k . '" name="total_tax[]" value="' . $product_value->TaxAmt . '"/>';
                                                    $product_row .= '<input type="hidden" id="discount_ind_' . $k . '" name="discount_ind[]" value="' . $product_value->DiscountAmt . '"/>';
                                                    $product_row .= '<input type="hidden" id="BasicTotal_' . $k . '" name="BasicTotal[]" value="' . $product_value->UnitAmt * $product_value->Qty . '"/>';
                                                    $product_row .= '<input type="hidden" id="Tax_id_' . $k . '" name="Tax_id[]" value="' . $product_value->Tax_id . '"/>';

                                                    $product_row .= '<input type="hidden" id="total_amt_' . $k . '" name="total_amt[]" value="' . $product_value->TotalAmt . '"/>';
                                                    $product_row .= '<input type="hidden" id="tax_name_' . $k . '" name="tax_name[]" value="' . $product_value->tax_name . '"/>';

                                                    $product_row .= '<td><input type="text" id="unit_price_' . $k . '" name="unit_price[]" onfocus="this.select();" class="form-control desimalcheck aligning" placeholder="Unit Price" value="' . $product_value->UnitAmt . '" onkeyup="return calculateAll();" required/><span class="label_aligning"></span></td>';
                                                    $product_row .= '<td><input type="text" data-countID="' . $k . '" id="quantity_amount_' . $k . '" name="quantity_amount[]" onfocus="this.select();" class="form-control integerchkforqua aligning countID"  placeholder="Quantity" onkeyup="return calculateAll();" value="' . $product_value->Qty . '" required data-sale_outward_qty="" onblur="checkStock(' . $k . ')"></td>';
                                                    $product_row .= '<td><input type="text" data-countID="' . $k . '" id="discount_' . $k . '" name="discount[]" onfocus="this.select();" class="form-control integerchkforqua aligning countID" value="' . $product_value->DiscountPer . '" placeholder="Discount in percentage" onkeyup="return calculateAll();" ></td>';
                                                    $product_row .= '<td><span id="tax_display_' . $k . '">' . $product_value->tax_per . '%</span></td>';
                                                    $product_row .= '<td><input type="text" id="tax_amount_' . $k . '" name="tax_amount[]" class="form-control" value="' . $product_value->TaxAmt . '" placeholder="Tax Amount" readonly /><span class="label_aligning"></span></td>';

                                                    $total = floatval($product_value->TotalAmt) + floatval($product_value->TaxAmt);
                                                    $product_row .= '<td><input type="text" id="total_' . $k . '" name="total[]" class="form-control aligning" value="' . number_format(floatval($total), 2) . '" placeholder="Total" readonly /><span class="label_aligning"></span></td>';
                                                    $product_row .= '<td><button class="btn btn-danger red valid" type="button" value="Delete" onclick="deleter(' . $k . ', \'' . $product_value->refProductID . '\')" autocomplete="off" aria-invalid="false"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                                                    $product_row .= '</tr>';
                                                    echo $product_row;
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div> 
                                <div class="row mb-3">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group">
                                            <label for="fright_charges" class="form-label">Fright Charges</label>
                                            <input type="text" class="form-control desimalcheck" id="fright_charges" name="fright_charges" placeholder="Fright Charges" onkeyup="calculateAll();" value="<?= isset($sale_invoice['FrightCharges']) && !empty($sale_invoice['FrightCharges']) ? $sale_invoice['FrightCharges'] : null ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-9"></div>
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group">
                                            <label for="round_up_amt" class="form-label">Round Up</label>
                                            <input type="text" class="form-control desimalcheck" id="round_up_amt" name="round_up_amt" placeholder="Round Up Amount" onkeyup="calculateAll();" value="<?= isset($sale_invoice['RoundUpAmt']) && !empty($sale_invoice['RoundUpAmt']) ? $sale_invoice['RoundUpAmt'] : null ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-9"></div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="g_total" class="form-label">Grand Total</label>
                                            <input type="text" class="form-control" id="g_total" name="g_total" placeholder="Grand Total" value="<?= $sale_invoice['GrandTotal'] ?>" readonly />
                                            <input type="hidden" class="payment_status" id="payment_status" name="payment_status" value="<?= $sale_invoice['is_paid'] ?>">
                                            <input type="hidden" class="form-control" id="transaction_id" name="transaction_id" value="<?= isset($payment_data['transaction_id']) && !empty($payment_data['transaction_id']) ? $payment_data['transaction_id'] : null ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-md-row flex-column justify-content-end mt-2" style="gap: 8px;">
                            <button type="submit" class="btn btn-danger waves-effect waves-themed" name="add-purchase-invoice" value="Save"><span class="fal fa-check mr-1"></span>Save</button>
                            <button type="submit" class="btn btn-warning waves-effect waves-themed" name="add-purchase-invoice-another" value="Save & Add Another"><span class="fal fa-check mr-1"></span>Save & Add Another</button>
                            <a href="<?= base_url() ?>admin/Purchase_invoice"  class="btn btn-secondary btn-large">Cancel</a>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary mr-2",
            cancelButton: "btn btn-danger"
        },
    });

    $(function () {
        var inputs = $('input, select, textarea, a').not(':input[type=button],:input[type=submit], :input[type=reset], :input[type=password]');
        $(inputs).bind('keydown', function (e) {
            var self = $(this), form = self.parents('form:eq(0)'), focusable, next;
            if (e.keyCode == 13) {
                focusable = form.find('input, select, textarea, a').not(':input[type=button], :input[type=reset], :input[type=password]').filter(':visible');
//                next = focusable.eq(focusable.index(this) + 1);
//                next.focus();
                self.focus();
                e.preventDefault();
                return false;
            }
        });
    });

    var company_code = '<?= $company_code ?>';

<?php
$ingredient_id_container = "[";
if ($invoice_sale_list && !empty($invoice_sale_list)) {
    foreach ($invoice_sale_list as $pi) {
        $ingredient_id_container .= '"' . $pi->refProductID . '",';
    }
}
$ingredient_id_container = substr($ingredient_id_container, 0, -1);
$ingredient_id_container .= "]";
?>
    var product_id_container = <?= $ingredient_id_container ?>;

    var suffix =
<?php
if (isset($invoice_sale_list)) {
    echo count($invoice_sale_list);
} else {
    echo 0;
}
?>;

    $(function () {
        var supp_selcted_or_not = 1;
        $(document).on('change', '#product_id', function () {
            var customer_id = $('#hidden_customer_id').val();
            var product_details = $('#product_id').val();

            if (product_details != '' && product_details != null) {
                var product_details_array = product_details.split('|');
                var index = product_id_container.indexOf(product_details_array[0]);
                var product_id = product_details_array[0];
                var product_name = product_details_array[1];
                var product_price = product_details_array[2];
                var tax_name = product_details_array[3];
                var tax_per = product_details_array[4];
                var tax_id = product_details_array[5];

                if (index > -1) {
                    swalWithBootstrapButtons.fire({
                        icon: 'warning',
                        title: "Alert!",
                        text: "Product already ramain in cart.",
                        confirmButtonText: 'Ok',
                        confirmButtonColor: '#3c8dbc'
                    });
                    $('#product_id').val('').change();
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url: '<?= base_url('admin/Sale_invoice/getProductInformationByProductId') ?>',
                    data: {product_id: product_id, customer_id: customer_id},
                    success: function (returnData) {

                        var data = JSON.parse(returnData);
                        if (data.result == true) {
                            var sale_outward_detail = data.sale_outward_detail;

                            suffix++;
                            var product_row = '';
                            product_row += '<tr class="rowCount" data-id="' + suffix + '" id="row_' + suffix + '">';
                            product_row += '<td style="padding-left: 10px;"><p id="sl_' + suffix + '">' + suffix + '</p></td>';
                            product_row += '<td><span style="padding-bottom: 5px;">' + product_name + '</span></td>';

                            product_row += '<input type="hidden" id="product_id_' + suffix + '" name="product_id[]" value="' + product_id + '"/>';
                            product_row += '<input type="hidden" id="tax_per_' + suffix + '" name="tax_per[]" value="' + tax_per + '"/>';
                            product_row += '<input type="hidden" id="tax_per_amt_' + suffix + '" name="tax_per_amt[]" value=""/>';
                            product_row += '<input type="hidden" id="total_tax_' + suffix + '" name="total_tax[]" value=""/>';
                            product_row += '<input type="hidden" id="discount_ind_' + suffix + '" name="discount_ind[]" value=""/>';
                            product_row += '<input type="hidden" id="BasicTotal_' + suffix + '" name="BasicTotal[]" value=""/>';
                            product_row += '<input type="hidden" id="Tax_id_' + suffix + '" name="Tax_id[]" value="' + tax_id + '"/>';

                            product_row += '<input type="hidden" id="total_amt_' + suffix + '" name="total_amt[]" value=""/>';
                            product_row += '<input type="hidden" id="tax_name_' + suffix + '" name="tax_name[]" value="' + tax_name + '"/>';

                            product_row += '<td><input type="text" id="unit_price_' + suffix + '" name="unit_price[]" onfocus="this.select();" class="form-control desimalcheck aligning" placeholder="Unit Price" value="' + product_price + '" onkeyup="return calculateAll();" required /><span class="label_aligning"></span></td>';
                            product_row += '<td><input type="text" data-countID="' + suffix + '" id="quantity_amount_' + suffix + '" name="quantity_amount[]" value="1" onfocus="this.select();" class="form-control integerchkforqua aligning countID"  placeholder="Quantity" onkeyup="return calculateAll();" required data-sale_outward_qty="0" onblur="checkStock(' + suffix + ')"></td>';
                            product_row += '<td><input type="text" data-countID="' + suffix + '" id="discount_' + suffix + '" name="discount[]" onfocus="this.select();" class="form-control integerchkforqua aligning countID"  placeholder="Discount in percentage" onkeyup="return calculateAll();" ></td>';
                            product_row += '<td><span id="tax_display_' + suffix + '">' + tax_per + '%</span></td>';
                            product_row += '<td><input type="text" id="tax_amount_' + suffix + '" name="tax_amount[]" class="form-control" placeholder="Tax Amount" readonly /><span class="label_aligning"></span></td>';
                            product_row += '<td><input type="text" id="total_' + suffix + '" name="total[]" class="form-control aligning" placeholder="Total" readonly /><span class="label_aligning"></span></td>';
                            product_row += '<td><button class="btn btn-danger red valid" type="button" value="Delete" onclick="deleter(' + suffix + ',\'' + product_id + '\')" autocomplete="off" aria-invalid="false"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                            product_row += '</tr>';

                            $('#product_cart tbody').append(product_row);
                            $('.select222').css({"width": "100%"}).select2({
                                allowClear: false,
                                placeholder: $(this).data('placeholder') != '' && $(this).data('placeholder') != null ? $(this).data('placeholder') : '-- Select Option --'
                            });
                            product_id_container.push(product_details_array[0]);
                            $('#product_id').val('').change();
                            calculateAll();
                        } else {
                            swalWithBootstrapButtons.fire({
                                icon: 'warning',
                                title: "Alert!",
                                text: "This customer have no sale outward.",
                                confirmButtonText: 'Ok',
                                confirmButtonColor: '#3c8dbc'
                            });
                            $('#product_id').val('').change();
                            return false;
                        }
                    }
                });
            }
        });
    });

    function getSaleOrderData(so_id) {

        var prod_id = $('#pomst_id_' + so_id).attr("data-product_id");
        var cus_id = $('#pomst_id_' + so_id).attr("data-customer_id");
        var sale_outward_no = $('#pomst_id_' + so_id).val();

        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Sale_invoice/getSaleOutwardDataBySOID') ?>',
            data: {product_id: prod_id, customer_id: cus_id, sale_outward_no: sale_outward_no},
            success: function (returnData) {

                var data = JSON.parse(returnData);
                if (data.result == true) {
                    var Unit_amt = data.sale_outward_detail[0]['UnitAmt'];
                    var tax = data.sale_outward_detail[0]['tax'];
                    var tax_name = data.sale_outward_detail[0]['tax_name'];
                    var sale_outward_qty = data.sale_outward_detail[0]['sale_outward_qty'];
                    if (Unit_amt < 1) {
                        $('#unit_price_' + so_id).val('');
                        $('#quantity_amount_' + so_id).val('');
                        $('#discount_' + so_id).val('');
                    } else {
                        $('#unit_price_' + so_id).val(Unit_amt);
                    }
                    $('#quantity_amount_' + so_id).attr('data-sale_outward_qty', sale_outward_qty);
                    if (tax < 1) {
                        $("#tax_per_" + so_id).val('');
                    } else {
                        $("#tax_per_" + so_id).val(tax);
                    }
                    if (tax_name < 1) {
                        $("#tax_name_" + so_id).html('');
                        $("#tax_display_" + so_id).html('');
                    } else {
                        $("#tax_name_" + so_id).html(tax_name);
                        $("#tax_display_" + so_id).html(tax_name);
                    }
                    calculateAll();
                }
            }
        });
    }

    function deleter(suffix, product_id) {
        swalWithBootstrapButtons.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then((result) => {
            if (result.value) {
                $("#row_" + suffix).remove();
                $("#paid").val('');
                var ingredient_id_container_new = [];
                for (var i = 0; i < product_id_container.length; i++) {
                    if (product_id_container[i] != product_id) {
                        ingredient_id_container_new.push(product_id_container[i]);
                    }
                }
                product_id_container = ingredient_id_container_new;
                calculateAll();
            }
        });
    }

    function calculateAll() {
        var subtotal = 0;
        var i = 1;
        $(".rowCount").each(function () {
            var id = $(this).attr("data-id");
            var unit_price = $("#unit_price_" + id).val();
            var tax = $("#tax_per_" + id).val().split(',');
            var temp = "#sl_" + id;
            var product_id = $("#product_id_" + id).val();
            var quantity = $("#quantity_amount_" + id).val();
            var disc = $("#discount_" + id).val();
            console.log(disc);
            var disc_type = '';
            var temp_amount = 0;
            var total_tax = 0;

            $(temp).html(i);
            i++;
            var quantity_amount = $("#quantity_amount_" + id).val();
            if ($.trim(unit_price) == "" || $.isNumeric(unit_price) == false || unit_price == '' || unit_price == null || unit_price == NaN || unit_price == undefined) {
                unit_price = 0;
            }
            if ($.trim(quantity_amount) == "" || $.isNumeric(quantity_amount) == false || quantity_amount == '' || quantity_amount == null || quantity_amount == NaN || quantity_amount == undefined) {
                quantity_amount = 0;
            }
            if ($.trim(disc) == '' || $.trim(disc) == '%' || $.trim(disc) == '%%' || $.trim(disc) == '%%%' || $.trim(disc) == '%%%%') {
                disc = 0;
            }
            if (disc.length > 0) {
                disc_type = 'percentage';
            }

            var quantity_amount_and_unit_price = parseFloat($.trim(unit_price)) * parseFloat($.trim(quantity_amount));

            if (disc_type == 'percentage') {
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                temp_amount = quantity_amount_and_unit_price * disc;
                temp_amount = temp_amount / 100;
                $('#discount_ind_' + id).val(temp_amount);
                quantity_amount_and_unit_price = quantity_amount_and_unit_price - temp_amount;
            }

            if (tax.length > 0) {
                var tax_product = 0;
                tax.forEach(function (key, val) {
                    var tax1 = quantity_amount_and_unit_price;
                    var tax2 = (tax1 * key) / 100;
                    total_tax += tax2;
                    tax_product += tax2;
                });
                quantity_amount_and_unit_price = quantity_amount_and_unit_price + tax_product;
            }

            var BasicTotal = quantity * unit_price;
            $('#BasicTotal_' + id).val(BasicTotal);

            var total_amt = parseFloat(BasicTotal) - parseFloat(temp_amount);
            $('#total_amt_' + id).val(parseFloat(total_amt).toFixed(2));

            $('#total_tax_' + id).val(total_tax);
            $('#tax_amount_' + id).val(total_tax);
            $("#total_" + id).val(quantity_amount_and_unit_price.toFixed(2));
            subtotal += parseFloat($.trim($("#total_" + id).val()));
        });

        if (isNaN(subtotal)) {
            subtotal = 0;
        }
        $("#subtotal").val(subtotal);
        var other = parseFloat($.trim($("#other").val()));
        if ($.trim(other) == "" || $.isNumeric(other) == false) {
            other = 0;
        }

        var fright_charges = $('#fright_charges').val();
        var round_up_amt = $('#round_up_amt').val();

        var grand_total = parseFloat(subtotal) + parseFloat(other) + (fright_charges != '' && fright_charges != null && fright_charges != undefined ? parseFloat(fright_charges) : 0) + parseFloat(round_up_amt != '' && round_up_amt != null && round_up_amt != undefined ? parseFloat(round_up_amt) : 0);
        grand_total = grand_total.toFixed(2);
        $("#g_total").val(grand_total);
        $('#payment_amt').val(grand_total);

        var paid = $("#paid").val();
        if ($.trim(paid) == "" || $.isNumeric(paid) == false) {
            paid = 0;
        }

        var due = parseFloat(grand_total) - parseFloat(paid);

        $("#due").val(due.toFixed(2));
    }

    function updateRowNo() {
        var numRows = $("#product_cart tbody tr").length;
        for (var r = 0; r < numRows; r++) {
            $("#product_cart tbody tr").eq(r).find("td:first p").text(r + 1);
        }
    }

    $(document).on('change', '#supplier_id', function () {
        var address = $(this).find(':selected').attr('data-address');
        if (address != '' && address != undefined) {
            $('#supplier_address').text(address);
            $('#hidden_supplier_address').val(address);
        } else {
            $('#supplier_address').text('');
            $('#hidden_supplier_address').val('');
        }
    });

    function checkStock(suffix) {
        var quantity_amount = $('#quantity_amount_' + suffix).val();
        var quantity_sale_order = $('#quantity_amount_' + suffix).attr('data-sale_outward_qty');
        quantity_amount = parseFloat(quantity_amount);
        quantity_sale_order = parseFloat(quantity_sale_order);

        if ($.trim(quantity_amount) == "" || quantity_amount == undefined || isNaN(quantity_amount)) {
            quantity_amount = 0;
        }
        if ($.trim(quantity_sale_order) == "" || quantity_sale_order == undefined || isNaN(quantity_sale_order)) {
            quantity_sale_order = 0;
        }

        if (quantity_amount <= 0) {
            $('#quantity_amount_' + suffix).val(null);
            calculateAll();
            swalWithBootstrapButtons.fire({
                icon: 'warning',
                title: "Alert!",
                text: "Quantity must be more than 0.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
            return false;
        }

        var stored_qty = $('#stored_qty_td_' + suffix).text();
        var stored_qty_val = stored_qty != '' && stored_qty != null ? stored_qty.split(' ')[0] : null;
//        console.log(stored_qty_val);

        if (stored_qty_val != '' && stored_qty_val != ' ' && stored_qty_val != null) {
            if (quantity_amount > parseFloat(stored_qty_val)) {
                $('#quantity_amount_' + suffix).val(null);
                calculateAll();
                swalWithBootstrapButtons.fire({
                    icon: 'warning',
                    title: "Alert!",
                    text: "Quantity must be less than or equal to stored qty.",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
                return false;
            }
        }
    }

    $(document).on('change', '#customer_id', function () {

        var same_as_billing = $('#sameasbilling').prop("checked");

        $('#customer_all_detail').html('');
        var customer_all_detail = '<label for="customer_detail" class="col-form-label" >Billing Address<a style="margin-left: 11px; color: black;" id="edit_billing_address_btn" href="javascript:void(0);"><i class="fa fa-pencil" style="font-size: 18px;" title="Edit"></i></a></label><br>';
        var customer_name = $(this).find(':selected').attr('data-name');
        var address = $(this).find(':selected').attr('data-address');
        var phone_number = $(this).find(':selected').attr('data-phonenumber');
        var email_address = $(this).find(':selected').attr('data-email');

        if (address != '' && address != undefined) {
            customer_all_detail += '<div id="customer_address">';
            customer_all_detail += '<textarea class="form-control" style="resize:vertical;" rows="3" name="hidden_customer_address" id="hidden_customer_address" readonly>' + address + '</textarea>';
            customer_all_detail += '</div>';
            $('#hidden_customer_address').val(address);

            if (same_as_billing == true) {
                $('#si_address_1').val(address);
            }
            $('#bi_address_1').val(address);

        } else {
            if (same_as_billing == true) {
                $('#si_address_1').val('');
            }
            $('#customer_address').text('');
            $('#bi_address_1').val('');
            $('#hidden_customer_address').val('');
        }

        if (phone_number != '' && phone_number != undefined) {
            customer_all_detail += '<input type="hidden" value="' + phone_number + '" name="hidden_customer_phonenumber" id="hidden_customer_phonenumber">';
            $('#hidden_customer_phonenumber').val(phone_number);

            if (same_as_billing == true) {
                $('#si_phone').val(phone_number);
            }
            $('#bi_phone').val(phone_number);
        } else {
            if (same_as_billing == true) {
                $('#si_phone').val('');
            }
            $('#customer_phonenumber').text('');
            $('#bi_phone').val('');
            $('#hidden_customer_phonenumber').val('');
        }

        if (email_address != '' && email_address != undefined) {
            customer_all_detail += '<input type="hidden" value="' + email_address + '" name="hidden_customer_email" id="hidden_customer_email">';
            $('#hidden_customer_email').val(email_address);
        } else {
            $('#customer_email').text('');
            $('#hidden_customer_email').val('');
        }

        if (customer_name != '' && customer_name != undefined) {
            if (same_as_billing == true) {
                $('#si_customer_name').val(customer_name);
            }
            $('#bi_customer_name').val(customer_name);
        } else {
            if (same_as_billing == true) {
                $('#si_customer_name').val('');
            }
            $('#bi_customer_name').val('');
        }

        $('#customer_all_detail').append(customer_all_detail);
    });

    $(document).on('change', '#agent_id', function () {

        var same_as_billing = $('#sameasbilling').prop("checked");

        $('#agent_all_detail').html('');
        var agent_all_detail = '<label for="agent_detail" class="col-form-label" >Agent Detail</label><br>';
        var agent_name = $(this).find(':selected').attr('data-name');
        var agent_address = $(this).find(':selected').attr('data-address');
        var agent_phone_number = $(this).find(':selected').attr('data-phonenumber');
        var agent_email_address = $(this).find(':selected').attr('data-email');

        if (agent_address != '' && agent_address != undefined) {
            agent_all_detail += '<span id="agent_address"><b>Address : </b>' + agent_address + '</span><br>';
            agent_all_detail += '<input type="hidden" value="' + agent_address + '" name="hidden_agent_address" id="hidden_agent_address">';
            $('#hidden_agent_address').val(agent_address);

            if (same_as_billing == true) {
                $('#si_address_1').val(agent_address);
            }
            $('#bi_address_1').val(agent_address);

        } else {
            if (same_as_billing == true) {
                $('#si_address_1').val('');
            }
            $('#agent_address').text('');
            $('#bi_address_1').val('');
            $('#hidden_agent_address').val('');
        }

        if (agent_phone_number != '' && agent_phone_number != undefined) {
            agent_all_detail += '<span id="agent_phonenumber"><b>Phone : </b>' + agent_phone_number + '</span><br>';
            agent_all_detail += '<input type="hidden" value="' + agent_phone_number + '" name="hidden_agent_phonenumber" id="hidden_agent_phonenumber">';
            $('#hidden_agent_phonenumber').val(agent_phone_number);

            if (same_as_billing == true) {
                $('#si_phone').val(agent_phone_number);
            }
            $('#bi_phone').val(agent_phone_number);
        } else {
            if (same_as_billing == true) {
                $('#si_phone').val('');
            }
            $('#agent_phonenumber').text('');
            $('#bi_phone').val('');
            $('#hidden_agent_phonenumber').val('');
        }

        if (agent_email_address != '' && agent_email_address != undefined) {
            agent_all_detail += '<span id="agent_email"><b>Email : </b>' + agent_email_address + '</span><br>';
            agent_all_detail += '<input type="hidden" value="' + agent_email_address + '" name="hidden_agent_email" id="hidden_agent_email">';
            $('#hidden_agent_email').val(agent_email_address);
        } else {
            $('#agent_email').text('');
            $('#hidden_agent_email').val('');
        }

        if (agent_name != '' && agent_name != undefined) {
            if (same_as_billing == true) {
                $('#si_agent_name').val(agent_name);
            }
            $('#bi_agent_name').val(agent_name);
        } else {
            if (same_as_billing == true) {
                $('#si_agent_name').val('');
            }
            $('#bi_agent_name').val('');
        }

        $('#agent_all_detail').append(agent_all_detail);
    });


    $(document).ready(function () {
        $('.select222').select2({
            allowClear: false,
            placeholder: $(this).data('placeholder') != '' && $(this).data('placeholder') != null ? $(this).data('placeholder') : '-- Select Option --'
        });

        $('#update_sale_invoice').validate({
            errorPlacement: function (error, element) {
                if (element.is("select")) {
                    error.appendTo(element.parent());
                } else if (element.data('type') == "date") {
                    error.appendTo(element.parent().parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                sale_invoice_no: {
                    remote: {
                        url: "<?= base_url('admin/Sale_invoice/checkSaleInvoiceNo') ?>",
                        type: "POST",
                        data: {
                            sale_invoice_id: function () {
                                return $("#sale_invoice_id").val();
                            }
                        }
                    }
                }
            },
            messages: {
                sale_invoice_no: {
                    remote: "Sale invoice number is already in use!"
                }
            },
            submitHandler: function (form) {
                if ($('.rowCount').length == 0) {
                    swalWithBootstrapButtons.fire('Alert!', 'Add at least one product.', 'warning');
                    return false;
                }
                form.submit();
            }
        });

        $('.select2222').select2();

        $(document).on('change', '.discount_percentage', function () {
            let qty = parseFloat($(this).val());
            if (qty > 100) {
                $(this).val(null);
                swalWithBootstrapButtons.fire({
                    icon: 'warning',
                    title: "Alert!",
                    text: "Discount must be less than or equal to 100%.",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
            } else if (qty <= 0) {
                $(this).val(null);
                swalWithBootstrapButtons.fire({
                    icon: 'warning',
                    title: "Alert!",
                    text: "Discount must be more than 0%.",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
            } else {
//                alert('ok');
            }
        });

        var cnt = 0;

        $(document).on('click', '#edit_billing_address_btn', function (e) {
            e.preventDefault();
            let address = $('#hidden_customer_address').text();
            if (address != '' && address != null) {
                $("#hidden_customer_address").attr("readonly", false);
            } else {
                $("#hidden_customer_address").attr("readonly", true);
            }
        });

        $(document).on('keyup', '#product_barcode', function (e) {
            e.preventDefault();
            var key = e.which;
            var _this = $(this);
            if (key == 13) {
                let product_barcode = $(this).val();
                if (product_barcode != '' && product_barcode != ' ' && product_barcode != null) {
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url("admin/Sale_invoice/getProductDataByBarcode") ?>",
                        data: {product_barcode: product_barcode},
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.status == true) {
                                let response = data.product_info;
                                if (product_id_container.includes(response.id)) {
                                    $('.rowCount').each(function (key, value) {
                                        let dataId = $(this).data('id');
                                        if (response.id == $('#product_id_' + dataId).val()) {
                                            let existed_qty = $('#quantity_amount_' + dataId).val();
                                            let new_qty = parseInt(existed_qty) + 1;
                                            $('#quantity_amount_' + dataId).val(parseInt(new_qty));
                                            calculateAll();
                                        }
                                    });
                                    _this.val('');
                                    setTimeout(function () {
                                        $('#product_barcode').focus();
                                    }, 100);
                                    return false;
                                }
                                insertProductRow(response);
                            } else {
                                _this.val('');
                                setTimeout(function () {
                                    $('#product_barcode').focus();
                                }, 100);
                                swalWithBootstrapButtons.fire('Oops...!', data.msg, 'error');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swalWithBootstrapButtons.fire('Oops...!', 'Something went wrong, Try again!', 'error');
                        }
                    });
                } else {
                    swalWithBootstrapButtons.fire('Oops...!', 'Please enter valid product barcode!', 'error');
                }
                setTimeout(function () {
                    $('#product_barcode').focus();
                }, 100);

            }
        });

//        $("#product_barcode").on('paste', function (e) {
//            var _this = $(this);
//            setTimeout(function () {
//                let product_barcode = $(_this).val();
//                if (product_barcode != '' && product_barcode != ' ' && product_barcode != null) {
//                    $.ajax({
//                        type: "POST",
//                        url: "<?= base_url("admin/Sale_invoice/getProductDataByBarcode") ?>",
//                        data: {product_barcode: product_barcode},
//                        cache: false,
//                        dataType: 'json',
//                        success: function (data) {
//                            if (data.status == true) {
//                                let response = data.product_info;
//
//                                if (product_id_container.includes(response.id)) {
//                                    console.log('1');
//                                    $('.rowCount').each(function (key, value) {
//                                        let dataId = $(this).data('id');
//                                        if (response.id == $('#product_id_' + dataId).val()) {
//                                            console.log('2');
//                                            let existed_qty = $('#quantity_amount_' + dataId).val();
//                                            let new_qty = parseInt(existed_qty) + 1;
//                                            $('#quantity_amount_' + dataId).val(parseInt(new_qty));
//                                            calculateAll();
//                                        }
//                                    });
//                                    _this.val(null);
//                                    _this.focus();
//                                    return false;
//                                }
//
//                                insertProductRow(response);
//                            } else {
//                                swalWithBootstrapButtons.fire('Oops...!', data.msg, 'error');
//                            }
//                        },
//                        error: function (jqXHR, textStatus, errorThrown) {
//                            swalWithBootstrapButtons.fire('Oops...!', 'Something went wrong, Try again!', 'error');
//                        }
//                    });
//                } else {
//                    swalWithBootstrapButtons.fire('Oops...!', 'Please enter valid product barcode!', 'error');
//                }
//            }, 500);
//        });

    });

    function insertProductRow(data) {
        var product_row = '';
        suffix++;

        product_row += '<tr class="rowCount" data-id="' + suffix + '" id="row_' + suffix + '">';
        product_row += '<td style="padding-left: 10px;"><p id="sl_' + suffix + '">' + suffix + '</p></td>';
        product_row += '<td><span style="padding-bottom: 5px;">' + data.product_name + ' - (' + data.category_name + ')</span></td>';
        product_row += '<input type="hidden" id="product_id_' + suffix + '" name="product_id[]" value="' + data.id + '"/>';
        product_row += '<input type="hidden" id="tax_per_' + suffix + '" name="tax_per[]" value="' + data.tax + '"/>';
        product_row += '<input type="hidden" id="tax_per_amt_' + suffix + '" name="tax_per_amt[]" value=""/>';
        product_row += '<input type="hidden" id="total_tax_' + suffix + '" name="total_tax[]" value=""/>';
        product_row += '<input type="hidden" id="discount_ind_' + suffix + '" name="discount_ind[]" value=""/>';
        product_row += '<input type="hidden" id="BasicTotal_' + suffix + '" name="BasicTotal[]" value=""/>';
        product_row += '<input type="hidden" id="Tax_id_' + suffix + '" name="Tax_id[]" value="' + data.tax_id + '"/>';
        product_row += '<input type="hidden" id="total_amt_' + suffix + '" name="total_amt[]" value=""/>';
        product_row += '<input type="hidden" id="tax_name_' + suffix + '" name="tax_name[]" value="' + data.tax_name + '"/>';

        product_row += '<td><input type="text" id="unit_price_' + suffix + '" name="unit_price[]" onfocus="this.select();" class="form-control desimalcheck aligning" placeholder="Unit Price" value="' + data.price + '" onkeyup="return calculateAll();" required /><span class="label_aligning"></span></td>';
        product_row += '<td><input type="text" data-countID="' + suffix + '" id="quantity_amount_' + suffix + '" name="quantity_amount[]" value="1" onfocus="this.select();" class="form-control desimalcheck aligning countID"  placeholder="Quantity" onkeyup="return calculateAll();" required data-sale_outward_qty="0" onblur="checkStock(' + suffix + ')"></td>';
        product_row += '<td><input type="text" data-countID="' + suffix + '" id="discount_' + suffix + '" name="discount[]" onfocus="this.select();" class="form-control desimalcheck aligning countID discount_percentage"  placeholder="Discount in percentage" onkeyup="return calculateAll();" ></td>';
        product_row += '<td><span id="tax_display_' + suffix + '">' + data.tax + '%</span></td>';
        product_row += '<td><input type="text" id="tax_amount_' + suffix + '" name="tax_amount[]" class="form-control" placeholder="Tax Amount" readonly /><span class="label_aligning"></span></td>';
        product_row += '<td><input type="text" id="total_' + suffix + '" name="total[]" class="form-control aligning" placeholder="Total" readonly /><span class="label_aligning"></span></td>';
        product_row += '<td><button class="btn btn-danger red valid" type="button" value="Delete" onclick="deleter(' + suffix + ',\'' + data.id + '\')" tabindex="10" autocomplete="off" aria-invalid="false"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
        product_row += '</tr>';

        $('#product_cart tbody').append(product_row);
        $('#product_id').val('').change();
        $('#product_barcode').val('');

        product_id_container.push(data.id);
        calculateAll();
    }

</script>



