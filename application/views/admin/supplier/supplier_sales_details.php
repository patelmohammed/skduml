<!-- Stock report start -->
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        document.body.style.marginTop = "0px";
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<!-- Supplier Sales Report Start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('supplier_sales_details') ?></h1>
            <small><?php echo display('supplier_sales_details') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('supplier') ?></a></li>
                <li class="active"><?php echo display('supplier_sales_details') ?></li>
            </ol>
        </div>
    </section>

    <!-- Search Supplier -->
    <section class="content">

        <div class="row">
            <div class="col-sm-12">
                <div class="column">

                    <a href="<?php echo base_url('Csupplier') ?>" class="btn btn-info m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('add_supplier') ?> </a>

                    <a href="<?php echo base_url('Csupplier/manage_supplier') ?>" class="btn btn-primary m-b-5 m-r-2"><i class="ti-align-justify"> </i>  <?php echo display('manage_supplier') ?> </a>

                    <a href="<?php echo base_url('Csupplier/supplier_ledger_report') ?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-align-justify"> </i>  <?php echo display('supplier_ledger') ?> </a>

                </div>
            </div>
        </div>

        <!-- Sales Details -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('supplier_sales_details') ?></h4>
                        </div>

                    </div>
                    <div class="panel-body">
                        <!--                        <div class="text-right">
                                                    <a class="btn btn-warning text-right" href="#" onclick="printDiv('printableArea')"><?php echo display('print') ?></a>
                                                </div>-->
                        <div id="printableArea" style="margin-left:2px;">

                            <?php if ($supplier_name) { ?>

                                <div class="text-center">
                                    <h3> {supplier_name} </h3>
                                    <h4><?php echo display('address') ?> : {supplier_address} </h4>
                                    <h4> <?php echo display('print_date') ?>: <?php echo date("d/m/Y h:i:s"); ?> </h4>
                                </div>

                            <?php } ?>

                            <div class="table-responsive">
                                <table id="supplier_sales_data" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Supplier Name</th>
                                            <th style="text-align: right !important;"><?php echo display('ammount') ?></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Supplier Sales Details End -->

<script>
    $(document).ready(function () {
        var _supplier_sales_data = $('#supplier_sales_data').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>t<'bottom'><'row'<'col-sm-5'i>p>",
            "oTableTools": {
                "aButtons": [
                ]
            },
            buttons: [
                {extend: 'excel', title: 'manage_supplier_sales', className: 'btn-sm', exportOptions: {columns: [0, 1, 2], modifier: {page: 'current'}}},
                {extend: 'print', className: 'btn-sm', exportOptions: {columns: [0, 1, 2], modifier: {page: 'current'}}}
            ],
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false
                }, {
                    "targets": [0],
                    "className": 'text-center'
                }],
            "ajax": {
                "url": "<?= base_url() ?>Csupplier/get_supplier_sales_list",
                "type": "POST"
            }
        });
        $("#supplier_sales_data thead input").on('keyup change', function () {
            _supplier_sales_data.column($(this).parent().index() + ':visible').search(this.value).draw();
        });
    });
</script>