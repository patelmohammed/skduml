<!-- Supplier Details Start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('supplier_details') ?></h1>
            <small><?php echo display('manage_your_supplier_details') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('supplier') ?></a></li>
                <li class="active"><?php echo display('supplier_details') ?></li>
            </ol>
        </div>
    </section>

    <!-- Supplier information -->
    <section class="content">

        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>


        <div class="row">
            <div class="col-sm-12">
                <div class="column">

                    <a href="<?php echo base_url('Csupplier') ?>" class="btn btn-info m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('add_supplier') ?> </a>

                    <a href="<?php echo base_url('Csupplier/manage_supplier') ?>" class="btn btn-info m-b-5 m-r-2"><i class="ti-align-justify"> </i> <?php echo display('manage_supplier') ?> </a>

                    <a href="<?php echo base_url('Csupplier/supplier_ledger_report') ?>" class="btn btn-primary m-b-5 m-r-2"><i class="ti-align-justify"> </i>  <?php echo display('supplier_ledger') ?> </a>

                    <a href="<?php echo base_url('Csupplier/supplier_sales_details_all') ?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-align-justify"> </i>  <?php echo display('supplier_sales_details') ?> </a>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('purchase_report') ?></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div style="float:left">
                            <h2>{supplier_name}</h2>
                            <h4>{supplier_mobile}</h4>
                            <h5>{supplier_address}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Manage Supplier -->

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('purchase_report') ?> </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="purchaseDataTable" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th><?php echo display('date') ?></th>
                                        <th>Purchase Invoice No</th>
                                        <th>Total Product</th>
                                        <!--<th>Total Landed Cost</th>-->
                                        <th>Basic Total</th>
                                        <th>Total Discount</th>
                                        <th>Total Tax Amount</th>
                                        <th style="text-align:right;">Grand Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($purchase_info)) {
                                        $sl = 0;
                                        foreach ($purchase_info as $k1 => $v1) {
                                            $sl++;
                                            ?>
                                            <tr>
                                                <td><?= $sl ?></td>
                                                <td><?= $v1->DateTime ?></td>
                                                <td>
                                                    <a href="<?php echo base_url() . 'Cpurchase/purchase_invoice_details_data/' . $v1->PINID; ?>">
                                                        <?= $v1->PINNO ?>
                                                    </a>
                                                </td>
                                                <td><?= getPurchaseCountForInvoice($v1->PINID) ?></td>
                                                <!--<td  style="text-align:right;"><?php echo (($position == 0) ? "$currency " . $v1->TotalLandedCost : $v1->TotalLandedCost . " $currency") ?></td>-->
                                                <td  style="text-align:right;"><?php echo (($position == 0) ? "$currency " . $v1->BasicTotal : $v1->BasicTotal . " $currency") ?></td>
                                                <td  style="text-align:right;"><?php echo (($position == 0) ? "$currency " . $v1->Discount : $v1->Discount . " $currency") ?></td>
                                                <td  style="text-align:right;"><?php echo (($position == 0) ? "$currency " . $v1->Taxtotal : $v1->Taxtotal . " $currency") ?></td>
                                                <td style="text-align:right;"> <?php echo (($position == 0) ? "$currency " . $v1->GrandTotal : $v1->GrandTotal . " $currency") ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4" style="text-align:right;"><b><?php echo display('total') ?></b></td>
                                        <td style="text-align:right;"> <?= $total_purchase ?></td>
                                        <td colspan="2" style="text-align:right;"><b><?php echo display('grand_total') ?></b></td>
                                        <td style="text-align:right;"><b> <?php echo (($position == 0) ? "$currency " . $purchaseTotalAmount : $purchaseTotalAmount . " $currency") ?></b></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Supplier Details End  -->

<script>
    $(document).ready(function () {
        $("#purchaseDataTable").DataTable({
            responsive: true,
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'excel', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'print', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}}
            ],
            order: [1, 'desc'],
        });
    });
</script>