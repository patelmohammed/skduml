<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Supplier Ledger
        </h1>      
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <a href="<?php echo base_url('admin/Csupplier') ?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-plus"> </i> Add Supplier </a>

                    <a href="<?php echo base_url('admin/Csupplier/manage_supplier') ?>" class="btn btn-primary bg-trans-gradient waves-effect waves-themed m-b-5 m-r-2"><i class="ti-align-justify"> </i>  Manage Supplier </a>

                    <!--<a href="<?php echo base_url('admin/Csupplier/supplier_sales_details_all') ?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-align-justify"> </i>  Supplier Sales Details </a>-->

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <form action="<?php echo base_url('admin/Csupplier/supplier_ledger') ?>" method="post" accept-charset="utf-8" class="mb-2">
                            <div class="row">
                                <div class="col-md-1 text-right"><b>Select Supplier</b></div>
                                <div class="col-md-2">
                                    <select class="form-control select2" name="supplier_id" id="supplier_id">
                                        <option value="">Select Supplier...</option>
                                        <?php
                                        if (isset($supplier) && !empty($supplier)) {
                                            foreach ($supplier as $key => $value) {
                                                if ($supplier_id == $value['supplier_id']) {
                                                    $sel_sup = 'selected';
                                                } else {
                                                    $sel_sup = '';
                                                }
                                                ?>
                                                <option value="<?= isset($value['supplier_id']) && !empty($value['supplier_id']) ? $value['supplier_id'] : null ?>" <?= $sel_sup ?> ><?= isset($value['supplier_name']) && !empty($value['supplier_name']) ? $value['supplier_name'] : null ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-1 text-right"><b>From</b></div>
                                <div class="col-md-2"><input type="date" name="from_date" class="form-control" id="from_date" value="<?= date('Y-m-d', strtotime(FIN_YEAR_STRT_DATE)) ?>" /></div>
                                <div class="col-md-1 text-right"><b>To</b></div>
                                <div class="col-md-2"><input type="date" name="to_date" value="<?= date('Y-m-d', strtotime(FIN_YEAR_END_DATE)) ?>" class="form-control" id="to_date"/></div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search-plus" aria-hidden="true"></i> Generate</button>
                                    <a  class="btn btn-warning printPdfSupplierLedger" href="javascript:void(0);" >Print</a>
                                </div>
                            </div>
                        </form>

                        <div id="printableArea" style="margin-left:2px;">
                            <?php if ($supplier_name) { ?>
                                <div class="text-center">
                                    <h3><?= $supplier_name ?></h3>
                                    <h4>Address: <?= $address ?> </h4>
                                    <h4>Print Date: <?php echo date("d/m/Y h:i:s"); ?> </h4>
                                </div>
                            <?php } ?>

                            <table id="supplier_data" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                                <thead class="thead-dark">
                                    <tr>
                                        <th class="text-center">SL</th>
                                        <th class="text-start">Date</th>
                                        <th class="text-start">Supplier Name</th>
                                        <th class="text-start">Invoice no</th>
                                        <th class="text-start">Deposit ID</th>
                                        <th class="text-start">Description</th>
                                        <th class="text-right">Credit</th>
                                        <th class="text-right">Debit</th>
                                        <th class="text-right">Balance</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if (isset($ledgers) && !empty($ledgers)) {
                                        $sl = 0;
                                        $debit = $credit = $balance = 0;
                                        foreach ($ledgers as $ledger) {
                                            $sl++;
                                            ?>
                                            <tr>
                                                <td class="text-center"><?php echo $sl; ?></td>
                                                <td class="text-center"> <?= isset($ledger->date) && !empty($ledger->date) ? date('d-M-Y', strtotime($ledger->date)) : null ?></td>
                                                <td><?= isset($ledger->supplier_name) && !empty($ledger->supplier_name) ? $ledger->supplier_name : null ?></td>
                                                <?php
                                                if (isset($ledger->PINID) && isset($ledger->PINNO) && !empty($ledger->PINID) && !empty($ledger->PINNO)) {
                                                    $style = 'style="text-align: start;"';
                                                    $url = base_url() . 'admin/Purchase_invoice/purchaseInvoiceDetails/' . $ledger->PINID;
                                                    $PINNO = $ledger->PINNO;
                                                } else if (isset($ledger->PRID) && isset($ledger->PRNO) && !empty($ledger->PRID) && !empty($ledger->PRNO)) {
                                                    $style = 'style="text-align: start;"';
                                                    $url = 'javascript:void(0);';
                                                    //                                                        $url = base_url() . 'Cpurchase/purchase_invoice_details_data/' . $ledger->PRID;
                                                    $PINNO = $ledger->PRNO;
                                                } else {
                                                    $style = 'style="text-align: start;"';
                                                    $url = 'javascript:void(0);';
                                                    $PINNO = '—';
                                                }
                                                ?>
                                                <td <?= $style ?>>
                                                    <a href="<?= $url ?>"><?= $PINNO ?></a>
                                                </td>
                                                <td class="text-start"><?php echo @$ledger->deposit_no; ?></td>
                                                <?php
                                                if (isset($ledger->description) && !empty($ledger->description)) {
                                                    ?>
                                                    <td><?= $ledger->description ?></td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td style="text-align: center;">—</td>
                                                    <?php
                                                }
                                                ?>
                                                <td align="right">
                                                    <?php
                                                    if ($ledger->d_c == 'c') {
                                                        echo (($position == 0) ? "$currency " : " $currency");
                                                        echo number_format($ledger->amount, 2, '.', ',');
                                                        $credit += $ledger->amount;
                                                    } else {
                                                        $credit += '0.00';
                                                    }
                                                    ?>
                                                </td>
                                                <td align="right">
                                                    <?php
                                                    if ($ledger->d_c == 'd') {
                                                        echo (($position == 0) ? "$currency " : " $currency");
                                                        echo number_format($ledger->amount, 2, '.', ',');
                                                        $debit += $ledger->amount;
                                                        //                                                         $d = 12;
                                                    } else {
                                                        $debit += '0.00';
                                                    }
                                                    ?>
                                                </td>
                                                <td align='right'>
                                                    <?php
                                                    $balance = $credit - $debit;
                                                    echo (($position == 0) ? "$currency " : " $currency");
                                                    echo number_format($balance, 2, '.', ',');
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot class="thead-dark">
                                    <tr>
                                        <td colspan="6" align="right"><b>Grand Total:</b></td>
                                        <td align="right"><b><?php
                                                echo (($position == 0) ? "$currency " : "$currency");
                                                echo number_format((@$credit), 2, '.', ',');
                                                ?></b>
                                        </td>
                                        <td align="right"><b><?php
                                                echo (($position == 0) ? "$currency " : "$currency");
                                                echo number_format((@$debit), 2, '.', ',');
                                                ?></b>
                                        </td>
                                        <td align="right"><b><?php
                                                echo (($position == 0) ? "$currency " : "$currency");
                                                echo number_format((@$balance), 2, '.', ',');
                                                ?></b></td>
                                    </tr>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<!-- Supplier Ledger End  -->
<script type="text/javascript">

    $(document).ready(function () {
        $('#supplier_id').select2({
            placeholder: '-- Select Supplier --'
        });

        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });

        $(document).on('click', '.printPdfSupplierLedger', function () {
            let supplier_id = $('#supplier_id').val();
            let start_date = $('#from_date').val();
            let end_date = $('#to_date').val();
            if (supplier_id != '' && supplier_id != null) {
                $.ajax({
                    cache: false,
                    method: 'POST',
                    url: '<?= base_url('admin/Csupplier/printPdfSupplierLedger') ?>',
                    data: {supplier_id: supplier_id, start_date: start_date, end_date: end_date},
                    success: function (response, status, xhr) {
                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");
                        // safari doesn't support this yet
                        if (typeof a.download === 'undefined') {
                            window.location = response;
                        } else {
                            a.href = response;
                            a.download = "supplier_ledger.pdf";
                            document.body.appendChild(a);
                            a.target = "_blank";
                            a.click();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        swalWithBootstrapButtons.fire('Oops...!', 'Something went wrong, Try again!', 'error');
                    }
                });
            } else {
                swalWithBootstrapButtons.fire('Oops...!', 'Plaese select supplier!', 'warning');
            }
        });
    });
</script>






