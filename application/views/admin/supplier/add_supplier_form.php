



<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Supplier
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Csupplier/manage_supplier">Supplier</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Csupplier/insert_supplier', array('id' => 'insert_supplier')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="supplier_name">Supplier Name (Company Name) <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="supplier_name" id="supplier_name" placeholder="" required value="">
                                <div class="invalid-feedback">
                                    Supplier Name Required or Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="serial_no">Contact Person Name</label>
                                <input type="text" class="form-control" name="contact_person" id="contact_person" placeholder="" value="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="address_id">Address  </label>
                                <textarea class="form-control" id="address" name="address" placeholder=""></textarea>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="unit">Mobile </label>
                                <input class="form-control" type="number" id="mobile" name="mobile"  placeholder="">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sell_price">Supplier GST Number</label>
                                <input type="text" class="form-control" name="supplier_gst_no" id="supplier_gst_no" placeholder="" value="">

                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="unit">Email </label>
                                <input class="form-control " type="email" id="supplier_email" name="supplier_email"  placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="description">Supplier State </label>
                                <select class="form-control select2" name="ref_state_id" id="ref_state_id" data-placeholder="-- Select state --">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($stateList)) {
                                        foreach ($stateList as $key => $value) {
                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>                                
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="supplier_bank_name" class="form-label">Supplier Bank Name   </label>
                                <input class="form-control" name="supplier_bank_name" id="supplier_bank_name" type="text" placeholder="" value="">

                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="supplier_bank_name" class="form-label">  Bank IFSC Code </label>
                                <input class="form-control" name="bank_ifsc_code" id="bank_ifsc_code" type="text" placeholder="" value="">

                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="supplier_bank_name" class="form-label"> Details </label>
                                <textarea class="form-control" name="details" id="details" placeholder=" " ></textarea>

                            </div>                                                      
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="supplier_bank_name" class="form-label"> Account Holder Name </label>
                                <input class="form-control" name="acoount_holder_name" id="acoount_holder_name" type="text" placeholder="" value="">

                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="supplier_bank_name" class="form-label">  Bank Account Number </label>
                                <input class="form-control" name="bank_account_number" id="bank_account_number" type="number" placeholder="" value="">

                            </div>                                                      
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="supplier_bank_name" class="form-label"> office number </label>
                                <input class="form-control" name="office_number" id="office_number" type="number" placeholder="" value="">

                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="supplier_bank_name" class="form-label">  Previous Credit Balance</label>
                                <input class="form-control" name="Previous_Credit_Balance" id="Previous_Credit_Balance" type="number" placeholder="" value="">

                            </div>                                                      


                        </div>

                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-supplier" value="Save"><span class="fal fa-check mr-1"></span>Save</button>
                        <button type="submit" class="btn btn-warning ml-1 waves-effect waves-themed" name="add-supplier-another" value="Save & Add Another"><span class="fal fa-check mr-1"></span>Save & Add Another</button>
                    </div>

                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>

</main>

<script>
    $(document).ready(function () {
        $('#insert_supplier').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                supplier_name: {
                    remote: '<?= base_url() ?>admin/Csupplier/checkSupplier'
                }
            },
            messages: {
                supplier_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>