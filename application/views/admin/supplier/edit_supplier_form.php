<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit  Supplier
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Csupplier/manage_supplier">Supplier</a>
        </div>
    </div>  
    <!-- New supplier -->
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">                    
                    <div class="panel-title">
                        <?php echo form_open_multipart('admin/Csupplier/supplier_update', array('id' => 'supplier_update')) ?>
                        <div class="panel-content">
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="">Supplier Name (Company Name) <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="supplier_name" id="supplier_name"  required value="<?= isset($supplier_name) && !empty($supplier_name) ? $supplier_name : null ?>">
                                    <div class="invalid-feedback">
                                        Supplier Name Required or Already Exist
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="serial_no">Contact Person</label>
                                    <input type="text" class="form-control" name="contact_person" id="contact_person"  value="<?= isset($contact_person) && !empty($contact_person) ? $contact_person : null ?>">

                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="address_id">Address  </label>
                                    <textarea class="form-control" id="address" name="address" ><?= isset($address) && !empty($address) ? $address : null ?></textarea>

                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="unit">Mobile </label>
                                    <input class="form-control" type="number" id="mobile" name="mobile"  value="<?= isset($mobile) && !empty($mobile) ? $mobile : null ?>" >
                                </div>  
                            </div>

                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="sell_price">Supplier GST Number</label>
                                    <input type="text" class="form-control" name="supplier_gst_no" id="supplier_gst_no"  value= "<?= isset($supplier_gst_no) && !empty($supplier_gst_no) ? $supplier_gst_no : null ?>">

                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="email">Email</label>
                                    <input class="form-control" type="email" id="email" name="email"  value="<?= isset($supplier_email) && !empty($supplier_email) ? $supplier_email : null ?>" >
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="description">Supplier State </label>
                                    <select class="form-control select2" name="ref_state_id" id="ref_state_id" >
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($stateList)) {
                                            foreach ($stateList as $key => $value) {
                                                $sel = $value['id'] == $ref_state_id ? 'selected=""' : '';
                                                echo '<option value="' . $value['id'] . '" ' . $sel . '>' . $value['name'] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>

                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="supplier_bank_name" class="form-label">  Supplier Bank Name </label>
                                    <input class="form-control" name="supplier_bank_name" id="supplier_bank_name" type="text"  value="<?= isset($supplier_bank_name) && !empty($supplier_bank_name) ? $supplier_bank_name : null ?>">

                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="supplier_bank_name" class="form-label">  Bank IFSC Code </label>
                                    <input class="form-control" name="bank_ifsc_code" id="bank_ifsc_code" type="text"   value="<?= isset($bank_ifsc_code) && !empty($bank_ifsc_code) ? $bank_ifsc_code : null ?>">

                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="supplier_bank_name" class="form-label"> details </label>
                                    <textarea class="form-control" name="detais" id="details"><?= isset($details) && !empty($details) ? $details : null ?></textarea>

                                </div>                                                      
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="supplier_bank_name" class="form-label"> Account Holder Name </label>
                                    <input class="form-control" name="acoount_holder_name" id="acoount_holder_name" type="text"  value="<?= isset($account_holder_name) && !empty($account_holder_name) ? $account_holder_name : null ?>">

                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="supplier_bank_name" class="form-label">  Bank Account Number </label>
                                    <input class="form-control" name="bank_account_number" id="bank_account_number" type="number"  value="<?= isset($bank_account_number) && !empty($bank_account_number) ? $bank_account_number : null ?>">

                                </div>                                                      
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="supplier_bank_name" class="form-label"> office number </label>
                                    <input class="form-control" name="office_num" id="office_num" type="number"  value="<?= isset($office_num) && !empty($office_num) ? $office_num : null ?>">
                                </div>
                            </div>

                        </div>
                        <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                            <input type="hidden" name="supplier_id" value="<?= $supplier_id ?>" />
                            <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add_update" value="submit"><span class="fal fa-check mr-1"></span>update</button>
                        </div>

                        <?= form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- Edit supplier page end -->

<script>
    $(document).ready(function () {
        $('#supplier_update').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                supplier_name: {
                    remote: '<?= base_url() ?>admin/Csupplier/checkSupplier/<?= $supplier_id ?>'
                                    }
                                },
                                messages: {
                                    supplier_name: {
                                        remote: jQuery.validator.format("{0} is already in use, Try another modal")
                                    }
                                }, submitHandler: function (form) {
                                    form.submit();
                                },
                                errorPlacement: function (error, element) {
                                    return true;
                                }
                            });

                        });

</script>
