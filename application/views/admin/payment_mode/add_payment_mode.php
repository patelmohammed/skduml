
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Payment Mode
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Payment_Mode/manage_payment_mode">Payment Mode</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Payment_Mode/insert_payment_mode', array('class' => '', 'id' => 'insert_payment_mode', 'name' => 'insert_payment_mode')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="payment_mode_name">Payment Mode Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="payment_mode_name" id="payment_mode_name" placeholder="Payment Mode Name" required value="">
                                <div class="invalid-feedback">
                                    Payment Mode Name Required or alredy in use
                                </div>
                            </div>
                            
                        </div>
                        
                        
                        
                        
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-paymentmode" value="Save" id="add-paymentmode"><span class="fal fa-check mr-1"></span>Save</button>
                        <button type="submit" class="btn btn-warning ml-1 waves-effect waves-themed" id="add-paymentmode-another" name="add-paymentmode-another" value="Save & Add Another"><span class="fal fa-check mr-1"></span>Save & Add Another</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>




<!-- Add new customer start -->



<!-- Add new customer end -->
<script>
$(document).ready(function(){
    
      $('#insert_payment_mode').validate({
        validClass: "is-valid",
        errorClass: "is-invalid",
        rules: {
            payment_mode_name: {
                remote: '<?= base_url() ?>admin/Payment_Mode/checkPaymentmode'
            }
        },
        messages: {
            payment_mode_name: {
                remote: jQuery.validator.format("{0} is already in use, Try another modal")
            }
        }, submitHandler: function (form) {
            form.submit();
        },
        errorPlacement: function (error, element) {
            return true;
        }
    });
});
</script>




