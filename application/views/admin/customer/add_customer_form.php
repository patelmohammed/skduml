<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Customer
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Ccustomer/manage_customer">Customer</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Ccustomer/insert_customer', array('class' => '', 'id' => 'insert_customer', 'name' => 'insert_customer')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="company__name">Customer Name (Company Name) <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="company__name" id="company__name" placeholder="Company Name" required value="">
                                <div class="invalid-feedback">
                                    Company Name Required or Already Used
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="customer_name">Contact Person Name</label>
                                <input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="Customer Contact Person Name" value="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="email">Email</label>
                                <input class="form-control" name ="email" id="email" type="email" placeholder="Customer Email" value=""> 
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="mobile">Mobile No </label>
                                <input class="form-control numbersonly" name ="mobile" id="mobile" type="text" placeholder="Customer Mobile No" min="0" value="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="address">Customer Address</label>
                                <textarea class="form-control" name="address" id="address " rows="3" placeholder="Customer Address" value=""></textarea>
                                
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="customer_city">Customer City </label>
                                <input class="form-control" name="customer_city" id="customer_city" type="text" placeholder="Customer City" value="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="ref_state_id"> State </label>
                                <select class="form-control select2" name="ref_state_id" id="ref_state_id"  data-placeholder="-- Select state --">
                                    <option value=""></option>
                                    <?php 
                                    if(!empty($stateList)){
                                        foreach ($stateList as $key => $value) {
                                            echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="customer_country">Country</label>
                                <input class="form-control" name="customer_country" id="customer_country" type="text" placeholder="Customer Country" value="">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="customer_pincode">Pincode</label>
                                <input class="form-control numbersonly" name="customer_pincode" id="customer_pincode" type="number" placeholder="Customer Pincode" value="" min="0">
                                
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="customer_gst_no"> GST No </label>
                                <input class="form-control" name="customer_gst_no" id="customer_gst_no" type="text" placeholder="Customer GST No" value="">
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="pan_number">Pan Number</label>
                                <input class="form-control" name="pan_number" id="pan_number" type="text" placeholder="Customer Pan No" value="">
                                
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="payment_terms"> Payment Terms </label>
                                <textarea class="form-control" name="payment_terms" id="payment_terms" rows="4" placeholder="Company Payment Terms"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="client_bank_name">Client Bank Name</label>
                                <input class="form-control" name="client_bank_name" id="client_bank_name" type="text" placeholder="Client Bank Name" value="">
                                
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="bank_ifsc_code" class="form-label">Bank IFSC Code</label>
                                <input class="form-control" name="bank_ifsc_code" id="bank_ifsc_code" type="text" placeholder="Bank IFSC Code" value="">
                            </div>
                        </div>
                        
                        
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="bank_account_number" class="form-label">Bank Account Number</label>
                                <input class="form-control numbersonly" name="bank_account_number" id="bank_account_number" type="" placeholder="Bank Account Number" value="">
                                
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="acoount_holder_name" class="form-label">Account Holder Name</label>
                                <input class="form-control" name="acoount_holder_name" id="acoount_holder_name" type="text" placeholder="Account Holder Name" value="">
                            </div>
                        </div>
                        
                        
                        
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-customer" value="Save" id="add-customer"><span class="fal fa-check mr-1"></span>Save</button>

                        <button type="submit" class="btn btn-warning ml-1 waves-effect waves-themed" id="add-customer-another" name="add-customer-another" value="Save & Add Another"><span class="fal fa-check mr-1"></span>Save & Add Another</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>



<script>
    $(document).ready(function(){
    $('#insert_customer').validate({
        validClass: "is-valid",
        errorClass: "is-invalid",
        rules: {
            company__name: {
                remote: '<?= base_url() ?>admin/Ccustomer/checkCustomer'
            }
        },
        messages: {
            company__name: {
                remote: jQuery.validator.format("{0} is already in use, Try another modal")
            }
        }, submitHandler: function (form) {
            form.submit();
        },
        errorPlacement: function (error, element) {
            return true;
        }
    });
//    var count = 2;
//    var limits = 500;
//    var cnt = 1;
//    //Add purchase input field
//    function addpruduct(e, total_supplier = '', selected_supplier = '') {
//
//        if (selected_supplier != '') {
//            cnt = selected_supplier;
//        }
//        cnt++;
//
//        if ($('.rowCount').length == total_supplier) {
//            swalWithBootstrapButtons.fire({
//                title: "Alert!",
//                type: 'warning',
//                text: 'There are no more suppliers to add.',
//            });
//        } else {
//            var t = $("#new_tr").html();
//            $("tbody#proudt_item").append('<tr class="rowCount" id="tr_' + cnt + '">' + t + '</tr>');
//            $('#tr_' + cnt).find('#tmp_1').removeClass('dont-select-me');
//            $('#tr_' + cnt).find('#tmp_1').attr('name', 'supplier_id[' + cnt + ']');
//            $('#tr_' + cnt).find('#tmp_2').attr('name', 'supplier_price[' + cnt + ']');
//            $('#tr_' + cnt).find('#tmp_3').attr('name', 'LandedCost[' + cnt + ']');
//            $('#tr_' + cnt).find('#tmp_4').attr('name', 'OpeningStock[' + cnt + ']');
//            $('#tr_' + cnt).find('#tmp_1').removeAttr('id');
//            $('#tr_' + cnt).find('#tmp_2').removeAttr('id');
//            $('#tr_' + cnt).find('#tmp_3').removeAttr('id');
//            $('#tr_' + cnt).find('#tmp_5').removeAttr('id');
//            $('#tr_' + cnt).find('select').select2({
//                placeholder: "Select option",
//                allowClear: true,
//                width: '100%'
//            });
//    }
//    }
//    function deleteRow(e) {
//        var t = $("#product_table > tbody > tr").length;
//        if (1 == t) {
//            swalWithBootstrapButtons.fire({title: 'Alert!', type: 'warning', text: "There is only one row you can't delete."});
//        } else {
//            var a = e.parentNode.parentNode;
//            a.parentNode.removeChild(a)
//        }
//    }
//    $(document).on('change', '.suppliers', function () {
//        var serials = [];
//        $("#insert_product .suppliers").each(function () {
//            if ($(this).val() != '') {
//                serials.push($.trim($(this).val()));
//            }
//        });
//        var new_serials = serials.uniqueK();
//        if (serials.length != new_serials.length) {
//            swalWithBootstrapButtons.fire('Alert!', 'Supplier is already selected, Select another supplier', 'error');
//            $(this).val(null).trigger('change');
//        }
//    });
    });
</script>


