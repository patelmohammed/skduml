

<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Customer
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Ccustomer/manage_customer">Customer</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Ccustomer/customer_update', array('class' => '', 'id' => 'customer_update', 'name' => 'customer_update')) ?>
                    <input type="hidden" name="customer_id" value="<?= $customer_detail['customer_id'] ?>">
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="company__name">Company Name <span class="text-danger">*</span></label>  
                                <input type="text" class="form-control" name="company__name" id="company__name" placeholder="Company Name" required="" value="<?= $customer_detail['company__name'] ?>">
                                <div class="invalid-feedback">
                                    Company Name Required or Already Used
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="customer_name">Contact Person</label>
                                <input class="form-control" name ="customer_name" id="customer_name" type="text" placeholder="Customer Contact Person Name"  value="<?= $customer_detail['customer_name'] ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="email">Email</label>
                                <input class="form-control" name ="email" value="<?= $customer_detail['customer_email'] ?>" id="email" type="email" placeholder="customer_email">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="mobile">Mobile No </label>
                                <input class="form-control numbersonly" name ="mobile" value="<?= $customer_detail['customer_mobile'] ?>" id="mobile" type="text" placeholder="customer_mobile" min="0">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="address">Customer Address</label>
                                <textarea class="form-control" name="address" id="address " rows="3" placeholder="customer_address"><?= $customer_detail['customer_address'] ?></textarea>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="customer_city">Customer City </label>
                                <input class="form-control" name="customer_city" id="customer_city" type="text" value="<?= $customer_detail['customer_city'] ?>" placeholder="Customer City">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="ref_state_id"> State </label>
                                <select class="form-control" name="ref_state_id" id="ref_state_id"  data-placeholder="Customer State">
                                    <option value=""></option>
                                    
                                    
                                    <?php
                                    if (!empty($customer_detail['stateList'])) {
                                        foreach ($customer_detail['stateList'] as $key => $value) {
                                            $sel = $value['id'] == $customer_detail['ref_state_id'] ? 'selected=""' : '';
                                            echo '<option value="' . $value['id'] . '" ' . $sel . '>' . $value['name'] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="customer_country">Country</label>
                                <input class="form-control" name="customer_country" id="customer_country" type="text" value="<?= $customer_detail['customer_country'] ?>" placeholder="Customer Country">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="customer_pincode">Pincode</label>
                                <input class="form-control numbersonly" name="customer_pincode" id="customer_pincode" type="text" value="<?= $customer_detail['customer_pincode'] ?>" placeholder="Customer Pincode">

                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="customer_gst_no"> GST No </label>
                                <input class="form-control" name="customer_gst_no" id="customer_gst_no" type="text" value="<?= $customer_detail['customer_gst_no'] ?>" placeholder="Customer GST No">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="pan_number">Pan Number</label>
                                <input class="form-control" name="pan_number" id="pan_number" type="text" value="<?= $customer_detail['pan_number'] ?>" placeholder="Customer PAN No">

                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="payment_terms"> Payment Terms (Optional) </label>
                                <textarea class="form-control" name="payment_terms" id="payment_terms" rows="4"  placeholder="Company Payment Terms"><?= $customer_detail['payment_terms'] ?></textarea>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="client_bank_name">Client Bank Name</label>
                                <input class="form-control" name="client_bank_name" id="client_bank_name" value="<?= $customer_detail['client_bank_name'] ?>" type="text" placeholder="Client Bank Name">

                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="bank_ifsc_code" class="form-label">Bank IFSC Code</label>
                                <input class="form-control" name="bank_ifsc_code" id="bank_ifsc_code" value="<?= $customer_detail['bank_ifsc_code'] ?>" type="text" placeholder="Bank IFSC Code">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="bank_account_number" class="form-label">Bank Account Number</label>
                                <input class="form-control numbersonly" name="bank_account_number" id="bank_account_number" value="<?= $customer_detail['bank_account_number'] ?>" type="text" placeholder="Bank Account Number">

                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="acoount_holder_name" class="form-label">Account Holder Name</label>
                                <input class="form-control" name="acoount_holder_name" id="acoount_holder_name" value="<?= $customer_detail['acoount_holder_name'] ?>" type="text" placeholder="Account Holder Name">
                            </div>
                        </div>

                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-Customer" value="Save"><span class="fal fa-check mr-1"></span>Save Changes</button>

                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>


<script>
    $(document).ready(function () {
        $('#customer_update').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                    company__name: {
                        remote: '<?= base_url() ?>admin/Ccustomer/checkCustomer/<?=$customer_detail['customer_id']?>'
                    }
            },
            messages: {
                company__name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>




<!--Edit customer start -->




<!-- New customer -->


<!-- Edit customer end -->

