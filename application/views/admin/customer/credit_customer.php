<!-- Customer js php -->
<script src="<?php echo base_url() ?>my-assets/js/admin_js/json/customer.js.php" ></script>
<!-- Manage credit customer -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('credit_customer') ?></h1>
            <small><?php echo display('manage_your_credit_customer') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('customer') ?></a></li>
                <li class="active"><?php echo display('credit_customer') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">
        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">

                    <!--<a href="<?php echo base_url('Ccustomer') ?>" class="btn btn-info m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('add_customer') ?> </a>-->

                    <a href="<?php echo base_url('Ccustomer/manage_customer') ?>" class="btn btn-primary m-b-5 m-r-2"><i class="ti-align-justify"> </i>  <?php echo display('manage_customer') ?> </a>

                    <a href="<?php echo base_url('Ccustomer/paid_customer') ?>" class="btn btn-warning m-b-5 m-r-2"><i class="ti-align-justify"> </i>  <?php echo display('paid_customer') ?> </a>

                </div>
            </div>
        </div>

        <!-- Manage credit customer -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body"> 
                        <div class="row">
                            <form action="<?php echo base_url('Ccustomer/credit_customer') ?>" class="form-inline" method="post" accept-charset="utf-8">
                                <div class="col-sm-6">
                                    <label class="select"><?php echo display('customer_name') ?>:</label>
                                    <select class="form-control" name="customer_id" style="width: 300px" required="">
                                        <option value=""></option>
                                        <?php
                                        foreach ($all_credit_customer_list as $customers) {
                                            $sql = "SELECT (SELECT SUM(amount) FROM customer_ledger WHERE d_c ='d' AND customer_id = '" . $customers['customer_id'] . "') as  debit, 
                                                (SELECT SUM(amount) FROM customer_ledger WHERE d_c ='c' AND customer_id = '" . $customers['customer_id'] . "') as  credit";
                                            $query = $this->db->query($sql)->result();
                                            $balance = $query[0]->credit - $query[0]->debit;
                                            if ($balance > 0) {
                                                ?>
                                                <option value="<?php echo $customers['customer_id']; ?>"><?php echo $customers['customer_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <button type="submit" style="margin-top: 25px;"  class="btn btn-primary"><?php echo display('search') ?></button>
                                </div>
                            </form>	
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Manage Customer -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('credit_customer') ?></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="credit_customer_data" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th><?php echo display('sl') ?></th>
                                        <th><?php echo display('customer_name') ?></th>
                                        <th><?php echo display('address') ?></th>
                                        <th><?php echo display('mobile') ?></th>
                                        <th style="text-align:right !Important"><?php echo display('balance') ?></th>
                                        <th style="width: 80px !Important;text-align:center !Important"><?php echo display('action') ?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Manage Customer End -->

<!-- Delete Customer ajax code -->
<script type="text/javascript">

    $(document).ready(function () {
        var _credit_customer_data = $('#credit_customer_data').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>t<'bottom'><'row'<'col-sm-5'i>p>",
            "oTableTools": {
                "aButtons": [
                ]
            },
            buttons: [
                {extend: 'excel', title: 'manage_credit_customer', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3, 4], modifier: {page: 'current'}}},
                {extend: 'print', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3, 4], modifier: {page: 'current'}}}
            ],
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": [0, 5],
                    "orderable": false
                }, {
                    "targets": [0, 5],
                    "className": 'text-center'
                }],
            "ajax": {
                "url": "<?= base_url() ?>Ccustomer/get_credit_customer_list/<?= $customer_id ?>",
                                "type": "POST"
                            }
                        });
                        $("#credit_customer_data thead input").on('keyup change', function () {
                            _credit_customer_data.column($(this).parent().index() + ':visible').search(this.value).draw();
                        });

                        $(document).on('click', '.deleteCustomer', function () {
                            var customer_id = $(this).attr('name');
                            var csrf_test_name = $("[name=csrf_test_name]").val();
                            swal({
                                title: 'Are you sure?',
                                text: "You won't be able to revert this!",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, delete it!'
                            }, function (result) {
                                fullScreenLoader();
                                if (result) {
                                    $.ajax({
                                        type: "POST",
                                        url: '<?php echo base_url('Ccustomer/customer_delete') ?>',
                                        data: {customer_id: customer_id, csrf_test_name: csrf_test_name},
                                        cache: false,
                                        success: function (datas) {
                                            HoldOn.close();
                                            location.reload();
                                        }
                                    });
                                }
                                HoldOn.close();
                            });
                        });
                    });
</script>