<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Customer Ledger
        </h1>      
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <a href="<?php echo base_url('admin/Ccustomer') ?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-plus"> </i> Add Customer </a>

                    <a href="<?php echo base_url('admin/Ccustomer/manage_customer') ?>" class="btn btn-primary bg-trans-gradient waves-themed waves-effect m-b-5 m-r-2"><i class="ti-align-justify"> </i>  Manage Customer </a>

                    <!--<a href="<?php //echo base_url('admin/Csupplier/supplier_sales_details_all')   ?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-align-justify"> </i>  Supplier Sales Details </a>-->

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <form action="<?php echo base_url('admin/Ccustomer/customerledger') ?>" method="post" accept-charset="utf-8" class="mb-2">
                            <?php
                            //$today = date('Y-m-d');
                            ?>
                            <div class="row">
                                <div class="col-md-1 text-right"><b>Select Customer</b></div>
                                <div class="col-md-2">
                                    <select class="form-control select2" name="customer_id" id="customer_id">
                                        <option value="">Select Customer...</option>
                                        <?php
                                        if (isset($customer_list) && !empty($customer_list)) {
                                            foreach ($customer_list as $key => $value) {
                                                if ($customer_id == $value['customer_id']) {
                                                    $sel_c = 'selected';
                                                } else {
                                                    $sel_c = '';
                                                }
                                                ?>
                                                <option value="<?= isset($value['customer_id']) && !empty($value['customer_id']) ? $value['customer_id'] : null ?>" <?= $sel_c ?>><?= isset($value['company__name']) && !empty($value['company__name']) ? $value['company__name'] : null ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-1 text-right"><b>From</b></div>
                                <div class="col-md-2"><input type="date" name="from_date" class="form-control" id="from_date" value="<?= date('Y-m-d', strtotime(FIN_YEAR_STRT_DATE)) ?>" /></div>
                                <div class="col-md-1 text-right"><b>To</b></div>
                                <div class="col-md-2"><input type="date" name="to_date" value="<?= date('Y-m-d', strtotime(FIN_YEAR_END_DATE)) ?>" class="form-control" id="to_date"/></div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary "><i class="fa fa-search-plus" aria-hidden="true"></i> Generate</button>
                                    <a  class="btn btn-warning printPdfCustomerLedger" href="javascript:void(0);" >Print</a>
                                </div>
                            </div>
                        </form>

                        <div id="printableArea" style="margin-left:2px;">
                            <?php if ($customer_name) { ?>
                                <div class="text-center">
                                    <h3><?= $customer_name ?></h3>
                                    <h4>Address: <?= $customer_address ?> </h4>
                                    <h4>Print Date: <?php echo date("d/m/Y h:i:s"); ?> </h4>
                                </div>
                            <?php } ?>

                            <table id="customer_data" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                                <thead class="thead-dark">
                                    <tr>
                                        <th class="text-center">SL</th>
                                        <th class="text-start">Date</th>
                                        <th class="text-start">Customer Name</th>
                                        <th class="text-start">Invoice No</th>
                                        <th class="text-start">Receipt No</th>
                                        <th class="text-start">Description</th>
                                        <th class="text-right">Credit</th>
                                        <th class="text-right">Debit</th>
                                        <th class="text-right">Balance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($ledgers) {
                                        $sl = 0;
                                        ?>
                                        <?php
                                        $debit = $credit = $balance = 0;
                                        foreach ($ledgers as $single) {
                                            $sl++;
                                            ?>
                                            <tr>
                                                <td><?= $sl ?></td>
                                                <td><?php echo date('d-M-Y', strtotime($single['date'])); ?></td>
                                                <td><?php echo $single['company__name']; ?></td>
                                                <?php
                                                if (isset($single['SIID']) && !empty($single['SIID']) && $single['SIID'] != "NA") {
                                                    $style = 'style="text-align: start;"';
                                                    ?>
                                                    <td <?= $style ?>>
                                                        <a href = "<?= base_url() . 'admin/Sale_invoice/saleInvoiceDetails/' . $single['SIID'] ?>">
                                                            <?= $single['SINO'] ?>
                                                        </a>
                                                    </td>
                                                    <?php
                                                } else if (isset($single['SRID']) && !empty($single['SRID']) && $single['SRID'] != "NA") {
                                                    $style = 'style="text-align: start;"';
                                                    ?>
                                                    <td <?= $style ?>>
                                                        <a href = "javascript:void(0);">
                                                            <?= $single['SRNO'] ?>
                                                        </a>
                                                    </td>
                                                    <?php
                                                } else {
                                                    $style = 'style="text-align: start;"';
                                                    ?>
                                                    <td <?= $style ?>>
                                                        <a href="javascript:void(0);">
                                                            —
                                                        </a>
                                                    </td>
                                                    <?php
                                                }
                                                ?>
                                                </td>
                                                <td>
                                                    <?php echo $single['receipt_no'] ?>
        <!--                                                    <a href="<?php echo base_url() . 'Cinvoice/receipt_inserted_data/' . $single['receipt_no']; ?>">
                                                    <?php echo $single['receipt_no'] ?></a>-->

                                                </td>
                                                <td><?php echo $single['description'] ?></td>
                                                <td style="text-align:right;"> 
                                                    <?php
                                                    if ($single['d_c'] == 'c') {
                                                        echo (($position == 0) ? "$currency " : " $currency");
                                                        echo number_format($single['amount'], 2, '.', ',');
                                                        $credit += $single['amount'];
                                                    } else {
                                                        $credit += '0.00';
                                                    }
                                                    ?>
                                                </td>
                                                <td style="text-align:right;"> 
                                                    <?php
                                                    if ($single['d_c'] == 'd') {
                                                        echo (($position == 0) ? "$currency " : " $currency");
                                                        echo number_format($single['amount'], 2, '.', ',');
                                                        $debit += $single['amount'];
                                                    } else {
                                                        $debit += '0.00';
                                                    }
                                                    ?>
                                                </td>
                                                <td align='right'>
                                                    <?php
                                                    $balance = $credit - $debit;
                                                    echo (($position == 0) ? "$currency " : " $currency");
                                                    echo number_format($balance, 2, '.', ',');
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6" align="right"><b><?php echo 'Grand Total' ?>:</b></td>
                                        <td align="right">
                                            <b>
                                                <?php
                                                echo (($position == 0) ? "$currency " : "$currency");
                                                echo number_format((@$credit), 2, '.', ',');
                                                ?>
                                            </b>
                                        </td>
                                        <td align="right">
                                            <b>
                                                <?php
                                                echo (($position == 0) ? "$currency " : "$currency");
                                                echo number_format((@$debit), 2, '.', ',');
                                                ?>
                                            </b>
                                        </td>
                                        <td align="right">
                                            <b>
                                                <?php
                                                echo (($position == 0) ? "$currency " : "$currency");
                                                echo number_format((@$balance), 2, '.', ',');
                                                ?>
                                            </b>
                                        </td>
                                    </tr>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>








<!-- Customer Ledger Start -->

<!-- Customer Ledger End  -->
<script>
    $(document).ready(function () {
        $("#saleDataTable").DataTable({
            responsive: true,
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'excel', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'print', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}}
            ],
            order: [0, 'asc'],
        });

        $('#customer_id').select2({
            placeholder: '-- Select Customer --'
        });

        $(document).on('click', '.printPdfCustomerLedger', function () {
            let customer_id = $('#customer_id').find(':selected').val();
            let start_date = $('#from_date').val();
            let end_date = $('#to_date').val();
            console.log(customer_id);
            if (customer_id != '' && customer_id != null) {
                
                $.ajax({
                    cache: false,
                    method: 'POST',
//                    contentType: false,
//                    processData: false,
                    url: '<?= base_url('admin/Ccustomer/printPdfCustomerLedger') ?>',
                    data: {customer_id: customer_id, start_date: start_date, end_date: end_date},
//                    dataType: 'json',
//                    xhrFields: {
//                        responseType: 'blob'                                                        //xhrFields is what did the trick to read the blob to pdf
//                    },
                    success: function (response, status, xhr) {
                       

                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");

                        // safari doesn't support this yet
                        if (typeof a.download === 'undefined') {
                            window.location = response;
                        } else {
                            a.href = response;
                            a.download = "customer_ledger.pdf";
                            document.body.appendChild(a);
                            a.target = "_blank";
                            a.click();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        
                        swal('Oops...!', 'Something went wrong, Try again!', 'error');
                    }
                });
            } else {
               
                swal('Oops...!', 'Plaese select customer!', 'warning');
            }
            
        });
    });
</script>