<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> <?= isset($sabhasad_data->sabhasad_id) && !empty($sabhasad_data->sabhasad_id) ? 'Edit' : 'Add' ?> Sabhasad
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Sabhasad">Sabhasad</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Sabhasad/addEditSabhasad/' . (isset($sabhasad_data->sabhasad_id) && !empty($sabhasad_data->sabhasad_id) ? $sabhasad_data->sabhasad_id : ''), $arrayName = array('id' => 'addEditSabhasad', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sabhasad_code">Sabhasad Code <span class="text-danger">*</span></label>
                                <input tabindex="1" type="text" class="form-control contactnumber" name="sabhasad_code" id="sabhasad_code" placeholder="Sabhasad Code" required value="<?= isset($sabhasad_data->sabhasad_code) && !empty($sabhasad_data->sabhasad_code) ? $sabhasad_data->sabhasad_code : '' ?>">
                                <div class="invalid-feedback">
                                    Sabhasad Code Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sabhasad_name">Sabhasad Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="sabhasad_name" id="sabhasad_name" placeholder="Sabhasad Name" required value="<?= isset($sabhasad_data->sabhasad_name) && !empty($sabhasad_data->sabhasad_name) ? $sabhasad_data->sabhasad_name : '' ?>">
                                <div class="invalid-feedback">
                                    Sabhasad Name Required / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sabhasad_name_en">Sabhasad Name English<span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="sabhasad_name_en" id="sabhasad_name_en" placeholder="Sabhasad Name English" required value="<?= isset($sabhasad_data->sabhasad_name_en) && !empty($sabhasad_data->sabhasad_name_en) ? $sabhasad_data->sabhasad_name_en : '' ?>">
                                <div class="invalid-feedback">
                                    Sabhasad Name Required / Already Exist
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="mobile">Contact Number <span class="text-danger">*</span></label>
                                <input tabindex="3" type="text" class="form-control contactnumber" name="sabhasad_contact" id="sabhasad_contact" placeholder="Contact Number" required value="<?= isset($sabhasad_data->sabhasad_contact) && !empty($sabhasad_data->sabhasad_contact) ? $sabhasad_data->sabhasad_contact : '' ?>">
                                <div class="invalid-feedback">
                                    Contact Number Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sabhasad_address">Sabhasad Address <span class="text-danger">*</span></label>
                                <textarea tabindex="4" class="form-control address" name="sabhasad_address" id="sabhasad_address" placeholder="Sabhasad Address" required><?= isset($sabhasad_data->sabhasad_address) && !empty($sabhasad_data->sabhasad_address) ? $sabhasad_data->sabhasad_address : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Sabhasad Address Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sabhasad_password">Password <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input tabindex="5" type="text" class="form-control contactnumber" name="sabhasad_password" id="sabhasad_password" placeholder="Password" maxlength="4" <?= isset($sabhasad_data->sabhasad_password) && !empty($sabhasad_data->sabhasad_password) ? '' : 'required' ?>>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" onclick="generatePassword()">Generate</button>
                                    </div>
                                </div>
                                <div class="invalid-feedback">
                                    Password Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sabhasad_confirm_password">Confirm Password <span class="text-danger">*</span></label>
                                <input tabindex="6" type="text" class="form-control contactnumber" name="sabhasad_confirm_password" id="sabhasad_confirm_password" placeholder="Confirm Password" maxlength="4" <?= isset($sabhasad_data->sabhasad_password) && !empty($sabhasad_data->sabhasad_password) ? '' : 'required' ?>>
                                <div class="invalid-feedback">
                                    Confirm Password Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var sabhasad_id = '<?= isset($sabhasad_data->sabhasad_id) && !empty($sabhasad_data->sabhasad_id) ? $sabhasad_data->sabhasad_id : '' ?>';
    $(document).ready(function () {
        $('#addEditSabhasad').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                    sabhasad_name: {
                    remote: {
                        url: "<?= base_url('/admin/Sabhasad/checkSabhasadName/') ?>" + sabhasad_id,
                        type: "get"
                    }
                },
                sabhasad_name_en: {
                    remote: {
                        url: "<?= base_url('/admin/Sabhasad/checkSabhasadNameEnglish/') ?>" + sabhasad_id,
                        type: "get"
                    }
                },
                sabhasad_code: {
                    remote: {
                        url: "<?= base_url('/admin/Sabhasad/checkSabhasadCode/') ?>" + sabhasad_id,
                        type: "get"
                    }
                },
            },
            messages: {
                sabhasad_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                sabhasad_name_en: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                sabhasad_code: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    function generatePassword() {
        var length = 4,
                charset = "0123456789",
                retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        $('#sabhasad_password').val(retVal);
    }
</script>