<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Sabhasad
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Sabhasad/addEditSabhasad">Add Sabhasad</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Sabhasad Code</th>
                                    <th>Sabhasad Name</th>
                                    <th>Sabhasad Contact</th>
                                    <th>Password</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($sabhasad_data) && !empty($sabhasad_data)) {
                                    $sn = 0;
                                    foreach ($sabhasad_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $value->sabhasad_code ?></td> 
                                            <td><a href="javascript:void(0);" class="text-danger sabhasad_ledger" data-sabhasad_id="<?= $value->sabhasad_id ?>"><?= $value->sabhasad_name_en ?><?= isset($value->sabhasad_name) && !empty($value->sabhasad_name) ? ' (' . $value->sabhasad_name . ')' : '' ?></a></td>
                                            <td><?= $value->sabhasad_contact ?></td>
                                            <td><?= $value->raw ?></td>
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Sabhasad/addEditSabhasad/<?= $value->sabhasad_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-id="<?= $value->sabhasad_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_user' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).on('click', '.delete_user', function () {
        var id = $(this).attr('data-id');
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Sabhasad/deleteSabhasad') ?>',
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    ``
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Your record not deleted :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });

    $(document).on('click', '.sabhasad_ledger', function (e) {
        var form = document.createElement("form");
        var element1 = document.createElement("input");

        form.method = "POST";
        form.action = '<?= base_url('admin/Report/saleReportBySabhasad') ?>';

        element1.value = $(this).data('sabhasad_id');
        element1.name = "sabhasad_id";
        form.appendChild(element1);

        document.body.appendChild(form);

        form.submit();
    });
</script>