<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Sabhasad Payment
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Sabhasad/addEditPayment">Add Sabhasad Payment</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <?php echo form_open(base_url() . 'admin/Sabhasad/payment') ?>
                        <div class="row"> 
                            <div class="col-md-2 mb-3">
                                <div class="form-group"> 
                                    <input tabindex="1" type="text" id="" name="startDate" readonly class="form-control date" placeholder="Start Date" value="<?= isset($startDate) && !empty($startDate) ? date('d-m-Y', strtotime($startDate)) : '' ?>">
                                </div> 
                            </div>
                            <div class="col-md-2 mb-3">
                                <div class="form-group">
                                    <input tabindex="2" type="text" id="" name="endDate" readonly class="form-control date" placeholder="End Date" value="<?= isset($endDate) && !empty($endDate) ? date('d-m-Y', strtotime($endDate)) : '' ?>">
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <div class="form-group">
                                    <button type="submit"  class="btn btn-block btn-primary pull-left">Submit</button>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Sabhasad Name</th>
                                    <th>Date</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Total</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($sabhasad_payment_data) && !empty($sabhasad_payment_data)) {
                                    $sn = 0;
                                    foreach ($sabhasad_payment_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->sabhasad_name_en) && !empty($value->sabhasad_name_en) ? $value->sabhasad_name_en : '' ?><?= isset($value->sabhasad_name) && !empty($value->sabhasad_name) ? ' (' . $value->sabhasad_name . ')' : '' ?></td> 
                                            <td data-sort='"<?= isset($value->payment_date) && !empty($value->payment_date) ? strtotime($value->payment_date) : '' ?>"'><?= isset($value->payment_date) && !empty($value->payment_date) ? date('d-m-Y H:i', strtotime($value->payment_date)) : '' ?></td> 
                                            <td><?= $value->milk_quantity ?></td> 
                                            <td><?= $value->milk_price ?></td> 
                                            <td><?= $value->total_payment ?></td> 
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Sabhasad/addEditPayment/<?= $value->payment_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-id="<?= $value->payment_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_user' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).on('click', '.delete_user', function () {
        var id = $(this).attr('data-id');
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Sabhasad/deletePayment') ?>',
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Your record not deleted :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });
</script>