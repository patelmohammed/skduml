<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> <?= isset($payment_data->payment_id) && !empty($payment_data->payment_id) ? 'Edit' : 'Add' ?> Sabhasad Payment
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Sabhasad/payment">Sabhasad Payment</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Sabhasad/addEditPayment/' . (isset($payment_data->payment_id) && !empty($payment_data->payment_id) ? $payment_data->payment_id : ''), $arrayName = array('id' => 'addEditMilkPrice', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-12 mb-3" style="text-align: right;">
                                <h3>Wallet Balance <span class="badge badge-primary">&#8377;&nbsp;<span id="wallet_balance"><?= isset($total_balance) && !empty($total_balance) ? $total_balance : 0 ?></span></span></h3>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="payment_date">Date <span class="text-danger">*</span></label>
                                <input tabindex="1" type="datetime-local" class="form-control" name="payment_date" id="payment_date" placeholder="Date" required value="<?= isset($payment_data->payment_date) && !empty($payment_data->payment_date) ? str_replace(' ', 'T', $payment_data->payment_date) : '' ?>">
                                <div class="invalid-feedback">
                                    Date Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="ref_sabhasad_id">Sabhasad <span class="text-danger">*</span></label>
                                <select tabindex="2" class="select2 form-control" name="ref_sabhasad_id" id="ref_sabhasad_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($sabhasad_data) && !empty($sabhasad_data)) {
                                        foreach ($sabhasad_data as $key1 => $value1) {
                                            ?>
                                            <option value="<?= $value1->sabhasad_id ?>" <?= isset($payment_data->ref_sabhasad_id) && !empty($payment_data->ref_sabhasad_id) ? set_selected($payment_data->ref_sabhasad_id, $value1->sabhasad_id) : '' ?>><?= isset($value1->sabhasad_name_en) && !empty($value1->sabhasad_name_en) ? $value1->sabhasad_name_en : '' ?><?= isset($value1->sabhasad_name) && !empty($value1->sabhasad_name) ? ' (' . $value1->sabhasad_name . ')' : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Sabhasad Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="milk_quantity">Milk Quantity (Ltr) <span class="text-danger">*</span></label>
                                <input tabindex="3" type="text" class="form-control" name="milk_quantity" id="milk_quantity" placeholder="Milk Quantity" required value="<?= isset($payment_data->milk_quantity) && !empty($payment_data->milk_quantity) ? $payment_data->milk_quantity : '' ?>" onkeyup="calculateAll()">
                                <div class="invalid-feedback">
                                    Milk Required
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="milk_price">Milk Price per Ltr <span class="text-danger">*</span></label>
                                <input tabindex="3" type="text" class="form-control" name="milk_price" id="milk_price" placeholder="Milk Price" required value="<?= isset($milk_price_data[0]->milk_price) && !empty($milk_price_data[0]->milk_price) ? $milk_price_data[0]->milk_price : '' ?>" readonly="">
                                <div class="invalid-feedback">
                                    Milk Price Required
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="total_payment">Total <span class="text-danger">*</span></label>
                                <input tabindex="3" type="text" class="form-control" name="total_payment" id="total_payment" placeholder="Total" required readonly="" value="<?= isset($payment_data->total_payment) && !empty($payment_data->total_payment) ? $payment_data->total_payment : '' ?>">
                                <div class="invalid-feedback">
                                    Milk Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var old_payment = '<?= isset($payment_data->total_payment) && !empty($payment_data->total_payment) ? $payment_data->total_payment : 0 ?>';
    var payment_id = '<?= isset($milk_price_data->payment_id) && !empty($milk_price_data->payment_id) ? $milk_price_data->payment_id : '' ?>';
    $(document).ready(function () {
//        $('#payment_date').datepicker({
//            format: 'dd-mm-yyyy',
//            todayHighlight: true,
//            orientation: "bottom left",
//            templates: controls,
//            autoclose: true,
//            onClose: function () {
//                $(this).valid();
//            }
//        });

        $("#ref_sabhasad_id").select2({
            placeholder: "Select sabhasad",
            allowClear: true,
            width: '100%'
        });

        $('#addEditMilkPrice').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                var sabhasad_id = $('#ref_sabhasad_id').val();
                var wallet_balance = 0;
                if (sabhasad_id != undefined && sabhasad_id != null && sabhasad_id != '') {
                    $.ajax({
                        type: "POST",
                        url: '<?= base_url('admin/Sabhasad/getSabhasadWalletBalance') ?>',
                        data: {sabhasad_id: sabhasad_id},
                        success: function (returnData) {
                            var data = JSON.parse(returnData);
                            if (data.result) {
                                wallet_balance = parseFloat(data.total_balance) + parseFloat(old_payment);
                            }
                            if (parseFloat(wallet_balance) >= parseFloat($('#total_payment').val())) {
                                form.submit();
                            } else {
                                swalWithBootstrapButtons.fire("Warning", "Not sufficient balance in wallet.", "error");
                            }
                        }
                    });
                } else {
                    swalWithBootstrapButtons.fire("Warning", "Please select sabhasad first.", "error");
                }
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    function calculateAll() {
        var milk_quantity = $('#milk_quantity').val();
        var milk_price = $('#milk_price').val();

        if ($.trim(milk_quantity) == "" || milk_quantity == undefined || isNaN(milk_quantity)) {
            milk_quantity = 0;
        }
        if ($.trim(milk_price) == "" || milk_price == undefined || isNaN(milk_price)) {
            milk_price = 0;
        }
        var total_payment = milk_quantity * milk_price;

        $('#total_payment').val(total_payment);
    }

    $(document).on('change', '#ref_sabhasad_id', function () {
        var sabhasad_id = $(this).val();
        var wallet_balance = '0';
        if (sabhasad_id != undefined && sabhasad_id != null && sabhasad_id != '') {
            $.ajax({
                type: "POST",
                url: '<?= base_url('admin/Sabhasad/getSabhasadWalletBalance') ?>',
                data: {sabhasad_id: sabhasad_id},
                success: function (returnData) {
                    var data = JSON.parse(returnData);
                    if (data.result) {
                        wallet_balance = data.total_balance;
                    }
                    $('#wallet_balance').html(wallet_balance);
                }
            });
        } else {
            $('#wallet_balance').html(wallet_balance);
            $('#hidden_wallet_balance').val(wallet_balance);

        }
    });
</script>