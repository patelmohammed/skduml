<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> <?= isset($payment_data->credit_id) && !empty($payment_data->credit_id) ? 'Edit' : 'Add' ?> Sabhasad Payment Credit
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Sabhasad/paymentCredit">Sabhasad Payment Credit</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Sabhasad/addEditPaymentCredit/' . (isset($payment_data->credit_id) && !empty($payment_data->credit_id) ? $payment_data->credit_id : ''), $arrayName = array('id' => 'addEditMilkPrice', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="credit_date">Date <span class="text-danger">*</span></label>
                                <input tabindex="1" type="datetime-local" class="form-control" name="credit_date" id="credit_date" placeholder="Date" required value="<?= isset($payment_data->credit_date) && !empty($payment_data->credit_date) ? str_replace(' ', 'T', $payment_data->credit_date) : '' ?>">
                                <div class="invalid-feedback">
                                    Date Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="ref_sabhasad_id">Sabhasad <span class="text-danger">*</span></label>
                                <select tabindex="2" class="select2 form-control" name="ref_sabhasad_id" id="ref_sabhasad_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($sabhasad_data) && !empty($sabhasad_data)) {
                                        foreach ($sabhasad_data as $key1 => $value1) {
                                            ?>
                                            <option value="<?= $value1->sabhasad_id ?>" <?= isset($payment_data->ref_sabhasad_id) && !empty($payment_data->ref_sabhasad_id) ? set_selected($payment_data->ref_sabhasad_id, $value1->sabhasad_id) : '' ?>><?= isset($value1->sabhasad_name_en) && !empty($value1->sabhasad_name_en) ? $value1->sabhasad_name_en : '' ?><?= isset($value1->sabhasad_name) && !empty($value1->sabhasad_name) ? ' (' . $value1->sabhasad_name . ')' : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Sabhasad Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="amount">Amount <span class="text-danger">*</span></label>
                                <input tabindex="3" type="text" class="form-control contactnumber" name="amount" id="amount" placeholder="Amount" required value="<?= isset($payment_data->amount) && !empty($payment_data->amount) ? $payment_data->amount : '' ?>">
                                <div class="invalid-feedback">
                                    Amount Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="remark">Remark</label>
                                <textarea tabindex="4" class="form-control address" name="remark" id="remark" placeholder="Remark"><?= isset($payment_data->remark) && !empty($payment_data->remark) ? $payment_data->remark : '' ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var credit_id = '<?= isset($milk_price_data->credit_id) && !empty($milk_price_data->credit_id) ? $milk_price_data->credit_id : '' ?>';
    $(document).ready(function () {
//        $('#credit_date').datepicker({
//            format: 'dd-mm-yyyy',
//            todayHighlight: true,
//            orientation: "bottom left",
//            templates: controls,
//            autoclose: true,
//            onClose: function () {
//                $(this).valid();
//            }
//        });

        $("#ref_sabhasad_id").select2({
            placeholder: "Select sabhasad",
            allowClear: true,
            width: '100%'
        });

        $('#addEditMilkPrice').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>