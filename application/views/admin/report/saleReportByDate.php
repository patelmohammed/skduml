<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-shopping-bag'></i>Sale Report By Date
        </h1>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <?php echo form_open(base_url() . 'admin/Report/saleReportByDate') ?>
                        <div class="row"> 
                            <div class="col-md-2 mb-3">
                                <div class="form-group"> 
                                    <input tabindex="1" type="text" id="" name="startDate" readonly class="form-control date" placeholder="Start Date" value="<?php echo set_value('startDate'); ?>">
                                </div> 
                            </div>
                            <div class="col-md-2 mb-3">
                                <div class="form-group">
                                    <input tabindex="2" type="text" id="" name="endDate" readonly class="form-control date" placeholder="End Date" value="<?php echo set_value('endDate'); ?>">
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <div class="form-group">
                                    <button type="submit"  class="btn btn-block btn-primary pull-left">Submit</button>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <div class="row ">
                            <div class="col-md-12">
                                <?php
                                if (isset($user_id) && $user_id) {
                                    echo "<h4 style='text-align: center;margin-top: 0px'>User: " . userName($user_id) . "</span></h4>";
                                }
                                ?>
                                <?php
                                echo (isset($start_date) && !empty($start_date) && $start_date) || (isset($end_date) && !empty($end_date) && $end_date) ? "<h4>" : '';
                                echo isset($start_date) && $start_date && isset($end_date) && $end_date ? "Date: " . date('d-m-Y', strtotime($start_date)) . " to " . date('d-m-Y', strtotime($end_date)) : '';
                                echo isset($start_date) && $start_date && !$end_date ? "Date: " . date($this->session->userdata(SITE_NAME . '_admin')['date_format'], strtotime($start_date)) : '';
                                echo isset($end_date) && $end_date && !$start_date ? "Date: " . date($this->session->userdata(SITE_NAME . '_admin')['date_format'], strtotime($end_date)) . '' : '';
                                echo (isset($start_date) && !empty($start_date) && $start_date) || (isset($end_date) && !empty($end_date) && $end_date) ? "</h4>" : '';
                                ?>
                                <table id="sale_report_by_date_datatable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Date</th>
                                            <th>Price</th>
                                            <th>Milk (Ltr)</th>
                                            <th>Total Sale</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $grandTotal = 0;
                                        $totalMilkSale = 0;
                                        if (isset($saleReportByDate)):
                                            foreach ($saleReportByDate as $key => $value) {
                                                $grandTotal += $value->total_debit;
                                                $totalMilkSale += $value->total_milk_sale;
                                                $key++;
                                                ?>
                                                <tr>
                                                    <td style="text-align: center"><?php echo $key; ?></td>
                                                    <td><?= date('d-m-Y H:i', strtotime($value->payment_date)) ?></td>
                                                    <td><?= $value->milk_price ?></td>
                                                    <td><?= $value->total_milk_sale ?></td>
                                                    <td><?= $value->total_debit ?></td>
                                                </tr>
                                                <?php
                                            }
                                        endif;
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th style="width: 2%;text-align: center"></th>
                                            <th style="text-align: right" colspan="2">Total </th>
                                            <th><?= number_format($totalMilkSale, 2) ?></th>
                                            <th><?= number_format($grandTotal, 2) ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).ready(function () {
        var table = $('#sale_report_by_date_datatable').dataTable({
            responsive: true,
            select: true,
            dom: "<'row '<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'lf><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                    extend: 'pdfHtml5',
                    text: '<i class="fal fa-file-pdf "></i>',
                    titleAttr: 'Generate PDF',
                    className: 'btn-outline-danger btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#sale_report_by_date_datatable').data('title'),
                    messageTop: $('#sale_report_by_date_datatable').data('msgtop')
                }, {
                    extend: 'excelHtml5',
                    text: '<i class="fal fa-file-excel "></i>',
                    titleAttr: 'Generate Excel',
                    className: 'btn-outline-success btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#sale_report_by_date_datatable').data('title'),
                    messageTop: $('#sale_report_by_date_datatable').data('msgtop')
                }, {
                    extend: 'csvHtml5',
                    text: '<i class="fal fa-file-csv "></i>',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-primary btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    }
                }, {
                    extend: 'copyHtml5',
                    text: '<i class="fal fa-copy "></i>',
                    titleAttr: 'Copy to clipboard',
                    className: 'btn-outline-info btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#sale_report_by_date_datatable').data('title'),
                    messageTop: $('#sale_report_by_date_datatable').data('msgtop')
                }, {
                    extend: 'print',
                    text: '<i class="fal fa-print "></i>',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-dark btn-sm hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-dark-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#sale_report_by_date_datatable').data('title'),
                    messageTop: $('#sale_report_by_date_datatable').data('msgtop')
                }
            ],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            },
            columnDefs: [{orderable: false, targets: [0, 1, 2, 3, 4]}],
            "language": {
                "lengthMenu": "_MENU_"
            }
        });
    });
</script>