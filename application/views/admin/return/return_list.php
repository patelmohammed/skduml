<!-- Manage Invoice Start -->
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Purchase Return List
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Purchase_return/createPurchaseReturn"> Add Return </a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <div class="row mb-3">
                            <div class="panel-body"> 
                                <div class="form-inline">   
                                    <div class="form-group m-r-15">
                                        <label class="pl-3" for="from_date"><?php echo ('Start Date') ?></label>
                                        <input type="date" name="from_date" class="form-control datepicker ml-2" id="from_date" placeholder="DD-MM-YYYY" value="<?= date('Y-m-d', strtotime(FIN_YEAR_STRT_DATE)) ?>">
                                    </div> 
                                    <div class="form-group ml-4">
                                        <label class="m-r-5" for="to_date"><?php echo ('End Date') ?></label>
                                        <input type="date" name="to_date" class="form-control datepicker ml-2" id="to_date" placeholder="DD-MM-YYYY" value="<?= date('Y-m-d', strtotime(FIN_YEAR_END_DATE)) ?>">
                                    </div>  
                                    <button type="submit" class="btn btn-primary ml-4" id="date_filter_btn"><?php echo ('search') ?></button>
                                </div>
                            </div>
                        </div>   
                        <div>
                            <table id="purchase_return_list" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>SN</th>
                                        <th>Purchase Return No</th>
                                        <th>Supplier</th>
                                        <th>Date</th>
                                        <th>Return Description</th>
                                        <th>Total Product</th>
                                        <th>Grand Total</th>
                                        <th>Created By</th>
                                        <th>Updated By</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).ready(function () {
        getPurchaseReturnList();
    });
    $(document).on('click', '#date_filter_btn', function () {
        $('#purchase_return_list').DataTable().destroy();
        getPurchaseReturnList();
    });
    $(document).on('click', '.deletePurchaseReturn', function () {
        var product_return_id = $(this).attr('name');
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: '<?php echo base_url('admin/Purchase_return/deletePurchaseReturn') ?>',
                            data: {product_return_id: product_return_id},
                            dataType: 'json',
                            cache: false,
                            success: function (datas) {
                                if (datas.status == true) {
                                    location.reload();
                                } else {
                                }
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });

    $(document).on('click', '.printPurchaseReturn', function (e) {
        e.preventDefault();
        var purchase_return_id = $(this).attr('name');
        var PRNO = $(this).attr('data-PRNO');
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('admin/Purchase_return/printPurchaseReturn') ?>',
            data: {purchase_return_id: purchase_return_id},
            cache: false,
            success: function (datas) {
                $('#pdfPurchaseReturn').attr('href', '<?php echo base_url('admin/Purchase_return/printPDFPurchaseReturn') ?>/' + purchase_return_id + '/' + PRNO);
                $('#purchase-return-model').html(datas);
                $('#purchaseReturnModal').modal('show');
            }
        });
    });
    function getPurchaseReturnList() {
        var _product_data = $('#purchase_return_list').DataTable({
            dom: "<'row '<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'lf><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                    extend: 'pdfHtml5',
                    text: '<i class="fal fa-file-pdf "></i>',
                    titleAttr: 'Generate PDF',
                    className: 'btn-outline-danger btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'excelHtml5',
                    text: '<i class="fal fa-file-excel "></i>',
                    titleAttr: 'Generate Excel',
                    className: 'btn-outline-success btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'csvHtml5',
                    text: '<i class="fal fa-file-csv "></i>',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-primary btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    }
                }, {
                    extend: 'copyHtml5',
                    text: '<i class="fal fa-copy "></i>',
                    titleAttr: 'Copy to clipboard',
                    className: 'btn-outline-info btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'print',
                    text: '<i class="fal fa-print "></i>',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-dark btn-sm hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-dark-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            },
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": [0, 9],
                    "orderable": false
                }, {
                    "targets": [0, 9],
                    "className": 'text-center'
                }, {"width": "80px", "targets": 9}],

            "ajax": {
                "type": "POST",
                "url": "<?= base_url() ?>admin/Purchase_return/get_purchase_return_list",
                "data": function (d) {
                    d.start_date = $('#from_date').val();
                    d.end_date = $('#to_date').val();
                }
            }


        });
        $("#purchase_return_list thead input").on('keyup change', function () {
            _product_data.column($(this).parent().index() + ':visible').search(this.value).draw();
        });
    }
</script>
