<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Purchase Return
        </h1>
        <div class="d-flex mr-0">
            <a href="<?php echo base_url('admin/Purchase_return/manageReturnList') ?>" class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed"><i class="ti-align-justify"> </i>Manage Purchase Return</a>
            <a href="<?php echo base_url('admin/Sale_return/manageReturnList') ?>" class="btn btn-primary ml-1 bg-trans-gradient waves-effect waves-themed"><i class="ti-align-justify"> </i>Manage Sale Return</a>
        </div>
    </div>
    <div class="row" id="search_row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-hdr">
                        <h2>Purchase Invoice Details</h2>
                    </div>
                    <div class="panel-content">
                        <form action="<?= base_url() ?>admin/Purchase_return/updatePurchaseReturn/<?= $PRID ?>" class="form-vertical" id="add_purchase_return" name="add_purchase_return" method="post">
                        <div class="col-sm-12">

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="datetime">Invoice Date & Time</label>
                                        <div class="input-group date" id="datetime">
                                            <input type="text" class="form-control" id="datetime" name="datetime" value="<?= isset($purchase_invoice_data['DateTime']) && !empty($purchase_invoice_data['DateTime']) ? date_format(date_create($purchase_invoice_data['DateTime']), 'Y-m-d H:i') : null ?>" disabled="">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="invoice_no">Invoice No.</label>
                                        <input type="text" class="form-control" id="invoice_no" name="invoice_no" value="<?= isset($purchase_invoice_data['PINNO']) && !empty($purchase_invoice_data['PINNO']) ? $purchase_invoice_data['PINNO'] : null ?>" readonly>
                                        <input type="hidden" name="hidden_purchase_invoice_id" id="hidden_purchase_invoice_id" value="<?= isset($purchase_invoice_data['PINID']) && !empty($purchase_invoice_data['PINID']) ? $purchase_invoice_data['PINID'] : null ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="supplier_id">Supplier</label>
                                        <select name="supplier_id" class="form-control " id="supplier_id" tabindex="8" disabled="">
                                            <option value="<?= isset($purchase_invoice_data['RefSupplierID']) && !empty($purchase_invoice_data['RefSupplierID']) ? $purchase_invoice_data['RefSupplierID'] : null ?>" data-address=""><?= isset($purchase_invoice_data['supplier_name']) && !empty($purchase_invoice_data['supplier_name']) ? $purchase_invoice_data['supplier_name'] : null ?></option>
                                        </select>
                                        <input type="hidden" name="hidden_supplier_id" id="hidden_supplier_id" value="<?= isset($purchase_invoice_data['RefSupplierID']) && !empty($purchase_invoice_data['RefSupplierID']) ? $purchase_invoice_data['RefSupplierID'] : null ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="supplier_address" class="col-form-label" >Supplier Address</label><br>
                                        <sapn id="supplier_address"><?= isset($purchase_invoice_data['RefSupplierAddress']) && !empty($purchase_invoice_data['RefSupplierAddress']) ? $purchase_invoice_data['RefSupplierAddress'] : null ?></sapn>
                                        <input type="hidden" value="<?= isset($purchase_invoice_data['RefSupplierAddress']) && !empty($purchase_invoice_data['RefSupplierAddress']) ? $purchase_invoice_data['RefSupplierAddress'] : null ?>" name="hidden_supplier_address" id="hidden_supplier_address">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="return_description">Return Description</label>
                                        <textarea type="text" rows="3" class="form-control" id="return_description" name="return_description"><?= isset($purchase_return_data['ReturnDescription']) && !empty($purchase_return_data['ReturnDescription']) ? $purchase_return_data['ReturnDescription'] : null ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive" id="product_cart">          
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th width="1%">SN</th>
                                                    <th width="20%">Products</th>
                                                    <th width="12%">Product Price</th>
                                                    <th width="11%">Purchase Quantity</th>
                                                    <th width="11%">Return Quantity</th>
                                                    <th width="11%">Discount</th>
                                                    <th width="7%">Tax</th>
                                                    <th width="9%">Tax Amount</th>
                                                    <th width="15%">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $cnt = 0;
                                                if (isset($purchase_return_data['purchase_return_item_data']) && !empty($purchase_return_data['purchase_return_item_data'])) {
                                                    foreach ($purchase_return_data['purchase_return_item_data'] as $k => $product_value) {
                                                        $cnt++;
//                                                        preprint();
                                                        $product_row = '';
                                                        $product_row .= '<tr class="rowCount" data-id="' . $cnt . '" id="row_' . $cnt . '">';
                                                        $product_row .= '<td style="padding-left: 10px;"><p id="sl_' . $cnt . '">' . $cnt . '</p></td>';
                                                        $product_row .= '<td><span style="padding-bottom: 5px;">' . getProductNameById($product_value->refProductID) . '</span></td>';

                                                        $product_row .= '<input type="hidden" id="product_id_' . $cnt . '" name="product_id[]" value="' . $product_value->refProductID . '"/>';
                                                        $product_row .= '<input type="hidden" id="tax_per_' . $cnt . '" name="tax_per[]" value="' . (isset($purchase_return_data['purchase_return_item_tax_data'][$k]->TaxPer) && !empty($purchase_return_data['purchase_return_item_tax_data'][$k]->TaxPer) ? $purchase_return_data['purchase_return_item_tax_data'][$k]->TaxPer : '') . '"/>';
                                                        $product_row .= '<input type="hidden" id="tax_per_amt_' . $cnt . '" name="tax_per_amt[]" value=""/>';
                                                        $product_row .= '<input type="hidden" id="total_tax_' . $cnt . '" name="total_tax[]" value="' . (isset($purchase_return_data['purchase_return_item_tax_data'][$k]->TaxAmt) && !empty($purchase_return_data['purchase_return_item_tax_data'][$k]->TaxAmt) ? $purchase_return_data['purchase_return_item_tax_data'][$k]->TaxAmt : '') . '"/>';
                                                        $product_row .= '<input type="hidden" id="discount_ind_' . $cnt . '" name="discount_ind[]" value="' . $product_value->DiscountAmt . '"/>';
                                                        $product_row .= '<input type="hidden" id="BasicTotal_' . $cnt . '" name="BasicTotal[]" value="' . $product_value->UnitAmt * $product_value->Qty . '"/>';
                                                        $product_row .= '<input type="hidden" id="Tax_id_' . $cnt . '" name="Tax_id[]" value="' . (isset($purchase_return_data['purchase_return_item_tax_data'][$k]->refTaxID) && !empty($purchase_return_data['purchase_return_item_tax_data'][$k]->refTaxID) ? $purchase_return_data['purchase_return_item_tax_data'][$k]->refTaxID : null) . '"/>';

                                                        $product_row .= '<input type="hidden" id="total_amt_' . $cnt . '" name="total_amt[]" value="' . $product_value->TotalAmt . '"/>';
                                                        $product_row .= '<input type="hidden" id="tax_name_' . $cnt . '" name="tax_name[]" value="' . (isset($purchase_return_data['purchase_return_item_tax_data'][$k]->refTaxName) && !empty($purchase_return_data['purchase_return_item_tax_data'][$k]->refTaxName) ? $purchase_return_data['purchase_return_item_tax_data'][$k]->refTaxName : null) . '"/>';

                                                        $product_row .= '<td><input type="text" id="unit_price_' . $cnt . '" name="unit_price[]" onfocus="this.select();" class="form-control desimalcheck aligning" placeholder="Unit Price" value="' . $product_value->UnitAmt . '" onkeyup="return calculateAll();" readonly/><span class="label_aligning"></span></td>';
                                                        $product_row .= '<td><span style="padding-bottom: 5px;">' . (isset($product_value->invoice_qty) && !empty($product_value->invoice_qty) ? $product_value->invoice_qty : 0) . '</span></td>';
                                                        $product_row .= '<td><input type="text" data-countID="' . $cnt . '" id="quantity_amount_' . $cnt . '" name="quantity_amount[]" onfocus="this.select();" class="form-control desimalcheck aligning countID quantity_amount"  placeholder="Quantity" onkeyup="return calculateAll();" value="' . (isset($product_value->Qty) && !empty($product_value->Qty) && $product_value->Qty != 0 ? $product_value->Qty : null) . '" data-purchase_invoice_qty="' . (isset($product_value->returnable_qty) && !empty($product_value->returnable_qty) ? $product_value->returnable_qty : 0) . '" onblur="checkStock(' . $cnt . ')"></td>';
                                                        $product_row .= '<td><input type="text" data-countID="' . $cnt . '" id="discount_' . $cnt . '" name="discount[]" onfocus="this.select();" class="form-control desimalcheck aligning countID discount_percentage" value="' . (isset($product_value->DiscountPer) && !empty($product_value->DiscountPer) ? $product_value->DiscountPer : null) . '" placeholder="Discount" onkeyup="return calculateAll();" readonly /></td>';
                                                        $product_row .= '<td><span id="tax_display_' . $cnt . '">' . (isset($purchase_return_data['purchase_return_item_tax_data'][$k]->refTaxName) && !empty($purchase_return_data['purchase_return_item_tax_data'][$k]->refTaxName) ? $purchase_return_data['purchase_return_item_tax_data'][$k]->refTaxName : null) . '</span></td>';
                                                        $product_row .= '<td><input type="text" id="tax_amount_' . $cnt . '" name="tax_amount[]" class="form-control" value="' . (isset($purchase_return_data['purchase_return_item_tax_data'][$k]->TaxAmt) && !empty($purchase_return_data['purchase_return_item_tax_data'][$k]->TaxAmt) ? $purchase_return_data['purchase_return_item_tax_data'][$k]->TaxAmt : null) . '" placeholder="Tax Amount" readonly /><span class="label_aligning"></span></td>';

                                                        $total = $product_value->TotalAmt + (isset($purchase_return_data['purchase_return_item_tax_data'][$k]->TaxAmt) && !empty($purchase_return_data['purchase_return_item_tax_data'][$k]->TaxAmt) ? $purchase_return_data['purchase_return_item_tax_data'][$k]->TaxAmt : 0);
                                                        $product_row .= '<td><input type="text" id="total_' . $cnt . '" name="total[]" class="form-control aligning" value="' . $total . '" placeholder="Total" readonly /><span class="label_aligning"></span></td>';
                                                        $product_row .= '</tr>';
                                                        echo $product_row;
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-9"></div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="g_total">Grand Total</label>
                                        <input type="text" class="form-control" id="g_total" name="g_total" placeholder="Grand Total" onchange="checkDue();" value="<?= isset($purchase_return_data['GrandTotal']) && !empty($purchase_return_data['GrandTotal']) ? $purchase_return_data['GrandTotal'] : null ?>" readonly />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" style="display:flex; gap:5px;">
                                    <button type="submit" id="add-purchase-return" class="btn btn-primary btn-large" name="add-purchase-return" value="Save">Update</button>
                                    <a href="<?= base_url() ?>admin/Purchase_return/manageReturnList"  class="btn btn-danger btn-large">Cancel</a>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- Purchase Report End -->
<script>
    $(document).ready(function () {
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });
        $(document).on('change', '.quantity_amount', function () {
            let qty = parseFloat($(this).val());
            let invoice_qty = parseFloat($(this).data('purchase_invoice_qty'));
            if (qty > invoice_qty) {
                $(this).val(null);
                swalWithBootstrapButtons.fire({
                    title: "Alert!",
                    text: "Return quantity must be less than or equal to " + invoice_qty,
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
            } else if (qty <= 0) {
                $(this).val(null);
                swalWithBootstrapButtons.fire({
                    title: "Alert!",
                    text: "Return quantity must be more than 0.",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
            } else {
//                alert('ok');
            }
        });
    });

    function calculateAll() {
        var subtotal = 0;
        var i = 1;
        $(".rowCount").each(function () {
            var id = $(this).attr("data-id");
            var unit_price = $("#unit_price_" + id).val();
            var tax = $("#tax_per_" + id).val().split(',');
            var temp = "#sl_" + id;
            var product_id = $("#product_id_" + id).val();
            var quantity = $("#quantity_amount_" + id).val();
            var disc = $("#discount_" + id).val();
            var disc_type = '';
            var temp_amount = '';
            var total_tax = 0;
            $(temp).html(i);
            i++;
            var quantity_amount = $("#quantity_amount_" + id).val();
            if ($.trim(unit_price) == "" || $.isNumeric(unit_price) == false) {
                unit_price = 0;
            }
            if ($.trim(quantity_amount) == "" || $.isNumeric(quantity_amount) == false) {
                quantity_amount = 0;
            }
            if ($.trim(disc) == '' || $.trim(disc) == '%' || $.trim(disc) == '%%' || $.trim(disc) == '%%%' || $.trim(disc) == '%%%%') {
                disc = 0;
            }
            if (disc.length > 0) {
                disc_type = 'percentage';
            }
//            else {
//                disc_type = 'plain';
//            }

            var quantity_amount_and_unit_price = parseFloat($.trim(unit_price)) * parseFloat($.trim(quantity_amount));
            if (disc_type == 'percentage') {
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                temp_amount = quantity_amount_and_unit_price * disc;
                temp_amount = temp_amount / 100;
                $('#discount_ind_' + id).val(temp_amount);
                quantity_amount_and_unit_price = quantity_amount_and_unit_price - temp_amount;
            } else {
                $('#discount_ind_' + id).val(disc);
                quantity_amount_and_unit_price = quantity_amount_and_unit_price - disc;
            }

            if (tax.length > 0) {
                var tax_product = 0;
                tax.forEach(function (key, val) {
                    var tax1 = quantity_amount_and_unit_price;
                    var tax2 = (tax1 * key) / 100;
                    total_tax += tax2;
                    tax_product += tax2
                });
                quantity_amount_and_unit_price = quantity_amount_and_unit_price + tax_product;
            }

            var BasicTotal = quantity * unit_price;
            $('#BasicTotal_' + id).val(BasicTotal);
            var total_amt = BasicTotal - temp_amount;
            $('#total_amt_' + id).val(total_amt);
            $('#total_tax_' + id).val(total_tax);
            $('#tax_amount_' + id).val(total_tax);
            $("#total_" + id).val(quantity_amount_and_unit_price.toFixed(2));
            subtotal += parseFloat($.trim($("#total_" + id).val()));
        });
        if (isNaN(subtotal)) {
            subtotal = 0;
        }
        $("#subtotal").val(subtotal);
        var other = parseFloat($.trim($("#other").val()));
        if ($.trim(other) == "" || $.isNumeric(other) == false) {
            other = 0;
        }

        var grand_total = parseFloat(subtotal) + parseFloat(other);
        grand_total = grand_total.toFixed(2);
        $("#g_total").val(grand_total);
        var paid = $("#paid").val();
        if ($.trim(paid) == "" || $.isNumeric(paid) == false) {
            paid = 0;
        }

        var due = parseFloat(grand_total) - parseFloat(paid);
        $("#due").val(due.toFixed(2));
    }

    function checkDue() {
        let grand_total = $("#g_total").val();
        let paid_amount = $("#paid_amount").val();
        let due_amount = parseFloat(grand_total) - parseFloat(paid_amount);
//        console.log(due_amount);
        $('#due_amount').val(parseFloat(due_amount).toFixed(2));
    }

    function checkStock(suffix) {
        var quantity_amount = $('#quantity_amount_' + suffix).val();
        var quantity_sale_order = $('#quantity_amount_' + suffix).attr('data-purchase_inward_qty');
        quantity_amount = parseFloat(quantity_amount);
        quantity_sale_order = parseFloat(quantity_sale_order);
        if ($.trim(quantity_amount) == "" || quantity_amount == undefined || isNaN(quantity_amount)) {
            quantity_amount = 0;
        }
        if ($.trim(quantity_sale_order) == "" || quantity_sale_order == undefined || isNaN(quantity_sale_order)) {
            quantity_sale_order = 0;
        }
    }

    $(document).on('change', '.discount_percentage', function () {
        let qty = parseFloat($(this).val());
        if (qty > 100) {
            $(this).val(null);
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Discount must be less than or equal to 100%.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
        } else if (qty <= 0) {
            $(this).val(null);
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Discount must be more than 0%.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
        } else {
//                alert('ok');
        }
    });
</script>



