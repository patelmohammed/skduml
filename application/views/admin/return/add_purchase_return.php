<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Purchase Return
        </h1>
        <div class="d-flex mr-0">
            <a href="<?php echo base_url('admin/Purchase_return/manageReturnList') ?>" class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed"><i class="ti-align-justify"> </i>Manage Purchase Return</a>
            <a href="<?php echo base_url('admin/Sale_return/manageReturnList') ?>" class="btn btn-primary ml-1 bg-trans-gradient waves-effect waves-themed"><i class="ti-align-justify"> </i>Manage Sale Return</a>
        </div>
    </div>
    <div class="row" id="search_row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <div class="col-sm-12" style="padding: 0;">
                            <div class="d-flex" style="display: flex; align-items: end; margin-bottom: 15px;">

                                <div class="col-sm-4" style="padding: 0;">
                                    <label for="purchase_invoice_no">Purchase Invoice No:</label>
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" name="purchase_invoice_no" class="form-control" id="purchase_invoice_no" placeholder="Enter Your Purchase Invoice No" required="required">
                                </div>
                                <div class="col-sm-3 text-center">
                                    <button type="button" class="btn btn-success ml-1" id="invoice_search_btn">Search</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>

    $(document).ready(function () {
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });

        $(document).on('click', '#invoice_search_btn', function () {
            let cnt = 0;
            let invoice_no = $('#purchase_invoice_no').val();
            if (invoice_no != '' && invoice_no != null) {
                $.ajax({
                    method: 'POST',
                    url: '<?= base_url() ?>admin/Purchase_return/addPurchaseReturn',
                    data: {invoice_no: invoice_no},
                    dataType: 'json',
                    success: function (response) {
                        if (response.status == true) {
                            $('#purchase_return_row').empty();

                            let invoice_details = '';
                            cnt++;

                            invoice_details += `<form action="<?= base_url() ?>admin/Purchase_return/insertPurchaseReturn" class="form-vertical" id="add_purchase_return" name="add_purchase_return" method="post">`;
                            invoice_details += `<div class="row" id="purchase_return_row">`;
                            invoice_details += `<div class="col-sm-12">`;
                            invoice_details += `<div id="panel-2" class="panel"><div class="panel-container show">`;
                            invoice_details += `<div class="panel-hdr">`;
//                        invoice_details += `<div class="panel-title">`;
                            invoice_details += `<h2>Purchase Invoice Details</h2>`;
//                        invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="panel-content">`;
                            invoice_details += `<div class="row">`;
                            invoice_details += `<div class="col-sm-3">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="datetime">Invoice Date & Time</label>`;
                            invoice_details += `<div class="input-group date" id="datetime">`;
                            invoice_details += `<input type="text" class="form-control" id="datetime" name="datetime" value="` + response.purchase_invoice_data.DateTime + `" required readonly>`;
                            invoice_details += `<span class="input-group-addon">`;
                            invoice_details += `<span class="glyphicon glyphicon-calendar"></span>`;
                            invoice_details += `</span>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="col-sm-3">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="invoice_no">Invoice No.</label>`;
                            invoice_details += `<input type="text" class="form-control" id="invoice_no" name="invoice_no" value="` + response.purchase_invoice_data.PINNO + `" required readonly>`;
                            invoice_details += `<input type="hidden" name="hidden_purchase_invoice_id" id="hidden_purchase_invoice_id" value="` + response.purchase_invoice_data.PINID + `">`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="col-sm-3">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="supplier_id">Supplier</label>`;
                            invoice_details += `<select name="supplier_id" class="form-control " id="supplier_id" required="" tabindex="8" disabled="">`;
                            invoice_details += `<option value="` + response.purchase_invoice_data.RefSupplierID + `" data-address="">` + response.purchase_invoice_data.supplier_name + `</option>`;
                            invoice_details += `</select>`;
                            invoice_details += `<input type="hidden" name="hidden_supplier_id" id="hidden_supplier_id" value="` + response.purchase_invoice_data.RefSupplierID + `">`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="col-sm-3">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="supplier_address" class="col-form-label" >Supplier Address</label><br>`;
                            invoice_details += `<sapn id="supplier_address">` + response.purchase_invoice_data.RefSupplierAddress + `</sapn>`;
                            invoice_details += `<input type="hidden" value="` + response.purchase_invoice_data.RefSupplierAddress + `" name="hidden_supplier_address" id="hidden_supplier_address">`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="col-sm-12">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="return_description">Return Description</label>`;
                            invoice_details += `<textarea type="text" rows="3" class="form-control" id="return_description" name="return_description"></textarea>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<hr>`;
                            invoice_details += `<div class="row">`;
                            invoice_details += `<div class="col-md-12">`;
                            invoice_details += `<div class="table-responsive" id="product_cart">          `;
                            invoice_details += `<table class="table">`;
                            invoice_details += `<thead>`;
                            invoice_details += `<tr>`;
                            invoice_details += `<th width="1%">SN</th>`;
                            invoice_details += `<th width="20%">Products</th>`;
                            invoice_details += `<th width="12%">Product Price</th>`;
                            invoice_details += `<th width="11%">Purchase Qty</th>`;
                            invoice_details += `<th width="11%">Available Qty</th>`;
                            invoice_details += `<th width="11%">Return Qty</th>`;
                            invoice_details += `<th width="11%">Discount</th>`;
                            invoice_details += `<th width="7%">Tax</th>`;
                            invoice_details += `<th width="9%">Tax Amount</th>`;
                            invoice_details += `<th width="15%">Total</th>`;
                            invoice_details += `</tr>`;
                            invoice_details += `</thead>`;
                            invoice_details += `<tbody>`;
                            invoice_details += `</tbody>`;

                            if (response.purchase_invoice_item_data != undefined && response.purchase_invoice_item_data != '' && response.purchase_invoice_item_data != null) {
                                $(response.purchase_invoice_item_data).each(function (key, value) {
                                    key++;
                                    invoice_details += `<tr class="rowCount" data-id="` + key + `" id="row_` + key + `">`;
                                    invoice_details += `<td style="padding-left: 10px;"><p id="sl_` + key + `">` + key + `</p></td>`;
                                    invoice_details += `<td><span style="padding-bottom: 5px;">` + value.product_name_with_category + `</span></td>`;
                                    invoice_details += `<input type="hidden" id="product_id_` + key + `" name="product_id[]" value="` + value.refProductID + `"/>`;
                                    invoice_details += `<input type="hidden" id="tax_per_` + key + `" name="tax_per[]" value="` + value.tax_per + `"/>`;
                                    invoice_details += `<input type="hidden" id="tax_per_amt_` + key + `" name="tax_per_amt[]" value=""/>`;
                                    invoice_details += `<input type="hidden" id="total_tax_` + key + `" name="total_tax[]" value="` + value.TaxAmt + `"/>`;
                                    invoice_details += `<input type="hidden" id="discount_ind_` + key + `" name="discount_ind[]" value="` + value.Discount + `"/>`;
                                    invoice_details += `<input type="hidden" id="BasicTotal_` + key + `" name="BasicTotal[]" value="` + parseFloat(value.Qty) * parseFloat(value.UnitAmt) + `"/>`;
                                    invoice_details += `<input type="hidden" id="Tax_id_` + key + `" name="Tax_id[]" value="` + value.Tax_id + `"/>`;
                                    invoice_details += `<input type="hidden" id="total_amt_` + key + `" name="total_amt[]" value="` + value.TotalAmt + `"/>`;
                                    invoice_details += `<input type="hidden" id="tax_name_` + key + `" name="tax_name[]" value="` + value.tax_name + `"/>`;
                                    invoice_details += `<input type="hidden" id="invoice_product_qty_` + key + `" name="invoice_product_qty_[]" value="` + value.Qty + `"/>`;
                                    invoice_details += `<td><input type="text" id="unit_price_` + key + `" name="unit_price[]" onfocus="this.select();" class="form-control desimalcheck aligning" placeholder="Unit Price" value="` + value.UnitAmt + `" onkeyup="return calculateAll();" required readonly /><span class="label_aligning"></span></td>`;
                                    invoice_details += `<td><span style="padding-bottom: 5px;">` + value.Qty + `</span></td>`;
                                    invoice_details += `<td><span style="padding-bottom: 5px;">` + value.returnable_qty + `</span></td>`;
                                    invoice_details += `<td><input type="text" data-countID="` + key + `" id="quantity_amount_` + key + `" name="quantity_amount[]" onfocus="this.select();" class="form-control desimalcheck aligning countID quantity_amount"  placeholder="Quantity" onkeyup="calculateAll(); checkDue();" value="" data-purchase_invoice_qty="` + value.returnable_qty + `" onblur="checkStock(` + key + `)"></td>`;
                                    invoice_details += `<td><input type="text" data-countID="` + key + `" id="discount_` + key + `" name="discount[]" onfocus="this.select();" class="form-control desimalcheck aligning countID discount_percentage" value="` + value.Discount + `" placeholder="Discount" onkeyup="return calculateAll();" readonly></td>`;
                                    invoice_details += `<td><span id="tax_display_` + key + `">` + value.tax_name + `</span></td>`;
                                    invoice_details += `<td><input type="text" id="tax_amount_` + key + `" name="tax_amount[]" class="form-control" value="` + value.TaxAmt + `" placeholder="Tax Amount" readonly /><span class="label_aligning"></span></td>`;
                                    let total = parseFloat(value.TotalAmt) + parseFloat(value.TaxAmt);
                                    invoice_details += `<td><input type="text" id="total_` + key + `" name="total[]" class="form-control aligning" value="` + parseFloat(total).toFixed(2) + `" placeholder="Total" readonly /><span class="label_aligning"></span></td>`;
                                    invoice_details += `</tr>`;
                                });
                            }

                            invoice_details += `</table>`;
                            invoice_details += `</div> `;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<br>`;
                            invoice_details += `<div class="row">`;
                            invoice_details += `<div class="col-md-9"></div>`;
                            invoice_details += `<div class="col-md-3">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="g_total">Grand Total</label>`;
                            invoice_details += `<input type="text" class="form-control" id="g_total" name="g_total" placeholder="Grand Total" onchange="checkDue();" value="` + response.purchase_invoice_data.GrandTotal + `" readonly />`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="col-md-12">`;
                            invoice_details += `<div class="row" style="display:flex; gap:5px;">`;
                            invoice_details += `<input type="submit" id="add-purchase-return" class="btn btn-primary btn-large" name="add-purchase-return" value="Save" tabindex="10"/>`;
                            invoice_details += `<input type="submit" value="Save and Add another" name="add-purchase-return-another" class="btn btn-large btn-success" id="add-purchase-return-another" tabindex="9">`;
                            invoice_details += `<a href="<?= base_url() ?>admin/Purchase_return/manageReturnList"  class="btn btn-danger btn-large">Cancel</a>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div></div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</form>`;
                            $('#search_row').after(invoice_details);
                        } else {
                            swalWithBootstrapButtons.fire({
                                type: 'error',
                                title: 'Oops...',
                                text: response.msg,
                            });
                        }
                    },
                    error: function () {
                    }
                });
            } else {
                swalWithBootstrapButtons.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Please enter valid invoice number!'
                });
            }
        });

        $(document).on('change', '.quantity_amount', function () {
            let qty = parseFloat($(this).val());
            let invoice_qty = parseFloat($(this).data('purchase_invoice_qty'));
            if (qty > invoice_qty) {
                $(this).val(null);
                swalWithBootstrapButtons.fire({
                    type: 'warning',
                    title: "Alert!",
                    text: "Return quantity must be less than or equal to available quantity!",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
            } else if (qty <= 0) {
                $(this).val(null);
                swalWithBootstrapButtons.fire({
                    type: 'warning',
                    title: "Alert!",
                    text: "Return quantity must be more than 0.",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
            } else {
            }
        });

        $('#add_purchase_return').validate({
            ignore: [],
            rules: {
                quantity_amount: {
                    required: true
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });

    $(document).on('change', '.discount_percentage', function () {
        let qty = parseFloat($(this).val());
        if (qty > 100) {
            $(this).val(null);
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Discount must be less than or equal to 100%.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
        } else if (qty <= 0) {
            $(this).val(null);
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Discount must be more than 0%.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
        } else {
        }
    });

    function calculateAll() {
        var subtotal = 0;
        var i = 1;
        $(".rowCount").each(function () {
            var id = $(this).attr("data-id");
            var unit_price = $("#unit_price_" + id).val();
            var tax = $("#tax_per_" + id).val().split(',');
            var temp = "#sl_" + id;
            var product_id = $("#product_id_" + id).val();
            var quantity = $("#quantity_amount_" + id).val();
            var disc = $("#discount_" + id).val();
            var disc_type = '';
            var temp_amount = '';
            var total_tax = 0;
            $(temp).html(i);
            i++;
            var quantity_amount = $("#quantity_amount_" + id).val();
            if ($.trim(unit_price) == "" || $.isNumeric(unit_price) == false) {
                unit_price = 0;
            }
            if ($.trim(quantity_amount) == "" || $.isNumeric(quantity_amount) == false) {
                quantity_amount = 0;
            }
            if ($.trim(disc) == '' || $.trim(disc) == '%' || $.trim(disc) == '%%' || $.trim(disc) == '%%%' || $.trim(disc) == '%%%%') {
                disc = 0;
            }

            if (disc.length > 0) {
                disc_type = 'percentage';
            }

            var quantity_amount_and_unit_price = parseFloat($.trim(unit_price)) * parseFloat($.trim(quantity_amount));
            if (disc_type == 'percentage') {
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                temp_amount = quantity_amount_and_unit_price * disc;
                temp_amount = temp_amount / 100;
                $('#discount_ind_' + id).val(temp_amount);
                quantity_amount_and_unit_price = quantity_amount_and_unit_price - temp_amount;
            }
            if (tax.length > 0) {
                var tax_product = 0;
                tax.forEach(function (key, val) {
                    var tax1 = quantity_amount_and_unit_price;
                    var tax2 = (tax1 * key) / 100;
                    total_tax += tax2;
                    tax_product += tax2;
                });
                quantity_amount_and_unit_price = quantity_amount_and_unit_price + tax_product;
            }

            var BasicTotal = quantity * unit_price;
            $('#BasicTotal_' + id).val(BasicTotal);
            var total_amt = BasicTotal - disc;
            $('#total_amt_' + id).val(total_amt);
            $('#total_tax_' + id).val(total_tax);
            $('#tax_amount_' + id).val(total_tax);
            $("#total_" + id).val(quantity_amount_and_unit_price.toFixed(2));
            subtotal += parseFloat($.trim($("#total_" + id).val()));
        });
        if (isNaN(subtotal)) {
            subtotal = 0;
        }
        $("#subtotal").val(subtotal);
        var other = parseFloat($.trim($("#other").val()));
        if ($.trim(other) == "" || $.isNumeric(other) == false) {
            other = 0;
        }

        var grand_total = parseFloat(subtotal) + parseFloat(other);
        grand_total = grand_total.toFixed(2);
        $("#g_total").val(grand_total);
        var paid = $("#paid").val();
        if ($.trim(paid) == "" || $.isNumeric(paid) == false) {
            paid = 0;
        }

        var due = parseFloat(grand_total) - parseFloat(paid);
        $("#due").val(due.toFixed(2));
    }

    function checkDue() {
        let grand_total = $("#g_total").val();
        let paid_amount = $("#paid_amount").val();
        let due_amount = parseFloat(grand_total) - parseFloat(paid_amount);
        $('#due_amount').val(parseFloat(due_amount).toFixed(2));
    }

    function checkStock(suffix) {
        var quantity_amount = $('#quantity_amount_' + suffix).val();
        var quantity_sale_order = $('#quantity_amount_' + suffix).attr('data-purchase_inward_qty');
        quantity_amount = parseFloat(quantity_amount);
        quantity_sale_order = parseFloat(quantity_sale_order);
        if ($.trim(quantity_amount) == "" || quantity_amount == undefined || isNaN(quantity_amount)) {
            quantity_amount = 0;
        }
        if ($.trim(quantity_sale_order) == "" || quantity_sale_order == undefined || isNaN(quantity_sale_order)) {
            quantity_sale_order = 0;
        }
    }
</script>