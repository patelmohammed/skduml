<!-- Product details page start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('product_report') ?></h1>
            <small><?php echo display('product_sales_and_purchase_report') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('report') ?></a></li>
                <li class="active"><?php echo display('product_report') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">

                    <a href="<?php echo base_url('Cproduct') ?>" class="btn btn-info m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('add_product') ?> </a>

                    <a href="<?php echo base_url('Cproduct/manage_product') ?>" class="btn btn-primary m-b-5 m-r-2"><i class="ti-align-justify"> </i>  <?php echo display('manage_product') ?> </a>

                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('product_details') ?> </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <h2>
                            <span style="font-weight:normal;"><?php echo display('product_name') ?>: </span>
                            <span style="color:#005580;">{product_name} - ({category_name})</span>
                        </h2>
                        <h4>
                            <span style="font-weight:normal;">Product Type:</span>
                            <span style="color:#005580;">{product_group_name}</span>
                        </h4>
                        <h4>
                            <span style="font-weight:normal;"><?php echo display('price') ?>:</span> 
                            <span style="color:#005580;"><?= (($position == 0) ? "$currency {price}" : "{price} $currency") ?></span>
                        </h4>
                        <table class="table">
                            <tr>
                                <th><?php echo display('total_purchase') ?> = <span style="color:#ff0000;"><?= (($position == 0) ? "$currency {total_purchase}" : "{total_purchase} $currency") ?></span></th>
                                <th><?php echo display('total_sales') ?> = <span style="color:#ff0000;"><?= (($position == 0) ? "$currency {total_sales}" : "{total_sales} $currency") ?></span></th>
                                <th><?php echo display('stock') ?> = <span style="color:#ff0000;"> {stock}</span></th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Total Purchase report -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('purchase_report') ?> </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="purchaseDataTable" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th><?php echo display('date') ?></th>
                                        <th>Purchase Invoice No</th>
                                        <th><?php echo display('supplier_name') ?></th>
                                        <th>Landed Cost</th>
                                        <th><?php echo display('rate') ?></th>
                                        <th>Quantity</th>
                                        <th>Discount</th>
                                        <th>Tax Amount</th>
                                        <th style="text-align:right;"><?php echo display('total_ammount') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($purchaseData)) {
                                        $sl = 0;
                                        foreach ($purchaseData as $k1 => $v1) {
                                            $sl++;
                                            ?>
                                            <tr>
                                                <td><?= $sl ?></td>
                                                <td><?= date('d-m-Y', strtotime($v1->DateTime)) ?></td>
                                                <td>
                                                    <a href="<?php echo base_url() . 'Cpurchase/purchase_details_data/' . $v1->PINID; ?>">
                                                        <?= $v1->PINNO ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="<?php echo base_url() . 'Csupplier/supplier_details/' . $v1->RefSupplierID; ?>"><?= getSupplierNameById($v1->RefSupplierID) ?></a>
                                                </td>
                                                <td  style="text-align:right;"><?php echo (($position == 0) ? "$currency " . $v1->LandedCost : $v1->LandedCost . " $currency") ?></td>
                                                <td  style="text-align:right;"><?php echo (($position == 0) ? "$currency " . $v1->UnitAmt : $v1->UnitAmt . " $currency") ?></td>
                                                <td  style="text-align:right;"><?= $v1->Qty ?></td>
                                                <td  style="text-align:right;"><?php echo (($position == 0) ? "$currency " . $v1->Discount : $v1->Discount . " $currency") ?></td>
                                                <td  style="text-align:right;"><?php echo (($position == 0) ? "$currency " . $v1->TaxAmt : $v1->TaxAmt . " $currency") ?></td>
                                                <?php
                                                $Total = $v1->TotalAmt;
                                                if (!empty($v1->TaxAmt)) {
                                                    $Total += $v1->TaxAmt;
                                                }
                                                ?>
                                                <td style="text-align:right;"> <?php echo (($position == 0) ? "$currency " . number_format($Total, 2) : number_format($Total, 2) . " $currency") ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6" style="text-align:right;"><b><?php echo display('total') ?></b></td>
                                        <td style="text-align:right;"> <?php //  $total_purchase  ?></td>
                                        <td colspan="2" style="text-align:right;"><b><?php echo display('grand_total') ?></b></td>
                                        <td style="text-align:right;"><b> <?php echo (($position == 0) ? "$currency " . $purchaseTotalAmount : $purchaseTotalAmount . " $currency") ?></b></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Total sales report -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('sales_report') ?> </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="saleDataTable" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th><?= display('date') ?></th>
                                        <th>Sale Invoice No</th>
                                        <th><?php echo display('customer_name') ?></th>
                                        <th><?php echo display('rate') ?></th>
                                        <th>Qty</th>
                                        <th>Discount</th>
                                        <th>Tax Amount</th>
                                        <th style="text-align:right;"><?php echo display('total_ammount') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($salesData) {
                                        $sl = 0;
                                        foreach ($salesData as $key => $value) {
                                            $sl++;
                                            ?>
                                            <tr>
                                                <td><?= $sl ?></td>
                                                <td><?= date('d-m-Y', strtotime($value->DateTime)) ?></td>
                                                <td>
                                                    <a href="<?php echo base_url() . 'Cinvoice/invoice_inserted_data/' . $value->SIID; ?>">
                                                        <?= $value->SINO ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="<?php echo base_url() . 'Ccustomer/customerledger/' . $value->RefCustomerID; ?>"><?= getCustomerNameById($value->RefCustomerID) ?></a>
                                                </td>
                                                <td style = "text-align:right;"> <?php echo (($position == 0) ? "$currency " . $value->UnitAmt : $value->UnitAmt . " $currency") ?></td>
                                                <td style = "text-align:right;"><?= $value->Qty ?></td>
                                                <td style = "text-align:right;"> <?php echo (($position == 0) ? "$currency " . $value->DiscountAmt : $value->DiscountAmt . " $currency") ?></td>
                                                <td style = "text-align:right;"> <?php echo (($position == 0) ? "$currency " . $value->TaxAmt : $value->TaxAmt . " $currency") ?></td>
                                                <?php
                                                $Total = $value->TotalAmt;
                                                if (!empty($value->TaxAmt)) {
                                                    $Total += $value->TaxAmt;
                                                }
                                                ?>
                                                <td style="text-align:right;"> <?php echo (($position == 0) ? "$currency " . number_format($Total, 2) : number_format($Total, 2) . " $currency") ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4" style="text-align:right;"></td>
                                        <td class="text-right"><b><?php echo display('total') ?></b></td>
                                        <td  style="text-align:right;"><?php //  $total_sales  ?></td>
                                        <td colspan="2" class="text-right"><b><?php echo display('grand_total') ?></b></td>
                                        <td  style="text-align:right;"><b> <?php echo (($position == 0) ? "$currency " . $salesTotalAmount : $salesTotalAmount . " $currency") ?></b></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Production Report</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="productionDataTable" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Date</th>
                                        <th>Reference No</th>
                                        <th><?php echo display('supplier_name') ?></th>
                                        <th>Total Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($productionData) && $productionData && !empty($productionData)) {
                                        $i = 1;
                                        $sl = 0;
                                        foreach ($productionData as $key => $value) {
                                            $sl++;
                                            if ($value->production_lot_no == '') {
                                                break;
                                            }
                                            ?>
                                            <tr>
                                                <td><?= $sl ?></td>
                                                <td><?= dateprint($value->production_start_datetime) ?></td>
                                                <td>
                                                    <a href="<?php echo base_url() . 'Cproduction/production_details_data/' . $value->production_id; ?>">
                                                        <?= $value->production_lot_no ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="<?php echo base_url() . 'Csupplier/supplier_details/' . $value->supplier_id; ?>"><?= getSupplierNameById($value->supplier_id) ?></a>
                                                </td>
                                                <?php
                                                $total_qty = $value->product_qty * $value->consumption;
                                                ?>
                                                <td><?= number_format($total_qty, 2) ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Product details page end -->
<script>
    $(document).ready(function () {
        $("#purchaseDataTable").DataTable({
            responsive: true,
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'excel', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'print', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}}
            ],
            order: [1, 'desc'],
        });
        $("#saleDataTable").DataTable({
            responsive: true,
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'excel', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'print', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}}
            ],
            order: [1, 'desc'],
        });
        $("#productionDataTable").DataTable({
            responsive: true,
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'excel', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}},
                {extend: 'print', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3], modifier: {page: 'current'}}}
            ],
            order: [1, 'desc'],
        });
    });
</script>