<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Product
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Product/manage_product">Product</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Product/product_update', array('class' => '', 'id' => 'product_update', 'name' => 'product_update')) ?>
                    <input type="hidden" name="product_id" value="<?= $product_detail['id'] ?>">
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="product_name">Product Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Product Name" required value="<?= $product_detail['product_name'] ?>">
                                <div class="invalid-feedback">
                                    Product Name Required or Already Used
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="serial_no">HSN Code</label>
                                <input type="text" class="form-control" name="serial_no" id="serial_no" placeholder="HSN Code" value="<?= $product_detail['serial_no'] ?>">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="product_barcode">Barcode</label>
                                <input type="text" class="form-control" name="product_barcode" id="product_barcode" placeholder="Barcode" value="<?= $product_detail['Product_Barcode'] ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="category_id">Category <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="category_id" name="category_id" required="">
                                    <option value="">Select One</option>
                                    <?php
                                    foreach ($category_list as $key => $value3) {
                                        ?>
                                        <option value="<?= $value3['category_id'] ?>"
                                        <?php
                                        if ($value3['category_id'] == $product_detail['category_id']) {
                                            echo "selected";
                                        }
                                        ?>
                                                ><?= $value3['category_name'] ?> </option>
                                            <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="unit">Unit <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="unit" name="unit" required="">
                                    <option value="">Select One</option>
                                    <?php
                                    foreach ($unit_list as $single) {
                                        if ($single['unit_id'] == $product_detail['unit']) {
                                            ?>
                                            <option selected value="<?php echo $single['unit_id']; ?>">
                                                <?php echo $single['unit_name']; ?>
                                            </option>
                                        <?php } else { ?>
                                            <option  value="<?php echo $single['unit_id']; ?>">
                                                <?php echo $single['unit_name']; ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sell_price">Sell Price <span class="text-danger">*</span></label>
                                <input type="text" class="form-control price" name="sell_price" id="sell_price" placeholder="Sell Price" required value="<?= $product_detail['price'] ?>">
                                <div class="invalid-feedback">
                                    Sales Price Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="tax">Tax <span class="text-danger">*</span></label>
                                <select name="tax[]" id="tax" class="form-control select2" required="" >
                                    <?php
                                    if ($tax_list) {
                                        foreach ($tax_list as $key => $taxvalue) {
                                            ?>                                            
                                            <option value="<?= $taxvalue->tax_id ?>"
                                            <?php
                                            foreach ($tax_select as $key => $taxvalue2) {
                                                if ($taxvalue->tax_id == $taxvalue2->tax_id) {
                                                    echo "selected";
                                                }
                                            }
                                            ?>
                                                    ><?= $taxvalue->tax_name ?> - <?= $taxvalue->tax ?>%</option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="image">Product Image </label>
                                <input type="file" class="form-control" name="image" id="image">
                                <img class="img img-responsive text-center" src="<?= isset($product_detail['image']) && !empty($product_detail['image']) && file_exists($product_detail['image']) ? base_url() . $product_detail['image'] : base_url('assets/admin/img/product.png') ?>" height="80" width="80" style="padding: 5px;">
                                <input type="hidden" value="<?= isset($product_detail['image']) && !empty($product_detail['image']) && file_exists($product_detail['image']) ? $product_detail['image'] : 'assets/admin/img/product.png' ?>" name="old_image">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="description">Product Description</label>
                                <textarea class="form-control" name="description" id="description" rows="3" placeholder="Product Details"><?= $product_detail['product_details'] ?></textarea>
                            </div>
                        </div>

                        <div class="table-responsive mt-4">
                            <table class="table table-bordered table-hover"  id="product_table">
                                <thead>
                                    <tr>
                                        <th class="">Supplier<i class="text-danger">*</i></th>
                                        <th class="">Supplier Price<i class="text-danger">*</i></th>
                                        <th class="">Landed Cost<i class="text-danger">*</i></th>
                                        <th class="">Opening Stock<i class="text-danger">*</i></th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="proudt_item">
                                    <?php
                                    $cnt = 0;
                                    $tab_cnt = 11;
                                    if (!empty($supplier_product_data) && $supplier_product_data != '') {
                                        foreach ($supplier_product_data as $sk => $sv) {
                                            $cnt++;
                                            ?>
                                            <tr class="rowCount" id="tr_<?= $cnt ?>">
                                                <td style="width: 30%">
                                                    <select name="supplier_id[<?= $cnt ?>]" class="form-control suppliers select2" required="">
                                                        <?php
                                                        foreach ($supplier_list as $sl => $slv) {
                                                            if ($sv['supplier_id'] == $slv['supplier_id']) {
                                                                ?>
                                                                <option selected="" value="<?= $slv['supplier_id'] ?>"><?= $slv['supplier_name'] ?></option>
                                                            <?php } else {
                                                                ?>
                                                                <option value="<?= $slv['supplier_id'] ?>"><?= $slv['supplier_name'] ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                                <td class="text-center" style="width: 20%">
                                                    <input type="text"  class="form-control text-right price" name="supplier_price[<?= $cnt ?>]" placeholder="0.00"  min="0" value="<?= $sv['supplier_price'] ?>" required=""/>
                                                    <span></span>
                                                </td>
                                                <td class="text-center" style="width: 20%">
                                                    <input type="text"  class="form-control text-right price" name="LandedCost[<?= $cnt ?>]" placeholder="0.00"  min="0" value="<?= $sv['LandedCost'] ?>" required=""/>
                                                    <span></span>
                                                </td>
                                                <td class="text-center" style="width: 20%">
                                                    <input type="text"  class="form-control text-right price" name="OpeningStock[<?= $cnt ?>]" placeholder="0.00"  min="0" value="<?= $sv['opening_stock'] ?>" required=""/>
                                                    <span></span>
                                                </td>

                                                <td class="text-center" style="width: 10%"> 
                                                    <button  type="button" id="add_purchase_item" class="btn btn-sm btn-primary mr-2" name="add-invoice-item" onClick="addpruduct('proudt_item', '<?= count($supplier_list) ?>', '<?= count($supplier_product_data) ?>');" /><i class="fa fa-plus" aria-hidden="true"></i></button> 
                                                    <button  class="btn btn-sm btn-danger" type="button" value="delete" onclick="deleteRow(this)"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr class="rowCount" id="tr_1">
                                            <td style="width: 30%">
                                                <select id="supplier_id_1" name="supplier_id[1]" class="suppliers form-control select2"  required="">
                                                    <option value=""> select Supplier</option>
                                                    <?php
                                                    if (isset($supplier) && !empty($supplier)) {
                                                        foreach ($supplier as $sk => $sv) {
                                                            ?>
                                                            <option value="<?= $sv['supplier_id'] ?>" <?= $sv['supplier_id'] == 'EX93D37LXEZKDBTGB4P2' ? 'selected' : '' ?>><?= $sv['supplier_name'] ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td class="text-center" style="width: 20%">
                                                <input type="text" class="form-control text-right price" name="supplier_price[1]" placeholder="0.00"  min="0" required=""/>
                                                <span></span>
                                            </td>
                                            <td class="text-center" style="width: 20%">
                                                <input type="text" class="form-control text-right price" name="LandedCost[1]" placeholder="0.00"  min="0" required=""/>
                                                <span></span>
                                            </td>
                                            <td class="text-center" style="width: 20%">
                                                <input type="text"  class="form-control text-right price" name="OpeningStock[1]" placeholder="0.00"  min="0" required=""/>
                                                <span></span>
                                            </td>
                                            <td class="text-center" style="width: 10%">
                                                <button type="button" id="add_purchase_item" class="btn btn-sm btn-primary mr-2" name="add-invoice-item" onClick="addpruduct('proudt_item', '<?= count($supplier_list) ?>');"  >
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button> 
                                                <button class="btn btn-sm btn-danger" type="button" value="delete" onclick="deleteRow(this)" >
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-product" value="Save"><span class="fal fa-check mr-1"></span>Save</button>
                        <button type="submit" class="btn btn-warning ml-1 waves-effect waves-themed" name="add-product-another" value="Save & Add Another"><span class="fal fa-check mr-1"></span>Save & Add Another</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<table  style="display: none">
    <tr id="new_tr">
        <td style="width: 30%">
            <select id="tmp_1" name="tmp_supplier_id" class="suppliers form-control dont-select-me" required="">
                <option value=""> select Supplier</option>
                <?php
                if ($supplier) {
                    foreach ($supplier as $sk => $sv) {
                        ?>
                        <option value="<?= $sv['supplier_id'] ?>"><?= $sv['supplier_name'] ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <span></span>
        </td>
        <td class="text-center" style="width: 20%">
            <input type="text" class="form-control text-right" id="tmp_2" name="tmp_supplier_price" placeholder="0.00"   min="0" required=""/>
            <span></span>
        </td>
        <td class="text-center" style="width: 20%">
            <input type="text"  class="form-control text-right" id="tmp_3" name="tmp_LandedCost" placeholder="0.00"  min="0" required=""/>
            <span></span>
        </td>
        <td class="text-center" style="width: 20%">
            <input type="text" class="form-control text-right" id="tmp_4" name="tmp_opening_stock" placeholder="0.00"  min="0" required=""/>
            <span></span>
        </td>
        <td class="text-center" style="width: 10%">
            <button type="button" id="add_purchase_item" class="btn btn-icon btn-sm hover-effect-dot btn-outline-info" name="add-invoice-item" onClick="addpruduct('proudt_item', '<?= isset($supplier) && !empty($supplier) ? count($supplier) : 0 ?>');">
                <i class="fa fa-plus-square" aria-hidden="true"></i>
            </button> 
            <button class="btn btn-icon btn-sm hover-effect-dot btn-outline-warning" type="button" value="delete" onclick="deleteRow(this)">
                <i class="fa fa-trash" aria-hidden="true"></i>
            </button>
        </td>
    </tr>
</table>

<script>
    $(document).ready(function () {
        $('#product_update').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                product_name: {
                    remote: '<?= base_url() ?>admin/Product/checkProduct/<?= $product_detail['id'] ?>'
                                    },
                                    product_barcode: {
                                        remote: '<?= base_url() ?>admin/Product/checkProductBarcode/<?= $product_detail['id'] ?>'
                                                        }
                                                    },
                                                    messages: {
                                                        product_name: {
                                                            remote: jQuery.validator.format("{0} is already in use, Try another modal")
                                                        },
                                                        product_barcode: {
                                                            remote: jQuery.validator.format("{0} is already in use, Try another modal")
                                                        }
                                                    }, submitHandler: function (form) {
                                                        form.submit();
                                                    },
                                                    errorPlacement: function (error, element) {
                                                        return true;
                                                    }
                                                });
                                            });
                                            var count = 2;
                                            var limits = 500;
                                            var cnt = 1;
                                            //Add purchase input field
                                            function addpruduct(e, total_supplier = '', selected_supplier = '') {

                                                if (selected_supplier != '') {
                                                    cnt = selected_supplier;
                                                }
                                                cnt++;

                                                if ($('.rowCount').length == total_supplier) {
                                                    swalWithBootstrapButtons.fire({
                                                        title: "Alert!",
                                                        type: 'warning',
                                                        text: 'There are no more suppliers to add.',
                                                    });
                                                } else {
                                                    var t = $("#new_tr").html();
                                                    $("tbody#proudt_item").append('<tr class="rowCount" id="tr_' + cnt + '">' + t + '</tr>');
                                                    $('#tr_' + cnt).find('#tmp_1').removeClass('dont-select-me');
                                                    $('#tr_' + cnt).find('#tmp_1').attr('name', 'supplier_id[' + cnt + ']');
                                                    $('#tr_' + cnt).find('#tmp_2').attr('name', 'supplier_price[' + cnt + ']');
                                                    $('#tr_' + cnt).find('#tmp_3').attr('name', 'LandedCost[' + cnt + ']');
                                                    $('#tr_' + cnt).find('#tmp_4').attr('name', 'OpeningStock[' + cnt + ']');
                                                    $('#tr_' + cnt).find('#tmp_1').removeAttr('id');
                                                    $('#tr_' + cnt).find('#tmp_2').removeAttr('id');
                                                    $('#tr_' + cnt).find('#tmp_3').removeAttr('id');
                                                    $('#tr_' + cnt).find('#tmp_5').removeAttr('id');
                                                    $('#tr_' + cnt).find('select').select2({
                                                        placeholder: "Select option",
                                                        allowClear: true,
                                                        width: '100%'
                                                    });
                                            }
                                            }
                                            function deleteRow(e) {
                                                var t = $("#product_table > tbody > tr").length;
                                                if (1 == t) {
                                                    swalWithBootstrapButtons.fire({title: 'Alert!', type: 'warning', text: "There is only one row you can't delete."});
                                                } else {
                                                    var a = e.parentNode.parentNode;
                                                    a.parentNode.removeChild(a)
                                                }
                                            }
                                            $(document).on('change', '.suppliers', function () {
                                                var serials = [];
                                                $("#product_update .suppliers").each(function () {
                                                    if ($(this).val() != '') {
                                                        serials.push($.trim($(this).val()));
                                                    }
                                                });
                                                var new_serials = serials.uniqueK();
                                                if (serials.length != new_serials.length) {
                                                    swalWithBootstrapButtons.fire('Alert!', 'Supplier is already selected, Select another supplier', 'error');
                                                    $(this).val(null).trigger('change');
                                                }
                                            });
</script>