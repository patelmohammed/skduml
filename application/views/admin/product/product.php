<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Manage Product
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Product">Add Product</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
<!--                        <form action="<?php echo base_url('admin/Product/manage_product') ?>" method="post" accept-charset="utf-8" class="mb-2">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="select">Product Name:</label>
                                    <select class="form-control select2" name="product_id">
                                        <option value="">Select product...</option>
                                        <?php
                                        if (!empty($all_product_list)) {
                                            foreach ($all_product_list as $key => $value) {
                                                ?>
                                                <option value="<?= $value['product_id'] ?>"><?= $value['product_name'] ?>( <?= $value['category_name'] ?> )</option>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Search</button>
                                </div>
                            </div>
                        </form>-->
                        <table id="product_data" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-center">SN</th>
                                    <th>Product Name</th>
                                    <th>Product Category</th>
                                    <th>Supplier Name</th>
                                    <th>Sale Price</th>
                                    <th>Landed Cost</th>
                                    <th>Supplier Price</th>
                                    <th>Opening Stock</th>
                                    <th class="text-center notexport">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $('body').on('click', '.deleteProduct', function () {
        var id = $(this).attr('name');
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Product/product_delete') ?>/' + id,
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Oops!",
                                            returnData.msg,
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });

    $(document).ready(function () {
        var _product_data = $('#product_data').DataTable({
            dom: "<'row '<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'lf><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                    extend: 'pdfHtml5',
                    text: '<i class="fal fa-file-pdf "></i>',
                    titleAttr: 'Generate PDF',
                    className: 'btn-outline-danger btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'excelHtml5',
                    text: '<i class="fal fa-file-excel "></i>',
                    titleAttr: 'Generate Excel',
                    className: 'btn-outline-success btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'csvHtml5',
                    text: '<i class="fal fa-file-csv "></i>',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-primary btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    }
                }, {
                    extend: 'copyHtml5',
                    text: '<i class="fal fa-copy "></i>',
                    titleAttr: 'Copy to clipboard',
                    className: 'btn-outline-info btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'print',
                    text: '<i class="fal fa-print "></i>',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-dark btn-sm hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-dark-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            },
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": [0, 8],
                    "orderable": false
                }, {
                    "targets": [0, 8],
                    "className": 'text-center'
                }, {"width": "80px", "targets": 8}],
            "ajax": {"url": "<?= base_url() ?>admin/Product/get_product_list<?= isset($product_id) ? '/' . $product_id : '' ?>", "type": "POST"}
                    });
                    $("#product_data thead input").on('keyup change', function () {
                        _product_data.column($(this).parent().index() + ':visible').search(this.value).draw();
                    });
                });
                $(document).on('click', '.generateBarcode', function () {
                    var product_id = $(this).attr('name');
                    var csrf_test_name = $("[name=csrf_test_name]").val();
                    swalWithBootstrapButtons.fire({
                        title: 'Generate Barcode',
                        text: 'Enter Column Count<br><input id="input1" type="number" class="swalInput" style="display:block !important;"><br>Enter Qty for Barcode<br><input id="input2" type="number" class="swalInput" style="display:block !important;">',
                        html: true,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Generate'
                    }, function (result) {
                        var per_row = $('#input1').val();
                        var barcode_qty = $('#input2').val();
                        if (per_row != '' && per_row != null && barcode_qty != '' && barcode_qty != null) {
                            location.href = '<?= base_url() ?>admin/Product/generateBarcode?pid=' + product_id + '&qty=' + barcode_qty + '&per_row=' + per_row;
                        }
                    });
                });
</script>
