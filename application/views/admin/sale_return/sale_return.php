<script src="<?php echo base_url() ?>my-assets/js/admin_js/json/product.js" type="text/javascript"></script>
<!-- Add Product Start -->

<style type="text/css">
    /* second checbox  */
    input[type=checkbox] {
        display: none;
    }
    input[type=checkbox] + label {
        display: inline-block;
        position: relative;
        padding: 8px;
        background-color: white;
        border: 1px solid green;
        border-radius: 5px;
        width:40px;
        height: 30px;
    }
    input[type=checkbox]:checked + label {
        background-color: green;
        color: #A4B7C6;
    }
    input[type=checkbox]:checked + label:after {
        position: absolute;
        left: 12px;
        top: 0px;
        color: #fff; 
        content: '\2714'; 
        font-size: 20px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1>Sale Return</h1>
            <small>Add Sale Return</small>
            <ol class="breadcrumb">
                <li><a href="index.html"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#">Return</a></li>
                <li class="active">Sale Return</li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">

                    <a href="<?php echo base_url('Sale_return/manageReturnList') ?>" class="btn btn-primary m-b-5 m-r-2"><i class="ti-align-justify"> </i>Sale Return List</a>

                </div>
            </div>
        </div>

        <!-- Add Product -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Sale Return</h4>
                        </div>
                    </div>
                    <?php echo form_open_multipart('Sale_return/insertSaleReturn', array('class' => 'form-vertical', 'id' => 'insert_sale_return')) ?>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="datetime">Return Date & Time</label>
                                    <input type="text" class="form-control" name="datetime" value="<?php //  date('d-m-Y H:i', strtotime($sale_outward['DateTime'])); ?>" required readonly="">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="customer_id">Customer</label>
                                    <select name="customer_id" class="form-control dont-select-me" id="customer_id" required="" tabindex="8" disabled="">
                                        <option value="" data-address=""><?php //  getCustomerNameById($sale_outward['RefCustomerID']) ?></option>
                                    </select>
                                    <input type="hidden" name="hidden_customer_id" id="hidden_customer_id" value="<?php //  $sale_outward['RefCustomerID'] ?>">
                                    <input type="hidden" name="hidden_SOID" value="<?php // $SOID ?>" id="hidden_SOID">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="supplier_address" class="col-form-label" >Customer Details</label><br>

                                    <sapn id="supplier_address"><?php // $sale_outward['RefCustomerAddress'] != '' ? '<b>Address : </b>' . $sale_outward['RefCustomerAddress'] . '<br>' : '' ?></sapn>
                                    <input type="hidden" value="<?php // $sale_outward['RefCustomerAddress'] ?>" name="hidden_customer_address" id="hidden_customer_address">

                                    <sapn id="supplier_address"><?php // $sale_outward['RefCustomerPhone'] != '' ? '<b>Phone : </b>' . $sale_outward['RefCustomerPhone'] . '<br>' : '' ?></sapn>
                                    <input type="hidden" value="<?php // $sale_outward['RefCustomerPhone'] ?>" name="hidden_customer_phonenumber" id="hidden_customer_phonenumber">

                                    <sapn id="supplier_address"><?php // $sale_outward['RefCustomerEmail'] != '' ? '<b>Email : </b>' . $sale_outward['RefCustomerEmail'] . '<br>' : '' ?></sapn>
                                    <input type="hidden" value="<?php // $sale_outward['RefCustomerEmail'] ?>" name="hidden_customer_email" id="hidden_customer_email">
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive" id="product_cart">          
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th width="1%">SN</th>
                                                <th width="20%">Products</th>
                                                <th width="15%">Sold Qty</th>
                                                <th width="12%">Return Qty</th>
                                                <th width="14%">Rate</th>
                                                <th width="11%">Discount</th>
                                                <th width="7%">Tax</th>
                                                <th width="9%">Tax Amount</th>
                                                <th width="15%">Total</th>
                                                <th width="3%">Action</th>  
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
//                                            if (!empty($outward_sale_list) && $outward_sale_list != '' && isset($outward_sale_list)) {
//                                                foreach ($outward_sale_list as $k => $product_value) {
//                                                    $k++;
//                                                    $total = $product_value->TotalAmt + $product_value->TaxAmt;
                                                    ?>
<!--                                                    <tr class="rowCount" data-id="<?= $k ?>" id="row_<?= $k ?>">
                                                        <td style="padding-left: 10px;"><p id="sl_<?= $k ?>"><?= $k ?></p></td>
                                                        <td><span style="padding-bottom: 5px;"><?= getProductNameById($product_value->refProductID) ?></span></td>

                                                <input type="hidden" id="product_id_<?= $k ?>" value="<?= $product_value->refProductID ?>"/>
                                                <input type="hidden" id="tax_per_<?= $k ?>" value="<?= $product_value->tax_per ?>"/>
                                                <input type="hidden" id="total_tax_<?= $k ?>" value=""/>
                                                <input type="hidden" id="discount_ind_<?= $k ?>" value=""/>
                                                <input type="hidden" id="BasicTotal_<?= $k ?>" value=""/>
                                                <input type="hidden" id="Tax_id_<?= $k ?>" value="<?= $product_value->Tax_id ?>"/>
                                                <input type="hidden" id="total_amt_<?= $k ?>" value=""/>
                                                <input type="hidden" id="tax_name_<?= $k ?>" value="<?= $product_value->tax_name ?>"/>

                                                <td><input type="text" class="form-control text-right available_quantity_<?= $k ?>" value="<?= $product_value->Qty ?>" readonly="" id="invoice_qty_<?= $k ?>" /><span class="label_aligning" id="sold_qty_<?= $k ?>"></span></td>
                                                <td><input type="text" onkeyup="calculateAll();" onchange="calculateAll();"  class="total_qntt_<?= $k ?> form-control text-right chkquawithsold desimalcheck" id="total_qntt_<?= $k ?>" min="0" placeholder="0.00" tabindex="4" /><span class="label_aligning"></span></td>
                                                <td><input type="text" onkeyup="calculateAll();" onchange="calculateAll();" value="<?= $product_value->UnitAmt ?>" id="price_item_<?= $k ?>" class="price_item<?= $k ?> form-control text-right" min="0" tabindex="5" required="" placeholder="0.00" readonly=""/></td>
                                                <td><input type="text" onkeyup="calculateAll();" onchange="calculateAll();" id="discount_<?= $k ?>" class="form-control text-right desimalcheck" placeholder="0.00" value="" min="0" tabindex="6"/></td>
                                                <td><span><?= $product_value->tax_name ?></span></td>
                                                <td><input type="text" id="tax_amount_<?= $k ?>" class="form-control" value="" placeholder="Tax Amount" readonly /><span class="label_aligning"></span></td>

                                                <td><input class="total_price form-control text-right" type="text"  id="total_price_<?= $k ?>" value="" readonly="readonly" /><span class="label_aligning"></span></td>
                                                <td><input type="checkbox" name="rtn[]"  onclick="checkboxcheck(<?= $k ?>)" id="check_id_<?= $k ?>" value="<?= $k ?>" class="chk returnchecked" ><label for="check_id_<?= $k ?>"></label></td>
                                                </tr>-->
                                                <?php
//                                            }
//                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div> 
                            </div>
                        </div>

                        <br>

                        <div class="row">
                            <div class="col-md-9"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="g_total"><?php echo display('total_tax') ?>:</label>
                                    <input id="total_tax_ammount" tabindex="-1" class="form-control text-right valid" name="all_total_tax" value="" readonly="readonly" aria-invalid="false" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-9"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="g_total"><?php echo display('nt_return') ?>:</label>
                                    <input type="text" id="grandTotal" class="form-control text-right" name="grand_total_price" value="" readonly="readonly" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="button" id="add-sale-return" class="btn btn-primary btn-large" name="add-sale-return" value="<?php echo display('save') ?>" tabindex="10"/>
                                <input type="submit" value="<?php echo display('save_and_add_another') ?>" name="add-sale-return-another" class="btn btn-large btn-success" id="add-sale-return-another" tabindex="9">
                                <a href="<?= base_url() ?>Sale_return/manageReturnList"  class="btn btn-danger btn-large">Cancel</a>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Add Product End -->

<script type="text/javascript">

//    $(document).ready(function () {
//        $('input[type=checkbox]').each(function () {
//            if (this.nextSibling.nodeName != 'label') {
//                $(this).after('<label for="' + this.id + '"></label>')
//            }
//        });
//    });

    $(document).on('click', '#add-sale-return', function () {
        var total_checked = $('.chk').filter(':checked').length;
        if (total_checked <= 0) {
            alert('Please select atleast 1 product.');
        } else {
            $('#insert_sale_return').submit();
        }
    });

    $(document).on('blur', '.chkquawithsold', function () {
        var sold_qty = $(this).parent().prev().find('input[type=text]').val();
        var return_qty = $(this).val();
        sold_qty = parseFloat(sold_qty);
        return_qty = parseFloat(return_qty);
        if (sold_qty < return_qty) {
            $(this).val('');
            $(this).parent().next().next().find('input[type=text]').val('');
            calculateAll();
            alert('Please select quantity less than sold quantity.');
        }
    });
</script>

<script type="text/javascript">
    $('#add-sale-return').prop("disabled", true);
    $('#add-sale-return-another').prop("disabled", true);
    $('input:checkbox').click(function () {
        if ($(this).is(':checked')) {
            $('#add-sale-return').prop("disabled", false);
            $('#add-sale-return-another').prop("disabled", false);
        } else {
            if ($('.chk').filter(':checked').length < 1) {
                $('#add-sale-return').prop("disabled", true);
                $('#add-sale-return-another').prop("disabled", true);
            }
        }
    });

</script>

<script>
    function checkboxcheck(sl) {
        var check_id = 'check_id_' + sl;

        var product_id = 'product_id_' + sl;
        var tax_per = 'tax_per_' + sl;
        var total_tax = 'total_tax_' + sl;
        var basictotal = 'BasicTotal_' + sl;
        var tax_id = 'Tax_id_' + sl;
        var total_amt = 'total_amt_' + sl;
        var tax_name = 'tax_name_' + sl;

        var sold_qty = 'sold_qty_' + sl;
        var product_qua = 'total_qntt_' + sl;
        var product_rate = 'price_item_' + sl;
        var discount = 'discount_' + sl;
        var tax_amount = 'tax_amount_' + sl;
        
        if ($('#' + check_id).prop("checked") == true) {
            document.getElementById(product_id).setAttribute("name", "product_id[]");
            document.getElementById(tax_per).setAttribute("name", "tax_per[]");
            document.getElementById(total_tax).setAttribute("name", "total_tax[]");
            document.getElementById(basictotal).setAttribute("name", "BasicTotal[]");
            document.getElementById(tax_id).setAttribute("name", "Tax_id[]");
            document.getElementById(total_amt).setAttribute("name", "total_amt[]");
            document.getElementById(tax_name).setAttribute("name", "tax_name[]");

            document.getElementById(sold_qty).setAttribute("name", "sold_qty[]");
            document.getElementById(product_qua).setAttribute("required", "required");
            document.getElementById(product_qua).setAttribute("name", "product_quantity[]");
            document.getElementById(product_rate).setAttribute("name", "product_rate[]");
            document.getElementById(discount).setAttribute("name", "discount[]");
            document.getElementById(tax_amount).setAttribute("name", "tax_amount[]");

        } else if ($('#' + check_id).prop("checked") == false) {
            document.getElementById(product_id).removeAttribute("name", "");
            document.getElementById(tax_per).removeAttribute("name", "");
            document.getElementById(total_tax).removeAttribute("name", "");
            document.getElementById(basictotal).removeAttribute("name", "");
            document.getElementById(tax_id).removeAttribute("name", "");
            document.getElementById(total_amt).removeAttribute("name", "");
            document.getElementById(tax_name).removeAttribute("name", "");

            document.getElementById(sold_qty).setAttribute("name", "");
            document.getElementById(product_qua).removeAttribute("required");
            document.getElementById(product_qua).removeAttribute("name", "");
            document.getElementById(product_rate).removeAttribute("name", "");
            document.getElementById(discount).removeAttribute("name", "");
            document.getElementById(tax_amount).setAttribute("name", "");

        }
    }

    function calculateAll() {
        var subtotal = 0;
        var i = 1;
        var all_tax = 0;
        $(".rowCount").each(function () {
            var id = $(this).attr("data-id");
            var unit_price = $("#price_item_" + id).val();
            var tax = $("#tax_per_" + id).val().split(',');
            var temp = "#sl_" + id;
            var product_id = $("#product_id_" + id).val();
            var quantity = $("#total_qntt_" + id).val();
            var disc = $("#discount_" + id).val();
            var disc_type = '';
            var temp_amount = '';
            var total_tax = 0;

            $(temp).html(i);
            i++;
            var quantity_amount = $("#total_qntt_" + id).val();
            if ($.trim(unit_price) == "" || $.isNumeric(unit_price) == false) {
                unit_price = 0;
            }
            if ($.trim(quantity_amount) == "" || $.isNumeric(quantity_amount) == false) {
                quantity_amount = 0;
            }
            if ($.trim(disc) == '' || $.trim(disc) == '%' || $.trim(disc) == '%%' || $.trim(disc) == '%%%' || $.trim(disc) == '%%%%') {
                disc = 0;
            }
            if (disc.length > 0 && disc.substr(disc.length - 1) == '%') {
                disc_type = 'percentage';
            } else {
                disc_type = 'plain';
            }


            var quantity_amount_and_unit_price = parseFloat($.trim(unit_price)) * parseFloat($.trim(quantity_amount));

            if (disc_type == 'percentage') {
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                temp_amount = quantity_amount_and_unit_price * disc;
                temp_amount = temp_amount / 100;
                $('#discount_ind_' + id).val(temp_amount);
                quantity_amount_and_unit_price = quantity_amount_and_unit_price - temp_amount;
            } else {
                $('#discount_ind_' + id).val(disc);
                quantity_amount_and_unit_price = quantity_amount_and_unit_price - disc;
            }

            if (tax.length > 0) {
                var tax_product = 0;
                tax.forEach(function (key, val) {
                    var tax1 = quantity_amount_and_unit_price;
                    var tax2 = (tax1 * key) / 100;
                    total_tax = total_tax + tax2;
                    tax_product = tax_product + tax2
                });
                quantity_amount_and_unit_price = quantity_amount_and_unit_price + tax_product;
            }

            var BasicTotal = quantity * unit_price;
            $('#BasicTotal_' + id).val(BasicTotal);

            var total_amt = BasicTotal - disc;
            $('#total_amt_' + id).val(total_amt);

            all_tax = all_tax + total_tax;
            $('#total_tax_' + id).val(total_tax);

            $('#tax_amount_' + id).val(total_tax);
            $("#total_price_" + id).val(quantity_amount_and_unit_price.toFixed(2));
            subtotal += parseFloat($.trim($("#total_price_" + id).val()));
        });

        if (isNaN(subtotal)) {
            subtotal = 0;
        }

        $('#total_tax_ammount').val(all_tax.toFixed(2));


        $("#subtotal").val(subtotal);
        var other = parseFloat($.trim($("#other").val()));
        if ($.trim(other) == "" || $.isNumeric(other) == false) {
            other = 0;
        }

        var grand_total = parseFloat(subtotal) + parseFloat(other);
        grand_total = grand_total.toFixed(2);
        $("#grandTotal").val(grand_total);

        var paid = $("#paid").val();
        if ($.trim(paid) == "" || $.isNumeric(paid) == false) {
            paid = 0;
        }

        var due = parseFloat(grand_total) - parseFloat(paid);

        $("#due").val(due.toFixed(2));
    }

</script>



