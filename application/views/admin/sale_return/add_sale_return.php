<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Sale Return
        </h1>
        <div class="d-flex mr-0">
            <a href="<?php echo base_url('admin/Purchase_return/manageReturnList') ?>" class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed"><i class="ti-align-justify"> </i>Manage Purchase Return</a>
            <a href="<?php echo base_url('admin/Sale_return/manageReturnList') ?>" class="btn btn-primary ml-1 bg-trans-gradient waves-effect waves-themed"><i class="ti-align-justify"> </i>Manage Sale Return</a>
        </div>
    </div>
    <div class="row" id="search_row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <div class="col-sm-12" style="padding: 0;">
                            <div class="d-flex" style="display: flex; align-items: end; margin-bottom: 15px;">
                                <div class="col-sm-4" style="padding: 0;">
                                    <label for="sale_invoice_no">Sale Invoice No:</label>
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" name="sale_invoice_no" class="form-control" id="sale_invoice_no" placeholder="Enter Your Sale Invoice No" required="required">
                                </div>
                                <div class="col-sm-3">
                                    <button type="submit" class="btn btn-success" id="invoice_search_btn">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).ready(function () {
        $(document).on('click', '#invoice_search_btn', function () {
            let cnt = 0;
            let sale_invoice_no = $('#sale_invoice_no').val();
            if (sale_invoice_no != '' && sale_invoice_no != null) {
                $.ajax({
                    method: 'POST',
                    url: '<?= base_url() ?>admin/Sale_return/addSaleReturn',
                    data: {sale_invoice_no: sale_invoice_no},
                    dataType: 'json',
                    success: function (response) {
                        if (response.status == true) {
                            $('#sale_return_row').empty();

                            let invoice_details = '';
                            cnt++;

                            invoice_details += `<form action="<?= base_url() ?>admin/Sale_return/insertSaleReturn" class="form-vertical" id="add_sale_return" name="add_sale_return" method="post">`;
                            invoice_details += `<div class="row" id="sale_return_row">`;
                            invoice_details += `<div class="col-sm-12">`;
                            invoice_details += `<div id="panel-2" class="panel"><div class="panel-container show">`;
                            invoice_details += `<div class="panel-hdr">`;
                            invoice_details += `<h2>Sale Invoice Details</h2>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="panel-content">`;
                            invoice_details += `<div class="row">`;
                            invoice_details += `<div class="col-sm-6">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="invoice_no">Invoice No.</label>`;
                            invoice_details += `<input type="text" class="form-control" id="invoice_no" name="invoice_no" value="` + response.sale_invoice_data.SINO + `" required readonly>`;
                            invoice_details += `<input type="hidden" name="hidden_sale_invoice_id" id="hidden_sale_invoice_id" value="` + response.sale_invoice_data.SIID + `">`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="col-sm-6">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="datetime">Invoice Date & Time</label>`;
                            invoice_details += `<div class="input-group date" id="datetime">`;
                            invoice_details += `<input type="text" class="form-control" id="datetime" name="datetime" value="` + response.sale_invoice_data.DateTime + `" required readonly>`;
                            invoice_details += `<span class="input-group-addon">`;
                            invoice_details += `<span class="glyphicon glyphicon-calendar"></span>`;
                            invoice_details += `</span>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="col-sm-6">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="supplier_id">Customer</label>`;
                            invoice_details += `<select name="supplier_id" class="form-control " id="supplier_id" required="" disabled="">`;
                            invoice_details += `<option value="` + response.sale_invoice_data.RefCustomerID + `" data-address="">` + response.sale_invoice_data.customer_name + `</option>`;
                            invoice_details += `</select>`;
                            invoice_details += `<input type="hidden" name="hidden_customer_id" id="hidden_customer_id" value="` + response.sale_invoice_data.RefCustomerID + `">`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="col-sm-6">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="customer_details" class="col-form-label" >Customer Details</label><br>`;

                            if (response.sale_invoice_data.RefCustomerAddress != '' && response.sale_invoice_data.RefCustomerAddress != null && response.sale_invoice_data.RefCustomerAddress != undefined) {
                                invoice_details += `<sapn id="customer_address"><b>Address : </b>` + response.sale_invoice_data.RefCustomerAddress + `</sapn><br>`;
                                invoice_details += `<input type="hidden" value="` + response.sale_invoice_data.RefCustomerAddress + `" name="hidden_customer_address" id="hidden_customer_address">`;
                            }

                            if (response.sale_invoice_data.RefCustomerPhone != '' && response.sale_invoice_data.RefCustomerPhone != null && response.sale_invoice_data.RefCustomerPhone != undefined) {
                                invoice_details += `<sapn id="customer_phone"><b>Phone : </b>` + response.sale_invoice_data.RefCustomerPhone + `</sapn><br>`;
                                invoice_details += `<input type="hidden" value="` + response.sale_invoice_data.RefCustomerPhone + `" name="hidden_customer_phone" id="hidden_customer_phone">`;
                            }

                            if (response.sale_invoice_data.RefCustomerEmail != '' && response.sale_invoice_data.RefCustomerEmail != null && response.sale_invoice_data.RefCustomerEmail != undefined) {
                                invoice_details += `<sapn id="customer_email"><b>Email : </b>` + response.sale_invoice_data.RefCustomerEmail + `</sapn><br>`;
                                invoice_details += `<input type="hidden" value="` + response.sale_invoice_data.RefCustomerEmail + `" name="hidden_customer_email" id="hidden_customer_email">`;
                            }

                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<hr>`;
                            invoice_details += `<div class="row">`;
                            invoice_details += `<div class="col-md-12">`;
                            invoice_details += `<div class="table-responsive" id="product_cart">          `;
                            invoice_details += `<table class="table">`;
                            invoice_details += `<thead>`;
                            invoice_details += `<tr>`;
                            invoice_details += `<th width="1%">SN</th>`;
                            invoice_details += `<th width="20%">Products</th>`;
                            invoice_details += `<th width="12%">Product Price</th>`;
                            invoice_details += `<th width="11%">Sale Quantity</th>`;
                            invoice_details += `<th width="11%">Available Quantity</th>`;
                            invoice_details += `<th width="11%">Return Quantity</th>`;
                            invoice_details += `<th width="11%">Discount</th>`;
                            invoice_details += `<th width="7%">Tax</th>`;
                            invoice_details += `<th width="9%">Tax Amount</th>`;
                            invoice_details += `<th width="15%">Total</th>`;
                            invoice_details += `</tr>`;
                            invoice_details += `</thead>`;
                            invoice_details += `<tbody>`;
                            invoice_details += `</tbody>`;

                            if (response.sale_invoice_item_data != undefined && response.sale_invoice_item_data != '' && response.sale_invoice_item_data != null) {
                                $(response.sale_invoice_item_data).each(function (key, value) {
                                    key++;
                                    invoice_details += `<tr class="rowCount" data-id="` + key + `" id="row_` + key + `">`;
                                    invoice_details += `<td style="padding-left: 10px;"><p id="sl_` + key + `">` + key + `</p></td>`;
                                    invoice_details += `<td><span style="padding-bottom: 5px;">` + value.product_name_with_category + `</span></td>`;
                                    invoice_details += `<input type="hidden" id="product_id_` + key + `" name="product_id[]" value="` + value.refProductID + `"/>`;
                                    invoice_details += `<input type="hidden" id="tax_per_` + key + `" name="tax_per[]" value="` + value.tax_per + `"/>`;
                                    invoice_details += `<input type="hidden" id="tax_per_amt_` + key + `" name="tax_per_amt[]" value=""/>`;
                                    invoice_details += `<input type="hidden" id="total_tax_` + key + `" name="total_tax[]" value="` + value.TaxAmt + `"/>`;
                                    invoice_details += `<input type="hidden" id="discount_ind_` + key + `" name="discount_ind[]" value="` + value.DiscountAmt + `"/>`;
                                    invoice_details += `<input type="hidden" id="BasicTotal_` + key + `" name="BasicTotal[]" value="` + parseFloat(value.Qty) * parseFloat(value.UnitAmt) + `"/>`;
                                    invoice_details += `<input type="hidden" id="Tax_id_` + key + `" name="Tax_id[]" value="` + value.Tax_id + `"/>`;
                                    invoice_details += `<input type="hidden" id="total_amt_` + key + `" name="total_amt[]" value="` + value.TotalAmt + `"/>`;
                                    invoice_details += `<input type="hidden" id="tax_name_` + key + `" name="tax_name[]" value="` + value.tax_name + `"/>`;
                                    invoice_details += `<input type="hidden" id="invoice_product_qty_` + key + `" name="invoice_product_qty[]" value="` + value.Qty + `"/>`;
                                    invoice_details += `<td><input type="text" id="unit_price_` + key + `" name="unit_price[]" onfocus="this.select();" class="form-control desimalcheck aligning" placeholder="Unit Price" value="` + value.UnitAmt + `" onkeyup="return calculateAll();" required readonly /><span class="label_aligning"></span></td>`;
                                    invoice_details += `<td><span style="padding-bottom: 5px;">` + value.Qty + `</span></td>`;
                                    invoice_details += `<td><span style="padding-bottom: 5px;">` + value.returnable_qty + `</span></td>`;
                                    invoice_details += `<td><input type="text" data-countID="` + key + `" id="quantity_amount_` + key + `" name="quantity_amount[]" onfocus="this.select();" class="form-control desimalcheck aligning countID quantity_amount"  placeholder="Quantity" onkeyup="calculateAll(); checkDue();" value="" data-sale_invoice_qty="` + value.returnable_qty + `" onblur="checkStock(` + key + `)" required=""></td>`;
                                    invoice_details += `<td><input type="text" data-countID="` + key + `" id="discount_` + key + `" name="discount[]" onfocus="this.select();" class="form-control desimalcheck aligning countID discount_percentage" value="` + value.DiscountPer + `" placeholder="Discount" onkeyup="return calculateAll();" readonly></td>`;
                                    invoice_details += `<td><span id="tax_display_` + key + `">` + value.tax_name + `</span></td>`;
                                    invoice_details += `<td><input type="text" id="tax_amount_` + key + `" name="tax_amount[]" class="form-control" value="` + value.TaxAmt + `" placeholder="Tax Amount" readonly /><span class="label_aligning"></span></td>`;
                                    let total = parseFloat(value.TotalAmt) + parseFloat(value.TaxAmt);
                                    invoice_details += `<td><input type="text" id="total_` + key + `" name="total[]" class="form-control aligning" value="` + parseFloat(total).toFixed(2) + `" placeholder="Total" readonly /><span class="label_aligning"></span></td>`;
                                    invoice_details += `</tr>`;
                                });
                            }

                            invoice_details += `</table>`;
                            invoice_details += `</div> `;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<br>`;
                            invoice_details += `<div class="row">`;
                            invoice_details += `<div class="col-md-9"></div>`;
                            invoice_details += `<div class="col-md-3">`;
                            invoice_details += `<div class="form-group">`;
                            invoice_details += `<label for="g_total">Grand Total</label>`;
                            invoice_details += `<input type="text" class="form-control" id="g_total" name="g_total" placeholder="Grand Total" onchange="checkDue();" value="` + response.sale_invoice_data.GrandTotal + `" readonly />`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `<div class="col-md-12">`;
                            invoice_details += `<div class="row" style="display:flex; gap:5px;">`;
                            invoice_details += `<input type="submit" id="add-sale-return" class="btn btn-primary btn-large" name="add-sale-return" value="Save" tabindex="10"/>`;
                            invoice_details += `<input type="submit" value="Save & Add Another" name="add-sale-return-another" class="btn btn-large btn-success" id="add-sale-return-another" tabindex="9">`;
                            invoice_details += `<a href="<?= base_url() ?>admin/Sale_return/manageReturnList"  class="btn btn-danger btn-large">Cancel</a>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</div>`;
                            invoice_details += `</form>`;
                            $('#search_row').after(invoice_details);
                        } else {
                            swalWithBootstrapButtons.fire({
                                type: 'error',
                                title: 'Oops...',
                                text: response.msg,
                            });
                        }
                    },
                    error: function () {
                    }
                });
            } else {
                swalWithBootstrapButtons.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Please enter valid sale invoice number!'
                });
            }
        });

        $(document).on('change', '.quantity_amount', function () {
            let qty = parseFloat($(this).val());
            let invoice_qty = parseFloat($(this).data('sale_invoice_qty'));
            if (qty > invoice_qty) {
                $(this).val(null);
                swalWithBootstrapButtons.fire({
                    title: "Alert!",
                    text: "Return quantity must be less than or equal to avialable quantity.",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
            } else if (qty <= 0) {
                $(this).val(null);
                swalWithBootstrapButtons.fire({
                    title: "Alert!",
                    text: "Return quantity must be more than 0.",
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#3c8dbc'
                });
            } else {
//                alert('ok');
            }
        });

        $('#add_sale_return').validate({
            ignore: [],
            rules: {
                quantity_amount: {
                    required: true
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });

    $(document).on('change', '.discount_percentage', function () {
        let qty = parseFloat($(this).val());
        if (qty > 100) {
            $(this).val(null);
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Discount must be less than or equal to 100%.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
        } else if (qty <= 0) {
            $(this).val(null);
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Discount must be more than 0%.",
                confirmButtonText: 'Ok',
                confirmButtonColor: '#3c8dbc'
            });
        } else {
//                alert('ok');
        }
    });

    function calculateAll() {
        var subtotal = 0;
        var i = 1;
        $(".rowCount").each(function () {
            var id = $(this).attr("data-id");
            var unit_price = $("#unit_price_" + id).val();
            var tax = $("#tax_per_" + id).val().split(',');
            var temp = "#sl_" + id;
            var product_id = $("#product_id_" + id).val();
            var quantity = $("#quantity_amount_" + id).val();
            var disc = $("#discount_" + id).val();
            var disc_type = '';
            var temp_amount = '';
            var total_tax = 0;
            $(temp).html(i);
            i++;
            var quantity_amount = $("#quantity_amount_" + id).val();
            if ($.trim(unit_price) == "" || $.isNumeric(unit_price) == false) {
                unit_price = 0;
            }
            if ($.trim(quantity_amount) == "" || $.isNumeric(quantity_amount) == false) {
                quantity_amount = 0;
            }
            if ($.trim(disc) == '' || $.trim(disc) == '%' || $.trim(disc) == '%%' || $.trim(disc) == '%%%' || $.trim(disc) == '%%%%') {
                disc = 0;
            }

            if (disc.length > 0) {
                disc_type = 'percentage';
            }
//            else {
//                disc_type = 'plain';
//            }

            var quantity_amount_and_unit_price = parseFloat($.trim(unit_price)) * parseFloat($.trim(quantity_amount));
            if (disc_type == 'percentage') {
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                disc = disc.replace('%', '');
                temp_amount = quantity_amount_and_unit_price * disc;
                temp_amount = temp_amount / 100;
                $('#discount_ind_' + id).val(temp_amount);
                quantity_amount_and_unit_price = quantity_amount_and_unit_price - temp_amount;
            }
//            else {
//                $('#discount_ind_' + id).val(disc);
//                quantity_amount_and_unit_price = quantity_amount_and_unit_price - disc;
//            }

            if (tax.length > 0) {
                var tax_product = 0;
                tax.forEach(function (key, val) {
                    var tax1 = quantity_amount_and_unit_price;
                    var tax2 = (tax1 * key) / 100;
                    total_tax += tax2;
                    tax_product += tax2;
                });
                quantity_amount_and_unit_price = quantity_amount_and_unit_price + tax_product;
            }

            var BasicTotal = quantity * unit_price;
            $('#BasicTotal_' + id).val(BasicTotal);
            var total_amt = BasicTotal - temp_amount;
            $('#total_amt_' + id).val(total_amt);
            $('#total_tax_' + id).val(total_tax);
            $('#tax_amount_' + id).val(total_tax);
            $("#total_" + id).val(quantity_amount_and_unit_price.toFixed(2));
            subtotal += parseFloat($.trim($("#total_" + id).val()));
        });
        if (isNaN(subtotal)) {
            subtotal = 0;
        }
        $("#subtotal").val(subtotal);
        var other = parseFloat($.trim($("#other").val()));
        if ($.trim(other) == "" || $.isNumeric(other) == false) {
            other = 0;
        }

        var grand_total = parseFloat(subtotal) + parseFloat(other);
        grand_total = grand_total.toFixed(2);
        $("#g_total").val(grand_total);
        var paid = $("#paid").val();
        if ($.trim(paid) == "" || $.isNumeric(paid) == false) {
            paid = 0;
        }

        var due = parseFloat(grand_total) - parseFloat(paid);
        $("#due").val(due.toFixed(2));
    }

    function checkDue() {
        let grand_total = $("#g_total").val();
        let paid_amount = $("#paid_amount").val();
        let due_amount = parseFloat(grand_total) - parseFloat(paid_amount);
//        console.log(due_amount);
        $('#due_amount').val(parseFloat(due_amount).toFixed(2));
    }

    function checkStock(suffix) {
        var quantity_amount = $('#quantity_amount_' + suffix).val();
        var quantity_sale_order = $('#quantity_amount_' + suffix).attr('data-sale_inward_qty');
        quantity_amount = parseFloat(quantity_amount);
        quantity_sale_order = parseFloat(quantity_sale_order);
        if ($.trim(quantity_amount) == "" || quantity_amount == undefined || isNaN(quantity_amount)) {
            quantity_amount = 0;
        }
        if ($.trim(quantity_sale_order) == "" || quantity_sale_order == undefined || isNaN(quantity_sale_order)) {
            quantity_sale_order = 0;
        }

//        if (quantity_amount > 0 && quantity_amount > quantity_sale_order) {
//            $('#quantity_amount_' + suffix).val('');
//            calculateAll();
//            swalWithBootstrapButtons.fire({
//                title: "Alert!",
//                text: "Quantity must be less than sale inward quantity.",
//                confirmButtonText: 'Ok',
//                confirmButtonColor: '#3c8dbc'
//            });
//            return false;
//        }
    }
</script>



