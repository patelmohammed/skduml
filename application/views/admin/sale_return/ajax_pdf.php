<page backtop="37mm" backbottom="0mm" backleft="0mm" backright="0mm">
    <style>
        table {
            border-collapse: collapse;
            width:100%;
        }
    </style>
    <page_header style="height:20%;">
        <table>
            <tr>
                <td style="width: 55%;vertical-align: top;height: 100%;">
                    <table style="height: 100%;" border='1'>
                        <tr>
                            <td style="width: 100%;height: 50%;vertical-align: top;">
                                <table>
                                    <tr>
                                        <td style="width: 100%;font-size: 15pt;"><b>M/S. <?= $company_detail['company_name'] ?></b></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size: 8pt;">
                                            <?= $company_detail['address'] ?>
                                            <?= $company_detail['company_state'] ?>
                                            <?= $company_detail['company_country'] ?>
                                            <?= $company_detail['company_pincode'] ?><br>
                                            <?= $company_detail['mobile'] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;height: 50%;vertical-align: top;">
                                <table>
                                    <tr>
                                        <td style="width: 100%;font-width: bold;"><?= $customer_detail['customer_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size: 8pt;">
                                            <?= $customer_detail['customer_address'] ?>
                                            <?= $customer_detail['customer_city'] . ', ' ?>
                                            <?= $customer_detail['customer_state'] ?>
                                            <?= $customer_detail['customer_country'] ?>
                                            <?= $customer_detail['customer_pincode'] ?><br>
                                            <?= $customer_detail['customer_mobile'] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size: 8pt;">
                                            GSTIN No:<?= $customer_detail['customer_gst_no'] ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;font-size: 10pt;text-transform:capitalize;text-align: right;padding-right: 5pt;padding-bottom: 3pt;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 45%;height: 100%;">
                    <table border="1">
                        <tr>
                            <td style="width: 100%;font-size: 10pt;padding: 2pt;text-align: center;" colspan="2">ORIGINAL</td>
                        </tr>
                        <tr>
                            <td style="width: 50%;font-size: 10pt;padding: 2pt;border-right: 0px solid black;">GST TAX INVOICE</td>
                            <td style="width: 50%;font-size: 10pt;padding: 2pt;text-align: right;">CASH MEMO</td>
                        </tr>
                        <tr>
                            <td style="width: 50%;font-size: 10pt;padding: 2pt;" colspan="2">Inv. No  :&nbsp;<b><?= $sale_return_data['SINO'] ?></b></td>
                        </tr>
                        <tr>
                            <td style="width: 50%;font-size: 10pt;padding: 2pt;" colspan="2">Inv. Dt  :&nbsp;<b><?= date('d/m/Y', strtotime($sale_return_data['DateTime'])); ?></b></td>
                        </tr>
                        <tr>
                            <td style="width: 50%;font-size: 10pt;padding: 2pt;" colspan="2">Delivery Dt :&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 50%;font-size: 10pt;padding: 2pt;" colspan="2">E-Way No: &nbsp;<b><?= isset($sale_return_data['EwayBillNo']) && !empty($sale_return_data['EwayBillNo']) ? $sale_return_data['EwayBillNo'] : '' ?></b></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </page_header>
    <table>
        <thead>
            <tr>
                <td style="width: 3%;border-top: 1px solid black;border-bottom: 1px solid black;border-left: 1px solid black;font-size: 8pt;border-right: 1px solid black;font-size: 8pt;text-align: center;" rowspan="2">Sr</td>
                <td style="width: 23%;border-top: 1px solid black;border-bottom: 1px solid black;border-left: 1px solid black;font-size: 8pt;border-right: 1px solid black;font-size: 8pt;padding-left: 3px;" rowspan="2">Product Name</td>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;font-size: 8pt;text-align: center;" rowspan="2">Rate</td>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;font-size: 8pt;text-align: center;" rowspan="2">Qty</td>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;font-size: 8pt;text-align: center;" rowspan="2">Dis</td>
                <td style="width: 10%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;font-size: 8pt;text-align: center;" rowspan="2">Amt</td>
                <td style="width: 10%;border-top: 1px solid black;border-right: 1px solid black;font-size: 8pt;text-align: center;">Taxable</td>
                <td style="width: 13%;text-align: center;border-top: 1px solid black;border-right: 1px solid black;font-size: 8pt;border-bottom: 1px solid #344050;" colspan="2">GST</td>
            </tr>
            <tr>
                <td style="width: 10%;border-bottom: 1px solid black;border-right: 1px solid black;font-size: 8pt;text-align: center;">Amount</td>
                <td style="width: 5%;border-bottom: 1px solid black;border-right: 1px solid black;font-size: 8pt;text-align: center;">%</td>
                <td style="width: 8%;border-bottom: 1px solid black;border-right: 1px solid black;font-size: 8pt;text-align: center;">Amt</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $totalQty = $totalBase = $totalDis = $totalCgst = $totalSgst = $grandtotal = $cgst0 = $cgst3 = $cgst6 = $cgst9 = $sgst0 = $sgst3 = $sgst6 = $sgst9 = $gstbase0 = $gstbase3 = $gstbase6 = $gstbase9 = 0.00;
            $cnt_min = $cnt = $total_page = 0;
            $totalrow = count($sale_return_item_data);
            $total_page = ceil($totalrow / 25);
            $sl = 0;
            $base_amt = $totalTax = $grandtotal = 0;

            if (!empty($sale_return_item_data)) {
                foreach ($sale_return_item_data as $key => $v) {
                    $sl++;
                    $totalQty = $totalQty + $v->Qty;
                    $total = ($v->Qty * $v->UnitAmt);
                    $base_amt = $base_amt + $total;
                    $totalTax = $totalTax + $v->TaxAmt;
                    $grandtotal = ($grandtotal + $total + $v->TaxAmt) - ($v->DiscountAmt);
                    $cgst = number_format(((($v->UnitAmt * $v->Qty) - $v->DiscountAmt) * $v->tax_per) / 100, 2, '.', '');
                    $sgst = number_format(((($v->UnitAmt * $v->Qty) - $v->DiscountAmt) * $v->tax_per) / 100, 2, '.', '');

                    if ($v->tax_name == 'CGST') {
                        $gstbase9 += $cgst;
                        $cgst9 += $cgst;
                    } else if ($v->tax_name == 'SGST') {
                        $gstbase9 += $cgst;
                        $sgst9 += $sgst;
                    } else if ($v->tax_name == 'GST') {
                        $gstbase9 += $cgst;
                        $cgst9 += ($cgst / 2);
                        $sgst9 += ($sgst / 2);
                    }

                    $cnt++;
                    $cnt_min++;
                    $border_bottom = '';
                    if ($cnt_min == 25 || ($totalrow == 15 && $cnt == 15) || ($totalrow == 40 && $cnt == 40) || ($totalrow == 65 && $cnt == 65) || ($totalrow == 90 && $cnt == 90) || ($totalrow == 115 && $cnt == 115)) {
                        if ($cnt_min == 25) {
                            $cnt_min = 0;
                        }
                        $border_bottom = 'border-bottom: 1px solid black;';
                    }
                    ?>
                    <tr>
                        <td style="width: 3%;font-size: 8pt;font-size: 8pt;text-align: center;border-left: 1px solid #344050;<?= $border_bottom ?>"><?= $sl < 10 ? '0' . $sl : $sl ?></td>
                        <td style="width: 23%;font-size: 8pt;font-size: 8pt;text-align: left;padding-left: 3px;<?= $border_bottom ?>"><?= $v->product_name ?></td>
                        <td style="width: 8%;font-size: 8pt;font-size: 8pt;text-align: center;<?= $border_bottom ?>"><?= number_format($v->UnitAmt, 2) ?></td>
                        <td style="width: 8%;font-size: 8pt;font-size: 8pt;text-align: center;<?= $border_bottom ?>"><?= number_format($v->Qty, 2) ?></td>
                        <td style="width: 8%;font-size: 8pt;font-size: 8pt;text-align: center;<?= $border_bottom ?>"><?= number_format($v->DiscountAmt, 2) ?></td>
                        <td style="width: 10%;font-size: 8pt;font-size: 8pt;text-align: center;<?= $border_bottom ?>"><?= number_format($total, 2) ?></td>
                        <td style="width: 10%;font-size: 8pt;font-size: 8pt;text-align: center;<?= $border_bottom ?>"><?= number_format($total, 2) ?></td>
                        <td style="width: 5%;font-size: 8pt;font-size: 8pt;text-align: center;<?= $border_bottom ?>"><?= $v->tax_per ?></td>
                        <td style="width: 8%;font-size: 8pt;font-size: 8pt;text-align: center;border-right: 1px solid #344050;<?= $border_bottom ?>"><?= number_format($v->TaxAmt, 2) ?></td>
                    </tr>
                    <?php
                }
            }
            $border_bottom = '';
            if ($cnt_min > 15) {
                for ($i = 0; $cnt < (($total_page * 25)); $i++) {
                    $cnt++;
                    $cnt_min++;
                    if ($cnt == ($total_page * 25)) {
                        $border_bottom = 'border-bottom: 1px solid black;';
                    }
                    ?>
                    <tr>
                        <td style="width: 100%;font-size: 8pt;border-left: 1px solid black;font-size: 8pt;border-right: 1px solid black;font-size: 8pt;<?= $border_bottom ?>" colspan="9">&nbsp;</td>
                    </tr>
                    <?php
                }
                for ($i = 0; $i < 15; $i++) {
                    $border_bottom = '';
                    if ($i == 14) {
                        $border_bottom = 'border-bottom: 1px solid black;';
                    }
                    ?>
                    <tr>
                        <td style="width: 100%;font-size: 8pt;border-left: 1px solid black;font-size: 8pt;border-right: 1px solid black;font-size: 8pt;<?= $border_bottom ?>" colspan="9">&nbsp;</td>
                    </tr>
                    <?php
                }
            } else {
                for ($i = 0; $cnt_min <= 14; $i++) {
                    $cnt++;
                    $cnt_min++;
                    if ($cnt_min == 15) {
                        $border_bottom = 'border-bottom: 1px solid black;';
                    }
                    ?>
                    <tr>
                        <td style="width: 100%;font-size: 8pt;border-left: 1px solid black;font-size: 8pt;border-right: 1px solid black;font-size: 8pt;<?= $border_bottom ?>" colspan="9">&nbsp;</td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <end_last_page end_height="34mm">
        <table>
            <tr>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;text-align: center;">GST%</td>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: center;">Ass. Amt</td>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: center;">SGST</td>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: center;">CGST</td>
                <td style="width: 21%;border-top: 1px solid black;border-bottom: 1px solid black;text-align: left;padding-left: 2pt;">Total Qty</td>
                <td style="width: 7%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: right;"><?= number_format($totalQty, 2, '.', '') ?></td>
                <td style="width: 6%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: right;padding-right: 2pt;">&nbsp;&nbsp;</td>
                <td style="width: 6%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: right;padding-right: 2pt;"></td>
                <td style="width: 13%;border-top: 1px solid black;border-bottom: 1px solid black;padding-left: 2pt;">Total</td>
                <td style="width: 10%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: right;padding-right: 2pt;"><?= number_format($base_amt, 2, '.', '') ?></td>
                <!--<td style="width: 10%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: right;padding-right: 2pt;">&nbsp;&nbsp;</td>-->
            </tr>
            <tr>
                <td style="width: 8%;border-top: 1px solid black;border-left: 1px solid black;text-align: center;">0.00%</td>
                <td style="width: 8%;border-top: 1px solid black;text-align: center;"><?= number_format($gstbase0, 2, '.', '') ?></td>
                <td style="width: 8%;border-top: 1px solid black;text-align: center;"><?= number_format($sgst0, 2, '.', '') ?></td>
                <td style="width: 8%;border-top: 1px solid black;border-right: 1px solid black;text-align: center;padding-right: 2pt;"><?= number_format($cgst0, 2, '.', '') ?></td>
                <td style="width: 40%;border-top: 1px solid black;border-right: 1px solid black;padding-left: 2pt;" colspan="4">24% interest P.A. after due date. E. & O.E.</td>
                <td style="width: 13%;border-top: 1px solid black;padding-left: 2pt;">[+]SGST Amt</td>
                <td style="width: 10%;border-top: 1px solid black;text-align: right;border-right: 1px solid black;padding-right: 2pt;"><?= number_format($cgst9, 2, '.', '') ?></td>
            </tr>
            <tr>
                <td style="width: 8%;border-top: 1px solid black;border-left: 1px solid black;text-align: center;">6.00%</td>
                <td style="width: 8%;border-top: 1px solid black;text-align: center;"><?= number_format($gstbase3, 2, '.', '') ?></td>
                <td style="width: 8%;border-top: 1px solid black;text-align: center;"><?= number_format($sgst3, 2, '.', '') ?></td>
                <td style="width: 8%;border-top: 1px solid black;border-right: 1px solid black;text-align: center;padding-right: 2pt;"><?= number_format($cgst3, 2, '.', '') ?></td>
                <td style="width: 40%;border-top: 1px solid black;border-right: 1px solid black;padding-left: 2pt;" colspan="4">Subject To BARDOLI jurisdiction Only.</td>
                <td style="width: 13%;border-top: 1px solid black;padding-left: 2pt;">[+]CGST Amt</td>
                <td style="width: 10%;border-top: 1px solid black;text-align: right;border-right: 1px solid black;padding-right: 2pt;"><?= number_format($sgst9, 2, '.', '') ?></td>
            </tr>
            <tr>
                <td style="width: 8%;border-top: 1px solid black;border-left: 1px solid black;text-align: center;">12.00%</td>
                <td style="width: 8%;border-top: 1px solid black;text-align: center;"><?= number_format($gstbase6, 2, '.', '') ?></td>
                <td style="width: 8%;border-top: 1px solid black;text-align: center;"><?= number_format($sgst6, 2, '.', '') ?></td>
                <td style="width: 8%;border-top: 1px solid black;border-right: 1px solid black;text-align: center;padding-right: 2pt;"><?= number_format($cgst6, 2, '.', '') ?></td>
                <td style="width: 40%;border-top: 1px solid black;border-right: 1px solid black;padding-left: 2pt;" colspan="4">GST No : <?= $company_detail['company_gst_no'] ?></td>
                <td style="width: 13%;border-top: 1px solid black;text-align: right;">&nbsp;</td>
                <td style="width: 10%;border-top: 1px solid black;border-right: 1px solid black;text-align: right;">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;border-left: 1px solid black;text-align: center;">18.00%</td>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;text-align: center;"><?= number_format($gstbase9, 2, '.', '') ?></td>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;text-align: center;"><?= number_format($sgst9, 2, '.', '') ?></td>
                <td style="width: 8%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: center;padding-right: 2pt;"><?= number_format($cgst9, 2, '.', '') ?></td>
                <td style="width: 40%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;padding-left: 2pt;" colspan="4">PAN No : <?= $company_detail['pan_number'] ?></td>
                <td style="width: 13%;border-top: 1px solid black;border-bottom: 1px solid black;">&nbsp;</td>
                <td style="width: 10%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: right;">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 100%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;border-left: 1px solid black;padding-left: 2pt;" colspan="9">For,M/S. <?= $company_detail['company_name'] ?></td>
            </tr>
            <tr>
                <td style="width: 75%;border-top: 1px solid black;border-bottom: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;padding-left: 2pt;" colspan="8">Rs&nbsp;<b><?= strtoupper(getIndianCurrency(ceil($grandtotal))) ?></b></td>
                <td style="width: 13%;border-top: 1px solid black;border-bottom: 1px solid black;text-align: left;padding-left: 2pt;">Bill Total</td>
                <td style="width: 10%;border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;text-align: right;padding-right: 2pt;"><b><?= number_format(ceil($grandtotal), 2, '.', ''); ?></b></td>
            </tr>
        </table>
    </end_last_page>
</page>