<!-- Manage Sale Return Start -->
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Manage Sale Return
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Sale_return/createSaleReturn">Add Sale Return</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <div class="row mb-3">
                            <div class="col-md-1">
                                <label class="form-label">Start Date</label>
                            </div>
                            <div class="col-md-2">
                                <input type="date" name="from_date" class="form-control" id="from_date" placeholder="DD-MM-YYYY" value="<?= date('Y-m-d', strtotime(FIN_YEAR_STRT_DATE)) ?>">
                            </div>
                            <div class="col-md-1">
                                <label class="form-label">End Date</label>
                            </div>
                            <div class="col-md-2">
                                <input type="date" name="to_date" class="form-control" id="to_date" placeholder="DD-MM-YYYY" value="<?= date('Y-m-d', strtotime(FIN_YEAR_END_DATE)) ?>">
                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-primary" id="date_filter_btn">Search</button>
                            </div>
                        </div>
                        <table id="sale_return_data" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Date</th>
                                    <th>Sale Return No.</th>
                                    <th>Customer Name</th>
                                    <th>Total Product</th>
                                    <th>Grand Total</th>
                                    <th>Created By</th>
                                    <th>Updated By</th>
                                    <th class="text-center notexport">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<div class="modal fade" id="saleReturnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">  
    <div class="modal-dialog" role="document" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-2x">×</i></span></button>
                <h4 class="modal-title" id="myModalLabel">Print Purchase Return</h4>
            </div>
            <div class="modal-body" id="saleReturnModalData">

            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" target="_blanck" type="button" class="btn btn-primary" id="pdfSaleReturn" data-id="">
                    <i class="fa fa-save"></i> Print</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mailSaleReturnDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-2x">×</i></span></button>
                    <h4 class="modal-title" id="myModalLabel">Mail Purchase Return</h4>
                </div>
                <form class="form-vertical">
                    <div class="row" style="padding-top: 10px;">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="mail_to" class="col-form-label">Mail To<i class="text-danger"> *</i></label>
                                    </div>
                                    <div class="col-sm-10">
                                        <select name="mail_to" class="form-control dynamic_tag_select2 select2-container" id="mail_to" multiple="" required>
                                            <?php
                                            if (!empty($employee_list)) {
                                                foreach ($employee_list as $k1 => $v1) {
                                                    ?>
                                                    <option value="<?= $v1['user_email'] ?>"><?= !empty($v1['user_email']) ? $v1['user_email'] : '' ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="mail_subject" class="col-form-label">Mail Subject</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="mail_subject" name="mail_subject" placeholder="Mail Subject"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="hidden_sr_id" id="hidden_sr_id" value="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="mail_body" class="col-form-label">Mail Body</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <textarea class="form-control ckeditor" id="mail_body" name="mail_body" placeholder="Mail Body"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        &nbsp;
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="checkbox" name="mail_send_me" id="mail_send_me">  Send Me
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="mailSaleReturnData">
                    <i class="fa fa-envelope"></i> Mail</button>
            </div>
        </div>
    </div>
</div>
<!-- Manage Invoice End -->
<script type="text/javascript">

    $(document).ready(function () {
        getSaleReturnList();

        $(document).on('click', '#date_filter_btn', function () {
            $('#sale_return_data').DataTable().destroy();
            getSaleReturnList();
        });

        $(document).on('click', '.deleteSaleReturn', function () {
            var sale_return_id = $(this).attr('name');
            var csrf_test_name = $("[name=csrf_test_name]").val();
            swalWithBootstrapButtons.fire({
                icon: 'warning',
                title: 'Are you sure, Want to Delete?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url('admin/Sale_return/deleteSaleReturn') ?>',
                        data: {sale_return_id: sale_return_id, csrf_test_name: csrf_test_name},
                        cache: false,
                        success: function (datas) {

                            location.reload();
                        }
                    });
                }

            });
        });

        $(document).on('click', '.printSaleReturn', function (e) {

            e.preventDefault();
            var sale_return_id = $(this).attr('name');
            var SRNO = $(this).attr('data-SRNO');
            $.ajax({
                type: "POST",
                url: '<?php echo base_url('admin/Sale_return/printSaleReturn') ?>',
                data: {sale_return_id: sale_return_id},
                cache: false,
                success: function (datas) {
                    $('#pdfSaleReturn').attr('href', '<?php echo base_url('admin/Sale_return/printPDFSaleReturn') ?>/' + sale_return_id + '/' + SRNO);
                    $('#saleReturnModalData').html(datas);

                    $('#saleReturnModal').modal('show');
                }
            });
        });

        $(document).on('click', '.mailSaleReturnForm', function (e) {

            e.preventDefault();
            var SRID = $(this).attr('name');
            $('#hidden_sr_id').val(SRID);
            document.getElementById("mailSaleReturnData").disabled = false;

            $('#mailSaleReturnDetail').modal('show');
        });

        $(document).on('click', '#mailSaleReturnData', function (e) {

            e.preventDefault();
            document.getElementById("mailSaleReturnData").disabled = true;
            var SRID = $('#hidden_sr_id').val();
            var mail_to = $('#mail_to').val();
            var mail_subject = $('#mail_subject').val();
            var mail_body = CKEDITOR.instances['mail_body'].getData()
            var send_me = $('#mail_send_me').prop("checked");
            $.ajax({
                type: "POST",
                url: '<?php echo base_url('admin/Sale_return/mailSaleReturnDetail') ?>',
                data: {SRID: SRID, mail_to: mail_to, mail_subject: mail_subject, mail_body: mail_body, send_me: send_me},
                cache: false,
                success: function (returnData) {

                    var data = JSON.parse(returnData);
                    if (data.result == true) {
                        $('#mailSaleReturnDetail').modal('hide');
                    }
                }
            });
        });

    });

    function getSaleReturnList() {
        var _sale_return_data = $('#sale_return_data').DataTable({
            dom: "<'row '<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'lf><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            "oTableTools": {
                "aButtons": [
                ]
            },
            buttons: [{
                    extend: 'pdfHtml5',
                    text: '<i class="fal fa-file-pdf "></i>',
                    titleAttr: 'Generate PDF',
                    className: 'btn-outline-danger btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'excelHtml5',
                    text: '<i class="fal fa-file-excel "></i>',
                    titleAttr: 'Generate Excel',
                    className: 'btn-outline-success btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'csvHtml5',
                    text: '<i class="fal fa-file-csv "></i>',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-primary btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    }
                }, {
                    extend: 'copyHtml5',
                    text: '<i class="fal fa-copy "></i>',
                    titleAttr: 'Copy to clipboard',
                    className: 'btn-outline-info btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }, {
                    extend: 'print',
                    text: '<i class="fal fa-print "></i>',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-dark btn-sm hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-dark-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#datatable').data('title'),
                    messageTop: $('#datatable').data('msgtop')
                }],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            },
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": [0, 6],
                    "orderable": false
                }, {
                    "targets": [0],
                    "className": 'text-center'
                }],
            "ajax": {
                "url": "<?= base_url() ?>admin/Sale_return/get_sale_return_list",
                "type": "POST",
                "data": function (d) {
                    d.start_date = $('#from_date').val();
                    d.end_date = $('#to_date').val();
                }
            }
        });

        $("#sale_return_data thead input").on('keyup change', function () {
            _sale_return_data.column($(this).parent().index() + ':visible').search(this.value).draw();
        });
    }

</script>
