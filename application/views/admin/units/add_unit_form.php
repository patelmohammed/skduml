<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Unit
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Unit/manage_unit">Unit</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open('admin/Unit/insert_unit', array('class' => 'form-vertical', 'id' => 'insert_unit')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="unit_name">Unit Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="unit_name" id="unit_name" placeholder="Unit Name" required value="">
                                <div class="invalid-feedback">
                                    Unit Name Required or Already Used
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-unit" value="Save"><span class="fal fa-check mr-1"></span>Save</button>
                        <button type="submit" class="btn btn-warning ml-1 waves-effect waves-themed" name="add-unit-another" value="Save & Add Another"><span class="fal fa-check mr-1"></span>Save & Add Another</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#insert_unit').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                    unit_name: {
                        remote: '<?= base_url() ?>admin/Unit/checkProductUnit'
                    }
            },
            messages: {
                unit_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>



