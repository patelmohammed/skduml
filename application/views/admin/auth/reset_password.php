<!DOCTYPE html>
<!-- 
Template Name:  SmartAdmin Responsive WebApp - Template build with Twitter Bootstrap 4
Version: 4.4.5
Author: Sunnyat Ahmmed
Website: http://gootbootstrap.com
Purchase: https://wrapbootstrap.com/theme/smartadmin-responsive-webapp-WB0573SK0
License: You must have a valid license purchased only from wrapbootstrap.com (link above) in order to legally use this theme for your project.
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>
            Login - <?= COMPANY_NAME ?>
        </title>
        <meta name="description" content="<?= COMPANY_NAME ?>">
        <meta name="author" content="Irosha Waas">
        <meta property="og:type" content="website">
        <meta property="og:title" content="<?= COMPANY_NAME ?> By Weborative" />
        <meta property="og:description" content="" />
        <!--<meta property="og:image" content="http://fdmaster.foodmohalla.in/assets/images/logo_640.png" />-->
        <meta property="og:site_name" content="<?= COMPANY_NAME ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/page-login-alt.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/notifications/toastr/toastr.css">
    </head>
    <body>
        <div class="blankpage-form-field">
            <div class="page-logo m-0 w-100 align-items-center justify-content-center rounded border-bottom-left-radius-0 border-bottom-right-radius-0 px-4" style="background-color: #fff;">
                <a href="javascript:void(0)" class="page-logo-link press-scale-down d-flex align-items-center">
                    <!--<img src="<?= base_url() ?>assets/admin/img/logo.png" alt="SmartAdmin WebApp" aria-roledescription="logo" style="margin: 0 auto;">-->
                    <span class="page-logo-text mr-1 text-center text-primary"><?= COMPANY_NAME ?> Admin Panel</span>
                    <span class="position-absolute text-white opacity-50 small pos-top pos-right mr-2 mt-n2 text-danger"></span>
                </a>
            </div>
            <div class="card p-4 border-top-left-radius-0 border-top-right-radius-0 bg-brand-gradient">
                <?= form_open(base_url('admin/Auth/resetPassword/') . $id . '/' . $token, 'class="needs-validation" novalidate') ?>
                <div class="form-group">
                    <label class="form-label" for="password" style="color: white;">Password</label>
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="">
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label" for="confirm_password" style="color: white;">Confirm Password</label>
                    <input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="Confirm Password" required="">
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <button type="submit" class="btn btn-success waves-effect waves-themed float-right"><span class="fal fa-sign-in mr-1"></span>Submit</button>
                <?= form_close() ?>
            </div>
            <div class="blankpage-footer text-center">
                <a href="<?= base_url() ?>admin/Auth"><strong>Login</strong></a>
            </div>
        </div>
        <video poster="<?= base_url() ?>assets/admin/img/backgrounds/clouds.png" id="bgvid" playsinline autoplay muted loop>
            <source src="<?= base_url() ?>assets/admin/media/video/cc.webm" type="video/webm">
            <source src="<?= base_url() ?>assets/admin/media/video/cc.mp4" type="video/mp4">
        </video>
        <!-- BEGIN Color profile -->
        <!-- this area is hidden and will not be seen on screens or screen readers -->
        <!-- we use this only for CSS color refernce for JS stuff -->
        <p id="js-color-profile" class="d-none">
            <span class="color-primary-50"></span>
            <span class="color-primary-100"></span>
            <span class="color-primary-200"></span>
            <span class="color-primary-300"></span>
            <span class="color-primary-400"></span>
            <span class="color-primary-500"></span>
            <span class="color-primary-600"></span>
            <span class="color-primary-700"></span>
            <span class="color-primary-800"></span>
            <span class="color-primary-900"></span>
            <span class="color-info-50"></span>
            <span class="color-info-100"></span>
            <span class="color-info-200"></span>
            <span class="color-info-300"></span>
            <span class="color-info-400"></span>
            <span class="color-info-500"></span>
            <span class="color-info-600"></span>
            <span class="color-info-700"></span>
            <span class="color-info-800"></span>
            <span class="color-info-900"></span>
            <span class="color-danger-50"></span>
            <span class="color-danger-100"></span>
            <span class="color-danger-200"></span>
            <span class="color-danger-300"></span>
            <span class="color-danger-400"></span>
            <span class="color-danger-500"></span>
            <span class="color-danger-600"></span>
            <span class="color-danger-700"></span>
            <span class="color-danger-800"></span>
            <span class="color-danger-900"></span>
            <span class="color-warning-50"></span>
            <span class="color-warning-100"></span>
            <span class="color-warning-200"></span>
            <span class="color-warning-300"></span>
            <span class="color-warning-400"></span>
            <span class="color-warning-500"></span>
            <span class="color-warning-600"></span>
            <span class="color-warning-700"></span>
            <span class="color-warning-800"></span>
            <span class="color-warning-900"></span>
            <span class="color-success-50"></span>
            <span class="color-success-100"></span>
            <span class="color-success-200"></span>
            <span class="color-success-300"></span>
            <span class="color-success-400"></span>
            <span class="color-success-500"></span>
            <span class="color-success-600"></span>
            <span class="color-success-700"></span>
            <span class="color-success-800"></span>
            <span class="color-success-900"></span>
            <span class="color-fusion-50"></span>
            <span class="color-fusion-100"></span>
            <span class="color-fusion-200"></span>
            <span class="color-fusion-300"></span>
            <span class="color-fusion-400"></span>
            <span class="color-fusion-500"></span>
            <span class="color-fusion-600"></span>
            <span class="color-fusion-700"></span>
            <span class="color-fusion-800"></span>
            <span class="color-fusion-900"></span>
        </p>
        <script src="<?= base_url() ?>assets/admin/js/vendors.bundle.js"></script>
        <script src="<?= base_url() ?>assets/admin/js/app.bundle.js"></script>
        <!-- Page related scripts -->
        <script src="<?= base_url() ?>assets/admin/js/notifications/toastr/toastr.js"></script>
        <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": 300,
                "hideDuration": 100,
                "timeOut": 5000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            $(document).ready(function () {
<?= $add_message; ?>
            });
            if ('serviceWorker' in navigator) {
                window.addEventListener('load', function () {
                    navigator.serviceWorker.register('/service-worker.js').then(function (registration) {
                        // Registration was successful
                        console.log('ServiceWorker registration successful with scope: ', registration.scope);
                    }, function (err) {
                        // registration failed :(
                        console.log('ServiceWorker registration failed: ', err);
                    });
                });
            }
        </script>
    </body>
</html>
