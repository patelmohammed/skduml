
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Tax
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Caccounts/manage_tax">Manage Tax</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Caccounts/tax_entry', array('class' => '', 'id' => 'tax_entry', 'name' => 'tax_entry')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="tax_name">Tax Name/Type <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="tax_name" id="tax_name" placeholder=" enter Tax Name/Type" required value="">
                                <div class="invalid-feedback">
                                    Tax Name Required or alredy in use
                                </div>
                            </div>
                            
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="enter_tax">Tax<span class="text-danger">*</span></label>
                                <input type="number" class="form-control" name="enter_tax" id="enter_tax" required="" placeholder="enter tax" value="" min="0" max="100" />
                                <div class="col-sm-2" style="margin:0px;padding: 0px;font-size: 18px">
                                <i class="text-success">%</i>
                            </div>
                                <div class="invalid-feedback">
                                    Tax Required 
                                </div>
                            </div>
                            
                        </div>
                        
                        
                        
                        
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="add-tax" value="Save" id="add-tax"><span class="fal fa-check mr-1"></span>Save</button>
                        
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>




<script>
   $(document).ready(function(){
    
      $('#tax_entry').validate({
        validClass: "is-valid",
        errorClass: "is-invalid",
        rules: {
            tax_name: {
                remote: '<?= base_url() ?>admin/Caccounts/checkTax'
            }
        },
        messages: {
            tax_name: {
                remote: jQuery.validator.format("{0} is already in use, Try another modal")
            }
        }, submitHandler: function (form) {
            form.submit();
        },
        errorPlacement: function (error, element) {
            return true;
        }
    });
});
    
</script>



