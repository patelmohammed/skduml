<!-- Cheaque Manager Start -->
<style>
    .btn-success, .btn-success:hover, .btn-success:active, .btn-success:focus {
        color: #fff;
        background-color: #5cb85c;
        border-color: #4cae4c;
    }

    .label-success {
        color: #fff;
        background-color: #5cb85c;
        border-color: #4cae4c;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('cheque_manager') ?></h1>
            <small><?php echo display('cheque_manager') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('accounts') ?></a></li>
                <li class="active"><?php echo display('cheque_manager') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <!-- Inflow report -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Cheque List</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="cheque_manager_table">
                                <thead>
                                    <tr>
                                        <th><?php echo display('sl') ?></th>
                                        <th><?php echo display('date') ?></th>
                                        <th><?php echo display('cheque_no') ?></th>
                                        <th>Payee Name</th>
                                        <th><?php echo display('bank_name') ?></th>
                                        <th><?php echo display('ammount') ?></th>
                                        <th><?php echo display('action') ?> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
//                                    if ($cheque_manager) {
                                    ?>
                                    <!--                                        {cheque_manager}
                                                                            <tr>
                                                                                <td>{sl}</td>
                                                                                <td>{date}</td>
                                                                                <td>{cheque_no}</td>
                                                                                <td>{name}</td>
                                                                                <td>{bank_name}</td>
                                                                                <td>////<?php //  echo (($position == 0) ? "$currency {amount}" : "{amount} $currency")       ?></td>
                                                                                <td>
                                                                        <center>
                                                                            <a href="////<?php //  echo base_url() . 'Caccounts/cheque_manager_edit/{transection_id}/{action}';       ?>">{action_value}</a>
                                                                             <a href="////<?php //  echo base_url() . 'Caccounts/inout_edit/{transection_id}/{transection_type}/del';       ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo display('delete') ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a> 
                                                                        </center>
                                                                        </td>
                                                                        </tr>
                                                                        {/cheque_manager} -->
                                    <?php
//                                }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!--<div class="text-right"><?php //  $links       ?></div>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Cheaque Manager End -->
<script>
    $(document).ready(function () {
        var _cheque_manager_data = $('#cheque_manager_table').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>t<'bottom'><'row'<'col-sm-5'i>p>",
            "oTableTools": {
                "aButtons": [
                ]
            },
            buttons: [
                {extend: 'excel', title: 'manage_purchase_invoice', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3, 4, 5], modifier: {page: 'current'}}},
                {extend: 'print', className: 'btn-sm', exportOptions: {columns: [0, 1, 2, 3, 4, 5], modifier: {page: 'current'}}}
            ],
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": [0, 6],
                    "orderable": false
                }, {
                    "targets": [0],
                    "className": 'text-center'
                }],
            "ajax": {
                "url": "<?= base_url() ?>Caccounts/get_cheque_payment_list",
                "type": "POST",
//                "data": function (d) {
//                    d.start_date = $('#from_date').val();
//                    d.end_date = $('#to_date').val();
//                }
            }
        });

        $("#cheque_manager_table thead input").on('keyup change', function () {
            _cheque_manager_data.column($(this).parent().index() + ':visible').search(this.value).draw();
        });

        $(document).on('click', '.rejectCheque', function (e) {
            e.preventDefault();
            var cheque_id = $(this).attr('name');
            var transaction_id = $(this).data('transaction_id');
            var transaction_type = $(this).data('transaction_type');
            var is_approve = $(this).attr('is_approve');
            if (is_approve == 0) {
                swal({
//                    html: true,
                    title: 'Are you sure! Want to reject?',
//                    text: "<p>You won't be able to revert this!</p><br>",
                    type: 'warning',
//                    inputPlaceholder: "Write remark here...",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Reject'
                }, function (result) {
                    fullScreenLoader();
                    if (result) {
                        $.ajax({
                            type: "POST",
                            url: '<?php echo base_url('Caccounts/cheque_manager_edit/2') ?>',
                            data: {cheque_id: cheque_id, transaction_id: transaction_id, transaction_type: transaction_type},
                            cache: false,
                            success: function (returnData) {
                                HoldOn.close();
                                var data = JSON.parse(returnData);
                                if (data.status == true) {
                                    location.reload();
                                }
                            }
                        });
                    }
                    HoldOn.close();
                });
            } else {
                swal({
                    title: "This Cheque already cleared.",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
        });

        $(document).on('click', '.approveCheque', function (e) {
            e.preventDefault();
            var cheque_id = $(this).attr('name');
            var transaction_id = $(this).data('transaction_id');
            var transaction_category = $(this).data('transaction_category');
            var is_approve = $(this).attr('is_approve');
            if (is_approve == 0) {
                swal({
//                    html: true,
                    title: 'Are you sure! Want to approve?',
//                    text: "<p>You won't be able to revert this!</p><br>",
                    type: 'warning',
//                    inputPlaceholder: "Write remark here...",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Approve'
                }, function (result) {
                    fullScreenLoader();
                    if (result) {
                        $.ajax({
                            type: "POST",
                            url: '<?= base_url('Caccounts/cheque_manager_edit/1') ?>',
                            data: {cheque_id: cheque_id, transaction_id: transaction_id, transaction_category: transaction_category},
                            cache: false,
                            success: function (returnData) {
                                HoldOn.close();
                                var data = JSON.parse(returnData);
                                if (data.status == true) {
                                    location.reload();
                                }
                            }
                        });
                    }
                    HoldOn.close();
                });
            } else {
                swal({
                    title: "This Cheque already cleared.",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
        });
    });
</script>