<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Tax
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Caccounts/manage_tax">Manage Tax</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open_multipart('admin/Caccounts/update_tax', array('class' => '', 'id' => 'update_tax', 'name' => 'update_tax')) ?>
                    <input type="hidden" name="tax_id" value="<?= $tax_id ?>">
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="tax_name">Tax Name / Type <span class="text-danger">*</span></label>  
                                <input type="text" class="form-control" name="tax_name" id="tax_name" required="" value="<?= $tax_name ?>" placeholder="Enter tax Name / Type" />
                                <div class="invalid-feedback">
                                    Tax Name Required or Already Used
                                </div>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="enter_tax">Tax<span class="text-danger">*</span></label>  
                                <input type="number" class="form-control" name="enter_tax" id="enter_tax" required="" value="<?php echo $tax ?>" placeholder="Enter tax" />
                                  
                                
                                
                                <div class="invalid-feedback">
                                    Tax Required
                                </div>
                            </div>

                        </div>


                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed" name="edit-tax" value="Save"><span class="fal fa-check mr-1"></span>Save Changes</button>

                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>


<!-- Edit TAX start -->


<script>
    
    
    
    
    
     $(document).ready(function () {
        $('#update_tax').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                    tax_name: {
                        remote: '<?= base_url() ?>admin/Caccounts/checkTax/<?=$tax_id?>'
                    }
            },
            messages: {
                tax_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>