<!-- Manage Product Start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('manage_account') ?></h1>
            <small><?php echo display('manage_account') ?></small>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url('') ?>"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="<?php echo base_url('Account_Controller') ?>"><?php echo display('account') ?></a></li>
                <li class="active"><?php echo display('manage_account') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <?php
                    if ($menu_rights['add_right'] == 1) {
                        ?>
                        <a href="<?php echo base_url('Account_Controller') ?>" class="btn btn-info m-b-5 m-r-2"><i class="ti-plus"> </i><?php echo display('create_accounts') ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>


        <!-- Manage Account -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('manage_account') ?></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="account_data" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th><?php echo display('sl') ?></th>
                                        <th><?php echo display('account_name') ?></th>
                                        <th>Created <?php echo display('date') ?></th>
                                        <th style="width : 130px"><?php echo display('action') ?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Manage Product End -->

<!-- Delete Product ajax code -->
<script type="text/javascript">

    $(document).ready(function () {
        var _account_data = $('#account_data').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>t<'bottom'><'row'<'col-sm-5'i>p>",
//            dom: "<'top'<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>>t<'bottom'<'row'<'col-sm-4'i><'col-sm-4 text-center'><'col-sm-4'p>>>",
            "oTableTools": {
                "aButtons": [
                ]
            },
            buttons: [
                {extend: 'excel', title: 'manage_account', className: 'btn-sm', exportOptions: {columns: [0, 1, 2], modifier: {page: 'current'}}},
                {extend: 'print', className: 'btn-sm', exportOptions: {columns: [0, 1, 2], modifier: {page: 'current'}}}
            ],
            "autoWidth": true,
            "pageLength": 10,
            "serverSide": true,
            "processing": true,
            "order": [],
            "columnDefs": [{
                    "targets": [0, 3],
                    "orderable": false
                }, {
                    "targets": [0, 3],
                    "className": 'text-center'
                }],
            "ajax": {
                "url": "<?= base_url() ?>Account_Controller/get_account_list",
                "type": "POST"
            }
        });
        $("#account_data thead input").on('keyup change', function () {
            _account_data.column($(this).parent().index() + ':visible').search(this.value).draw();
        });

        $(document).on('click', '.deleteAccount', function () {
            var account_id = $(this).attr('name');
            var csrf_test_name = $("[name=csrf_test_name]").val();

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }, function (result) {
                fullScreenLoader();
                if (result) {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url('Account_Controller/account_delete') ?>',
                        data: {account_id: account_id, csrf_test_name: csrf_test_name},
                        cache: false,
                        success: function (datas) {
                            HoldOn.close();
                            location.reload();
                        }
                    });
                }
                HoldOn.close();
            });
        });
    });
</script>
