<!--Edit account start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('account_edit') ?></h1>
            <small><?php echo display('account_edit') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('account') ?></a></li>
                <li class="active"><?php echo display('account_edit') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>                    
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <a href="<?php echo base_url('Account_Controller/manage_account') ?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-align-justify"> </i> <?php echo display('manage_account') ?></a>
                </div>
            </div>
        </div>

        <!-- Edit account -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('account_edit') ?></h4>
                        </div>
                    </div>
                    <?php echo form_open('Account_Controller/account_update', array('class' => 'form-vertical', 'id' => 'add_account')) ?>
                    <div class="panel-body">

                        <div class="form-group row">
                            <label for="first_name" class="col-sm-3 col-form-label"><?php echo display('account_name') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="account_name" id="account_name" type="text" placeholder="Account Name"  required="" value="{account_name}">
                                <span></span>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="parent_id" class="col-sm-3 col-form-label"><?php echo display('root_account') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <select name="parent_id" class="form-control" id="parent_account" data-placeholder="-- select one --" required=""> 
                                    <option value=""></option>
                                    <option value="1" <?php
                                    if ($parent_id == 1) {
                                        echo "selected";
                                    }
                                    ?>><?php echo display('supplier') ?></option>
                                    <option value="2" <?php
                                    if ($parent_id == 2) {
                                        echo "selected";
                                    }
                                    ?>><?php echo display('customer') ?> </option>
                                    <option value="3" <?php
                                    if ($parent_id == 3) {
                                        echo "selected";
                                    }
                                    ?>>Fixed Asset</option>
                                    <!--<option value="4" <?php // if ($parent_id == 4) {echo "selected"; }                    ?>><?php // echo display('loan')                    ?></option>-->
                                </select>
                                <span></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-sm-3 col-form-label">Account For<i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <select name="status" class="form-control" id="status" data-placeholder="-- select one --" required=""> 
                                    <option value=""></option>
                                    <option value="1" <?php echo $status == 1 ? 'selected' : '' ?>>Payment</option>
                                    <option value="2" <?php echo $status == 2 ? 'selected' : '' ?>>Expense</option>
                                </select>
                                <span></span>
                            </div>
                        </div>


                        <input type="hidden" value="{account_id}" name="account_id">

                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <input type="submit" id="add-Account" class="btn btn-success btn-large" name="add-Account" value="<?php echo display('save_changes') ?>" />
                            </div>
                        </div>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $("#add_account").validate({
        errorPlacement: function (error, element) {
            error.insertAfter(element.next());
        },
        rules: {
            account_name: {
                remote: '<?= base_url() ?>/Account_controller/checkAccount/{account_id}'
            }
        },
        messages: {
            account_name: {remote: jQuery.validator.format("{0} is already in use, Try another Account")}
        }, submitHandler: function (form) {
            form.submit();
        }
    });
</script>
<!-- Edit account end -->



