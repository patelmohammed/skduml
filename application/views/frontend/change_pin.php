<div class="container-fluid">
    <div class="col-12 p-0">
        <div class="jumbotron text-center m-0 d-flex flex-column justify-content-center" style="min-height: 78vh !important;background: transparent;">
            <div class="row">
                <div class="col-xl-12">
                    <h2 class="fs-xxl fw-500 mt-4 text-white text-center">
                        Change Your Pin
                    </h2>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 ml-auto mr-auto">
                    <div class="card p-4 rounded-plus bg-faded">
                        <?= form_open(base_url($this->uri->uri_string()), $arrayName = array('id' => 'change_pin', "autocomplete" => "off")) ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert" style="margin-bottom: 0;">
                                    OTP send to <strong><?= isset($sabhasad_data->sabhasad_contact) && !empty($sabhasad_data->sabhasad_contact) ? '+91' . getTruncatedCCNumber($sabhasad_data->sabhasad_contact) : '' ?></strong>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-6 mt-3">
                                <a href="<?= base_url('dashboard') ?>" class="btn btn-block btn-primary btn-lg" style="padding: 0.5rem 0.5rem;">Dashboard</a>
                            </div>
                            <div class="col-6 mt-3">
                                <button type="submit" class="btn btn-block btn-danger btn-lg" value="1" name="submit" style="padding: 0.5rem 0.5rem;">Send OTP</button>
                            </div>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#change_pin').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>