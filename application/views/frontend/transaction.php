<div class="container py-4 py-lg-5 px-4 px-sm-0" style="padding-left: 1rem !important;padding-right: 1rem !important;">
    <div class="row">
        <div class="col-xl-12 col-md-12 col-sm-12 ml-auto mr-auto">
            <div class="card rounded-plus bg-faded" style="padding: 1.5rem 1rem 1.5rem 1rem;">
                <form method="post" id="transactionHistory" action="javascript:void(0);">
                    <div class="row"> 
                        <div class="col-md-2 mb-3">
                            <div class="form-group"> 
                                <input tabindex="1" type="text" id="startDate" name="startDate" readonly class="form-control" placeholder="Start Date" value="<?php echo set_value('startDate'); ?>" required="">
                            </div> 
                        </div>
                        <div class="col-md-2 mb-3">
                            <div class="form-group">
                                <input tabindex="2" type="text" id="endDate" name="endDate" readonly class="form-control" placeholder="End Date" value="<?php echo set_value('endDate'); ?>" required="">
                            </div>
                        </div>
                        <div class="col-md-2 mb-3">
                            <div class="form-group">
                                <button type="submit"  class="btn btn-block btn-primary pull-left">Search</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!--                <div class="row">
                                    <div class="col-md-12">
                                        <div class="demo-window-content">
                                            <div class="row no-gutters">
                                                <div data-spy="scroll" data-target="#spyscroll-2" data-offset="0" class="position-relative overflow-auto" style="width: 100%;" id="scrollspy">
                                                    <table id="transactionHistory" class="table table-bordered table-hover table-striped w-100">
                                                        <thead>
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Milk (Ltr)</th>
                                                                <th><span style="color: green;">Credit</span><br><span style="color: red;">Debit</span></th>
                                                                <th>Balance</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-hover table-striped w-100" style="margin-bottom: 0;">
                            <thead>
                                <tr>
                                    <th class="text-center" style="vertical-align: middle;width: 25%;">Date</th>
                                    <th class="text-center" style="vertical-align: middle;width: 25%;">Milk (Ltr)</th>
                                    <th class="text-center" style="vertical-align: middle;width: 25%;"><span style="color: green;">Credit</span> /<span style="color: red;">Debit</span></th>
                                    <th class="text-center" style="vertical-align: middle;width: 25%;">Balance</th>
                                </tr>
                            </thead>
                        </table>
                        <div id="scroll_div">
                            <table id="transactionHistory" class="table table-bordered table-hover table-striped w-100">
                                <tbody>

                                </tbody>
                            </table>                     
                        </div>                       
                    </div>   
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
//        $('#scrollspy').css({'height': window.innerHeight - 250});
        $('#scroll_div').slimScroll({
            color: color.primary._700,
            size: '20px',
            height: window.innerHeight - 280,
            alwaysVisible: true
        });


        getTransactionHistory();

        $('#startDate').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            orientation: "bottom left",
            templates: controls,
            autoclose: true,
            endDate: "nextday",
            maxDate: today,
            onClose: function () {
                $(this).valid();
            }
        });
        $('#endDate').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            orientation: "bottom left",
            templates: controls,
            autoclose: true,
            endDate: "nextday",
            maxDate: today,
            onClose: function () {
                $(this).valid();
            }
        });

        $('#transactionHistory').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
//                form.submit();
                getTransactionHistory();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    function getTransactionHistory() {
        $.ajax({
            type: "POST",
            url: '<?= base_url('getTransactionHistory') ?>',
            data: $('#transactionHistory').serialize(),
            success: function (returnData) {
                var data = JSON.parse(returnData);
                $('#transactionHistory tbody').html('');
                $('#transactionHistory tbody').append(data.row_data);
            }
        });
    }
</script>