<div class="container py-4 py-lg-5 px-4 px-sm-0">
    <div class="row">
        <div class="col-xl-6 col-md-6 col-sm-6 ml-auto mr-auto d-none d-xl-block d-lg-block d-md-block">
            <div class="card p-4 rounded-plus bg-faded">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                        <a href="<?= base_url('booking') ?>" class="btn btn-block btn-success btn-lg d-none d-xl-block d-lg-block d-md-block">Booking</a>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                        <a href="<?= base_url('transaction') ?>" class="btn btn-block btn-info btn-lg d-none d-xl-block d-lg-block d-md-block">Transaction</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-3">
                        <a href="<?= base_url('change-pin') ?>" class="btn btn-block btn-primary btn-lg d-none d-xl-block d-lg-block d-md-block">Change PIN</a>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-3">
                        <a href="<?= base_url('logout') ?>" class="btn btn-block btn-dark btn-lg d-none d-xl-block d-lg-block d-md-block">Logout</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-md-8 col-sm-8 ml-auto mr-auto d-none d-sm-block d-md-none">
            <div class="card p-4 rounded-plus bg-faded">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                        <a href="<?= base_url('booking') ?>" class="btn btn-block btn-success btn-lg d-none d-sm-block d-md-none" style="padding: 0.75rem 0.75rem;">Booking</a>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                        <a href="<?= base_url('transaction') ?>" class="btn btn-block btn-info btn-lg d-none d-sm-block d-md-none" style="padding: 0.5rem 0.5rem;">Transaction</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-3">
                        <a href="<?= base_url('change-pin') ?>" class="btn btn-block btn-primary btn-lg d-none d-sm-block d-md-none" style="padding: 0.5rem 0.5rem;">Change PIN</a>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-3">
                        <a href="<?= base_url('logout') ?>" class="btn btn-block btn-dark btn-lg d-none d-sm-block d-md-none" style="padding: 0.5rem 0.5rem;">Logout</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-md-6 col-sm-6 ml-auto mr-auto d-block d-sm-none">
            <div class="card p-4 rounded-plus bg-faded">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                        <a href="<?= base_url('booking') ?>" class="btn btn-block btn-success btn-lg d-block d-sm-none" style="padding: 0.75rem 0.75rem;">Booking</a>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                        <a href="<?= base_url('transaction') ?>" class="btn btn-block btn-info btn-lg d-block d-sm-none" style="padding: 0.75rem 0.75rem;">Transaction</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-3">
                        <a href="<?= base_url('change-pin') ?>" class="btn btn-block btn-primary btn-lg d-block d-sm-none" style="padding: 0.75rem 0.75rem;">Change PIN</a>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-3">
                        <a href="<?= base_url('logout') ?>" class="btn btn-block btn-dark btn-lg d-block d-sm-none" style="padding: 0.75rem 0.75rem;">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
//        $('#sabhasad_password').on('keyup', function () {
//            if ($(this).val().length == 4) {
//                $('#login').submit();
//            }
//        });
//
//        $('#sabhasad_code').on('blur', function () {
//            if ($(this).val() != undefined && $(this).val() != null && $(this).val() != '') {
//                $('#login').submit();
//            }
//        });

        $('#login').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>