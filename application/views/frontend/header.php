<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>
            <?= isset($this->page_title) && !empty($this->page_title) ? $this->page_title : '' ?>
        </title>
        <meta name="description" content="<?= COMPANY_NAME ?>">
        <meta name="author" content="Weborative IT Consultancy LLP">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>favicon-32x32.png">
        <link rel="mask-icon" href="<?= base_url() ?>assets/admin/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/fa-brands.css">

        <script src="<?= base_url() ?>assets/admin/js/vendors.bundle.js"></script>
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/notifications/toastr/toastr.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/notifications/sweetalert2/sweetalert2.bundle.css">

        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/datagrid/datatables/datatables.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?= base_url() ?>assets/admin/css/formplugins/bootstrap-datepicker/bootstrap-datepicker.css">

        <style>
            .page-logo{
                width: 100%;
            }
        </style>
    </head>
    <body>
        <div class="page-wrapper">
            <div class="page-inner bg-brand-gradient">
                <div class="page-content-wrapper bg-transparent m-0">
                    <div class="height-10 w-100 shadow-lg px-4 bg-brand-gradient">
                        <div class="d-flex align-items-center container p-0">
                            <div class="page-logo width-mobile-auto m-0 align-items-center justify-content-center p-0 bg-transparent bg-img-none shadow-0 height-9">
                                <a href="javascript:void(0)" class="page-logo-link press-scale-down d-flex align-items-center">
                                    <span class="page-logo-text mr-1 text-center d-none d-xl-block d-lg-block d-md-block d-sm-block" style="font-weight: bold;font-size: 2rem;">શ્રી કુડસદ દૂધ ઉત્પાદક સહકારી મંડળી લી.</span>
                                    <span class="page-logo-text mr-1 text-center d-block d-sm-none" style="font-weight: bold;font-size: 1.1rem;">શ્રી કુડસદ દૂધ ઉત્પાદક સહકારી મંડળી લી.</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="flex-1" style="background: url(<?= base_url() ?>assets/admin/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover;">