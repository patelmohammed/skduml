<div class="position-absolute pos-bottom pos-left pos-right text-center text-white" style="padding: 0 1rem 1rem 1rem 1rem;">
    <?= '2021' . ' - ' . date('Y', strtotime('+1 year')) ?> © <?= COMPANY_NAME ?> by&nbsp;<a href='https://www.weborative.com' class='text-white opacity-40 fw-500' title='weborative.com' target='_blank'>weborative.com</a>
</div>
</div>
</div>
</div>
</div>
<!-- BEGIN Color profile -->
<!-- this area is hidden and will not be seen on screens or screen readers -->
<!-- we use this only for CSS color refernce for JS stuff -->
<p id="js-color-profile" class="d-none">
    <span class="color-primary-50"></span>
    <span class="color-primary-100"></span>
    <span class="color-primary-200"></span>
    <span class="color-primary-300"></span>
    <span class="color-primary-400"></span>
    <span class="color-primary-500"></span>
    <span class="color-primary-600"></span>
    <span class="color-primary-700"></span>
    <span class="color-primary-800"></span>
    <span class="color-primary-900"></span>
    <span class="color-info-50"></span>
    <span class="color-info-100"></span>
    <span class="color-info-200"></span>
    <span class="color-info-300"></span>
    <span class="color-info-400"></span>
    <span class="color-info-500"></span>
    <span class="color-info-600"></span>
    <span class="color-info-700"></span>
    <span class="color-info-800"></span>
    <span class="color-info-900"></span>
    <span class="color-danger-50"></span>
    <span class="color-danger-100"></span>
    <span class="color-danger-200"></span>
    <span class="color-danger-300"></span>
    <span class="color-danger-400"></span>
    <span class="color-danger-500"></span>
    <span class="color-danger-600"></span>
    <span class="color-danger-700"></span>
    <span class="color-danger-800"></span>
    <span class="color-danger-900"></span>
    <span class="color-warning-50"></span>
    <span class="color-warning-100"></span>
    <span class="color-warning-200"></span>
    <span class="color-warning-300"></span>
    <span class="color-warning-400"></span>
    <span class="color-warning-500"></span>
    <span class="color-warning-600"></span>
    <span class="color-warning-700"></span>
    <span class="color-warning-800"></span>
    <span class="color-warning-900"></span>
    <span class="color-success-50"></span>
    <span class="color-success-100"></span>
    <span class="color-success-200"></span>
    <span class="color-success-300"></span>
    <span class="color-success-400"></span>
    <span class="color-success-500"></span>
    <span class="color-success-600"></span>
    <span class="color-success-700"></span>
    <span class="color-success-800"></span>
    <span class="color-success-900"></span>
    <span class="color-fusion-50"></span>
    <span class="color-fusion-100"></span>
    <span class="color-fusion-200"></span>
    <span class="color-fusion-300"></span>
    <span class="color-fusion-400"></span>
    <span class="color-fusion-500"></span>
    <span class="color-fusion-600"></span>
    <span class="color-fusion-700"></span>
    <span class="color-fusion-800"></span>
    <span class="color-fusion-900"></span>
</p>
<!-- END Color profile -->
<script src="<?= base_url() ?>assets/admin/js/app.bundle.js"></script>
<script src="<?= base_url() ?>assets/admin/js/formplugins/jquery-validate/jquery.validate.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/formplugins/jquery-validate/additional-methods.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/notifications/toastr/toastr.js"></script>
<script src="<?= base_url() ?>assets/admin/js/notifications/sweetalert2/sweetalert2.bundle.js"></script>

<script src="<?= base_url() ?>assets/admin/js/datagrid/datatables/datatables.bundle.js"></script>
<script src="<?= base_url() ?>assets/admin/js/datagrid/datatables/datatables.export.js"></script>
<script src="<?= base_url() ?>assets/admin/js/formplugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });

    var today = new Date();
    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    };

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": 300,
        "hideDuration": 100,
        "timeOut": 5000,
        "extendedTimeOut": 1000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    $(document).ready(function () {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom left",
            templates: controls,
            autoclose: true,
//            endDate: "nextday",
            maxDate: today
        });

<?= $add_message; ?>
    });
</script>
</body>
</html>
