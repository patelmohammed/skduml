<div class="container py-4 py-lg-5 px-4 px-sm-0" style="padding-left: 1rem !important;padding-right: 1rem !important;">
    <div class="row">
        <div class="col-xl-12 col-md-12 col-sm-12 ml-auto mr-auto">
            <div class="card rounded-plus bg-faded" style="padding: 1.5rem 1rem 1.5rem 1rem;">
                <!--                <div class="panel-content border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                                    <a href="< base_url('dashboard') ?>" class="btn btn-primary btn-md ml-auto">Dashboard</a>
                                </div>-->
                <?= form_open(base_url($this->uri->uri_string())) ?>
                <div class="row"> 
                    <div class="col-md-2 mb-3">
                        <div class="form-group"> 
                            <input tabindex="1" type="text" id="startDate" name="startDate" readonly class="form-control date" placeholder="Start Date" value="<?php echo set_value('startDate'); ?>">
                        </div> 
                    </div>
                    <div class="col-md-2 mb-3">
                        <div class="form-group">
                            <input tabindex="2" type="text" id="endDate" name="endDate" readonly class="form-control date" placeholder="End Date" value="<?php echo set_value('endDate'); ?>">
                        </div>
                    </div>
                    <div class="col-md-2 mb-3">
                        <div class="form-group">
                            <button type="submit"  class="btn btn-block btn-primary pull-left">Search</button>
                        </div>
                    </div>
                </div>
                <?= form_close() ?>
                <div class="row">
                    <div class="col-md-12">
                        <table id="sale_report_by_sabhasad_datatable" class="table table-bordered table-hover table-striped w-100">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Milk (Ltr)</th>
                                    <th><span style="color: green;">Credit</span><br><span style="color: red;">Debit</span></th>
                                    <th>Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $totalBalance = $totalCredit = $totalDebit = $totalMilkQuantity = 0;
                                if (isset($saleReportBySabhasad)) {
                                    foreach ($saleReportBySabhasad as $key => $value) {
                                        if ($value->payment_type == 'credit') {
                                            $totalBalance += $value->credit_amount;
                                            $totalCredit += $value->credit_amount;
                                        } else if ($value->payment_type == 'debit') {
                                            $totalBalance -= $value->debit_amount;
                                            $totalDebit += $value->debit_amount;
                                        }
                                        $totalMilkQuantity += (isset($value->milk_quantity) && !empty($value->milk_quantity) ? $value->milk_quantity : 0);
                                        $key++;
                                        ?>
                                        <tr>
                                            <td><?= date('d-m-Y', strtotime($value->paymentDate)) ?></br><?= date('h:i a', strtotime($value->paymentDate)) ?></td>
                                            <td><?= $value->milk_quantity . (isset($value->milk_price) && !empty($value->milk_price) ? ' * ' . number_format($value->milk_price, 2) : '') ?></td>
                                            <td>
                                                <?= isset($value->credit_amount) && !empty($value->credit_amount) ? '<span style="color: green;">' . $value->credit_amount . '</span>' : '' ?>
                                                <?= isset($value->debit_amount) && !empty($value->debit_amount) ? '<span style="color: red;">' . $value->debit_amount . '</span>' : '' ?>
                                            </td>
                                            <td><?= number_format($totalBalance, 2) ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
<!--                            <tfoot>
                                <tr>
                                    <th style="text-align: right">Total </th>
                                    <th colspan="2"> number_format($totalMilkQuantity, 2) ?></th>
                                    <th> number_format($totalBalance, 2) ?></th>
                                </tr>
                            </tfoot>-->
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
//        $('#sabhasad_password').on('keyup', function () {
//            if ($(this).val().length == 4) {
//                $('#login').submit();
//            }
//        });
//
//        $('#sabhasad_code').on('blur', function () {
//            if ($(this).val() != undefined && $(this).val() != null && $(this).val() != '') {
//                $('#login').submit();
//            }
//        });

        $('#login').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });

        var table = $('#sale_report_by_sabhasad_datatable').dataTable({
            responsive: true,
            select: true,
//            dom: "<'row '<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'lf><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
//                    "<'row'<'col-sm-12'tr>>" +
//                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                    extend: 'pdfHtml5',
                    text: '<i class="fal fa-file-pdf "></i>',
                    titleAttr: 'Generate PDF',
                    className: 'btn-outline-danger btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#sale_report_by_sabhasad_datatable').data('title'),
                    messageTop: $('#sale_report_by_sabhasad_datatable').data('msgtop')
                }, {
                    extend: 'excelHtml5',
                    text: '<i class="fal fa-file-excel "></i>',
                    titleAttr: 'Generate Excel',
                    className: 'btn-outline-success btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-success-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#sale_report_by_sabhasad_datatable').data('title'),
                    messageTop: $('#sale_report_by_sabhasad_datatable').data('msgtop')
                }, {
                    extend: 'csvHtml5',
                    text: '<i class="fal fa-file-csv "></i>',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-primary btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    }
                }, {
                    extend: 'copyHtml5',
                    text: '<i class="fal fa-copy "></i>',
                    titleAttr: 'Copy to clipboard',
                    className: 'btn-outline-info btn-sm mr-1 hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#sale_report_by_sabhasad_datatable').data('title'),
                    messageTop: $('#sale_report_by_sabhasad_datatable').data('msgtop')
                }, {
                    extend: 'print',
                    text: '<i class="fal fa-print "></i>',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-dark btn-sm hover-effect-dot',
                    attr: {
                        "data-toggle": 'tooltip',
                        "data-template": '<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-dark-500"></div></div>'
                    },
                    exportOptions: {
                        columns: ':not(.notexport)'
                    },
                    title: $('#sale_report_by_sabhasad_datatable').data('title'),
                    messageTop: $('#sale_report_by_sabhasad_datatable').data('msgtop')
                }
            ],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            },
            columnDefs: [
                {orderable: false, targets: [0, 1, 2, 3]},
//                {"width": "25%", "targets": 0},
//                {"width": "25%", "targets": 1},
//                {"width": "25%", "targets": 2},
//                {"width": "25%", "targets": 3},
            ],
            "language": {
                "lengthMenu": "_MENU_"
            }
        });
    });
</script>