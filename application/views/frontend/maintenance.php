<main id="js-page-content" role="main" class="page-content">
    <div class="h-alt-f d-flex flex-column align-items-center justify-content-center text-center">
        <h1 class="page-error color-danger-400">
            SORRY
            <small class="fw-500">
                Sorry for the inconvenience our site is under maintenance.
            </small>
        </h1>
        <h3 class="fw-500 mb-5 text-danger">
            You have experienced a technical error. We apologize.
        </h3>
    </div>
</main>
