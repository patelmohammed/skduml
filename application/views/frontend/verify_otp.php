<div class="container-fluid">
    <div class="col-12 p-0">
        <div class="jumbotron text-center m-0 d-flex flex-column justify-content-center" style="min-height: 78vh !important;background: transparent;">
            <div class="row">
                <div class="col-xl-12">
                    <h2 class="fs-xxl fw-500 mt-4 text-white text-center">
                        Verify Your OTP
                        <small class="h3 fw-300 mt-3 mb-5 text-white opacity-60 hidden-sm-down">
                            Verify your otp to reset pin.
                        </small>
                    </h2>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 ml-auto mr-auto">
                    <div class="card p-4 rounded-plus bg-faded">
                        <?= form_open(base_url('verify_otp'), $arrayName = array('id' => 'verify_otp')) ?>
                        <div class="form-group row">
                            <label class="col-xl-6 col-md-6 col-sm-6 form-label" for="otp">OTP</label>
                            <div class="col-xl-6 col-md-6 col-sm-12 pr-1">
                                <input type="text" id="otp" name="otp" class="form-control" placeholder="OTP" required>
                                <div class="invalid-feedback">No, you missed this one.</div>
                                <div class="help-block">Enter otp sent to your verified contact number.</div>
                            </div>
                        </div>
                        <div class="form-group row" style="text-align: center;margin-bottom: 0;">
                            <label class="col-xl-12 col-md-12 col-sm-12 form-label" id="timer"></label>
                        </div>
                        <div class="row">
                            <div class="col-4 mt-3 pr-0">
                                <a href="<?= base_url('dashboard') ?>" class="btn btn-block btn-primary btn-lg" style="font-size: 0.86rem;padding: 0.5rem 0.5rem;">Dashboard</a>
                            </div>
                            <div class="col-4 mt-3 pr-0">
                                <button id="resend_otp" type="button" class="btn btn-block btn-success btn-lg" style="padding: 0.5rem 0.5rem;">Resend</button>
                            </div>
                            <div class="col-4 mt-3">
                                <button id="js-login-btn" type="submit" class="btn btn-block btn-danger btn-lg" style="padding: 0.5rem 0.5rem;">Verify</button>
                            </div>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });

        disableResend();
        timer(120);

        $('#resend_otp').on('click', function () {
            disableResend();
            timer(120);
            $.ajax({
                url: "<?= base_url() ?>resend_otp",
                type: 'post',
                data: {},
                success: function (data) {
                    var returndata = JSON.parse(data);
                    if (returndata.result == true) {
                        swalWithBootstrapButtons.fire("Success!", returndata.message, "success");
                    } else {
                        swalWithBootstrapButtons.fire("Error!", returndata.message, "error");
                    }
                },
                error: function () {
                    swalWithBootstrapButtons.fire("Error!", "We are facing technical error!", "error");
                    return false;
                }
            });

        });

        $('#verify_otp').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    function disableResend() {
        $("#resend_otp").attr("disabled", true);
//        $("#resend_otp").hide();
        timer(120);
        setTimeout(function () {
            $('#resend_otp').removeAttr("disabled");
//            $("#resend_otp").show();
        }, 120000);
    }

    let timerOn = true;

    function timer(remaining) {
        var m = Math.floor(remaining / 60);
        var s = remaining % 60;

        m = m < 10 ? '0' + m : m;
        s = s < 10 ? '0' + s : s;
        document.getElementById('timer').innerHTML = m + ':' + s;
        remaining -= 1;

        if (remaining >= 0 && timerOn) {
            setTimeout(function () {
                timer(remaining);
            }, 1000);
            return;
        }

        if (!timerOn) {
            // Do validate stuff here
            return;
        }
    }
</script>