<div class="container-fluid">
    <div class="col-12 p-0">
        <div class="jumbotron text-center m-0 d-flex flex-column justify-content-center" style="min-height: 78vh !important;background: transparent;">
            <div class="row">
                <div class="col-xl-12">
                    <h2 class="fs-xxl fw-500 mt-4 text-white text-center">
                        Reset Your Pin
                        <small class="h3 fw-300 mt-3 mb-5 text-white opacity-60 hidden-sm-down">
                            Verify your otp to reset pin.
                        </small>
                    </h2>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 ml-auto mr-auto">
                    <div class="card p-4 rounded-plus bg-faded">
                        <?= form_open(base_url('reset_pin'), $arrayName = array('id' => 'reset_pin', "autocomplete" => "off")) ?>
                        <div class="form-group row">
                            <label class="col-xl-6 col-md-6 col-sm-6 form-label" for="new_pin">New PIN</label>
                            <div class="col-6 pr-1">
                                <input type="password" id="new_pin" name="new_pin" class="form-control" placeholder="New Pin" required maxlength="4" autocomplete="new-password">
                                <div class="invalid-feedback">No, you missed this one.</div>
                                <div class="help-block">Your new 4 digit PIN</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-6 col-md-6 col-sm-6 form-label" for="confirm_pin">Confirm PIN</label>
                            <div class="col-6 pr-1">
                                <input type="password" id="confirm_pin" name="confirm_pin" class="form-control" placeholder="Confirm Pin" required maxlength="4" autocomplete="new-password">
                                <div class="invalid-feedback">No, you missed this one.</div>
                                <div class="help-block">Confirm your 4 digit PIN</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 mt-3">
                                <a href="<?= base_url('dashboard') ?>" class="btn btn-block btn-primary btn-lg" style="padding: 0.5rem 0.5rem;">Dashboard</a>
                            </div>
                            <div class="col-6 mt-3">
                                <button id="js-login-btn" type="submit" class="btn btn-block btn-danger btn-lg" style="padding: 0.5rem 0.5rem;">Reset PIN</button>
                            </div>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#reset_pin').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>