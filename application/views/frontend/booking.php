<style>
    .calculator {
        border: 1px solid #ccc;
        border-radius: 5px;
        top: 50%;
        /*left: 5%;*/
        width: 100%;
    }

    .calculator-screen {
        width: 100%;
        height: 80px;
        border: none;
        background-color: #252525;
        color: #fff;
        text-align: right;
        padding-right: 20px;
        padding-left: 10px;
        font-size: 4rem;
    }
    .calculator-keys {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        grid-gap: 20px;
        padding: 20px;
    }

    #invoice-POS{
        box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
        padding:2mm;
        margin: 0 auto;
        width: 100%;
        background: #FFF;


        ::selection {background: #f31544; color: #FFF;}
        ::moz-selection {background: #f31544; color: #FFF;}
    }
    #invoice-POS h1{
        /*font-size: 1.5em;*/
        color: #222;
    }
    /*#invoice-POS h2{font-size: 1.5em;}*/
    #invoice-POS h3{
        /*font-size: 1.2em;*/
        font-weight: 300;
        line-height: 2em;
    }
    #invoice-POS p{
        /*font-size: .7em;*/
        color: #666;
        line-height: 1.2em;
    }

    #invoice-POS #top, #mid,#bot{ /* Targets all id with 'col-' */
        border-bottom: 1px solid #EEE;
    }

    /*    #invoice-POS #top{min-height: 100px;}*/
    #invoice-POS #mid{min-height: 80px;} 
    #invoice-POS #bot{ min-height: 50px;}

    #invoice-POS .info{
        display: block;
        /*float:left;*/
        margin-left: 0;
    }
    #invoice-POS .title{
        float: right;
    }
    #invoice-POS .title p{text-align: right;} 
    #invoice-POS table{
        width: 100%;
        border-collapse: collapse;
    }
    #invoice-POS td{
        /*padding: 5px 0 5px 15px;*/
        /*border: 1px solid #EEE*/
    }
    #invoice-POS .tabletitle{
        /*padding: 5px;*/
        font-size: .5em;
        background: #EEE;
    }
    #invoice-POS .tabletitle h2{
        /*padding: 5px;*/
        font-size: 2.5em;
    }
    #invoice-POS .service{border-bottom: 1px solid #EEE;}
    #invoice-POS .item{width: 24mm;}
    /*#invoice-POS .itemtext{font-size: .5em;}*/

    #invoice-POS #legalcopy{
        margin-top: 5mm;
    }

</style>
<div class="container px-sm-0">
    <div class="row">
        <div class="col-xl-12">
            <h2 class="fs-xxl fw-500 mt-3 text-white text-center">
                <?= $this->sabhasad_id = $this->session->userdata(SITE_NAME . '_sabhasad') != NULL ? $this->session->userdata(SITE_NAME . '_sabhasad')['sabhasad_code'] : '' ?>
                -
                <?= $this->session->userdata(SITE_NAME . '_sabhasad') != NULL ? $this->session->userdata(SITE_NAME . '_sabhasad')['sabhasad_name_en'] : '' ?>
                <?= $this->session->userdata(SITE_NAME . '_sabhasad') != NULL ? ' (' . $this->session->userdata(SITE_NAME . '_sabhasad')['sabhasad_name'] . ')' : '' ?>
                <small class="h3 fw-300 mt-1 text-white opacity-60">
                    Wallet Balance : <?= isset($total_balance) && !empty($total_balance) ? $total_balance : 0 ?>
                </small>
            </h2>
        </div>
        <div class="col-xl-12 col-md-12 col-sm-12 ml-auto mr-auto mb-5">
            <div class="card p-4 rounded-plus bg-faded">
                <?= form_open(base_url($this->uri->uri_string()), $arrayName = array('id' => 'debit')) ?>
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-6" style="padding: 0;">
                        <div class=""><!--my-4-->

                            <div class="calculator card">

                                <input type="text" class="calculator-screen z-depth-1" value="" disabled />

                                <div class="calculator-keys">

                                    <button type="button" value="7" class="btn btn-light waves-effect">7</button>
                                    <button type="button" value="8" class="btn btn-light waves-effect">8</button>
                                    <button type="button" value="9" class="btn btn-light waves-effect">9</button>


                                    <button type="button" value="4" class="btn btn-light waves-effect">4</button>
                                    <button type="button" value="5" class="btn btn-light waves-effect">5</button>
                                    <button type="button" value="6" class="btn btn-light waves-effect">6</button>


                                    <button type="button" value="1" class="btn btn-light waves-effect">1</button>
                                    <button type="button" value="2" class="btn btn-light waves-effect">2</button>
                                    <button type="button" value="3" class="btn btn-light waves-effect">3</button>


                                    <button type="button" value="0" class="btn btn-light waves-effect">0</button>
                                    <button type="button" class="decimal function btn btn-secondary" value=".50">.50</button>
                                    <button type="button" class="all-clear function btn btn-danger btn-sm" value="all-clear">Clear</button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-6 d-none d-xl-block d-lg-block d-md-block d-sm-block">

                        <div id="invoice-POS">

                            <center id="top">
                                <div class="info"> 
                                    <h2>શ્રી કુડસદ દૂધ ઉત્પાદક <br>સહકારી મંડળી લી.</h2>
                                </div><!--End Info-->
                            </center><!--End InvoiceTop-->

                            <div id="mid">
                                <div class="info">
                                    <h2 style="margin-top: 0.5rem;">
                                        <?= $this->session->userdata(SITE_NAME . '_sabhasad') != NULL ? $this->session->userdata(SITE_NAME . '_sabhasad')['sabhasad_name_en'] : '' ?>
                                        <?= $this->session->userdata(SITE_NAME . '_sabhasad') != NULL ? ' (' . $this->session->userdata(SITE_NAME . '_sabhasad')['sabhasad_name'] . ')' . '<br>' : '' ?>
                                    </h2>
                                    <p>
                                        Code : <?= $this->session->userdata(SITE_NAME . '_sabhasad') != NULL ? $this->session->userdata(SITE_NAME . '_sabhasad')['sabhasad_code'] : '' ?>
                                    </p>
                                    <p> 
                                        Phone   : <?= isset($sabhasad_data->sabhasad_contact) && !empty($sabhasad_data->sabhasad_contact) ? $sabhasad_data->sabhasad_contact : '' ?>
                                    </p>
                                </div>
                            </div><!--End Invoice Mid-->

                            <div id="bot">

                                <div id="table">
                                    <table>
                                        <tr class="tabletitle">
                                            <td class="item"><h2>Item</h2></td>
                                            <td class="Hours"><h2>Qty * Price</h2></td>
                                            <td class="Rate"><h2>Sub Total</h2></td>
                                        </tr>

                                        <tr class="service">
                                            <td class="tableitem"><p class="itemtext">Buffalo</p></td>
                                            <td class="tableitem"><p class="itemtext"> <span id="milk_quantity">0</span> * <?= isset($milk_data[0]->milk_price) && !empty($milk_data[0]->milk_price) ? $milk_data[0]->milk_price : 0 ?></p></td>
                                            <td class="tableitem"><p class="itemtext">&#8377;&nbsp;<span id="sub_total">0</span></p></td>
                                        </tr>
                                        <tr class="service">
                                            <td class="tableitem"><p class="itemtext">&nbsp;</p></td>
                                            <td class="tableitem"><p class="itemtext">&nbsp;</p></td>
                                            <td class="tableitem"><p class="itemtext">&nbsp;</p></td>
                                        </tr>

                                        <tr class="tabletitle">
                                            <td></td>
                                            <td class="Rate"><h2>Total</h2></td>
                                            <td class="payment"><h2>&#8377;&nbsp;<span id="total_payment">0</span></h2></td>
                                        </tr>

                                    </table>
                                </div><!--End Table-->
                            </div><!--End InvoiceBot-->
                        </div><!--End Invoice-->
                    </div>
                </div>
                <input type="hidden" name="hidden_milk_quantity" id="hidden_milk_quantity" value="0">
                <input type="hidden" name="hidden_milk_price" id="hidden_milk_price" value="0">
                <input type="hidden" name="hidden_total_payment" id="hidden_total_payment" value="0">
                <div class="row">
                    <div class="col-6" style="padding-left: 0;">
                        <a href="<?= base_url('dashboard') ?>" class="btn btn-block btn-primary btn-lg mt-3">Dashboard</a>
                    </div>
                    <div class="col-6" style="padding-right: 0;">
                        <button type="submit" class="btn btn-block btn-success btn-lg mt-3">Done</button>
                    </div>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>
<script>
    var milk_price = '<?= isset($milk_data[0]->milk_price) && !empty($milk_data[0]->milk_price) ? $milk_data[0]->milk_price : 0 ?>';
    var wallet_balance = '<?= isset($total_balance) && !empty($total_balance) ? $total_balance : 0 ?>';
    $(document).ready(function () {
        $('#debit').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                var error = false;
                var minumum_buy = '0.50';
                var hidden_milk_quantity = $('#hidden_milk_quantity').val();
                var hidden_milk_price = $('#hidden_milk_price').val();
                var hidden_total_payment = $('#hidden_total_payment').val();

                if (hidden_milk_quantity == undefined || hidden_milk_quantity == null && hidden_milk_quantity == '' || hidden_milk_quantity < parseFloat(minumum_buy)) {
                    error = true;
                    swalWithBootstrapButtons.fire("Error!", 'There is a minimum quantity is 0.50 ltr for this product.', "error");
                    return false;
                }
                if (hidden_milk_price == undefined || hidden_milk_price == null && hidden_milk_price == '') {
                    error = true;
                    return false;
                }
                if (hidden_total_payment == undefined || hidden_total_payment == null && hidden_total_payment == '' || hidden_total_payment <= 0) {
                    error = true;
                    return false;
                }
                console.log(error);
                if (error === false) {
                    form.submit();
                }
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    const calculator = {
        displayValue: '0',
        firstOperand: null,
        waitingForSecondOperand: false,
        operator: null,
    };

    function inputDigit(digit) {
        const {displayValue, waitingForSecondOperand} = calculator;

        if (waitingForSecondOperand === true) {
            calculator.displayValue = digit;
            calculator.waitingForSecondOperand = false;
        } else {
            if (displayValue.includes(".") == false) {
                calculator.displayValue = displayValue === '0' ? digit : displayValue + digit;
                if (checkBalance(calculator.displayValue) == false) {
                    swalWithBootstrapButtons.fire("Error!", 'You have not sufficient balance.', "error");
                    calculator.displayValue = displayValue;
                }
            }
        }
    }

    function inputDecimal(dot) {
        // If the `displayValue` does not contain a decimal point
        if (!calculator.displayValue.includes(dot)) {
            // Append the decimal point
            calculator.displayValue += dot;
            if (checkBalance(calculator.displayValue) == false) {
                swalWithBootstrapButtons.fire("Error!", 'You have not sufficient balance.', "error");
                calculator.displayValue = calculator.displayValue.split('.')[0];
            }
        }
    }

    function handleOperator(nextOperator) {
        const {firstOperand, displayValue, operator} = calculator
        const inputValue = parseFloat(displayValue);

        if (operator && calculator.waitingForSecondOperand) {
            calculator.operator = nextOperator;
            return;
        }

        if (firstOperand == null) {
            calculator.firstOperand = inputValue;
        } else if (operator) {
            const currentValue = firstOperand || 0;
            const result = performCalculation[operator](currentValue, inputValue);

            calculator.displayValue = String(result);
            calculator.firstOperand = result;
        }

        calculator.waitingForSecondOperand = true;
        calculator.operator = nextOperator;
    }

    const performCalculation = {
        '/': (firstOperand, secondOperand) => firstOperand / secondOperand,

        '*': (firstOperand, secondOperand) => firstOperand * secondOperand,

        '+': (firstOperand, secondOperand) => firstOperand + secondOperand,

        '-': (firstOperand, secondOperand) => firstOperand - secondOperand,

        '=': (firstOperand, secondOperand) => secondOperand
    };

    function resetCalculator() {
        calculator.displayValue = '0';
        calculator.firstOperand = null;
        calculator.waitingForSecondOperand = false;
        calculator.operator = null;
    }

    function updateDisplay() {
        const display = document.querySelector('.calculator-screen');
        display.value = calculator.displayValue;
        calculateAll(display.value);
    }

    updateDisplay();

    const keys = document.querySelector('.calculator-keys');
    keys.addEventListener('click', (event) => {
        const {target} = event;
        if (!target.matches('button')) {
            return;
        }

        if (target.classList.contains('operator')) {
            handleOperator(target.value);
            updateDisplay();
            return;
        }

        if (target.classList.contains('decimal')) {
            inputDecimal(target.value);
            updateDisplay();
            return;
        }

        if (target.classList.contains('all-clear')) {
            resetCalculator();
            updateDisplay();
            return;
        }

        inputDigit(target.value);
        updateDisplay();
    });

    function calculateAll(milk_quantity) {
        if ($.trim(milk_quantity) == "" || milk_quantity == undefined || isNaN(milk_quantity)) {
            milk_quantity = 0;
        }

        var total = milk_quantity * milk_price;
//        if (parseFloat(total) > parseFloat(wallet_balance)) {
//            total = 0;
//            resetCalculator();
//            updateDisplay();
//            $('#sub_total').html(total);
//            $('#total_payment').html(total);
//            swalWithBootstrapButtons.fire("Error!", 'You have not sufficient balance.', "error");
//            return;
//        }
        $('#milk_quantity').html(milk_quantity);
        $('#sub_total').html(total);
        $('#total_payment').html(total);

        $('#hidden_milk_quantity').val(milk_quantity);
        $('#hidden_milk_price').val(milk_price);
        $('#hidden_total_payment').val(total);
    }

    function checkBalance(milk_quantity) {
        if ($.trim(milk_quantity) == "" || milk_quantity == undefined || isNaN(milk_quantity)) {
            milk_quantity = 0;
        }

        var total = milk_quantity * milk_price;
        if (parseFloat(total) > parseFloat(wallet_balance)) {
            return false;
        } else {
            return true;
        }
    }
</script>