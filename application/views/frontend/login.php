<div class="container-fluid">
    <div class="row">
        <div class="col-12 p-0">
            <div class="jumbotron text-center m-0 d-flex flex-column justify-content-center" style="min-height: 78vh !important;background: transparent;">
                <div class="col-xl-12">
                    <h2 class="fs-xxl fw-500 mt-4 text-white text-center">
                        Login now!
                        <small class="h3 fw-300 mt-3 mb-5 text-white opacity-60 hidden-sm-down">
                            Your login required for Milk purchase.
                        </small>
                    </h2>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 ml-auto mr-auto">
                    <div class="card p-4 rounded-plus bg-faded">
                        <?= form_open(base_url(), $arrayName = array('id' => 'login', "autocomplete" => "off")) ?>
                        <div class="form-group row">
                            <label class="col-xl-6 col-md-6 col-sm-12 form-label" for="sabhasad_code">Sabhasad Code</label>
                            <div class="col-xl-6 col-md-6 col-sm-12 pr-1">
                                <input type="text" id="sabhasad_code" name="sabhasad_code" class="form-control" placeholder="Sabhasad Code" required autocomplete="off">
                                <div class="invalid-feedback">No, you missed this one.</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-6 col-md-6 col-sm-12 form-label" for="sabhasad_password">Sabhasad PIN:</label>
                            <div class="col-xl-6 col-md-6 col-sm-12 pr-1">
                                <input type="password" id="sabhasad_password" name="sabhasad_password" class="form-control" placeholder="Sabhasad Pin" required maxlength="4" autocomplete="new-password">
                                <div class="invalid-feedback">No, you missed this one.</div>
                                <div class="help-block">Your 4 digit PIN for Authentication</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                &nbsp;
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-block btn-danger btn-lg">Login</button>
                            </div>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
//        $('#sabhasad_password').on('keyup', function () {
//            if ($(this).val().length == 4) {
//                $('#login').submit();
//            }
//        });
//
//        $('#sabhasad_code').on('blur', function () {
//            if ($(this).val() != undefined && $(this).val() != null && $(this).val() != '') {
//                $('#login').submit();
//            }
//        });

        $('#login').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>