<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function dateprint($dt = '') {
    if ($dt == '') {
        return date('d-m-Y');
    } else {
        return date('d-m-Y', strtotime($dt));
    }
}

function dbdate($dt = '') {
    if ($dt == '') {
        return date('Y-m-d');
    } else {
        $old = explode('-', $dt);
        return date('Y-m-d', strtotime($old[2] . '-' . $old[1] . '-' . $old[0]));
    }
}

function preprint($val, $flag = TRUE) {
    echo '<pre>';
    print_r($val);
    echo '</pre>';

    if ($flag) {
        die;
    }
}

function cleanString($string) {
    $string = str_replace(' ', '-', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
    return preg_replace('/-+/', '-', $string);
}

function cleanStringNew($string) {
    $string = str_replace(' ', '_', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '_', $string);
    return strtolower(preg_replace('/-+/', '_', $string));
}

function set_selected($desired_value, $new_value) {
    if ($desired_value == $new_value) {
        echo ' selected="selected"';
    }
}

function set_checked($desired_value, $new_value) {
    if ($desired_value == $new_value) {
        echo ' checked="checked"';
    }
}

function generator($lenth, $table_name = '', $field_name = '') {
    $CI = & get_instance();
    $CI->load->model('Auth_model');

    $number = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "N", "M", "O", "P", "Q", "R", "S", "U", "V", "T", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

    for ($i = 0; $i < $lenth; $i++) {
        $rand_value = rand(0, 34);
        $rand_number = $number["$rand_value"];

        if (empty($con)) {
            $con = $rand_number;
        } else {
            $con = "$con" . "$rand_number";
        }
    }

    $existing_id = $CI->Auth_model->check_existing_generator_id($table_name, $field_name, $con);

    if (isset($existing_id) && !empty($existing_id)) {
        $this->generator(10, $table_name, $field_name);
    } else {
        return $con;
    }
}

function capitalFirst($string) {
    $string = strtolower($string);
    $string = ucwords($string);
    return $string;
}

function encodeId($simple_string) {
    // Use OpenSSl Encryption method
    $iv_length = openssl_cipher_iv_length(CIPHERING);
    $options = 0;

    // Use openssl_encrypt() function to encrypt the data
    $encryption = openssl_encrypt($simple_string, CIPHERING, ENCRYPTION_DECRYPTION_KEY, $options, ENCRYPTION_IV);

    return $encryption;
}

function decodeId($encryption) {
    // Use OpenSSl Encryption method
    $iv_length = openssl_cipher_iv_length(CIPHERING);
    $options = 0;

    // Use openssl_decrypt() function to decrypt the data
    $decryption = openssl_decrypt($encryption, CIPHERING, ENCRYPTION_DECRYPTION_KEY, $options, ENCRYPTION_IV);

    return $decryption;
}

function getMilkName($id) {
    $CI = & get_instance();
    $result = $CI->db->query("SELECT * FROM tbl_milk where milk_id ='$id' ")->row();
    return (isset($result->milk_name) && !empty($result->milk_name) ? $result->milk_name : '');
}

function getSabhasadName($id) {
    $return_data = new stdClass();
    $return_data->sabhasad_name = '';
    $return_data->sabhasad_name_en = '';
    $CI = & get_instance();
    $result = $CI->db->query("SELECT * FROM tbl_sabhasad where sabhasad_id ='$id' ")->row();
    return (isset($result) && !empty($result) ? $result : $return_data);
}

function getTruncatedCCNumber($ccNum) {
    return str_replace(range(0, 9), "*", substr($ccNum, 0, -4)) . substr($ccNum, -4);
}

function get_company_detail() {
    $CI = & get_instance();
    $sql = "SELECT * FROM company_information WHERE status='1' ";
    return $CI->db->query($sql)->row_array();
}

function getSupplierNameById($id) {
    $CI = & get_instance();
    $sql = "SELECT supplier_name FROM supplier_information WHERE supplier_id='$id'";
    return $CI->db->query($sql)->row()->supplier_name;
}

function getProductNameById($id) {
    $CI = & get_instance();
    $sql = "SELECT CONCAT(pi.product_name, ' - (',pc.category_name,')') AS product_name 
            FROM product_information pi
                INNER JOIN product_category pc ON pc.category_id = pi.category_id 
            WHERE pi.id='$id'";
    return $CI->db->query($sql)->row()->product_name;
}

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . '' : '') . $paise;
}

function getPaymentModeNameById($id) {
    $CI = & get_instance();
    $sql = "SELECT pm.* FROM payment_mode_list pm
            WHERE pm.payment_mode_id = $id AND pm.status = 1";
    return $CI->db->query($sql)->row()->payment_mode_name;
}
