<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sale_return_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insertSaleReturn($invoice_data) {
        $this->db->trans_start();
        $this->db->insert('sale_return', $invoice_data);
        if ($this->db->affected_rows() > 0) {
            $refSRID = $this->db->insert_id();
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        }
        return $refSRID;
    }

    public function get_tax_list() {
        $CI = & get_instance();
        $sql = "SELECT * FROM tax_information";
        return $CI->db->query($sql)->result();
    }

    public function get_product_list() {
        $sql = "SELECT sp.supplier_price,pi.*,
                       (SELECT GROUP_CONCAT(ti.tax_name SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax_name,
                       (SELECT GROUP_CONCAT(ti.tax SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax,
                       (SELECT GROUP_CONCAT(ti.tax_id SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax_id 
                FROM product_information pi
                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
                WHERE pi.status = '1' 
                GROUP BY pi.product_id 
                HAVING pi.product_id IS NOT NULL
                ORDER BY pi.product_id DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_company($limit) {
        $this->db->select('*');
        $this->db->from('company_information');
        $this->db->limit($limit);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function purchase_invoice_list() {
        $sql = "SELECT si.*,sii.* 
                FROM sale_invoice si 
                LEFT JOIN sale_invoice_item sii ON sii.refSIID = si.SIID
                WHERE si.status = '1' 
                GROUP BY si.SIID 
                HAVING si.SIID IS NOT NULL
                ORDER BY si.SIID DESC";
        return $this->db->query($sql)->result();
    }

    public function all_purchase_invoice_list() {
        $sql = "SELECT si.* 
                FROM sale_invoice si 
                WHERE si.status = '1' ";
        return $this->db->query($sql)->result();
    }

    public function product_edit_data($SIID) {
        $sql = "SELECT * FROM sale_invoice WHERE SIID = $SIID AND status = '1' ";
        return $this->db->query($sql)->row_array();
    }

    public function saleInvoiceDataById($SIID) {
        $sql = "SELECT sii.*,GROUP_CONCAT(siit.refTaxID  SEPARATOR ', ') AS Tax_id,GROUP_CONCAT(siit.refTaxName  SEPARATOR ', ') AS tax_name,GROUP_CONCAT(siit.TaxPer  SEPARATOR ', ') AS tax_per,SUM(siit.TaxAmt) AS TaxAmt,GROUP_CONCAT(DISTINCT so.SONO  SEPARATOR ', ') AS SONO,GROUP_CONCAT(DISTINCT so.SOID  SEPARATOR ', ') AS SOID 
                FROM sale_invoice_item sii 
                LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = sii.SIItemID AND siit.status = '1'
                LEFT JOIN sale_outward so ON so.SOID = sii.refSIID 
                WHERE sii.status = '1' AND sii.refSIID = '$SIID' 
                GROUP BY sii.SIItemID 
                HAVING sii.SIItemID IS NOT NULL ";
        return $this->db->query($sql)->result();
    }

    public function update_sale_invoice($SIID, $data) {
        $this->db->where('SIID', $SIID);
        $this->db->update('sale_invoice', $data);
        return TRUE;
    }

    public function deleteSaleReturnItem($SIID, $table_name) {
        $this->db->where('refSRID', $SIID);
        $this->db->update($table_name, array('status' => '0'));
        return TRUE;
    }

    public function deleteSaleReturn($sale_return_id) {
        $this->db->select('SRID');
        $this->db->from('sale_return');
        $this->db->where('SRID', $sale_return_id);
        $this->db->where('status', '1');
        $query = $this->db->get();
        $affected_row = $this->db->affected_rows();

        if ($affected_row > 0) {
            $this->db->where('SRID', $sale_return_id);
            $this->db->update('sale_return', array('status' => '0'));
            $this->db->where('refSRID', $sale_return_id);
            $this->db->update('sale_return_item', array('status' => '0'));
            $this->db->where('refSRID', $sale_return_id);
            $this->db->update('sale_return_itemtaxdet', array('status' => '0'));
            $this->db->where('invoice_no', $sale_return_id);
            $this->db->where('ref_table_name', 'sale_return');
            $this->db->update('customer_ledger', array('is_deleted' => 'Deleted'));
            $this->session->set_userdata(array('message' => 'Sale return deleted succesfully.'));

            $this->db->select('*');
            $this->db->from('sale_return');
            $this->db->where('status', '1');
            $query = $this->db->get();
            return true;
        } else {
            $this->session->set_userdata(array('error_message' => 'Data not found!'));
            return false;
        }
    }

    public function get_company_detail() {
        $sql = "SELECT * FROM company_information WHERE status='1' ";
        return $this->db->query($sql)->row_array();
    }

    public function get_tax_detail() {
        $sql = "SELECT * FROM tax_information WHERE status='1' ";
        return $this->db->query($sql)->result();
    }

    public function purchase_invoice_list_for_print($SIID) {
        $sql = "SELECT sii.*,GROUP_CONCAT(DISTINCT siit.refTaxID SEPARATOR ', ') AS Tax_id,GROUP_CONCAT(DISTINCT siit.refTaxName SEPARATOR ', ') AS tax_name,GROUP_CONCAT(DISTINCT siit.TaxPer SEPARATOR ', ') AS tax_per,SUM(DISTINCT siit.TaxAmt) AS TaxAmt,so.SONO,ps.PSNO 
                FROM sale_invoice_item sii 
                LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = sii.SIItemID 
                LEFT JOIN sale_outward so ON so.SOID = sii.refSOID 
                LEFT JOIN sale_outward_item soi ON soi.refSOID = so.SOID 
                LEFT JOIN product_sale ps ON ps.PSID = soi.refPSID
                WHERE sii.status = '1' AND sii.refSIID = '$SIID' 
                GROUP BY sii.SIItemID 
                HAVING sii.SIItemID IS NOT NULL";
        return $this->db->query($sql)->result();
    }

    public function invoice_tax_detail_for_print($SIID) {
        $sql = "SELECT CONCAT(siit.refTaxName,' ',siit.TaxPer,'%') as taxname, SUM(siit.TaxAmt) AS totaltax 
                FROM sale_invoice_itemtaxdet siit
                WHERE siit.refSIID = '$SIID' AND siit.status = '1' 
                GROUP BY siit.refTaxID 
                ORDER BY siit.refTaxName";
        return $this->db->query($sql)->result();
    }

    public function get_product_list_supllier($supplier_id) {
        $sql = "SELECT si.supplier_id,sp.supplier_price,pi.*,tp.tax_id,GROUP_CONCAT(ti.tax_name SEPARATOR ', ') AS tax_name,GROUP_CONCAT(ti.tax SEPARATOR ', ') AS tax,GROUP_CONCAT(ti.tax_id SEPARATOR ', ') AS tax_id  
                FROM product_information pi
                LEFT JOIN tax_product tp ON tp.product_id = pi.product_id
                LEFT JOIN tax_information ti ON ti.tax_id = tp.tax_id 
                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
                LEFT JOIN supplier_information si ON si.supplier_id = '$supplier_id' 
                WHERE sp.supplier_id = si.supplier_id AND pi.status = '1'
                GROUP BY pi.product_id 
                HAVING pi.product_id IS NOT NULL
                ORDER BY pi.product_id DESC";
//        return $this->db->query($sql)->result();
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getSaleOutwardProductVise($product_id, $customer_id) {
        $sql = "SELECT soi.*,so.* 
                FROM sale_outward_item soi 
                LEFT JOIN sale_outward so ON so.SOID = soi.refSOID
                WHERE soi.status = '1' AND soi.refProductID = '$product_id' AND so.RefCustomerID = '$customer_id' ";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    public function get_product_list_with_so() {
        $sql = "SELECT sp.supplier_price,pi.*,tp.tax_id,GROUP_CONCAT(DISTINCT ti.tax_name SEPARATOR ', ') AS tax_name,GROUP_CONCAT(DISTINCT ti.tax SEPARATOR ', ') AS tax,GROUP_CONCAT(DISTINCT ti.tax_id SEPARATOR ', ') AS tax_id,GROUP_CONCAT(DISTINCT soi.refSOID SEPARATOR ', ') AS SOID,GROUP_CONCAT(DISTINCT so.SONO SEPARATOR ', ') AS SONO
                FROM product_information pi
                LEFT JOIN tax_product tp ON tp.product_id = pi.product_id
                LEFT JOIN tax_information ti ON ti.tax_id = tp.tax_id 
                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
                LEFT JOIN sale_outward_item soi ON soi.refProductID = pi.product_id
                LEFT JOIN sale_outward so ON so.SOID = soi.refSOID
                WHERE pi.status = '1'
                GROUP BY pi.product_id 
                HAVING pi.product_id IS NOT NULL
                ORDER BY pi.product_id DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function getSaleReturnList() {
        $sql = "SELECT sr.* FROM sale_return sr WHERE sr.status = '1' ";
        return $this->db->query($sql)->result();
    }

    public function getSaleReturnDataByid($SRID) {
        $sql = "SELECT * FROM sale_return WHERE SRID = $SRID AND status = '1' ";
        return $this->db->query($sql)->row_array();
    }

    public function getSaleReturnItemById($SRID) {
//        $sql = "SELECT * 
//                FROM sale_return_item sri 
//                LEFT JOIN sale_return_itemtaxdet sriit ON sriit.refSRItemID = sri.SRItemID
//                WHERE sri.refSRID = '1' AND sri.status = '1' 
//                HAVING sri.SRItemID IS NOT NULL";

        $sql = "SELECT sri.*,GROUP_CONCAT(DISTINCT srit.refTaxID  SEPARATOR ',') AS Tax_id,GROUP_CONCAT(DISTINCT srit.refTaxName  SEPARATOR ',') AS tax_name,GROUP_CONCAT(DISTINCT srit.TaxPer  SEPARATOR ',') AS tax_per,SUM(srit.TaxAmt) AS TaxAmt
                FROM sale_return_item sri
                LEFT JOIN sale_return_itemtaxdet srit ON srit.refSRItemID = sri.SRItemID 
                WHERE sri.refSRID = '$SRID' AND srit.status = '1'
                GROUP BY sri.SRItemID";
        return $this->db->query($sql)->result();
    }

    public function saleInvoiceDataWithReturn($SRID) {
//        $sql = "SELECT sr.SRID, si.refProductID, si.UnitAmt, si.Qty, sri.Qty as returnQty, sri.Discount, sri.Remark,GROUP_CONCAT(DISTINCT srit.refTaxID  SEPARATOR ',') AS Tax_id,GROUP_CONCAT(DISTINCT srit.refTaxName  SEPARATOR ',') AS tax_name,GROUP_CONCAT(DISTINCT srit.TaxPer  SEPARATOR ',') AS tax_per,SUM(srit.TaxAmt) AS TaxAmt
//FROM sale_return sr
//INNER JOIN sale_invoice_item si ON si.refSIID = sr.refSIID
//LEFT JOIN sale_return_item sri ON sri.refSRID = sr.SRID AND sri.refProductID = si.refProductID
//LEFT JOIN sale_return_itemtaxdet srit ON srit.refSRItemID = sri.SRItemID 
//WHERE sr.SRID = '$SRID' AND sr.status = '1' AND si.status = '1'
//GROUP BY si.refProductID";
//
//        $sql = "SELECT sr.SRID, si.refProductID, si.UnitAmt, si.Qty, sri.Qty as returnQty, sri.Discount, sri.Remark,GROUP_CONCAT(DISTINCT siit.refTaxID  SEPARATOR ',') AS Tax_id,GROUP_CONCAT(DISTINCT siit.refTaxName  SEPARATOR ',') AS tax_name,GROUP_CONCAT(DISTINCT siit.TaxPer  SEPARATOR ',') AS tax_per,SUM(DISTINCT srit.TaxAmt) AS TaxAmt
//                FROM sale_return sr
//                INNER JOIN sale_invoice_item si ON si.refSIID = sr.refSIID
//                LEFT JOIN sale_return_item sri ON sri.refSRID = sr.SRID AND sri.refProductID = si.refProductID AND sri.status = '1'
//                LEFT JOIN sale_return_itemtaxdet srit ON srit.refSRItemID = sri.SRItemID 
//                LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = si.SIItemID
//                WHERE sr.SRID = '$SRID' AND sr.status = '1'
//                GROUP BY si.refProductID";
//        $sql = " SELECT sr.SRID, si.refProductID, si.UnitAmt, si.Qty,sri.SRItemID, sri.Qty as returnQty, sri.Discount, sri.Remark, GROUP_CONCAT(siit.refTaxID SEPARATOR ',') AS Tax_id, GROUP_CONCAT(siit.refTaxName SEPARATOR ',') AS tax_name, GROUP_CONCAT(siit.TaxPer SEPARATOR ',') AS tax_per
//                FROM sale_return sr
//                INNER JOIN sale_invoice_item si ON si.refSIID = sr.refSIID AND si.status = '1'
//                LEFT JOIN sale_return_item sri ON sri.refSRID = sr.SRID AND sri.refProductID = si.refProductID AND sri.status = '1'
//                LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = si.SIItemID AND siit.status = '1'
//                WHERE sr.SRID = '$SRID' AND sr.status = '1'
//                GROUP BY si.refProductID";

        $sql = "SELECT sr.SRID,soi.refProductID,soi.UnitAmt,soi.Qty,sri.SRItemID,sri.Qty AS returnQty,sri.Discount,sri.Remark,GROUP_CONCAT(soit.refTaxID SEPARATOR ',') AS Tax_id, GROUP_CONCAT(soit.refTaxName SEPARATOR ',') AS tax_name, GROUP_CONCAT(soit.TaxPer SEPARATOR ',') AS tax_per
                FROM sale_return sr
                INNER JOIN sale_outward_item soi ON soi.refSOID = sr.refSOID AND soi.status = '1'
                LEFT JOIN sale_return_item sri ON sri.refSRID = sr.SRID AND sri.refProductID = soi.refProductID AND sri.status = '1'
                LEFT JOIN sale_outward_itemtaxdet soit ON soit.refSOItemID = soi.SOItemID AND soit.status = '1'
                WHERE sr.SRID = '$SRID' AND sr.status = '1'
                GROUP BY soi.refProductID";

        return $this->db->query($sql)->result();
    }

    public function saleReturnDataByID($SRID) {
        $sql = "SELECT sr.*, si.SINO, ci.ref_state_id, s.code_int
                FROM sale_return sr
                    INNER JOIN sale_invoice si ON si.SIID = sr.refSIID AND si.status = '1'
                    INNER JOIN customer_information ci ON ci.customer_id = sr.RefCustomerID AND ci.is_deleted = '1'
                    LEFT JOIN states s ON s.id = ci.ref_state_id
                WHERE sr.SRID = '$SRID'";
        return $this->db->query($sql)->row_array();
    }

    public function saleReturnItemDataById($SRID) {
        $sql = "SELECT sri.*,srit.refTaxID AS Tax_id,srit.refTaxName AS tax_name,srit.TaxPer AS tax_per,SUM(srit.TaxAmt) AS TaxAmt ,pi.product_name
                FROM sale_return_item sri
                    INNER JOIN sale_return_itemtaxdet srit ON srit.refSRItemID = sri.SRItemID AND srit.status = '1'
                    INNER JOIN sale_return sr ON sr.SRID = sri.refSRID AND sr.status = '1'
                    LEFT JOIN sale_invoice si ON si.SIID = sr.refSIID AND si.status = '1'
                    LEFT JOIN product_information pi ON pi.id = sri.refProductID AND pi.status = 1
                    LEFT JOIN sale_invoice_item sii ON sii.refSIID = si.SIID AND sii.status = '1'
                WHERE sri.refSRID = '$SRID' AND sri.status = '1'
                GROUP BY sri.SRItemID
                HAVING sri.SRItemID IS NOT NULL";
        return $this->db->query($sql)->result();
    }

    public function saleReturnTaxDetail($SRID) {
        $sql = "SELECT CONCAT(srit.refTaxName,' ',TaxPer,'%') as taxname, SUM(srit.TaxAmt) AS totaltax
                FROM sale_return_itemtaxdet srit
                WHERE srit.refSRID = '$SRID' AND srit.status = '1'
                GROUP BY srit.refTaxID
                ORDER BY srit.refTaxName";
        return $this->db->query($sql)->result();
    }

    public function getOutwardDetail($SOID) {
        $sql = "SELECT so.SOID, COUNT(so.SOID) AS outward_cnt, COUNT(sr.SRID) AS return_cnt
                FROM sale_outward so 
                LEFT JOIN sale_return sr ON sr.refSOID = so.SOID AND sr.status = '1'
                WHERE so.SOID = '$SOID' AND so.status = '1'";
        return $this->db->query($sql)->row();
    }

    function get_sale_return_list($postData) {
        $sql = $this->get_sale_return_list_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_sale_return_list_search_data($postData) {
        $sql = $this->get_sale_return_list_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_sale_return_list_query($postData) {
        $column_order = array('', 'a.SRNO', 'a.DateTime', 'a.CustomerName', 'a.ProductCount', 'a.GrandTotal', '');
        $sql = "SELECT a.*, DATE_FORMAT(a.DateTime,'%d-%m-%Y %H:%i') AS DateTime FROM (
                    SELECT sr.DateTime, sr.SRID, sr.SRNO, ci.customer_name AS CustomerName, (SELECT COUNT(SRItemID) FROM sale_return_item WHERE refSRID = sr.SRID AND status='1') AS ProductCount, sr.GrandTotal, ui.user_name as created_by, u.user_name as updated_by
                    FROM sale_return sr
                        INNER JOIN customer_information ci ON ci.customer_id = sr.RefCustomerID AND ci.is_deleted = '1'
                        LEFT JOIN user_information ui ON ui.id = sr.InsUser 
                        LEFT JOIN user_information u ON u.id = sr.UpdUser
                    WHERE sr.status = '1'
                ) a WHERE 1=1";

        if (isset($postData['start_date']) && !empty($postData['start_date'])) {
            $start_date = date_format(date_create($postData['start_date']), 'Y-m-d');
            $sql .= " AND a.DateTime >= '" . $start_date . "'";
        }

        if (isset($postData['end_date']) && !empty($postData['end_date'])) {
            $end_date = date_format(date_create($postData['end_date']), 'Y-m-d');
            $sql .= " AND a.DateTime <= '" . $end_date . "'";
        }

        $column_cnt = count($column_order);

        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.SRID";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.SRID DESC';
        }
        return $sql;
    }

    public function getSaleInvoiceData($invoice_no = null, $SIID = null) {
        $sql = "SELECT si.*, ci.customer_name
                FROM sale_invoice si
                LEFT JOIN customer_information ci ON ci.customer_id = si.RefCustomerID AND ci.is_deleted = 1
                WHERE si.status = 1";
        if (isset($invoice_no) && !empty($invoice_no)) {
            $sql .= " AND si.SINO = '$invoice_no'";
        }
        if (isset($SIID) && !empty($SIID)) {
            $sql .= " AND si.SIID = $SIID";
        }
        return $this->db->query($sql)->row();
    }

    public function getSaleInvoiceProductDetails($invoice_id) {
        $sql = "SELECT sii.*,GROUP_CONCAT( siit.refTaxID SEPARATOR ',') AS Tax_id, GROUP_CONCAT( siit.refTaxName SEPARATOR ',') AS tax_name, GROUP_CONCAT( siit.TaxPer SEPARATOR ',') AS tax_per, SUM(siit.TaxAmt) AS TaxAmt, GROUP_CONCAT(DISTINCT p.id  SEPARATOR ',') AS product_id, GROUP_CONCAT(DISTINCT p.serial_no  SEPARATOR ',') AS barcode, CONCAT(p.product_name,' - (',pc.category_name,')') AS product_name_with_category, IFNULL(SUM(sri.Qty), 0) AS total_returned_qty, (sii.Qty - IFNULL(SUM(sri.Qty), 0)) AS returnable_qty
                FROM sale_invoice_item sii
                LEFT JOIN sale_invoice si ON si.SIID = sii.refSIID AND si.status = '1'
                LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = sii.SIItemID AND siit.status = '1'
                LEFT JOIN product_information p ON p.id = sii.refProductID AND p.status = '1'
                LEFT JOIN product_category pc ON pc.category_id = p.category_id AND pc.status = '1'
                LEFT JOIN sale_return sr ON sr.refSIID = si.SIID AND sr.status = '1'
                LEFT JOIN sale_return_item sri ON sri.refSRID = sr.SRID AND sri.status = '1' AND sri.refProductID = sii.refProductID
                WHERE sii.status = '1' AND sii.refSIID = $invoice_id
                GROUP BY sii.SIItemID";
        return $this->db->query($sql)->result();
    }

    public function generateSaleReturnNo() {
        $sql = "SELECT sr.SRNO FROM sale_return sr ORDER BY sr.SRNO DESC LIMIT 1";
        $cnt = $this->db->query($sql)->row();
        if (!empty($cnt)) {
            return $cnt->SRNO;
        }
    }

    public function getSaleReturnDataByReturnId($SRID) {
        $sql = "SELECT sr.* FROM sale_return sr WHERE sr.SRID = $SRID AND sr.status = '1'";
        $result = $this->db->query($sql)->row_array();
        if (isset($result) && !empty($result)) {
            $result['sale_return_item_data'] = $this->getSaleReturnItemDataByRetrunId($SRID, $result['refSIID']);
            $result['sale_return_item_tax_data'] = $this->getSaleReturnItemTaxDataByRetrunId($SRID);
//            preprint($result);
            return $result;
        } else {
            return $result;
        }
    }

    public function getSaleReturnItemDataByRetrunId($SRID, $SIID) {
        $sql = "SELECT *, IFNULL((IFNULL(a.sale_invoice_qty,0) - IFNULL(a.total_returned_qty,0)),0) AS returnable_qty FROM (SELECT sri.*, pi.product_name, sii.Qty as sale_invoice_qty,
                    IFNULL((SELECT IFNULL(SUM(sri1.Qty),0) AS returned_qty
                            FROM sale_return sr
                                LEFT JOIN sale_return_item sri1 ON sri.refSRID = sr.SRID AND sri1.status = 1
                            WHERE sr.refSIID = $SIID AND sr.status = 1 AND sri1.refSRID != $SRID AND sri1.refProductID = sri.refProductID
                            GROUP BY sri1.refProductID
                            ORDER BY sri1.refProductID ASC),0) AS total_returned_qty
                    FROM sale_return_item sri 
                    	INNER JOIN product_information pi ON pi.id = sri.refProductID AND pi.status = 1
                    	INNER JOIN sale_return sr ON sr.SRID = sri.refSRID AND sr.status = 1
                    	INNER JOIN sale_invoice si ON si.SIID = sr.refSIID AND si.status
                    	INNER JOIN sale_invoice_item sii ON sii.refSIID = si.SIID AND sii.status = 1 AND sii.refProductID = sri.refProductID
                    WHERE sri.refSRID = $SRID AND sri.status = 1 
                    GROUP BY sri.SRItemID
                    ORDER BY sri.SRItemID ASC) a";
//        preprint($sql);
        return $this->db->query($sql)->result();
    }

    public function getSaleReturnItemTaxDataByRetrunId($SRID) {
        $sql = "SELECT srit.* 
                FROM sale_return_itemtaxdet srit 
                WHERE srit.refSRID = $SRID AND srit.status = 1 
                ORDER BY srit.SRItemTaxID ASC";
        return $this->db->query($sql)->result();
    }
}
