<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function add_payment($data = array()) {
        return $this->db->insert('transaction', $data);
    }

    public function payment_list($per_page, $page) {
        $this->db->select('
   i.*, i.amount as debit,
    i.pay_amount as credit,i.transaction_id,i.relation_id,f.customer_name,h.supplier_name,m.parent_id');
        $this->db->from('transaction i');
        $this->db->join('customer_information f', 'i.relation_id=f.customer_id', 'left');
        $this->db->join('supplier_information h', 'i.relation_id=h.supplier_id', 'left');
        $this->db->join('account m', 'i.transaction_category=m.parent_id');
        $this->db->group_by('i.transaction_id');
        $this->db->limit($per_page, $page);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function delete_payment($id) {
        $this->db->where('transaction_id', $id);
        $this->db->delete('transaction');
        return true;
    }

    // customer ledger delete
    public function delete_customer_ledger($id) {
        $this->db->where('transaction_id', $id);
        $this->db->delete('customer_ledger');
        return true;
    }

    // supplier ledger delete

    public function delete_supplier_ledger($id) {
        $this->db->where('transaction_id', $id);
        $this->db->delete('supplier_ledger');
        return true;
    }

    // loan or person ledger delete

    public function delete_person_ledger($id) {
        $this->db->where('transaction_id', $id);
        $this->db->delete('person_ledger');
        return true;
    }

//invoice inflow delege
    public function delete_inflow($id) {
        $this->db->where('transaction_id', $id);
        $this->db->delete('inflow_92mizdldrv');
        return true;
    }

    public function update_payment($data, $trans) {
        $this->db->where('transaction_id', $trans);
        $this->db->update('transaction', $data);
        return true;
    }

    public function payment_updata($transaction_id) {

        $sql = "SELECT `a`.*, `b`.`supplier_name`, `b`.`supplier_id`,  `c`.`customer_name`,`c`.`customer_id`, 
                `d`.`account_name`,`d`.`account_id`,               cm.bank_id, cm.chq_given_date, cm.cheque_no
                FROM `transaction` `a` 
                LEFT JOIN `supplier_information` `b` ON `b`.`supplier_id` = `a`.`relation_id` 
                LEFT JOIN `customer_information` `c` ON `c`.`customer_id` = `a`.`relation_id` 
                LEFT JOIN `account` `d` ON `d`.`account_id` = `a`.`relation_id` 
                LEFT JOIN `cheque_manger` `cm` ON cm.transaction_id = a.transaction_id
                WHERE `a`.`transaction_id` = '$transaction_id'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    //customer_inflow_updata
    public function customer_inflow_updata($data, $trans) {
        $this->db->where('transaction_id', $trans);
        $this->db->update('inflow_92mizdldrv', $data);
        return true;
    }

    // update customer ledger from transaciton update form
    public function customer_leder_updata($data, $trans) {
//        echo 'V<pre>';        print_r($data);die();
        $this->db->where('transaction_id', $trans);
        $this->db->update('customer_ledger', $data);
        return true;
    }

    public function supplier_leder_updata($data, $trans) {


        $this->db->where('transaction_id', $trans);
        $this->db->update('supplier_ledger', $data);
        return true;
    }

    public function person_leder_updata($trans) {
        $this->db->select('*');
        $this->db->from('person_ledger');
        $this->db->where('transaction_id', $trans);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function selected_transaction($category_id) {
        $this->db->select('*');
        $this->db->from('account');
        $this->db->where('account_id', $category_id);
        $this->db->limit('500');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //customer select in transaction update
    public function selected_customer($trans) {

        $this->db->select('f.*,j.customer_name');
        $this->db->from('customer_ledger f');
        $this->db->join('customer_information j', 'f.customer_id=j.customer_id', 'left');
        $this->db->where('f.transaction_id', $trans);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

//loan selecete in transaction update
    public function selected_loan($trans) {
        $this->db->select('f.*,j.person_name');
        $this->db->from('person_ledger f');
        $this->db->join('person_information j', 'f.person_id=j.person_id', 'left');
        $this->db->where('f.transaction_id', $trans);
        $query = $this->db->get();
//        preprint($this->db->last_query());
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    //Supplier update selected info
    public function selected_supplier($trans) {

        $this->db->select('f.*,j.supplier_name');
        $this->db->from('supplier_ledger f');
        $this->db->join('supplier_information j', 'f.supplier_id=j.supplier_id', 'left');
        $this->db->where('f.transaction_id', $trans);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    //office accunt update sele
    public function selected_office_trns($trans) {

        $this->db->select('a.*,b.*');
        $this->db->from('transaction a');
        $this->db->join('account  b', 'a.relation_id=b.account_name');
        $this->db->where('a.transaction_id', $trans);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    //transaction payment type seleceted
    public function selected_transaction_type($trans) {

        $this->db->select('*');
        $this->db->from('transaction');
        $this->db->where('transaction_id', $trans);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    //

    public function transaction_report() {
        $query = $this->db->select('transaction.*,
      count(transaction.transaction_id) as Total_trans,
      sum(transaction.amount) as amount
      ')
                ->from('transaction')
                ->group_by("transaction.relation_id")
                ->limit('500')
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    // admin dashboard data




    public function table_name($type) {
        $this->db->select(' * ');
        $this->db->from('account');
        $this->db->where('parent_id', $type);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function payment_summary() {
        $todays_date = date('Y-m-d');
        $this->db->select('transaction.transaction_category,account.*');
        $this->db->from('transaction');
        $this->db->join('account', 'transaction.transaction_category=account.parent_id');
        $this->db->where('transaction.transaction_type', 1);
        $this->db->where('transaction.date_of_transaction', $todays_date);
        $this->db->group_by('transaction.transaction_category');
        $query = $this->db->get();
        $accounts = array();
        $serial = 1;
        $total = 0;
        foreach ($query->result_array() as $tran_cat) {

            $table = 'transaction';
            $todays_date = date('Y-m-d');
            $this->db->select("count(amount) as no_trans,sum(amount) as 'total',transaction_category");
            $this->db->from($table);
            $this->db->where('date_of_transaction', $todays_date);
            $this->db->where('transaction_type', 1);
            $this->db->where('transaction_category', $tran_cat['transaction_category']);
            // $this->db->group_by('transaction_category');
            $result_account = $this->db->get();
            $account = $result_account->result_array();
            //$tran_cat=$account[0]['transaction_category'];
            $no_trans = $account[0]['no_trans'];
            $total += $account[0]['total'];
            // foreach ($tran_cat as $tran_cat) {
            // }
            if ($no_trans > 0) {
                $gmdate = date('Y-m-d');

                $link = base_url() . "Payment/summary_single/" . $gmdate . "/" . $todays_date . "/" . $table;

                $account_name = "<a href='" . $link . "' >" . $tran_cat["account_name"] . "</a>";

                $accounts[] = array("sl" => $serial, "table" => $account_name, "no_transaction" => $no_trans, "sub_total" => $account[0]['total']);
                $serial++;
            }
        }
        $this->sub_total = $total;

        return $accounts;
    }

    public function receipt_summary_datewise($start, $end) {
        $this->db->select('transaction.transaction_category,account.*');
        $this->db->from('transaction');
        $this->db->join('account', 'transaction.transaction_category=account.parent_id');
        $this->db->where('transaction.transaction_type', 2);
        $this->db->group_by('transaction.transaction_category');
        $query = $this->db->get();
        $accounts = array();
        $serial = 1;
        $total = 0;
        foreach ($query->result_array() as $tran_cat) {

            $table = 'transaction';
            $todays_date = date('Y-m-d');
            $this->db->select("count(amount) as no_trans,sum(amount) as 'total',transaction_category");
            $this->db->from($table);
            $this->db->where(array('date_of_transaction >=' => $start, 'date_of_transaction <=' => $end));
            $this->db->where('transaction_type', 2);
            $this->db->where('transaction_category', $tran_cat['transaction_category']);
            // $this->db->group_by('transaction_category');
            $result_account = $this->db->get();
            $account = $result_account->result_array();
            //$tran_cat=$account[0]['transaction_category'];
            $no_trans = $account[0]['no_trans'];
            $total += $account[0]['total'];
            // foreach ($tran_cat as $tran_cat) {
            // }
            if ($no_trans > 0) {
                $gmdate = date('Y-m-d');

                $link = base_url() . "Payment/summary_single/" . $gmdate . "/" . $todays_date . "/" . $table;

                $account_name = "<a href='" . $link . "' >" . $tran_cat["account_name"] . "</a>";

                $accounts[] = array("sl" => $serial, "table" => $account_name, "no_transaction" => $no_trans, "sub_total" => $account[0]['total']);
                $serial++;
            }
        }
        $this->sub_total = $total;

        return $accounts;
    }

// transaction details
    public function trans_summary_details($start, $end) {
        $this->db->select(" * ");
        $this->db->from('payment_trans');
        $this->db->where(array('date >=' => $start, 'date <=' => $end));
        // $this->db->where("status",1);
        $result_account = $this->db->get();
        $account = $result_account->result_array();
        $serial = 1;
        $data = array();
        foreach ($account as $account) {
            $date = substr($account['date'], 0, 10);

            if ($account['payment_type'] == 1) {
                $payment_type = "Cash";
            } elseif ($account['payment_type'] == 2) {
                $payment_type = "Cheque";
            } else {
                $payment_type = "Pay Order";
            }

            $data[] = array("sl" => $serial++, "table" => $table, "date" => $date, "transaction_id" => $account['transaction_id'], "tracing_id" => $this->idtracker($account['tracing_id']), "description" => $account['description'], "amount" => $account['amount'], "payment_type" => $payment_type);
        }

        return $data;
    }

    // transaction Daily details report 
    public function tran_rep_query() {
        $datse = date('Y-m-d');

        $this->db->select('transaction.*, sum(transaction.receipt_amount) as debit,
                           sum(transaction.pay_amount) as credit,count(transaction.transaction_id) as tran_no');
        $this->db->from('transaction');
        $this->db->where('transaction.date_of_transaction', $datse);
        $this->db->group_by('transaction.transaction_category');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

// report popup query
    public function get_details($transaction_category) {

        $date = date('Y-m-d');
        $this->db->select('i.*, i.receipt_amount as debit,i.pay_amount as credit,i.transaction_id,i.relation_id,f.customer_name,f.customer_id,h.supplier_name,h.supplier_id');
        $this->db->from('transaction i');
        $this->db->join('customer_information f', 'i.relation_id=f.customer_id', 'left');
        //$this->db->join('account m','i.transaction_category=m.parent_id','left');
        $this->db->join('supplier_information h', 'i.relation_id=h.supplier_id', 'left');
        $this->db->where("i.date_of_transaction", $date);
        $this->db->where("i.transaction_category", $transaction_category);

        $query = $this->db->get();

        return $query->result_array();
    }

// todays report pop up query
    public function today_details($transaction_category) {

        $date = date('Y-m-d');
        $this->db->select('*');
        $this->db->from('transaction');
        $this->db->where("date_of_transaction", $date);
        $this->db->where("transaction_category", $transaction_category);
        $query = $this->db->get();

        return $query->result_array();
    }

    //invoic number search 
    public function invoice($transaction_id) {

        $this->db->select('*');
        $this->db->from('customer_v_transaction');

        $this->db->where('transaction_id', $transaction_id);
        //$this->db->where("customer_ledger.customer_id",$customer_id);
        $query = $this->db->get();

        return $query->result_array();
    }

    // date to date query 
    public function date_summary_query($per_page = '', $page = '') {

        $today = date('Y-m-d');

        $this->db->select('a.*, b.supplier_name, c.customer_name, d.account_name');
        $this->db->from('transaction a');
        $this->db->join('supplier_information b', 'b.supplier_id = a.relation_id', 'left');
        $this->db->join('customer_information c', 'c.customer_id = a.relation_id', 'left');
        $this->db->join('account d', 'd.account_id = a.relation_id', 'left');
        $this->db->group_by('a.transaction_id');
        $this->db->order_by('a.transaction_id', 'desc');
        if ($per_page != '' && $page != '') {
            $this->db->limit($per_page, $page);
        }
        $query = $this->db->get();

//        echo $this->db->last_query($query);die;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function date_summary_query11($per_page = '', $page = '') {

        $today = date('Y-m-d');
        $this->db->select('c.supplier_name, c.supplier_id, e.customer_name, e.customer_id, a.*');
        $this->db->from('transaction a');
        $this->db->join('supplier_ledger b', 'b.transaction_id = a.transaction_id', 'left');
        $this->db->join('supplier_information c', 'c.supplier_id = b.supplier_id', 'left');
        $this->db->join('customer_ledger d', 'd.transaction_id = a.transaction_id', 'left');
        $this->db->join('customer_information e', 'e.customer_id = d.customer_id', 'left');
        $this->db->where('a.date_of_transaction', $today);
        $this->db->group_by('a.transaction_id');
        $this->db->order_by('a.date_of_transaction', 'desc');
        $query = $this->db->get();

        return $query->result_array();
    }

// search query date between search by date
    public function search_query($start, $end) {

        $date_range = "a.date_of_transaction BETWEEN '$start' AND '$end'";
        $this->db->select('c.supplier_name, c.supplier_id, e.customer_name, e.customer_id, a.*');
        $this->db->from('transaction a');
        $this->db->join('supplier_ledger b', 'b.transaction_id = a.transaction_id', 'left');
        $this->db->join('supplier_information c', 'c.supplier_id = b.supplier_id', 'left');
        $this->db->join('customer_ledger d', 'd.transaction_id = a.transaction_id', 'left');
        $this->db->join('customer_information e', 'e.customer_id = d.customer_id', 'left');
        $this->db->where($date_range);
        $this->db->group_by('a.transaction_id');
        $this->db->order_by('a.date_of_transaction', 'desc');
        $query = $this->db->get();

        return $query->result_array();
    }

    // Account2 list for transaction
    public function account_list() {
        $this->db->select('*');
        $this->db->from('account');
        $this->db->where('parent_id', 3);
        $this->db->where('status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    // office_person_list list for transaction
    public function office_person_list() {
//        $this->db->select('*');
//        $this->db->from('person_information');
//        $this->db->where('status', '1');
//
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
        return false;
    }

    //Transection category list
    public function account_list_category() {
        $this->db->select('*');
        $this->db->from('account');
        $this->db->where('status', '1');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Custom repor search query result
    public function custom_search_query($start, $end, $account) {
        $this->db->select('
      i.*, i.amount as debit,
      i.pay_amount as credit,i.transaction_id,i.relation_id,f.customer_name,h.supplier_name,m.parent_id, m.account_name');
        $this->db->from('transaction i');
        $this->db->join('customer_information f', 'i.relation_id=f.customer_id', 'left');
        $this->db->join('account m', 'i.relation_id=m.account_id', 'left');
        $this->db->join('supplier_information h', 'i.relation_id=h.supplier_id', 'left');
        $this->db->where(array('i.date_of_transaction >=' => $start, 'i.date_of_transaction <=' => $end));
        $this->db->where('i.transaction_category', $account);
        $this->db->group_by('i.relation_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    // transaction category list
    public function tran_cat_list() {
        $this->db->select('*');
        $this->db->from('account');
//        $this->db->group_by('parent_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    // dashboard admin panel data
    public function dashboard_info() {
        return $last = $this->db->order_by('transaction_id', "desc")->limit(1)->get('transaction')->row();
    }

    public function count_transaction() {
        return $this->db->count_all("transaction");
    }

    public function date_summary_date_to_date($star_date, $end_date, $per_page = '', $page = '') {
        $this->db->select('a.*, b.supplier_name, c.customer_name, d.account_name');
        $this->db->from('transaction a');
        $this->db->join('supplier_information b', 'b.supplier_id = a.relation_id', 'left');
        $this->db->join('customer_information c', 'c.customer_id = a.relation_id', 'left');
        $this->db->join('account d', 'd.account_id = a.relation_id', 'left');
        if (!empty($star_date) && $star_date != '' && !empty($end_date) && $end_date != '') {
            $this->db->where('DATE_FORMAT(a.date_of_transaction,"%Y-%m-%d") >=', $star_date);
            $this->db->where('DATE_FORMAT(a.date_of_transaction,"%Y-%m-%d") <=', $end_date);
        }
        $this->db->group_by('a.transaction_id');
        $this->db->order_by('a.transaction_id', 'desc');
//        $this->db->select('
//     i.*, i.amount as debit,
//      i.pay_amount as credit,q.*,r.*,s.*');
//        $this->db->from('transaction i');
//        $this->db->join('view_customer_transaction q', 'i.transaction_id=q.transaction_id', 'left');
//        $this->db->join('view_supplier_transaction r', 'i.transaction_id=r.transaction_id', 'left');
//        $this->db->join('view_person_transaction s', 'i.transaction_id=s.transaction_id', 'left');
//        $this->db->join('account a', 'i.transaction_category=a.parent_id');
//        $this->db->limit($per_page, $page);
//        $this->db->group_by('i.transaction_id');
        $query = $this->db->get();
//        echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //count date to date payment row
    public function count_date_summary_date_to_date($star_date, $end_date) {

        $this->db->select('i.*, i.amount as debit, i.pay_amount as credit,q.*,r.*,s.*,a.*');
        $this->db->from('transaction i');
        $this->db->join('view_customer_transaction q', 'i.transaction_id=q.transaction_id', 'left');
        $this->db->join('view_supplier_transaction r', 'i.transaction_id=r.transaction_id', 'left');
        $this->db->join('view_person_transaction s', 'i.transaction_id=s.transaction_id', 'left');
        $this->db->join('account a', 'i.transaction_category=a.parent_id');

        $this->db->where('i.date_of_transaction >=', $star_date);
        $this->db->where('i.date_of_transaction <=', $end_date);
        $this->db->group_by('i.transaction_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    public function supplier_product_sale_previous_debit($start) {
        $query = $this->db->select('sum(receipt_amount) as pre_debit,sum(pay_amount) as pre_credit')
                ->from('transaction')
                ->where("date_of_transaction <", $start)
                ->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function insert_transaction($data) {
        $this->db->insert('transaction', $data);
        return $this->db->insert_id();
    }

    public function delete_transaction($transaction_id) {
        $this->db->trans_start();

        $this->db->where('transaction_id', $transaction_id);
        $this->db->update('transaction', array('is_deleted' => '0'));

        $this->db->where('transaction_id', $transaction_id);
        $this->db->update('customer_ledger', array('is_deleted' => 'Deleted'));

        $this->db->where('transaction_id', $transaction_id);
        $this->db->update('supplier_ledger', array('is_deleted' => 'Deleted'));

        $this->db->where('transaction_id', $transaction_id);
        $this->db->update('cheque_manger', array('status' => '0'));

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    function get_payment_list($postData) {
        $sql = $this->get_payment_list_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_payment_list_search_data($postData) {
        $sql = $this->get_payment_list_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_payment_list_query($postData) {
        $column_order = array('', 'a.TransactionDate', 'a.Name', 'a.AccountType', 'a.description', 'a.TransactionMode', 'a.receipt_amount', 'a.pay_amount', '');
        $sql = "SELECT a.* FROM (
                        SELECT tr.transaction_id, tr.transaction_type, tr.is_transaction, DATE_FORMAT(tr.date_of_transaction, '%d-%m-%Y') AS TransactionDate, IF(tr.transaction_category = 1, si.supplier_name, IF(tr.transaction_category = 2, ci.company__name, '')) Name, IF(tr.transaction_category = 1, 'Supplier', IF(tr.transaction_category = 2, 'Customer', IF(tr.transaction_category = 3, 'Office', IF(tr.transaction_category = 5, 'Salary', 'Loan')))) AS AccountType, tr.description, pm.payment_mode_name AS TransactionMode, FORMAT(tr.receipt_amount, 2) AS receipt_amount, FORMAT(tr.pay_amount, 2) AS pay_amount
                        FROM transaction tr
                            LEFT JOIN supplier_information si ON si.supplier_id = tr.relation_id
                            LEFT JOIN customer_information ci ON ci.customer_id = tr.relation_id
                            LEFT JOIN account ac ON ac.account_id = tr.relation_id
                            LEFT JOIN payment_mode_list pm ON pm.payment_mode_id = tr.transaction_mode
                        WHERE tr.is_deleted = 1";

        if (isset($postData['start_date']) && !empty($postData['start_date'])) {
            $start_date = date_format(date_create($postData['start_date']), 'Y-m-d');
            $sql .= " AND DATE(tr.date_of_transaction) >= '" . $start_date . "'";
        }

        if (isset($postData['end_date']) && !empty($postData['end_date'])) {
            $end_date = date_format(date_create($postData['end_date']), 'Y-m-d');
            $sql .= " AND DATE(tr.date_of_transaction) <= '" . $end_date . "'";
        }

        $sql .= " GROUP BY tr.transaction_id
                        ORDER BY tr.transaction_id DESC
                ) a WHERE 1=1";

        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.transaction_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.transaction_id DESC';
        }
//        preprint($sql);
        return $sql;
    }

    public function getPaymentModeList() {
        $sql = "SELECT pm.* 
                FROM payment_mode_list pm
                WHERE pm.active_status = '1' AND pm.status = '1'";
        return $this->db->query($sql)->result();
    }

    public function getPaymentAmountBySupplier($supplier_id) {
        $sql = "SELECT (a.debit_amount - a.credit_amount) AS total_payable_amount 
                FROM(SELECT (SELECT IFNULL(SUM(sl.amount),0)
                                FROM supplier_ledger sl
                                WHERE sl.d_c = 'd' AND sl.status = '1' AND sl.supplier_id = '$supplier_id') debit_amount,
                            (SELECT IFNULL(SUM(sl.amount),0)
		 		FROM supplier_ledger sl
		 		WHERE sl.d_c = 'c' AND sl.status = '1' AND sl.supplier_id = '$supplier_id') credit_amount
                    FROM supplier_ledger sup
                    WHERE sup.is_deleted = 'Live' AND sup.supplier_id = '$supplier_id'
                    GROUP BY sup.supplier_id
                    ORDER BY sup.supplier_id ASC) a";
        $result = $this->db->query($sql)->row();
        if (!empty($result)) {
            return $result->total_payable_amount;
        } else {
            return $result;
        }
    }

    public function getPaymentAmountByCustomer($customer_id) {
        $sql = "SELECT (a.credit_amount - a.debit_amount) AS total_payable_amount 
                FROM(SELECT (SELECT IFNULL(SUM(cl.amount),0)
                                FROM customer_ledger cl
                                WHERE cl.d_c = 'd' AND cl.status = '1' AND cl.customer_id = '$customer_id') debit_amount,
                            (SELECT IFNULL(SUM(cl.amount),0)
		 		FROM customer_ledger cl
		 		WHERE cl.d_c = 'c' AND cl.status = '1' AND cl.customer_id = '$customer_id') credit_amount
                    FROM customer_ledger cus
                    WHERE cus.is_deleted = 'Live' AND cus.customer_id = '$customer_id'
                    GROUP BY cus.customer_id
                    ORDER BY cus.customer_id ASC) a";
        $result = $this->db->query($sql)->row();
        if (!empty($result)) {
            return $result->total_payable_amount;
        } else {
            return $result;
        }
    }
}
