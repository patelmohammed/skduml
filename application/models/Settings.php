<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

//BANK LIST
    public function get_bank_list() {
        $this->db->select('
				bank_add.*,
				sum(bank_summary.dr) as debit,
				sum(bank_summary.cr) as credit
			');
        $this->db->from('bank_add');
        $this->db->join('bank_summary', 'bank_summary.bank_id = bank_add.bank_id', 'left');
        $this->db->group_by('bank_add.bank_id');
        $this->db->where('bank_add.status', 1);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Person  List
    public function person_list() {
//        $this->db->select('*');
//        $this->db->from('person_information');
//        $this->db->where('status', 1);
//
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
        return false;
    }

    public function pesonal_loan_information() {
        $this->db->select('*');
        $this->db->from('pesonal_loan_information');
        $this->db->where('status', 1);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Person  List
    public function person_list_limt($per_page, $limit) {
//        $this->db->select('*');
//        $this->db->from('person_information');
//        $this->db->where('status', 1);
//        $this->db->limit($per_page, $limit);
//
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
        return false;
    }

//Selecst Person
    public function select_person_by_id($person_id) {
//        $this->db->select('*');
//        $this->db->from('person_information');
//        $this->db->where('person_id', $person_id);
//        $this->db->where('status', 1);
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
        return false;
    }

// personal loan person id selecetd
    public function select_loan_person_by_id($person_id) {
        $this->db->select('*');
        $this->db->from('pesonal_loan_information');
        $this->db->where('person_id', $person_id);
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//update date for loan
    public function updata_loan_id($person_id) {
        $this->db->select('a.*,b.*');
        $this->db->from('personal_loan a');
        $this->db->join('pesonal_loan_information  b', 'a.person_id=b.person_id', 'left');
        $this->db->where('per_loan_id', $person_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Person  List Count
    public function person_list_count() {
//        $this->db->select('*');
//        $this->db->from('person_information');
//        $this->db->where('status', 1);
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->num_rows();
//        }
        return false;
    }

    public function person_loan_count() {
        $this->db->select('*');
        $this->db->from('personal_loan');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

//Retrieve customer All data 
    public function personledger_tradational($person_id) {
        $this->db->select('
			person_ledger.*,
			sum(person_ledger.debit) as debit,
			sum(person_ledger.credit) as credit
			');
        $this->db->from('person_ledger');
        $this->db->where('person_id', $person_id);
        $this->db->order_by('id', 'asc');
        $this->db->group_by('transaction_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Personal loan detail ledger
    public function personal_loan_tradational($person_id) {

        $this->db->select('
			personal_loan.*,
			sum(personal_loan.debit) as debit,
			sum(personal_loan.credit) as credit
			');
        $this->db->from('personal_loan');
        $this->db->where('person_id', $person_id);
        $this->db->group_by('transaction_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//personal loan limit 
    public function person_list_limt_loan($per_page, $limit) {
        $this->db->select('
				pesonal_loan_information.*,
				sum(personal_loan.debit) as debit,
				sum(personal_loan.credit) as credit
			');
        $this->db->from('pesonal_loan_information');
        $this->db->join('personal_loan', 'pesonal_loan_information.person_id = personal_loan.person_id', 'left');
        $this->db->where('pesonal_loan_information.status', 1);
        $this->db->group_by('pesonal_loan_information.person_id');
        $this->db->limit($per_page, $limit);
        $this->db->order_by('pesonal_loan_information.id', 'DESC');
        $query = $this->db->get();
//        echo '<pre>'; 
//        echo  $this->db->last_query($query);
//        die;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

// personal loan list
    public function loan_list_personal($per_page, $limit) {
        $this->db->select('a.*,b.*');
        $this->db->from('personal_loan a');
        $this->db->join('pesonal_loan_information  b', 'a.person_id=b.person_id', 'left');

        $this->db->limit($per_page, $limit);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//loan person list for update form
    public function loan_updatlist_personal() {
        $this->db->select('*');
        $this->db->from('pesonal_loan_information');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

// submit person for personal loan
    public function submit_person_personal_loan($data) {
        $result = $this->db->insert('pesonal_loan_information', $data);
        if ($result) {
            $this->db->select('*');
            $this->db->from('pesonal_loan_information');
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                $json_loan[] = array('label' => $row->person_name, 'value' => $row->person_id);
            }
            $cache_file = './my-assets/js/admin_js/json/loan_personl.json';
            $loanList = json_encode($json_loan);
            file_put_contents($cache_file, $loanList);
            return TRUE;
        } else {
            return false;
        }
    }

    public function person_list_personal_loan() {
        $this->db->select('*');
        $this->db->from('pesonal_loan_information');
        $this->db->where('status', 1);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Retrieve customer All data 
    public function ledger_search_by_date($person_id, $from_date, $to_date) {
        $this->db->select('
			person_ledger.*,
			sum(person_ledger.debit) as debit,
			sum(person_ledger.credit) as credit
			');
        $this->db->from('person_ledger');
        $this->db->where('person_id', $person_id);
//        $this->db->where($dateRange);
        $this->db->where('date >=', $from_date);
        $this->db->where('date <=', $to_date);
        $this->db->group_by('transaction_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Retrieve customer All data 
    public function person_loan_search_by_date($person_id, $from_date, $to_date) {
//        $dateRange = "date BETWEEN '$from_date' AND '$to_date'";
        $this->db->select('
			personal_loan.*,
			sum(personal_loan.debit) as debit,
			sum(personal_loan.credit) as credit
			');
        $this->db->from('personal_loan');
        $this->db->where('person_id', $person_id);
//        $this->db->where($dateRange);
        $this->db->where('date >=', $from_date);
        $this->db->where('date <=', $to_date);
        $this->db->group_by('transaction_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//
    public function ledger_search_by_date_count($person_id, $from_date, $to_date) {
        $this->db->select('
			person_ledger.*,
			sum(person_ledger.debit) as debit,
			sum(person_ledger.credit) as credit
			');
        $this->db->from('person_ledger');
        $this->db->where('person_id', $person_id);
        $this->db->where('date >=', $from_date);
        $this->db->where('date <=', $to_date);
        $this->db->group_by('transaction_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    public function checkExistingBank($model, $id = '') {
        if ($id != '') {

            $result = $this->db->select('bank_name')
                    ->from('bank_add')
                    ->where('bank_name', $model)
                    ->where('status', 1)
                    ->where('bank_id != ', $id)
                    ->get()
                    ->row('bank_name');
        } else {
            $result = $this->db->select('bank_name')
                    ->from('bank_add')
                    ->where('status', 1)
                    ->where('bank_name', $model)
                    ->get()
                    ->row('bank_name');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

    public function person_loan_search_by_date_count($person_id, $from_date, $to_date) {
        $this->db->select('
			personal_loan.*,
			sum(personal_loan.debit) as debit,
			sum(personal_loan.credit) as credit
			');
        $this->db->from('personal_loan');
        $this->db->where('person_id', $person_id);
        $this->db->where('date >=', $from_date);
        $this->db->where('date <=', $to_date);
        $this->db->group_by('transaction_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

//Get bank by id
    public function get_bank_by_id($bank_id) {
        $this->db->select('*');
        $this->db->from('bank_add');
        $this->db->where('bank_id', $bank_id);
        $this->db->where('status', 1);
        $this->db->where('active_status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

//Bank updaet by id
    public function bank_update_by_id($bank_id) {
        if ($_FILES['signature_pic']['name']) {

            $config['upload_path'] = './my-assets/image/logo/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
            $config['max_size'] = "*";
            $config['max_width'] = "*";
            $config['max_height'] = "*";
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('signature_pic')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_userdata(array('error_message' => $this->upload->display_errors()));
                redirect(base_url('Csettings/index'));
            } else {
                $image = $this->upload->data();
                $signature_pic = base_url() . "my-assets/image/logo/" . $image['file_name'];
            }
        }

        $old_logo = $this->input->post('old_pic');

        $data = array(
//            'bank_id' => $this->auth->generator(10),
            'bank_name' => $this->input->post('bank_name'),
            'ac_name' => $this->input->post('ac_name'),
            'ac_number' => $this->input->post('ac_no'),
            'branch' => $this->input->post('branch'),
            'IFSC_code' => $this->input->post('IFSC_code'),
            'signature_pic' => (!empty($signature_pic) ? $signature_pic : $old_logo),
            'status' => 1
        );

        $data['bank_name'] = $this->input->post('bank_name');
        $this->db->where('bank_id', $bank_id);
        $this->db->update('bank_add', $data);
        return true;
    }

//==========Bank Ledger=============//
    public function bank_ledger($bank_id) {
//        $this->db->select('*');
//        $this->db->from('bank_summary');
//        $this->db->where('bank_id', $bank_id);
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return false;
        $sql = "SELECT cm.transaction_id,tr.date_of_transaction,tr.description,ba.bank_name,tr.transaction_type,cm.amount,tr.transaction_mode,tr.receipt_amount,tr.pay_amount 
                FROM cheque_manger cm
                INNER JOIN transaction tr ON tr.transaction_id = cm.transaction_id AND tr.is_deleted = '1'
                INNER JOIN bank_add ba ON ba.bank_id = cm.bank_id AND ba.status = '1'
                WHERE cm.bank_id = '$bank_id' AND cm.status = '1' 
                ORDER BY tr.date_of_transaction ASC";
        return $this->db->query($sql)->result();
    }

//==========Bank Info=============//
    public function bank_info($bank_id) {
        $this->db->select('*');
        $this->db->from('bank_add');
        $this->db->where('bank_id', $bank_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //active_inactive_status
    function updateStatus($data, $bank_id) {
        $this->db->where('bank_id', $bank_id);
        $this->db->update('bank_add', $data);

        return true;
    }

//    public function delete_bank($bank_id) {
//
//        $this->db->where('bank_id', $bank_id);
//        $this->db->update('bank_add', array('status' => 0));
//        return true;
//    }
//Table list
    public function table_list($per_page, $page) {
        $this->db->select('*');
        $this->db->from('accounts');
        $this->db->limit($per_page, $page);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Table list _count
    public function table_list_count() {
        $this->db->select('*');
        $this->db->from('accounts');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

//COUNT PRODUCT
    public function bank_entry($data) {
        $this->db->insert('bank_add', $data);
    }

//Add person
    public function submit_person($data) {
        $result = $this->db->insert('person_information', $data);
        if ($result) {
            $this->db->select('*');
            $this->db->from('person_information');
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                $json_loan[] = array('label' => $row->person_name, 'value' => $row->person_id);
            }
            $cache_file = './my-assets/js/admin_js/json/loan.json';
            $loanList = json_encode($json_loan);
            file_put_contents($cache_file, $loanList);
            return TRUE;
        } else {
            return false;
        }
    }

//Update person 
    public function update_person($data, $person_id) {
        $this->db->where('person_id', $person_id);
        $result = $this->db->update('person_information', $data);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

// Update loan 
    public function update_per_loan($data, $person_id) {
        $this->db->where('per_loan_id', $person_id);
        $result = $this->db->update('personal_loan', $data);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

// update loan Person
    public function update_loan_person($data, $person_id) {
        $this->db->where('person_id', $person_id);
        $result = $this->db->update('pesonal_loan_information', $data);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

//Add loan
    public function submit_loan($data) {
        $result = $this->db->insert('person_ledger', $data);
        if ($result) {
            $cache_file = './my-assets/js/admin_js/json/loan.json';
            file_put_contents($cache_file);
            return TRUE;
        } else {
            return false;
        }
    }

    public function submit_loan_personal($data) {
        $result = $this->db->insert('personal_loan', $data);
        if ($result) {
            $cache_file = './my-assets/js/admin_js/json/loan.json';
            $loanList = json_encode($result);
            file_put_contents($cache_file, $loanList);
            return TRUE;
        } else {
            return false;
        }
    }

// loan person select list
    public function loan_list() {
//        $this->db->select('*');
//        $this->db->from('person_information');
//        $this->db->order_by('person_id', 'desc');
////        $this->db->limit('500');
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
        return false;
    }

//Submit payment
    public function submit_payment($data) {
        $result = $this->db->insert('person_ledger', $data);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

####===========Submit payment personal loan ==================###

    public function submit_payment_per_loan($data) {
        $result = $this->db->insert('personal_loan', $data);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function bank_debit_credit_manage_entry($data) {
        $this->db->insert('bank_summary', $data);
        return true;
    }

//Table create
    public function table_create($account_id) {
        $account_name = $this->input->post('account_name');
        $status = $this->input->post('account_type');

        $account_exists = $this->db->select('*')
                ->from('accounts')
                ->where('account_name', $account_name)
                ->get()
                ->num_rows();
        if ($account_exists > 0) {
            $this->session->set_userdata(array('error_message' => display('account_already_exists')));
            return true;
        } else {
            if ($status == 1) {
                $account_table_name = "outflow_" . $account_id;
            } else {
                $account_table_name = "inflow_" . $account_id;
            }

            $data = array(
                'account_id' => $this->auth->generator(10),
                'account_name' => $account_name,
                'account_table_name' => $account_table_name,
                'status' => $status
            );

            $this->db->insert('accounts', $data);
            $sql = "CREATE TABLE IF NOT EXISTS " . $account_table_name . " (
					`transection_id` varchar(200) NOT NULL,
					`tracing_id` varchar(200) NOT NULL,
					`payment_type` varchar(10) NOT NULL,
					`date` varchar(255) NOT NULL,
					`amount` float NOT NULL,
					`description` varchar(255) NOT NULL,
					`status` int(5) NOT NULL,
					 PRIMARY KEY (`transection_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1";
            $this->db->query($sql);
            $this->session->set_userdata(array('message' => display('successfully_created')));
        }
    }

//Retrive table data by id
    public function retrive_table_data($account_id) {
        $this->db->select('*');
        $this->db->from('accounts');
        $this->db->where('account_id', $account_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Update table data by id
    public function update_table_data($account_name, $account_id) {

        $account_exists = $this->db->select('*')
                ->from('accounts')
                ->where('account_name', $account_name['account_name'])
                ->get()
                ->num_rows();
        if ($account_exists > 0) {
            $this->session->set_userdata(array('error_message' => display('account_already_exists')));
            return false;
        } else {
            $this->db->where('account_id', $account_id);
            $this->db->update('accounts', $account_name);
            $this->session->set_userdata(array('message' => display('successfully_updated')));
            return true;
        }
    }

//Select customer from invoice
    public function customer_info() {
//        $query = $this->db->select('*')
//                ->from('customer_information')
//                ->join('invoice', 'invoice.customer_id = customer_information.customer_id')
//                ->group_by('invoice.customer_id')
//                ->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return false;
        $sql = "SELECT * 
                FROM customer_information ci
                LEFT JOIN sale_invoice si ON si.RefCustomerID = ci.customer_id AND si.status = '1'
                GROUP BY si.RefCustomerID";
        return $this->db->query($sql)->result_array();
    }

//Retrive product info
    public function product_info($customer_id, $product_id, $from, $to) {
//        $sql = "SELECT si.DateTime, sii.*, pi.product_name 
//                FROM sale_invoice_item sii
//                LEFT JOIN sale_invoice si ON si.SIID = sii.refSIID AND si.status = '1'
//                INNER JOIN product_information pi ON pi.product_id = sii.refProductID AND pi.status = '1'
//                WHERE si.RefCustomerID = '$customer_id' 
//                AND DATE_FORMAT(si.DateTime,'%Y-%m-%d') >= '$from' 
//                AND DATE_FORMAT(si.DateTime,'%Y-%m-%d') <= '$to'  
//                AND sii.refProductID = '$product_id' 
//                AND sii.status = '1' ";

        $sql = "SELECT so.DateTime, soi.*, pi.product_name 
                FROM sale_outward_item soi 
                LEFT JOIN sale_outward so ON so.SOID = soi.refSOID AND so.status = '1'
                INNER JOIN product_information pi ON pi.product_id = soi.refProductID AND pi.status = '1'
                WHERE so.RefCustomerID = '$customer_id' 
                AND DATE_FORMAT(so.DateTime,'%Y-%m-%d') >= '$from' 
                AND DATE_FORMAT(so.DateTime,'%Y-%m-%d') <= '$to'  
                AND soi.refProductID = '$product_id' 
                AND soi.status = '1'";
        return $this->db->query($sql)->result_array();
    }

    public function get_bank_transactions($bank_id = null) {
        $sql = "SELECT bs.*,b.bank_name,b.ac_name,b.ac_number,b.branch,b.IFSC_code,b.signature_pic   
                FROM bank_summary bs
                    INNER JOIN bank_add b ON bs.bank_id = b.bank_id
                WHERE bs.status = 1";

        if (isset($bank_id) && !empty($bank_id)) {
            $sql .= " AND bs.bank_id = $bank_id";
        }
        return $this->db->query($sql)->result();
    }

    public function delete_transaction($id) {

        $this->db->where('id', $id);
        $this->db->update('bank_summary', array('status' => 0));
        return true;
    }

    public function retrieve_transaction_editdata($id) {

        return $this->db->select('bank_summary.*,bank_add.bank_name,bank_add.bank_id')
                        ->from('bank_summary')
                        ->join('bank_add', 'bank_summary.bank_id = bank_add.bank_id', 'inner')
                        ->where('bank_summary.id', $id)->where('bank_summary.status', 1)->get()->result(); //->row();
    }

    public function update_bank_transaction($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('bank_summary', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getCommissionData() {
//          $sql = "SELECT si.DateTime, sii.*, co.commission_rate 
//                FROM commission co 
//                INNER JOIN sale_invoice si ON si.RefCustomerID = co.ref_customer_id AND si.status = '1'
//                INNER JOIN sale_invoice_item sii ON sii.refSIID = si.SIID AND sii.refProductID = co.ref_product_id AND sii.status = '1'
//                WHERE co.status = '1'";

        $sql = "SELECT so.DateTime, soi.*, co.commission_rate 
                FROM commission co 
                INNER JOIN sale_outward so ON so.RefCustomerID = co.ref_customer_id AND so.status = '1'
                INNER JOIN sale_outward_item soi ON soi.refSOID = so.SOID AND soi.refProductID = co.ref_product_id AND soi.status = '1'
                WHERE co.status = '1'";
        return $this->db->query($sql)->result_array();
    }

    function get_banks_lists($postData) {

        $sql = $this->get_banks_lists_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);

        return $query->result();
    }

    function get_banks_lists_search_data($postData) {
        $sql = $this->get_banks_lists_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_banks_lists_query($postData) {
        $column_order = array('', 'a.bank_name', 'a.ac_name', 'a.ac_number', 'a.branch', 'a.IFSC_code', 'a.Balance', '', '');
        $sql = "SELECT a.* FROM (
                    SELECT ba.*, (IFNULL(SUM(bs.cr),0) - IFNULL(SUM(bs.dr),0)) AS Balance
                    FROM bank_add ba
                        LEFT JOIN bank_summary bs ON bs.bank_id = ba.bank_id AND bs.status = '1'
                    WHERE ba.status = '1'
                    GROUP BY ba.bank_id
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 3)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.bank_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.bank_id DESC';
        }
        return $sql;
    }

    function get_commission_list($postData, $type) {
        $sql = $this->get_commission_list_query($postData, $type);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_commission_list_search_data($postData, $type) {
        $sql = $this->get_commission_list_query($postData, $type);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_commission_list_query($postData, $type) {
        $column_order = array('', 'a.DateTime', 'a.product_name', 'a.Qty', 'a.commission_rate', 'a.TotalCommission', '');

        $sql = "SELECT a.* FROM(
                    SELECT si.*, SUM(sii.Qty) AS total_qty, al.agent_name, cl.commission_name
                    FROM sale_invoice si
                        INNER JOIN sale_invoice_item sii ON sii.refSIID = si.SIID
                        LEFT JOIN agent_list al ON al.agent_id = si.RefAgentId
                        LEFT JOIN commission_list cl ON cl.commission_id = si.RefCommissionRateId
                    WHERE si.status = 1 AND si.InvoiceType = 2 AND si.RefAgentId IS NOT NULL AND si.RefCommissionRate != 0";

        if (isset($postData['agent_id']) && !empty($postData['agent_id'])) {
            $sql .= " AND si.RefAgentId = " . $postData['agent_id'];
        }

        if (isset($postData['start_date']) && !empty($postData['start_date'])) {
            $sql .= " AND si.DateTime >= '" . date('Y-m-d', strtotime($postData['start_date'])) . "%'";
        }

        if (isset($postData['end_date']) && !empty($postData['end_date'])) {
            $sql .= " AND si.DateTime <= '" . date('Y-m-d', strtotime($postData['end_date'])) . "%'";
        }

        $sql .= " GROUP BY si.SIID
                ORDER BY si.DateTime ASC";

        $sql .= " )a WHERE 1=1";

        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }

        $sql .= " GROUP BY a.SIID";

        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.DateTime ASC';
        }

//        preprint($sql);
        return $sql;
    }

    function get_transaction_list($postData, $bank_id) {
        $sql = $this->get_transaction_list_query($postData, $bank_id);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_transaction_list_search_data($postData, $bank_id) {
        $sql = $this->get_transaction_list_query($postData, $bank_id);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_transaction_list_query($postData, $bank_id = null) {
        $column_order = array('', 'a.date', 'a.bank_name', 'a.ac_name', 'a.deposite_id', 'a.deposite_id', 'a.dr', 'a.cr', '');

        $sql = "SELECT a.* FROM (
                        SELECT bs.*, b.bank_name, b.ac_name
                        FROM bank_summary bs
                            INNER JOIN bank_add b ON bs.bank_id = b.bank_id
                        WHERE bs.status = 1";

        if (isset($bank_id) && !empty($bank_id)) {
            $sql .= " AND bs.bank_id = $bank_id";
        }

        $sql .= " ) a WHERE 1=1";

        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.date ASC, a.id ASC';
        }
        return $sql;
    }
}
