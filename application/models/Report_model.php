<?php

class Report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function saleReportByDate($startDate = '', $endDate = '') {
        $sql = "SELECT spd.payment_date, SUM(spd.total_payment) as total_debit, SUM(spd.milk_quantity) as total_milk_sale, spd.milk_price 
                FROM tbl_sabhasad_payment_debit spd 
                INNER JOIN tbl_sabhasad s ON s.sabhasad_id = spd.ref_sabhasad_id 
                WHERE 1 = 1 ";
        if (!empty($startDate) && $startDate != '') {
            $sql .= " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d') >= '$startDate' ";
        }
        if (!empty($endDate) && $endDate != '') {
            $sql .= " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d') <= '$endDate' ";
        }
        $sql .= " GROUP BY date(spd.payment_date), spd.milk_price ";
        return $this->db->query($sql)->result();
    }

    public function saleReportBySabhasad($startDate = '', $endDate = '', $sabhasad_id = '') {
        $sabhasad_where = (isset($sabhasad_id) && !empty($sabhasad_id) ? ' AND s.sabhasad_id = ' . $sabhasad_id : '');
        $credit_where = '';
        $debit_where = '';
        if (isset($startDate) && !empty($startDate)) {
            $credit_where .= " AND DATE_FORMAT(spc.credit_date, '%Y-%m-%d') >= '$startDate' ";
            $debit_where .= " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d') >= '$startDate' ";
        }
        if (isset($endDate) && !empty($endDate)) {
            $credit_where .= " AND DATE_FORMAT(spc.credit_date, '%Y-%m-%d') <= '$endDate' ";
            $debit_where .= " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d') <= '$endDate' ";
        }
        $sql = "SELECT * 
                FROM(
                    SELECT spc.credit_id AS primaryId, spc.credit_date AS paymentDate, spc.remark, '' AS milk_quantity, spc.amount AS credit_amount, '' AS debit_amount, spc.InsDateTime, 'credit' AS payment_type, s.sabhasad_name, s.sabhasad_code, s.sabhasad_name_en, 'tbl_sabhasad_payment_credit' AS tableName, '' AS milk_price 
                    FROM tbl_sabhasad_payment_credit spc 
                    INNER JOIN tbl_sabhasad s ON s.sabhasad_id = spc.ref_sabhasad_id AND s.del_status = 'Live' $sabhasad_where 
                    WHERE spc.del_status = 'Live' $credit_where 
                    UNION(
                        SELECT spd.payment_id AS primaryId, spd.payment_date AS paymentDate, ''AS remark, spd.milk_quantity AS milk_quantity, '' AS credit_amount, spd.total_payment AS debit_amount, spd.InsDateTime, 'debit' AS payment_type, s.sabhasad_name, s.sabhasad_code, s.sabhasad_name_en, 'tbl_sabhasad_payment_debit' AS tableName, spd.milk_price AS milk_price 
                        FROM tbl_sabhasad_payment_debit spd 
                        INNER JOIN tbl_sabhasad s ON s.sabhasad_id = spd.ref_sabhasad_id AND s.del_status = 'Live' $sabhasad_where 
                        WHERE spd.del_status = 'Live' $debit_where 
                    )
                ) a 
                WHERE 1 = 1 
                ORDER BY InsDateTime ASC";
        return $this->db->query($sql)->result();
    }

    public function detailedSaleReport($startDate = '', $endDate = '') {
        $sql = "SELECT spd.*, s.sabhasad_name, s.sabhasad_name_en 
                FROM tbl_sabhasad_payment_debit spd 
                INNER JOIN tbl_sabhasad s ON s.sabhasad_id = spd.ref_sabhasad_id AND s.del_status = 'Live'
                WHERE spd.del_status = 'Live' ";
        if (!empty($startDate) && $startDate != '') {
            $sql .= " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d') >= '$startDate' ";
        }
        if (!empty($endDate) && $endDate != '') {
            $sql .= " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d') <= '$endDate' ";
        }
        $sql .= " GROUP BY spd.payment_id ";
        return $this->db->query($sql)->result();
    }

}
