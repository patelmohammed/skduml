<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //Count Product
    public function count_product() {
        $sql = "SELECT * FROM product_information pi WHERE pi.status = 1";
        return $this->db->query($sql)->num_rows();
    }

    //Product List
    public function product_list($per_page = '', $page = '') {
        $query = $this->db->select('supplier_information.*,product_information.*,supplier_product.supplier_price,supplier_product.opening_stock as s_opening_stock,supplier_product.LandedCost as s_LandedCost,product_category.category_name,product_group.product_group_name')
                ->from('product_information')
                ->join('supplier_product', 'product_information.id = supplier_product.product_id', 'left')
                ->join('supplier_information', 'supplier_information.supplier_id = supplier_product.supplier_id', 'left')
                ->join('product_category', 'product_category.category_id = product_information.category_id', 'left')
                ->join('product_group', 'product_group.id = product_information.product_group_id')
                ->where('product_information.status', '1')
                ->order_by('product_information.product_id', 'desc')
//                ->limit($per_page, $page)
                ->get();
//        preprint($query);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //All Product List
    public function all_product_list() {
        $query = $this->db->select('supplier_information.*,product_information.*,supplier_product.*,product_category.category_name')
                ->from('product_information')
                ->join('supplier_product', 'product_information.id = supplier_product.product_id', 'LEFT')
                ->join('supplier_information', 'supplier_information.supplier_id = supplier_product.supplier_id', 'LEFT')
                ->join('product_category', 'product_category.category_id = product_information.category_id', 'LEFT')
                ->group_by('product_information.id')
                ->where('product_information.status', '1')
                ->order_by('product_information.id', 'DESC')
                ->get();
//        preprint($this->db->last_query());
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Product List
    public function product_list_count() {
        $query = $this->db->select('supplier_information.*,product_information.*,supplier_product.*')
                ->from('product_information')
                ->join('supplier_product', 'product_information.product_id = supplier_product.product_id', 'left')
                ->join('supplier_information', 'supplier_information.supplier_id = supplier_product.supplier_id', 'left')
                ->where('product_information.status', '1')
                ->order_by('product_information.product_id', 'desc')
                ->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

//Product tax list
    public function retrieve_product_tax() {
        $result = $this->db->select('*')
                ->from('tax_information')
                ->get()
                ->result();

        return $result;
    }

//Tax selected item
    public function tax_selected_item($tax_id) {
        $result = $this->db->select('*')
                ->from('tax_information')
                ->where('tax_id', $tax_id)
                ->get()
                ->result();

        return $result;
    }

//Product generator id check 
    public function product_id_check($product_id) {
        $query = $this->db->select('*')
                ->from('product_information')
                ->where('product_id', $product_id)
                ->get();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

//Count Product
    public function product_entry($data, $taxdata, $supplier_data) {

        $this->db->trans_start();
        $this->db->insert('product_information', $data);
        $product_id = $this->db->insert_id();

        foreach ($taxdata as $key => $tax) {
            if ($tax != '') {
                $tax_ind = array(
                    'product_id' => $product_id,
                    'tax_id' => $tax
                );
                $this->db->insert('tax_product', $tax_ind);
            }
        }

        if (!empty($supplier_data) && $supplier_data != '') {
            foreach ($supplier_data as $skey => $sval) {

                $_supplier_id = isset($sval['supplier_id']) && !empty($sval['supplier_id']) ? $sval['supplier_id'] : null;
                $_supplier_price = isset($sval['supplier_price']) && !empty($sval['supplier_price']) ? $sval['supplier_price'] : $data['price'];
                $_LandedCost = isset($sval['LandedCost']) && !empty($sval['LandedCost']) ? $sval['LandedCost'] : $data['LandedCost'];
                $_opening_stock = isset($sval['opening_stock']) && !empty($sval['opening_stock']) ? $sval['opening_stock'] : $data['opening_stock'];

                $supp_prd = array(
                    'product_id' => $product_id,
                    'supplier_id' => $_supplier_id, //$sval['supplier_id'],
                    'supplier_price' => $_supplier_price, //$sval['supplier_price'],
                    'LandedCost' => $_LandedCost, //$sval['LandedCost'],
                    'opening_stock' => $_opening_stock, //$sval['opening_stock'],
//                    'products_model' => $data['product_model']
                );

                $this->db->insert('supplier_product', $supp_prd);
            }
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            return false;
        }
        return true;
    }

    public function checkExistingProduct($model, $id = '') {
        if ($id != '') {

            $result = $this->db->select('product_name')
                    ->from('product_information')
                    ->where('product_name', $model)
                    ->where('status', 1)
                    ->where('id != ', $id)
                    ->get()
                    ->row('product_name');
        } else {
            $result = $this->db->select('product_name')
                    ->from('product_information')
                    ->where('status', 1)
                    ->where('product_name', $model)
                    ->get()
                    ->row('product_name');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }
    public function checkExistingProductBarcode($model, $id = '') {
        if ($id != '') {

            $result = $this->db->select('Product_Barcode')
                    ->from('product_information')
                    ->where('Product_Barcode', $model)
                    ->where('status', 1)
                    ->where('id != ', $id)
                    ->get()
                    ->row('Product_Barcode');
        } else {
            $result = $this->db->select('Product_Barcode')
                    ->from('product_information')
                    ->where('status', 1)
                    ->where('Product_Barcode', $model)
                    ->get()
                    ->row('Product_Barcode');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

//Retrieve Product Edit Data
    public function retrieve_product_editdata($product_id) {
        $this->db->select('*');
        $this->db->from('product_information');
        $this->db->where('id', $product_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

// Supplier product information
    public function supplier_product_editdata($product_id) {
        $this->db->select('a.*,b.*');
        $this->db->from('supplier_product a');
        $this->db->join('supplier_information b', 'a.supplier_id=b.supplier_id');
        $this->db->where('a.product_id', $product_id);
        $this->db->where('a.status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//selected supplier product
    public function supplier_selected($product_id) {
        $this->db->select('*');
        $this->db->from('supplier_product');
        $this->db->where('product_id', $product_id);
        $this->db->where('status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Retrieve company Edit Data
    public function retrieve_company() {
        $this->db->select('*');
        $this->db->from('company_information');
        $this->db->limit('1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Update Categories
    public function update_product($data, $product_id, $supplier_data, $taxdata) {

        $this->db->trans_start();

        $this->db->where('id', $product_id);
        $this->db->update('product_information', $data);

        //text data
        $this->db->delete('tax_product', array('product_id' => $product_id));

        foreach ($taxdata as $key => $tax) {
            if ($tax != '') {
                $tax_ind = array(
                    'product_id' => $product_id,
                    'tax_id' => $tax
                );
                $this->db->insert('tax_product', $tax_ind);
            }
        }

        //supplier data
        $this->db->delete('supplier_product', array('product_id' => $product_id));
        if (!empty($supplier_data) && $supplier_data != '') {
            foreach ($supplier_data as $skey => $sval) {

                $_supplier_id = $sval['supplier_id'] != '' ? $sval['supplier_id'] : null;
                $_supplier_price = $sval['supplier_price'] != '' ? $sval['supplier_price'] : $data['price'];
                $_LandedCost = $sval['LandedCost'] != '' ? $sval['LandedCost'] : $data['LandedCost'];
                $_opening_stock = $sval['opening_stock'] != '' ? $sval['opening_stock'] : $data['opening_stock'];

                $supp_prd = array(
                    'product_id' => $product_id,
                    'supplier_id' => $_supplier_id, //$sval['supplier_id'],
                    'supplier_price' => $_supplier_price, //$sval['supplier_price'],
                    'LandedCost' => $_LandedCost, //$sval['LandedCost'],
                    'opening_stock' => $_opening_stock, //$sval['opening_stock'],
//                    'products_model' => $data['product_model']
                );
                $this->db->insert('supplier_product', $supp_prd);
            }
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            return false;
        }
        return true;
    }

    function updateStatus($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('product_group', $data);

        return true;
    }

// Delete Product Item
    public function delete_product($product_id) {

#### Check product is using on system or not##########
# If this product is used any calculation you can't delete this product.
# but if not used you can delete it from the system.
//        $this->db->select('product_id');
//        $this->db->from('product_purchase_details');
//        $this->db->where('product_id', $product_id);
//        $query = $this->db->get();
//
//        $affected_row = $this->db->affected_rows();
//        if ($affected_row == 0) {
        $data = array(
            'status' => '0'
        );
        $this->db->trans_start();
        $this->db->where('id', $product_id);
        $this->db->update('product_information', $data);
        $this->db->where('product_id', $product_id);
        $this->db->update('supplier_product', $data);
        $this->db->where('product_id', $product_id);
        $this->db->update('tax_product', $data);
        $this->db->trans_complete();

        $this->db->select('*');
        $this->db->from('product_information');
        $this->db->where('status', 1);
        $query = $this->db->get();
        return true;
//        } else {
//            $this->session->set_userdata(array('error_message' => display('you_cant_delete_this_product')));
//            return false;
//        }
    }

//Product By Search 
    public function product_search_item($product_id) {

        $query = $this->db->select('supplier_information.*,product_information.*,supplier_product.*,product_category.category_name,product_group.product_group_name')
                ->from('product_information')
                ->join('supplier_product', 'product_information.product_id = supplier_product.product_id', 'left')
                ->join('supplier_information', 'supplier_product.supplier_id = supplier_information.supplier_id', 'left')
                ->join('product_category', 'product_category.category_id = product_information.category_id')
                ->join('product_group', 'product_group.id = product_information.product_group_id')
                ->order_by('product_information.product_id', 'desc')
                ->where('product_information.product_id', $product_id)
                ->where('product_information.status', '1')
                ->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//Duplicate Entry Checking 
    public function product_model_search($product_model) {
        $this->db->select('*');
        $this->db->from('product_information');
        $this->db->where('product_model', $product_model);
        $query = $this->db->get();
        return $this->db->affected_rows();
    }

//Product Details
    public function product_details_info($product_id) {

        $this->db->select('p.*,pc.category_name,pg.product_group_name');
        $this->db->from('product_information p');
        $this->db->join('product_group pg', 'pg.id = p.product_group_id', 'inner');
        $this->db->join('product_category pc', 'pc.category_id = p.category_id', 'inner');
        $this->db->where('p.id', $product_id);
        $this->db->where('p.status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

// Product Purchase Report
    public function product_purchase_info($product_id) {
        $sql = "SELECT pi.DateTime,pi.PINID,pi.PINNO,pi.RefSupplierID,pi.GrandTotal,
                pii.Qty,pii.TotalAmt,pii.UnitAmt,p.LandedCost,pii.Discount, 
                GROUP_CONCAT(piit.refTaxID SEPARATOR ',') AS Tax_id, 
                GROUP_CONCAT(piit.refTaxName SEPARATOR ',') AS tax_name, 
                GROUP_CONCAT(piit.TaxPer SEPARATOR ',') AS tax_per, 
                SUM(piit.TaxAmt) AS TaxAmt 
                FROM purchase_invoice pi
                INNER JOIN purchase_invoice_item pii ON pii.refPINID = pi.PINID AND pii.status = '1'
                INNER JOIN purchase_invoice_itemtaxdet piit ON piit.refPINItemID = pii.PINItemID AND piit.status = '1'
                LEFT JOIN  product_information p ON pii.refProductID = p.id AND p.status = '1'
                WHERE  pi.status = '1' AND pii.refProductID = '$product_id'
                GROUP BY pii.PINItemID";
        return $this->db->query($sql)->result();
    }

// Invoice Data for specific data
    public function invoice_data($product_id) {
        $sql = "SELECT si.DateTime,si.SIID,si.SINO,si.RefCustomerID,si.GrandTotal,sii.Qty,sii.TotalAmt,sii.UnitAmt,sii.DiscountAmt,GROUP_CONCAT( siit.refTaxID SEPARATOR ',') AS Tax_id, GROUP_CONCAT( siit.refTaxName SEPARATOR ',') AS tax_name, GROUP_CONCAT( siit.TaxPer SEPARATOR ',') AS tax_per, SUM(siit.TaxAmt) AS TaxAmt
                FROM sale_invoice_item sii
                LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = sii.SIItemID AND siit.status = '1'
                LEFT JOIN sale_invoice si ON si.SIID = sii.refSIID AND si.status = '1'
                WHERE sii.refProductID = '$product_id' AND sii.status = '1'
                GROUP BY si.SIID
                HAVING si.SIID IS NOT NULL";

        return $this->db->query($sql)->result();
    }

    public function previous_stock_data($product_id, $startdate) {

        $this->db->select('date,sum(quantity) as quantity');
        $this->db->from('product_report');
        $this->db->where('product_id', $product_id);
        $this->db->where('date <=', $startdate);
        $query = $this->db->get();
//echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

// Invoice Data for specific data
    public function invoice_data_supplier_rate($product_id, $startdate, $enddate) {

        $this->db->select('
					date,
					sum(quantity) as quantity,
					rate,
					-rate*sum(quantity) as total_price,
					account
				');

        $this->db->from('product_report');
        $this->db->where('product_id', $product_id);

        $this->db->where('date >=', $startdate);
        $this->db->where('date <=', $enddate);

        $this->db->group_by('account');
        $this->db->order_by('date', 'asc');
        $query = $this->db->get();
//echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

// csv export info
    public function product_csv_file() {
        $query = $this->db->select('a.product_id,b.supplier_id,a.category_id,a.product_name,a.price,b.supplier_price,a.unit,a.tax,a.product_model,a.product_details,a.image,a.status')
                ->from('product_information a')
                ->join('supplier_product b', 'a.product_id = b.product_id', 'left')
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function product_godown_list() {
        $this->db->select('*');
        $this->db->from('product_godown');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function godown_entry($data) {
        $this->db->select('*');
        $this->db->from('product_godown');
        $this->db->where('status', 1);
        $this->db->where('godown_name', $data['godown_name']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('product_godown', $data);
            return TRUE;
        }
    }

    public function retrieve_godown_editdata($godown_id) {
        $this->db->select('*');
        $this->db->from('product_godown');
        $this->db->where('id', $godown_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function update_godown($data, $godown_id) {
        $this->db->where('id', $godown_id);
        $this->db->update('product_godown', $data);
        return true;
    }

// Delete customer Item
    public function delete_godown($godown_id) {
        $this->db->where('id', $godown_id);
        $this->db->update('product_godown', ['status' => 0]);
        return true;
    }

// Product Group

    public function productGroupList() {
        $this->db->select('*');
        $this->db->from('product_group');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function product_group_entry($data) {

        $this->db->select('*');
        $this->db->from('product_group');
        $this->db->where('status', 1);
        $this->db->where('product_group_name', $data['product_group_name']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('product_group', $data);
            return TRUE;
        }
    }

    public function retrieve_product_group_editdata($product_group_id) {
        $this->db->select('*');
        $this->db->from('product_group');
        $this->db->where('id', $product_group_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function update_product_group($data, $product_group_id) {
        $this->db->where('id', $product_group_id);
        $this->db->update('product_group', $data);
        return true;
    }

// Delete customer Item
    public function delete_product_group($product_group_id) {
        $this->db->where('id', $product_group_id);
        $this->db->delete('product_group');
        return true;
    }

    public function productMfgList() {
        $this->db->select('*');
        $this->db->from('product_mfg');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function product_mfg_entry($data) {

        $this->db->select('*');
        $this->db->from('product_mfg');
        $this->db->where('status', 1);
        $this->db->where('mfg_name', $data['mfg_name']);
        $this->db->where('mfg_code', $data['mfg_code']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('product_mfg', $data);
            return TRUE;
        }
    }

    public function retrieve_product_mfg_editdata($product_mfg_id) {
        $this->db->select('*');
        $this->db->from('product_mfg');
        $this->db->where('id', $product_mfg_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function update_product_mfg($data, $product_mfg_id) {
        $this->db->where('id', $product_mfg_id);
        $this->db->update('product_mfg', $data);
        return true;
    }

    public function delete_product_mfg($product_mfg_id) {
        $this->db->where('id', $product_mfg_id);
        $this->db->delete('product_mfg');
        return true;
    }

//Product Association

    public function raw_product_association_list() {
        $sql = "SELECT pi.id, 
                CONCAT(pi.product_name, ' - (',pc.category_name,')') as product_name,
                CONCAT(pi2.product_name, ' - (',pc2.category_name,')')  as raw_product,
                pra.Qty as Quantity
                FROM product_raw_association pra 
                INNER JOIN product_information pi ON pi.id = pra.ref_product_id 
                INNER JOIN product_information pi2 ON pi2.id = pra.ref_raw_product_id
                INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                INNER JOIN product_category pc2 ON pc2.category_id = pi2.category_id 
                WHERE 1 = 1"; // GROUP BY pra.ref_product_id";

        return $this->db->query($sql)->result();
    }

    public function getGroupProducts($product_group_id) {

        $sql = "SELECT pi.id, pi.product_name, pi.unit,CONCAT(pi.product_name, ' - (',pc.category_name,')') as product_name
                FROM product_information pi
                LEFT JOIN product_group pg ON pi.product_group_id = pg.id
                INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                WHERE pi.product_group_id = $product_group_id
                AND pi.id NOT IN (SELECT ref_product_id FROM `product_raw_association`)";

        return $this->db->query($sql)->result();
    }

    public function getGroupRawProducts($product_group_id) {

        $sql = "SELECT pi.id, CONCAT(pi.product_name, ' - (',pc.category_name,')') as product_name, pi.unit
                FROM product_information pi
                INNER JOIN product_group pg ON pi.product_group_id = pg.id 
                INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                WHERE pi.product_group_id = $product_group_id AND pi.status = '1' ";

        return $this->db->query($sql)->result();
    }

    public function insert_product_association($data) {
        $this->db->insert('product_raw_association', $data);
        return true;
    }

    public function delete_raw_product_association($product_id) {

        $this->db->where('ref_product_id', $product_id);
        $this->db->delete('product_raw_association');
        return true;
    }

    public function raw_product_association_by_id($product_id) {

        $sql = "SELECT pi.id, 
                CONCAT(pi.product_name, ' - (',pc.category_name,')') as product_name, pi.unit, 
                CONCAT(pi2.product_name, ' - (',pc2.category_name,')') as raw_product,
                pra.ref_raw_product_id as raw_product_id, 
                pra.Qty as Quantity,pra.ref_stage_id, 
                pi2.unit as unit 
                FROM product_raw_association pra 
                LEFT JOIN product_information pi ON pi.id = pra.ref_product_id 
                LEFT JOIN product_information pi2 ON pi2.id = pra.ref_raw_product_id
                INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                INNER JOIN product_category pc2 ON pc2.category_id = pi2.category_id 
                WHERE ref_product_id = $product_id";

        return $this->db->query($sql)->result();
    }

    public function all_product_list_by_type($product = false, $raw_product = false) {
        $sql = "SELECT si.*,pi.*,sp.*,CONCAT(pi.product_name, ' - (',pc.category_name,')') as product_name, pg.product_group_name
                FROM product_information pi
                    INNER JOIN supplier_product sp ON sp.product_id = pi.id AND sp.status = '1'
                    INNER JOIN supplier_information si ON si.supplier_id = sp.supplier_id AND si.status = '1'
                    INNER JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                    INNER JOIN product_group pg ON pg.id = pi.product_group_id AND pg.status = '1'
                WHERE pi.status = '1'";
        if ($product == true) {
            $sql .= " AND pi.product_group_id IN (1,3)";
        }
        if ($raw_product == true) {
            if ($product == true) {
                $sql .= " OR pi.product_group_id IN (2,4)";
            } else {
                $sql .= " AND pi.product_group_id IN (2,4)";
            }
        }
        $sql .= " GROUP BY pi.id
                ORDER BY pi.product_name ASC";
        $result = $this->db->query($sql)->result_array();
        if (!empty($result) && isset($result)) {
            return $result;
        }
//        preprint($sql);
        return FALSE;
    }

    public function checkExistingGodown($godown_name, $id = '') {
        if ($id != '') {

            $result = $this->db->select('godown_name')
                    ->from('product_godown')
                    ->where('godown_name', $godown_name)
                    ->where('status', 1)
                    ->where('id != ', $id)
                    ->get()
                    ->row('godown_name');
        } else {
            $result = $this->db->select('godown_name')
                    ->from('product_godown')
                    ->where('status', 1)
                    ->where('godown_name', $godown_name)
                    ->get()
                    ->row('godown_name');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

    public function checkExistingProductGroup($product_group_name, $id = '') {
        if ($id != '') {

            $result = $this->db->select('product_group_name')
                    ->from('product_group')
                    ->where('product_group_name', $product_group_name)
                    ->where('status', 1)
                    ->where('id != ', $id)
                    ->get()
                    ->row('product_group_name');
        } else {
            $result = $this->db->select('product_group_name')
                    ->from('product_group')
                    ->where('status', 1)
                    ->where('product_group_name', $product_group_name)
                    ->get()
                    ->row('product_group_name');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

    public function checkExistingProductMfg($product_mfg_name, $id = '') {
        if ($id != '') {

            $result = $this->db->select('mfg_name')
                    ->from('product_mfg')
                    ->where('mfg_name', $product_mfg_name)
                    ->where('status', 1)
                    ->where('id != ', $id)
                    ->get()
                    ->row('mfg_name');
        } else {
            $result = $this->db->select('mfg_name')
                    ->from('product_mfg')
                    ->where('status', 1)
                    ->where('mfg_name', $product_mfg_name)
                    ->get()
                    ->row('mfg_name');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

    public function checkExistingProductMfgCode($mfg_code, $id = '') {
        if ($id != '') {

            $result = $this->db->select('mfg_code')
                    ->from('product_mfg')
                    ->where('mfg_code', $mfg_code)
                    ->where('status', 1)
                    ->where('id != ', $id)
                    ->get()
                    ->row('mfg_code');
        } else {
            $result = $this->db->select('mfg_code')
                    ->from('product_mfg')
                    ->where('status', 1)
                    ->where('mfg_code', $mfg_code)
                    ->get()
                    ->row('mfg_code');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

    public function checkExistingProductStage($product_stage_name, $id = '') {
        if ($id != '') {
            $result = $this->db->select('product_stage_name')
                    ->from('product_stage')
                    ->where('product_stage_name', $product_stage_name)
                    ->where('status', 1)
                    ->where('stage_id != ', $id)
                    ->get()
                    ->row('product_stage_name');
        } else {
            $result = $this->db->select('product_stage_name')
                    ->from('product_stage')
                    ->where('status', 1)
                    ->where('product_stage_name', $product_stage_name)
                    ->get()
                    ->row('product_stage_name');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

    public function productStageList() {
        $this->db->select('*');
        $this->db->from('product_stage');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function product_stage_entry($data) {

        $this->db->select('*');
        $this->db->from('product_stage');
        $this->db->where('status', 1);
        $this->db->where('product_stage_name', $data['product_stage_name']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('product_stage', $data);
            return TRUE;
        }
    }

    public function retrieve_product_stage_editdata($product_stage_id) {
        $this->db->select('*');
        $this->db->from('product_stage');
        $this->db->where('stage_id', $product_stage_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function update_product_stage($data, $product_stage_id) {
        $this->db->where('stage_id', $product_stage_id);
        $this->db->update('product_stage', $data);
        return true;
    }

    public function delete_product_stage($product_stage_id) {
        $this->db->where('stage_id', $product_stage_id);
        $this->db->update('product_stage', array('status' => 0));
        return true;
    }

    //15/02/2020

    public function getTotalPurchaseSaleAndStock_byProductID($product_id) {
        $date = date('Y-m-d H:i:s');

        $sql = "SELECT sh.product_id, SUM(sh.Purchase) AS total_purchase, SUM(sh.sell) AS total_sell, 
                    (SUM(sh.opening_stock)
                    + SUM(sh.Purchase)
                    - SUM(sh.purchase_return)
                    - SUM(sh.sell)
                    + SUM(sh.sell_return)) AS stock
                FROM stock_history sh
                WHERE sh.product_id = '$product_id' AND sh.vdate <= '$date'";
//        preprint($sql);

        return $this->db->query($sql)->row();
    }

    public function getDataByProductId($product_id) {
//        $sql = "SELECT pp.id AS production_id, ppd.product_qty, ppr.consumption, ppr.supplier_id, pp.reference_no, pp.transaction_date 
//                FROM product_information pi
//                LEFT JOIN product_group pg ON pi.product_group_id = pg.id
//                LEFT JOIN product_production_raw ppr ON ppr.product_row_id = '$product_id'
//                LEFT JOIN product_production_det ppd ON ppd.id = ppr.refProductionDetID
//                LEFT JOIN product_production pp ON pp.id = ppd.refProductionId AND pp.is_deleted = '0'
//                LEFT JOIN users u ON u.user_id = pp.emp_id
//                LEFT JOIN product_category pc ON pc.category_id = pi.category_id 
//                WHERE pi.status = '1' AND pi.product_id = '$product_id'";

        $sql = "SELECT a.* 
                FROM (
                SELECT pp2.id AS production_id,pp2.production_lot_no, ppd2.product_qty, ppd2.refSupplierId AS supplier_id, pp2.production_start_datetime,'1' AS consumption
                FROM product_production_det ppd2
                LEFT JOIN product_production pp2 ON pp2.id = ppd2.refProductionId
                WHERE ppd2.refProductId = '$product_id'
                UNION (
                SELECT pp.id AS production_id,pp.production_lot_no, ppd.product_qty, ppr.supplier_id AS supplier_id, pp.production_start_datetime ,ppr.consumption
                FROM product_production_raw ppr
                LEFT JOIN product_production_det ppd ON ppd.id = ppr.refProductionDetID
                LEFT JOIN product_production pp ON pp.id = ppd.refProductionId AND pp.delete_status = '1'
                WHERE ppr.product_row_id = '$product_id'
                )) a WHERE 1 = 1";

        return $this->db->query($sql)->result();
    }

    public function get_production_raw_products_by_id($id, $product_id) {

        $sql = "SELECT pi.product_id as raw_product_id, 
                CONCAT(pi.product_name,' - (',pc.category_name,')') as product_name, 
                pi.unit, pi.price, b.supplier_id, b.consumption, b.total_qty,pra.Qty,b.ref_stage_id
                FROM product_raw_association pra
                INNER JOIN product_information pi ON pi.product_id = pra.ref_raw_product_id AND pi.status = '1' 
                INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                LEFT JOIN (SELECT ppr.* 
               	FROM product_production_raw ppr
               	INNER JOIN product_production_det ppd ON ppd.id = ppr.refProductionDetID AND ppd.refProductId = '$product_id'
               	INNER JOIN product_production pp ON pp.id = ppd.refProductionId
               	Where pp.id = $id
              ) b ON b.product_row_id = pi.product_id
              WHERE pra.ref_product_id = '$product_id'";
        return $this->db->query($sql)->result();
    }

    public function getPendingProductionCount($product_id) {
        $sql = "SELECT COUNT(pp.is_completed) AS is_pending_cnt
                FROM product_production_det ppd 
                LEFT JOIN product_production pp ON pp.id = ppd.refProductionId AND pp.is_deleted = 'Live'
                WHERE ppd.refProductId = '$product_id' AND pp.is_completed = 0 ";
        return $this->db->query($sql)->row()->is_pending_cnt;
    }

    function get_godown_data($postData) {
        $sql = $this->get_godown_data_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_godown_search_data($postData) {
        $sql = $this->get_godown_data_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_godown_data_query($postData) {
        $column_order = array('', 'a.godown_name', '');
        $sql = "SELECT a.* FROM (
                    SELECT pg.* 
                    FROM product_godown pg
                    WHERE pg.status = '1'
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.id DESC';
        }
        return $sql;
    }

    function get_product_list($postData, $product_id = '') {
        $sql = $this->get_product_list_query($postData, $product_id);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_product_list_search_data($postData, $product_id = '') {
        $sql = $this->get_product_list_query($postData, $product_id);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_product_list_query($postData, $product_id = '') {
        $column_order = array('', 'a.product_name', 'a.category_name', 'a.supplier_name', 'a.price', 'a.s_LandedCost', 'a.supplier_price', 's_opening_stock', '');
        $sql = "SELECT a.* FROM (
                   SELECT si.supplier_id, si.supplier_name, pi.*, sp.supplier_price, sp.opening_stock AS s_opening_stock, sp.LandedCost AS s_LandedCost, pc.category_name
                    FROM product_information pi
                    LEFT JOIN supplier_product sp ON sp.product_id = pi.id AND sp.status = '1'
                    LEFT JOIN supplier_information si ON si.supplier_id = sp.supplier_id AND si.status = '1'
                    LEFT JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                    WHERE pi.status = '1'";
        if (isset($product_id) && !empty($product_id)) {
            $sql .= " AND pi.id = '$product_id'";
        }
        $sql .= " ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
//        $sql .= " GROUP BY a.user_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.product_name DESC';
        }
//        preprint($sql);
        return $sql;
    }

    function get_product_group($postData) {
        $sql = $this->get_product_group_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_product_group_search_data($postData) {
        $sql = $this->get_product_group_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_product_group_query($postData) {
        $column_order = array('', 'a.product_group_name', '');
        $sql = "SELECT a.* FROM (
                    SELECT pg.* 
                    FROM product_group pg
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.id DESC';
        }
        return $sql;
    }

    function get_product_mfg($postData) {
        $sql = $this->get_product_mfg_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_product_mfg_search_data($postData) {
        $sql = $this->get_product_mfg_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_product_mfg_query($postData) {
        $column_order = array('', 'a.mfg_name', 'a.mfg_code', '');
        $sql = "SELECT a.* FROM (
                    SELECT pm.* 
                    FROM product_mfg pm
                    WHERE pm.status = '1'
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.id DESC';
        }
        return $sql;
    }

    function get_product_stage_list($postData) {
        $sql = $this->get_product_stage_list_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_product_stage_list_search_data($postData) {
        $sql = $this->get_product_stage_list_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_product_stage_list_query($postData) {
        $column_order = array('', 'a.product_stage_name', '');
        $sql = "SELECT a.* FROM (
                    SELECT ps.* 
                    FROM product_stage ps
                    WHERE ps.status = '1'
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.stage_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.stage_id DESC';
        }
        return $sql;
    }

    function get_raw_product_association($postData) {
        $sql = $this->get_raw_product_association_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
//        preprint($sql);
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_raw_product_association_search_data($postData) {
        $sql = $this->get_raw_product_association_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_raw_product_association_query($postData) {
        $column_order = array('', 'a.product_name', 'a.raw_product', 'a.Quantity');
        $sql = "SELECT a.* FROM (
                        SELECT pi.id, 
                        CONCAT(pi.product_name, ' - (',pc.category_name,')') as product_name,
                        CONCAT(pi2.product_name, ' - (',pc2.category_name,')')  as raw_product,
                        pra.Qty as Quantity
                        FROM product_raw_association pra 
                        INNER JOIN product_information pi ON pi.id = pra.ref_product_id 
                        INNER JOIN product_information pi2 ON pi2.id = pra.ref_raw_product_id
                        INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                        INNER JOIN product_category pc2 ON pc2.category_id = pi2.category_id 
                        WHERE 1 = 1
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 4)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.id DESC';
        }
        return $sql;
    }

    function getProductById($product_id) {
        return $this->db->select('*')->from('product_information')->where('id', $product_id)->get()->row();
    }

}
