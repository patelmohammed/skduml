<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounts extends CI_Model {

    public $todays_date;
    public $sub_total;
    public $Idtrack_link = 1;

    public function __construct() {
        parent::__construct();
        $this->todays_date = date("m-d-Y");
    }

    //Payment account finder
    public function accounts_name_finder($status = 1) {
        $this->db->select("*");
        $this->db->from('account');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result_array();
    }

    //tax entry
    public function tax_entry($data) {
        $this->db->select("*");
        $this->db->from('tax_information');
        $this->db->where('tax_name', $data['tax_name']);
        $query = $this->db->get()->num_rows();

        if ($query > 0) {
            return FALSE;
        } else {
            $this->db->insert('tax_information', $data);
            return true;
        }
    }

    //tax entry
    public function update_tax_data($data, $id) {

        $this->db->where('tax_id', $id);
        $this->db->update('tax_information', $data);
        return true;
    }

    //active_inactive_status
    function updateStatus($data, $tax_id) {
        $this->db->where('tax_id', $tax_id);
        $this->db->update('tax_information', $data);

        return true;
    }

    //customer_ledger ENTRY
    public function customer_ledger($data) {
        $this->db->insert('customer_ledger', $data);
        return true;
    }

    // supplier_ledger Entry
    public function supplier_ledger($data) {
        $this->db->insert('supplier_ledger', $data);
        return true;
    }

    //Following function will receive payment inflow & Outflow.
    public function pay_table($data, $account_table) {
        $this->db->insert($account_table, $data);
    }

    /// transaction sent pay and receipt table
    public function tan_pay_table($data) {
        $this->db->insert('payment_trans', $data);
    }

// receipt table
    public function receipt_table($data) {
        $this->db->insert('receipt_trans', $data);
    }

    //DRAWING ENTRY
    public function drawing_entry($data) {
        $this->db->insert('drawing_add', $data);
    }

    //TRANSACTION ENTRY
    public function expence_entry($data) {
        $this->db->insert('expence_add', $data);
    }

    //BANKING ENTRY
    public function banking_data_entry($data) {
        $this->db->insert('daily_banking_add', $data);
    }

    //BANKING ENTRY
    public function daily_closing_entry($data) {
        $this->db->insert('daily_closing', $data);
        $closing_id = $this->db->insert_id();
        return $closing_id;
    }

    // This function will find out all closing information of daily closing.
    public function accounts_closing_data() {
        $this->load->model('Reports');
        $last_closing_amount = $this->get_last_closing_amount();
        $cash_in = $this->cash_data_receipt();
        $cash_out = $this->cash_data();
//        $cash_in_hand = $cash_in - $cash_out;
        if (isset($last_closing_amount) && !empty($last_closing_amount)) {
            $last_closing_amount = number_format(end($last_closing_amount)['amount'], 2);
            $cash_in_hand = $cash_in - $cash_out;
//            $cash_in_hand = end($last_closing_amount)['amount'] + $cash_in - $cash_out;
        } else {
            $last_closing_amount = 0.00;
            $cash_in_hand = 0.00;
        }
        
//      echo $cash_in_hand;die();
        $company_info = $this->Reports->retrieve_company();
        return array(
            "last_day_closing" => $last_closing_amount,
            "cash_in" => $cash_in,
            "cash_out" => $cash_out,
            "company_info" => $company_info,
            "cash_in_hand" => $cash_in_hand,
        );
    }

//  public function accounts_closing_data() {
//        $last_closing_amount = $this->get_last_closing_amount();
//        $cash_in = $this->cash_data_receipt();
//        $cash_out = $this->cash_data();
////        $cash_in_hand = $last_closing_amount[0]['amount'] + $cash_in - $cash_out;
//        $cash_in_hand = $cash_in - $cash_out;
////        echo '<pre>';        print_r($cash_out);die();
//        return array(
//            "last_day_closing" => number_format($last_closing_amount[0]['amount'], 2, '.', ','),
//            "cash_in" => number_format($cash_in, 2, '.', ','),
//            "cash_out" => number_format($cash_out, 2, '.', ','),
//            "cash_in_hand" => number_format($cash_in_hand, 2, '.', ',')
//        );
//    }
    public function cash_data_receipt() {
        //-----------
        $cash = 0;
        $datse = date('Y-m-d');
        $this->db->select('transaction.*,sum(transaction.receipt_amount) as amount');
        $this->db->from('transaction');
        $this->db->where('transaction_type', 2);
        $this->db->where('date_of_transaction', $datse);
        $result_amount = $this->db->get();
        $amount = $result_amount->result_array();
        $cash += $amount[0]['amount'];
        return $cash;
    }

    public function cash_data() {
        //-----------
        $cash = 0;
        $sql = "SELECT sum(e.expense_amount) as amount
                FROM expense e
                WHERE e.status = '1' AND e.approval_status = 'Approved' AND e.expense_date LIKE '" . date('Y-m-d') . "%'";
        $result = $this->db->query($sql)->row();
        $cash += $result->amount;
        return $cash;
    }

    public function accounts_summary($type) {
        $this->db->select(' * ');
        $this->db->from('account');
        $this->db->where('status', $type);
        $query = $this->db->get();
        $accounts = array();
        $serial = 1;
        $total = 0;
        foreach ($query->result_array() as $result) {

            $table = $result['account_table_name'];

            $this->db->select("count(amount) as no_trans,sum(amount) as 'total'");
            $this->db->from($table);
            $this->db->where(array('date >=' => $this->todays_date));
            $result_account = $this->db->get();
            $account = $result_account->result_array();

            $no_trans = $account[0]['no_trans'];
            $total += $account[0]['total'];
            if ($no_trans > 0) {
                $gmdate = gmdate("m-d-Y", time() - (24 * 60 * 60));

                $link = base_url() . "Caccounts/summary_single/" . $gmdate . "/" . $this->todays_date . "/" . $table;

                $account_name = "<a href='" . $link . "' >" . $result['account_name'] . "</a>";

                $accounts[] = array("sl" => $serial, "table" => $account_name, "no_transaction" => $no_trans, "sub_total" => $account[0]['total']);
                $serial++;
            }
        }
        $this->sub_total = $total;

        return $accounts;
    }

    public function accounts_summary_datewise($start, $end, $type) {
        $this->db->select(' * ');
        $this->db->from('account');
        $this->db->where('status', $type);
        $query = $this->db->get();
        $accounts = array();
        $serial = 1;
        $total = 0;
        foreach ($query->result_array() as $result) {

            $table = $result['account_table_name'];

            $this->db->select("count(amount) as no_trans,sum(amount) as 'total'");
            $this->db->from($table);
            $this->db->where(array('date >=' => $start, 'date <=' => $end));
            $result_account = $this->db->get();
            $account = $result_account->result_array();

            $no_trans = $account[0]['no_trans'];
            $total += $account[0]['total'];
            if ($no_trans > 0) {
                $gmdate = gmdate("m-d-Y", time() - (24 * 60 * 60));

                $link = base_url() . "Caccounts/summary_single/" . $start . "/" . $end . "/" . $table;
                $account_name = "<a href='" . $link . "' >" . $result['account_name'] . "</a>";

                $accounts[] = array("sl" => $serial, "table" => $account_name, "no_transaction" => $no_trans, "sub_total" => $account[0]['total']);
                $serial++;
            }

            //echo $query=$this->db->last_query();
        }
        $this->sub_total = $total;

        return $accounts;
    }

    public function accounts_summary_details($start, $end, $table) {
        $this->db->select(" * ");
        $this->db->from($table);
        $this->db->where(array('date >=' => $start, 'date <=' => $end));
        // $this->db->where("status",1);
        $result_account = $this->db->get();
        $account = $result_account->result_array();
        $serial = 1;
        $data = array();
        foreach ($account as $account) {
            $date = substr($account['date'], 0, 10);

            if ($account['payment_type'] == 1) {
                $payment_type = "Cash";
            } elseif ($account['payment_type'] == 2) {
                $payment_type = "Cheque";
            } else {
                $payment_type = "Pay Order";
            }

            $data[] = array("sl" => $serial++, "table" => $table, "date" => $date, "transaction_id" => $account['transaction_id'], "tracing_id" => $this->idtracker($account['tracing_id']), "description" => $account['description'], "amount" => $account['amount'], "payment_type" => $payment_type);
        }

        return $data;
    }

    public function idtracker($id) {

        $this->db->select(" * ");
        $this->db->from('customer_information');
        $this->db->where(array('customer_id =' => $id));
        $result_account = $this->db->get();
        $account = $result_account->result_array();

        $afrows = $this->db->affected_rows();
        if ($afrows == 0) {
            $this->db->select(" * ");
            $this->db->from('supplier_information');
            $this->db->where(array('supplier_id =' => $id));
            $result_account = $this->db->get();
            $account = $result_account->result_array();
            $afrows = $this->db->affected_rows();

            if ($afrows == 0) {
                return $id;
            } else {

                $supplier = "<a href=" . base_url() . "Csupplier/supplier_ledger/" . $id . ">" . $account[0]['supplier_name'] . "</a>";
                if ($this->Idtrack_link == 1) {
                    return $supplier;
                } else {
                    return $account[0]['supplier_name'];
                }
            }
        } else {
            $customer = "<a href=" . base_url() . "Ccustomer/customerledger/" . $id . ">" . $account[0]['customer_name'] . "</a>";

            if ($this->Idtrack_link == 1) {
                return $customer;
            } else {
                return $account[0]['customer_name'];
            }
        }
    }

    public function in_out_del($transaction_id, $table) {
        if (substr($table, 0, 2) == "in") {
            $this->db->delete('customer_ledger', array('transaction_id' => $transaction_id));
        } else {
            $this->db->delete('supplier_ledger', array('transaction_id' => $transaction_id));
        }

        $this->db->delete($table, array('transaction_id' => $transaction_id));
    }

    //THis function will delete any data from table.
    public function data_del_table($table, $transaction_id) {
        $this->db->delete($table, array('transaction_id' => $transaction_id));
    }

    //THis function will delete any data from table.

    public function delete_all_table_data($table, $condation) {
        $this->db->delete($table, $condation);
    }

    public function inflow_edit($transaction_id, $table, $type) {
        $this->load->model('settings');

        $this->db->select(' * ');
        $this->db->from('customer_ledger');
        $this->db->where('transaction_id', $transaction_id);
        $query = $this->db->get();
        $result = $query->result_array();

        if ($result[0]["payment_type"] == 1) {
            $result[0]["payment"] = "Cash";
        } else {
            $result[0]["payment"] = "Cheque";
        }
        $this->Idtrack_link = 0; //0 means return will be without link.
        $result[0]["name"] = $this->idtracker($result[0]["customer_id"]);
        $result[0]["accounts"] = $this->table_name($type);
        $result[0]["selected"] = $this->table_name_finder($table);
        $result[0]["selected_table_id"] = $table;

        $result[0]["transaction_id"] = $transaction_id;

        $result[0]["bank"] = $this->settings->get_bank_list();

        //Banking selection
        $cheque_manager = $this->transaction_info($transaction_id, "cheque_manger", array("transaction_id" => $transaction_id));

        if ($cheque_manager) {
            $bank_name = $this->transaction_info($transaction_id, "bank_add", array("bank_id" => $cheque_manager[0]["bank_id"]));
            $result[0]["selected_bank_id"] = $cheque_manager[0]["bank_id"];
            $result[0]["selected_bank"] = $bank_name[0]["bank_name"];
        }
        return $result;
    }

    public function outflow_edit($transaction_id, $table, $type) {
        $this->load->model('settings');

        $this->db->select(' * ');
        $this->db->from('supplier_ledger');
        $this->db->where('transaction_id', $transaction_id);
        $query = $this->db->get();
        $result = $query->result_array();

        if ($result[0]["payment_type"] == 1) {
            $result[0]["payment"] = "Cash";
        } else {
            $result[0]["payment"] = "Cheque";
        }
        $this->Idtrack_link = 0; //0 means return will be without link.
        $result[0]["name"] = $this->idtracker($result[0]["supplier_id"]);
        $result[0]["accounts"] = $this->table_name($type);
        $result[0]["selected"] = $this->table_name_finder($table);
        $result[0]["selected_table_id"] = $table;
        $result[0]["transaction_id"] = $transaction_id;

        $result[0]["bank"] = $this->settings->get_bank_list();

        //Banking selection
        $cheque_manager = $this->transaction_info($transaction_id, "cheque_manger", array("transaction_id" => $transaction_id));
        if ($cheque_manager) {
            $bank_name = $this->transaction_info($transaction_id, "bank_add", array("bank_id" => $cheque_manager[0]["bank_id"]));
            $result[0]["selected_bank_id"] = $cheque_manager[0]["bank_id"];
            $result[0]["selected_bank"] = $bank_name[0]["bank_name"];
        }

        return $result;
    }

    ######### Cheque Manager  || data Retreive ########## 

    public function cheque_manager($limit, $page) {
        $this->db->select(' * ');
        $this->db->from('cheque_manger');
        $this->db->where('status', 1);
        $this->db->limit($limit, $page);
        $this->db->order_by('chq_given_date', 'desc');
        $query = $this->db->get();

        $result = $query->result_array();
//        preprint($result);
        // Rearrange the internal data.

        $i = 0;

        foreach ($result as $res) {
            $result[$i]["sl"] = $i + 1;
            $result[$i]["date"] = substr($res['chq_given_date'], 0, 10);
            $result[$i]["name"] = $this->idtracker($res['customer_id']);

            if ($res['cheque_status'] == "0") {
                $result[$i]["action"] = 1;
                $result[$i]["action_value"] = "No Clear";
            } else {
                $result[$i]["action"] = 0;
                $result[$i]["action_value"] = "Cleared";
            }

            $bank_name = $this->transaction_info("", "bank_add", array("bank_id" => $res["bank_id"]));
            $result[$i]["bank_name"] = $bank_name[0]["bank_name"];
            $i++;
        }
        return $result;
    }

    ######### Cheque Manager  || data Retreive ##########   

    public function table_name($type) {
        $this->db->select(' * ');
        $this->db->from('account');
        $this->db->where('status', $type);
        $query = $this->db->get();
        return $query->result_array();
    }

    // public function table_name()
    //  {
    //          return $this->db->select('account.*, transaction.*')   
    //          ->from('transaction')
    //          ->join('account', 'transaction.transaction_category = account.parent_id', 'left')
    //          ->order_by('transaction_category', 'desc')
    //          ->get()
    //          ->result();
    //  }

    public function table_name_finder($table_id) {
        $this->db->select(' * ');
        $this->db->from('accounts');
        $this->db->where('account_table_name', $table_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    ######### Common function for updating data ############  

    public function data_update($data, $table, $condation) {
        $this->db->update($table, $data, $condation);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    ##### ******* Update All *** &&&& End &&&&& #############

    public function transaction_info($transaction_id, $table, $condation) {
        $this->db->select(' * ');
        $this->db->from($table);
        $this->db->where($condation);
        $query = $this->db->get();
        return $result = $query->result_array();
    }

    public function get_last_closing_amount() {
        $sql = "SELECT amount FROM daily_closing WHERE date = (SELECT MAX(date) FROM daily_closing)";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function get_todays_draw_amount() {
        $this->db->select("sum(amount) as 'total_draw_amount'");
        $this->db->from('drawing_add');
        $this->db->where('date', $this->todays_date);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_todays_expense_amount() {
        $this->db->select("sum(amount) as 'total_expense_amount'");
        $this->db->from('expence_add');
        $this->db->where('date', $this->todays_date);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_todays_cheque_sales_amount() {
        $this->db->select("sum(amount) as 'total_chq_sales_amount'");
        $this->db->from('customer_ledger');
        $this->db->where(array('payment_type' => 2, 'date' => $this->todays_date));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_todays_cash_sales_amount() {
        $this->db->select("sum(amount) as 'total_cash_sales_amount'");
        $this->db->from('customer_ledger');
        $this->db->where(array('payment_type' => 1, 'date' => $this->todays_date));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_todays_cheque_to_cash() {
        $this->db->select("sum(amount) as 'cheque_to_cash_amount'");
        $this->db->from('daily_banking_add');
        $this->db->where(array('deposit_type' => "cheque", 'transaction_type' => "draw", 'date' => $this->todays_date));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function count_daily_closing_data() {
        $this->db->select('*');
        $this->db->from('daily_closing');
        $this->db->where('status', 1);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_closing_report($per_page = null, $page = null) {
        $this->db->select("* ,cash_in - cash_out as 'cash_in_hand'");
        $this->db->from('daily_closing');
        $this->db->where('status', 1);
        $this->db->order_by('date', 'desc');
        $this->db->limit($per_page, $page);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_closing_report_count() {
        $this->db->select("* ,cash_in - cash_out as 'cash_in_hand'");
        $this->db->from('daily_closing');
        $this->db->where('status', 1);
        $this->db->order_by('date', 'desc');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_date_wise_closing_report($from_date, $to_date,$per_page = null, $page = null) {
        $dateRange = "DATE(date) BETWEEN '$from_date' AND '$to_date'";
//        echo $dateRange;die();
        $this->db->select("* ,cash_in - cash_out as 'cash_in_hand'");
        $this->db->from('daily_closing');
        $this->db->where('status', 1);
        $this->db->where($dateRange, NULL, FALSE);
        $this->db->order_by('date', 'desc');
//        $this->db->limit($per_page, $page);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_date_wise_closing_report_count($from_date, $to_date) {

        $dateRange = "date BETWEEN '$from_date%' AND '$to_date%'";
        $this->db->select("* ,cash_in - cash_out as 'cash_in_hand'");
        $this->db->from('daily_closing');
        $this->db->where('status', 1);
        $this->db->where($dateRange, NULL, FALSE);
        $this->db->order_by('date', 'desc');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function insertChequePayorderData($data) {
        $this->db->insert('cheque_manger', $data);
        return true;
    }

    public function delete_tax($id) {

        $this->db->where('tax_id', $id);
        $this->db->update('tax_information', ['status' => 0]);
        return true;
    }

    public function checkExistingTaxName($tax_name, $id = '') {
        if ($id != '') {

            $result = $this->db->select('tax_name')
                    ->from('tax_information')
                    ->where('tax_name', $tax_name)
                    ->where('status', 1)
                    ->where('tax_id != ', $id)
                    ->get()
                    ->row('tax_name');
        } else {
            $result = $this->db->select('tax_name')
                    ->from('tax_information')
                    ->where('status', 1)
                    ->where('tax_name', $tax_name)
                    ->get()
                    ->row('tax_name');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

    function get_tax_list($postData) {
        $sql = $this->get_tax_list_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_tax_list_search_data($postData) {
        $sql = $this->get_tax_list_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_tax_list_query($postData) {
        $column_order = array('', 'a.tax_name', 'a.TaxPer', '');
        $sql = "SELECT a.* FROM (
                    SELECT ti.*, CONCAT(ti.tax, ' ', '%') AS TaxPer
                    FROM tax_information ti
                    WHERE ti.status = '1'
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.tax_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.tax_id';
        }
        return $sql;
    }

    function get_cheque_payment_list($postData) {
        $sql = $this->get_cheque_payment_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_cheque_payment_search_data($postData) {
        $sql = $this->get_cheque_payment_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_cheque_payment_query($postData) {
        $column_order = array('', 'a.DateTime', 'a.PINNO', 'a.supplier_name', 'a.product_count', 'a.GrandTotal', '');

        $sql = "SELECT a.* FROM
                        (SELECT cm.*, t.relation_id, t.transaction_type, t.transaction_category, si.supplier_name, ci.customer_name, b.bank_name
                        FROM cheque_manger cm
                            LEFT JOIN transaction t ON t.transaction_id = cm.transaction_id AND t.is_deleted = '1' AND t.is_transaction = '1'
                            LEFT JOIN supplier_information si ON si.supplier_id = t.relation_id AND si.status = '1' AND cm.transection_type = '1'
                            LEFT JOIN customer_information ci ON ci.customer_id = t.relation_id AND ci.status = '1' AND cm.transection_type = '2'
                            LEFT JOIN bank_add b ON b.bank_id = cm.bank_id AND b.status = '1'
                        WHERE cm.status = '1' AND cm.cheque_status = '0'
                        GROUP BY cm.cheque_id
                        ORDER BY cm.cheque_id ASC) a 
                WHERE 1 = 1";

        $column_cnt = count($column_order);

        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }

        $sql .= " GROUP BY a.cheque_id";

        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.cheque_id DESC';
        }

//        preprint($sql);
        return $sql;
    }

    public function authorizeCheque($trans_id) {
        $this->db->set('email', $new_email);
        $this->db->where('transaction_id', $trans_id);
        $this->db->update($table);
    }
}
