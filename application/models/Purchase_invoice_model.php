<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase_invoice_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function productInsert_pomst($data) {
        $this->db->select('user_information*');
        $this->db->from('user_information');
        $this->db->where('status', 1);
        $this->db->where('product_model', $data['product_model']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('product_information', $data);
            $this->db->select('*');
            $this->db->from('product_information');
            $this->db->where('status', 1);
            $query = $this->db->get();
            return TRUE;
        }
    }
 
    public function get_tax_list() {
        $CI = & get_instance();
        $sql = "SELECT * FROM tax_information";
        return $CI->db->query($sql)->result();
    }

    public function get_product_list() {
        $sql = "SELECT sp.supplier_price,pi.*,
                       (SELECT GROUP_CONCAT(ti.tax_name SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax_name,
                       (SELECT GROUP_CONCAT(ti.tax SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax,
                       (SELECT GROUP_CONCAT(ti.tax_id SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax_id 
                FROM product_information pi
                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
                WHERE pi.status = '1'
                GROUP BY pi.product_id 
                HAVING pi.product_id IS NOT NULL
                ORDER BY pi.product_id DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_employee_list() {
        $this->db->select('user_information.*');
        $this->db->from('user_information');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function insert_purchase_invoice($data) {

        $this->db->select('*');
        $this->db->from('purchase_invoice');
        $this->db->where('PINNO', $data['PINNO']);
        $this->db->where('DateTime >= ', FIN_YEAR_STRT_DATE);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('purchase_invoice', $data);
            return $this->db->insert_id();
        }
    }

    public function insert_product($product_id, $pinid) {
        foreach ($product_id as $key => $value) {
            $product_data['refPINID'] = $pinid;
            $product_data['refProductID'] = $_POST['product_id'][$key];
            $product_data['UnitAmt'] = $_POST['unit_price'][$key];
            $product_data['Qty'] = $_POST['quantity_amount'][$key];
            $product_data['Discount'] = $_POST['discount'][$key];
            $product_data['DiscountAmt'] = $_POST['discount_ind'][$key];
            $total_amt = $_POST['total_amt'][$key];
            $product_data['TotalAmt'] = $total_amt;
            $this->db->insert('purchase_invoice_item', $product_data);
            $POItemID = $this->db->insert_id();

            $tax_id = $_POST['Tax_id'][$key];
            $tax_per = $_POST['tax_per'][$key];
            $tax_name = $_POST['tax_name'][$key];
            $tax_amt = $_POST['single_tax_amt'][$key];
            if (!empty($tax_id) && isset($tax_id) && $tax_id != '' && $tax_id != ' ') {
                $tax_id_array = explode(',', $tax_id);
                $tax_per_array = explode(',', $tax_per);
                $tax_name_array = explode(',', $tax_name);
                $tax_amt_array = explode(',', $tax_amt);
                foreach ($tax_id_array as $key2 => $value2) {
                    $tax_data['refPINID'] = $pinid;
                    $tax_data['refPINItemID'] = $POItemID;
//                    $tax_data['refPIID'] = $_POST['product_inward_no'][$key];
                    $tax_data['refTaxID'] = $value2;
                    $tax_data['refTaxName'] = $tax_name_array[$key2];
                    $tax_per_new = $tax_per_array[$key2];
                    $tax_data['TaxPer'] = $tax_per_new;
                    $tax_data['TaxAmt'] = $tax_amt_array[$key2];
                    $this->db->insert('purchase_invoice_itemtaxdet', $tax_data);
                }
            }
        }
        return TRUE;
    }

    public function get_company($limit) {
        $this->db->select('*');
        $this->db->from('company_information');
        $this->db->limit($limit);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function purchase_invoice_list() {
        $sql = "SELECT pi.*,pii.* 
                FROM purchase_invoice pi 
                LEFT JOIN purchase_invoice_item pii ON pii.refPINID = pi.PINID 
                WHERE pi.status = '1' AND pii.status = '1'
                GROUP BY pi.PINID
                HAVING pi.PINID IS NOT NULL
                ORDER BY pi.PINID DESC";

        return $this->db->query($sql)->result();
    }

    public function all_purchase_invoice_list() {
        $sql = "SELECT pp.* 
                FROM purchase_invoice pp 
                WHERE pp.status = '1' ";
        return $this->db->query($sql)->result();
    }

    public function product_edit_data($PINID) {
        $sql = "SELECT pi.*, si.supplier_name, si.ref_state_id, si.contact_person, si.mobile as supplier_phone, si.supplier_email
                FROM purchase_invoice pi
                    LEFT JOIN supplier_information si ON si.supplier_id = pi.RefSupplierID AND si.status = '1'
                WHERE pi.PINID = '$PINID' AND pi.status = '1'";
        return $this->db->query($sql)->row_array();
    }

    public function invoice_purchase_list($PINID) {
//        $sql = "SELECT pini.*,
//        GROUP_CONCAT(DISTINCT pinit.refTaxID  SEPARATOR ',') AS Tax_id,
//        GROUP_CONCAT(DISTINCT pinit.refTaxName  SEPARATOR ',') AS tax_name,
//        GROUP_CONCAT(DISTINCT pinit.TaxPer  SEPARATOR ',') AS tax_per,
//        SUM(pinit.TaxAmt) AS TaxAmt,
//        GROUP_CONCAT(DISTINCT pi.PINO  SEPARATOR ',') AS PINO,
//        GROUP_CONCAT(DISTINCT pi.PIID  SEPARATOR ',') AS PIID
//                FROM purchase_invoice_item pini 
//                LEFT JOIN purchase_invoice_itemtaxdet pinit ON pinit.refPINItemID = pini.PINItemID 
//                LEFT JOIN product_inward pi ON pi.PIID = pini.refPIID 
//                WHERE pini.status = '1' AND pinit.status = '1' AND pi.status = '1' AND pini.refPINID = '$PINID' 
//                GROUP BY pini.PINItemID
//                HAVING pini.PINItemID IS NOT NULL ";

        $sql = "SELECT pi.*,pinw.PIID,
                GROUP_CONCAT(piit.refTaxID  SEPARATOR ',') AS Tax_id,
                GROUP_CONCAT(piit.refTaxName  SEPARATOR ',') AS tax_name,
                GROUP_CONCAT(piit.TaxPer  SEPARATOR ',') AS tax_per,
                SUM(piit.TaxAmt) AS TaxAmt
                FROM purchase_invoice pi
                INNER JOIN purchase_invoice_item pii ON pii.refPINID = pi.PINID
                INNER JOIN purchase_invoice_itemtaxdet piit ON piit.refPINItemID = pii.PINItemID
                INNER JOIN product_inward pinw ON pinw.PIID = pii.refPIID 
                WHERE pi.status = '1' AND pii.status = '1' AND piit.status = '1' AND pi.PINID = '$PINID'
                GROUP BY pii.refProductID";

        return $this->db->query($sql)->result();
    }

    public function purchase_invoice_product_details($invoice_id) {

        $sql = "SELECT pii.*,GROUP_CONCAT( piit.refTaxID SEPARATOR ',') AS Tax_id, GROUP_CONCAT( piit.refTaxName SEPARATOR ',') AS tax_name, GROUP_CONCAT( piit.TaxPer SEPARATOR ',') AS tax_per, GROUP_CONCAT( piit.TaxAmt SEPARATOR ',') AS tax_amt, SUM(piit.TaxAmt) AS TotalTaxAmt, GROUP_CONCAT(DISTINCT p.id  SEPARATOR ',') AS product_id, GROUP_CONCAT(DISTINCT p.serial_no  SEPARATOR ',') AS barcode
                FROM purchase_invoice_item pii
                LEFT JOIN purchase_invoice pi ON pi.PINID = pii.refPINID
                LEFT JOIN purchase_invoice_itemtaxdet piit ON pii.PINItemID = piit.refPINItemID
                LEFT JOIN product_information p ON pii.refProductID = p.id
                WHERE pii.status = '1' AND pii.refPINID = $invoice_id
                GROUP BY pii.PINItemID";
//        preprint($sql);
        return $this->db->query($sql)->result();
    }

    public function update_purchase_invoice($PINID, $data) {
        $this->db->where('PINID', $PINID);
        $this->db->update('purchase_invoice', $data);
        return TRUE;
    }

    public function deletePurchaseInvoiceItem($PINID, $table_name) {
        $this->db->where('refPINID', $PINID);
        $this->db->update($table_name, array('status' => '0'));
        return TRUE;
    }

    public function clearPurchaseInvoiceItem($PIID, $table_name) {
        $this->db->where('refPINID', $PIID);
        $this->db->delete($table_name);
        return TRUE;
    }

    public function delete_purchase_invoice($purchase_invoice_id) {
        $this->db->where('PINID', $purchase_invoice_id);
        $this->db->update('purchase_invoice', array('status' => '0'));
        $this->db->where('refPINID', $purchase_invoice_id);
        $this->db->update('purchase_invoice_item', array('status' => '0'));
        $this->db->where('refPINID', $purchase_invoice_id);
        $this->db->update('purchase_invoice_itemtaxdet', array('status' => '0'));
        $this->session->set_userdata(array('message' => 'Purchase invoice deleted successfully.'));

        $this->db->select('*');
        $this->db->from('purchase_invoice');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return true;
    }

    public function get_tax_detail() {
        $sql = "SELECT * FROM tax_information WHERE status='1' ";
        return $this->db->query($sql)->result();
    }

    public function purchase_invoice_list_for_print($PINID) {
//        $sql = "SELECT pini.*,GROUP_CONCAT(pinit.refTaxID SEPARATOR ',') AS Tax_id,GROUP_CONCAT(pinit.refTaxName SEPARATOR ',') AS tax_name,GROUP_CONCAT(pinit.TaxPer SEPARATOR ',') AS tax_per,SUM(pinit.TaxAmt) AS TaxAmt,pi.PINO
//            FROM purchase_invoice_item pini 
//            LEFT JOIN purchase_invoice_itemtaxdet pinit ON pinit.refPINItemID = pini.PINItemID 
//            LEFT JOIN product_inward pi ON pi.PIID = pini.refPIID 
//            WHERE pini.status = '1' AND pini.refPINID = '$PINID' 
//            GROUP BY pini.PINItemID";

        $sql = "SELECT pini.*,GROUP_CONCAT(DISTINCT pinit.refTaxID SEPARATOR ',') AS Tax_id,
                GROUP_CONCAT(DISTINCT pinit.refTaxName SEPARATOR ',') AS tax_name,
                GROUP_CONCAT(DISTINCT pinit.TaxPer SEPARATOR ',') AS tax_per,
                GROUP_CONCAT(DISTINCT pinit.TaxAmt SEPARATOR ',') AS tax_amt,
                SUM(DISTINCT pinit.TaxAmt) AS TotalTaxAmt ,pi.product_name
                FROM purchase_invoice_item pini 
                LEFT JOIN purchase_invoice_itemtaxdet pinit ON pinit.refPINItemID = pini.PINItemID AND pinit.status = 1
                LEFT JOIN product_information pi ON pi.id = pini.refProductID AND pi.status = 1
                WHERE pini.status = '1' AND pini.refPINID = $PINID 
                GROUP BY pini.PINItemID";
//        preprint($sql);
        return $this->db->query($sql)->result();
    }

    public function invoice_tax_detail_for_print($PIID) {
        $sql = "SELECT CONCAT(refTaxName,' ',TaxPer,'%') as taxname, SUM(TaxAmt) AS totaltax 
                FROM purchase_invoice_itemtaxdet 
                WHERE refPINID = '$PIID' AND status = '1' 
                GROUP BY refTaxID 
                ORDER BY refTaxName";
        return $this->db->query($sql)->result();
    }

    public function get_product_list_supllier($supplier_id) {
        $sql = "SELECT si.supplier_id,sp.supplier_price,pi.*,tp.tax_id,GROUP_CONCAT(ti.tax_name SEPARATOR ', ') AS tax_name,GROUP_CONCAT(ti.tax SEPARATOR ',') AS tax,GROUP_CONCAT(ti.tax_id SEPARATOR ',') AS tax_id  
                FROM product_information pi
                LEFT JOIN tax_product tp ON tp.product_id = pi.product_id
                LEFT JOIN tax_information ti ON ti.tax_id = tp.tax_id 
                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
                LEFT JOIN supplier_information si ON si.supplier_id = '$supplier_id' 
                WHERE sp.supplier_id = si.supplier_id AND pi.status = '1'
                GROUP BY pi.product_id 
                HAVING pi.product_id IS NOT NULL
                ORDER BY pi.product_id DESC";
//        return $this->db->query($sql)->result();
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getPurchaseInwardProductVise($product_id, $supplier_id) {
//        $sql = "SELECT pii.*,pi.* 
//                FROM product_inward_item pii 
//                LEFT JOIN product_inward pi ON pi.PIID = pii.refPIID  
//                WHERE pii.status = '1' AND pi.status = '1' 
//                AND pii.refProductID = '$product_id' AND pi.RefSupplierID = '$supplier_id' ";
//
//        $sql = "SELECT pinw.PIID, pp.PONo AS PONo, ppi.refPOID AS POID
//                FROM product_pomst pp
//                INNER JOIN product_pomst_item ppi ON ppi.refPOID = pp.POID
//                INNER JOIN product_pomst_itemtaxdet ppit ON ppit.refPOItemID = ppi.POItemID
//                INNER JOIN product_inward_item pinwi ON pinwi.refPOID = pp.POID
//                INNER JOIN product_inward pinw ON pinw.PIID = pinwi.refPIID
//                WHERE ppi.refProductID = '$product_id' AND PP.RefSupplierID = '$supplier_id'
//                GROUP BY pp.POID";

        $sql = "SELECT pi.PIID, pi.PINO
                FROM product_inward pi 
                INNER JOIN product_inward_item pii ON pii.refPIID = pi.PIID AND pii.status = '1'
                INNER JOIN product_inward_itemtaxdet piit ON piit.refPIItemID = pii.PIItemID AND piit.status = '1'
                WHERE pii.refProductID = '$product_id' AND pi.RefSupplierID = '$supplier_id' AND pi.status = '1'
                GROUP BY pi.PIID";

        $result = $this->db->query($sql)->result();
        return $result;
    }

    public function getInwardDataByPIID($inward_id, $product_id, $supplier_id) {

        $sql = "SELECT  pii.UnitAmt,
                GROUP_CONCAT(piit.refTaxName SEPARATOR ', ') AS tax_name,
                GROUP_CONCAT(piit.TaxPer SEPARATOR ',') AS tax,
                GROUP_CONCAT(piit.refTaxID SEPARATOR ',') AS tax_id,
                GROUP_CONCAT(DISTINCT pi.PIID SEPARATOR ',') AS PIID,
                GROUP_CONCAT(DIStINCT pi.PINO SEPARATOR ',') AS PINO, 
                pii.Qty AS purchase_inward_qty 
                FROM product_inward pi
                INNER JOIN product_inward_item pii ON pii.refPIID = pi.PIID AND pii.status = '1'
                LEFT JOIN product_inward_itemtaxdet piit ON piit.refPIItemID = pii.PIItemID AND piit.status = '1'
                WHERE pi.PIID = '$inward_id' AND pii.refProductID = '$product_id' AND pi.RefSupplierID = '$supplier_id' AND pi.status = '1'";

        $result = $this->db->query($sql)->result();

        return $result;
    }

    public function get_product_list_with_pi() {

//        $sql = "SELECT sp.supplier_price,pi.*,tp.tax_id,GROUP_CONCAT(DISTINCT ti.tax_name SEPARATOR ', ') AS tax_name,GROUP_CONCAT(DISTINCT ti.tax SEPARATOR ',') AS tax,GROUP_CONCAT(DISTINCT ti.tax_id SEPARATOR ',') AS tax_id,GROUP_CONCAT(DISTINCT pii.refPIID SEPARATOR ',') AS PIID,GROUP_CONCAT(DISTINCT pin.PINO SEPARATOR ',') AS PINO
//                FROM product_information pi
//                LEFT JOIN tax_product tp ON tp.product_id = pi.product_id
//                LEFT JOIN tax_information ti ON ti.tax_id = tp.tax_id 
//                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
//                LEFT JOIN product_inward_item pii ON pii.refProductID = pi.product_id
//                LEFT JOIN product_inward pin ON pin.PIID = pii.refPIID
//                GROUP BY pi.product_id 
//                HAVING pi.product_id IS NOT NULL
//                ORDER BY pi.product_id DESC";


        $sql = "SELECT sp.supplier_price,pi.*,CONCAT(pi.product_name, ' - (',pc.category_name,')') as product_name,
                       (SELECT GROUP_CONCAT(ti.tax_name SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.id AND tp.status = 1) AS tax_name,
                       (SELECT GROUP_CONCAT(ti.tax SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.id AND tp.status = 1) AS tax,
                       (SELECT GROUP_CONCAT(ti.tax_id SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.id AND tp.status = 1) AS tax_id 
                FROM product_information pi
                INNER JOIN supplier_product sp ON sp.product_id = pi.id
                INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                WHERE pi.status = '1'
                GROUP BY pi.id";

//        LEFT JOIN tax_product tp ON tp.product_id = pi.product_id
//                LEFT JOIN tax_information ti ON ti.tax_id = tp.tax_id 
//                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    function get_purchase_invoice($postData) {
        $sql = $this->get_purchase_invoice_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_purchase_invoice_search_data($postData) {
        $sql = $this->get_purchase_invoice_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_purchase_invoice_query($postData) {
        $column_order = array('', 'a.DateTime', 'a.PINNO', 'a.supplier_name','a.InvoiceDescription', 'a.product_count', 'a.GrandTotal','a.created_by','a.updated_by', '');
        $sql = "SELECT a.*,DATE_FORMAT(a.DateTime,'%d-%m-%Y %H:%i') AS DateTime FROM (
                    SELECT pi.DateTime, pi.PINID, pi.PINNO, pi.InvoiceDescription, si.supplier_name, (SELECT COUNT(PINItemID) FROM purchase_invoice_item WHERE refPINID = pi.PINID AND status='1') AS product_count, pi.GrandTotal, pi.PaidAmount, pi.DueAmount,u1.user_name as created_by, u2.user_name as updated_by
                    FROM purchase_invoice pi 
                    LEFT JOIN purchase_invoice_item pii ON pii.refPINID = pi.PINID AND pii.status = '1'
                    LEFT JOIN supplier_information si ON si.supplier_id = pi.RefSupplierID AND si.status = '1'
    				LEFT JOIN user_information u1 ON u1.id = pi.InsUser 
    				LEFT JOIN user_information u2 ON u2.id = pi.UpdUser 
                    WHERE pi.status = '1'
                    GROUP BY pi.PINID
                ) a WHERE 1=1";
        
        if (isset($postData['start_date']) && !empty($postData['start_date'])) {
//            $start_date = date_format(date_create($postData['start_date']), 'Y-m-d');
            $start_date = date('Y-m-d', strtotime($postData['start_date']));
            $sql .= " AND DATE(a.DateTime) >= '" . $start_date . "'";
        }
        
        if (isset($postData['end_date']) && !empty($postData['end_date'])) {
//            $end_date = date_format(date_create($postData['end_date']), 'Y-m-d');
            $end_date = date('Y-m-d', strtotime($postData['end_date']));
            $sql .= " AND DATE(a.DateTime) <= '" . $end_date . "'";
        }
        
        $column_cnt = count($column_order);
        
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.PINID";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.PINID DESC';
        }
//        preprint($sql);
        return $sql;
    }

    public function getProductInformationByProductId($product_id) {
        $sql = "SELECT pi.* FROM product_information pi WHERE pi.status = 1 AND pi.id = '$product_id'";
        return $this->db->query($sql)->row();
    }

    public function getLastTransactionId() {
        $sql = "SELECT sl.transaction_id FROM supplier_ledger sl 
                WHERE sl.status = 1
                ORDER BY sl.id DESC
                LIMIT 1";
        $result = $this->db->query($sql)->row();
        if (isset($result) && !empty($result)) {
            return $result->transaction_id;
        } else {
            return $result;
        }
    }
}
