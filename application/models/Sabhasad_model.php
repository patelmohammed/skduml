<?php

class Sabhasad_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkSabhasadName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND sabhasad_name != '$currentName'" : '');
        $sql = "SELECT sabhasad_name FROM tbl_sabhasad 
                WHERE sabhasad_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getSabhasadName($id) {
        $sql = "SELECT sabhasad_name FROM tbl_sabhasad
                WHERE del_status = 'Live'
                AND sabhasad_id  = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->sabhasad_name;
    }

    public function checkSabhasadNameEnglish($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND sabhasad_name_en != '$currentName'" : '');
        $sql = "SELECT sabhasad_name_en FROM tbl_sabhasad 
                WHERE sabhasad_name_en = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getSabhasadNameEnglish($id) {
        $sql = "SELECT sabhasad_name_en FROM tbl_sabhasad
                WHERE del_status = 'Live'
                AND sabhasad_id  = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->sabhasad_name_en;
    }

    public function checkSabhasadCode($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND sabhasad_code != '$currentName'" : '');
        $sql = "SELECT sabhasad_code FROM tbl_sabhasad 
                WHERE sabhasad_code = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getSabhasadCode($id) {
        $sql = "SELECT sabhasad_code FROM tbl_sabhasad
                WHERE del_status = 'Live'
                AND sabhasad_id  = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->sabhasad_code;
    }

    public function getSabhasadWalletBalance($sabhasad_id) {
        $sql = "SELECT IFNULL(SUM(cr.amount), 0) AS total_credit, (SELECT IFNULL(SUM(dr.total_payment), 0) FROM tbl_sabhasad_payment_debit dr WHERE dr.del_status = 'Live' AND dr.ref_sabhasad_id = $sabhasad_id) AS total_debit 
                FROM tbl_sabhasad_payment_credit cr
                WHERE cr.del_status = 'Live' AND cr.ref_sabhasad_id = $sabhasad_id";
        return $this->db->query($sql)->row();
    }

    public function getSabhasadCredit($startDate = NULL, $endDate = NULL) {
        $sql = "SELECT spc.*, s.sabhasad_id, s.sabhasad_name, s.sabhasad_name_en, s.sabhasad_code 
                FROM tbl_sabhasad_payment_credit spc 
                INNER JOIN tbl_sabhasad s ON s.sabhasad_id = spc.ref_sabhasad_id AND s.del_status = 'Live'
                WHERE spc.del_status = 'Live' ";
        if (isset($startDate) && !empty($startDate)) {
            $sql .= " AND DATE_FORMAT(spc.credit_date, '%Y-%m-%d') >= '" . date('Y-m-d', strtotime($startDate)) . "'";
        }
        if (isset($endDate) && !empty($endDate)) {
            $sql .= " AND DATE_FORMAT(spc.credit_date, '%Y-%m-%d') <= '" . date('Y-m-d', strtotime($endDate)) . "'";
        }
        $sql .= " ORDER BY spc.credit_date DESC";
        return $this->db->query($sql)->result();
    }

    public function getSabhasadDebit($startDate = NULL, $endDate = NULL) {
        $sql = "SELECT spd.*, s.sabhasad_id, s.sabhasad_name, s.sabhasad_name_en, s.sabhasad_code 
                FROM tbl_sabhasad_payment_debit spd 
                INNER JOIN tbl_sabhasad s ON s.sabhasad_id = spd.ref_sabhasad_id AND s.del_status = 'Live'
                WHERE spd.del_status = 'Live' ";
        if (isset($startDate) && !empty($startDate)) {
            $sql .= " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d') >= '" . date('Y-m-d', strtotime($startDate)) . "'";
        }
        if (isset($endDate) && !empty($endDate)) {
            $sql .= " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d') <= '" . date('Y-m-d', strtotime($endDate)) . "'";
        }
        $sql .= " ORDER BY spd.payment_date DESC";
        return $this->db->query($sql)->result();
    }

}
