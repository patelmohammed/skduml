<?php

class Common_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function check_for_delete_master($table_name, $column_name, $id, $status = NULL) {
        $sql = "SELECT COUNT(*) AS cnt FROM $table_name WHERE $column_name = '$id'";
        if ($status == 1) {
            $sql .= " AND status = '1' ";
        }
        $res = $this->db->query($sql)->row_array();
        return $res['cnt'];
    }

    public function insertInformation($data, $table_name) {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function insertBatchInformation($data, $table_name) {
        $this->db->trans_start();
        $this->db->insert_batch($table_name, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            return false;
        }
        return true;
    }

    public function updateInformation($data, $id, $table_name) {
        $this->db->where('id', $id);
        $this->db->update($table_name, $data);
    }

    public function updateInformation2($data, $column_name, $id, $table_name, $status = null, $column_name_2 = null, $id_2 = null) {
        if (!empty($status) && $status == 1) {
            $this->db->where('status', '1');
        }

        if (!empty($status) && $status == 'Live') {
            $this->db->where('del_status', 'Live');
        }

        $this->db->where($column_name, $id);
        if (!empty($column_name_2) && !empty($id_2)) {
            $this->db->where($column_name_2, $id_2);
        }

        $this->db->update($table_name, $data);
        return true;
    }

    public function deleteStatusChange($id, $table_name, $column_name) {
        $this->db->set('del_status', "Deleted");
        $this->db->where($column_name, $id);
        $this->db->update($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteRecord($id, $table_name, $column_name, $condition = null) {
        if (isset($condition) && !empty($condition)) {
            $where = explode('||', $condition);
            $this->db->where($where[0], $where[1]);
        }
        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteRecordUpdateStatus($id, $table_name, $column_name, $condition = null, $status = null) {
        if (isset($status) && !empty($status)) {
            $set = explode('||', $status);
            $this->db->set($set[0], $set[1]);
        }

        if (isset($condition) && !empty($condition)) {
            $where = explode('||', $condition);
            $this->db->where($where[0], $where[1]);
        }

        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getDataById($table_id, $id) {
        $sql = "SELECT * FROM $table_id WHERE del_status = 'Live' AND id = '$id' ";
        return $this->db->query($sql)->row();
    }

    public function getDataById2($table_id, $column_name, $id, $status = null, $column_name_2 = null, $id_2 = null) {
        $sql = "SELECT * FROM $table_id WHERE $column_name = '$id' ";
        if (isset($column_name_2) && !empty($column_name_2) && isset($id_2) && !empty($id_2)) {
            $sql .= " AND $column_name_2 = '$id_2' ";
        }
        if (isset($status) && !empty($status)) {
            if ($status == '1') {
                $sql .= " AND status = '1'";
            } elseif ($status == 'Live') {
                $sql .= " AND del_status = 'Live'";
            }
        }
        return $this->db->query($sql)->row();
    }

    public function getDataByIdStatus($table_id, $column_name, $id, $status = null) {
        $sql = "SELECT * FROM $table_id WHERE $column_name = '$id' ";
        if (isset($status) && !empty($status)) {
            $sql .= " AND $status";
        }
        return $this->db->query($sql)->row();
    }

    public function geAlldata($table_name, $orderBy = '') {
        $sql = "SELECT * FROM $table_name WHERE del_status = 'Live' $orderBy";
        return $this->db->query($sql)->result();
    }

    public function geAlldataById($table_name, $column_name_1 = null, $id_1 = null, $status = null, $column_name_2 = null, $id_2 = null) {
        $sql = "SELECT * FROM $table_name WHERE 1 = 1 ";
        if (isset($status) && !empty($status)) {
            $sql .= " AND $status ";
        } else {
            $sql .= " AND del_status = 'Live'";
        }

        if (isset($column_name_1) && !empty($column_name_1) && isset($id_1) && !empty($id_1)) {
            $sql .= " AND $column_name_1 = '$id_1' ";
        }
        if (isset($column_name_2) && !empty($column_name_2) && isset($id_2) && !empty($id_2)) {
            $sql .= " AND $column_name_2 = '$id_2' ";
        }
        return $this->db->query($sql)->result();
    }

    public function deleteInformation($column_name, $id, $table_name) {
        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        return true;
    }

    public function getCompanyInformation() {
        $sql = "SELECT * FROM company_information";
        return $this->db->query($sql)->row();
    }

    public function generateNoWithCompanyCode($column_name, $table_name) {
        $sql = "SELECT (IFNULL(MAX($column_name),0)+1)AS Number FROM $table_name WHERE status= '1' ";
        return $this->db->query($sql)->row()->Number;
    }

    public function getCompanyCode() {
        $sql = "SELECT company_code FROM company_information WHERE status = '1' LIMIT 1";
        return $this->db->query($sql)->row()->company_code;
    }

    public function chkUniqueCode($table_name, $column_name, $code, $status = '') {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where("$column_name", "$code");
        if (!empty($status) && $status == 1) {
            $this->db->where('status', '1');
        }
        if (!empty($status) && $status == 'Live') {
            $this->db->where('status', 'Live');
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function chkUniqueData($table_name, $column_name, $value, $status = '', $column_name_2 = '', $value_2 = '', $status_2 = '') {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where("$column_name", "$value");
        if (isset($status) && !empty($status)) {
            $this->db->where('del_status', 'Live');
        }

        if (isset($column_name_2) && !empty($column_name_2) && isset($value_2) && !empty($value_2)) {
            $this->db->where("$column_name_2", "$value_2");
        }
        if (isset($status_2) && !empty($status_2)) {
            if (!empty($status_2) && $status_2 == 1) {
                $this->db->where('status', '1');
            } elseif (!empty($status_2) && $status_2 == 'Live') {
                $this->db->where('status', 'Live');
            }
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function generateNoIndividual($column_name, $table_name, $primary_id, $status = null) {
        $sql = "SELECT (IFNULL(MAX($column_name),0)+1)AS Number FROM $table_name WHERE 1 = 1 ";
        if (isset($status) && !empty($status)) {
            $where = explode('||', $status);
            if (isset($where[0]) && isset($where[1]) && !empty($where[1]) && !empty($where[1])) {
                $sql .= " AND $where[0] = '$where[1]'";
            }
        }
        if (isset($primary_id) && !empty($primary_id)) {
            $sql .= " AND ref_outlet_id = '$primary_id'";
        }
        return $this->db->query($sql)->row()->Number;
    }

    public function GetBackgroundImage($page_id) {
        $sql = "SELECT * FROM background_img WHERE page_id = '$page_id' AND del_status= 'Live' ";
        return $this->db->query($sql)->result();
    }

    public function getMailSmtpSetting() {
        $sql = "SELECT * FROM smtp_setting WHERE del_status = 'Live' LIMIT 1";
        return $this->db->query($sql)->row();
    }

    public function resizeImage($sourceImage, $targetImage, $maxWidth, $maxHeight, $quality = 80) {
        list($origWidth, $origHeight, $type) = getimagesize($sourceImage);

        if ($type == 1) {
            header('Content-Type: image/gif');
            $image = imagecreatefromgif($sourceImage);
        } elseif ($type == 2) {
            header('Content-Type: image/jpeg');
            $image = imagecreatefromjpeg($sourceImage);
        } elseif ($type == 3) {
            header('Content-Type: image/png');
            $image = imagecreatefrompng($sourceImage);
        } else {
            header('Content-Type: image/x-ms-bmp');
            $image = imagecreatefromwbmp($sourceImage);
        }

        if ($maxWidth == 0) {
            $maxWidth = $origWidth;
        }

        if ($maxHeight == 0) {
            $maxHeight = $origHeight;
        }

// Calculate ratio of desired maximum sizes and original sizes.
        $widthRatio = $maxWidth / $origWidth;
        $heightRatio = $maxHeight / $origHeight;

// Ratio used for calculating new image dimensions.
        $ratio = min($widthRatio, $heightRatio);

// Calculate new image dimensions.
        $newWidth = (int) $origWidth * $ratio;
        $newHeight = (int) $origHeight * $ratio;

// Create final image with new dimensions.
// if($type==1 or $type==3)
// {
//    $newImage = imagefill($newImage,0,0,0x7fff0000);
// }

        $newImage = imagecreatetruecolor($newWidth, $newHeight);

        $transparent = imagecolorallocatealpha($newImage, 0, 0, 0, 127);
        imagefill($newImage, 0, 0, $transparent);
        imagesavealpha($newImage, true);

        imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
        imagepng($newImage, $targetImage);

// Free up the memory.
        imagedestroy($image);
        imagedestroy($newImage);

        return true;
    }

    public function check_menu_access($constant, $type, $login_type) {
        $data = array();
        $user_assigned_menus = $this->session->userdata(SITE_NAME . '_' . $login_type)['side_menu'];
        //echo '<pre>'; print_r($user_assigned_menus); die;
        $return = false;
        if (!empty($user_assigned_menus)) {
            foreach ($user_assigned_menus as $key => $value) {
                if ($value->menu_constant === $constant) {
                    if ($value->view_right >= 1 && $type == 'VIEW') {
                        $return = true;
                    }
                    if ($value->add_right >= 1 && $type == 'ADD') {
                        $return = true;
                    }
                    if ($value->edit_right >= 1 && $type == 'EDIT') {
                        $return = true;
                    }
                    if ($value->delete_right >= 1 && $type == 'DELETE') {
                        $return = true;
                    }
                }
            }
        }
        if (!$return) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(['res' => 'error', 'result' => 'error', 'msg' => 'Unauthorized access']);
                die;
            } else {
                redirect('admin/Auth/Unauthorized');
            }
        }
        return $return;
    }

    public function get_menu_rights($constant, $login_type) {
        $user_assigned_menus = $this->session->userdata(SITE_NAME . '_' . $login_type)['side_menu'];
        if (!empty($user_assigned_menus)) {
            foreach ($user_assigned_menus as $key => $value) {
                if ($value->menu_constant === $constant) {
                    return (array) $value;
                }
            }
        } else {
            redirect('admin/Auth/Unauthorized');
        }
    }

    public function getPageDataById() {
        $sql = "SELECT * 
                FROM";
    }

    public function getPageitemData($id, $is_active = null) {
        $sql = "SELECT * 
                FROM tbl_coaching_item_page tcip 
                WHERE tcip.ref_coaching_page_id = '$id' ";
        if (isset($is_active) && !empty($is_active)) {
            $sql .= " AND tcip.is_active = '$is_active' ";
        }
        return $this->db->query($sql)->result();
    }

    public function getCountryPageitemData($id, $is_active = null) {
        $sql = "SELECT * 
                FROM tbl_country_item_page tcip 
                WHERE tcip.ref_country_page_id = '$id' ";
        if (isset($is_active) && !empty($is_active)) {
            $sql .= " AND tcip.is_active = '$is_active' ";
        }
        return $this->db->query($sql)->result();
    }

    public function getVisaPageitemData($id, $is_active = null) {
        $sql = "SELECT * 
                FROM tbl_visa_item_page tcip 
                WHERE tcip.ref_visa_page_id = '$id' ";
        if (isset($is_active) && !empty($is_active)) {
            $sql .= " AND tcip.is_active = '$is_active' ";
        }
        return $this->db->query($sql)->result();
    }

    public function checkCoachingPage($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND coaching_page_name != '$currentName'" : '');
        $sql = "SELECT coaching_page_name FROM tbl_coaching_page 
                WHERE coaching_page_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getCoachingPage($id) {
        $sql = "SELECT coaching_page_name FROM tbl_coaching_page
                WHERE del_status = 'Live'
                AND coaching_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->coaching_page_name;
    }

    public function checkCoachingPageShortName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND coaching_page_short_name != '$currentName'" : '');
        $sql = "SELECT coaching_page_short_name FROM tbl_coaching_page 
                WHERE coaching_page_short_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getCoachingPageShortName($id) {
        $sql = "SELECT coaching_page_short_name FROM tbl_coaching_page
                WHERE del_status = 'Live'
                AND coaching_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->coaching_page_short_name;
    }

    public function checkCountryPage($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND country_page_name != '$currentName'" : '');
        $sql = "SELECT country_page_name FROM tbl_country_page 
                WHERE country_page_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getCountryPage($id) {
        $sql = "SELECT country_page_name FROM tbl_country_page
                WHERE del_status = 'Live'
                AND country_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->country_page_name;
    }

    public function checkCountryPageShortName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND country_page_short_name != '$currentName'" : '');
        $sql = "SELECT country_page_short_name FROM tbl_country_page 
                WHERE country_page_short_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getCountryPageShortName($id) {
        $sql = "SELECT country_page_short_name FROM tbl_country_page
                WHERE del_status = 'Live'
                AND country_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->country_page_short_name;
    }

    public function checkVisaPage($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND visa_page_name != '$currentName'" : '');
        $sql = "SELECT visa_page_name FROM tbl_visa_page 
                WHERE visa_page_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getVisaPage($id) {
        $sql = "SELECT visa_page_name FROM tbl_visa_page
                WHERE del_status = 'Live'
                AND visa_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->visa_page_name;
    }

    public function checkVisaPageShortName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND visa_page_short_name != '$currentName'" : '');
        $sql = "SELECT visa_page_short_name FROM tbl_visa_page 
                WHERE visa_page_short_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getVisaPageShortName($id) {
        $sql = "SELECT visa_page_short_name FROM tbl_visa_page
                WHERE del_status = 'Live'
                AND visa_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->visa_page_short_name;
    }

    public function getEmailTemplate($type) {
        $sql = "SELECT mt.* 
                FROM tbl_mail_template mt 
                WHERE mt.type = '$type' AND mt.del_status = 'Live' AND mt.active = 1 ";
        return $this->db->query($sql)->row();
    }

    public function getAllCountry() {
        $sql = "SELECT tcp.country_page_id, tcp.country_page_name, tcp.country_page_short_name, tcp.country_page_desc, tcp.home_page_desc, tcp.country_page_image, tcp.home_page_image, tcp.country_flag_image, tcp.background_image, tcp.is_active, tcp.is_show_home_page, tcp.del_status, '' AS continent_id, '' AS continent_name 
                FROM tbl_country_page tcp 
                WHERE tcp.del_status = 'Live' AND tcp.is_active = '1' AND tcp.continent_id IS NULL
                UNION(
                    SELECT '' AS country_page_id, '' AS country_page_name, '' AS country_page_short_name, '' AS country_page_desc, '' AS home_page_desc, '' AS country_page_image, '' AS home_page_image, '' AS country_flag_image, '' AS background_image, '' AS is_active, '' AS is_show_home_page, tc.del_status, tc.continent_id, tc.continent_name
                    FROM tbl_country_page tcp 
                    INNER JOIN tbl_continent tc ON tc.continent_id = tcp.continent_id AND tc.del_status = 'Live' 
                    WHERE tcp.del_status = 'Live' AND tcp.is_active = '1' AND tcp.continent_id IS NOT NULL
                    GROUP BY tcp.continent_id
                )";
        $result = $this->db->query($sql)->result();
        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {
                $result[$key]->sub_country = $this->getSubCountry($value->continent_id);
            }
        }
        return $result;
    }

    public function getSubCountry($continent_id) {
        $sql = "SELECT * 
                FROM tbl_country_page tcp 
                WHERE tcp.del_status = 'Live' AND tcp.is_active = '1' AND tcp.continent_id = '$continent_id' ";
        return $this->db->query($sql)->result();
    }

    public function send_otp_message($contact_number, $email_address = null) {
        $curl = curl_init();
        $url = "https://api.msg91.com/api/v5/otp?authkey=341380A94flRwVU5f5b3930P1";
        if (isset($email_address) && !empty($email_address)) {
            $url .= "&email=" . $email_address;
        }
        $url .= "&mobile=" . $contact_number . "&template_id=5f6302e863583b5a106feb47";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cookie: PHPSESSID=3kqe19dtvt86shurc2gu7vnh92"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function resend_otp($contact_number) {
        log_message('ERROR', $contact_number);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/v5/otp/retry?authkey=341380A94flRwVU5f5b3930P1&mobile=" . $contact_number . "&retrytype=voice",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cookie: PHPSESSID=3kqe19dtvt86shurc2gu7vnh92"
            ),
        ));

        $response = curl_exec($curl);
        log_message('ERROR', print_r($response, true));
        curl_close($curl);
        return $response;
    }

    public function otp_verify($contact_number, $otp) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/v5/otp/verify?otp=" . $otp . "&authkey=341380A94flRwVU5f5b3930P1&mobile=" . $contact_number,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cookie: PHPSESSID=3kqe19dtvt86shurc2gu7vnh92"
            ),
        ));

        $response = curl_exec($curl);
        log_message('ERROR', print_r($response, true));
        curl_close($curl);
        return $response;
    }

    public function getAlldataAPI($table_name, $select = "*", $orderBy = '') {
        $sql = "SELECT $select FROM $table_name WHERE del_status = 'Live' $orderBy";
        return $this->db->query($sql)->result();
    }

    public function getDataAPI($table_name, $whare, $select = "*", $orderBy = '') {
        $sql = "SELECT $select FROM $table_name WHERE del_status = 'Live' $whare $orderBy";
        return $this->db->query($sql)->row();
    }

    public function report_sabhasad($startDate, $endDate, $sabhasad_code) {

        $sabhasadData = $this->db->where("sabhasad_code", $sabhasad_code)->where("del_status", "Live")->get("tbl_sabhasad")->row();

        $sql = "SELECT a.* FROM (SELECT spc.credit_date AS paymentDate, spc.remark, '' AS milk_quantity, spc.amount AS amount, '' as milk_price, 'credit' AS payment_type
                    FROM tbl_sabhasad_payment_credit spc 
                    WHERE spc.del_status = 'Live' AND spc.ref_sabhasad_id = " . $sabhasadData->sabhasad_id . "
                UNION(
                    SELECT spd.payment_date AS paymentDate, '' AS remark, spd.milk_quantity AS milk_quantity, spd.total_payment AS amount, spd.milk_price, 'debit' AS payment_type
                        FROM tbl_sabhasad_payment_debit spd
                        WHERE spd.del_status = 'Live' AND spd.ref_sabhasad_id = " . $sabhasadData->sabhasad_id . "
                ) ) a WHERE DATE(a.paymentDate) >= '" . $startDate . "' AND DATE(a.paymentDate) <= '" . $endDate . "' ORDER BY a.paymentDate ASC";
        $historyData = $this->db->query($sql)->result();

        $opCreditBalance = $this->db->query("SELECT SUM(amount) as CreditAmt FROM tbl_sabhasad_payment_credit WHERE del_status = 'Live' AND DATE(credit_date) < '" . $startDate . "' AND ref_sabhasad_id = " . $sabhasadData->sabhasad_id)->row();
        $opDebitBalance = $this->db->query("SELECT SUM(total_payment) as DebitAmt FROM tbl_sabhasad_payment_debit WHERE del_status = 'Live' AND DATE(payment_date) < '" . $startDate . "' AND ref_sabhasad_id = " . $sabhasadData->sabhasad_id)->row();

        $openingBalance = $opCreditBalance->CreditAmt - $opDebitBalance->DebitAmt;

        return ["openingBalance" => $openingBalance, "historyData" => $historyData];
    }

    public function getBillNumber() {
        $sql = "SELECT (IFNULL(MAX(Convert(spd.bill_no, SIGNED)), 0) +1) AS bill_no
                FROM tbl_sabhasad_payment_debit spd 
                WHERE del_status = 'Live' ";
        return $this->db->query($sql)->row()->bill_no;
    }

    public function report_admin_total_qty($dt, $start_time, $end_time) {
        $sql = "SELECT SUM(milk_quantity) as qty FROM tbl_sabhasad_payment_debit WHERE del_status = 'Live' AND (payment_date BETWEEN '" . $dt . " " . $start_time . ":00' AND '" . $dt . " " . $end_time . ":00')";
        $res = $this->db->query($sql)->row();
        return !empty($res) ? ($res->qty != '' ? $res->qty : '0') : '0';
    }

    public function report_admin_total_sabhasad($dt, $start_time, $end_time) {
        $sql = "SELECT COUNT(*) as qty FROM tbl_sabhasad_payment_debit WHERE del_status = 'Live' AND (payment_date BETWEEN '" . $dt . " " . $start_time . ":00' AND '" . $dt . " " . $end_time . ":00')";
        $res = $this->db->query($sql)->row();
        return !empty($res) ? ($res->qty != '' ? $res->qty : '0') : '0';
    }

    public function checkPrintPending($sabhasad_id) {
        $sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM tbl_sabhasad_payment_debit spd WHERE spd.ref_sabhasad_id = $sabhasad_id AND spd.IsPrint = 0 ";
        $result = $this->db->query($sql)->row();
        return (isset($result->cnt) && !empty($result->cnt) ? $result->cnt : 0);
    }

    public function report_admin_sabhasad_by_data($dt, $start_time, $end_time) {
        $sql = "SELECT spd.milk_quantity, s.sabhasad_name, s.sabhasad_name_en, s.sabhasad_code 
                FROM tbl_sabhasad_payment_debit spd
                INNER JOIN tbl_sabhasad s ON s.sabhasad_id = spd.ref_sabhasad_id AND s.del_status = 'Live'
                WHERE spd.del_status = 'Live' AND (spd.payment_date BETWEEN '" . $dt . " " . $start_time . ":00' AND '" . $dt . " " . $end_time . ":00')
                GROUP BY spd.payment_id ";
        return $this->db->query($sql)->result();
    }

    public function report_admin_total_dtrange_qty($start_dt, $end_dt) {
        $sql = "SELECT SUM(milk_quantity) as qty FROM tbl_sabhasad_payment_debit WHERE del_status = 'Live' AND (IFNULL(UpdDateTime, InsDateTime) BETWEEN '" . $start_dt . " 00:00:00' AND '" . $end_dt . " 23:59:59')";
        $res = $this->db->query($sql)->row();
        return !empty($res) ? ($res->qty != '' ? $res->qty : '0.00') : '0.00';
    }

    public function report_admin_trans_by_date($start_dt, $end_dt) {
        $sql = "SELECT DATE((IFNULL(UpdDateTime, InsDateTime))) as trn_date, SUM(milk_quantity) as qty FROM tbl_sabhasad_payment_debit WHERE del_status = 'Live' AND (IFNULL(UpdDateTime, InsDateTime) BETWEEN '" . $start_dt . " 00:00:00' AND '" . $end_dt . " 23:59:59') GROUP BY DATE((IFNULL(UpdDateTime, InsDateTime))) ORDER BY trn_date ASC";
        return $this->db->query($sql)->result();
    }

    public function get_all_states() {
        $this->db->select('*');
        $this->db->from('states');
        $this->db->order_by('name');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCustomerList() {
        $sql = "SELECT ci.*, s.name as state_name 
                FROM customer_information ci
                    LEFT JOIN states s ON s.id = ci.ref_state_id 
                WHERE ci.is_deleted = '1' AND ci.active_status = '1'
                ORDER BY ci.customer_id ASC";
        return $this->db->query($sql)->result_array();
    }

    public function getSupplierDataByid($supplier_id) {
        $sql = "SELECT s.*, st.name AS state_name, st.code_int AS state_code_int, st.code_str AS state_code_str
                FROM supplier_information s 
                    LEFT JOIN states st ON st.id = s.ref_state_id
                WHERE s.supplier_id = '$supplier_id' AND s.status = '1'";
        $result = $this->db->query($sql)->row_array();
        if ($result == NULL) {
            $result = 0;
        }
        return $result;
    }

    public function getBankDetails() {
        $sql = "SELECT b.* FROM bank_add b WHERE b.bank_id = " . DEFAULT_BANK_ID;
        return $this->db->query($sql)->row_array();
    }

    public function get_all_users() {
        $this->db->select('*');
        $this->db->from('user_information');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insertData($table_name, $data) {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function updateData($table_name, $column_name, $id, $data, $status = '') {

        if (!empty($status) && $status === 1) {
            $this->db->where('status', '1');
        }

        if (!empty($status) && $status === 'Live') {
            $this->db->where('is_deleted', 'Live');
        }

        $this->db->where($column_name, $id);
        $this->db->update($table_name, $data);
        return TRUE;
    }

    public function deleteUpdate($column_name, $id, $table_name) {
        $this->db->where($column_name, $id);
        $this->db->update($table_name, array('is_deleted' => 'Deleted'));
    }

    public function insertDataInBatch($table_name, $data) {
        $this->db->insert_batch($table_name, $data);
        return $this->db->insert_id();
    }

    public function deleteData($table_name, $column_name, $id, $status = '') {
        if (!empty($status) && isset($status)) {
            if ($status == '1') {
                $this->db->where('status', '1');
            }
            if ($status == 'Live') {
                $this->db->where('is_deleted', 'Live');
            }
        }
        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        $this->session->set_userdata(array('message' => 'Data deleted successfully.'));
        return true;
    }

    public function updateData2($table_name, $column_name, $id, $column_name_2 = '', $id_2 = '', $data, $status = '') {

        if (!empty($status) && $status == 1) {
            $this->db->where('status', '1');
        }

        if (!empty($status) && $status == 'Live') {
            $this->db->where('is_deleted', 'Live');
        }

        $this->db->where($column_name, $id);
        if (!empty($column_name_2) && !empty($id_2)) {
            $this->db->where($column_name_2, $id_2);
        }
        $this->db->update($table_name, $data);
        return TRUE;
    }

    public function deleteUpdate2($column_name, $id, $table_name, $status) {
        if ($status == 'Deleted') {
            $status_column = 'is_deleted';
        }
        if ($status == 0) {
            $status_column = 'status';
        }
        $this->db->where($column_name, $id);
        $this->db->update($table_name, array("$status_column" => "$status"));
        $this->session->set_userdata(array('message' => display('successfully_delete')));
    }

    public function deleteUpdate3($table_name, $status, $column_name_1, $id_1, $column_name_2 = '', $id_2 = '') {
        if ($status === 'Deleted') {
            $status_column = 'is_deleted';
//            echo '1';
        }
        if ($status === 0) {
            $status_column = 'status';
//            echo '2';
        }
        $this->db->where($column_name_1, $id_1);
        if (isset($column_name_2) && !empty($column_name_2) && isset($id_2) && !empty($id_2)) {
            $this->db->where($column_name_2, $id_2);
        }
        $this->db->update($table_name, array("$status_column" => "$status"));
        return true;
    }
    
    public function get_company_detail() {
        $sql = "SELECT * FROM company_information WHERE status='1' ";
        return $this->db->query($sql)->row_array();
    }

    //Retrieve company Edit Data
    public function retrieve_company() {
        $this->db->select('*');
        $this->db->from('company_information');
        $this->db->limit('1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function getPaymentModeList() {
        $sql = "SELECT pm.*
                FROM payment_mode_list pm
                WHERE pm.status = 1 AND pm.active_status = 1
                ORDER BY pm.payment_mode_name ASC";
        return $this->db->query($sql)->result();
    }

    public function getBankList() {
        $sql = "SELECT b.* 
                FROM bank_add b 
                WHERE b.status = 1 AND b.active_status = 1";
        return $this->db->query($sql)->result();
    }
    
    public function getTaxList() {
        $sql = "SELECT ti.*
                FROM tax_information ti
                WHERE ti.status = 1 AND ti.active_status = 1
                GROUP BY ti.tax_id
                ORDER BY ti.tax ASC";
        return $this->db->query($sql)->result();
    }
    
    public function getCustomerDataByid($customer_id) {
        $sql = "SELECT ci.*, s.name AS state_name, s.code_int AS state_code_int, s.code_str AS state_code_str
                FROM customer_information ci 
                    LEFT JOIN states s ON s.id = ci.ref_state_id
                WHERE ci.customer_id = '$customer_id'";
        $result = $this->db->query($sql)->row_array();
        if ($result == NULL) {
            $result = 0;
        }
        return $result;
    }
}
