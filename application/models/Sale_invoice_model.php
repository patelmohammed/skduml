<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sale_invoice_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function productInsert_pomst($data) {
        $this->db->select('*');
        $this->db->from('product_information');
        $this->db->where('status', 1);
        $this->db->where('product_model', $data['product_model']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('product_information', $data);
            $this->db->select('*');
            $this->db->from('product_information');
            $this->db->where('status', 1);
            $query = $this->db->get();
            return TRUE;
        }
    }

    public function get_tax_list() {
        $CI = & get_instance();
        $sql = "SELECT * FROM tax_information";
        return $CI->db->query($sql)->result();
    }

    public function get_product_list() {
        $sql = "SELECT sp.supplier_price,pi.*,
                       (SELECT GROUP_CONCAT(ti.tax_name SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax_name,
                       (SELECT GROUP_CONCAT(ti.tax SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax,
                       (SELECT GROUP_CONCAT(ti.tax_id SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax_id 
                FROM product_information pi
                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
                WHERE pi.status = '1'
                GROUP BY pi.product_id 
                HAVING pi.product_id IS NOT NULL
                ORDER BY pi.product_id DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_employee_list() {
        $this->db->select('users.*');
        $this->db->from('users');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function insert_sale_invoice($data) {

        $this->db->select('*');
        $this->db->from('sale_invoice');
        $this->db->where('SINO', $data['SINO']);
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('sale_invoice', $data);
            return $this->db->insert_id();
        }
    }

    public function insert_product($product_id, $SIID) {
        if (isset($product_id) && !empty($product_id)) {
            foreach ($product_id as $key => $value) {
                $product_data['refSIID'] = $SIID;
                $product_data['refProductID'] = $_POST['product_id'][$key];

                $UnitAmt = (isset($_POST['unit_price'][$key]) && !empty($_POST['unit_price'][$key]) ? $_POST['unit_price'][$key] : 0);
                $product_data['UnitAmt'] = $UnitAmt;

                $Qty = (isset($_POST['quantity_amount'][$key]) && !empty($_POST['quantity_amount'][$key]) ? $_POST['quantity_amount'][$key] : 0);
                $product_data['Qty'] = $Qty;

                $product_data['DiscountPer'] = isset($_POST['discount'][$key]) && !empty($_POST['discount'][$key]) ? $_POST['discount'][$key] : 0;
                $dicount_amt = isset($_POST['discount_ind'][$key]) && !empty($_POST['discount_ind'][$key]) ? $_POST['discount_ind'][$key] : 0;
                $product_data['DiscountAmt'] = $dicount_amt;

                $total_amt = (isset($_POST['total_amt'][$key]) && !empty($_POST['total_amt'][$key]) ? $_POST['total_amt'][$key] : ($UnitAmt * $Qty) - $dicount_amt);
                $product_data['TotalAmt'] = $total_amt;

                $this->db->insert('sale_invoice_item', $product_data);
                $POItemID = $this->db->insert_id();

                $tax_id = $_POST['Tax_id'][$key];
                $tax_per = $_POST['tax_per'][$key];
                $tax_name = $_POST['tax_name'][$key];
                if (!empty($tax_id) && isset($tax_id) && $tax_id != '' && $tax_id != ' ') {
                    $tax_id_array = explode(',', $tax_id);
                    $tax_per_array = explode(',', $tax_per);
                    $tax_name_array = explode(',', $tax_name);
                    foreach ($tax_id_array as $key2 => $value2) {
                        $tax_data['refSIID'] = $SIID;
                        $tax_data['refSIItemID'] = $POItemID;
                        $tax_data['refTaxID'] = $value2;
                        $tax_data['refTaxName'] = $tax_name_array[$key2];
                        $tax_per_new = $tax_per_array[$key2];
                        $tax_data['TaxPer'] = $tax_per_new;
                        $tax_data['TaxAmt'] = ($total_amt * $tax_per_new) / 100;
                        $this->db->insert('sale_invoice_itemtaxdet', $tax_data);
                    }
                }
            }
            return TRUE;
        }
    }

    public function get_company($limit) {
        $this->db->select('*');
        $this->db->from('company_information');
        $this->db->limit($limit);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function purchase_invoice_list() {
        $sql = "SELECT si.*,sii.* 
                FROM sale_invoice si 
                LEFT JOIN sale_invoice_item sii ON sii.refSIID = si.SIID
                WHERE si.status = '1' AND sii.status = '1' 
                GROUP BY si.SIID 
                HAVING si.SIID IS NOT NULL
                ORDER BY si.SIID DESC";
        return $this->db->query($sql)->result();
    }

    public function all_purchase_invoice_list() {
        $sql = "SELECT si.* 
                FROM sale_invoice si 
                WHERE si.status = '1' ";
        return $this->db->query($sql)->result();
    }

    public function product_edit_data($SIID) {
        $sql = "SELECT si.*, ci.company__name, ci.ref_state_id, s.name AS state_name
                FROM sale_invoice si 
                    INNER JOIN customer_information ci ON ci.customer_id = si.RefCustomerID AND ci.is_deleted = '1'
                    LEFT JOIN states s ON s.id = ci.ref_state_id
                WHERE si.SIID = $SIID AND si.status = '1'";
        return $this->db->query($sql)->row_array();
    }

    public function invoice_purchase_list($SIID) {
        $sql = "SELECT sii.*,GROUP_CONCAT(siit.refTaxID  SEPARATOR ',') AS Tax_id,GROUP_CONCAT(siit.refTaxName  SEPARATOR ',') AS tax_name,GROUP_CONCAT(siit.TaxPer  SEPARATOR ',') AS tax_per,SUM(siit.TaxAmt) AS TaxAmt
                FROM sale_invoice_item sii 
                LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = sii.SIItemID AND siit.status = '1'
                WHERE sii.refSIID = $SIID
                GROUP BY sii.SIItemID
                HAVING sii.SIItemID IS NOT NULL ";
        return $this->db->query($sql)->result();
    }

    public function update_sale_invoice($SIID, $data) {
        $this->db->where('SIID', $SIID);
        $this->db->update('sale_invoice', $data);
        return TRUE;
    }

    public function deleteSaleInvoiceItem($SIID, $table_name) {
        $this->db->where('refSIID', $SIID);
        $this->db->update($table_name, array('status' => '0'));
        return TRUE;
    }

    public function delete_purchase_invoice($sale_invoice_id) {
        $this->db->select('SIID');
        $this->db->from('sale_invoice');
        $this->db->where('SIID', $sale_invoice_id);
        $this->db->where('status', '1');
        $query = $this->db->get();
        $affected_row = $this->db->affected_rows();

        if ($affected_row > 0) {
            $this->db->where('SIID', $sale_invoice_id);
            $this->db->update('sale_invoice', array('status' => '0'));
            $this->db->where('refSIID', $sale_invoice_id);
            $this->db->update('sale_invoice_item', array('status' => '0'));
            $this->db->where('refSIID', $sale_invoice_id);
            $this->db->update('sale_invoice_itemtaxdet', array('status' => '0'));
            $this->session->set_userdata(array('message' => 'Sale invoice deleted successfully.'));

            $this->db->select('*');
            $this->db->from('sale_invoice');
            $this->db->where('status', '1');
            $query = $this->db->get();
            return true;
        } else {
            $this->session->set_userdata(array('error_message' => 'Sale invoice not found!'));
            return false;
        }
    }

    public function get_tax_detail() {
        $sql = "SELECT * FROM tax_information WHERE status='1' ";
        return $this->db->query($sql)->result();
    }

    public function purchase_invoice_list_for_print($SIID) {
        $sql = "SELECT sii.SIItemID, sii.refSIID, sii.refProductID, pi.product_name, pi.unit, AVG(sii.UnitAmt) AS UnitAmt, SUM(sii.TotalAmt) AS TotalAmt, SUM(sii.Qty) AS Qty, 
                    AVG(sii.DiscountPer) AS DiscountPer, SUM(sii.DiscountAmt) AS DiscountAmt,(siit.refTaxID) AS Tax_id, (siit.refTaxName) AS tax_name, (siit.TaxPer) AS tax_per, SUM(siit.TaxAmt) AS TaxAmt 
                FROM sale_invoice_item sii 
                    INNER JOIN product_information pi ON pi.id = sii.refProductID 
                    LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = sii.SIItemID AND siit.status = '1' 
                WHERE sii.refSIID = $SIID AND sii.status = '1'
                GROUP BY sii.refProductID";
//        preprint($sql);
        return $this->db->query($sql)->result();
    }

    public function invoice_tax_detail_for_print($SIID) {
        $sql = "SELECT siit.refTaxName as taxname, siit.TaxPer as tax_percentage, SUM(siit.TaxAmt) AS totaltax 
                FROM sale_invoice_itemtaxdet siit
                WHERE siit.refSIID = '$SIID' AND siit.status = '1' 
                GROUP BY siit.refTaxID 
                ORDER BY siit.refTaxName";
        return $this->db->query($sql)->result();
    }

    public function get_product_list_supllier($supplier_id) {
        $sql = "SELECT si.supplier_id,sp.supplier_price,pi.*,tp.tax_id,GROUP_CONCAT(ti.tax_name SEPARATOR ', ') AS tax_name,GROUP_CONCAT(ti.tax SEPARATOR ',') AS tax,GROUP_CONCAT(ti.tax_id SEPARATOR ',') AS tax_id  
                FROM product_information pi
                LEFT JOIN tax_product tp ON tp.product_id = pi.product_id
                LEFT JOIN tax_information ti ON ti.tax_id = tp.tax_id 
                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
                LEFT JOIN supplier_information si ON si.supplier_id = '$supplier_id' 
                WHERE sp.supplier_id = si.supplier_id AND pi.status = '1' 
                GROUP BY pi.product_id 
                HAVING pi.product_id IS NOT NULL
                ORDER BY pi.product_id DESC";
//        return $this->db->query($sql)->result();
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getProductInformationByProductId($product_id) {
        $sql = "SELECT * 
                FROM product_information p
                WHERE p.id = $product_id AND p.status = '1'";
        return $this->db->query($sql)->row_array();
    }

    public function get_product_list_with_so() {
        $sql = "SELECT sp.supplier_price,pi.*,CONCAT(pi.product_name, ' - (',pc.category_name,')') as product_name,
                       (SELECT GROUP_CONCAT(ti.tax_name SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.id AND tp.status = 1) AS tax_name,
                       (SELECT GROUP_CONCAT(ti.tax SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.id AND tp.status = 1) AS tax,
                       (SELECT GROUP_CONCAT(ti.tax_id SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.id AND tp.status = 1) AS tax_id 
                FROM product_information pi
                    LEFT JOIN tax_product tp ON tp.product_id = pi.id
                    LEFT JOIN tax_information ti ON ti.tax_id = tp.tax_id 
                    LEFT JOIN supplier_product sp ON sp.product_id = pi.id
                    INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                WHERE pi.status = '1'
                GROUP BY pi.id 
                HAVING pi.id IS NOT NULL
                ORDER BY pi.id ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_customer_list() {
        $this->db->select('customer_information.*, states.name AS state_name');
        $this->db->from('customer_information');
        $this->db->join('states', 'states.id = customer_information.ref_state_id', 'left');
        $this->db->where('active_status', 1);
        $this->db->where('is_deleted', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function getSaleOutwardDataBySOID($SOID, $product_id, $customer_id) {

        $sql = "SELECT soi.UnitAmt,
                GROUP_CONCAT( soit.refTaxName SEPARATOR ', ') AS tax_name,
                GROUP_CONCAT( soit.TaxPer SEPARATOR ',') AS tax,
                GROUP_CONCAT(soit.refTaxID SEPARATOR ',') AS tax_id,
                GROUP_CONCAT(DISTINCT soi.refSOID SEPARATOR ',') AS SOID,
                GROUP_CONCAT(DISTINCT so.SONO SEPARATOR ',') AS SONO, 
                soi.Qty AS sale_outward_qty 
                FROM sale_outward so
                INNER JOIN sale_outward_item soi ON soi.refSOID = so.SOID AND soi.refProductID = '$product_id' AND soi.status = '1'
                LEFT JOIN sale_outward_itemtaxdet soit ON soit.refSOItemID = soi.SOItemID AND soit.status = '1'
                WHERE so.SOID = '$SOID' AND so.status = '1' AND so.RefCustomerID = '$customer_id'";

        $result = $this->db->query($sql)->result();

        return $result;
    }

    function get_sale_invoice_list($postData, $invoice_type) {
        $sql = $this->get_sale_invoice_list_query($postData, $invoice_type);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
//        preprint($sql);
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_sale_invoice_list_search_data($postData, $invoice_type) {
        $sql = $this->get_sale_invoice_list_query($postData, $invoice_type);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_sale_invoice_list_query($postData, $invoice_type) {
        $column_order = array('', 'a.DateTime', 'a.SINO', 'a.company__name', 'a.ProductCount', 'a.GrandTotal', 'a.created_by', 'a.updated_by', '');
        $sql = "SELECT a.*,DATE_FORMAT(a.DateTime, '%d-%m-%Y %H:%i') AS DateTime FROM (
                    SELECT si.DateTime, si.SIID, si.SINO, si.InvoiceType, ci.customer_name, ci.company__name, (SELECT COUNT(SIItemID) FROM sale_invoice_item WHERE refSIID = si.SIID) AS ProductCount, si.GrandTotal, ui.user_name as created_by, u.user_name as updated_by
                    FROM sale_invoice si 
                        INNER JOIN sale_invoice_item sii ON sii.refSIID = si.SIID
                        INNER JOIN customer_information ci ON ci.customer_id = si.RefCustomerID AND ci.is_deleted = '1'
                        LEFT JOIN user_information ui ON ui.id = si.InsUser 
                        LEFT JOIN user_information u ON u.id = si.UpdUser
                    WHERE si.status = '1' AND sii.status = '1' 
                    GROUP BY si.SIID 
                    HAVING si.SIID IS NOT NULL
                    ORDER BY si.SIID DESC
                ) a WHERE 1=1";

        if (isset($invoice_type) && !empty($invoice_type)) {
            $sql .= " AND a.InvoiceType = $invoice_type";
        }

        if (isset($postData['start_date']) && !empty($postData['start_date'])) {
            $start_date = date_format(date_create($postData['start_date']), 'Y-m-d');
            $sql .= " AND a.DateTime >= '" . $start_date . "'";
        }

        if (isset($postData['end_date']) && !empty($postData['end_date'])) {
            $end_date = date_format(date_create($postData['end_date']), 'Y-m-d');
            $sql .= " AND a.DateTime <= '" . $end_date . "'";
        }

        $column_cnt = count($column_order);

        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.SIID";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.SIID DESC';
        }
        return $sql;
    }

    public function generateSaleInvoiceNo() {
        $sql = "SELECT si.SINO 
                FROM sale_invoice si 
                WHERE si.InvoiceType = 1 AND si.SINO LIKE '" . COMPANY_SHORT_NAME . "-%'
                ORDER BY si.SIID DESC LIMIT 1";
        $cnt = $this->db->query($sql)->row();
        if (!empty($cnt)) {
            return $cnt->SINO;
        }
    }

    public function checkSaleInvoiceNo($SINO, $current_invoice_no = null) {
        $start_date = date('Y-m-d H:i:s', strtotime(FIN_YEAR_STRT_DATE));
        $end_date = date('Y-m-d H:i:s', strtotime(FIN_YEAR_END_DATE));

        $sql = "SELECT si.* 
                FROM sale_invoice si
                WHERE si.SINO = '$SINO' AND si.DateTime >= '$start_date' AND si.DateTime <= '$end_date' AND si.status = '1'";

        if (isset($current_invoice_no) && !empty($current_invoice_no)) {
            $sql .= " AND si.SINO != '$current_invoice_no'";
        }

        $result = $this->db->query($sql)->num_rows();
        if ($result > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getSaleInvoiceNo($sale_invoice_id) {
        $sql = "SELECT si.SINO
                FROM sale_invoice si
                WHERE si.SIID = '$sale_invoice_id' AND si.status = '1'
                LIMIT 1";
        $result = $this->db->query($sql)->row();
        return $result->SINO;
    }

    public function generateNonGSTSaleInvoiceNo() {
        $sql = "SELECT si.SINO 
                FROM sale_invoice si 
                WHERE si.InvoiceType = 2 AND si.SINO LIKE '" . NON_GST_INVOICE_SHORT_CODE . "-%'
                ORDER BY si.SIID DESC LIMIT 1";
        $cnt = $this->db->query($sql)->row();
        if (!empty($cnt)) {
            return $cnt->SINO;
        }
    }

    public function insert_non_gst_product($product_id, $SIID) {
        if (isset($product_id) && !empty($product_id)) {
            foreach ($product_id as $key => $value) {
                $product_data['refSIID'] = $SIID;
                $product_data['refProductID'] = $_POST['product_id'][$key];

                $UnitAmt = (isset($_POST['unit_price'][$key]) && !empty($_POST['unit_price'][$key]) ? $_POST['unit_price'][$key] : 0);
                $product_data['UnitAmt'] = $UnitAmt;

                $Qty = (isset($_POST['quantity_amount'][$key]) && !empty($_POST['quantity_amount'][$key]) ? $_POST['quantity_amount'][$key] : 0);
                $product_data['Qty'] = $Qty;

                $product_data['DiscountPer'] = isset($_POST['discount'][$key]) && !empty($_POST['discount'][$key]) ? $_POST['discount'][$key] : 0;
                $dicount_amt = isset($_POST['discount_ind'][$key]) && !empty($_POST['discount_ind'][$key]) ? $_POST['discount_ind'][$key] : 0;
                $product_data['DiscountAmt'] = $dicount_amt;

                $total_amt = (isset($_POST['total_amt'][$key]) && !empty($_POST['total_amt'][$key]) ? $_POST['total_amt'][$key] : ($UnitAmt * $Qty) - $dicount_amt);
                $product_data['TotalAmt'] = $total_amt;

                $this->db->insert('sale_invoice_item', $product_data);
            }
            return TRUE;
        }
    }

    public function getCommisionDataById($commision_id) {
        $sql = "SELECT cl.*
                FROM commission_list cl
                WHERE cl.status = 1 AND cl.active_status = 1 AND cl.commission_id = $commision_id";
        return $this->db->query($sql)->row();
    }

    public function checkUniqueCustomerPhoneNo($phone_no) {
        $sql = "SELECT ci.*
                FROM customer_information ci
                WHERE ci.is_deleted = 1 AND ci.customer_mobile = '$phone_no'";
        return $this->db->query($sql)->row();
    }

    public function insertCustomer($customer) {
        $this->db->trans_start();
        $this->db->insert('customer_information', $customer);
        $customer_id = $this->db->insert_id();
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        }
        return $customer_id;
    }

    public function getProductDataByBarcode($barcode) {
        $sql = "SELECT pi.*, pc.category_name, sp.supplier_id, sp.supplier_price, sp.opening_stock, sp.LandedCost, tp.tax_id, ti.tax, ti.tax_name
                FROM product_information pi
                    INNER JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = 1
                    INNER JOIN supplier_product sp ON sp.product_id = pi.id AND sp.status = 1
                    INNER JOIN tax_product tp ON tp.product_id = pi.id AND tp.status = 1
                    INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1
                WHERE pi.status = 1 AND pi.Product_Barcode = '$barcode'
                GROUP BY pi.id
                ORDER BY pi.id ASC 
                LIMIT 1";
        return $this->db->query($sql)->row();
    }

    public function getSalesInvoicePaymentDataById($SIID) {
        $sql = "SELECT tr.*, cm.bank_id, cm.cheque_no, cm.chq_given_date
                FROM transaction tr
                    LEFT JOIN cheque_manger cm ON cm.transaction_id = tr.transaction_id AND cm.status = 1
                WHERE tr.invoice_no = $SIID AND tr.is_deleted = 1 AND tr.is_transaction = 1
                ORDER BY tr.transaction_id DESC LIMIT 1";
        return $this->db->query($sql)->row_array();
    }
}
