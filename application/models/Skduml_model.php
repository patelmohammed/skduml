<?php

class Skduml_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getSabhasadWalletBalance($sabhasad_id) {
        $sql = "SELECT IFNULL(SUM(cr.amount), 0) AS total_credit, (SELECT IFNULL(SUM(dr.total_payment), 0) FROM tbl_sabhasad_payment_debit dr WHERE dr.del_status = 'Live' AND dr.ref_sabhasad_id = $sabhasad_id) AS total_debit 
                FROM tbl_sabhasad_payment_credit cr
                WHERE cr.del_status = 'Live' AND cr.ref_sabhasad_id = $sabhasad_id";
        return $this->db->query($sql)->row();
    }

}
