<?php

class Dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkUserName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND user_name != '$currentName'" : '');
        $sql = "SELECT user_name FROM user_information 
                WHERE user_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getUserName($id) {
        $sql = "SELECT user_name FROM user_information
                WHERE del_status = 'Live'
                AND id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->user_name;
    }

    public function checkUserEmail($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND user_email != '$currentName'" : '');
        $sql = "SELECT user_email FROM user_information 
                WHERE user_email = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getUserEmail($id) {
        $sql = "SELECT user_email FROM user_information
                WHERE del_status = 'Live'
                AND id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->user_email;
    }

    public function getSabhsadCount() {
        $sql = "SELECT IFNULL(COUNT(*), 0) AS total_sabhasad
                FROM tbl_sabhasad s 
                WHERE s.del_status = 'Live'";
        return $this->db->query($sql)->row()->total_sabhasad;
    }

    public function getMilkSoldCount() {
        $sql = "SELECT IFNULL(SUM(spd.milk_quantity), 0) AS total_milk_sold
                FROM tbl_sabhasad_payment_debit spd 
                WHERE spd.del_status = 'Live' ";
        return $this->db->query($sql)->row()->total_milk_sold;
    }

    public function getTotalCreditAmount() {
        $sql = "SELECT IFNULL(SUM(spc.amount), 0) AS toatl_credit
                FROM tbl_sabhasad_payment_credit spc 
                WHERE spc.del_status = 'Live' ";
        return $this->db->query($sql)->row()->toatl_credit;
    }

    public function getTotalDebitAmount() {
        $sql = "SELECT IFNULL(SUM(spd.total_payment), 0) AS total_debit
                FROM tbl_sabhasad_payment_debit spd 
                WHERE spd.del_status = 'Live' ";
        return $this->db->query($sql)->row()->total_debit;
    }

    public function getMilkPrice() {
        $sql = "SELECT IFNULL(mp.milk_price, 0) AS milk_price 
                FROM tbl_milk_price mp 
                WHERE mp.del_status = 'Live' ";
        return $this->db->query($sql)->row()->milk_price;
    }

    public function getLessBalanceSabhasadList() {
//        $sql = "SELECT (SELECT IFNULL(SUM(spc.amount), 0) FROM tbl_sabhasad_payment_credit spc WHERE spc.del_status = 'Live' AND spc.ref_sabhasad_id = s.sabhasad_id) AS total_credit, (SELECT IFNULL(SUM(spd.total_payment), 0) FROM tbl_sabhasad_payment_debit spd WHERE spd.del_status = 'Live' AND spd.ref_sabhasad_id = s.sabhasad_id) AS total_debit
//                FROM tbl_sabhasad s 
//                WHERE s.del_status = 'Live'
//                GROUP BY s.sabhasad_id ";
        $sql = "SELECT s.*, (SELECT IFNULL(SUM(spc.amount), 0) FROM tbl_sabhasad_payment_credit spc WHERE spc.del_status = 'Live' AND spc.ref_sabhasad_id = s.sabhasad_id) AS total_credit, (SELECT IFNULL(SUM(spd.total_payment), 0) FROM tbl_sabhasad_payment_debit spd WHERE spd.del_status = 'Live' AND spd.ref_sabhasad_id = s.sabhasad_id) AS total_debit 
                FROM tbl_sabhasad s 
                WHERE s.del_status = 'Live' AND ( ((SELECT IFNULL(SUM(spc.amount), 0) FROM tbl_sabhasad_payment_credit spc WHERE spc.del_status = 'Live' AND spc.ref_sabhasad_id = s.sabhasad_id) - (SELECT IFNULL(SUM(spd.total_payment), 0) FROM tbl_sabhasad_payment_debit spd WHERE spd.del_status = 'Live' AND spd.ref_sabhasad_id = s.sabhasad_id)) <= 100 )
                GROUP BY s.sabhasad_id";
        return $this->db->query($sql)->result();
    }

    public function sale_report_ajax_get($start_date, $end_date) {
        $sql = "SELECT year(spd.payment_date) as year, month(spd.payment_date) as month, SUM(spd.total_payment) as total_amount 
                FROM tbl_sabhasad_payment_debit spd 
                INNER JOIN tbl_sabhasad s On s.sabhasad_id = spd.ref_sabhasad_id AND s.del_status = 'Live'
                WHERE spd.del_status = 'Live' AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d') BETWEEN '$start_date' AND '$end_date' 
                GROUP BY year(spd.payment_date), month(spd.payment_date) ";
        return $this->db->query($sql)->row();
    }

}
