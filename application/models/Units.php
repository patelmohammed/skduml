<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Units extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

//    =========== its for unit check and insert start ===============
    public function insert_unit($data) {
        $this->db->select('*');
        $this->db->from('units');
        $this->db->where('status', 1);
        $this->db->where('unit_name', $data['unit_name']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('units', $data);
            return TRUE;
        }
    }

//    =========== its for unit check and insert close ===============
//    =========== its for unit list show start ===============
    public function unit_list() {
        $this->db->select('*');
        $this->db->from('units');
        $this->db->where('active_status', 1);
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return FALSE;
    }

//    =========== its for unit list show close ===============
//    =========== its for unit editable data show start ===============
    public function retrieve_unit_editdata($unit_id) {
        $this->db->select('*');
        $this->db->from('units');
        $this->db->where('unit_id', $unit_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return FALSE;
    }

//    =========== its for unit editable data show close ===============
//    =========== its for unit update start  ===============
    public function unit_update($data, $unit_id) {
        $this->db->where('unit_id', $unit_id);
        $this->db->update('units', $data);
        return TRUE;
    }
    
    //active_inactive_status
    function updateStatus($data, $unit_id) {        
        $this->db->where('unit_id', $unit_id);
        $this->db->update('units', $data);

        return true;
    }

//    =========== its for unit update close  ===============
//    =========== its for unit unit delete start  ===============
    public function unit_delete($unit_id) {
        $this->db->where('unit_id', $unit_id);
        $this->db->delete('units');
        return TRUE;
    }

    public function checkExistingUnit($unit_name, $id = '') {
        if ($id != '') {

            $result = $this->db->select('unit_name')
                    ->from('units')
                    ->where('unit_name', $unit_name)
                    ->where('status', 1)
                    ->where('unit_id != ', $id)
                    ->get()
                    ->row('unit_name');
        } else {
            $result = $this->db->select('unit_name')
                    ->from('units')
                    ->where('status', 1)
                    ->where('unit_name', $unit_name)
                    ->get()
                    ->row('unit_name');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

    function get_product_unit_list($postData) {
        $sql = $this->get_product_unit_list_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_product_unit_list_search_data($postData) {
        $sql = $this->get_product_unit_list_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_product_unit_list_query($postData) {
        $column_order = array('', 'a.unit_name', '');
        $sql = "SELECT a.* FROM (
                    SELECT un.* 
                    FROM units un
                    WHERE un.status = '1'
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.unit_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.unit_id DESC';
        }
        return $sql;
    }
    
    public function getCollectionCountByStorage($unit_id) {
        $this->db->select('*');
        $this->db->from('storage_list');
        $this->db->where('ref_unit_id', $unit_id);
        $this->db->where('status', 1);
        $query = $this->db->get();
        return $query->num_rows();        
    }

}
