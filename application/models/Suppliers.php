<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Suppliers extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //Count supplier
    public function count_supplier() {
        $sql = "SELECT * FROM supplier_information si WHERE si.status = 1";
        return $this->db->query($sql)->num_rows();
    }

    //supplier List
    public function supplier_list_pag($per_page = '', $page = '') {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('status', '1');
//        $this->db->limit($per_page, $page);
        $this->db->order_by('supplier_id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    // supplier search
    public function supplier_search($supplier_id) {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where('status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //supplier list
    public function supplier_list() {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('active_status', '1');
        $this->db->where('status', '1');
        $this->db->order_by('supplier_id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //supplier List For Report
    public function supplier_list_report() {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('status', '1');
        $this->db->order_by('supplier_id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //supplier List
    public function supplier_list_count() {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    //Retrieve company Edit Data
    public function retrieve_company() {
        $this->db->select('*');
        $this->db->from('company_information');
        $this->db->limit('1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //supplier Search List
    public function supplier_search_item($supplier_id) {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where('status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Selected Supplier List
    public function selected_product($product_id) {
        $this->db->select('*');
        $this->db->from('product_information');
        $this->db->where('product_id', $product_id);
        $this->db->where('status', '1');
        return $query = $this->db->get()->row();
    }

    //Product search item
    public function product_search_item($supplier_id, $product_name) {
        $this->db->select('a.*,b.*');
        $this->db->from('product_information a');
        $this->db->join('supplier_product b', 'a.product_id=b.product_id');
        $this->db->where('b.supplier_id', $supplier_id);
        $this->db->where('a.status', '1');
        $this->db->where('b.status', '1');
        $this->db->like('product_model', $product_name, 'both');
        $this->db->or_like('product_name', $product_name, 'both');
        $this->db->order_by('product_name', $product_name, 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //supplier product
    public function supplier_product($supplier_id) {
        $this->db->select('*');
        $this->db->from('product_information');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where('status', '1');
        return $query = $this->db->get()->result();
    }

    //Count supplier
    public function supplier_entry($data) {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('supplier_name', $data['supplier_name']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('supplier_information', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

    //Supplier Previous balance adjustment
    public function previous_balance_add($balance, $supplier_id) {
        $data = array(
            'supplier_id' => $supplier_id,
            'chalan_no' => 'Adjustment ',
            'deposit_no' => NULL,
            'amount' => $balance,
            'description' => "Previous adjustment with software",
            'payment_type' => "NA",
            'cheque_no' => "NA",
            'date' => date("Y-m-d"),
            'status' => 1,
            'd_c' => 'c'
        );

        $this->db->insert('supplier_ledger', $data);
    }

    //Retrieve supplier Edit Data
    public function retrieve_supplier_editdata($supplier_id) {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where('status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Update Categorie
    public function update_supplier($data, $supplier_id) {
        $this->db->where('supplier_id', $supplier_id);
        $this->db->update('supplier_information', $data);
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('status', 1);
        $query = $this->db->get();
        return true;
    }

    //active_inactive_status
    function updateStatus($data, $supplier_id) {
        $this->db->where('supplier_id', $supplier_id);
        $this->db->update('supplier_information', $data);

        return true;
    }

    // Delete supplier ledger
    public function delete_supplier_ledger($supplier_id) {
        $this->db->where('supplier_id', $supplier_id);
        $this->db->delete('supplier_ledger');
    }

// Delete supplier from transection 
    public function delete_supplier_transection($supplier_id) {
        $this->db->where('relation_id', $supplier_id);
        $this->db->delete('transaction');
    }

    // Delete supplier from transection 
    // Delete supplier Item

    public function delete_supplier_data($supplier_id) {

        $this->db->trans_start();

        $this->db->where('supplier_id', $supplier_id);
        $this->db->update('supplier_information', array('status' => '0'));

        $this->db->where('relation_id', $supplier_id);
        $this->db->update('transaction', array('is_deleted' => '0'));

        $this->db->where('supplier_id', $supplier_id);
        $this->db->update('supplier_ledger', array('is_deleted' => 'Deleted'));

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            return false;
        }
        return true;
    }

    public function delete_supplier($supplier_id) {
        $this->db->where('supplier_id', $supplier_id);
        $this->db->delete('supplier_information');

        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('status', 1);
        $query = $this->db->get();
        return true;
    }

    //Retrieve supplier Personal Data 
    public function supplier_personal_data($supplier_id) {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where('status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    /// Supplier person data all
    public function supplier_personal_data_all() {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    // second
    public function supplier_personal_data1() {
        $this->db->select('*');
        $this->db->from('supplier_information');
        $this->db->where('status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Retrieve Supplier Purchase Data 
    public function supplier_purchase_data($supplier_id) {
        $sql = "SELECT pi.DateTime,pi.PINID,pi.PINNO, pi.GrandTotal,pi.BasicTotal,pi.Discount,pi.Taxtotal,pii.*, SUM(piit.TaxAmt) AS TaxAmt
                FROM purchase_invoice pi
                    INNER JOIN purchase_invoice_item pii ON pii.refPINID = pi.PINID AND pii.status = '1'
                    INNER JOIN purchase_invoice_itemtaxdet piit ON piit.refPINItemID = pii.PINItemID AND piit.status = '1'
                    LEFT JOIN  product_information ppo ON ppo.id = pii.refProductID AND ppo.status = '1'
                WHERE  pi.status = '1' AND pi.RefSupplierID = '$supplier_id'
                GROUP BY pi.PINID";
//        preprint($sql);
        return $this->db->query($sql)->result();
    }

    //Supplier Search Data
    public function supplier_search_list($cat_id, $company_id) {
        $this->db->select('a.*,b.sub_category_name,c.category_name');
        $this->db->from('suppliers a');
        $this->db->join('supplier_sub_category b', 'b.sub_category_id = a.sub_category_id');
        $this->db->join('supplier_category c', 'c.category_id = b.category_id');
        $this->db->where('a.sister_company_id', $company_id);
        $this->db->where('c.category_id', $cat_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Supplioer product information
    public function supplier_product_sale($supplier_id) {
        $query = $this->db->select('
								a.product_name,
								a.supplier_price,
								b.quantity,
								CAST(sum(b.quantity * b.supplier_rate) AS DECIMAL(16,2)) as total_taka,
								c.date
								')
                ->from('product_information a')
                ->join('invoice_details b', 'a.product_id = b.product_id', 'left')
                ->join('invoice c', 'c.invoice_id = b.invoice_id', 'left')
                ->where('a.supplier_id', $supplier_id)
                ->group_by('c.date')
                ->order_by('c.date')
                ->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    // Second 
    public function supplier_product_sale1($per_page = '', $page = '') {
//        $this->db->select('*');
//        $this->db->from('supplier_ledger');
//        if ($per_page != '' && $page != '') {
//            $this->db->limit($per_page, $page);
//        }
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return false;
        $sql = "SELECT sl.*, si.supplier_name, pi.PINNO, pi.PINID, pr.PRID, pr.PRNO
                FROM supplier_ledger sl
                    LEFT JOIN purchase_invoice pi ON sl.chalan_no = pi.PINID AND sl.ref_table_name = 'purchase_invoice' AND pi.status = '1'
                    LEFT JOIN purchase_return pr ON sl.chalan_no = pr.PRID AND sl.ref_table_name = 'purchase_return' AND pr.status = '1'
                    LEFT JOIN supplier_information si ON si.supplier_id = sl.supplier_id AND si.status = '1'
                WHERE sl.is_deleted = 'Live' AND sl.status = '1' ORDER BY sl.date DESC";
        return $this->db->query($sql)->result();
    }

    // count ledger info
    public function count_supplier_product_info() {
        $this->db->select('*');
        $this->db->from('supplier_ledger');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    //To get certain supplier's chalan info by which this company got products day by day
    public function suppliers_ledger($supplier_id = null, $start_date = null, $end_date = null) {
        $sql = "SELECT sl.*, si.supplier_name, pi.PINNO, pi.PINID, pr.PRID, pr.PRNO
                FROM supplier_ledger sl
                    LEFT JOIN purchase_invoice pi ON sl.chalan_no = pi.PINID AND sl.ref_table_name = 'purchase_invoice' AND pi.status = '1'
                    LEFT JOIN purchase_return pr ON sl.chalan_no = pr.PRID AND sl.ref_table_name = 'purchase_return' AND pr.status = '1'
                    LEFT JOIN supplier_information si ON si.supplier_id = sl.supplier_id AND si.status = '1'
                WHERE sl.is_deleted = 'Live' AND sl.status = '1'";

        if (isset($supplier_id) && !empty($supplier_id)) {
            $sql .= " AND sl.supplier_id = '$supplier_id'";
        }

        if (isset($start_date) && !empty($start_date)) {
            $sql .= " AND sl.date >= '$start_date'";
        }
        if (isset($end_date) && !empty($end_date)) {
            $sql .= " AND sl.date <= '$end_date'";
        }
        return $this->db->query($sql)->result();
    }

    //Retrieve supplier Transaction Summary
    public function suppliers_transection_summary($supplier_id, $start = '', $end = '') {
        $result = array();
        $this->db->select_sum('amount', 'total_credit');
        $this->db->from('supplier_ledger');
        $this->db->where(array('deposit_no' => NULL, 'status' => 1, 'supplier_id' => $supplier_id));
        if (isset($start) && !empty($start)) {
            $this->db->where(array('date >=' => $start));
        }
        if (isset($end) && !empty($end)) {
            $this->db->where(array('date >=' => $end));
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result[] = $query->result_array();
        }

        $this->db->select_sum('amount', 'total_debit');
        $this->db->from('supplier_ledger');
        $this->db->where(array('chalan_no' => NULL, 'status' => 1, 'supplier_id' => $supplier_id));
        if (isset($start) && !empty($start)) {
            $this->db->where(array('date >=' => $start));
        }
        if (isset($end) && !empty($end)) {
            $this->db->where(array('date >=' => $end));
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result[] = $query->result_array();
        }
        return $result;
    }

    public function suppliers_transection_summary1() {
        $result = array();
        $this->db->select_sum('amount', 'total_credit');
        $this->db->from('supplier_ledger');
        $this->db->where(array('deposit_no' => NULL, 'status' => 1));

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result[] = $query->result_array();
        }

        $this->db->select_sum('amount', 'total_debit');
        $this->db->from('supplier_ledger');
        $this->db->where(array('chalan_no' => NULL, 'status' => 1));

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result[] = $query->result_array();
        }
        return $result;
    }

    //Findings a certain supplier products sales information
    public function supplier_sales_details() {
        $supplier_id = $this->uri->segment(3);
        $start = $this->uri->segment(4);
        $end = $this->uri->segment(5);

        $this->db->select('
					date,
					product_name,
					product_model,
					product_id,
					quantity,
					supplier_rate,
					CAST(quantity*supplier_rate AS DECIMAL(16,2) ) as total
				');
        $this->db->from('sales_report');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where(array('date >=' => $start, 'date <=' => $end));
        $this->db->order_by('date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    ################################################################################################ Supplier sales details all menu################

    public function supplier_sales_details_all($per_page = '', $page = '') {
//        $this->db->select('
//						date,
//						product_name,
//						product_model,
//						product_id,
//						quantity,
//						supplier_rate,
//						CAST(quantity*supplier_rate AS DECIMAL(16,2) ) as total
//					');
//        $this->db->from('sales_report');
//        $this->db->order_by('date', 'desc');
//        if ($per_page != '' && $page != '') {
//            $this->db->limit($per_page, $page);
//        }
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return false;

        $sql = "SELECT pi.RefSupplierID, SUM(pi.GrandTotal) AS GrandTotal
                FROM product_inward pi
                WHERE pi.status = '1'
                GROUP BY pi.RefSupplierID";
        return $this->db->query($sql)->result();
    }

    //Findings a certain supplier products sales information
    public function supplier_sales_details_count($supplier_id) {
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');

        $this->db->select('date,product_name,product_model,product_id,quantity,supplier_rate,(quantity*supplier_rate) as total');
        $this->db->from('sales_report');
        $this->db->where('supplier_id', $supplier_id);
        if ($from_date != null AND $to_date != null) {
            $this->db->where('date >', $from_date);
            $this->db->where('date <', $to_date);
        }
        $this->db->order_by('date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    // supplier sales details count menu all
    public function supplier_sales_details_count_all() {

        $this->db->select('date,product_name,product_model,product_id,quantity,supplier_rate,(quantity*supplier_rate) as total');
        $this->db->from('sales_report');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    public function supplier_sales_summary($per_page, $page) {
        $date = $this->input->post('date');
        $supplier_id = $this->uri->segment(3);
        $start = $this->uri->segment(4);
        $end = $this->uri->segment(5);

        $this->db->select('
						date,
						quantity,
						product_name,product_model,
						product_id, 
						sum(quantity) as quantity ,
						supplier_rate,
						CAST(sum(quantity*supplier_rate) AS DECIMAL(16,2)) as total,
					');

        $this->db->from('sales_report');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where(array('date >=' => $start, 'date <=' => $end));
        $this->db->group_by('invoice_id');
        //$this->db->order_by('date','desc');
        $this->db->limit($per_page, $page);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function supplier_sales_summary_count($supplier_id) {
        $date = $this->input->post('date');

        $this->db->select('
						date,
						quantity,
						product_name,product_model,
						product_id,
						sum(quantity) as quantity ,
						supplier_rate,
						sum(quantity*supplier_rate) as total,
					');

        $this->db->from('sales_report');
        $this->db->where('supplier_id', $supplier_id);
        if ($date != null) {
            $this->db->where('date =', $date);
        }
        $this->db->group_by('product_id,date,supplier_rate');
        $this->db->order_by('date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    ################## Ssales & Payment Details ####################

    public function sales_payment_actual($per_page, $page) {
        $supplier_id = $this->uri->segment(3);
        $start = $this->uri->segment(4);
        $end = $this->uri->segment(5);

        $this->db->select('*');
        $this->db->from('supplier_ledger');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where(array('date >=' => $start, 'date <=' => $end));
        $this->db->limit($per_page, $page);
        $this->db->order_by('date');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return false;
    }

    ################## Ssales & Payment Details ####################

    public function sales_payment_actual_count() {
        $supplier_id = $this->uri->segment(3);

        $start = $this->uri->segment(4);
        $end = $this->uri->segment(5);

        $this->db->select('*');
        $this->db->from('supplier_ledger');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where(array('date >=' => $start, 'date <=' => $end));
        $this->db->order_by('date');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }

        return false;
    }

################## total sales & payment information ####################

    public function sales_payment_actual_total() {
        $supplier_id = $this->uri->segment(3);
        $start = $this->uri->segment(4);
        $end = $this->uri->segment(5);

        $this->db->select_sum('sub_total');
        $this->db->from('sales_actual');
        $this->db->where('supplier_id', $supplier_id);
        //$this->db->where(array('date >='=>$start , 'date <='=>$end));
        $this->db->where('sub_total >', 0);
        $query = $this->db->get();
        $result = $query->result_array();
        $data[0]["debit"] = $result[0]["sub_total"];

        $this->db->select_sum('sub_total');
        $this->db->from('sales_actual');
        $this->db->where('supplier_id', $supplier_id);
        //$this->db->where(array('date >='=>$start , 'date <='=>$end));
        $this->db->where('sub_total <', 0);
        $query = $this->db->get();
        $result = $query->result_array();
        $data[0]["credit"] = $result[0]["sub_total"];

        $data[0]["balance"] = $data[0]["debit"] + $data[0]["credit"];

        return $data;
    }

//To get certain supplier's payment info which was paid day by day
    public function supplier_paid_details($supplier_id) {
        $this->db->select('*');
        $this->db->from('supplier_ledger');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where('chalan_no', NULL);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//To get certain supplier's chalan info by which this company got products day by day
    public function supplier_chalan_details($supplier_id) {
        $this->db->select('*');
        $this->db->from('supplier_ledger');
        $this->db->where('supplier_id', $supplier_id);
        $this->db->where('deposit_no', NULL);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    #############################################################################################Search supplier report by id and datebetween####################################################

    public function suppliers_transection_report($supplier_id, $start, $end) {
        $result = array();

        $this->db->select('
			CAST(amount AS DECIMAL(16,2)) as total_debit,
			date as ledger_date,
			description,
			deposit_no

			');
        $this->db->from('supplier_ledger');
        $this->db->where(array('date >=' => $start, 'date <=' => $end));
        $this->db->where('supplier_id', $supplier_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //Retrieve supplier Transaction Summary by supplier id
    public function suppliers_transection_summary_info($supplier_id) {
        $result = array();
        $this->db->select_sum('amount', 'total_credit');
        $this->db->from('supplier_ledger');
        $this->db->where(array('deposit_no' => NULL, 'status' => 1));
        $this->db->where(array('supplier_id' => $supplier_id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result[] = $query->result_array();
        }

        $this->db->select_sum('amount', 'total_debit');
        $this->db->from('supplier_ledger');
        $this->db->where(array('chalan_no' => NULL, 'status' => 1));
        $this->db->where(array('supplier_id' => $supplier_id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result[] = $query->result_array();
        }
        return $result;
    }

    public function supplier_product_sale_info($supplier_id) {
//        $this->db->select('*');
//        $this->db->from('supplier_ledger');
//        $this->db->where('supplier_id', $supplier_id);
//        $this->db->order_by('id', 'asc');
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return false;
        $sql = "SELECT sl.*,pi.PINNO, pi.PINID 
                FROM supplier_ledger sl
                LEFT JOIN purchase_invoice pi ON sl.chalan_no = pi.PINID AND pi.status = '1' AND sl.ref_table_name = 'purchase_invoice'
                WHERE supplier_id = '$supplier_id' AND sl.is_deleted = 'Live' ";
        return $this->db->query($sql)->result();
    }

    public function checkExistingSupplier($supplier, $id = '') {
        if ($id != '') {

            $result = $this->db->select('supplier_name')
                    ->from('supplier_information')
                    ->where('supplier_name', $supplier)
                    ->where('status', 1)
                    ->where('supplier_id != ', $id)
                    ->get()
                    ->row('supplier_name');
        } else {
            $result = $this->db->select('supplier_name')
                    ->from('supplier_information')
                    ->where('status', 1)
                    ->where('supplier_name', $supplier)
                    ->get()
                    ->row('supplier_name');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

    public function purchaseDataBySupplierId($supplier_id) {
        $sql = "SELECT pi.DateTime,pi.PIID,pi.PINO,pi.RefSupplierID,pi.GrandTotal,pi.TotalLandedCost,pi.Discount,pi.Taxtotal,pi.BasicTotal
                FROM product_inward pi
                INNER JOIN product_inward_item pii ON pii.refPIID = pi.PIID AND pii.status = '1'
                INNER JOIN product_inward_itemtaxdet piit ON piit.refPIItemID = pii.PIItemID AND piit.status = '1'
                LEFT JOIN  product_pomst ppo ON ppo.POID = pii.refPOID
                WHERE  pi.status = '1' AND pi.RefSupplierID = '$supplier_id'
                GROUP BY pi.PIID";
//        preprint($sql);
        return $this->db->query($sql)->result();
    }

    public function getTotalPurchaseSaleAndStockBySupplierID($supplier_id) {

        $date = date('Y-m-d H:i:s');

        $sql = "SELECT SUM(sh.Purchase) as total_purchase, SUM(sh.sell) as total_sales, 
                 (SUM(opening_stock)
                        + SUM(sh.Purchase) 
                        - SUM(sh.sell) 
                        + SUM(sh.sell_return) 
                        - SUM(sh.purchase_return) 
                        - SUM(sh.consumption) 
                        + SUM(sh.production) 
                        + SUM(sh.production_return)) as stock
               FROM stock_history sh WHERE sh.supplier_id = '$supplier_id' AND sh.vdate  <= '$date'";
//        preprint($sql);

        return $this->db->query($sql)->row();
    }

    function get_supplier_list($postData) {
        $sql = $this->get_supplier_list_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_supplier_list_search_data($postData) {
        $sql = $this->get_supplier_list_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_supplier_list_query($postData) {
        $column_order = array('', 'a.supplier_name', 'a.contact_person', 'a.address', 'a.mobile', 'a.details', '');
        $sql = "SELECT a.* FROM (
                    SELECT * 
                    FROM supplier_information si 
                    WHERE si.status = '1'
                    ORDER BY si.supplier_id DESC
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.supplier_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.supplier_id DESC';
        }
        return $sql;
    }

    function get_supplier_sales_list($postData) {
        $sql = $this->get_supplier_sales_list_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_supplier_sales_list_search_data($postData) {
        $sql = $this->get_supplier_sales_list_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_supplier_sales_list_query($postData) {
        $column_order = array('', 'a.supplier_name', 'a.GrandTotal', '');
        $sql = "SELECT a.* FROM (
                    SELECT si.supplier_name,pi.RefSupplierID, SUM(pi.GrandTotal) AS GrandTotal
                    FROM product_inward pi
                    INNER JOIN supplier_information si ON si.supplier_id = pi.RefSupplierID AND si.status = '1'
                    WHERE pi.status = '1'
                    GROUP BY pi.RefSupplierID
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.RefSupplierID";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.GrandTotal DESC';
        }
        return $sql;
    }
}
