<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //customer List
    public function category_list() {
        $this->db->select('*');
        $this->db->from('product_category');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //customer List
    public function category_list_product() {
        $this->db->select('*');
        $this->db->from('product_category');
        $this->db->where('active_status', 1);
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //group list
    public function group_list_product() {
        $this->db->select('*');
        $this->db->from('product_group');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //manufacturing list
    public function mfg_list_product() {
        $this->db->select('*');
        $this->db->from('product_mfg');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //customer List
    public function category_list_count() {
        $this->db->select('*');
        $this->db->from('product_category');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    //Category Search Item
    public function category_search_item($category_id) {
        $this->db->select('*');
        $this->db->from('product_category');
        $this->db->where('category_id', $category_id);
        $this->db->limit('500');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Count customer
    public function category_entry($data) {
        $this->db->select('*');
        $this->db->from('product_category');
        $this->db->where('status', 1);
        $this->db->where('category_name', $data['category_name']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('product_category', $data);
            return TRUE;
        }
    }

    //Retrieve customer Edit Data
    public function retrieve_category_editdata($category_id) {
        $this->db->select('*');
        $this->db->from('product_category');
        $this->db->where('category_id', $category_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Update Categories
    public function update_category($data, $category_id) {
        $this->db->where('category_id', $category_id);
        $this->db->update('product_category', $data);
        return true;
    }
    
    function updateStatus($data, $category_id) {        
        $this->db->where('category_id', $category_id);
        $this->db->update('product_category', $data);

        return true;
    }

    // Delete customer Item
    public function delete_category($category_id) {
       
        $this->db->where('category_id', $category_id);
        $this->db->update('product_category', ['status' => 0]);
        return true;
    }

    function get_category_list($postData) {
        $sql = $this->get_category_list_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_category_list_search_data($postData) {
        $sql = $this->get_category_list_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_category_list_query($postData) {
        $column_order = array('', 'a.category_name', '');
        $sql = "SELECT a.* FROM (
                    SELECT * 
                    FROM product_category pc
                    WHERE pc.status = '1'
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.category_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.category_id DESC';
        }
        return $sql;
    }
    
    public function checkExistingCategory($category_name, $id = '') {
        if ($id != '') {

            $result = $this->db->select('category_name')
                    ->from('product_category')
                    ->where('category_name', $category_name)
                    ->where('status', 1)
                    ->where('category_id != ', $id)
                    ->get()
                    ->row('category_name');
        } else {
            $result = $this->db->select('category_name')
                    ->from('product_category')
                    ->where('status', 1)
                    ->where('category_name', $category_name)
                    ->get()
                    ->row('category_name');
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

}
