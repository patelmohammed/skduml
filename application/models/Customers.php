<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customers extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //Count customer
    public function count_customer() {
        $sql = "SELECT * FROM customer_information ci WHERE ci.is_deleted = 1";
        return $this->db->query($sql)->num_rows();
    }

    //customer List
    public function customer_list_count() {
        $this->db->select('customer_information.*,sum(customer_transection_summary.amount) as customer_balance');
        $this->db->from('customer_information');
        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id');
        $this->db->where('customer_information.is_deleted', '1');
        $this->db->group_by('customer_transection_summary.customer_id');
        $this->db->order_by('customer_information.create_date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    //customer List
    public function customer_list($per_page = '', $page = '') {
        $this->db->select('customer_information.*,
			CAST(sum(customer_transection_summary.amount) AS DECIMAL(16,2)) as customer_balance
			');
        $this->db->from('customer_information');
        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id');
        $this->db->where('customer_information.is_deleted', '1');
        $this->db->group_by('customer_transection_summary.customer_id');
        $this->db->order_by('customer_information.create_date', 'ASC');
        if ($per_page != '' && $page != '') {
            $this->db->limit($per_page, $page);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //all customer List
    public function all_customer_list() {
        $this->db->select('customer_information.*,sum(customer_transection_summary.amount) as customer_balance');
        $this->db->from('customer_information');
        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id', 'LEFT');
        $this->db->where('customer_information.is_deleted', '1');
        $this->db->group_by('customer_transection_summary.customer_id');
        $this->db->order_by('customer_information.create_date', 'DESC');
        $query = $this->db->get();
//        preprint($this->db->last_query());
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Credit customer List
    public function credit_customer_list($per_page = '', $page = '') {
        $this->db->distinct('customer_id');
        $this->db->select('*');
        $this->db->from('customer_information');
        $this->db->where('is_deleted', '1');
        if ($per_page != '' && $per_page != '') {
            $this->db->limit($per_page, $page);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//    public function credit_customer_list($per_page, $page) {
//        $this->db->select('customer_information.*,
//			sum(customer_transection_summary.amount) as customer_balance
//			');
//        $this->db->from('customer_information');
//        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id');
//        //$this->db->where('customer_information.status',2);
//        $this->db->group_by('customer_transection_summary.customer_id');
//        $this->db->having('customer_balance < 0', NULL, FALSE);
//        $this->db->limit($per_page, $page);
//        $query = $this->db->get();
//
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return false;
//    }

    //All Credit customer List
    public function all_credit_customer_list() {
        $this->db->select('customer_information.*,
			sum(customer_transection_summary.amount) as customer_balance
			');
        $this->db->from('customer_information');
        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id');
        $this->db->where('customer_information.is_deleted', '1');
        $this->db->group_by('customer_transection_summary.customer_id');
        $this->db->having('customer_balance < 0', NULL, FALSE);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Credit customer List
    public function credit_customer_list_count() {
        $this->db->select('customer_information.*,sum(customer_transection_summary.amount) as customer_balance');
        $this->db->from('customer_information');
        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id');
        $this->db->where('customer_information.is_deleted', '1');
        $this->db->group_by('customer_transection_summary.customer_id');
        $this->db->having('customer_balance < 0', NULL, FALSE);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    //Paid Customer list
    public function paid_customer_list($per_page = '', $page = '') {
        $this->db->distinct('customer_id');
        $this->db->select('*');
        $this->db->from('customer_information');
        $this->db->where('is_deleted', '1');
        if ($per_page != '' && $page != '') {
            $this->db->limit($per_page, $page);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //All Paid Customer list
    public function all_paid_customer_list() {
        $this->db->select('customer_information.*,sum(customer_transection_summary.amount) as customer_balance');
        $this->db->from('customer_information');
        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id', 'LEFT');
        $this->db->where('customer_information.is_deleted', '1');
        $this->db->having('customer_balance >= 0', NULL, FALSE);
        $this->db->group_by('customer_transection_summary.customer_id');
        $query = $this->db->get();
//        preprint($this->db->last_query());
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Paid Customer list
    public function paid_customer_list_count() {
        $this->db->select('customer_information.*,sum(customer_transection_summary.amount) as customer_balance');
        $this->db->from('customer_information');
        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id');
        $this->db->where('customer_information.is_deleted', '1');
        $this->db->having('customer_balance >= 0', NULL, FALSE);
        $this->db->group_by('customer_transection_summary.customer_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    //Customer Search List
    public function customer_search_item($customer_id) {

        $this->db->select('customer_information.*,
			CAST(sum(customer_transection_summary.amount) AS DECIMAL(16,2)) as customer_balance');
        $this->db->from('customer_information');
        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id');
        $this->db->where('customer_information.is_deleted', '1');
        $this->db->where('customer_information.customer_id', $customer_id);
        $this->db->group_by('customer_transection_summary.customer_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Credit Customer Search List
    public function credit_customer_search_item($customer_id) {

        $this->db->select('customer_information.*,
			sum(customer_transection_summary.amount) as customer_balance
			');
        $this->db->from('customer_information');
        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id');
        $this->db->where('customer_information.status', 2);
        $this->db->group_by('customer_transection_summary.customer_id');
        $this->db->where('customer_information.customer_id', $customer_id);
        $this->db->where('customer_information.is_deleted', '1');
        $this->db->having('customer_balance != 0', NULL, FALSE);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            $this->session->set_userdata(array('error_message' => display('this_is_not_credit_customer')));
            redirect('Ccustomer/credit_customer');
        }
    }

    //Paid Customer Search List
    public function paid_customer_search_item($customer_id) {

        $this->db->select('customer_information.*,sum(customer_transection_summary.amount) as customer_balance');
        $this->db->from('customer_information');
        $this->db->join('customer_transection_summary', 'customer_transection_summary.customer_id= customer_information.customer_id');
        $this->db->where('customer_information.is_deleted', '1');
        $this->db->having('customer_balance >= 0', NULL, FALSE);
        $this->db->where('customer_information.customer_id', $customer_id);
        $this->db->group_by('customer_transection_summary.customer_id');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Count customer
    public function customer_entry($data) {
        
        $this->db->select('*');
        $this->db->from('customer_information');
        $this->db->where('company__name', $data['company__name']);
        $query = $this->db->get();

//        preprint($query->result());

        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('customer_information', $data);
            $customer_id = $this->db->insert_id();
            
            return $customer_id;
        }
    }

    //Customer Previous balance adjustment
    public function previous_balance_add($balance, $customer_id) {
        
//        $transaction_id = $this->auth->generator(10);
        $data = array(
            'transaction_id' => null,
            'customer_id' => $customer_id,
            'invoice_no' => "NA",
            'receipt_no' => NULL,
            'amount' => $balance,
            'description' => "Previous adjustment with software",
            'payment_type' => "NA",
            'cheque_no' => "NA",
            'date' => date("Y-m-d"),
            'status' => 1,
            'd_c' => "d"
        );

        $this->db->insert('customer_ledger', $data);
    }

    //Retrieve company Edit Data
    public function retrieve_company() {
        $this->db->select('*');
        $this->db->from('company_information');
        $this->db->limit('1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
            
        }
        return false;
    }

    //Retrieve customer Edit Data
    public function retrieve_customer_editdata($customer_id) {
        $this->db->select('*');
        $this->db->from('customer_information');
        $this->db->where('customer_id', $customer_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Retrieve customer Personal Data 
    public function customer_personal_data($customer_id = null) {
        $this->db->select('*');
        $this->db->from('customer_information');
        $this->db->where('is_deleted', '1');
        if (isset($customer_id) && !empty($customer_id)) {
            $this->db->where('customer_id', $customer_id);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Retrieve customer Invoice Data 
    public function customer_invoice_data($customer_id) {
        $this->db->select('*');
        $this->db->from('customer_ledger');
        $this->db->where(array('customer_id' => $customer_id, 'receipt_no' => NULL, 'status' => 1));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Retrieve customer Receipt Data 
    public function customer_receipt_data($customer_id) {
        $this->db->select('*');
        $this->db->from('customer_ledger');
        $this->db->where(array('customer_id' => $customer_id, 'invoice_no' => NULL, 'status' => 1));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Retrieve customer All data 
    public function customerledger_tradational($customer_id = null, $from_date = null, $to_date = null) {

        $sql = "SELECT cl.*, ci.company__name, si.SIID, si.SINO, sr.SRID, sr.SRNO
                FROM customer_ledger cl
                    LEFT JOIN sale_invoice si ON cl.invoice_no = si.SIID AND cl.ref_table_name = 'sale_invoice'
                    LEFT JOIN sale_return sr ON cl.invoice_no = sr.SRID AND cl.ref_table_name = 'sale_return'
                    LEFT JOIN customer_information ci ON ci.customer_id = cl.customer_id
                WHERE cl.is_deleted = 'Live' AND cl.status = '1'";

        if (isset($customer_id) && !empty($customer_id)) {
            $sql .= " AND cl.customer_id = '$customer_id'";
        }
        if (isset($from_date) && !empty($from_date)) {
            $sql .= " AND cl.date >= '$from_date'";
        }
        if (isset($to_date) && !empty($to_date)) {
            $sql .= " AND cl.date <= '$to_date'";
        }

        $sql .= " ORDER BY cl.date ASC";
        return $this->db->query($sql)->result_array();
    }

    //Retrieve customer total information
    public function customer_transection_summary($customer_id = null, $from_date = null, $to_date = null) {
        $result = array();
        $this->db->select_sum('amount', 'total_credit');
        $this->db->from('customer_ledger');
        $this->db->where(array('receipt_no' => NULL, 'status' => 1, 'is_deleted' => 'Live', 'd_c' => 'd'));
        if (isset($customer_id) && !empty($customer_id)) {
            $this->db->where('customer_id', $customer_id);
        }
        if (isset($from_date) && !empty($from_date)) {
            $this->db->where('date', $from_date);
        }
        if (isset($to_date) && !empty($to_date)) {
            $this->db->where('date', $to_date);
        }
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result[] = $query->result_array();
        }

        $this->db->select_sum('amount', 'total_debit');
        $this->db->from('customer_ledger');
        $this->db->where(array('invoice_no' => NULL, 'status' => 1, 'is_deleted' => 'Live', 'd_c' => 'd'));
        if (isset($customer_id) && !empty($customer_id)) {
            $this->db->where('customer_id', $customer_id);
        }
        if (isset($from_date) && !empty($from_date)) {
            $this->db->where('date', $from_date);
        }
        if (isset($to_date) && !empty($to_date)) {
            $this->db->where('date', $to_date);
        }
        $query2 = $this->db->get();

        if ($query2->num_rows() > 0) {
            $result[] = $query2->result_array();
        }
        return $result;
    }

    //Update Categories
    public function update_customer($data, $customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customer_information', $data);

        $this->db->select('*');
        $this->db->from('customer_information');
        $query = $this->db->get();
        foreach ($query->result() as $row) {
            $json_customer[] = array('label' => $row->customer_name, 'value' => $row->customer_id);
        }
        $cache_file = './my-assets/js/admin_js/json/customer.json';
        $customerList = json_encode($json_customer);
        file_put_contents($cache_file, $customerList);
        return true;
    }

    //active_inactive_status
    function updateStatus($data, $customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customer_information', $data);

        return true;
    }

    // custromer transection delete
    public function delete_transection($customer_id) {
        $this->db->where('relation_id', $customer_id);
        $this->db->delete('transaction');
    }

    // custromer invoicedetails delete
    public function delete_invoicedetails($customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->delete('invoice_details');
    }

    // custromer invoice delete
    public function delete_invoic($customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->delete('invoice');
    }

    // delete customer ledger 
    public function delete_customer_ledger($customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->delete('customer_ledger');
    }

    // Delete customer Item
    public function delete_customer($customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->delete('customer_information');

        $this->db->select('*');
        $this->db->from('customer_information');
        $query = $this->db->get();
        foreach ($query->result() as $row) {
            $json_customer[] = array('label' => $row->customer_name, 'value' => $row->customer_id);
        }
        $cache_file = './my-assets/js/admin_js/json/customer.json';
        $customerList = json_encode($json_customer);
        file_put_contents($cache_file, $customerList);
        return true;
    }

    public function delete_customer_data($customer_id) {

        $this->db->trans_start();

        $this->db->where('customer_id', $customer_id);
        $this->db->update('customer_information', array('is_deleted' => '0'));

        $this->db->where('relation_id', $customer_id);
        $this->db->update('transaction', array('is_deleted' => '0'));

        $this->db->where('customer_id', $customer_id);
        $this->db->update('customer_ledger', array('is_deleted' => 'Deleted'));

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        }
        return true;
    }

    public function customer_search_list($cat_id, $company_id) {
        $this->db->select('a.*,b.sub_category_name,c.category_name');
        $this->db->from('customers a');
        $this->db->join('customer_sub_category b', 'b.sub_category_id = a.sub_category_id');
        $this->db->join('customer_category c', 'c.category_id = b.category_id');
        $this->db->where('a.sister_company_id', $company_id);
        $this->db->where('c.category_id', $cat_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function saleDataByCustomerId($customer_id = null) {
        $sql = "SELECT so.DateTime,so.SIID,so.SINO,so.RefCustomerID,so.GrandTotal,so.BasicTotal,so.DiscountTotal,so.Taxtotal
                FROM sale_invoice_item soi 
                LEFT JOIN sale_invoice_itemtaxdet soit ON soit.refSIItemID = soi.SIItemID AND soit.status = '1'
                LEFT JOIN sale_invoice so ON so.SIID = soi.refSIID AND so.status = '1'
                WHERE soi.status = '1'";

        if (isset($customer_id) && !empty($customer_id)) {
            $sql .= " AND so.RefCustomerID = '$customer_id'";
        }

        $sql .= " GROUP BY so.SIID
                HAVING so.SIID IS NOT NULL";

        return $this->db->query($sql)->result();
    }

    function get_credit_customer_list($postData, $customer_id) {
        $sql = $this->get_credit_customer_list_query($postData, $customer_id);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_credit_customer_list_search_data($postData, $customer_id) {
        $sql = $this->get_credit_customer_list_query($postData, $customer_id);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_credit_customer_list_query($postData, $customer_id) {
        $column_order = array('', 'a.customer_name', 'a.customer_address', 'a.customer_mobile', 'a.balance', '');
        $sql = "SELECT a.* FROM (
                    SELECT b.*, (credit - debit) AS balance 
                    FROM (SELECT *, (SELECT SUM(amount) FROM customer_ledger WHERE d_c ='d' AND customer_id = ci.customer_id) as  debit, 
                         IFNULL((SELECT SUM(amount) FROM customer_ledger WHERE d_c ='c' AND customer_id = ci.customer_id),0) as  credit
                         FROM customer_information ci 
                         WHERE ci.is_deleted = '1'";
        if (isset($customer_id) && !empty($customer_id) && $customer_id != '') {
            $sql .= " AND ci.customer_id = '$customer_id'";
        }
        $sql .= " ) b 
                ) a WHERE 1 = 1 AND a.balance > 0";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.customer_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.customer_id DESC';
        }
        return $sql;
    }

    function get_customer_list($postData, $customer_id) {
        $sql = $this->get_customer_list_query($postData, $customer_id);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
//        preprint($sql);
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_customer_list_search_data($postData, $customer_id) {
        $sql = $this->get_customer_list_query($postData, $customer_id);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_customer_list_query($postData, $customer_id) {
        $column_order = array('a.customer_id', 'a.customer_name', 'a.company__name', 'a.customer_address', 'a.customer_mobile', 'a.customer_gst_no', 'a.balance');
        $sql = "SELECT a.* FROM (
                    SELECT b.*, (total_credit_amt - total_debit_amt) AS balance
                    FROM (
                        SELECT ci.*, s.name AS state_name, credit.cr AS total_credit_amt, debit.dr AS total_debit_amt
                        FROM customer_information ci 
                            LEFT JOIN states s ON s.id = ci.ref_state_id
                            LEFT JOIN (SELECT SUM(cl1.amount) as cr, cl1.customer_id 
                                       FROM customer_ledger cl1 
                                       WHERE cl1.d_c = 'c' AND cl1.is_deleted = 'Live' AND cl1.status = '1'
                                       GROUP BY cl1.customer_id) AS credit ON credit.customer_id = ci.customer_id
                            LEFT JOIN (SELECT SUM(cl2.amount) as dr, cl2.customer_id 
                                       FROM customer_ledger cl2 
                                       WHERE cl2.d_c = 'd' AND cl2.is_deleted = 'Live' AND cl2.status = '1'
                                       GROUP BY cl2.customer_id) AS debit ON debit.customer_id = ci.customer_id
                        WHERE ci.is_deleted = '1'";
        
        if (isset($customer_id) && !empty($customer_id)) {
            $sql .= " AND ci.customer_id = '$customer_id'";
        }
        
        $sql .= " GROUP BY ci.customer_id
                        ORDER BY ci.customer_id ASC
                    ) b
                ) a WHERE 1=1";
        
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.customer_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.customer_id ASC';
        }
        return $sql;
    }

    function get_paid_customer_list($postData, $customer_id) {
        $sql = $this->get_paid_customer_list_query($postData, $customer_id);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_paid_customer_list_search_data($postData, $customer_id) {
        $sql = $this->get_paid_customer_list_query($postData, $customer_id);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    public function checkExistingCustomer($model, $id = '') {
        if ($id != '') {

            $result = $this->db->select('company__name')
                    ->from('customer_information')
                    ->where('company__name', $model)
                    ->where('is_deleted', 1)
                    ->where('customer_id != ', $id)
                    ->get()
                    ->row('company__name');
        } else {
            $result = $this->db->select('company__name')
                    ->from('customer_information')
                    ->where('is_deleted', 1)
                    ->where('company__name', $model)
                    ->get()
                    ->row('company__name');
            
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }
    
    
    
    
    
    
    

    function get_paid_customer_list_query($postData, $customer_id) {
        $column_order = array('', 'a.customer_name', 'a.customer_address', 'a.customer_mobile', 'a.balance', '');
        $sql = "SELECT a.* FROM (
                    SELECT b.*, (credit - debit) AS balance 
                    FROM (SELECT *, (SELECT SUM(amount) FROM customer_ledger WHERE d_c ='d' AND customer_id = ci.customer_id) as  debit, 
                         IFNULL((SELECT SUM(amount) FROM customer_ledger WHERE d_c ='c' AND customer_id = ci.customer_id),0) as  credit
                         FROM customer_information ci 
                         WHERE ci.is_deleted = '1'";
        if (isset($customer_id) && !empty($customer_id) && $customer_id != '') {
            $sql .= " AND ci.customer_id = '$customer_id'";
        }
        $sql .= " ) b 
                ) a WHERE 1 = 1 AND a.balance <= 0";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.customer_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.customer_id DESC';
        }
        return $sql;
    }
}
