<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase_return_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insertPurchaseReturn($invoice_data) {
        $this->db->trans_start();
        $this->db->insert('purchase_return', $invoice_data);
        if ($this->db->affected_rows() > 0) {
            $PRID = $this->db->insert_id();
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        }
        return $PRID;
    }

    public function get_tax_list() {
//        $CI = & get_instance();
        $sql = "SELECT * FROM tax_information";
        return $CI->db->query($sql)->result();
    }

    public function get_product_list() {
        $sql = "SELECT sp.supplier_price,pi.*,
                       (SELECT GROUP_CONCAT(ti.tax_name SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax_name,
                       (SELECT GROUP_CONCAT(ti.tax SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax,
                       (SELECT GROUP_CONCAT(ti.tax_id SEPARATOR ', ') FROM tax_product tp INNER JOIN tax_information ti ON ti.tax_id = tp.tax_id AND ti.status = 1 WHERE tp.product_id = pi.product_id AND tp.status = 1) AS tax_id 
                FROM product_information pi
                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
                WHERE pi.status = '1' 
                GROUP BY pi.product_id 
                HAVING pi.product_id IS NOT NULL
                ORDER BY pi.product_id DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_company($limit) {
        $this->db->select('*');
        $this->db->from('company_information');
        $this->db->limit($limit);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function purchase_invoice_list() {
        $sql = "SELECT si.*,sii.* 
                FROM sale_invoice si 
                LEFT JOIN sale_invoice_item sii ON sii.refSIID = si.SIID
                WHERE si.status = '1' 
                GROUP BY si.SIID 
                HAVING si.SIID IS NOT NULL
                ORDER BY si.SIID DESC";
        return $this->db->query($sql)->result();
    }

    public function all_purchase_invoice_list() {
        $sql = "SELECT si.* 
                FROM sale_invoice si 
                WHERE si.status = '1' ";
        return $this->db->query($sql)->result();
    }

    public function product_edit_data($PINID) {
        $sql = "SELECT * FROM purchase_invoice WHERE PINID=$PINID AND status = '1' ";
        return $this->db->query($sql)->row_array();
    }

    public function purchaseInvoiceDataById($PINID) {
        $sql = "SELECT pi.*, si.supplier_name
                FROM purchase_invoice pi
                LEFT JOIN supplier_information si ON si.supplier_id = pi.RefSupplierID AND si.status = '1'
                WHERE pi.PINID = $PINID AND pi.status = '1'";
        $result = $this->db->query($sql)->row();
//        if (isset($result) && !empty($result)){
//            $result->purchase_invoice_item_data = $this->getPurchaseInvoiceItemDataByInvoiceId($result->PINID);
//        }
        return $result;
    }

    public function update_sale_invoice($SIID, $data) {
        $this->db->where('SIID', $SIID);
        $this->db->update('sale_invoice', $data);
        return TRUE;
    }

    public function deleteSaleReturnItem($SIID, $table_name) {
        $this->db->where('refPRID', $SIID);
        $this->db->update($table_name, array('status' => '0'));
        return TRUE;
    }

    public function deletePurchaseReturn($purchase_return_id) {
        $this->db->select('PRID');
        $this->db->from('purchase_return');
        $this->db->where('PRID', $purchase_return_id);
        $this->db->where('status', '1');
        $query = $this->db->get();
        $affected_row = $this->db->affected_rows();

        if ($affected_row > 0) {
            $this->db->where('PRID', $purchase_return_id);
            $this->db->update('purchase_return', array('status' => '0'));
            $this->db->where('refPRID', $purchase_return_id);
            $this->db->update('purchase_return_item', array('status' => '0'));
            $this->db->where('refPRID', $purchase_return_id);
            $this->db->update('purchase_return_itemtaxdet', array('status' => '0'));
            $this->db->where('chalan_no', $purchase_return_id);
            $this->db->where('ref_table_name', 'purchase_return');
            $this->db->update('supplier_ledger', array('is_deleted' => 'Deleted'));
            

            $this->db->select('*');
            $this->db->from('purchase_return');
            $this->db->where('status', '1');
            $query = $this->db->get();
            return true;
        } else {
            
            return false;
        }
    }

    public function get_tax_detail() {
        $sql = "SELECT * FROM tax_information WHERE status='1' ";
        return $this->db->query($sql)->result();
    }

    public function purchase_invoice_list_for_print($SIID) {
        $sql = "SELECT sii.*,GROUP_CONCAT(DISTINCT siit.refTaxID SEPARATOR ', ') AS Tax_id,GROUP_CONCAT(DISTINCT siit.refTaxName SEPARATOR ', ') AS tax_name,GROUP_CONCAT(DISTINCT siit.TaxPer SEPARATOR ', ') AS tax_per,SUM(DISTINCT siit.TaxAmt) AS TaxAmt,so.SONO,ps.PSNO 
                FROM sale_invoice_item sii 
                LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = sii.SIItemID 
                LEFT JOIN sale_outward so ON so.SOID = sii.refSOID 
                LEFT JOIN sale_outward_item soi ON soi.refSOID = so.SOID 
                LEFT JOIN product_sale ps ON ps.PSID = soi.refPSID
                WHERE sii.status = '1' AND sii.refSIID = '$SIID' 
                GROUP BY sii.SIItemID 
                HAVING sii.SIItemID IS NOT NULL";
        return $this->db->query($sql)->result();
    }

    public function invoice_tax_detail_for_print($SIID) {
        $sql = "SELECT CONCAT(siit.refTaxName,' ',siit.TaxPer,'%') as taxname, SUM(siit.TaxAmt) AS totaltax 
                FROM sale_invoice_itemtaxdet siit
                WHERE siit.refSIID = '$SIID' AND siit.status = '1' 
                GROUP BY siit.refTaxID 
                ORDER BY siit.refTaxName";
        return $this->db->query($sql)->result();
    }

    public function get_product_list_supllier($supplier_id) {
        $sql = "SELECT si.supplier_id,sp.supplier_price,pi.*,tp.tax_id,GROUP_CONCAT(ti.tax_name SEPARATOR ', ') AS tax_name,GROUP_CONCAT(ti.tax SEPARATOR ', ') AS tax,GROUP_CONCAT(ti.tax_id SEPARATOR ', ') AS tax_id  
                FROM product_information pi
                LEFT JOIN tax_product tp ON tp.product_id = pi.product_id
                LEFT JOIN tax_information ti ON ti.tax_id = tp.tax_id 
                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
                LEFT JOIN supplier_information si ON si.supplier_id = '$supplier_id' 
                WHERE sp.supplier_id = si.supplier_id AND pi.status = '1' 
                GROUP BY pi.product_id 
                HAVING pi.product_id IS NOT NULL
                ORDER BY pi.product_id DESC";
//        return $this->db->query($sql)->result();
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getSaleOutwardProductVise($product_id, $customer_id) {
        $sql = "SELECT soi.*,so.* 
                FROM sale_outward_item soi 
                LEFT JOIN sale_outward so ON so.SOID = soi.refSOID
                WHERE soi.status = '1' AND soi.refProductID = '$product_id' AND so.RefCustomerID = '$customer_id' ";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    public function get_product_list_with_so() {
        $sql = "SELECT sp.supplier_price,pi.*,tp.tax_id,GROUP_CONCAT(DISTINCT ti.tax_name SEPARATOR ', ') AS tax_name,GROUP_CONCAT(DISTINCT ti.tax SEPARATOR ', ') AS tax,GROUP_CONCAT(DISTINCT ti.tax_id SEPARATOR ', ') AS tax_id,GROUP_CONCAT(DISTINCT soi.refSOID SEPARATOR ', ') AS SOID,GROUP_CONCAT(DISTINCT so.SONO SEPARATOR ', ') AS SONO
                FROM product_information pi
                LEFT JOIN tax_product tp ON tp.product_id = pi.product_id
                LEFT JOIN tax_information ti ON ti.tax_id = tp.tax_id 
                LEFT JOIN supplier_product sp ON sp.product_id = pi.product_id 
                LEFT JOIN sale_outward_item soi ON soi.refProductID = pi.product_id
                LEFT JOIN sale_outward so ON so.SOID = soi.refSOID
                WHERE pi.status = '1' 
                GROUP BY pi.product_id 
                HAVING pi.product_id IS NOT NULL
                ORDER BY pi.product_id DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function getPurchaseReturnList() {
        $sql = "SELECT pr.* FROM purchase_return pr WHERE pr.status = '1' ";
        return $this->db->query($sql)->result();
    }

    public function getPurchaseReturnDataByReturnId($PRID) {
        $sql = "SELECT * FROM purchase_return WHERE PRID = $PRID AND status = '1' ";
        $result = $this->db->query($sql)->row();
        if (isset($result) && !empty($result)) {
            $result->purchase_return_item_data = $this->getPurchaseReturnItemDataByRetrunId($PRID, $result->refPINID);
            $result->purchase_return_item_tax_data = $this->getPurchaseReturnItemTaxDataByRetrunId($PRID);
            return $result;
        } else {
            return $result;
        }
    }

    public function getPurchaseReturnItemById($PRID) {
        $sql = "SELECT pri.*,GROUP_CONCAT(DISTINCT prit.refTaxID  SEPARATOR ',') AS Tax_id,GROUP_CONCAT(DISTINCT prit.refTaxName  SEPARATOR ',') AS tax_name,GROUP_CONCAT(DISTINCT prit.TaxPer  SEPARATOR ',') AS tax_per,SUM(prit.TaxAmt) AS TaxAmt
                FROM purchase_return_item pri 
                LEFT JOIN purchase_return_itemtaxdet prit ON prit.refPRItemID = pri.PRItemID
                WHERE pri.refPRID = '$PRID' AND pri.status = '1' AND prit.status = '1'
                GROUP BY pri.PRItemID";
        return $this->db->query($sql)->result();
    }

    public function purchaseInvoiceDataWithReturn($PRID) {
        $sql = "SELECT pr.PRID,pii.refProductID,pii.UnitAmt,pii.Qty,pri.PRItemID,pri.Qty AS returnQty,pri.Discount,pri.Remark,GROUP_CONCAT(piit.refTaxID) AS Tax_id,GROUP_CONCAT(piit.refTaxName) AS tax_name,GROUP_CONCAT(piit.TaxPer) AS tax_per
                FROM purchase_return pr
                INNER JOIN purchase_invoice_item pii ON pii.refPINID = pr.refPINID AND pii.status = '1'
                LEFT JOIN purchase_return_item pri ON pri.refPRID = pr.PRID AND pri.refProductID = pii.refProductID AND pri.status = '1'
                LEFT JOIN purchase_invoice_itemtaxdet piit ON piit.refPINItemID = pii.PINItemID AND piit.status = '1'
                WHERE pr.PRID = '$PRID' AND pr.status = '1'
                GROUP BY pii.refProductID";

        return $this->db->query($sql)->result();
    }

    public function purchaseReturnDataByID($PRID) {
        $sql = "SELECT pr.*, si.ref_state_id
                FROM purchase_return pr
                LEFT JOIN supplier_information si ON si.supplier_id = pr.RefSupplierID AND si.status = '1'
                WHERE pr.PRID = '$PRID'";
        return $this->db->query($sql)->row_array();
    }

    public function purchaseReturnItemDataById($PRID) {
        $sql = "SELECT pri.*,GROUP_CONCAT(prit.refTaxID  SEPARATOR ',') AS Tax_id,GROUP_CONCAT(prit.refTaxName  SEPARATOR ',') AS tax_name,GROUP_CONCAT(prit.TaxPer  SEPARATOR ',') AS tax_per,SUM(prit.TaxAmt) AS TaxAmt, pi.product_name
                FROM purchase_return_item pri
                LEFT JOIN purchase_return_itemtaxdet prit ON prit.refPRItemID = pri.PRItemID AND prit.status = '1'
                LEFT JOIN product_information pi ON pi.id = pri.refProductID AND pi.status = 1
                WHERE pri.refPRID = '$PRID' AND pri.status = '1'
                GROUP BY pri.PRItemID 
                HAVING pri.PRItemID IS NOT NULL";
        return $this->db->query($sql)->result();
    }

    public function purchaseReturnTaxDetail($PRID) {
        $sql = "SELECT CONCAT(prit.refTaxName,' ',TaxPer,'%') as taxname, SUM(prit.TaxAmt) AS totaltax
                FROM purchase_return_itemtaxdet prit
                WHERE prit.refPRID = '$PRID' AND prit.status = '1'
                GROUP BY prit.refTaxID
                ORDER BY prit.refTaxName";
        return $this->db->query($sql)->result();
    }

    public function getInwardDetail($PIID) {
        $sql = "SELECT COUNT(pi.PIID) AS inward_count, COUNT(pr.PRID) AS return_count, pi.PIID
                FROM product_inward pi
                LEFT JOIN purchase_return pr ON pr.refPIID = pi.PIID AND pr.status = '1' 
                WHERE pi.PIID = '$PIID' AND pi.status = '1'";
        return $this->db->query($sql)->row();
    }

    public function getApprovedSaleOutward() {
        $sql = "SELECT so.SOID, so.SONO 
                FROM sale_outward so
                INNER JOIN sale_outward_item soi ON soi.refSOID = so.SOID AND soi.status = '1'
                WHERE so.status = '1'
                GROUP BY so.SOID";
        return $this->db->query($sql)->result();
    }

    public function getApprovedPurchaseInward() {
        $sql = "SELECT pi.PIID, pi.PINO 
                FROM product_inward pi 
                INNER JOIN product_inward_item pii ON pii.refPIID = pi.PIID AND pii.status = '1'
                WHERE pi.status = '1'
                GROUP BY pi.PIID";
        return $this->db->query($sql)->result();
    }

    function get_purchase_return_list($postData) {
        $sql = $this->get_purchase_return_list_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
//        preprint($sql);
        return $query->result();
    }

    function get_purchase_return_list_search_data($postData) {
        $sql = $this->get_purchase_return_list_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_purchase_return_list_query($postData) {
        $column_order = array('', 'a.DateTime', 'a.supplier_name', 'a.product_count', 'a.GrandTotal', '');
        $sql = "SELECT a.*, DATE_FORMAT(a.DateTime,'%d-%m-%Y %H:%i') AS DateTime FROM (
                    SELECT pr.PRID, pr.PRNO, pr.DateTime, si.supplier_name, (SELECT COUNT(PRItemID) FROM purchase_return_item WHERE refPRID = pr.PRID AND status='1') AS product_count, pr.GrandTotal, pr.ReturnDescription, u1.user_name AS created_by, u2.user_name AS updated_by 
                    FROM purchase_return pr 
                        LEFT JOIN supplier_information si ON si.supplier_id = pr.RefSupplierID AND si.status = '1'
                        LEFT JOIN user_information u1 ON u1.id = pr.InsUser 
                        LEFT JOIN user_information u2 ON u2.id = pr.UpdUser 
                    WHERE pr.status = '1'
                ) a WHERE 1=1";

        if (isset($postData['start_date']) && !empty($postData['start_date'])) {
            $start_date = date_format(date_create($postData['start_date']), 'Y-m-d');
            $sql .= " AND DATE(a.DateTime) >= '" . $start_date . "'";
        }
        if (isset($postData['end_date']) && !empty($postData['end_date'])) {
            $end_date = date_format(date_create($postData['end_date']), 'Y-m-d');
            $sql .= " AND DATE(a.DateTime) <= '" . $end_date . "'";
        }

        $column_cnt = count($column_order);

        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.PRID";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.PRID DESC';
        }
//        preprint($sql);
        return $sql;
    }

    public function getPurchaseInvoiceData($invoice_no) {
        $sql = "SELECT pi.*, si.supplier_name
                FROM purchase_invoice pi
                LEFT JOIN supplier_information si ON pi.RefSupplierID = si.supplier_id
                WHERE pi.PINNO = '$invoice_no' AND pi.status = '1'";
        return $this->db->query($sql)->row();
    }

    public function purchase_invoice_product_details($invoice_id) {
        $sql = "SELECT pii.*,GROUP_CONCAT( piit.refTaxID SEPARATOR ',') AS Tax_id, GROUP_CONCAT( piit.refTaxName SEPARATOR ',') AS tax_name, GROUP_CONCAT( piit.TaxPer SEPARATOR ',') AS tax_per, SUM(piit.TaxAmt) AS TaxAmt, GROUP_CONCAT(DISTINCT p.id  SEPARATOR ',') AS product_id, GROUP_CONCAT(DISTINCT p.serial_no  SEPARATOR ',') AS barcode, CONCAT(p.product_name,' - (',pc.category_name,')') AS product_name_with_category, IFNULL(SUM(pri.Qty), 0) AS total_returned_qty, (pii.Qty - IFNULL(SUM(pri.Qty), 0)) AS returnable_qty
                FROM purchase_invoice_item pii
                LEFT JOIN purchase_invoice pi ON pi.PINID = pii.refPINID AND pi.status = '1'
                LEFT JOIN purchase_invoice_itemtaxdet piit ON pii.PINItemID = piit.refPINItemID AND piit.status = '1'
                LEFT JOIN product_information p ON pii.refProductID = p.id AND p.status = '1'
                LEFT JOIN product_category pc ON pc.category_id = p.category_id AND pc.status = '1'
                LEFT JOIN purchase_return pr ON pr.refPINID = pi.PINID AND pr.status = '1'
                LEFT JOIN purchase_return_item pri ON pri.refPRID = pr.PRID AND pri.status = '1' AND pri.refProductID = pii.refProductID
                WHERE pii.status = '1' AND pii.refPINID = $invoice_id
                GROUP BY pii.PINItemID";
        $result = $this->db->query($sql)->result();
//        if (isset($result) && !empty($result)) {
//            foreach ($result as $key => $value) {
//                $returned_qty_data = $this->getTotalReturnedQty($value->refPINID);
//                preprint($returned_qty_data, false);
//                $returned_qty = isset($returned_qty_data[$key]->return_qty) && !empty($returned_qty_data[$key]->return_qty) ? $returned_qty_data[$key]->return_qty : 0;
//                $result[$key]->total_returned_qty = $returned_qty;
//                $result[$key]->returnable_qty = (floatval($value->Qty) - floatval($returned_qty));
//            }
//        }
//        preprint($result);
        return $result;
    }

    public function generatePurchaseReturnNo() {
        $sql = "SELECT pr.PRNO FROM purchase_return pr ORDER BY pr.PRNO DESC LIMIT 1";
        $cnt = $this->db->query($sql)->row();
        if (!empty($cnt)) {
            return $cnt->PRNO;
        }
    }

    public function getPurchaseReturnItemDataByRetrunId($PRID, $PINID) {
        $sql = "SELECT pri.*, pi.product_name, ii.Qty as invoice_qty
                FROM purchase_return_item pri 
                    INNER JOIN product_information pi ON pi.id = pri.refProductID
                    INNER JOIN purchase_return pr ON pr.PRID = pri.refPRID 
                    INNER JOIN purchase_invoice i ON i.PINID = pr.refPINID 
                    INNER JOIN purchase_invoice_item ii ON ii.refPINID = i.PINID AND ii.refProductID = pri.refProductID 
                WHERE pri.refPRID = $PRID
                    AND pri.status = '1' 
                ORDER BY pri.PRItemID ASC";
        $result = $this->db->query($sql)->result();
        if (isset($result) && !empty($result)) {
            foreach ($result as $k => $val) {
                $returned_qty_data = $this->getTotalReturnedQtyByProductId($PINID, $val->refProductID, $val->refPRID);
                $returned_qty = isset($returned_qty_data->return_qty) && !empty($returned_qty_data->return_qty) ? $returned_qty_data->return_qty : 0;
                $result[$k]->total_returned_qty = $returned_qty;
                $result[$k]->returnable_qty = (floatval($val->invoice_qty) - floatval($returned_qty));
            }
        }
        return $result;
    }

    public function getPurchaseReturnItemTaxDataByRetrunId($PRID) {
        $sql = "SELECT prit.* 
                FROM purchase_return_itemtaxdet prit 
                WHERE prit.refPRID = $PRID AND prit.status = '1' 
                ORDER BY prit.PRItemTaxID ASC";
        return $this->db->query($sql)->result();
    }

    public function getTotalReturnedQty($PINID) {
        $sql = "SELECT pri.refPRID, pri.refProductID, IFNULL(SUM(pri.Qty),0) AS return_qty
                FROM purchase_return pr
                    LEFT JOIN purchase_return_item pri ON pri.refPRID = pr.PRID AND (pri.status = '1')
                WHERE pr.status = '1' AND pr.refPINID = $PINID
                GROUP BY pri.refProductID
                ORDER BY pri.refProductID ASC";
        return $this->db->query($sql)->result();
    }

    public function getTotalReturnedQtyByProductId($PINID, $product_id, $PRID) {
        $sql = "SELECT a.* FROM
			(SELECT IFNULL(SUM(pri.Qty),0) AS return_qty
                FROM purchase_return pr
                    LEFT JOIN purchase_return_item pri ON pri.refPRID = pr.PRID AND (pri.status = '1')
                WHERE pr.status = '1' AND pr.refPINID = $PINID AND pri.refProductID = $product_id AND pri.refPRID != $PRID) AS a";
        return $this->db->query($sql)->row();
    }

    public function getPurchaseInvoiceItemDataByInvoiceId($PINID) {
        $sql = "SELECT pii.*,piit.PINItemTaxID, piit.refTaxID, piit.refTaxName, piit.TaxPer, piit.TaxAmt
                FROM purchase_invoice_item pii
                    LEFT JOIN purchase_invoice_itemtaxdet piit ON piit.refPINItemID = pii.PINItemID AND piit.status = '1'
                WHERE pii.refPINID = $PINID AND pii.status = '1'
                ORDER BY pii.refProductID ASC";
        return $this->db->query($sql)->result();
    }
}
