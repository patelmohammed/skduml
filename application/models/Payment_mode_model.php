<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_mode_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //customer List
    public function payment_mode_list() {
        $this->db->select('*');
        $this->db->from('payment_mode_list');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //customer List
    public function payment_mode_list_product() {
        $this->db->select('*');
        $this->db->from('payment_mode_list');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

//    //group list
//    public function group_list_product() {
//        $this->db->select('*');
//        $this->db->from('product_group');
//        $this->db->where('status', 1);
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return false;
//    }

//    //manufacturing list
//    public function mfg_list_product() {
//        $this->db->select('*');
//        $this->db->from('product_mfg');
//        $this->db->where('status', 1);
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return false;
//    }

    //customer List
    public function payment_mode_list_count() {
        $this->db->select('*');
        $this->db->from('payment_mode_list');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    //Category Search Item
    public function payment_mode_search_item($payment_mode_id) {
        $this->db->select('*');
        $this->db->from('payment_mode_list');
        $this->db->where('payment_mode_id', $payment_mode_id);
        $this->db->limit('500');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Count customer
    public function payment_mode_entry($data) {
      
        
        $this->db->select('*');
        $this->db->from('payment_mode_list');
        $this->db->where('status', 1);
        $this->db->where('payment_mode_name', $data['payment_mode_name']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
//            echo '<pre>';
//            print_r($data);
//            die();
            $this->db->insert('payment_mode_list', $data);
            
            $result = $this->db->insert_id();
            
            return $result;
            
        }
    }

    //Retrieve customer Edit Data
    public function retrieve_payment_mode_editdata($payment_mode_id) {
        $this->db->select('*');
        $this->db->from('payment_mode_list');
        $this->db->where('payment_mode_id', $payment_mode_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Update Categories
    public function update_payment_mode($data, $payment_mode_id) {
        $this->db->where('payment_mode_id', $payment_mode_id);
        $this->db->update('payment_mode_list', $data);
        return true;
    }
    
    function updateStatus($data, $payment_mode_id) {        
        $this->db->where('payment_mode_id', $payment_mode_id);
        $this->db->update('payment_mode_list', $data);

        return true;
    }

    // Delete customer Item
    public function delete_payment_mode($payment_mode_id) {
        $this->db->where('payment_mode_id', $payment_mode_id);
        $this->db->update('payment_mode_list', ['status' => 0]);
        return true;
    }

    function get_payment_mode_list($postData) {
        $sql = $this->get_payment_mode_list_query($postData);
        if (isset($postData['length']) && $postData['length'] != -1) {
            $sql .= ' LIMIT ' . $postData['length'] . ' OFFSET ' . $postData['start'];
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_payment_mode_list_search_data($postData) {
        $sql = $this->get_payment_mode_list_query($postData);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_payment_mode_list_query($postData) {
        $column_order = array('', 'a.payment_mode_name', '');
        $sql = "SELECT a.* FROM (
                    SELECT * 
                    FROM payment_mode_list ml
                    WHERE ml.status = '1'
                ) a WHERE 1=1";
        $column_cnt = count($column_order);
        if (!empty($postData['search']['value'])) {
            $sql .= " AND (";
            foreach ($column_order as $key => $search_field) {
                if ($key > 0 && $key < ($column_cnt - 1)) {
                    if (!empty($postData['search']['value']) && !empty($search_field)) {
                        $sql .= " $search_field LIKE '%" . trim($postData['search']['value']) . "%'";
                        if ($key < ($column_cnt - 2)) {
                            $sql .= " OR";
                        }
                    }
                }
            }
            $sql .= " )";
        }
        $sql .= " GROUP BY a.payment_mode_id";
        if (isset($postData['order'])) {
            $sql .= " ORDER BY " . $column_order[$postData['order']['0']['column']] . ' ' . $postData['order']['0']['dir'];
        } else {
            $sql .= ' ORDER BY a.payment_mode_id ASC';
        }
        return $sql;
    }
    
    
    
    public function checkExistingPaymentmode($model, $id = '') {
      
        if ($id != '') {

            $result = $this->db->select('payment_mode_name')
                    ->from('payment_mode_list')
                    ->where('payment_mode_name', $model)
                    ->where('status', 1)
                    ->where('payment_mode_id != ', $id)
                    ->get()
                    ->row('payment_mode_name');
        } else {
            $result = $this->db->select('payment_mode_name')
                    ->from('payment_mode_list')
                   
                    ->where('payment_mode_name', $model)
                    ->get()
                    ->row('payment_mode_name');
            
        }

        if (!empty($result) && $result != '') {
            return true;
        } else {
            return false;
        }
    }

}
