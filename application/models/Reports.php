<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class reports extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    //Count report
    public function count_stock_report() {
        $this->db->select("a.product_name,a.product_id,a.cartoon_quantity,a.price,a.product_model,sum(b.quantity) as 'totalSalesQnty',(select sum(product_purchase_details.quantity) from product_purchase_details where product_id= `a`.`product_id`) as 'totalBuyQnty'");
        $this->db->from('product_information a');
        $this->db->join('invoice_details b', 'b.product_id = a.product_id');
        $this->db->where(array('a.status' => 1, 'b.status' => 1));
        $this->db->group_by('a.product_id');
        $query = $this->db->get();
        return $query->num_rows();
    }

    //Out of stock
    public function out_of_stock() {
        $sql = "SELECT pi.*, pc.category_name, IFNULL((SUM(sh.opening_stock) + SUM(sh.Purchase) - SUM(sh.purchase_return) - SUM(sh.sell) + SUM(sh.sell_return)),0) AS stock
                FROM product_information pi
                    LEFT JOIN stock_history sh ON sh.product_id = pi.id
                    INNER JOIN product_category pc ON pc.category_id = pi.category_id
                WHERE pi.status = 1
                GROUP BY pi.id
                HAVING stock < " . MIN_STOCK . "
                ORDER BY pi.product_name ASC";
        return $this->db->query($sql)->result_array();
    }

    //Out of stock count
    public function out_of_stock_count() {
        $sql = "SELECT pi.*, pc.category_name, IFNULL((SUM(sh.opening_stock) + SUM(sh.Purchase) - SUM(sh.purchase_return) - SUM(sh.sell) + SUM(sh.sell_return)),0) AS stock
                FROM product_information pi
                    LEFT JOIN stock_history sh ON sh.product_id = pi.id
                    INNER JOIN product_category pc ON pc.category_id = pi.category_id
                WHERE pi.status = 1
                GROUP BY pi.id
                HAVING stock < " . MIN_STOCK . "
                ORDER BY pi.product_name ASC";
        return $this->db->query($sql)->num_rows();
    }

    //Retrieve Single Item Stock Stock Report
    public function stock_report($limit, $page) {
        $this->db->select("a.product_name,a.product_id,a.cartoon_quantity,a.price, a.product_model,sum(b.quantity) as 'totalSalesQnty',(select sum(product_purchase_details.quantity) from product_purchase_details where product_id= `a`.`product_id`) as 'totalBuyQnty'");
        $this->db->from('product_information a');
        $this->db->join('invoice_details b', 'b.product_id = a.product_id');
        $this->db->where(array('a.status' => 1, 'b.status' => 1));
        $this->db->group_by('a.product_id');
        $this->db->order_by('a.product_id', 'desc');
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Retrieve Single Item Stock Stock Report
    public function stock_report_single_item($product_id) {
        $this->db->select("a.product_name,a.cartoon_quantity,a.price,a.product_model,sum(b.quantity) as 'totalSalesQnty',sum(c.quantity) as 'totalBuyQnty'");
        $this->db->from('product_information a');
        $this->db->join('invoice_details b', 'b.product_id = a.product_id');
        $this->db->join('product_purchase_details c', 'c.product_id = a.product_id');
        $this->db->where(array('a.product_id' => $product_id, 'a.status' => 1, 'b.status' => 1));
        $this->db->group_by('a.product_id');
        $this->db->order_by('a.product_id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Stock Report by date
    public function stock_report_bydate($product_id = '', $start_date = '', $end_date = '') {
        $sql = "SELECT pi.id,pi.product_name,pc.category_name,pi.price,pi.product_model,pi.serial_no,pi.unit, si.supplier_name, si.supplier_id, pg.product_group_name, 
                    (SUM(sh.opening_stock) + SUM(sh.Purchase) + SUM(sh.sell_return))AS inqty,
                    (SUM(sh.sell) + SUM(sh.purchase_return)) AS outqty,
                    (SUM(sh.opening_stock) + SUM(sh.Purchase) - SUM(sh.sell) + SUM(sh.sell_return) - SUM(sh.purchase_return))AS stock
                FROM product_information pi
                    LEFT JOIN stock_history sh ON sh.product_id = pi.id 
                    LEFT JOIN supplier_information si ON si.supplier_id = sh.supplier_id AND si.status = 1 
                    LEFT JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = 1
                    INNER JOIN product_group pg ON pg.id = pi.product_group_id AND pg.status = 1
                WHERE pi.status = '1'";

        if (!empty($product_id)) {
            $sql .= " AND pi.id = '$product_id'";
        }

        if (!empty($start_date)) {
            $sql .= " AND sh.vdate >= '$start_date'";
        }

        if (!empty($end_date)) {
            $sql .= " AND sh.vdate <= '$end_date'";
        }

        $sql .= " GROUP BY pi.id, sh.supplier_id
                ORDER BY pi.product_name ASC";

//        preprint($sql);
        $result = $this->db->query($sql)->result();
        if (!empty($result)) {
            return $result;
        }
        return FALSE;
    }

//Stock report supplier by date
    public function stock_report_supplier_bydate($product_id = null, $supplier_id = null, $start_date = null, $end_date = null) {
        $sql = "SELECT pi.id,pi.product_name,sh.supplier_id, si.supplier_name, pc.category_name,pg.product_group_name,pi.price,pi.product_model,pi.serial_no,pi.unit,
                (SUM(sh.opening_stock) + SUM(sh.Purchase) + SUM(sh.sell_return)) AS inqty,
                (SUM(sh.sell) + SUM(sh.purchase_return)) AS outqty,
                (SUM(sh.opening_stock) + SUM(sh.Purchase) - SUM(sh.sell) + SUM(sh.sell_return) - SUM(sh.purchase_return)) AS stock
                FROM product_information pi
                    INNER JOIN stock_history sh ON sh.product_id = pi.id
                    INNER JOIN supplier_information si ON si.supplier_id = sh.supplier_id
                    INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                    INNER JOIN product_group pg ON pg.id = pi.product_group_id 
                WHERE (pi.product_group_id IN (1,2,3,4) AND pi.status = '1')";

        if (!empty($start_date)) {
            $sql .= " AND sh.vdate >= '$start_date' ";
        }
        if (!empty($end_date)) {
            $sql .= " AND sh.vdate <= '$end_date' ";
        }
        if (!empty($product_id)) {
            $sql .= " AND sh.product_id = $product_id";
        }
        if (!empty($supplier_id)) {
            $sql .= " AND sh.supplier_id = $supplier_id";
        }
        $sql .= " GROUP BY pi.id,sh.supplier_id ORDER BY pi.product_name ASC";
        $result = $this->db->query($sql)->result();
        if (!empty($result)) {
            return $result;
        }
        return FALSE;
    }

    //Counter of unique product histor which has been affected
    public function product_counter_by_supplier($supplier_id) {
        $this->db->select('DISTINCT(a.product_id)');
        $this->db->from('product_information a');
        $this->db->join('supplier_product b', 'b.product_id=a.product_id');
        $this->db->where('a.status', 1);
        if (!empty($supplier_id)) {
            $this->db->where('b.supplier_id =', $supplier_id);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    //Stock report supplier by date
    public function stock_report_product_bydate($product_id = null, $from_date = null, $to_date = null) {
        $sql = "SELECT pi.id,pi.product_name,pc.category_name,pg.product_group_name,pi.price,pi.product_model,pi.serial_no,pi.unit, si.supplier_name, 
                (SUM(sh.opening_stock) + SUM(sh.Purchase) - SUM(sh.sell) + SUM(sh.sell_return) - SUM(sh.purchase_return)) AS stock
                FROM product_information pi
                    INNER JOIN stock_history sh ON sh.product_id = pi.id 
                    INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                    INNER JOIN product_group pg ON pg.id = pi.product_group_id 
                    INNER JOIN supplier_information si ON si.supplier_id = sh.supplier_id 
                WHERE (pi.product_group_id IN (1,3) AND pi.status = '1')";

        if (!empty($from_date)) {
            $sql .= " AND sh.vdate >= '$from_date' ";
        }

        if (!empty($to_date)) {
            $sql .= " AND sh.vdate <= '$to_date' ";
        }

        if (!empty($product_id)) {
            $sql .= " AND sh.product_id = $product_id ";
        }

        $sql .= " GROUP BY pi.id, si.supplier_id ORDER BY pi.product_name ASC";

        $result = $this->db->query($sql)->result();
        if (!empty($result)) {
            return $result;
        }
        return FALSE;
    }

    //Stock report supplier by date
    public function stock_report_product_bydate_count($product_id, $supplier_id, $from_date, $to_date) {

        $this->db->select("a.product_name,a.product_id,price,a.product_model,sum(b.sell) as 'totalSalesQnty',sum(b.Purchase) as 'totalPurchaseQnty',b.vdate as date");
        $this->db->from('product_information a');
        $this->db->join('stock_history b', 'b.product_id = a.product_id');
        $this->db->order_by('b.vdate', 'ASC');

        if (empty($supplier_id)) {
            $this->db->where(array('a.status' => 1));
            $this->db->group_by('b.product_id');
        } else {
            $this->db->where(
                    array(
                        'a.status' => 1,
                        'a.product_id' => $product_id
                    )
            );
            $this->db->where('b.vdate >=', $from_date);
            $this->db->where('b.vdate <=', $to_date);
            $this->db->group_by('b.vdate');
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    //Counter of unique product histor which has been affected
    public function product_counter($product_id) {
        $this->db->select('DISTINCT(product_id)');
        $this->db->from('product_information');
        $this->db->where('status', 1);
        if (!empty($product_id)) {
            $this->db->where('product_id =', $product_id);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    //Retrieve todays_total_sales_report
    public function todays_total_sales_report() {
        $today = date('Y-m-d');
        $sql = "SELECT sum(so.GrandTotal) AS GrandTotal
                FROM sale_outward so 
                WHERE so.status = '1' AND DATE_FORMAT(so.DateTime,'%Y-%m-%d') = '$today'";
        return 0; //$this->db->query($sql)->row()->GrandTotal;
    }

    //Retrieve todays_total_sales_report
    public function todays_total_purchase_report() {
        $today = date('Y-m-d');
        $sql = "SELECT sum(pi.GrandTotal) AS GrandTotal 
                FROM product_inward pi 
                WHERE pi.status = '1' AND DATE_FORMAT(pi.DateTime,'%Y-%m-%d') = '$today'";
        return 0; //$this->db->query($sql)->row()->GrandTotal;
    }

    // todays sales product
    public function todays_sale_product() {
        $today = date('Y-m-d');
        $sql = "SELECT so.SOID,so.SONO,so.DateTime,SUM(soi.Qty)AS totalQty,so.GrandTotal,soi.TotalAmt,so.RefCustomerID,pi.id,pi.product_name,pi.product_model,pc.category_name,so.BasicTotal,so.Discount,so.Taxtotal,sum(soit.TaxAmt)AS TaxTotal 
                FROM sale_outward_item soi
                LEFT JOIN sale_outward so ON so.SOID = soi.refSOID AND so.status = '1'
                LEFT JOIN sale_outward_itemtaxdet soit ON soit.refSOItemID = soi.SOItemID AND soit.status = '1'
                LEFT JOIN product_information pi ON pi.id = soi.refProductID AND pi.status = '1'
                LEFT JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                WHERE soi.status = '1' AND DATE_FORMAT(so.DateTime,'%Y-%m-%d') = '$today'
                GROUP BY soi.SOItemID 
                ORDER BY soi.SOItemID DESC                
                LIMIT 2";
        return []; //$this->db->query($sql)->result();
    }

    //Retrieve todays_sales_report
    public function todays_sales_report($per_page = '', $page = '') {
        $today = date('Y-m-d');
        $sql = "SELECT si.SIID,si.SINO,si.DateTime,SUM(sii.Qty)AS totalQty,si.GrandTotal,si.RefCustomerID,pi.id,pi.product_name,pi.product_model,pc.category_name,si.BasicTotal,si.DiscountTotal,si.Taxtotal  
                FROM sale_invoice_item sii
                    INNER JOIN sale_invoice si ON si.SIID = sii.refSIID AND si.status = '1'
                    INNER JOIN product_information pi ON pi.id = sii.refProductID AND pi.status = '1'
                    INNER JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                WHERE sii.status = '1' AND DATE_FORMAT(si.DateTime,'%Y-%m-%d') = '$today'
                GROUP BY si.SIID 
                ORDER BY si.SIID ASC";
        return $this->db->query($sql)->result();
    }

    //Retrieve todays_sales_report_count
    public function todays_sales_report_count() {
        $today = date('Y-m-d');
        $this->db->select("a.*, b.customer_id, b.customer_name");
        $this->db->from('invoice a');
        $this->db->join('customer_information b', 'b.customer_id = a.customer_id');
        $this->db->where('a.date', $today);
        $this->db->order_by('a.invoice_id', 'desc');
        $query = $this->db->get();
        return $query->num_rows();
    }

//     =============== its for purchase_report_category_wise_count =============
    public function purchase_report_category_wise_count() {
        
    }

//    ============= its for purchase_report_category_wise ===============
    public function purchase_report_category_wise($product_id = null, $supplier_id = null, $category_id = null, $start_date = null, $end_date = null) {
        $sql = "SELECT pin.PINID,pin.PINNO,pi.id,pc.category_name,pi.product_name,pi.product_model,pin.DateTime, pin.RefSupplierID, si.supplier_name, AVG(pii.UnitAmt) as avg_purchase_price, SUM(pii.Qty) as total_qty, SUM(pii.DiscountAmt) as total_discount_amt, SUM(piit.TaxAmt) AS total_tax_amt
                FROM purchase_invoice_item pii 
                    INNER JOIN purchase_invoice pin ON pin.PINID = pii.refPINID
                    INNER JOIN purchase_invoice_itemtaxdet piit ON piit.refPINItemID = pii.PINItemID
                    INNER JOIN product_information pi ON pi.id = pii.refProductID
                    INNER JOIN product_category pc ON pc.category_id = pi.category_id
                    INNER JOIN supplier_information si ON si.supplier_id = pin.RefSupplierID
                WHERE pii.status = '1'";

        if (isset($product_id) && !empty($product_id)) {
            $sql .= " AND pi.id = '$product_id'";
        }
        if (isset($supplier_id) && !empty($supplier_id)) {
            $sql .= " AND si.supplier_id = '$supplier_id'";
        }
        if (isset($category_id) && !empty($category_id)) {
            $sql .= " AND pi.category_id = '$category_id'";
        }
        if (isset($start_date) && !empty($start_date)) {
            $sql .= " AND DATE_FORMAT(pin.DateTime,'%Y-%m-%d') >= '$start_date'";
        }
        if (isset($end_date) && !empty($end_date)) {
            $sql .= " AND DATE_FORMAT(pin.DateTime,'%Y-%m-%d') <= '$end_date'";
        }

        $sql .= " GROUP BY pii.PINItemID
                ORDER BY pin.PINID,pc.category_name,pi.product_name ASC";

        return $this->db->query($sql)->result();
    }

//    ============= its for purchase_report_category_wise ===============
    public function filter_purchase_report_category_wise($category = null, $from_date = null, $to_date = null, $per_page = null, $page = null) {
        $sql = "SELECT pin.PIID,pin.PINO,pi.product_id,pc.category_name,pi.product_name,pi.product_model,pin.DateTime,SUM(pii.Qty)AS totalQty,pin.GrandTotal,pin.RefSupplierID,pii.LandedCost,pii.UnitAmt,pii.Qty,pii.Discount,sum(piit.TaxAmt) AS TaxAmt,pii.TotalAmt 
                FROM product_inward_item pii 
                INNER JOIN product_inward pin ON pin.PIID = pii.refPIID AND pin.status = '1'
                INNER JOIN product_inward_itemtaxdet piit ON piit.refPIItemID = pii.PIItemID AND piit.status = '1'
                INNER JOIN product_information pi ON pi.product_id = pii.refProductID AND pi.status = '1'
                INNER JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                WHERE pii.status = '1' ";

        if (isset($category) && !empty($category)) {
            $sql .= " AND pc.category_id = '$category'";
        }
        if (isset($from_date) && !empty($from_date)) {
            $sql .= " AND DATE_FORMAT(pin.DateTime,'%Y-%m-%d') >= '$from_date'";
        }
        if (isset($to_date) && !empty($to_date)) {
            $sql .= " AND DATE_FORMAT(pin.DateTime,'%Y-%m-%d') <= '$to_date'";
        }
        $sql .= " GROUP BY pii.PIItemID 
                  HAVING pin.PIID IS NOT NULL";
        return $this->db->query($sql)->result();
    }

//    =============== its for sales_report_category_wise_count =============
    public function sales_report_category_wise_count() {
        
    }

//    ============= its for sales_report_category_wise ===============
    public function sales_report_category_wise($per_page = null, $page = null) {
        $sql = "SELECT si.SIID, si.SINO, pi.id, pc.category_name, CONCAT(pi.product_name, ' - (',pc.category_name,')') AS product_name, si.DateTime, SUM(sii.Qty) AS totalQty, AVG(sii.UnitAmt) AS totalPrice,SUM(sii.DiscountAmt) AS totalDiscount, SUM(sii.TotalAmt) AS totalAmtAfterDisc, SUM(siit.TaxAmt) AS totalTax, si.RefCustomerID
                FROM sale_invoice_item sii
                    LEFT JOIN sale_invoice si ON si.SIID = sii.refSIID AND si.status = '1'
                    LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = sii.SIItemID AND siit.status = '1' 
                    LEFT JOIN product_information pi ON pi.id = sii.refProductID AND pi.status = '1'
                    LEFT JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                WHERE sii.status = '1'
                GROUP BY sii.refProductID, si.SIID
                ORDER BY si.SIID, sii.refProductID ASC ";
        return $this->db->query($sql)->result();
    }

//    ============= its for filter_sales_report_category_wise ===============
    public function filter_sales_report_category_wise($category = null, $from_date = null, $to_date = null, $per_page = null, $page = null) {
        $sql = "SELECT so.SOID,so.SONO,pi.product_id,pc.category_name,pi.product_name,pi.product_model,so.DateTime,SUM(soi.Qty)AS totalQty,so.GrandTotal,so.RefCustomerID,soi.UnitAmt,soi.Qty,soi.Discount,sum(soit.TaxAmt) AS TaxAmt,soi.TotalAmt 
                FROM sale_outward_item soi
                INNER JOIN sale_outward so ON so.SOID = soi.refSOID AND so.status = '1'
                INNER JOIN sale_outward_itemtaxdet soit ON soit.refSOItemID = soi.SOItemID AND soit.status = '1'
                INNER JOIN product_information pi ON pi.product_id = soi.refProductID AND pi.status = '1'
                INNER JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                WHERE soi.status = '1'";
        if (isset($category) && !empty($category)) {
            $sql .= " AND pc.category_id = '$category'";
        }
        if (isset($from_date) && !empty($from_date)) {
            $sql .= " AND DATE_FORMAT(so.DateTime,'%Y-%m-%d') >= '$from_date'";
        }
        if (isset($to_date) && !empty($to_date)) {
            $sql .= " AND DATE_FORMAT(so.DateTime,'%Y-%m-%d') <= '$to_date'";
        }
        $sql .= "  GROUP BY soi.SOItemID 
                  HAVING so.SOID IS NOT NULL";
        return $this->db->query($sql)->result();
    }

//Retrieve todays_purchase_report
    public function todays_purchase_report($per_page = '', $page = '') {
        $today = date('Y-m-d');
        $sql = "SELECT pin.PINID,pin.PINNO,pi.id,pc.category_name,pi.product_name,pi.product_model,pin.DateTime,SUM(pii.Qty)AS totalQty,pin.GrandTotal,pin.RefSupplierID,pin.BasicTotal,pin.Discount,pin.Taxtotal,SUM(sp.LandedCost) AS TotalLandedCost
                FROM purchase_invoice_item pii 
                    INNER JOIN purchase_invoice pin ON pin.PINID = pii.refPINID AND pin.status = '1'
                    INNER JOIN product_information pi ON pi.id = pii.refProductID AND pi.status = '1'
                    INNER JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                    LEFT JOIN supplier_product sp ON sp.product_id = pii.refProductID AND sp.status = '1'
                WHERE pii.status = '1' AND DATE_FORMAT(pin.DateTime,'%Y-%m-%d') = '$today' GROUP BY pin.PINID HAVING pin.PINID IS NOT NULL";
        return $this->db->query($sql)->result();
    }

    //Retrieve todays_purchase_report count
    public function todays_purchase_report_count() {
        $today = date('Y-m-d');
        $this->db->select("a.*, b.supplier_id, b.supplier_name");
        $this->db->from('product_purchase a');
        $this->db->join('supplier_information b', 'b.supplier_id = a.supplier_id');
        $this->db->where('a.purchase_date', $today);
        $this->db->order_by('a.purchase_id', 'desc');
        $this->db->limit('500');
        $query = $this->db->get();
        return $query->num_rows();
    }

    //Total profit report
    public function total_profit_report($perpage = '', $page = '') {
        $sql = "SELECT DATE(so.DateTime) AS DateTime, so.SIID, so.SINO, sum(so.GrandTotal) AS total_sale, SUM(soi.Qty * sp.supplier_price) AS total_supplier_rate, SUM(so.GrandTotal) - SUM(soi.Qty * sp.supplier_price) AS total_profit 
                FROM sale_invoice so
                    INNER JOIN sale_invoice_item soi ON soi.refSIID = so.SIID AND so.status = '1'
                    INNER JOIN supplier_product sp ON sp.product_id = soi.refProductID 
                GROUP BY so.SIID
                ORDER BY so.SINO DESC";
        return $this->db->query($sql)->result();
    }

    //Total profit report
    public function total_profit_report_count() {
        $this->db->select("a.date, a.invoice, b.invoice_id, sum(total_price) as total_sale");
        $this->db->select('sum(`quantity`*`supplier_rate`) as total_supplier_rate', FALSE);
        $this->db->select("(SUM(total_price) - SUM(`quantity`*`supplier_rate`)) AS total_profit");
        $this->db->from('invoice a');
        $this->db->join('invoice_details b', 'b.invoice_id = a.invoice_id');
        $this->db->group_by('b.invoice_id');
        $this->db->order_by('a.invoice', 'desc');
        $query = $this->db->get();
        return $query->num_rows();
    }

    //Retrieve all Report
    public function retrieve_dateWise_SalesReports($from_date, $to_date, $per_page, $page) {
        $sql = "SELECT so.SOID,so.SONO,so.DateTime,SUM(soi.Qty)AS totalQty,so.GrandTotal,so.RefCustomerID,pi.product_id,pi.product_name,pi.product_model,pc.category_name,so.BasicTotal,so.Discount,so.Taxtotal   
                FROM sale_outward_item soi
                INNER JOIN sale_outward so ON so.SOID = soi.refSOID AND so.status = '1'
                INNER JOIN product_information pi ON pi.product_id = soi.refProductID AND pi.status = '1'
                INNER JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                WHERE soi.status = '1'";
        if (isset($from_date) && !empty($from_date)) {
            $sql .= " AND DATE_FORMAT(so.DateTime,'%Y-%m-%d') >= '$from_date'";
        }
        if (isset($to_date) && !empty($to_date)) {
            $sql .= " AND DATE_FORMAT(so.DateTime,'%Y-%m-%d') <= '$to_date'";
        }
        $sql .= "  GROUP BY so.SOID";
        return $this->db->query($sql)->result();
    }

    // count sales report data
    public function count_retrieve_dateWise_SalesReports($from_date, $to_date) {
        //	$dateRange = "a.date BETWEEN '$from_date%' AND '$to_date%'";
        $this->db->select("a.*, b.*");
        $this->db->from('invoice a');
        $this->db->join('customer_information b', 'b.customer_id = a.customer_id');
        $this->db->where('a.date >=', $from_date);
        $this->db->where('a.date <=', $to_date);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    //Retrieve all Report
    public function retrieve_dateWise_PurchaseReports($start_date, $end_date, $per_page, $page) {
        $sql = "SELECT pi.PIID, pi.PINO, pi.DateTime, pi.RefSupplierID, pi.BasicTotal, pi.TotalLandedCost, pi.Discount, pi.Taxtotal, pi.GrandTotal
                FROM product_inward pi
                INNER JOIN product_inward_item pii ON pii.refPIID = pi.PIID AND pii.status = '1'
                WHERE pi.status = '1' AND DATE_FORMAT(pi.DateTime, '%Y-%m-%d') >= '$start_date' AND DATE_FORMAT(pi.DateTime, '%Y-%m-%d') <= '$end_date'
                GROUP BY pi.PIID ";
        return $this->db->query($sql)->result();
    }

    // count purchase report data
    public function count_retrieve_dateWise_PurchaseReports($start_date, $end_date) {
        //$dateRange = "purchase_date BETWEEN '$start_date%' AND '$end_date%'";
        $this->db->select("a.*, b.supplier_id, b.supplier_name");
        $this->db->from('product_purchase a');
        $this->db->join('supplier_information b', 'b.supplier_id = a.supplier_id');
        $this->db->where('a.purchase_date >=', $start_date);
        $this->db->where('a.purchase_date <=', $end_date);
        $this->db->group_by('a.purchase_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    //Retrieve date wise profit report
    public function retrieve_dateWise_profit_report($start_date, $end_date, $per_page, $page) {
        $this->db->select("a.date, a.invoice, b.invoice_id, CAST(sum(total_price) AS DECIMAL(16, 2)) as total_sale");
        $this->db->select('CAST(sum(`quantity`*`supplier_rate`) AS DECIMAL(16,2)) as total_supplier_rate', FALSE);
        $this->db->select("CAST(SUM(total_price) - SUM(`quantity`*`supplier_rate`) AS DECIMAL(16, 2)) AS total_profit");
        $this->db->from('invoice a');
        $this->db->join('invoice_details b', 'b.invoice_id = a.invoice_id');
        // $this->db->where($dateRange, NULL, FALSE); 
        $this->db->where('a.date >=', $start_date);
        $this->db->where('a.date <=', $end_date);
        $this->db->group_by('b.invoice_id');
        $this->db->order_by('a.invoice', 'desc');
        $this->db->limit($per_page, $page);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Retrieve date wise profit report
    public function retrieve_dateWise_profit_report_count($start_date, $end_date) {
        // $dateRange = "a.date BETWEEN '$start_date%' AND '$end_date%'";
        $this->db->select("a.date, a.invoice, b.invoice_id, sum(total_price) as total_sale");
        $this->db->select('sum(`quantity`*`supplier_rate`) as total_supplier_rate', FALSE);
        $this->db->select("(SUM(total_price) - SUM(`quantity`*`supplier_rate`)) AS total_profit");
        $this->db->from('invoice a');
        $this->db->join('invoice_details b', 'b.invoice_id = a.invoice_id');
        // $this->db->where($dateRange, NULL, FALSE); 
        $this->db->where('a.date >=', $start_date);
        $this->db->where('a.date <=', $end_date);
        $this->db->group_by('b.invoice_id');
        $this->db->order_by('a.invoice', 'desc');
        $query = $this->db->get();
        return $query->num_rows();
    }

    //Product wise sales report
    public function product_wise_report() {
        $today = date('Y-m-d');
        $this->db->select("a.*, b.customer_id, b.customer_name");
        $this->db->from('invoice a');
        $this->db->join('customer_information b', 'b.customer_id = a.customer_id');
        $this->db->where('a.date', $today);
        $this->db->order_by('a.invoice_id', 'desc');
        $this->db->limit('500');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_product_list_by_sales($start_date = null, $end_date = null) {
        $sql = "SELECT si.*, ci.company__name,sii.refProductID AS product_id, pi.product_name
                FROM sale_invoice si
                    INNER JOIN sale_invoice_item sii ON sii.refSIID = si.SIID
                    INNER JOIN customer_information ci ON ci.customer_id = si.RefCustomerID
                    INNER JOIN product_information pi ON pi.id = sii.refProductID
                WHERE si.status = 1";
        if (isset($start_date) && !empty($start_date)) {
            $start_date = date('Y-m-d H:i:s', strtotime($start_date));
            $sql .= " AND si.DateTime >= '$start_date'";
        }

        if (isset($end_date) && !empty($end_date)) {
            $end_date = date('Y-m-d', strtotime($end_date)) . ' ' . date('H:i:s');
            $sql .= " AND si.DateTime <= '$end_date'";
        }

        $sql .= " GROUP BY si.SIID
                ORDER BY pi.product_name,si.SIID ASC";
        return $this->db->query($sql)->result();
    }

    //RETRIEVE DATE WISE SINGE PRODUCT REPORT
    public function get_sales_data_by_product_id($product_id, $start_date = null, $end_date = null) {
        $sql = "SELECT si.SIID, si.SINO, pi.id, pc.category_name, CONCAT(pi.product_name, ' - (',pc.category_name,')') AS product_name, si.DateTime, SUM(sii.Qty) AS totalQty, AVG(sii.UnitAmt) AS totalPrice,SUM(sii.DiscountAmt) AS totalDiscount, SUM(sii.TotalAmt) AS totalAmtAfterDisc, SUM(siit.TaxAmt) AS totalTax, si.RefCustomerID
                FROM sale_invoice_item sii
                    LEFT JOIN sale_invoice si ON si.SIID = sii.refSIID AND si.status = '1'
                    LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = sii.SIItemID AND siit.status = '1' 
                    LEFT JOIN product_information pi ON pi.id = sii.refProductID AND pi.status = '1'
                    LEFT JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                WHERE sii.status = '1' AND sii.refProductID = '$product_id'";

        if (isset($start_date) && !empty($start_date)) {
            $start_date = date('Y-m-d H:i:s', strtotime($start_date));
            $sql .= " AND si.DateTime >= '$start_date'";
        }

        if (isset($end_date) && !empty($end_date)) {
            $end_date = date('Y-m-d', strtotime($end_date)) . ' ' . date('H:i:s');
            $sql .= " AND si.DateTime <= '$end_date'";
        }

        $sql .= " GROUP BY sii.refProductID, si.SIID
                ORDER BY sii.refProductID,si.SIID ASC";
        return $this->db->query($sql)->result();
    }

    //RETRIEVE DATE WISE SINGE PRODUCT REPORT
    public function retrieve_product_sales_report_count() {
        $this->db->select("a.*, b.product_name, b.product_model, c.date, c.total_amount, d.customer_name");
        $this->db->from('invoice_details a');
        $this->db->join('product_information b', 'b.id = a.product_id');
        $this->db->join('invoice c', 'c.invoice_id = a.invoice_id');
        $this->db->join('customer_information d', 'd.customer_id = c.customer_id');
        $this->db->order_by('c.date', 'desc');
        $query = $this->db->get();
        return $query->num_rows();
    }

    //RETRIEVE DATE WISE SEARCH SINGLE PRODUCT REPORT
    public function retrieve_product_search_sales_report($start_date, $end_date, $perpage, $page) {
        $sql = "SELECT si.SIID, si.SINO, pi.id, pc.category_name, CONCAT(pi.product_name, ' - (',pc.category_name,')') AS product_name, si.DateTime, SUM(sii.Qty) AS totalQty, AVG(sii.UnitAmt) AS totalPrice,SUM(sii.DiscountAmt) AS totalDiscount, SUM(sii.TotalAmt) AS totalAmtAfterDisc, SUM(siit.TaxAmt) AS totalTax, si.RefCustomerID
                FROM sale_invoice_item sii
                    LEFT JOIN sale_invoice si ON si.SIID = sii.refSIID AND si.status = '1'
                    LEFT JOIN sale_invoice_itemtaxdet siit ON siit.refSIItemID = sii.SIItemID AND siit.status = '1' 
                    LEFT JOIN product_information pi ON pi.id = sii.refProductID AND pi.status = '1'
                    LEFT JOIN product_category pc ON pc.category_id = pi.category_id AND pc.status = '1'
                WHERE sii.status = '1'";
        if (isset($start_date) && !empty($start_date)) {
            $sql .= " AND DATE_FORMAT(si.DateTime,'%Y-%m-%d') >= '$start_date'";
        }
        if (isset($end_date) && !empty($end_date)) {
            $sql .= " AND DATE_FORMAT(si.DateTime,'%Y-%m-%d') <= '$end_date'";
        }
        $sql .= "  GROUP BY sii.refProductID, si.SIID
                ORDER BY si.SIID, sii.refProductID ASC";
        return $this->db->query($sql)->result();
    }

    //RETRIEVE DATE WISE SEARCH SINGLE PRODUCT REPORT
    public function retrieve_product_search_sales_report_count($start_date = null, $end_date = null) {
        //$dateRange = "c.date BETWEEN '$start_date%' AND '$end_date%'";
        $this->db->select("a.*, b.product_name, b.product_model, c.date, d.customer_name");
        $this->db->from('invoice_details a');
        $this->db->join('product_information b', 'b.id = a.product_id');
        $this->db->join('invoice c', 'c.invoice_id = a.invoice_id');
        $this->db->join('customer_information d', 'd.customer_id = c.customer_id');
        if (isset($start_date) && !empty($start_date)) {
            $this->db->where('c.date >=', $start_date);
        }
        if (isset($end_date) && !empty($end_date)) {
            $this->db->where('c.date <=', $end_date);
        }
        $this->db->order_by('c.date', 'desc');
        $query = $this->db->get();
        return $query->num_rows();
    }

    //Retrieve company Edit Data
    public function retrieve_company() {
        $this->db->select('*');
        $this->db->from('company_information');
        $this->db->limit('1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    // date to date product stock report
    public function stock_report_product_date_date($from_date, $to_date, $per_page = null, $page = null) {
        $this->db->select("a.product_name, a.product_id, a.price, a.product_model, sum(b.sell) as 'totalSalesQnty', sum(b.Purchase) as 'totalPurchaseQnty', b.vdate as date");
        $this->db->from('product_information a');
        $this->db->join('stock_history b', 'b.product_id = a.id', 'left');
        $this->db->where('b.vdate >=', $from_date);
        $this->db->where('b.vdate <=', $to_date);
        $this->db->group_by('b.product_id');
        $this->db->limit($per_page, $page);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Counter of unique product histor which has been affected
    public function product_counter_by_productdatetodate($from_date, $to_date) {
        $this->db->select("a.product_name, a.product_id, a.price, a.product_model, sum(b.sell) as 'totalSalesQnty', sum(b.Purchase) as 'totalPurchaseQnty', b.vdate as date");
        $this->db->from('product_information a');
        $this->db->join('stock_history b', 'b.product_id = a.id', 'left');
        $this->db->where('b.vdate >=', $from_date);
        $this->db->where('b.vdate <=', $to_date);
        $this->db->group_by('b.product_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return false;
    }

    public function stockReportRawProduct($product_id = '', $start_date = '', $end_date = '') {
        $sql = "SELECT pi.id,pi.product_name,pc.category_name,pg.product_group_name,pi.price,pi.product_model,pi.serial_no,pi.unit, si.supplier_name, 
                (SUM(sh.opening_stock) + SUM(sh.Purchase) - SUM(sh.sell) + SUM(sh.sell_return) - SUM(sh.purchase_return))AS stock,
                (SUM(sh.opening_stock) + SUM(sh.Purchase) + SUM(sh.sell_return))AS inqty,
                (SUM(sh.sell) + SUM(sh.purchase_return))AS outqty
                FROM product_information pi
                	LEFT JOIN stock_history sh ON sh.product_id = pi.id
                	INNER JOIN product_category pc ON pc.category_id = pi.category_id 
                	INNER JOIN product_group pg ON pg.id = pi.product_group_id 
                	INNER JOIN supplier_information si ON si.supplier_id = sh.supplier_id 
                WHERE (pi.product_group_id = '2' OR pi.product_group_id = '4')";

        if (!empty($start_date)) {
            $sql .= " AND sh.vdate >= '$start_date'";
        }
        if (!empty($end_date)) {
            $sql .= " AND sh.vdate <= '$end_date'";
        }
        if (!empty($product_id)) {
            $sql .= " AND sh.product_id = '$product_id'";
        }

        $sql .= " GROUP BY pi.id, si.supplier_id ORDER BY pi.product_name ASC";

//        preprint($sql);

        $result = $this->db->query($sql)->result();
        if (!empty($result)) {
            return $result;
        }
        return FALSE;
    }

    public function monthlySaleReport($start = '', $end = '') {
        $sql = "SELECT EXTRACT(YEAR_MONTH FROM STR_TO_DATE(si.DateTime, '%Y-%m-%d')) as month, SUM(si.GrandTotal) AS GrandTotal 
                FROM sale_invoice si
                WHERE si.status = '1'";
        if (isset($start) && !empty($start) && isset($end) && !empty($end)) {
            $sql .= " AND EXTRACT(YEAR_MONTH FROM STR_TO_DATE(si.DateTime, '%Y-%m-%d')) >= EXTRACT(YEAR_MONTH FROM '$start') 
                      AND EXTRACT(YEAR_MONTH FROM STR_TO_DATE(si.DateTime, '%Y-%m-%d')) <= EXTRACT(YEAR_MONTH FROM '$end') ";
        } else {
            $sql .= " AND EXTRACT(YEAR FROM STR_TO_DATE(si.DateTime, '%Y-%m-%d')) >= EXTRACT(YEAR FROM NOW())";
        }
        $sql .= " GROUP BY EXTRACT(YEAR_MONTH FROM STR_TO_DATE(si.DateTime, '%Y-%m-%d'))
                  ORDER BY month ASC";
//        preprint($sql);
        return $this->db->query($sql)->result();
    }

    public function monthlyPurchaseReport($start = '', $end = '') {
        $sql = "SELECT EXTRACT(YEAR_MONTH FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d')) as month, SUM(pi.GrandTotal) AS GrandTotal 
                FROM product_inward pi
                WHERE pi.status = '1'";
        if (isset($start) && !empty($start) && isset($end) && !empty($end)) {
            $sql .= " AND EXTRACT(YEAR_MONTH FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d')) >= EXTRACT(YEAR_MONTH FROM '$start') 
                      AND EXTRACT(YEAR_MONTH FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d')) <= EXTRACT(YEAR_MONTH FROM '$end')";
        } else {
            $sql .= " AND EXTRACT(YEAR FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d')) >= EXTRACT(YEAR FROM pi.DateTime)";
        }
        $sql .= " GROUP BY EXTRACT(YEAR_MONTH FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d'))
                  ORDER BY month ASC";
        return $this->db->query($sql)->result();
    }

    public function expense_list_by_category($from_date = null, $to_date = null) {
        $sql = "SELECT ec.expense_category_name, e.expense_date, SUM(e.expense_amount) AS totalAmt, CONCAT(u.first_name,' ',u.last_name) AS employee_name
                FROM expense e
                    INNER JOIN expense_category ec ON ec.id = e.ref_expense_category_id
                    INNER JOIN users u ON u.user_id = e.ref_user_id
                WHERE e.status = '1' AND e.approval_status = 'Approved'";

        if (isset($from_date) && !empty($from_date)) {
            $sql .= " AND e.expense_date >= '$from_date'";
        }

        if (isset($to_date) && !empty($to_date)) {
            $sql .= " AND e.expense_date <= '$to_date'";
        }

        $sql .= "  GROUP BY e.ref_expense_category_id
                ORDER BY e.ref_expense_category_id ASC";
        return $this->db->query($sql)->result();
    }

    public function get_expense_list($daily = false, $from_date = null, $to_date = null, $approval_status = false) {
        $sql = "SELECT e.*, ec.expense_category_name, CONCAT(u.first_name,' ',u.last_name) AS employee_name
                FROM expense e
                    INNER JOIN expense_category ec ON ec.id = e.ref_expense_category_id
                    INNER JOIN users u ON u.user_id = e.ref_user_id
                WHERE e.status = '1'";

        if ($daily == true) {
            $sql .= " AND e.expense_date LIKE '" . date('Y-m-d') . "%'";
        }

        if ($approval_status == true) {
            $sql .= " AND e.approval_status = 'Approved'";
        }

        if (isset($from_date) && !empty($from_date)) {
            $sql .= " AND e.expense_date >= '$from_date'";
        }

        if (isset($to_date) && !empty($to_date)) {
            $sql .= " AND e.expense_date <= '$to_date'";
        }

        return $this->db->query($sql)->result();
    }

    public function dailySaleReportDashboard() {
        $sql = "SELECT DATE_FORMAT(so.DateTime, '%d-%m-%Y') as date, IFNULL(SUM(so.GrandTotal),0) AS GrandTotal 
                FROM sale_invoice so
                WHERE so.status = '1' AND so.DateTime >= '" . date('Y-m-01') . "' AND so.DateTime <= '" . date('Y-m-t') . "'
                GROUP BY date
                ORDER BY date ASC";
        return $this->db->query($sql)->result();
    }

    public function dailyPurchaseReportDashboard() {
        $sql = "SELECT DATE_FORMAT(pi.DateTime, '%d-%m-%Y') as date, IFNULL(SUM(pi.GrandTotal),0) AS GrandTotal 
                FROM purchase_invoice pi
                WHERE pi.status = '1' AND pi.DateTime >= '" . date('Y-m-01') . "' AND pi.DateTime <= '" . date('Y-m-t') . "'
                GROUP BY date
                ORDER BY date ASC";
        return $this->db->query($sql)->result();
    }

    public function dailyExpenseReportDashboard() {
        $sql = "SELECT DATE_FORMAT(e.expense_date, '%d-%m-%Y') as date, IFNULL(SUM(e.expense_amount),0) AS total_expense 
                FROM expense e
                WHERE e.status = '1' AND e.approval_status = 'Approved' AND e.expense_date >= '" . date('Y-m-01') . "' AND e.expense_date <= '" . date('Y-m-t') . "'
                GROUP BY date
                ORDER BY date ASC";
        return $this->db->query($sql)->result();
    }

    public function dailyPaymentReportDashboard() {
        $sql = "SELECT DATE_FORMAT(tr.date_of_transaction, '%d-%m-%Y') as date, IFNULL(SUM(tr.pay_amount),0) AS GrandTotal 
                FROM transaction tr
                WHERE tr.is_deleted = '1' AND tr.transaction_type = 1 AND tr.date_of_transaction >= '" . date('Y-m-01') . "' AND tr.date_of_transaction <= '" . date('Y-m-t') . "'
                GROUP BY date
                ORDER BY date ASC";
        return $this->db->query($sql)->result();
    }

    public function dailyReceiptReportDashboard() {
        $sql = "SELECT DATE_FORMAT(tr.date_of_transaction, '%d-%m-%Y') as date, IFNULL(SUM(tr.receipt_amount),0) AS GrandTotal 
                FROM transaction tr
                WHERE tr.is_deleted = '1' AND tr.transaction_type = 2 AND tr.date_of_transaction >= '" . date('Y-m-01') . "' AND tr.date_of_transaction <= '" . date('Y-m-t') . "'
                GROUP BY date
                ORDER BY date ASC";
        return $this->db->query($sql)->result();
    }

    public function weeklySaleReportDashboard() {
        $sql = "SELECT EXTRACT(WEEK FROM STR_TO_DATE(so.DateTime, '%Y-%m-%d')) as week, IFNULL(SUM(so.GrandTotal),0) AS GrandTotal 
                FROM sale_invoice so
                WHERE EXTRACT(MONTH FROM STR_TO_DATE(so.DateTime, '%Y-%m-%d')) = EXTRACT(MONTH FROM NOW()) AND so.status = '1'
                GROUP BY EXTRACT(WEEk FROM STR_TO_DATE(so.DateTime, '%Y-%m-%d'))
                ORDER BY week ASC";
        return $this->db->query($sql)->result();
    }

    public function weeklyPurchaseReportDashboard() {
        $sql = "SELECT EXTRACT(WEEK FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d')) as week, IFNULL(SUM(pi.GrandTotal),0) AS GrandTotal 
                FROM purchase_invoice pi
                WHERE EXTRACT(YEAR FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d')) = EXTRACT(YEAR FROM NOW()) AND pi.status = '1'
                GROUP BY EXTRACT(WEEK FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d'))
                ORDER BY week ASC";
        return $this->db->query($sql)->result();
    }

    public function weeklyExpenseReportDashboard() {
        $sql = "SELECT EXTRACT(WEEK FROM STR_TO_DATE(e.expense_date, '%Y-%m-%d')) as week, IFNULL(SUM(e.expense_amount),0) AS total_expense
                FROM expense e
                WHERE EXTRACT(YEAR FROM STR_TO_DATE(e.expense_date, '%Y-%m-%d')) = EXTRACT(YEAR FROM NOW()) AND e.status = '1' AND e.approval_status = 'Approved'
                GROUP BY EXTRACT(WEEK FROM STR_TO_DATE(e.expense_date, '%Y-%m-%d'))
                ORDER BY week ASC;";
        return $this->db->query($sql)->result();
    }

    public function weeklyPaymentReportDashboard() {
        $sql = "SELECT EXTRACT(WEEK FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d')) as week, IFNULL(SUM(tr.pay_amount),0) AS GrandTotal 
                FROM transaction tr
                WHERE EXTRACT(MONTH FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d')) = EXTRACT(MONTH FROM NOW()) AND tr.is_deleted = '1' AND tr.transaction_type = 1
                GROUP BY EXTRACT(WEEk FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d'))
                ORDER BY week ASC";
        return $this->db->query($sql)->result();
    }

    public function weeklyReceiptReportDashboard() {
        $sql = "SELECT EXTRACT(WEEK FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d')) as week, IFNULL(SUM(tr.receipt_amount),0) AS GrandTotal 
                FROM transaction tr
                WHERE EXTRACT(MONTH FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d')) = EXTRACT(MONTH FROM NOW()) AND tr.is_deleted = '1' AND tr.transaction_type = 2
                GROUP BY EXTRACT(WEEk FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d'))
                ORDER BY week ASC";
        return $this->db->query($sql)->result();
    }

    public function monthlySaleReportDashboard() {
        $sql = "SELECT EXTRACT(MONTH FROM STR_TO_DATE(so.DateTime, '%Y-%m-%d')) as month, IFNULL(SUM(so.GrandTotal),0) AS GrandTotal 
                FROM sale_invoice so
                WHERE EXTRACT(YEAR FROM STR_TO_DATE(so.DateTime, '%Y-%m-%d')) >= EXTRACT(YEAR FROM NOW()) AND so.status = '1'
                GROUP BY EXTRACT(YEAR_MONTH FROM STR_TO_DATE(so.DateTime, '%Y-%m-%d'))
                ORDER BY month ASC";
        return $this->db->query($sql)->result();
    }

    public function monthlyPurchaseReportDashboard() {
        $sql = "SELECT EXTRACT(MONTH FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d')) as month, IFNULL(SUM(pi.GrandTotal),0) AS GrandTotal 
                FROM purchase_invoice pi
                WHERE EXTRACT(YEAR FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d')) >= EXTRACT(YEAR FROM NOW()) AND pi.status = '1'
                GROUP BY EXTRACT(YEAR_MONTH FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d'))
                ORDER BY month ASC";
        return $this->db->query($sql)->result();
    }

    public function monthlyExpenseReportDashboard() {
        $sql = "SELECT EXTRACT(MONTH FROM STR_TO_DATE(e.expense_date, '%Y-%m-%d')) as month, IFNULL(SUM(e.expense_amount),0) AS total_expense
                FROM expense e
                WHERE EXTRACT(YEAR FROM STR_TO_DATE(e.expense_date, '%Y-%m-%d')) >= EXTRACT(YEAR FROM NOW()) AND e.status = '1' AND e.approval_status = 'Approved'
                GROUP BY EXTRACT(YEAR_MONTH FROM STR_TO_DATE(e.expense_date, '%Y-%m-%d'))
                ORDER BY month ASC";
        return $this->db->query($sql)->result();
    }

    public function monthlyPaymentReportDashboard() {
        $sql = "SELECT EXTRACT(MONTH FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d')) as month, IFNULL(SUM(tr.pay_amount),0) AS GrandTotal 
                FROM transaction tr
                WHERE EXTRACT(YEAR FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d')) >= EXTRACT(YEAR FROM NOW()) AND tr.is_deleted = '1' AND tr.transaction_type = 1
                GROUP BY EXTRACT(YEAR_MONTH FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d'))
                ORDER BY month ASC";
        return $this->db->query($sql)->result();
    }

    public function monthlyReceiptReportDashboard() {
        $sql = "SELECT EXTRACT(MONTH FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d')) as month, IFNULL(SUM(tr.receipt_amount),0) AS GrandTotal 
                FROM transaction tr
                WHERE EXTRACT(YEAR FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d')) >= EXTRACT(YEAR FROM NOW()) AND tr.is_deleted = '1' AND tr.transaction_type = 2
                GROUP BY EXTRACT(YEAR_MONTH FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d'))
                ORDER BY month ASC";
        return $this->db->query($sql)->result();
    }

    public function yearlySaleReportDashboard() {
        $sql = "SELECT EXTRACT(YEAR FROM STR_TO_DATE(so.DateTime, '%Y-%m-%d')) as year, IFNULL(SUM(so.GrandTotal),0) AS GrandTotal 
                FROM sale_invoice so
                WHERE so.status = '1'
                GROUP BY EXTRACT(YEAR FROM STR_TO_DATE(so.DateTime, '%Y-%m-%d'))
                ORDER BY year ASC";
        return $this->db->query($sql)->result();
    }

    public function yearlyPurchaseReportDashboard() {
        $sql = "SELECT EXTRACT(YEAR FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d')) as year, IFNULL(SUM(pi.GrandTotal),0) AS GrandTotal 
                FROM purchase_invoice pi
                WHERE pi.status = '1'
                GROUP BY EXTRACT(YEAR FROM STR_TO_DATE(pi.DateTime, '%Y-%m-%d'))
                ORDER BY year ASC";
        return $this->db->query($sql)->result();
    }

    public function yearlyExpenseReportDashboard() {
        $sql = "SELECT EXTRACT(YEAR FROM STR_TO_DATE(e.expense_date, '%Y-%m-%d')) as year, IFNULL(SUM(e.expense_amount),0) AS total_expense
                FROM expense e
                WHERE e.status = '1' AND e.approval_status = 'Approved'
                GROUP BY EXTRACT(YEAR FROM STR_TO_DATE(e.expense_date, '%Y-%m-%d'))
                ORDER BY year ASC";
        return $this->db->query($sql)->result();
    }

    public function yearlyPaymentReportDashboard() {
        $sql = "SELECT EXTRACT(YEAR FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d')) as year, IFNULL(SUM(tr.pay_amount),0) AS GrandTotal 
                FROM transaction tr
                WHERE tr.is_deleted = '1' AND tr.transaction_type = 1
                GROUP BY EXTRACT(YEAR FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d'))
                ORDER BY year ASC";
        return $this->db->query($sql)->result();
    }

    public function yearlyReceiptReportDashboard() {
        $sql = "SELECT EXTRACT(YEAR FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d')) as year, IFNULL(SUM(tr.receipt_amount),0) AS GrandTotal 
                FROM transaction tr
                WHERE tr.is_deleted = '1' AND tr.transaction_type = 2
                GROUP BY EXTRACT(YEAR FROM STR_TO_DATE(tr.date_of_transaction, '%Y-%m-%d'))
                ORDER BY year ASC";
        return $this->db->query($sql)->result();
    }
}
