<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Purchase_return extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Purchase_return_model');
        $this->load->model('Purchase_invoice_model');
        $this->load->model('Suppliers');
        $this->load->model('Categories');
        $this->load->model('Common_model');
        $this->load->model('Units');

        $this->page_id = 'PURCHASE';
        $this->menu_id = 'PURCHASE_RETURN';
    }

    public function index() {
        $data = array();

        $data['title'] = 'Return';
        $code = $this->Common_model->getCompanyCode();
        $data['company_code'] = strtoupper($code);
        $data['approved_sale_outward'] = $this->Purchase_return_model->getApprovedSaleOutward();
        $data['approved_purchase_retun'] = $this->Purchase_return_model->getApprovedPurchaseInward();
        $return = $this->parser->parse('purchase_return/form', $data, true);
        $this->template->full_admin_html_view($return);
    }

    public function manageReturnList() {
        $data = array();

        $this->Common_model->check_menu_access('PURCHASE_RETURN', 'VIEW', 'admin');
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PURCHASE_RETURN', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('Unauthorized');
        }

        $data['title'] = 'Purchase Return List';
        $data['employee_list'] = $this->Common_model->get_all_users();

        $code = $this->Common_model->getCompanyCode();
        $data['company_code'] = strtoupper($code);

        $view = 'admin/return/return_list';
        $this->page_title = 'Purchase Return List';
        $this->load_admin_view($view, $data);
    }

    public function get_purchase_return_list() {
        $menu_rights = $this->Common_model->get_menu_rights('PURCHASE_RETURN', 'admin');

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);

        $data['groupMasterData'] = array();
        $masterData = $this->Purchase_return_model->get_purchase_return_list($this->input->post());
        $cnt = 0;
        $PRNO = $DateTime = $ReturnDescription = $SupplierName = $ProductCount = $GrandTotal = $CreatedBy = $UpdatedBy = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $PRNO = $val->PRNO;
            $SupplierName = $val->supplier_name;
            $DateTime = $val->DateTime;
            $ReturnDescription = $val->ReturnDescription;
            $ProductCount = $val->product_count;
            $GrandTotal = $val->GrandTotal;
            $CreatedBy = $val->created_by;
            $UpdatedBy = $val->updated_by;

            $action_new = '';
            $action_new .= form_open();
            $action_new .= '<div style="display:flex; gap:5px; flex-wrap:nowrap; justify-content: center;">';
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Purchase_return/purchaseReturnUpdateForm/' . $val->PRID . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" title="Edit" data-toggle="tooltip" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>\'><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 deletePurchaseReturn" name="' . $val->PRID . '" data-toggle="tooltip" title="Delete Record" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>\'><i class="fal fa-times"></i></a>';
            }
//            $action_new .= '<a href="javascript:void(0);" class="btn btn-warning btn-sm printPurchaseReturn" name="' . $val->PRID . '" data-PRNO="' . cleanString($val->PRNO) . '" data-toggle="tooltip" data-placement="right" title="Print"><i class="fa fa-barcode" aria-hidden="true"></i></a>';
            $action_new .= '<a href="' . base_url() . 'admin/Purchase_return/printPDFPurchaseReturn/' . $val->PRID . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-warning mr-2" name="' . $val->PRID . '" data-PRNO="' . cleanString($val->PRNO) . '" data-toggle="tooltip" title="Print" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-warning-500"></div></div>\'><i class="fal fa-barcode"></i></a>';
            $action_new .= '</div>';
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $PRNO, $SupplierName, $DateTime, $ReturnDescription, $ProductCount, $GrandTotal, $CreatedBy, $UpdatedBy, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Purchase_return_model->get_purchase_return_list_search_data($this->input->post()),
            "recordsFiltered" => $this->Purchase_return_model->get_purchase_return_list_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    public function createPurchaseReturn() {
        $data = array();
        
        $this->Common_model->check_menu_access('PURCHASE_RETURN', 'ADD', 'admin');
        
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PURCHASE_RETURN', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        
        $view = 'admin/return/add_purchase_return';
        $this->page_title = 'Purchase Return List';
        $this->load_admin_view($view, $data);
    }

    public function addPurchaseReturn() {

        $data = array();
        $this->Common_model->check_menu_access('PURCHASE_RETURN', 'ADD', 'admin');

        $invoice_no = $this->input->post('invoice_no');

        $invoice_data = $this->Purchase_return_model->getPurchaseInvoiceData($invoice_no);

        if (isset($invoice_data) && !empty($invoice_data)) {
            $data['purchase_invoice_data'] = $invoice_data;

            $invoice_item_data = $this->Purchase_return_model->purchase_invoice_product_details($invoice_data->PINID);

            if (isset($invoice_item_data) && !empty($invoice_item_data)) {
                $cnt = -1;
                foreach ($invoice_item_data as $key => $value) {
                    if ($value->returnable_qty > 0) {
                        $cnt = $cnt + 1;
                        $data['purchase_invoice_item_data'][$cnt] = $value;
                    }
                }
            }

            if (isset($data['purchase_invoice_item_data']) && !empty($data['purchase_invoice_item_data'])) {
                $data['status'] = true;
            } else {
                $data['status'] = false;
                $data['msg'] = 'Purchase invoice products are returned already!';
            }
        } else {
            $data['status'] = false;
            $data['msg'] = 'Please enter valid purchase invoice number!';
        }
        
        echo json_encode($data);
        die();
    }

    public function insertPurchaseReturn() {
        $PINID = $this->input->post('hidden_purchase_invoice_id');
        $invoice_data = array();

        $returnDescription = $this->input->post('return_description') ? $this->input->post('return_description') : null;
        $invoice_data['ReturnDescription'] = $returnDescription;

        $DateTime = date("Y-m-d H:i:s");
        $invoice_data['DateTime'] = $DateTime;

        $ref_no = $this->Purchase_return_model->generatePurchaseReturnNo();
        $PRNO = FIN_YEAR . '-' . COMPANY_SHORT_NAME . '-PR-0001';

        if (isset($ref_no) && !empty($ref_no)) {
            $tmp_no = substr($ref_no, -4);
            $PRNO = FIN_YEAR . '-' . COMPANY_SHORT_NAME . '-PR-' . sprintf('%04d', $tmp_no + 1);
        }

        $invoice_data['PRNO'] = $PRNO;
        $invoice_data['refPINID'] = $PINID;

        $RefSupplierID = $this->input->post('hidden_supplier_id');
        $invoice_data['RefSupplierID'] = $RefSupplierID;
        $invoice_data['RefSupplierAddress'] = $this->input->post('hidden_supplier_address');

        $basicTotal = $this->input->post('BasicTotal');
        if (isset($basicTotal) && !empty($basicTotal)) {
            $BasicTotal = 0;
            foreach ($basicTotal as $key => $total) {
                $BasicTotal += $total;
            }
            $invoice_data['BasicTotal'] = $BasicTotal;
        }

        $discount = $this->input->post('discount_ind');
        if (isset($discount) && !empty($discount)) {
            $Discount = 0;
            foreach ($discount as $key => $disc) {
                $disc = isset($disc) ? $disc : 0;
                $Discount += $disc;
            }
            $invoice_data['DiscountTotal'] = $Discount;
        } else {
            $invoice_data['DiscountTotal'] = '';
        }

        $tax = $this->input->post('total_tax');
        if (isset($tax) && !empty($tax)) {
            $Taxtotal = 0;
            foreach ($tax as $key => $ta) {
                $Taxtotal += $ta;
            }
            $invoice_data['Taxtotal'] = $Taxtotal;
        } else {
            $invoice_data['Taxtotal'] = '';
        }

        $GrandTotal = $this->input->post('g_total');
        $invoice_data['GrandTotal'] = $GrandTotal;

        $invoice_data['InsUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        $invoice_data['InsTerminal'] = $this->input->ip_address();
        $invoice_data['InsDateTime'] = date('Y/m/d H:i:s');
        $invoice_data['UpdUser'] = NULL;
        $invoice_data['UpdTerminal'] = NULL;
        $invoice_data['UpdDateTime'] = NULL;

        $chk_uniq_invoice = true;

//        $chk_uniq_invoice = $this->Common_model->chkUniqueCode('purchase_return', 'refPIID', $PINID, 1, '', '', 'DateTime', $DateTime); //('table_name',column_name for chk,'column_data',status)

        if ($chk_uniq_invoice == true) {
            $PRID = $this->Purchase_return_model->insertPurchaseReturn($invoice_data);

            $total_amt = $this->input->post('total_amt');
            $qty = $this->input->post('quantity_amount');
            $product_rate = $this->input->post('unit_price');
            $discount_percenatge = $this->input->post('discount');

            $product_id = $this->input->post('product_id');
            if (isset($product_id) && !empty($product_id)) {
                foreach ($product_id as $k1 => $v1) {
                    if ($qty[$k1] > 0) {
                        $product_data['refPRID'] = $PRID;
                        $product_data['refProductID'] = $product_id[$k1];

                        $UnitAmt_new = (isset($product_rate[$k1]) && !empty($product_rate[$k1]) ? $product_rate[$k1] : 0);
                        $product_data['UnitAmt'] = $UnitAmt_new;

                        $Qty_new = (isset($qty[$k1]) && !empty($qty[$k1]) ? $qty[$k1] : 0);
                        $product_data['Qty'] = $Qty_new;

                        $total_amt1 = (isset($total_amt[$k1]) && !empty($total_amt[$k1]) ? $total_amt[$k1] : ($UnitAmt_new * $Qty_new));
                        $product_data['TotalAmt'] = $total_amt1;

                        $product_data['DiscountPer'] = (isset($discount_percenatge[$k1]) && !empty($discount_percenatge[$k1]) ? $discount_percenatge[$k1] : 0);
                        $product_data['DiscountAmt'] = (isset($discount[$k1]) && !empty($discount[$k1]) ? $discount[$k1] : 0);

                        $this->db->insert('purchase_return_item', $product_data);
                        $PRItemID = $this->db->insert_id();

                        $Tax_id = $_POST['Tax_id'][$k1];
                        $tax_per = $_POST['tax_per'][$k1];
                        $tax_name = $_POST['tax_name'][$k1];
                        $tax_id_array = explode(',', $Tax_id);
                        $tax_per_array = explode(',', $tax_per);
                        $tax_name_array = explode(',', $tax_name);
                        foreach ($tax_id_array as $key2 => $value) {
                            $tax_data['refPRID'] = $PRID;
                            $tax_data['refPRItemID'] = $PRItemID;
                            $tax_data['refTaxID'] = $value;
                            $tax_data['refTaxName'] = $tax_name_array[$key2];
                            $tax_per_new = $tax_per_array[$key2];
                            $tax_data['TaxPer'] = $tax_per_new;
                            $tax_data['TaxAmt'] = ($total_amt1 * $tax_per_new) / 100;
                            $this->db->insert('purchase_return_itemtaxdet', $tax_data);
                        }
                    }
                }
            }

            $supplier_ledger['transaction_id'] = null;
            $supplier_ledger['supplier_id'] = $RefSupplierID;
            $supplier_ledger['chalan_no'] = $PRID;
            $supplier_ledger['date'] = $DateTime;
            $supplier_ledger['description'] = $returnDescription;
            $supplier_ledger['amount'] = $GrandTotal;
            $supplier_ledger['status'] = 1;
            $supplier_ledger['d_c'] = 'c';
            $supplier_ledger['ref_table_name'] = 'purchase_return';
            $this->Common_model->insertData('supplier_ledger', $supplier_ledger);

            $this->_show_message("Information has been added successfully!", "success");
            if (isset($_POST['add-purchase-return'])) {
                redirect(base_url('admin/Purchase_return/manageReturnList'));
                exit;
            } elseif (isset($_POST['add-purchase-return-another'])) {
                redirect(base_url('admin/Purchase_return/createPurchaseReturn'));
                exit;
            }
            redirect(base_url('admin/Purchase_return/manageReturnList'));
        }
    }

    public function purchaseReturnUpdateForm($PRID) {
        $data = array();
        $data['title'] = 'Edit Purchase Return';

        $this->Common_model->check_menu_access('PURCHASE_RETURN', 'EDIT', 'admin');

        $data['PRID'] = $PRID;
        $data['purchase_return_data'] = (array) $this->Purchase_return_model->getPurchaseReturnDataByReturnId($PRID);
        $PINID = $data['purchase_return_data']['refPINID'];
        $data['PINID'] = $PINID;
        $data['purchase_invoice_data'] = (array) $this->Purchase_return_model->purchaseInvoiceDataById($PINID);

        $view = 'admin/return/edit_purchase_return';
        $this->page_title = 'Purchase Return List';
        $this->load_admin_view($view, $data);
    }

    public function updatePurchaseReturn($PRID) {
        $invoice_data = array();

        $returnDescription = $this->input->post('return_description') ? $this->input->post('return_description') : null;
        $invoice_data['ReturnDescription'] = $returnDescription;

        $DateTime = date("Y-m-d H:i:s");
        $invoice_data['DateTime'] = $DateTime;

        $basicTotal = $this->input->post('BasicTotal');
        if (isset($basicTotal) && !empty($basicTotal)) {
            $BasicTotal = 0;
            foreach ($basicTotal as $key => $total) {
                $BasicTotal += $total;
            }
            $invoice_data['BasicTotal'] = $BasicTotal;
        }

        $discount = $this->input->post('discount_ind');
        if (isset($discount) && !empty($discount)) {
            $Discount = 0;
            foreach ($discount as $key => $disc) {
                $disc = isset($disc) ? $disc : 0;
                $Discount += $disc;
            }
            $invoice_data['DiscountTotal'] = $Discount;
        } else {
            $invoice_data['DiscountTotal'] = '';
        }

        $tax = $this->input->post('total_tax');
        if (isset($tax) && !empty($tax)) {
            $Taxtotal = 0;
            foreach ($tax as $key => $ta) {
                $Taxtotal += $ta;
            }
            $invoice_data['Taxtotal'] = $Taxtotal;
        } else {
            $invoice_data['Taxtotal'] = '';
        }

        $GrandTotal = $this->input->post('g_total');
        $invoice_data['GrandTotal'] = $GrandTotal;

        $invoice_data['UpdUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        ;
        $invoice_data['UpdTerminal'] = $this->input->ip_address();
        $invoice_data['UpdDateTime'] = date('Y/m/d H:i:s');

        $supplier_ledger['amount'] = $GrandTotal;
        $supplier_ledger['description'] = $returnDescription;
        $supplier_ledger['ref_table_name'] = 'purchase_return';
        $supplier_ledger['d_c'] = 'c';

        if ($PRID != '') {
            $this->Common_model->updateData('purchase_return', 'PRID', $PRID, $invoice_data, 1);
            $this->Common_model->updateData2('supplier_ledger', 'chalan_no', $PRID, 'ref_table_name', 'purchase_return', $supplier_ledger, 'Live');

            $this->Purchase_return_model->deleteSaleReturnItem($PRID, 'purchase_return_item');
            $this->Purchase_return_model->deleteSaleReturnItem($PRID, 'purchase_return_itemtaxdet');

            $total_amt = $this->input->post('total_amt');
            $qty = $this->input->post('quantity_amount');
            $product_rate = $this->input->post('unit_price');
            $discount_percentage = $this->input->post('discount');

            $product_id = $this->input->post('product_id');

            if (!empty($product_id)) {
                foreach ($product_id as $k1 => $v1) {
                    if ($qty[$k1] > 0) {
                        $product_data['refPRID'] = $PRID;
                        $product_data['refProductID'] = $product_id[$k1];
                        $product_data['UnitAmt'] = $product_rate[$k1];
                        $total_amt1 = isset($total_amt[$k1]) && !empty($total_amt[$k1]) ? $total_amt[$k1] : 0;
                        $product_data['TotalAmt'] = $total_amt1;
                        $product_data['Qty'] = isset($qty[$k1]) && !empty($qty[$k1]) ? $qty[$k1] : 0;
                        $product_data['DiscountPer'] = (isset($discount_percentage[$k1]) && !empty($discount_percentage[$k1]) ? $discount_percentage[$k1] : 0);
                        $product_data['DiscountAmt'] = (isset($discount[$k1]) && !empty($discount[$k1]) ? $discount[$k1] : 0);
                        $this->db->insert('purchase_return_item', $product_data);
                        $SRItemID = $this->db->insert_id();

                        $Tax_id = $_POST['Tax_id'][$k1];
                        $tax_per = $_POST['tax_per'][$k1];
                        $tax_name = $_POST['tax_name'][$k1];
                        $tax_id_array = explode(',', $Tax_id);
                        $tax_per_array = explode(',', $tax_per);
                        $tax_name_array = explode(',', $tax_name);
                        foreach ($tax_id_array as $key2 => $value) {
                            $tax_data['refPRID'] = $PRID;
                            $tax_data['refPRItemID'] = $SRItemID;
                            $tax_data['refTaxID'] = $value;
                            $tax_data['refTaxName'] = $tax_name_array[$key2];
                            $tax_per_new = $tax_per_array[$key2];
                            $tax_data['TaxPer'] = $tax_per_new;
                            $tax_data['TaxAmt'] = ($total_amt1 * $tax_per_new) / 100;
                            $this->db->insert('purchase_return_itemtaxdet', $tax_data);
                        }
                    }
                }
            }

            if (!empty($SRItemID) && !empty($SRItemID)) {
                $this->_show_message("Information has been updated successfully!", "success");
                if (isset($_POST['add-sale-invoice'])) {
                    redirect(base_url('admin/Purchase_return/manageReturnList'));
                    exit;
                } elseif (isset($_POST['add-sale-invoice-another'])) {
                    redirect(base_url('admin/Cretrun_m'));
                    exit;
                }
                redirect(base_url('admin/Purchase_return/manageReturnList'));
            } else {
                $this->_show_message("This Sale Return already exist", "error");
                redirect(base_url('admin/Purchase_return'));
            }
        } else {
            $this->_show_message("This Sale Return already exist", "error");
            redirect(base_url('admin/Purchase_return'));
        }
    }

    public function deletePurchaseReturn() {

        $this->Common_model->check_menu_access('PURCHASE_RETURN', 'DELETE', 'admin');

        $data = array();
        $purchase_return_id = $this->input->post('product_return_id');

        if ($this->Purchase_return_model->deletePurchaseReturn($purchase_return_id)) {
            $this->_show_message("Information has been deleted successfully!", "success");
            $data['status'] = true;
        } else {
            $this->_show_message("you cant delete this product", "error");
            $data['status'] = true;
        }

        echo json_encode($data);
        die();
    }

    public function printPurchaseReturn() {
        $data = array();
        $PRID = $this->input->post('purchase_return_id');
        $data['purchase_return_data'] = $this->Purchase_return_model->purchaseReturnDataByID($PRID);
        $data['purchase_return_item_data'] = $this->Purchase_return_model->purchaseReturnItemDataById($PRID);
        $data['company_detail'] = get_company_detail();
        $data['tax_detail'] = $this->Purchase_return_model->purchaseReturnTaxDetail($PRID);
        $data['supplier_detail'] = $this->Common_model->getSupplierDataByid($data['purchase_return_data']['RefSupplierID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['CurrentUser'] = $this->db->where("user_id", $data['purchase_return_data']['InsUser'])->get('users')->row_array();
        $data['bank_data'] = $this->Common_model->getBankDetails();

        $content = $this->parser->parse('return/ajax_pdf', $data, true);
        echo $content;
        die;
    }

    function printPDFPurchaseReturn($PRID) {
        $data = array();
        $data['purchase_return_data'] = $this->Purchase_return_model->purchaseReturnDataByID($PRID);
        $data['purchase_return_item_data'] = $this->Purchase_return_model->purchaseReturnItemDataById($PRID);
        $data['company_detail'] = get_company_detail();
        $data['tax_detail'] = $this->Purchase_return_model->purchaseReturnTaxDetail($PRID);
        $data['supplier_detail'] = $this->Common_model->getSupplierDataByid($data['purchase_return_data']['RefSupplierID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['CurrentUser'] = $this->db->where("id", $data['purchase_return_data']['InsUser'])->get('user_information')->row_array();
        $data['bank_data'] = $this->Common_model->getBankDetails();

        if (!empty($data)) {
            try {
                $pdf_data = '';
                $pdfhtml = $this->parser->parse('admin/pdf/Purchase_return', $data, true);
                $pdf_data .= str_replace('%%File_type%%', 'ORIGINAL', $pdfhtml);
                $pdf_data .= str_replace('%%File_type%%', 'DUPLICATE', $pdfhtml);
                $pdfName = cleanString($data['purchase_return_data']['PRNO']) . '.pdf';
                $html2pdf = new Html2Pdf('L', 'A5', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdf_data);
                $html2pdf->output($pdfName, 'I');
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
    }

    public function mailPurchaseReturnDetail() {
        $data = $attachment = $mail_data = array();
        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $PRID = $this->input->post('PRID');
        $mail_to = $this->input->post('mail_to');
        $mail_subject = $this->input->post('mail_subject');
        $mail_body = $this->input->post('mail_body');
        $send_me = $this->input->post('send_me');
        $data['subject'] = !empty($mail_subject) ? $mail_subject : 'Purchase Return Detail';
        $mail_to_new = implode(', ', $mail_to);
        $mail_data['email_id'] = $mail_to_new;

        if ($send_me == 'true') {
            $mail_to_new .= ', ' . $this->session->userdata(SITE_NAME . '_admin')['user_email'];
            $mail_data['send_me'] = $this->session->userdata(SITE_NAME . '_admin')['user_email'];
        }

        $data['purchase_return_data'] = $this->Purchase_return_model->purchaseReturnDataByID($PRID);
        $data['purchase_return_item_data'] = $this->Purchase_return_model->purchaseReturnItemDataById($PRID);
        $data['company_detail'] = get_company_detail();
        $data['tax_detail'] = $this->Purchase_return_model->purchaseReturnTaxDetail($PRID);
        $data['supplier_detail'] = $this->Common_model->getSupplierDataByid($data['purchase_return_data']['RefSupplierID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['purchase_return_data']['PRNO'] = 'PR-' . $company_code . "-" . sprintf('%06d', $data['purchase_return_data']['PRNO']);

        if (!empty($data)) {
            try {
                $new_path = 'my-assets/documents/pdf/PurchaseReturn/';
                if (!is_dir($new_path)) {
                    if (!mkdir($new_path, 0777, true)) {
                        die('Not Created');
                    }
                }
                $pdfhtml = $this->parser->parse('pdf/Purchase_return', $data, true);
                $pdfName = $new_path . cleanString($data['purchase_return_data']['PRNO']) . '_' . date('H_i_s') . '.pdf';
                $attachment = array(getcwd() . '/' . $pdfName);
                $html2pdf = new Html2Pdf('L', 'A4', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdfhtml);
                $html2pdf->output(getcwd() . '/' . $pdfName, 'F');
                $attachment[] = getcwd() . '/' . $pdfName;
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }

        $data['login_url'] = 'google.com';
        $body = !empty($mail_body) ? $mail_body : 'Purchase Return Data';
        $data['company'] = get_company_detail();
        $company_name = $data['company']['company_name'];
        $data['body'] = $body;

        if (!empty($data)) {
            try {
                $from_email = "inquiry@foodmohalla.in";
                $this->email->from($from_email, $company_name);
                $this->email->subject($data['subject']);
                $this->email->message($data['body']);
                $this->email->attach($pdfName);
                if (!empty($mail_to_new)) {
                    $this->email->to($mail_to_new);
                    if ($this->email->send()) {
                        $mail_data['refTableName'] = 'purchase_return';
                        $mail_data['refTableId'] = $PRID;
                        $mail_data['attachments'] = $pdfName;
                        $mail_data['mailSubject'] = $data['subject'];
                        $mail_data['mailBody'] = $data['body'];
                        $mail_data['InsUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
                        ;
                        $mail_data['InsTerminal'] = $this->input->ip_address();
                        $mail_data['InsDateTime'] = date('Y/m/d H:i:s');
                        $this->Common_model->insertData('mailhistory', $mail_data);

                        $data['result'] = true;
                    }
                }
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
        echo json_encode($data);
        die;
    }
}
