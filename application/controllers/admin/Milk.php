<?php

class Milk extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'DASHBOARD';
//        $this->load->model('Milk_model');
        $this->load->model('Common_model');
        $this->load->model('Auth_model');
    }

    public function index() {
        $data = [];
        $this->page_id = 'MILK';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MILK', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MILK', 'VIEW', 'admin');
        $data['milk_data'] = $this->Common_model->geAlldata('tbl_milk_price');
        $view = 'admin/milk/milk';
        $this->page_title = 'MILK';
        $this->load_admin_view($view, $data);
    }

    public function addEditMilkPrice($encrypted_id = "") {
        $this->page_id = 'MILK';
        $id = $encrypted_id;
        $data = [];
        $data['milk_data'] = $this->Common_model->geAlldata('tbl_milk');
        if (isset($id) && !empty($id)) {
            $milk_price_data = $this->Common_model->getDataById2('tbl_milk_price', 'milk_price_id', $id, 'Live');
            $data['milk_price_data'] = $milk_price_data;
        }
        if ($this->input->post()) {
            $insert_data = array();
            $milk_price_date = $this->input->post('milk_price_date');
            $insert_data['milk_price_date'] = (isset($milk_price_date) && !empty($milk_price_date) ? date('Y-m-d', strtotime($milk_price_date)) : '');
            $insert_data['milk_price'] = $this->input->post('milk_price');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['ref_milk_id'] = $this->input->post('ref_milk_id');

                $insert_data['InsUser'] = $this->admin_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $milk_price_id = $this->Common_model->insertInformation($insert_data, 'tbl_milk_price');

                $history_insert_data = array();
                $history_insert_data['ref_milk_price_id'] = $milk_price_id;
                $history_insert_data['ref_milk_id_history'] = $insert_data['ref_milk_id'];
                $history_insert_data['milk_price_history_date'] = $insert_data['milk_price_date'];
                $history_insert_data['milk_price_history'] = $insert_data['milk_price'];
                $history_insert_data['InsUser'] = $this->admin_id;
                $history_insert_data['InsTerminal'] = $this->input->ip_address();
                $history_insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->insertInformation($history_insert_data, 'tbl_milk_price_history');
            } else {
                $insert_data['UpdUser'] = $this->admin_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'milk_price_id', $id, 'tbl_milk_price');

                $history_insert_data = array();
                $history_insert_data['ref_milk_price_id'] = $id;
                $history_insert_data['ref_milk_id_history'] = $milk_price_data->ref_milk_id;
                $history_insert_data['milk_price_history_date'] = $milk_price_data->milk_price_date;
                $history_insert_data['milk_price_history'] = $milk_price_data->milk_price;
                $history_insert_data['InsUser'] = $this->admin_id;
                $history_insert_data['InsTerminal'] = $this->input->ip_address();
                $history_insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->insertInformation($history_insert_data, 'tbl_milk_price_history');
            }
            redirect('admin/Milk');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('MILK', 'ADD', 'admin');
                $view = 'admin/milk/addEditMilkPrice';
                $this->page_title = 'MILK';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('MILK', 'EDIT', 'admin');
                $data['encrypted_id'] = $encrypted_id;
                $view = 'admin/milk/addEditMilkPrice';
                $this->page_title = 'MILK';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteMilkPrice() {
        $this->Common_model->check_menu_access('MILK', 'DELETE', 'admin');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $insert_data = array();
            $insert_data['del_status'] = "Delete";
            $insert_data['DelUser'] = $this->admin_id;
            $insert_data['DelTerminal'] = $this->input->ip_address();
            $insert_data['DelDateTime'] = date('Y-m-d H:i:s');

            if ($this->Common_model->updateInformation2($insert_data, 'milk_price_id', $id, 'tbl_milk_price')) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $this->_show_message("Something went wrong try again.", "error");
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

}
