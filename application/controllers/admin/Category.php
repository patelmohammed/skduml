<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends MY_Controller {

    public $menu;

    function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->load->model('Categories');
        $this->load->model('Common_model');
        
        $this->page_id = 'MASTERS';
        $this->menu_id = 'PRODUCT_CATEGORY';
    }

    //Default loading for Category system.
    public function index() {
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT_CATEGORY', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PRODUCT_CATEGORY', 'ADD','admin');
        
        $view = 'admin/category/add_category_form';
        $this->page_title = 'PRODUCT CATEGORY';
        $this->load_admin_view($view, $data);
    }

    //Manage category form
    public function manage_category() {
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT_UNIT', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PRODUCT_CATEGORY', 'VIEW','admin');
        $view = 'admin/category/category';
        $this->page_title = 'PRODUCT CATEGORY';
        $this->load_admin_view($view, $data);
    }

    public function get_category_list() {
        $menu_rights = $this->Common_model->get_menu_rights('PRODUCT_CATEGORY','admin');

        $data['groupMasterData'] = array();
        $masterData = $this->Categories->get_category_list($this->input->post());
        $cnt = 0;
        $CategoryName = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $CategoryName = $val->category_name;

            $active_action = '';
            if ($val->active_status == 1) {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Category/deactivateCategory/' . $val->category_id . '\'" data-toggle="tooltip" data-placement="left" title="Active Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox" checked/>
                            <label style="border: 0px; margin: 0px;"for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> ';
            } else {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Category/activateCategory/' . $val->category_id . '\'" data-toggle="tooltip" data-placement="left" title="Inactive Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox"/>
                            <label style="border: 0px; margin: 0px;" for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> </a>';
            }

            $active_status = $active_action;

            $action_new = '';
            $action_new .= form_open();
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Category/category_update_form/' . $val->category_id . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" title="Edit" data-toggle="tooltip" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>\'><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="DeleteCategory btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2" name="' . $val->category_id . '" data-toggle="tooltip" title="Delete Record" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>\'><i class="fal fa-times"></i></a>';
            }
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $CategoryName, $active_status, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Categories->get_category_list_search_data($this->input->post()),
            "recordsFiltered" => $this->Categories->get_category_list_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    //Insert category and upload
    public function insert_category() {
        $this->Common_model->check_menu_access('PRODUCT_CATEGORY', 'ADD','admin');
        $data = array(
            'category_name' => $this->input->post('category_name'),
            'status' => 1
        );

        $result = $this->Categories->category_entry($data);

        if ($result == TRUE) {
            $this->_show_message("Information has been added successfully!", "success");
            if (isset($_POST['add-category'])) {
                redirect(base_url('admin/Category/manage_category'));
            } elseif (isset($_POST['add-category-another'])) {
                redirect(base_url('admin/Category'));
            }
        } else {
            $this->_show_message("Information already inserted", "error");
            redirect(base_url('admin/Category'));
        }
    }

    //Category Update Form
    public function category_update_form($category_id) {
        
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT_CATEGORY', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PRODUCT_CATEGORY', 'EDIT','admin');
        $category_detail = $this->Categories->retrieve_category_editdata($category_id);

        $data['category_detail'] = $category_detail;
        
        $view = 'admin/category/edit_category_form';
        $this->page_title = 'PRODUCT CATEGORY';
        $this->load_admin_view($view, $data);
    }

    // Category Update
    public function category_update() {
         $this->Common_model->check_menu_access('PRODUCT_CATEGORY', 'EDIT','admin');
        $category_id = $this->input->post('category_id');
        $data = array(
            'category_name' => $this->input->post('category_name'),
        );

        $this->Categories->update_category($data, $category_id);
        $this->_show_message("Information has been updated successfully!", "success");
        redirect(base_url('admin/Category/manage_category'));
    }

    //customer active
    public function activateCategory($category_id) {
        $this->Common_model->check_menu_access('PRODUCT_CATEGORY', 'EDIT','admin');
        $data = array();
        $data['active_status'] = '1';
        $this->Categories->updateStatus($data, $category_id);
        $this->_show_message("Information has been activated!", "success");
        redirect(base_url('admin/Category/manage_category'));
    }

    //Customer Inactive
    public function deactivateCategory($category_id) {
        $this->Common_model->check_menu_access('PRODUCT_CATEGORY', 'EDIT','admin');
        $data = array();
        $data['active_status'] = '0';
        $this->Categories->updateStatus($data, $category_id);
        $this->_show_message("Information has been inactivated!", "success");
        redirect(base_url('admin/Category/manage_category'));
    }

    // Category delete
    public function category_delete() {
//        $chk_for_delete = 0;
        $this->Common_model->check_menu_access('PRODUCT_CATEGORY', 'DELETE', 'admin');
        $category_id = $this->input->post('id');
        $data = [];
        $chk_for_delete = $this->Common_model->check_for_delete_master('product_information', 'category_id', $category_id, 1);
//        preprint($chk_for_delete);

        if ($chk_for_delete > 0) {
            $this->_show_message("You can't delete this Category! Because This used in product.", "warning");
            $data['result'] = false;
        } else {
            $this->Categories->delete_category($category_id);
            $this->_show_message("Information has been deleted successfully!", "success");
            $data['result'] = true;
        }
        echo json_encode($data);
        die;
    }
    
    public function checkProductCategory($id = '') {

        $category_name = $this->input->get('category_name');

        if ($id != '') {
            if ($this->Categories->checkExistingCategory(trim($category_name), $id)) {
                echo 'false';
                die;
            }
        } else if ($this->Categories->checkExistingCategory(trim($category_name))) {
            echo 'false';
            die;
        }
        echo 'true';
        die;
    }
}
