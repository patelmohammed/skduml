<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Sale_return extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Sale_return_model');
        $this->load->model('Purchase_return_model');
        $this->load->model('Suppliers');
        $this->load->model('Categories');
        $this->load->model('Common_model');
        $this->load->model('Units');
        $this->page_id = 'SALES';
        $this->menu_id = 'SALE_RETURN';
    }

    public function index() {
        $data = array();

        $this->page_title = 'Sale Return';
        $code = $this->Common_model->getCompanyCode();
        $data['company_code'] = strtoupper($code);

        $view = 'admin/sale_return/sale_return';
        $this->load_admin_view($view, $data);
    }

    public function manageReturnList() {
        $data = array();

        $this->Common_model->check_menu_access('SALE_RETURN', 'VIEW', 'admin');
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SALE_RETURN', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('Unauthorized');
        }

        $this->page_title = 'Sale Return';
        $data['employee_list'] = $this->Common_model->get_all_users();

        $code = $this->Common_model->getCompanyCode();
        $data['company_code'] = strtoupper($code);

        $view = 'admin/sale_return/return_list';
        $this->load_admin_view($view, $data);
    }

    public function get_sale_return_list() {
        $menu_rights = $this->Common_model->get_menu_rights('SALE_RETURN', 'admin');

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);

        $data['groupMasterData'] = array();
        $masterData = $this->Sale_return_model->get_sale_return_list($this->input->post());
        $cnt = 0;
        $DateTime = $SRNO = $CustomerName = $ProductCount = $GrandTotal = $created_by = $updated_by = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $DateTime = $val->DateTime;
            $SRNO = $val->SRNO;
            $CustomerName = $val->CustomerName;
            $ProductCount = $val->ProductCount;
            $GrandTotal = $val->GrandTotal;
            $created_by = $val->created_by;
            $updated_by = $val->updated_by;

            $action_new = '';
            $action_new .= form_open();
            $action_new .= '<div style="display: flex; gap: 5px; flex-wrap: nowrap; justify-content: center;">';
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Sale_return/saleReturnUpdateForm/' . $val->SRID . '" class="btn btn-outline-primary hover-effect-dot btn-icon btn-sm" data-toggle="tooltip" title="" data-original-title="Update" data-template="<div class=&quot;tooltip&quot; role=&quot;tooltip&quot;><div class=&quot;tooltip-inner bg-primary-500&quot;></div></div>"><i class="fal fa-edit" aria-hidden="true"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="btn btn-outline-danger btn-icon hover-effect-dot btn-sm deleteSaleReturn" name="' . $val->SRID . '" data-toggle="tooltip" title="" data-original-title="Delete" style="margin-left: 5px;" data-toggle="tooltip" data-template="<div class=&quot;tooltip&quot; role=&quot;tooltip&quot;><div class=&quot;tooltip-inner bg-danger-500&quot;></div></div>"><i class="fal fa-times" aria-hidden="true"></i></a>';
            }
//            $action_new .= '<a href="javascript:void(0);" class="btn btn-warning btn-sm printSaleReturn" name="' . $val->SRID . '" data-SRNO="' . cleanString($val->SRNO) . '" data-toggle="tooltip" data-placement="right" title="Print" style="margin-left: 5px;"><i class="fa fa-barcode" aria-hidden="true"></i></a>';
            $action_new .= '<a href="' . base_url() . 'admin/Sale_return/printPDFSaleReturn/' . $val->SRID . '" class="btn btn-outline-warning hover-effect-dot btn-icon btn-sm" name="' . $val->SRID . '" data-SRNO="' . cleanString($val->SRNO) . '" data-toggle="tooltip" title="" data-original-title="Print" style="margin-left: 5px;" data-toggle="tooltip" data-template="<div class=\'tooltip\' role=\'tooltip\'><div class=\'tooltip-inner bg-warning-500\'></div></div>"><i class="fal fa-barcode" aria-hidden="true"></i></a>';
            $action_new .= '</div>';
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $SRNO, $DateTime, $CustomerName, $ProductCount, $GrandTotal, $created_by, $updated_by, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Sale_return_model->get_sale_return_list_search_data($this->input->post()),
            "recordsFiltered" => $this->Sale_return_model->get_sale_return_list_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    public function createSaleReturn() {
        $this->page_title = 'Add Sale Return';
        $this->Common_model->check_menu_access('SALE_RETURN', 'ADD', 'admin');

        $data['menu_rights'] = $this->Common_model->get_menu_rights('SALE_RETURN', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('Unauthorized');
        }

        $view = 'admin/sale_return/add_sale_return';
        $this->load_admin_view($view, $data);
    }

    public function addSaleReturn() {
        $data = array();

        $this->Common_model->check_menu_access('SALE_RETURN', 'ADD', 'admin');

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $SINO = $this->input->post('sale_invoice_no');

        $invoice_data = $this->Sale_return_model->getSaleInvoiceData($SINO);

        if (isset($invoice_data) && !empty($invoice_data)) {
            $data['sale_invoice_data'] = $invoice_data;
            $invoice_item_data = $this->Sale_return_model->getSaleInvoiceProductDetails($invoice_data->SIID);

            if (isset($invoice_item_data) && !empty($invoice_item_data)) {
                $cnt = -1;
                foreach ($invoice_item_data as $key => $value) {
                    if ($value->returnable_qty > 0) {
                        $cnt = $cnt + 1;
                        $data['sale_invoice_item_data'][$cnt] = $value;
                    }
                }
            }

            if (isset($data['sale_invoice_item_data']) && !empty($data['sale_invoice_item_data'])) {
                $data['status'] = true;
            } else {
                $data['status'] = false;
                $data['msg'] = 'Sale invoice products are returned already!';
            }
        } else {
            $data['status'] = false;
            $data['msg'] = 'Please enter valid sale invoice number!';
        }

        echo json_encode($data);
        die();
    }

    public function insertSaleReturn() {
        $SIID = $this->input->post('hidden_sale_invoice_id');
        $invoice_data = array();

        $DateTime = $this->input->post('datetime') ? date("Y-m-d H:i:s", strtotime($this->input->post('datetime'))) : date("Y-m-d H:i:s");
        $invoice_data['DateTime'] = $DateTime;

        $ref_no = $this->Sale_return_model->generateSaleReturnNo();
        $SRNO = FIN_YEAR . '-' . COMPANY_SHORT_NAME . '-SR-0001';

        if (isset($ref_no) && !empty($ref_no)) {
            $tmp_no = substr($ref_no, -4);
            $SRNO = FIN_YEAR . '-' . COMPANY_SHORT_NAME . '-SR-' . sprintf('%04d', $tmp_no + 1);
        }

        $invoice_data['SRNO'] = $SRNO;
        $invoice_data['refSIID'] = $SIID;

        $RefCustomerID = $this->input->post('hidden_customer_id');
        $invoice_data['RefCustomerID'] = $RefCustomerID;
        $invoice_data['RefCustomerAddress'] = $this->input->post('hidden_customer_address') ? $this->input->post('hidden_customer_address') : null;
        $invoice_data['RefCustomerPhone'] = $this->input->post('hidden_customer_phone') ? $this->input->post('hidden_customer_phone') : null;
        $invoice_data['RefCustomerEmail'] = $this->input->post('hidden_customer_email') ? $this->input->post('hidden_customer_email') : null;

        $unit_qua = $this->input->post('BasicTotal');
        if (isset($unit_qua) && $unit_qua != '') {
            $BasicTotal = 0;
            foreach ($unit_qua as $key => $total) {
                $BasicTotal += $total;
            }
            $invoice_data['BasicTotal'] = $BasicTotal;
        }

        $discount = $this->input->post('discount_ind');
        if (isset($discount) && $discount != '') {
            $Discount = 0;
            foreach ($discount as $key => $disc) {
                $disc = isset($disc) ? $disc : 0;
                $Discount += $disc;
            }
            $invoice_data['DiscountTotal'] = $Discount;
        } else {
            $invoice_data['DiscountTotal'] = '';
        }

        $tax = $this->input->post('total_tax');
        if (isset($tax) && $tax != '') {
            $Taxtotal = 0;
            foreach ($tax as $key => $ta) {
                $Taxtotal += $ta;
            }
            $invoice_data['Taxtotal'] = $Taxtotal;
        } else {
            $invoice_data['Taxtotal'] = '';
        }

        $GrandTotal = $this->input->post('g_total');
        $invoice_data['GrandTotal'] = $GrandTotal;

        $invoice_data['InsUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        ;
        $invoice_data['InsTerminal'] = $this->input->ip_address();
        $invoice_data['InsDateTime'] = date('Y/m/d H:i:s');
        $invoice_data['UpdUser'] = NULL;
        $invoice_data['UpdTerminal'] = NULL;
        $invoice_data['UpdDateTime'] = NULL;

        $chk_uniq_invoice = true;

//        $chk_uniq_invoice = $this->Common_model->chkUniqueCode('sale_return ', 'refSIID  ', $SIID, 1, '', '', 'DateTime', $DateTime); //('table_name',column_name for chk,'column_data',status)

        if ($chk_uniq_invoice == true) {
            $SRID = $this->Sale_return_model->insertSaleReturn($invoice_data);

            $total_amt = $this->input->post('total_amt');
            $qty = $this->input->post('quantity_amount');
            $product_rate = $this->input->post('unit_price');
            $discount_per = $this->input->post('discount');

            $product_id = $this->input->post('product_id');
            if (!empty($product_id)) {
                foreach ($product_id as $k1 => $v1) {
                    $product_data['refSRID'] = $SRID;
                    $product_data['refProductID'] = $product_id[$k1];

                    $UnitAmt = (isset($product_rate[$k1]) && !empty($product_rate[$k1]) ? $product_rate[$k1] : 0);
                    $product_data['UnitAmt'] = $UnitAmt;

                    $Qty_new = (isset($qty[$k1]) && !empty($qty[$k1]) ? $qty[$k1] : 0);
                    $product_data['Qty'] = $Qty_new;

                    $total_amt1 = (isset($total_amt[$k1]) && !empty($total_amt[$k1]) ? $total_amt[$k1] : ($UnitAmt * $Qty_new));
                    $product_data['TotalAmt'] = $total_amt1;

                    $product_data['DiscountPer'] = isset($discount_per[$k1]) && !empty($discount_per[$k1]) ? $discount_per[$k1] : 0;
                    $product_data['DiscountAmt'] = isset($discount[$k1]) && !empty($discount[$k1]) ? $discount[$k1] : 0;

                    $this->db->insert('sale_return_item ', $product_data);
                    $SRItemID = $this->db->insert_id();

                    $Tax_id = $_POST['Tax_id'][$k1];
                    $tax_per = $_POST['tax_per'][$k1];
                    $tax_name = $_POST['tax_name'][$k1];
                    $tax_id_array = isset($Tax_id) && !empty($Tax_id) ? explode(', ', $Tax_id) : null;
                    $tax_per_array = isset($tax_per) && !empty($tax_per) ? explode(', ', $tax_per) : null;
                    $tax_name_array = isset($tax_name) && !empty($tax_name) ? explode(', ', $tax_name) : null;
                    foreach ($tax_id_array as $key2 => $value) {
                        $tax_data['refSRID'] = $SRID;
                        $tax_data['refSRItemID'] = $SRItemID;
                        $tax_data['refTaxID'] = $value;
                        $tax_data['refTaxName'] = isset($tax_name_array[$key2]) && !empty($tax_name_array[$key2]) ? $tax_name_array[$key2] : null;
                        $tax_per_new = isset($tax_per_array[$key2]) && !empty($tax_per_array[$key2]) ? $tax_per_array[$key2] : 0;
                        $tax_data['TaxPer'] = $tax_per_new;
                        $tax_data['TaxAmt'] = ($total_amt1 * $tax_per_new) / 100;
                        $this->db->insert('sale_return_itemtaxdet ', $tax_data);
                    }
                }
            }

            $customer_ledger['transaction_id'] = null;
            $customer_ledger['customer_id'] = $RefCustomerID;
            $customer_ledger['invoice_no'] = $SRID;
            $customer_ledger['date'] = $DateTime;
            $customer_ledger['description'] = 'Return';
            $customer_ledger['amount'] = $GrandTotal;
            $customer_ledger['status'] = 1;
            $customer_ledger['d_c'] = 'd';
            $customer_ledger['ref_table_name'] = 'sale_return';

            $this->Common_model->insertData('customer_ledger ', $customer_ledger);

            $this->session->set_userdata(array('message' => 'Sale return added successfully.'));
            if (isset($_POST['add-sale-invoice'])) {
                redirect(base_url('admin/Sale_return/manageReturnList'));
                exit;
            } elseif (isset($_POST['add-sale-invoice-another'])) {
                redirect(base_url('admin/Sale_return/createSaleReturn'));
                exit;
            }
            redirect(base_url('admin/Sale_return/manageReturnList'));
        } else {
            $this->session->set_userdata(array('error_message' => "This Sale Return already exist"));
            redirect(base_url('admin/Sale_return/manageReturnList'));
        }
    }

    public function saleReturnUpdateForm($SRID) {
        $this->page_title = 'Update Sale Return';
        $data = array();

        $this->Common_model->check_menu_access('SALE_RETURN', 'EDIT', 'admin');

        $data['SRID'] = $SRID;
        $data['sale_return_data'] = $this->Sale_return_model->getSaleReturnDataByReturnId($SRID);
        $SIID = $data['sale_return_data']['refSIID'];
        $data['SIID'] = $SIID;
        $data['sale_invoice_data'] = (array) $this->Sale_return_model->getSaleInvoiceData(null, $SIID);

        $view = 'admin/sale_return/edit_sale_return';
        $this->load_admin_view($view, $data);
    }

    public function updateSaleReturn($SRID) {
        $invoice_data = array();

        $SIID = $this->input->post('hidden_sale_invoice_id');

        $DateTime = $this->input->post('datetime') ? date("Y-m-d H:i:s", strtotime($this->input->post('datetime'))) : date("Y-m-d H:i:s");

        $unit_qua = $this->input->post('BasicTotal');
        if (isset($unit_qua) && $unit_qua != '') {
            $BasicTotal = 0;
            foreach ($unit_qua as $key => $total) {
                $BasicTotal += $total;
            }
            $invoice_data['BasicTotal'] = $BasicTotal;
        }

        $discount = $this->input->post('discount_ind');
        if (isset($discount) && $discount != '') {
            $Discount = 0;
            foreach ($discount as $key => $disc) {
                $disc = isset($disc) ? $disc : 0;
                $Discount += $disc;
            }
            $invoice_data['DiscountTotal'] = $Discount;
        } else {
            $invoice_data['DiscountTotal'] = '';
        }

        $tax = $this->input->post('total_tax');
        if (isset($tax) && $tax != '') {
            $Taxtotal = 0;
            foreach ($tax as $key => $ta) {
                $Taxtotal += $ta;
            }
            $invoice_data['Taxtotal'] = $Taxtotal;
        } else {
            $invoice_data['Taxtotal'] = '';
        }

        $GrandTotal = $this->input->post('g_total');
        $invoice_data['GrandTotal'] = $GrandTotal;

        $invoice_data['UpdUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        ;
        $invoice_data['UpdTerminal'] = $this->input->ip_address();
        $invoice_data['UpdDateTime'] = date('Y/m/d H:i:s');

        $customer_ledger['amount'] = $GrandTotal;
        $customer_ledger['ref_table_name'] = 'sale_return';
        $customer_ledger['d_c'] = 'd';

        if ($SRID != '') {
            $this->Common_model->updateData('sale_return', 'SRID', $SRID, $invoice_data, 1);
            $this->Common_model->updateData2('customer_ledger', 'invoice_no', $SRID, 'ref_table_name', 'sale_return', $customer_ledger, 'Live');

            $this->Sale_return_model->deleteSaleReturnItem($SRID, 'sale_return_item');
            $this->Sale_return_model->deleteSaleReturnItem($SRID, 'sale_return_itemtaxdet');

            $total_amt = $this->input->post('total_amt');
            $qty = $this->input->post('quantity_amount');
            $product_rate = $this->input->post('unit_price');
            $discount_per = $this->input->post('discount');

            $product_id = $this->input->post('product_id');
            if (!empty($product_id)) {
                foreach ($product_id as $k1 => $v1) {
                    $product_data['refSRID'] = $SRID;
                    $product_data['refProductID'] = isset($product_id[$k1]) && !empty($product_id[$k1]) ? $product_id[$k1] : null;
                    $product_data['UnitAmt'] = isset($product_rate[$k1]) && !empty($product_rate[$k1]) ? $product_rate[$k1] : null;
                    $total_amt1 = isset($total_amt[$k1]) && !empty($total_amt[$k1]) ? $total_amt[$k1] : null;
                    $product_data['TotalAmt'] = $total_amt1;
                    $product_data['Qty'] = isset($qty[$k1]) && !empty($qty[$k1]) ? $qty[$k1] : 0;
                    $product_data['DiscountPer'] = isset($discount_per[$k1]) && !empty($discount_per[$k1]) ? $discount_per[$k1] : 0;
                    $product_data['DiscountAmt'] = isset($discount[$k1]) && !empty($discount[$k1]) ? $discount[$k1] : 0;
                    $this->db->insert('sale_return_item', $product_data);
                    $SRItemID = $this->db->insert_id();

                    $Tax_id = $_POST['Tax_id'][$k1];
                    $tax_per = $_POST['tax_per'][$k1];
                    $tax_name = $_POST['tax_name'][$k1];
                    $tax_id_array = isset($Tax_id) && !empty($Tax_id) ? explode(', ', $Tax_id) : null;
                    $tax_per_array = isset($tax_per) && !empty($tax_per) ? explode(', ', $tax_per) : null;
                    $tax_name_array = isset($tax_name) && !empty($tax_name) ? explode(', ', $tax_name) : null;
                    foreach ($tax_id_array as $key2 => $value) {
                        $tax_data['refSRID'] = $SRID;
                        $tax_data['refSRItemID'] = $SRItemID;
                        $tax_data['refTaxID'] = $value;
                        $tax_data['refTaxName'] = isset($tax_name_array[$key2]) && !empty($tax_name_array[$key2]) ? $tax_name_array[$key2] : null;
                        $tax_per_new = isset($tax_per_array[$key2]) && !empty($tax_per_array[$key2]) ? $tax_per_array[$key2] : 0;
                        $tax_data['TaxPer'] = $tax_per_new;
                        $tax_data['TaxAmt'] = ($total_amt1 * $tax_per_new) / 100;
                        $this->db->insert('sale_return_itemtaxdet', $tax_data);
                    }
                }
            }

            if (!empty($SRItemID) && !empty($SRItemID)) {
                $this->session->set_userdata(array('message' => 'Sale return updated successfully.'));
                if (isset($_POST['add-sale-invoice'])) {
                    redirect(base_url('admin/Sale_return/manageReturnList'));
                    exit;
                } elseif (isset($_POST['add-sale-invoice-another'])) {
                    redirect(base_url('admin/Sale_return/createSaleReturn'));
                    exit;
                }
                redirect(base_url('admin/Sale_return/manageReturnList'));
            } else {
                $this->session->set_userdata(array('error_message' => "This Sale Return already exist"));
                redirect(base_url('admin/Sale_return/manageReturnList'));
            }
        } else {
            $this->session->set_userdata(array('error_message' => "This Sale Return already exist"));
            redirect(base_url('admin/Sale_return/manageReturnList'));
        }
    }

    public function deleteSaleReturn() {
        $this->Common_model->check_menu_access('SALE_RETURN', 'DELETE', 'admin');
        $sale_return_id = $this->input->post('sale_return_id');
        $result = $this->Sale_return_model->deleteSaleReturn($sale_return_id);
        return TRUE;
    }

    public function printSaleInvoicePdf() {
        $data = array();

        $SOID = $this->input->post('sale_invoice_id');
        $data['sale_invoice'] = $this->Sale_return_model->product_edit_data($SOID);
        $data['sale_invoice_list'] = $this->Sale_return_model->purchase_invoice_list_for_print($SOID);
        $data['company_detail'] = $this->Sale_return_model->get_company_detail();
        $data['tax_detail'] = $this->Sale_return_model->invoice_tax_detail_for_print($SOID);
        $data['customer_detail'] = $this->Common_model->getCustomerDataByid($data['sale_invoice']['RefCustomerID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['sale_invoice']['SINO'] = $company_code . "-" . sprintf('%06d', $data['sale_invoice']['SINO']);
        $data['company_code'] = $company_code;

        $content = $this->parser->parse('sale_invoice/ajax_pdf', $data, true);
        echo $content;
        die;
    }

    public function getSaleOutwardProductVise() {


        $product_id = $this->input->post('product_id');
        $customer_id = $this->input->post('customer_id');
        $data['sale_outward_detail'] = $this->Sale_return_model->getSaleOutwardProductVise($product_id, $customer_id);
        if ($data['sale_outward_detail'] != '' && $data['sale_outward_detail'] != ' ' && isset($data['sale_outward_detail']) && !empty($data['sale_outward_detail'])) {
            $data['result'] = true;
            echo json_encode($data);
            die;
        } else {
            $data['result'] = false;
            echo json_encode($data);
            die;
        }
    }

    public function printSaleReturn() {
        $data = array();

        $SRID = $this->input->post('sale_return_id');
        $data['sale_return_data'] = $this->Sale_return_model->saleReturnDataByID($SRID);
        $data['sale_return_item_data'] = $this->Sale_return_model->saleReturnItemDataById($SRID);
        $data['company_detail'] = $this->Common_model->get_company_detail();
        $data['tax_detail'] = $this->Sale_return_model->saleReturnTaxDetail($SRID);
        $data['customer_detail'] = $this->Common_model->getCustomerDataByid($data['sale_return_data']['RefCustomerID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['CurrentUser'] = $this->db->where("user_id", $data['sale_return_data']['InsUser'])->get('users')->row();
        $data['bank_data'] = $this->Common_model->getBankDetails();

        $content = $this->load->view('sale_return/ajax_pdf', $data, true);
        echo $content;
        die;
    }

    function printPDFSaleReturn($SRID) {
        $data = array();

        $data['sale_return_data'] = $this->Sale_return_model->saleReturnDataByID($SRID);
        $data['sale_return_item_data'] = $this->Sale_return_model->saleReturnItemDataById($SRID);
        $data['company_detail'] = get_company_detail();
        $data['tax_detail'] = $this->Sale_return_model->saleReturnTaxDetail($SRID);
        $data['customer_detail'] = $this->Common_model->getCustomerDataByid($data['sale_return_data']['RefCustomerID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['company_code'] = $company_code;
        $data['CurrentUser'] = $this->db->where("id", $data['sale_return_data']['InsUser'])->get('user_information')->row();
        $data['bank_data'] = $this->Common_model->getBankDetails();

        if (!empty($data)) {
            try {
                $pdf_data = '';
                $pdfhtml = $this->load->view('admin/pdf/Sale_return', $data, true);
                $pdf_data .= str_replace('%%File_type%%', 'ORIGINAL', $pdfhtml);
                $pdf_data .= str_replace('%%File_type%%', 'DUPLICATE', $pdfhtml);
                $pdfName = cleanString($data['sale_return_data']['SRNO']) . '.pdf';
                $html2pdf = new Html2Pdf('L', 'A5', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdf_data);
                $html2pdf->output($pdfName, 'I');
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
    }

    public function mailSaleReturnDetail() {
        $data = $attachment = $mail_data = array();
        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $SRID = $this->input->post('SRID');
        $mail_to = $this->input->post('mail_to');
        $mail_subject = $this->input->post('mail_subject');
        $mail_body = $this->input->post('mail_body');
        $send_me = $this->input->post('send_me');
        $data['subject'] = !empty($mail_subject) ? $mail_subject : 'Sale Return Detail';
        $mail_to_new = implode(', ', $mail_to);
        $mail_data['email_id'] = $mail_to_new;

        if ($send_me == 'true') {
            $mail_to_new .= ', ' . $this->session->userdata('user_email');
            $mail_data['send_me'] = $this->session->userdata('user_email');
        }

        $data['sale_return_data'] = $this->Sale_return_model->saleReturnDataByID($SRID);
        $data['sale_return_item_data'] = $this->Sale_return_model->saleReturnItemDataById($SRID);
        $data['company_detail'] = $this->Common_model->get_company_detail();
        $data['tax_detail'] = $this->Sale_return_model->saleReturnTaxDetail($SRID);
        $data['customer_detail'] = $this->Common_model->getCustomerDataByid($data['sale_return_data']['RefCustomerID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['sale_return_data']['SRNO'] = 'SR-' . $company_code . "-" . sprintf('%06d', $data['sale_return_data']['SRNO']);

        if (!empty($data)) {
            try {
                $new_path = 'my-assets/documents/pdf/SaleReturn/';
                if (!is_dir($new_path)) {
                    if (!mkdir($new_path, 0777, true)) {
                        die('Not Created');
                    }
                }
                $pdfhtml = $this->parser->parse('pdf/Sale_return', $data, true);
                $pdfName = $new_path . cleanString($data['sale_return_data']['SRNO']) . '_' . date('H_i_s') . '.pdf';
                $attachment = array(getcwd() . '/' . $pdfName);
                $html2pdf = new Html2Pdf('P', 'A4', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdfhtml);
                $html2pdf->output(getcwd() . '/' . $pdfName, 'F');
                $attachment[] = getcwd() . '/' . $pdfName;
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }

        $data['login_url'] = 'google.com';
        $body = !empty($mail_body) ? $mail_body : 'Sale Return Data';
        $data['company'] = $this->Common_model->get_company_detail();
        $company_name = $data['company']['company_name'];
        $data['body'] = $body;

        if (!empty($data)) {
            try {
                $from_email = "inquiry@foodmohalla.in";
                $this->email->from($from_email, $company_name);
                $this->email->subject($data['subject']);
                $this->email->message($data['body']);
                $this->email->attach($pdfName);
                if (!empty($mail_to_new)) {
                    $this->email->to($mail_to_new);
                    if ($this->email->send()) {
                        $mail_data['refTableName'] = 'sale_return';
                        $mail_data['refTableId'] = $SRID;
                        $mail_data['attachments'] = $pdfName;
                        $mail_data['mailSubject'] = $data['subject'];
                        $mail_data['mailBody'] = $data['body'];
                        $mail_data['InsUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
                        ;
                        $mail_data['InsTerminal'] = $this->input->ip_address();
                        $mail_data['InsDateTime'] = date('Y/m/d H:i:s');
                        $this->Common_model->insertData('mailhistory', $mail_data);

                        $data['result'] = true;
                    }
                }
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
        echo json_encode($data);
        die;
    }
}
