<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Csupplier extends MY_Controller {

    public $supplier_id;

    function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }

        $this->load->model('Suppliers');
        $this->load->model('Common_model');
        $this->load->model('Web_settings');

        $this->page_id = 'SUPPLIERS';
        $this->menu_id = 'SUPPLIER';
    }

    public function index() {

        $data['menu_rights'] = $this->Common_model->get_menu_rights('SUPPLIER', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SUPPLIER', 'ADD', 'admin');

        $data['stateList'] = $this->Common_model->get_all_states();

        $view = 'admin/supplier/add_supplier_form';
        $this->page_title = 'SUPPLIER';
        $this->load_admin_view($view, $data);
    }

    //Insert supplier
    public function insert_supplier() {

        $data = array(
            'supplier_name' => $this->input->post('supplier_name'),
            'contact_person' => $this->input->post('contact_person'),
            'ref_state_id' => $this->input->post('ref_state_id'),
            'address' => $this->input->post('address'),
            'mobile' => $this->input->post('mobile'),
            'details' => $this->input->post('details'),
            'status' => 1,
            'supplier_email' => $this->input->post('supplier_email'),
            'office_num' => $this->input->post('office_number'),
            'supplier_gst_no' => $this->input->post('supplier_gst_no'),
            'supplier_bank_name' => $this->input->post('supplier_bank_name'),
            'bank_ifsc_code' => $this->input->post('bank_ifsc_code'),
            'bank_account_number' => $this->input->post('bank_account_number'),
            'account_holder_name' => $this->input->post('acoount_holder_name')
        );

        $supplier = $this->Suppliers->supplier_entry($data);

        if ($supplier) {
            //Previous balance adding -> Sending to supplier model to adjust the data.
            if (!empty($this->input->post('Previous_Credit_Balance'))) {
                $this->Suppliers->previous_balance_add($this->input->post('Previous_Credit_Balance'), $supplier);
            }
            $this->_show_message("Information has been added successfully!", "success");
            if (isset($_POST['add-supplier'])) {
                redirect(base_url('admin/Csupplier/manage_supplier'));
                exit;
            } elseif (isset($_POST['add-supplier-another'])) {
                redirect(base_url('admin/Csupplier'));
                exit;
            }
        } else {
            $this->_show_message("error ", "error");
            if (isset($_POST['add-supplier'])) {
                redirect(base_url('admin/Csupplier/manage_supplier'));
                exit;
            } elseif (isset($_POST['add-supplier-another'])) {
                redirect(base_url('admin/Csupplier'));
                exit;
            }
        }
    }

    //Manage supplier
    public function manage_supplier() {

        $data['menu_rights'] = $this->Common_model->get_menu_rights('SUPPLIER', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SUPPLIER', 'VIEW', 'admin');

        $view = 'admin/supplier/supplier';
        $this->page_title = 'SUPPLIER';
        $this->load_admin_view($view, $data);
    }

    public function get_supplier_list() {
        $menu_rights = $this->Common_model->get_menu_rights('SUPPLIER', 'admin');

        $data['groupMasterData'] = array();
        $masterData = $this->Suppliers->get_supplier_list($this->input->post());
        $cnt = 0;
        $SupplierName = $ContactPerson = $Address = $Mobile = $Details = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;

            $SupplierName = '';
            $SupplierName .= '<a href="' . base_url() . 'admin/Csupplier/supplier_ledger/' . $val->supplier_id . '">';
            $SupplierName .= $val->supplier_name;
            $SupplierName .= '</a>';

            $ContactPerson = $val->contact_person;
            $Address = $val->address;
            $Mobile = $val->mobile;
            $Email = $val->supplier_email;
            $GST_Num = $val->supplier_gst_no;
//            $Details = $val->details;

            $active_action = '';
            if ($val->active_status == 1) {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Csupplier/deactivateSupplier/' . $val->supplier_id . '\'" data-toggle="tooltip" data-placement="left" title="Active Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox" checked/>
                            <label style="border: 0px; margin: 0px;"for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> ';
            } else {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Csupplier/activateSupplier/' . $val->supplier_id . '\'" data-toggle="tooltip" data-placement="left" title="Inactive Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox"/>
                            <label style="border: 0px; margin: 0px;" for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> </a>';
            }

            $active_status = $active_action;

            $action_new = '';
            $action_new .= form_open();
            $action_new .= '<div style="display:flex;flex-wrap:nowrap;gap:5px;justify-content:center;">';

            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Csupplier/supplier_update_form/' . $val->supplier_id . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" title="Edit" data-toggle="tooltip" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>\'><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="deleteSupplier btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2" name="' . $val->supplier_id . '" data-toggle="tooltip" title="Delete Record" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>\'><i class="fal fa-times"></i></a>';
            }

            $action_new .= '</div>';
            $action_new .= form_close();

            $action = $action_new;

            $data['groupMasterData'][] = array($cnt, $SupplierName, $ContactPerson, $Address, $Mobile, $Email, $GST_Num, $active_status, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Suppliers->get_supplier_list_search_data($this->input->post()),
            "recordsFiltered" => $this->Suppliers->get_supplier_list_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    // search supplier 
    public function search_supplier() {

        $supplier_id = $this->input->post('supplier_id');
        $content = $this->Suppliers->supplier_search($supplier_id);
        $this->Suppliers->full_admin_html_view($content);
    }

//    //Supplier Update Form
    public function supplier_update_form($supplier_id) {

//        $content = $this->Suppliers->update_supplier($supplier_id);
//        $CI = & get_instance();
//        $CI->load->model('Suppliers');
//        $CI->Common_model->check_menu_access('SUPPLIER', 'EDIT');

        $supplier_detail = $this->Suppliers->retrieve_supplier_editdata($supplier_id);
        $data = array(
//            'title' => display('supplier_edit'),
            'supplier_id' => $supplier_detail[0]['supplier_id'],
            'ref_state_id' => $supplier_detail[0]['ref_state_id'],
            'supplier_name' => $supplier_detail[0]['supplier_name'],
            'contact_person' => $supplier_detail[0]['contact_person'],
            'address' => $supplier_detail[0]['address'],
            'mobile' => $supplier_detail[0]['mobile'],
            'supplier_email' => $supplier_detail[0]['supplier_email'],
            'office_num' => $supplier_detail[0]['office_num'],
            'details' => $supplier_detail[0]['details'],
            'supplier_gst_no' => $supplier_detail[0]['supplier_gst_no'],
            'supplier_bank_name' => $supplier_detail[0]['supplier_bank_name'],
            'bank_ifsc_code' => $supplier_detail[0]['bank_ifsc_code'],
            'bank_account_number' => $supplier_detail[0]['bank_account_number'],
            'account_holder_name' => $supplier_detail[0]['account_holder_name'],
            'active_status' => $supplier_detail[0]['active_status'],
            'status' => $supplier_detail[0]['status'],
            'stateList' => $this->Common_model->get_all_states()
        );

//        die();

        $view = 'admin/supplier/edit_supplier_form';
        $this->page_title = 'PRODUCT';
        $this->load_admin_view($view, $data);

//        
//        $chapterList = $CI->parser->parse('supplier/edit_supplier_form', $data, true);
//        return $chapterList;
    }

//
    // Supplier Update
    public function supplier_update() {
        $supplier_id = $this->input->post('supplier_id');
//         echo '<pre>'; print_r($this->input->post()); echo '</pre>';
        $data = array(
            'ref_state_id' => $this->input->post('ref_state_id'),
            'supplier_name' => $this->input->post('supplier_name'),
            'contact_person' => $this->input->post('contact_person'),
            'address' => $this->input->post('address'),
            'mobile' => $this->input->post('mobile'),
            'details' => $this->input->post('detais'),
            'supplier_email' => $this->input->post('email'),
            'office_num' => $this->input->post('office_num'),
            'supplier_gst_no' => $this->input->post('supplier_gst_no'),
            'supplier_bank_name' => $this->input->post('supplier_bank_name'),
            'bank_ifsc_code' => $this->input->post('bank_ifsc_code'),
            'bank_account_number' => $this->input->post('bank_account_number'),
            'account_holder_name' => $this->input->post('acoount_holder_name')
        );
//        echo '<pre>'; print_r($data); die;
        $result = $this->Suppliers->update_supplier($data, $supplier_id);

        if ($result == true) {
            $this->_show_message("Information has been updated successfully!", "success");
            redirect(base_url('admin/Csupplier/manage_supplier'));
        } else {
            redirect(base_url('admin/Csupplier/manage_supplier'));
        }
    }

    //customer active
    public function activateSupplier($supplier_id) {
        $data = array();
        $data['active_status'] = '1';
        $this->load->model('Suppliers');
        $this->Suppliers->updateStatus($data, $supplier_id);
        $this->_show_message("Information has been activated!", "success");
        redirect(base_url('admin/Csupplier/manage_supplier'));
    }

    //Customer Inactive
    public function deactivateSupplier($supplier_id) {
        $data = array();
        $data['active_status'] = '0';
        $this->load->model('Suppliers');
        $this->Suppliers->updateStatus($data, $supplier_id);
        $this->_show_message("Information has been inactivated!", "success");
        redirect(base_url('admin/Csupplier/manage_supplier'));
    }

    //Supplier Search Item
    public function supplier_search_item() {
        $supplier_id = $this->input->post('supplier_id');
        $content = $this->lsupplier->supplier_search_item($supplier_id);
        $this->template->full_admin_html_view($content);
    }

    // Supplier Delete from System

    public function supplier_delete() {
        $chk_for_delete = 0;
//        $CI = & get_instance();
//        $CI->Common_model->check_menu_access('SUPPLIER', 'DELETE');
        $this->Common_model->check_menu_access('SUPPLIER', 'DELETE', 'admin');

        $supplier_id = $this->input->post('id');

        $chk_for_delete += $this->Common_model->check_for_delete_master('supplier_product', 'supplier_id', $supplier_id, 1);
        $chk_for_delete += $this->Common_model->check_for_delete_master('purchase_invoice', 'RefSupplierID', $supplier_id, 1);
//        $chk_for_delete += $this->Common_model->check_for_delete_master('product_sale_item', 'RefSupplierID', $supplier_id, 1);

        
        if ($chk_for_delete <= 0) {
            if ($this->Suppliers->delete_supplier_data($supplier_id)) {
                $data['result'] = true;
                $data['msg'] = "Information has been deleted successfully!";
            } else {
                $data['result'] = false;
                $data['msg'] = "Something went wrong, Try again!";
            }
        } else {
            $data['result'] = false;
            $data['msg'] = "You can't delete this Supplier! Because supplier used in purchase or product.";
        }
        echo json_encode($data);
        die;
    }

    // Supplier details findings !!!!!!!!!!!!!! Inactive Now !!!!!!!!!!!!
    public function supplier_details($supplier_id) {
        $content = $this->lsupplier->supplier_detail_data($supplier_id);
        $this->supplier_id = $supplier_id;
        $this->template->full_admin_html_view($content);
    }

    //Supplier Ledger Book
    public function supplier_ledger($supplier_id = '') {
        $start = $this->input->post('from_date');
        if (isset($start) && !empty($start)) {
            $start = date('Y-m-d H:i:s', strtotime($start));
        }
        $end = $this->input->post('to_date');
        if (isset($end) && !empty($end)) {
            $end = date('Y-m-d', strtotime($end)) . ' ' . date('H:i:s');
        }

        if (empty($supplier_id)) {
            $supplier_id = $this->input->post('supplier_id');
        }
        $cat = $this->input->post('rep_cat');

//        preprint($cat);

        if (empty($supplier_id)) {
            $url = "admin/Csupplier/supplier_ledger_report";
            redirect(base_url($url));
            exit;
        }
        $sup_sale = $this->input->post('cat');

        if ($sup_sale == "2") {
            $url = "admin/Csupplier/supplier_sales_details" . '/' . $supplier_id . '/' . $start . '/' . $end;
            redirect(base_url($url));
            exit;
        }
        $sup_sale_summary = $this->input->post('cat');

        if ($sup_sale_summary == "3") {
            $url = "admin/Csupplier/supplier_sales_summary" . '/' . $supplier_id . '/' . $start . '/' . $end;
            redirect(base_url($url));
            exit;
        }
        $sup_sale_summary = $this->input->post('cat');

        if ($sup_sale_summary == "4") {
            $url = "admin/Csupplier/sales_payment_actual" . '/' . $supplier_id . '/' . $start . '/' . $end;
            redirect(base_url($url));
            exit;
        }

        $supplier = $this->Suppliers->supplier_list();
        $supplier_details = $this->Suppliers->supplier_personal_data($supplier_id);
        $ledger = $this->Suppliers->suppliers_ledger($supplier_id, $start, $end);
        $summary = $this->Suppliers->suppliers_transection_summary($supplier_id, $start, $end);

        $total_ammount = 0;
        $total_credit = 0;
        $total_debit = 0;

        $balance = 0;
        if (!empty($ledger)) {
            foreach ($ledger as $index => $value) {
                if (!empty($ledger[$index]->chalan_no) or $ledger[$index]->chalan_no == "NA") {
                    $ledger[$index]->credit = $ledger[$index]->amount;
                    $ledger[$index]->balance = $balance - $ledger[$index]->amount;
                    $ledger[$index]->debit = "";
                    $balance = $ledger[$index]->balance;
                } else {
                    $ledger[$index]->debit = $ledger[$index]->amount;
                    $ledger[$index]->balance = $balance + $ledger[$index]->amount;
                    $ledger[$index]->credit = "";
                    $balance = $ledger[$index]->balance;
                }
            }
        }

        $currency_details = $this->Web_settings->retrieve_setting_editdata();

        $data = array(
            'title' => 'Supplier Ledger',
            'ledgers' => $ledger,
            'supplier_name' => $supplier_details[0]['supplier_name'],
            'address' => $supplier_details[0]['address'],
            'total_amount' => number_format($summary[1][0]['total_debit'] - $summary[0][0]['total_credit'], 6, '.', ', '),
            'SubTotal_debit' => number_format($summary[1][0]['total_debit'], 6, '.', ', '),
            'SubTotal_credit' => number_format($summary[0][0]['total_credit'], 6, '.', ', '),
            'supplier_ledger' => 'Csupplier/supplier_ledger',
            'supplier' => $supplier,
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
            'links' => '',
            'supplier_id' => $supplier_id
        );

        $view = 'admin/supplier/supplier_ledger';
        $this->page_title = 'Supplier Ledger';
        $this->menu_id = 'SUPPLIER_LEDGER';
        $this->load_admin_view($view, $data);
    }

    public function supplier_ledger_report() {

        $data['menu_rights'] = $this->Common_model->get_menu_rights('SUPPLIER_LEDGER', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }


        $this->Common_model->check_menu_access('SUPPLIER_LEDGER', 'VIEW', 'admin');

        $supplier = $this->Suppliers->supplier_list();

        $ledger = $this->Suppliers->supplier_product_sale1();
        $summary = $this->Suppliers->suppliers_transection_summary1();

        $total_ammount = 0;
        $total_credit = 0;
        $total_debit = 0;

        $balance = 0;

        if (isset($ledger) && !empty($ledger)) {
            foreach ($ledger as $index => $value) {
                if (isset($ledger[$index]->d_c) && !empty($ledger[$index]->d_c) && $ledger[$index]->d_c == "c") {
                    $ledger[$index]->credit = $ledger[$index]->amount;
                    $ledger[$index]->balance = $balance - $ledger[$index]->amount;
                    $ledger[$index]->debit = "";
                    $balance = $ledger[$index]->balance;
                } else {
                    $ledger[$index]->debit = $ledger[$index]->amount;
                    $ledger[$index]->balance = $balance + $ledger[$index]->amount;
                    $ledger[$index]->credit = "";
                    $balance = $ledger[$index]->balance;
                }
            }
        }


        $currency_details = $this->Web_settings->retrieve_setting_editdata();

        $data = array(
            'ledgers' => $ledger,
            'supplier_name' => '',
            'total_amount' => number_format($summary[1][0]['total_debit'] - $summary[0][0]['total_credit'], 2, '.', ', '),
            'SubTotal_debit' => number_format($summary[1][0]['total_debit'], 2, '.', ', '),
            'SubTotal_credit' => number_format($summary[0][0]['total_credit'], 2, '.', ', '),
            'supplier_ledger' => 'Csupplier/supplier_ledger',
            'supplier' => $supplier,
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
            'links' => '',
        );

        $view = 'admin/supplier/supplier_ledger';
        $this->page_title = 'Supplier Ledger';
        $this->menu_id = 'SUPPLIER_LEDGER';
        $this->load_admin_view($view, $data);
    }

    // Supplier wise sales report details
    public function supplier_sales_details() {
        $start = $this->input->post('from_date');
        $end = $this->input->post('to_date');
        $supplier_id = $this->uri->segment(3);

        $content = $this->lsupplier->supplier_sales_details($supplier_id, $start, $end);
        $this->template->full_admin_html_view($content);
    }

    // Supplier wise sales report summary
    public function supplier_sales_summary() {
        #
        #pagination starts
        #
        $supplier_id = $this->uri->segment(4);
        $config["base_url"] = base_url('Csupplier/supplier_sales_summary/' . $supplier_id . "/");
        $config["total_rows"] = $this->Suppliers->supplier_sales_summary_count($supplier_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config["num_links"] = 5;
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #  
        $content = $this->lsupplier->supplier_sales_summary($supplier_id, $links, $config["per_page"], $page);

        $this->supplier_id = $supplier_id;
        $this->template->full_admin_html_view($content);
    }

    // Actual Ledger based on sales & deposited amount
    public function sales_payment_actual() {
        #
        #pagination starts
        $supplier_id = $this->uri->segment(3);

        $config["base_url"] = base_url('Csupplier/sales_payment_actual/' . $supplier_id . "/");
        $config["total_rows"] = $this->Suppliers->sales_payment_actual_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 6;
        $config["num_links"] = 5;
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #  
        $content = $this->lsupplier->sales_payment_actual($supplier_id, $links, $config["per_page"], $page);

        $this->supplier_id = $supplier_id;
        $this->template->full_admin_html_view($content);
    }

    // search report 
    public function search_supplier_report() {
        $start = $this->input->post('from_date');
        $end = $this->input->post('to_date');

        $content = $this->lpayment->result_datewise_data($start, $end);
        $this->template->full_admin_html_view($content);
    }

    //Supplier sales details all from menu
    public function supplier_sales_details_all() {
        $content = $this->lsupplier->supplier_sales_details_allm();
        $this->template->full_admin_html_view($content);
    }

    public function get_supplier_sales_list() {
        $menu_rights = $this->Common_model->get_menu_rights('SUPPLIER_SALE_DETAILS');

        $data['groupMasterData'] = array();
        $masterData = $this->Suppliers->get_supplier_sales_list($this->input->post());
        $cnt = 0;
        $SupplierName = $GrandTotal = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;

            $SupplierName = '';
            $SupplierName .= '<a href="' . base_url() . 'Csupplier/supplier_ledger_info/' . $val->RefSupplierID . '">';
            $SupplierName .= $val->supplier_name;
            $SupplierName .= '</a>';

            $GrandTotal = $val->GrandTotal;

            $data['groupMasterData'][] = array($cnt, $SupplierName, $GrandTotal);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Suppliers->get_supplier_sales_list_search_data($this->input->post()),
            "recordsFiltered" => $this->Suppliers->get_supplier_sales_list_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    public function checkSupplier($id = '') {

        $supplier = $this->input->get('supplier_name');

        if ($id != '') {
            if ($this->Suppliers->checkExistingSupplier(trim($supplier), $id)) {
                echo 'false';
                die;
            }
        } else if ($this->Suppliers->checkExistingSupplier(trim($supplier))) {
            echo 'false';
            die;
        }
        echo 'true';
        die;
    }

    public function printPdfSupplierLedger() {
        $data = array();

        $supplier_id = $this->input->post('supplier_id');

        $start_date = $this->input->post('start_date');
        $from_date = isset($start_date) && !empty($start_date) ? date('Y-m-d H:i:s', strtotime($start_date)) : null;

        $end_date = $this->input->post('end_date');
        $to_date = isset($end_date) && !empty($end_date) ? date('Y-m-d', strtotime($end_date)) . ' ' . date('H:i:s') : null;

        $data['supplier_ledger_data'] = $this->Suppliers->suppliers_ledger($supplier_id, $from_date, $to_date);
        $data['company_detail'] = get_company_detail();
        $data['supplier_detail'] = $this->Common_model->getSupplierDataByid($supplier_id);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['company_code'] = $company_code;
        $data['bank_data'] = $this->Common_model->getBankDetails();

        if (!empty($data)) {
            try {
                $pdf_data = '';
                $pdfhtml = $this->load->view('admin/pdf/Supplier_ledger', $data, true);
                $pdf_data .= str_replace('%%File_type%%', 'Original', $pdfhtml);
                $data['filename'] = 'Supplier_ledger_ajax.pdf';
                $html2pdf = new Html2Pdf('P', 'A4', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdf_data);
                //$data['filepath'] = $_SERVER['DOCUMENT_ROOT'] . 'skduml/assets/pdf/' . $data['filename'];                 //for local
                $data['filepath'] = $_SERVER['DOCUMENT_ROOT'] . 'assets/pdf/' . $data['filename'];
                $html2pdf->output($data['filepath'], 'F');
//                force_download($data['filepath'], NULL);
//                $html2pdf->output($data['filename'], 'I');
                echo base_url('assets/pdf/' . $data['filename']);
            } catch (Html2PdfException $e) {
                echo '<pre>';
                print_r($e);
                die;
//                $data = [];
//                $data['result'] = '<h1>Error In Attachment Generation, Try Again!</h1>';
            }
        }
    }
}
