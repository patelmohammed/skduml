<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Ccustomer extends MY_Controller {

    public $menu;

    function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->load->model('Customers');
        $this->load->model('Common_model');
        $this->load->model('Web_settings');
        $this->page_id = 'CUSTOMERS';
        $this->menu_id = 'CUSTOMER';
    }

    //Default loading for Customer System.
    public function index() {
        $data['menu_rights'] = $this->Common_model->get_menu_rights('CUSTOMER', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }

        $this->Common_model->check_menu_access('CUSTOMER', 'ADD', 'admin');

        $data = array(
            'stateList' => $this->Common_model->get_all_states()
        );

        $this->page_title = 'CUSTOMER';
        $view = 'admin/customer/add_customer_form';
        $this->load_admin_view($view, $data);
    }

    //customer_search_item
    public function customer_search_item() {
        $customer_id = $this->input->post('customer_id');
        $content = $this->lcustomer->customer_search_item($customer_id);
        $this->template->full_admin_html_view($content);
    }

    //credit customer_search_item
    public function credit_customer_search_item() {

        $customer_id = $this->input->post('customer_id');
        $content = $this->lcustomer->credit_customer_search_item($customer_id);
        $this->template->full_admin_html_view($content);
    }

    //paid customer_search_item
    public function paid_customer_search_item() {
        $customer_id = $this->input->post('customer_id');
        $content = $this->lcustomer->paid_customer_search_item($customer_id);
        $this->template->full_admin_html_view($content);
    }

    //Manage customer
    public function manage_customer() {
        $customer_id = $this->input->post('customer_id');

        $this->Common_model->check_menu_access('CUSTOMER', 'VIEW', 'admin');
        $data['menu_rights'] = $this->Common_model->get_menu_rights('CUSTOMER', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }

        $all_customer_list = $this->Customers->all_customer_list();
        $currency_details = $this->Web_settings->retrieve_setting_editdata();

        $data['all_customer_list'] = $all_customer_list;
        $data['links'] = '';
        $data['currency'] = $currency_details[0]['currency'];
        $data['position'] = $currency_details[0]['currency_position'];
        $data['customer_id'] = $customer_id;

        $this->page_title = 'CUSTOMER';
        $view = 'admin/customer/customer';
        $this->load_admin_view($view, $data);
    }

    public function get_customer_list($customer_id = '') {
        $menu_rights = $this->Common_model->get_menu_rights('CUSTOMER', 'admin');

        $data['groupMasterData'] = array();
        $masterData = $this->Customers->get_customer_list($this->input->post(), $customer_id);
        $cnt = 0;
        $CustomerName = $CustomerCompanyName = $CustomerAddress = $CustomerMobile = $CustomerGstNo = $CustomerEmail = $active_status = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;

            $CustomerName = '';
            $CustomerName .= '<a href="' . base_url() . 'admin/Ccustomer/customerledger/' . $val->customer_id . '">';
            $CustomerName .= $val->company__name;
            $CustomerName .= '</a>';

            $CustomerCompanyName = $val->customer_name;

            $CustomerAddress = (isset($val->customer_address) && !empty($val->customer_address) ? $val->customer_address : '') . (isset($val->customer_city) && !empty($val->customer_city) ? ', ' . $val->customer_city : '' ) . (isset($val->state_name) && !empty($val->state_name) ? ', ' . $val->state_name : '') . (isset($val->customer_country) && !empty($val->customer_country) ? ', ' . $val->customer_country : '') . (isset($val->customer_pincode) && !empty($val->customer_pincode) ? ' - ' . $val->customer_pincode : '');

            $CustomerMobile = $val->customer_mobile;
            $CustomerEmail = $val->customer_email;
            $CustomerGstNo = $val->customer_gst_no;
//            $CustomerBalance = $val->balance;

            $active_action = '';
            if ($val->active_status == 1) {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Ccustomer/deactivateCustomer/' . $val->customer_id . '\'" data-toggle="tooltip" data-placement="left" title="Active Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox" checked/>
                            <label style="border: 0px; margin: 0px;"for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> ';
            } else {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Ccustomer/activateCustomer/' . $val->customer_id . '\'" data-toggle="tooltip" data-placement="left" title="Inactive Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox"/>
                            <label style="border: 0px; margin: 0px;" for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> </a>';
            }

            $active_status = $active_action;

            $action_new = '';
            $action_new .= form_open();

            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Ccustomer/customer_update_form/' . $val->customer_id . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" title="Edit" data-toggle="tooltip" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>\'><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="deleteCustomer btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2" name="' . $val->customer_id . '" data-toggle="tooltip" title="Delete Record" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>\'><i class="fal fa-times"></i></a>';
            }

            $action_new .= form_close();

            $action = $action_new;

            $data['groupMasterData'][] = array($cnt, $CustomerName, $CustomerCompanyName, $CustomerAddress, $CustomerMobile, $CustomerEmail, $CustomerGstNo, $active_status, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Customers->get_customer_list_search_data($this->input->post(), $customer_id),
            "recordsFiltered" => $this->Customers->get_customer_list_search_data($this->input->post(), $customer_id),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    //Product Add Form
    public function credit_customer() {
        $this->load->model('Customers');
        $customer_id = $this->input->post('customer_id');
        $content = $this->lcustomer->credit_customer_list($customer_id);
        $this->template->full_admin_html_view($content);
    }

    public function get_credit_customer_list($customer_id = '') {
        $menu_rights = $this->Common_model->get_menu_rights('CREDIT_CUSTOMER');

        $data['groupMasterData'] = array();
        $masterData = $this->Customers->get_credit_customer_list($this->input->post(), $customer_id);
        $cnt = 0;
        $CustomerName = $CustomerAddress = $CustomerMobile = $Balance = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;

            $CustomerName = '';
            $CustomerName .= '<a href="' . base_url() . 'Ccustomer/customerledger/' . $val->customer_id . '">';
            $CustomerName .= $val->customer_name;
            $CustomerName .= '</a>';

            $CustomerAddress = $val->customer_address;
            $CustomerMobile = $val->customer_mobile;
            $Balance = $val->balance;

            $action_new = '';
            $action_new .= form_open();
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'Ccustomer/customer_update_form/' . $val->customer_id . '" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="deleteCustomer btn btn-danger btn-sm" name="' . $val->customer_id . '" data-toggle="tooltip" data-placement="right" title="" data-original-title="Delete" style="margin-left: 5px;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            }
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $CustomerName, $CustomerAddress, $CustomerMobile, $Balance, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Customers->get_credit_customer_list_search_data($this->input->post(), $customer_id),
            "recordsFiltered" => $this->Customers->get_credit_customer_list_search_data($this->input->post(), $customer_id),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    //Paid Customer list. The customer who will pay 100%.
    public function paid_customer() {
        $this->load->model('Customers');
        $customer_id = $this->input->post('customer_id');
        $content = $this->lcustomer->paid_customer_list($customer_id); //$links, $config["per_page"], $page);
        $this->template->full_admin_html_view($content);
    }

    public function get_paid_customer_list($customer_id = '') {
        $menu_rights = $this->Common_model->get_menu_rights('PAID_CUSTOMER');

        $data['groupMasterData'] = array();
        $masterData = $this->Customers->get_paid_customer_list($this->input->post(), $customer_id);
        $cnt = 0;
        $CustomerName = $CustomerAddress = $CustomerMobile = $Balance = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;

            $CustomerName = '';
            $CustomerName .= '<a href="' . base_url() . 'Ccustomer/customerledger/' . $val->customer_id . '">';
            $CustomerName .= $val->customer_name;
            $CustomerName .= '</a>';

            $CustomerAddress = $val->customer_address;
            $CustomerMobile = $val->customer_mobile;
            $Balance = $val->balance;

            $action_new = '';
            $action_new .= form_open();
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'Ccustomer/customer_update_form/' . $val->customer_id . '" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="deleteCustomer btn btn-danger btn-sm" name="' . $val->customer_id . '" data-toggle="tooltip" data-placement="right" title="" data-original-title="Delete" style="margin-left: 5px;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            }
            $action_new .= form_close();

            $action = $action_new;

            $active_status = '';

            $data['groupMasterData'][] = array($cnt, $CustomerName, $CustomerAddress, $CustomerMobile, $Balance, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Customers->get_paid_customer_list_search_data($this->input->post(), $customer_id),
            "recordsFiltered" => $this->Customers->get_paid_customer_list_search_data($this->input->post(), $customer_id),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    //Insert Product and upload
    public function insert_customer() {

        //Customer  basic information adding.
        $data = array(
            'customer_name' => $this->input->post('customer_name'),
            'ref_state_id' => $this->input->post('ref_state_id'),
            'company__name' => $this->input->post('company__name'),
            'customer_address' => $this->input->post('address'),
            'customer_mobile' => $this->input->post('mobile'),
            'customer_email' => $this->input->post('email'),
            'customer_city' => $this->input->post('customer_city'),
            'customer_state' => $this->input->post('customer_state'),
            'customer_country' => $this->input->post('customer_country'),
            'customer_pincode' => $this->input->post('customer_pincode'),
            'customer_gst_no' => $this->input->post('customer_gst_no'),
            'pan_number' => $this->input->post('pan_number'),
            'payment_terms' => $this->input->post('payment_terms'),
            'client_bank_name' => $this->input->post('client_bank_name'),
            'bank_ifsc_code' => $this->input->post('bank_ifsc_code'),
            'bank_account_number' => $this->input->post('bank_account_number'),
            'acoount_holder_name' => $this->input->post('acoount_holder_name'),
            'status' => 2
        );

        $customer_id = $this->Customers->customer_entry($data);

        if (!empty($customer_id) && $customer_id != '') {
            //Previous balance adding -> Sending to customer model to adjust the data.
            $this->Customers->previous_balance_add($this->input->post('previous_balance'), $customer_id);

            $this->_show_message("Information has been updated successfully!", "success");
            if (isset($_POST['add-customer'])) {
                redirect(base_url('admin/Ccustomer/manage_customer'));
                exit;
            } elseif (isset($_POST['add-customer-another'])) {
                redirect(base_url('admin/Ccustomer'));
                exit;
            }
        } else {
            $this->_show_message("already_exists", "error");
            redirect(base_url('admin/Ccustomer'));
        }
    }

    //customer Update Form
    public function customer_update_form($customer_id) {
        $data['menu_rights'] = $this->Common_model->get_menu_rights('CUSTOMER', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('CUSTOMER', 'EDIT', 'admin');

        $customer_detail = $this->Customers->retrieve_customer_editdata($customer_id);

        $data['customer_detail'] = array(
            'customer_id' => $customer_detail[0]['customer_id'],
            'ref_state_id' => $customer_detail[0]['ref_state_id'],
            'customer_name' => $customer_detail[0]['customer_name'],
            'company__name' => $customer_detail[0]['company__name'],
            'customer_address' => $customer_detail[0]['customer_address'],
            'customer_mobile' => $customer_detail[0]['customer_mobile'],
            'customer_email' => $customer_detail[0]['customer_email'],
            'customer_city' => $customer_detail[0]['customer_city'],
            'customer_state' => $customer_detail[0]['customer_state'],
            'customer_country' => $customer_detail[0]['customer_country'],
            'customer_pincode' => $customer_detail[0]['customer_pincode'],
            'customer_gst_no' => $customer_detail[0]['customer_gst_no'],
            'pan_number' => $customer_detail[0]['pan_number'],
            'payment_terms' => $customer_detail[0]['payment_terms'],
            'client_bank_name' => $customer_detail[0]['client_bank_name'],
            'bank_ifsc_code' => $customer_detail[0]['bank_ifsc_code'],
            'bank_account_number' => $customer_detail[0]['bank_account_number'],
            'acoount_holder_name' => $customer_detail[0]['acoount_holder_name'],
            'status' => $customer_detail[0]['status'],
            'stateList' => $this->Common_model->get_all_states()
        );

        $view = 'admin/customer/edit_customer_form';
        $this->page_title = 'Edit Customer';
        $this->load_admin_view($view, $data);
    }

    //Customer Ledger
    public function customer_ledger($customer_id) {
        $content = $this->lcustomer->customer_ledger_data($customer_id);
        $this->template->full_admin_html_view($content);
    }

    //Customer Final Ledger
    public function customerledger($customer_id = null) {
        $this->menu_id = 'CUSTOMER_LEDGER';
        if (empty($customer_id)) {
            $customer_id = $this->input->post('customer_id');
        }
        $from_date = $this->input->post('from_date') ? date('Y-m-d H:i:s', strtotime($this->input->post('from_date'))) : null;
        $to_date = $this->input->post('to_date') ? date('Y-m-d', strtotime($this->input->post('to_date'))) . ' ' . date('H:i:s') : null;

        $customer_detail = $this->Customers->customer_personal_data($customer_id);
        $ledger = $this->Customers->customerledger_tradational($customer_id, $from_date, $to_date);
        $summary = $this->Customers->customer_transection_summary($customer_id, $from_date, $to_date);
        $salesData = $this->Customers->saleDataByCustomerId($customer_id);
        $customer_list = $this->Common_model->getCustomerList();

        $totalSales = 0;
        $totaSalesAmt = 0;
        if (!empty($salesData)) {
            foreach ($salesData as $k => $v) {
                $totalSales = ($totalSales + $salesData[$k]->BasicTotal);
                if (!empty($salesData[$k]->GrandTotal)) {
                    $totaSalesAmt += $salesData[$k]->GrandTotal;
                }
            }
        }


        $balance = 0;
        if (!empty($ledger)) {
            foreach ($ledger as $index => $value) {
                $ledger_date = date('Y-m-d', strtotime($ledger[$index]['date']));
                $ledger[$index]['final_date'] = $ledger_date;

                if (!empty($ledger[$index]['invoice_no']) or $ledger[$index]['invoice_no'] == "NA") {
                    $ledger[$index]['credit'] = $ledger[$index]['amount'];
                    $ledger[$index]['balance'] = $balance - $ledger[$index]['amount'];
                    $ledger[$index]['debit'] = "";
                    $balance = $ledger[$index]['balance'];
                } else {
                    $ledger[$index]['debit'] = $ledger[$index]['amount'];
                    $ledger[$index]['balance'] = $balance + $ledger[$index]['amount'];
                    $ledger[$index]['credit'] = "";
                    $balance = $ledger[$index]['balance'];
                }
            }
        }

        $company_info = $this->Customers->retrieve_company();
        $code = $this->Common_model->getCompanyCode();
        $currency_details = $this->Web_settings->retrieve_setting_editdata();
        $data = array(
            'customer_list' => $customer_list,
            'customer_id' => isset($customer_id) && !empty($customer_id) ? $customer_id : null,
            'customer_name' => isset($customer_id) && !empty($customer_id) ? $customer_detail[0]['company__name'] : null,
            'customer_address' => isset($customer_id) && !empty($customer_id) ? $customer_detail[0]['customer_address'] : null,
            'customer_mobile' => isset($customer_id) && !empty($customer_id) ? $customer_detail[0]['customer_mobile'] : null,
            'customer_email' => isset($customer_id) && !empty($customer_id) ? $customer_detail[0]['customer_email'] : null,
            'ledgers' => $ledger,
            'total_credit' => number_format($summary[0][0]['total_credit'], 2, '.', ','),
            'total_debit' => number_format($summary[1][0]['total_debit'], 2, '.', ','),
            'total_balance' => number_format($summary[1][0]['total_debit'] - $summary[0][0]['total_credit'], 2, '.', ','),
            'company_info' => $company_info,
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
            'company_code' => strtoupper($code),
            'total_sales' => $totalSales,
            'salesTotalAmount' => number_format($totaSalesAmt, 2, '.', ','),
            'salesData' => $salesData,
        );

        $view = 'admin/customer/customer_ledger';
        $this->page_title = 'CUSTOMER_LEDGER';
        $this->load_admin_view($view, $data);
    }

    //Customer Previous Balance
    public function previous_balance_form() {
        $content = $this->lcustomer->previous_balance_form();
        $this->template->full_admin_html_view($content);
    }

    // customer Update
    public function customer_update() {

        $customer_id = $this->input->post('customer_id');

        $data = array(
            'customer_name' => $this->input->post('customer_name'),
            'ref_state_id' => $this->input->post('ref_state_id'),
            'company__name' => $this->input->post('company__name') ? $this->input->post('company__name') : null,
            'customer_address' => $this->input->post('address') ? $this->input->post('address') : null,
            'customer_mobile' => $this->input->post('mobile') ? $this->input->post('mobile') : null,
            'customer_email' => $this->input->post('email') ? $this->input->post('email') : null,
            'customer_city' => $this->input->post('customer_city') ? $this->input->post('customer_city') : null,
            'customer_state' => $this->input->post('customer_state') ? $this->input->post('customer_state') : null,
            'customer_country' => $this->input->post('customer_country') ? $this->input->post('customer_country') : null,
            'customer_pincode' => $this->input->post('customer_pincode') ? $this->input->post('customer_pincode') : null,
            'customer_gst_no' => $this->input->post('customer_gst_no') ? $this->input->post('customer_gst_no') : null,
            'pan_number' => $this->input->post('pan_number') ? $this->input->post('pan_number') : null,
            'payment_terms' => $this->input->post('payment_terms') ? $this->input->post('payment_terms') : null,
            'client_bank_name' => $this->input->post('client_bank_name') ? $this->input->post('client_bank_name') : null,
            'bank_ifsc_code' => $this->input->post('bank_ifsc_code') ? $this->input->post('bank_ifsc_code') : null,
            'bank_account_number' => $this->input->post('bank_account_number') ? $this->input->post('bank_account_number') : null,
            'acoount_holder_name' => $this->input->post('acoount_holder_name') ? $this->input->post('acoount_holder_name') : null,
        );

        $this->Customers->update_customer($data, $customer_id);
        $this->_show_message("Information has been updated successfully!", "success");
        redirect(base_url('admin/Ccustomer/manage_customer'));
        exit;
    }

    //customer active
    public function activateCustomer($customer_id) {
        $data = array();
        $data['active_status'] = '1';

        $this->Customers->updateStatus($data, $customer_id);
        $this->_show_message("Activated Customer", "success");

        redirect(base_url('admin/Ccustomer/manage_customer'));
    }

    //Customer Inactive
    public function deactivateCustomer($customer_id) {
        $data = array();
        $data['active_status'] = '0';

        $this->Customers->updateStatus($data, $customer_id);
        $this->_show_message("Inactivated Customer", "success");

        redirect(base_url('admin/Ccustomer/manage_customer'));
    }

    // Check Customer
    public function checkCustomer($id = '') {

        $model = $this->input->get('company__name');

        if ($id != '') {
            if ($this->Customers->checkExistingCustomer(trim($model), $id)) {
                echo 'false';
                die;
            }
        } else if ($this->Customers->checkExistingCustomer(trim($model))) {
            echo 'false';
            die;
        }
        echo 'true';
        die;
    }

    // product_delete
    public function customer_delete($id = null) {
        $chk_for_delete = 0;

        $this->Common_model->check_menu_access('CUSTOMER', 'DELETE', 'admin');
        $customer_id = $this->input->post('id');

//        $chk_for_delete += $this->Common_model->check_for_delete_master('product_sale', 'RefCustomerID', $customer_id, 1);
        $chk_for_delete += $this->Common_model->check_for_delete_master('sale_invoice', 'RefCustomerID', $customer_id, 1);

        if ($chk_for_delete == 0) {
            if ($this->Customers->delete_customer_data($customer_id)) {
                $data['result'] = true;
                $data['msg'] = "Information has been deleted successfully!";
            } else {
                $data['result'] = false;
                $data['msg'] = "Something went wrong, Try again!";
            }
        } else {
            $data['result'] = false;
            $data['msg'] = "You can't delete this customer! Because This used in sale.";
        }
        echo json_encode($data);
        die;
    }

    public function printPdfCustomerLedger() {
        $data = array();

        $customer_id = $this->input->post('customer_id');

        $start_date = $this->input->post('start_date');
        $from_date = isset($start_date) && !empty($start_date) ? date('Y-m-d H:i:s', strtotime($start_date)) : null;

        $end_date = $this->input->post('end_date');
        $to_date = isset($end_date) && !empty($end_date) ? date('Y-m-d', strtotime($end_date)) . ' ' . date('H:i:s') : null;

        $data['customer_ledger_data'] = $this->Customers->customerledger_tradational($customer_id, $from_date, $to_date);
        $data['company_detail'] = get_company_detail();
        $data['customer_detail'] = $this->Common_model->getCustomerDataByid($customer_id);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['company_code'] = $company_code;
        $data['bank_data'] = $this->Common_model->getBankDetails();

        if (!empty($data)) {
            try {
                $pdf_data = '';

                $pdfhtml = $this->load_view('admin/pdf/Customer_ledger', $data, true);
                $pdf_data .= str_replace('%%File_type%%', 'Original', $pdfhtml);
                $data['filename'] = 'Customer_ledger_ajax.pdf';
                $html2pdf = new Html2Pdf('P', 'A4', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdf_data);
//                $data['filepath'] = $_SERVER['DOCUMENT_ROOT'] . 'chem_erp/assets/pdf/' . $data['filename'];                //for local
                $data['filepath'] = $_SERVER['DOCUMENT_ROOT'] . 'assets/pdf/' . $data['filename'];
                $html2pdf->output($data['filepath'], 'F');
//                force_download($data['filepath'], NULL);
//                $html2pdf->output($data['filename'], 'I');
                echo base_url('assets/pdf/' . $data['filename']);
            } catch (Html2PdfException $e) {
//                $data = [];
//                $data['result'] = '<h1>Error In Attachment Generation, Try Again!</h1>';
            }
        }
    }
}
