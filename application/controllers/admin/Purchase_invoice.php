<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Purchase_invoice extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }

        $this->load->model('Web_settings');
        $this->load->model('Purchase_invoice_model');
        $this->load->model('Suppliers');
        $this->load->model('Categories');
        $this->load->model('Units');
        $this->load->model('Common_model');
        $this->page_id = 'PURCHASE';
        $this->menu_id = 'PURCHASE_INVOICE';
    }

    public function index() {
        $data = array();

        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }

        $this->Common_model->check_menu_access('PURCHASE_INVOICE', 'VIEW', 'admin');

        $data['title'] = 'Purchase Invoice Details';
        $data['employee_list'] = $this->Common_model->get_all_users();

        $code = $this->Common_model->getCompanyCode();
        $data['company_code'] = strtoupper($code);

        $view = 'admin/purchase_invoice/purchase_invoice_detail';
        $this->page_title = 'Purchase Invoice Details';
        $this->load_admin_view($view, $data);
    }

    public function get_purchase_invoice() {
        $menu_rights = $this->Common_model->get_menu_rights('PURCHASE_INVOICE', 'admin');

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);

        $data['groupMasterData'] = array();
        $masterData = $this->Purchase_invoice_model->get_purchase_invoice($this->input->post());
        $cnt = 0;
        $DateTime = $PINNO = $SupplierName = $InvoiceDescription = $ProductCount = $GrandTotal = $action = $CreatedBy = $UpdatedBy = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $DateTime = $val->DateTime;
            $PINNO = $val->PINNO;
            $InvoiceDescription = $val->InvoiceDescription;
            $SupplierName = $val->supplier_name;
            $ProductCount = $val->product_count;
            $GrandTotal = $val->GrandTotal;
            $CreatedBy = $val->created_by;
            $UpdatedBy = $val->updated_by;

            $action_new = '';
            $action_new .= form_open();
            $action_new .= '<div style="display:flex; gap:5px; flex-wrap:nowrap; justify-content: center;">';
            $action_new .= '<a href="' . base_url() . 'admin/Purchase_invoice/purchaseInvoiceDetails/' . $val->PINID . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-info mr-2" data-toggle="tooltip" title="View" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-info-500"></div></div>\'><i class="fal fa-eye"></i></a>';
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Purchase_invoice/purchaseInvoiceUpdateForm/' . $val->PINID . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" data-toggle="tooltip" title="Update" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>\'><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="deleteProductInvoice btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2" name="' . $val->PINID . '" data-toggle="tooltip" title="Delete" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-denger-500"></div></div>\'><i class="fal fa-times"></i></a>';
            }

            $action_new .= '<a href="' . base_url() . 'admin/Purchase_invoice/printPdfInvoice/' . $val->PINID . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-warning mr-2" name="' . $val->PINID . '" data-PINNO="' . cleanString($val->PINNO) . '" data-toggle="tooltip" title="Print" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-warning-500"></div></div>\'><i class="fal fa-barcode"></i></a>';
            $action_new .= '</div>';
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $DateTime, $PINNO, $SupplierName, $InvoiceDescription, $ProductCount, $GrandTotal, $CreatedBy, $UpdatedBy, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Purchase_invoice_model->get_purchase_invoice_search_data($this->input->post()),
            "recordsFiltered" => $this->Purchase_invoice_model->get_purchase_invoice_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    public function addPurchaseInvoice() {
        $data = array();
        $this->Common_model->check_menu_access('PURCHASE_INVOICE', 'ADD', 'admin');
        $data['title'] = 'Add Purchase Invoice';
        $data['supplier'] = $this->Suppliers->supplier_list("110", "0");
        $data['product_list'] = $this->Purchase_invoice_model->get_product_list_with_pi();
//        preprint($data['product_list']);
        $data['employee_list'] = $this->Purchase_invoice_model->get_employee_list();
        $code = $this->Common_model->getCompanyCode();
        $data['company_code'] = strtoupper($code);
        $purchase_invoice = $this->parser->parse('admin/product/add_product_form', $data, true);
//        $this->template->full_admin_html_view($purchase_invoice);


        $view = 'admin/purchase_invoice/add_purchase_invoice';
        $this->page_title = 'Add Purchase Invoice';
        $this->load_admin_view($view, $data);
    }

    public function insertPurchaseInvoice() {
        $this->Common_model->check_menu_access('PURCHASE_INVOICE', 'ADD', 'admin');

        $PINNO = $this->input->post('invoice_no');
        $inward_data['PINNO'] = $PINNO;

        $purchase_invoice_desc = $this->input->post('invoice_description');
        $inward_data['InvoiceDescription'] = isset($purchase_invoice_desc) && !empty($purchase_invoice_desc) ? $purchase_invoice_desc : null;

        $DateTime = $this->input->post('invoice_date') ? date("Y-m-d", strtotime($this->input->post('invoice_date'))) . " " . date('H:i:s') : date("Y-m-d H:i:s");
        $inward_data['DateTime'] = $DateTime;
        $inward_data['po_order_date'] = $this->input->post('order_date') ? date("Y-m-d", strtotime($this->input->post('order_date'))) : null;
        $inward_data['delivery_date'] = $this->input->post('delivery_date') ? date("Y-m-d", strtotime($this->input->post('delivery_date'))) : null;

        $RefSupplierID = $this->input->post('supplier_id');
        $inward_data['RefSupplierID'] = $RefSupplierID;
        $inward_data['RefSupplierAddress'] = $this->input->post('hidden_supplier_address');

        $unit_qua = $this->input->post('BasicTotal');
        if (isset($unit_qua) && !empty($unit_qua)) {
            $BasicTotal = 0;
            foreach ($unit_qua as $key => $total) {
                $BasicTotal += $total;
            }
            $inward_data['BasicTotal'] = $BasicTotal;
        }

        $discount_amt = $this->input->post('discount_ind');
        if (isset($discount_amt) && !empty($discount_amt)) {
            $Discount = 0;
            foreach ($discount_amt as $key => $disc) {
                $disc = isset($disc) && !empty($disc) ? $disc : 0;
                $Discount += $disc;
            }
            $inward_data['Discount'] = $Discount;
        } else {
            $inward_data['Discount'] = '';
        }

        $tax = $this->input->post('total_tax');
        if (isset($tax) && !empty($tax)) {
            $Taxtotal = 0;
            foreach ($tax as $key => $ta) {
                $Taxtotal += isset($ta) && !empty($ta) ? $ta : 0;
            }
            $inward_data['Taxtotal'] = $Taxtotal;
        } else {
            $inward_data['Taxtotal'] = '';
        }

        $inward_data['FrightCharges'] = $this->input->post('fright_charges') ? $this->input->post('fright_charges') : 0;
        $inward_data['RoundUpAmt'] = $this->input->post('round_up_amt') ? $this->input->post('round_up_amt') : 0;

        $GrandTotal = $this->input->post('g_total');
        $inward_data['GrandTotal'] = $GrandTotal;

        $inward_data['InsUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        ;
        $inward_data['InsTerminal'] = $this->input->ip_address();
        $inward_data['InsDateTime'] = date('Y-m-d H:i:s');

        $chk_uniq_invoice = true;

        if ($chk_uniq_invoice == true) {

            $PINID = $this->Common_model->insertData('purchase_invoice', $inward_data);

            $supplier_ledger['transaction_id'] = null;
            $supplier_ledger['chalan_no'] = $PINID;
            $supplier_ledger['supplier_id'] = $RefSupplierID;
            $supplier_ledger['amount'] = $GrandTotal;
            $supplier_ledger['date'] = $DateTime;
            $supplier_ledger['description'] = isset($purchase_invoice_desc) && !empty($purchase_invoice_desc) ? $purchase_invoice_desc : null;
            $supplier_ledger['status'] = 1;
            $supplier_ledger['d_c'] = 'd';
            $supplier_ledger['ref_table_name'] = 'purchase_invoice';

            $this->Common_model->insertData('supplier_ledger', $supplier_ledger);

            $product_id = $this->input->post('product_id');
            $unit_price = $this->input->post('unit_price');
            $total_amt = $this->input->post('total_amt');
            $quantity_amount = $this->input->post('quantity_amount');
            $discount = $this->input->post('discount');

            if (!empty($product_id)) {
                foreach ($product_id as $key => $value) {
                    $product_data['refPINID'] = $PINID;
                    $product_data['refProductID'] = $product_id[$key];

                    $unit_price_new = (isset($unit_price[$key]) && !empty($unit_price[$key]) ? $unit_price[$key] : 0);
                    $product_data['UnitAmt'] = $unit_price_new;

                    $quantity_amount_new = (isset($quantity_amount[$key]) && !empty($quantity_amount[$key]) ? $quantity_amount[$key] : 0);
                    $product_data['Qty'] = $quantity_amount_new;

                    $product_data['Discount'] = isset($discount[$key]) && !empty($discount[$key]) ? $discount[$key] : 0;

                    $discount_amt_new = isset($discount_amt[$key]) && !empty($discount_amt[$key]) ? $discount_amt[$key] : 0;
                    $product_data['DiscountAmt'] = $discount_amt_new;

                    $total_amt_new = isset($total_amt[$key]) && !empty($total_amt[$key]) ? $total_amt[$key] : ($unit_price_new * $quantity_amount_new - $discount_amt_new);
                    $product_data['TotalAmt'] = $total_amt_new;

                    $POItemID = $this->Common_model->insertData('purchase_invoice_item', $product_data);

                    $tax_id = $_POST['Tax_id'][$key];
                    $tax_per = $_POST['tax_per'][$key];
                    $tax_name = $_POST['tax_name'][$key];
                    $tax_amount = $_POST['single_tax_amt'][$key];
                    if (!empty($tax_id) && isset($tax_id) && $tax_id != '' && $tax_id != ' ') {
                        $tax_id_array = explode(',', $tax_id);
                        $tax_per_array = explode(',', $tax_per);
                        $tax_name_array = explode(',', $tax_name);
                        $tax_amount_array = explode(',', $tax_amount);
                        foreach ($tax_id_array as $key2 => $value2) {
                            $tax_data['refPINID'] = $PINID;
                            $tax_data['refPINItemID'] = $POItemID;
                            $tax_data['refTaxID'] = $value2;
                            $tax_data['refTaxName'] = $tax_name_array[$key2];
                            $tax_per_new = $tax_per_array[$key2];
                            $tax_data['TaxPer'] = $tax_per_new;
                            $tax_data['TaxAmt'] = $tax_amount_array[$key2];
                            $this->Common_model->insertData('purchase_invoice_itemtaxdet', $tax_data);
                        }
                    }
                }
            }
        } else {
            $this->_show_message("This Purchase Invoice already exist", "error");
            redirect(base_url('admin/Purchase_invoice'));
        }

        if (isset($POItemID) && !empty($POItemID)) {
            $this->_show_message("This Purchase Invoice Successfully Added", "success");
            if (isset($_POST['add-purchase-invoice'])) {
                redirect(base_url('admin/Purchase_invoice'));
                exit;
            } elseif (isset($_POST['add-purchase-invoice-another'])) {
                redirect(base_url('admin/Purchase_invoice/addPurchaseInvoice'));
                exit;
            }
        } else {
            $this->_show_message("This Purchase Invoice already exist", "error");
            redirect(base_url('admin/Purchase_invoice'));
        }
    }

    public function getPurchaseInwardDataByPIID() {

        $product_id = $this->input->post('product_id');
        $supplier_id = $this->input->post('supplier_id');
        $inward_id = $this->input->post('inward_id');

        $data['purchase_order_detail'] = $this->Purchase_invoice_model->getInwardDataByPIID($inward_id, $product_id, $supplier_id);
        if (isset($data['purchase_order_detail']) && !empty($data['purchase_order_detail'])) {
            $data['result'] = true;
            echo json_encode($data);
            die;
        } else {
            $data['result'] = false;
            echo json_encode($data);
            die;
        }
    }

    public function purchaseInvoiceUpdateForm($PINID) {
        $this->Common_model->check_menu_access('PURCHASE_INVOICE', 'EDIT', 'admin');

        $code = $this->Common_model->getCompanyCode();
        $data = array(
            'title' => 'Edit Purchase Invoice',
            'PINID' => $PINID,
            'product_invoice' => $this->Purchase_invoice_model->product_edit_data($PINID),
            'invoice_product_list' => $this->Purchase_invoice_model->purchase_invoice_product_details($PINID),
            'supplier' => $this->Suppliers->supplier_list(),
            'product_list' => $this->Purchase_invoice_model->get_product_list_with_pi(),
            'company_code' => strtoupper($code)
        );

        $view = 'admin/purchase_invoice/edit_purchase_invoice';
        $this->page_title = 'Edit Purchase Invoice';
        $this->load_admin_view($view, $data);
    }

    public function updatePurchaseInvoice($PINID) {

        $invoice_data = array();

        $purchase_invoice_desc = $this->input->post('invoice_description');
        $invoice_data['InvoiceDescription'] = isset($purchase_invoice_desc) && !empty($purchase_invoice_desc) ? $purchase_invoice_desc : null;

        $DateTime = $this->input->post('invoice_date') ? date("Y-m-d", strtotime($this->input->post('invoice_date'))) . " " . date('H:i:s') : date("Y-m-d H:i:s");
        $invoice_data['DateTime'] = $DateTime;

        $invoice_data['po_order_date'] = $this->input->post('order_date') ? date("Y-m-d", strtotime($this->input->post('order_date'))) : null;
        $invoice_data['delivery_date'] = $this->input->post('delivery_date') ? date("Y-m-d", strtotime($this->input->post('delivery_date'))) : null;

        $unit_qua = $this->input->post('BasicTotal');
        if (isset($unit_qua) && $unit_qua != '') {
            $BasicTotal = 0;
            foreach ($unit_qua as $key => $total) {
                $BasicTotal += $total;
            }
            $invoice_data['BasicTotal'] = $BasicTotal;
        }

        $discount_amt = $this->input->post('discount_ind');
        if (isset($discount_amt) && $discount_amt != '') {
            $Discount = 0;
            foreach ($discount_amt as $key => $disc) {
                $disc = isset($disc) ? $disc : 0;
                $Discount += $disc;
            }
            $invoice_data['Discount'] = $Discount;
        } else {
            $invoice_data['Discount'] = '';
        }

        $tax = $this->input->post('total_tax');
        if (isset($tax) && $tax != '') {
            $Taxtotal = 0;
            foreach ($tax as $key => $ta) {
                $Taxtotal += $ta;
            }
            $invoice_data['Taxtotal'] = $Taxtotal;
        } else {
            $invoice_data['Taxtotal'] = '';
        }

        $invoice_data['FrightCharges'] = $this->input->post('fright_charges') ? $this->input->post('fright_charges') : 0;
        $invoice_data['RoundUpAmt'] = $this->input->post('round_up_amt') ? $this->input->post('round_up_amt') : 0;

        $GrandTotal = $this->input->post('g_total');
        $invoice_data['GrandTotal'] = $GrandTotal;

        $invoice_data['UpdUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        ;
        $invoice_data['UpdTerminal'] = $this->input->ip_address();
        $invoice_data['UpdDateTime'] = date('Y-m-d H:i:s');

        if ($PINID != '') {

            $this->Common_model->updateData('purchase_invoice', 'PINID', $PINID, $invoice_data);

            $this->Purchase_invoice_model->clearPurchaseInvoiceItem($PINID, 'purchase_invoice_item');
            $this->Purchase_invoice_model->clearPurchaseInvoiceItem($PINID, 'purchase_invoice_itemtaxdet');

            $supplier_ledger['amount'] = $GrandTotal;
            $supplier_ledger['date'] = $DateTime;
            $supplier_ledger['description'] = isset($purchase_invoice_desc) && !empty($purchase_invoice_desc) ? $purchase_invoice_desc : null;
            $supplier_ledger['ref_table_name'] = 'purchase_invoice';
            $supplier_ledger['d_c'] = 'd';

            $this->Common_model->updateData2('supplier_ledger', 'chalan_no', $PINID, 'ref_table_name', 'purchase_invoice', $supplier_ledger);

            $product = $this->input->post('product_id');
            $result = $this->Purchase_invoice_model->insert_product($product, $PINID);
        } else {
            $this->_show_message("This Purchase Invoice already exist", "error");
        }

        if ($result == 1) {
            $this->_show_message("This Purchase Invoice Successfully Updated", "success");
            redirect(base_url('admin/Purchase_invoice'));
            exit;
        } else {
            $this->_show_message("This Purchase Invoice already exist", "error");
            redirect(base_url('admin/Purchase_invoice'));
        }
    }

    public function purchaseInvoiceDelete() {
        $this->Common_model->check_menu_access('PURCHASE_INVOICE', 'DELETE', 'admin');

        $response = [];

        $purchase_invoice_id = $this->input->post('id');

        if (isset($purchase_invoice_id) && !empty($purchase_invoice_id)) {
            if ($this->Purchase_invoice_model->delete_purchase_invoice($purchase_invoice_id)) {
                if ($this->Common_model->deleteUpdate3('supplier_ledger', 'Deleted', 'chalan_no', $purchase_invoice_id, 'ref_table_name', 'purchase_invoice')) {
                    $this->session->set_userdata(array('message' => ('successfully_delete')));
                    $response['status'] = true;
                } else {
                    $this->session->set_userdata(array('error_message' => ('something_went_wrong')));
                    $response['status'] = false;
                }
            } else {
                $this->session->set_userdata(array('error_message' => ('something_went_wrong')));
                $response['status'] = false;
            }
        } else {
            $this->session->set_userdata(array('error_message' => ('something_went_wrong')));
            $response['status'] = false;
        }

        echo json_encode($response);
        die();
    }

    public function getPurchaseInwardProductVise() {
        $product_id = $this->input->post('product_id');

        $data['product_detail'] = $this->Purchase_invoice_model->getProductInformationByProductId($product_id);

        if ($data['product_detail'] != '' && $data['product_detail'] != ' ' && isset($data['product_detail']) && !empty($data['product_detail'])) {
            $data['result'] = true;
            echo json_encode($data);
            die;
        } else {
            $data['result'] = false;
            echo json_encode($data);
            die;
        }
    }

    public function printInvoicePdf() {
        $data = array();

        $PINID = $this->input->post('product_invoice_id');
        $data['purchase_invoice_data'] = $this->Purchase_invoice_model->product_edit_data($PINID);
        $data['purchase_invoice_item_data'] = $this->Purchase_invoice_model->purchase_invoice_list_for_print($PINID);
        $data['company_detail'] = get_company_detail();
        $data['tax_detail'] = $this->Purchase_invoice_model->invoice_tax_detail_for_print($PINID);
        $data['supplier_detail'] = $this->Common_model->getSupplierDataByid($data['purchase_invoice_data']['RefSupplierID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['company_code'] = $company_code;
        $data['CurrentUser'] = $this->db->where("user_id", $data['purchase_invoice_data']['InsUser'])->get('users')->row_array();
        $data['bank_data'] = $this->Common_model->getBankDetails();

        $content = $this->parser->parse('purchase_invoice/ajax_pdf', $data, true);
        echo $content;
        die;
    }

    public function printPdfInvoice($PINID) {
        $data['purchase_invoice_data'] = $this->Purchase_invoice_model->product_edit_data($PINID);
        $data['purchase_invoice_item_data'] = $this->Purchase_invoice_model->purchase_invoice_list_for_print($PINID);
        $data['company_detail'] = get_company_detail();
        $data['tax_detail'] = $this->Purchase_invoice_model->invoice_tax_detail_for_print($PINID);
        $data['supplier_detail'] = $this->Common_model->getSupplierDataByid($data['purchase_invoice_data']['RefSupplierID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['company_code'] = $company_code;
        $data['CurrentUser'] = $this->db->where("id", $data['purchase_invoice_data']['InsUser'])->get('user_information')->row_array();
        $data['bank_data'] = $this->Common_model->getBankDetails();

        if (!empty($data)) {
            try {
                $pdf_data = '';
                $pdfhtml = $this->load->view('admin/pdf/Purchase_invoice', $data, true);
                $pdf_data .= str_replace('%%File_type%%', 'ORIGINAL', $pdfhtml);
                $pdf_data .= str_replace('%%File_type%%', 'DUPLICATE', $pdfhtml);
                $pdfName = cleanString($data['purchase_invoice_data']['PINNO']) . '.pdf';
                $html2pdf = new Html2Pdf('L', 'A5', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdf_data);
                $html2pdf->output($pdfName, 'I');
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
    }

    public function mailPurchaseInvoiceDetail() {
        $data = $attachment = $mail_data = array();
        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $PINID = $this->input->post('pin_id');
        $mail_to = $this->input->post('mail_to');
        $mail_subject = $this->input->post('mail_subject');
        $mail_body = $this->input->post('mail_body');
        $send_me = $this->input->post('send_me');

        $data['subject'] = !empty($mail_subject) ? $mail_subject : 'Purchase Invoice Detail';
        $data['body'] = !empty($mail_body) ? $mail_body : 'Purchase Invoice Data';

        $mail_to_new = implode(', ', $mail_to);
        $mail_data['email_id'] = $mail_to_new;
        if ($send_me == 'true') {
            $mail_to_new .= ', ' . $this->session->userdata('user_email');
            $mail_data['send_me'] = $this->session->userdata('user_email');
        }

        $data['purchase_invoice'] = $this->Purchase_invoice_model->product_edit_data($PINID);
        $data['purchase_invoice_list'] = $this->Purchase_invoice_model->purchase_invoice_list_for_print($PINID);
        $data['company_detail'] = get_company_detail();
        $data['tax_detail'] = $this->Purchase_invoice_model->invoice_tax_detail_for_print($PINID);
        $data['supplier_detail'] = $this->Common_model->getSupplierDataByid($data['purchase_invoice']['RefSupplierID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['purchase_invoice']['PINNO'] = 'PIN-' . $company_code . "-" . $data['purchase_invoice']['PINNO'];
        $data['company_code'] = $company_code;

        if (!empty($data)) {
            try {
                $new_path = 'my-assets/documents/pdf/PurchaseInvoice/';
                if (!is_dir($new_path)) {
                    if (!mkdir($new_path, 0777, true)) {
                        die('Not Created');
                    }
                }
                $pdfhtml = $this->parser->parse('pdf/Purchase_invoice', $data, true);
                $pdfName = $new_path . cleanString($data['purchase_invoice']['PINNO']) . '_' . date('H_i_s') . '.pdf';
                $attachment = array(getcwd() . '/' . $pdfName);
                $html2pdf = new Html2Pdf('P', 'A4', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdfhtml);
                $html2pdf->output(getcwd() . '/' . $pdfName, 'F');
                $attachment[] = getcwd() . '/' . $pdfName;
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }

        $data['login_url'] = 'google.com';
        $data['company'] = get_company_detail();
        $company_name = $data['company']['company_name'];

        if (!empty($data)) {
            try {
                $from_email = "inquiry@foodmohalla.in";
                $this->email->from($from_email, $company_name);
                $this->email->subject($data['subject']);
                $this->email->message($data['body']);
                $this->email->attach($pdfName);
                if (!empty($mail_to_new)) {
                    $this->email->to($mail_to_new);
                    if ($this->email->send()) {
                        $mail_data['refTableName'] = 'purchase_invoice';
                        $mail_data['refTableId'] = $PINID;
                        $mail_data['attachments'] = $pdfName;
                        $mail_data['mailSubject'] = $data['subject'];
                        $mail_data['mailBody'] = $data['body'];
                        $mail_data['InsUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
                        ;
                        $mail_data['InsTerminal'] = $this->input->ip_address();
                        $mail_data['InsDateTime'] = date('Y/m/d H:i:s');
                        $this->Common_model->insertData('mailhistory', $mail_data);

                        $data['result'] = true;
                    }
                }
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
        echo json_encode($data);
        die;
    }

    public function purchaseInvoiceDetails($PINID) {

        $this->Common_model->check_menu_access('PURCHASE_INVOICE', 'VIEW', 'admin');

        $purchase_invoice_data = $this->Purchase_invoice_model->product_edit_data($PINID);
        $purchase_invoice_item_data = $this->Purchase_invoice_model->purchase_invoice_list_for_print($PINID);

        $subtotal = $purchase_invoice_data['BasicTotal'] - $purchase_invoice_data['Discount'] + $purchase_invoice_data['Taxtotal'];

        $currency_details = $this->Web_settings->retrieve_setting_editdata();
        $company_info = get_company_detail();
        $code = $this->Common_model->getCompanyCode();

        $data = array(
            'title' => 'Sale Invoice Details',
            'purchase_invoive_id' => $PINID,
            'purchase_invoice_data' => $purchase_invoice_data,
            'purchase_invoice_item_data' => $purchase_invoice_item_data,
            'subtotal' => $subtotal,
            'company_info' => $company_info,
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
            'discount_type' => $currency_details[0]['discount_type'],
            'company_code' => strtoupper($code),
        );

        $view = 'admin/purchase_invoice/invoice_detail';
        $this->page_title = 'View Purchase Invoice';
        $this->load_admin_view($view, $data);
    }
}
