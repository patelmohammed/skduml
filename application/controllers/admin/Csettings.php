<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Csettings extends MY_Controller {

    function __construct() {
        parent::__construct();
//        $this->load->library('Settings');
//        $this->load->library('auth');
//        $this->load->library('session');
//        $this->load->model('Settings');
        $this->load->model('Settings');
        $this->load->model('Products');
        $this->load->model('Common_model');
//        $this->auth->check_admin_auth();
        $this->load->model('Web_settings');
    }

    public function index() {

        $data['menu_rights'] = $this->Common_model->get_menu_rights('BANK', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }

        $this->Common_model->check_menu_access('BANK', 'ADD', 'admin');
        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'BANK';

//        $data = array('title' => 'add_new_bank');
//        $content = $this->parser->parse('settings/new_bank', $data, true);
//        $this->template->full_admin_html_view($content);
        $view = 'admin/settings/new_bank';
        $this->page_title = 'new_bank';
        $this->load_admin_view($view, $data);
    }

    #================Add new bank==============#

    public function add_new_bank() {


        if ($_FILES['signature_pic']['name']) {

            $config['upload_path'] = './assets/admin/img/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
            $config['max_size'] = "*";
            $config['max_width'] = "*";
            $config['max_height'] = "*";
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('signature_pic')) {
                $this->_show_message($this->upload->display_errors(), "error");
                redirect(base_url('admin/Csettings/bank_list'));
            } else {
                $image = $this->upload->data();
                $signature_pic = base_url() . "assets/admin/img/" . $image['file_name'];
            }
        }


        $data = array(
            'bank_name' => $this->input->post('bank_name'),
            'ac_name' => $this->input->post('ac_name'),
            'ac_number' => $this->input->post('ac_no'),
            'branch' => $this->input->post('branch'),
            'IFSC_code' => $this->input->post('IFSC_code'),
            'signature_pic' => (!empty($signature_pic) ? $signature_pic : null),
            'status' => 1
        );

        $this->Settings->bank_entry($data);
        $this->_show_message("Information has been added successfully!", "success");
        redirect(base_url('admin/Csettings/bank_list'));
        exit;
    }

    public function bank_transaction() {

        $this->Common_model->check_menu_access('BANK', 'ADD', 'admin');
        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'BANK';

        $bank_list = $this->Settings->get_bank_list();

        $data = array(
            'title' => 'Add Bank Transaction',
            'bank_list' => $bank_list,
        );
        $view = 'admin/settings/bank_debit_credit_manage';
        $this->page_title = 'Bank Transaction';
        $this->load_admin_view($view, $data);
    }

    public function manage_bank_transaction($bank_id = null) {


        $menu_rights = $this->Common_model->get_menu_rights('BANK', 'admin');
        if (empty($menu_rights)) {
            redirect('admin/Auth/Unauthorized');
        }

        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'BANK';

        $total_debit = 0;
        $total_credit = 0;

        $bank_info = $this->Settings->get_bank_by_id($bank_id);
        $transactions = $this->Settings->get_bank_transactions($bank_id);

        if (isset($transactions) && !empty($transactions)) {
            foreach ($transactions as $key => $value) {
                $total_debit += (isset($value->dr) && !empty($value->dr) ? $value->dr : 0);
                $total_credit += (isset($value->cr) && !empty($value->cr) ? $value->cr : 0);
            }
        }

        $available_balance = $total_credit - $total_debit;

        $currency_details = $this->Web_settings->retrieve_setting_editdata();
        $this->Common_model->check_menu_access('BANK', 'VIEW', 'admin');
        $data = array(
            'title' => 'Bank Transactions',
            'bank_id' => $bank_id,
            'bank_info' => $bank_info,
            'available_balance' => $available_balance,
            'transactions' => $transactions,
            'menu_rights' => $menu_rights,
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position']
        );

        $view = 'admin/settings/manage_bank_transaction';
        $this->page_title = 'Bank Transaction';
        $this->load_admin_view($view, $data);
    }

    public function get_transaction_list($bank_id = null) {
        $menu_rights = $this->Common_model->get_menu_rights('BANK', 'admin');

        $data['groupMasterData'] = array();
        $masterData = $this->Settings->get_transaction_list($this->input->post(), $bank_id);
        $cnt = 0;
        $dr = 0;
        $cr = 0;
        $total = 0;
        $balance = 0;
        $Date = $BankName = $AcName = $DepositeId = $description = $debit_amt = $credit_amt = $balance_amt = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $Date = date('d-m-Y', strtotime($val->date));
            $BankName = $val->bank_name;
            $AcName = $val->ac_name;

            $DepositeId = $val->deposite_id;
            $description = $val->description;

            $dr = (isset($val->dr) && !empty($val->dr) ? $val->dr : 0);
            $cr = (isset($val->cr) && !empty($val->cr) ? $val->cr : 0);
            $total = $cr - $dr;
            $balance += $total;

            $debit_amt = (isset($dr) && !empty($dr) && $dr != 0 ? '<div class="text-danger">&#8377; ' . number_format($dr, 2) . '</div>' : null);
            $credit_amt = (isset($cr) && !empty($cr) && $cr != 0 ? '<div class="text-success">&#8377; ' . number_format($cr, 2) . '</div>' : null);
            $balance_amt = (isset($balance) && !empty($balance) ? '<div class="text-primary">&#8377; ' . number_format($balance, 2) . '</div>' : 0);

            $action_new = '';
//            $action_new .= form_open();
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Csettings/update_bank_transaction_form/' . $val->id . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" data-toggle="tooltip" title="" data-original-title="Update" data-template="<div class=\'tooltip\' role=\'tooltip\'><div class=\'tooltip-inner bg-primary-500\'></div></div>"><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="DeleteTransaction btn btn-icon hover-effect-dot btn-outline-danger btn-sm" name="' . $val->id . '" data-toggle="tooltip" title="" data-original-title="Delete" data-template="<div class=\'tooltip\' role=\'tooltip\'><div class=\'tooltip-inner bg-danger-500\'></div></div>"><i class="fal fa-times"></i></a>';
            }
//            $action_new .= form_close();

            $action = $action_new;

            $org_array = array($cnt, $Date, $DepositeId, $description, $credit_amt, $debit_amt, $balance_amt, $action);

            if (empty($bank_id)) {
                $ins_array = array($BankName, $AcName);
                array_splice($org_array, 1, 0, $ins_array);
                foreach (array_keys($org_array, $balance_amt, true) as $key) {
                    array_splice($org_array, $key, 1);
                }
            }

            $data['groupMasterData'][] = $org_array;
        }
//        preprint($data);

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Settings->get_transaction_list_search_data($this->input->post(), $bank_id),
            "recordsFiltered" => $this->Settings->get_transaction_list_search_data($this->input->post(), $bank_id),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    public function delete_bank_transaction() {

        $this->Common_model->check_menu_access('BANK', 'DELETE', 'admin');

        $id = $this->input->post('transaction_id');
        if ($this->Settings->delete_transaction($id)) {
            $this->session->set_userdata(array('message' => display('successfully_delete')));
        } else {
            $this->session->set_userdata(array('error' => 'Something went wrong, Please Contact Administrator'));
        }
        return true;
    }

    public function update_bank_transaction_form($id) {
        $this->page_title = 'Edit Bank Transaction';

        $this->Common_model->check_menu_access('BANK', 'EDIT', 'admin');

        $transaction_data = $this->Settings->retrieve_transaction_editdata($id);
        $bank_list = $this->Settings->get_bank_list();

        $data = array(
            'title' => 'Bank Transaction Edit',
            'bank_list' => $bank_list,
            'transaction' => $transaction_data,
            'id' => $id
        );

        $view = 'admin/settings/edit_bank_transaction';
        $this->load_admin_view($view, $data);
    }

    public function update_bank_transaction($id) {

        if ($this->input->post('account_type') == 1) {
            $cr = $this->input->post('ammount');
        } else {
            $dr = $this->input->post('ammount');
        }

        $data = array(
            'date' => date('Y-m-d H:i:s', strtotime($this->input->post('date'))),
            'ac_type' => $this->input->post('account_type'),
            'bank_id' => $this->input->post('bank_id'),
            'description' => $this->input->post('description'),
            'deposite_id' => $this->input->post('withdraw_deposite_id'),
            'dr' => (isset($dr) && !empty($dr) ? $dr : null),
            'cr' => (isset($cr) && !empty($cr) ? $cr : null),
            'amount' => $this->input->post('ammount'),
            'UpdUser' => $this->session->userdata(SITE_NAME . '_admin')['user_id'],
            'UpdTerminal' => $this->input->ip_address(),
            'UpdDateTime' => date('Y-m-d H:i:s'),
            'status' => 1
        );

//        preprint($data);

        if ($this->Settings->update_bank_transaction($data, $id)) {
            $this->session->set_userdata(array('message' => "Banl transaction updated successfully."));
            redirect(base_url('admin/Csettings/manage_bank_transaction'));
        } else {
            $this->session->set_userdata(array('error' => 'Something went wrong, Try again!'));
            redirect(base_url('admin/Csettings/manage_bank_transaction'));
        }
    }

    public function bank_debit_credit_manage() {
        $bank_list = $this->Settings->get_bank_list();
        $data = array(
            'title' => display('bank_transaction'),
            'bank_list' => $bank_list,
        );
        $content = $this->parser->parse('settings/bank_debit_credit_manage', $data, true);
        $this->template->full_admin_html_view($content);
    }

    #===========Bank Debit Credit Manage==========#

    public function bank_debit_credit_manage_add() {

        if ($this->input->post('account_type') == 1) {
            $cr = $this->input->post('ammount');
        } else {
            $dr = $this->input->post('ammount');
        }

        $data = array(
            'date' => date('Y-m-d H:i:s', strtotime($this->input->post('date'))),
            'ac_type' => $this->input->post('account_type'),
            'bank_id' => $this->input->post('bank_id'),
            'description' => $this->input->post('description'),
            'deposite_id' => $this->input->post('withdraw_deposite_id'),
            'dr' => (isset($dr) && !empty($dr) ? $dr : null),
            'cr' => (isset($cr) && !empty($cr) ? $cr : null),
            'amount' => $this->input->post('ammount'),
            'InsUser' => $this->session->userdata(SITE_NAME . '_admin')['user_id'],
            'InsTerminal' => $this->input->ip_address(),
            'InsDateTime' => date('Y-m-d H:i:s'),
            'status' => 1
        );
        if ($this->Settings->bank_debit_credit_manage_entry($data)) {
            $this->session->set_userdata(array('message' => 'Bank transcation added successfully.'));
            redirect(base_url('admin/Csettings/manage_bank_transaction'));
        } else {
            $this->session->set_userdata(array('error' => 'Something went wrong, Contact Administrator'));
            redirect(base_url('admin/Csettings/manage_bank_transaction'));
        }
    }

    #==============Bank Ledger============#

    public function bank_ledger($bank_id) {
        $content = $this->Settings->bank_ledger($bank_id);
        $this->template->full_admin_html_view($content);
    }

    #================Add Person==============#

    public function add_person() {
        $content = $this->Settings->add_person();
        $this->template->full_admin_html_view($content);
    }

    #================Submit Person==============#

    public function submit_person() {
        $data = array(
            'person_id' => $this->auth->generator(10, 'pesonal_loan_information', 'person_id'),
            'person_name' => $this->input->post('name'),
            'person_phone' => $this->input->post('phone'),
            'person_address' => $this->input->post('address'),
            'status' => 1
        );
        $result = $this->Settings->submit_person_personal_loan($data);
        if ($result) {
            $this->session->set_userdata(array('message' => display('successfully_added')));
            redirect(base_url('Csettings/manage_person'));
        } else {
            $this->session->set_userdata(array('error_message' => display('not_added')));
            redirect(base_url('Csettings/manage_person'));
        }
    }

    //Phone search by name
    public function phone_search_by_name() {
        $person_id = $this->input->post('person_id');
        $result = $this->db->select('person_phone')
                ->from('person_information')
                ->where('person_id', $person_id)
                ->get()
                ->row();
        if ($result) {
            echo $result->person_phone;
        } else {
            return false;
        }
    }

    //Person loan search by phone number
    public function loan_phone_search_by_name() {
        $person_id = $this->input->post('person_id');
        $result = $this->db->select('person_phone')
                ->from('pesonal_loan_information')
                ->where('person_id', $person_id)
                ->get()
                ->row();
        if ($result) {
            echo $result->person_phone;
        } else {
            return false;
        }
    }

    #================Add loan==============#

    public function add_loan() {
        $content = $this->Settings->add_loan();
        $this->template->full_admin_html_view($content);
    }

    #================Submit loan==============#

    public function submit_loan() {
        $data = array(
            'transaction_id' => $this->auth->generator(10, 'personal_loan', 'transaction_id'),
            'person_id' => $this->input->post('person_id'),
            'credit' => $this->input->post('ammount'),
            'date' => dbdate($this->input->post('date')),
            'details' => $this->input->post('details'),
            'status' => 1
        );
        $result = $this->Settings->submit_loan_personal($data);
        if ($result) {
            $this->session->set_userdata(array('message' => display('successfully_added')));
            redirect(base_url('Csettings/manage_person'));
        } else {
            $this->session->set_userdata(array('error_message' => display('not_added')));
            redirect(base_url('Csettings/add_loan'));
        }
    }

    #================Add payment==============#

    public function add_payment() {
        $content = $this->Settings->add_payment();
        $this->template->full_admin_html_view($content);
    }

    #================Submit loan==============#

    public function submit_payment() {
        $data = array(
            'transaction_id' => $this->auth->generator(10, 'personal_loan', 'transaction_id'),
            'person_id' => $this->input->post('person_id'),
            'debit' => $this->input->post('ammount'),
            'date' => dbdate($this->input->post('date')),
            'details' => $this->input->post('details'),
            'status' => 2
        );
        $result = $this->Settings->submit_payment_per_loan($data);
        if ($result) {
            $this->session->set_userdata(array('message' => display('successfully_added')));
            redirect(base_url('Csettings/manage_person'));
        } else {
            $this->session->set_userdata(array('error_message' => display('not_added')));
            redirect(base_url('Csettings/add_payment'));
        }
    }

    #================Manage Person==============#

    public function manage_person() {
        #
        #pagination starts
        #
        $config["base_url"] = base_url('Csettings/manage_person/');
        $config["total_rows"] = $this->Settings->person_list_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config["num_links"] = 5;
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #  
        $content = $this->Settings->manage_person_loan_person($links, $config["per_page"], $page);
        $this->template->full_admin_html_view($content);
    }

    #############===manage loan form ==#####################

    public function manage_loan() {
        #
        #pagination starts
        #
        $config["base_url"] = base_url('Csettings/manage_loan/');
        $config["total_rows"] = $this->Settings->person_loan_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config["num_links"] = 5;
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #  
        $content = $this->Settings->manage_loan($links, $config["per_page"], $page);
        $this->template->full_admin_html_view($content);
    }

    #================Edit Person==============#

    public function person_edit($person_id) {
        $content = $this->Settings->edit_person($person_id);
        $this->template->full_admin_html_view($content);
    }

    ########===========person loan edit data =========####

    public function person_loan_edit($person_id) {
        $content = $this->Settings->edit_person_loan($person_id);
        $this->template->full_admin_html_view($content);
    }

    ### Personal loan update ============================#####

    public function loan_edit($person_id) {
        $content = $this->Settings->edit_loan($person_id);
        $this->template->full_admin_html_view($content);
    }

    #================update Person==============#update_loan_person

    public function update_person($person_id) {
        $data = array(
            'person_name' => $this->input->post('name'),
            'person_phone' => $this->input->post('phone'),
            'person_address' => $this->input->post('address'),
            'status' => 1
        );
        $result = $this->Settings->update_person($data, $person_id);
        if ($result) {
            $this->session->set_userdata(array('message' => display('successfully_updated')));
            redirect(base_url('Cloan/manage1_person'));
        } else {
            $this->session->set_userdata(array('error_message' => display('not_added')));
            redirect(base_url('Cloan/manage1_person'));
        }
    }

    //############## update loan date##############
    public function update_loan($person_id) {
        $data = array(
            'per_loan_id' => $this->input->post('per_loan_id'),
            'person_id' => $this->input->post('person_id'),
            'date' => $this->input->post('date'),
            'debit' => $this->input->post('debit'),
            'credit' => $this->input->post('credit'),
            'details' => $this->input->post('details'),
        );
        $result = $this->Settings->update_per_loan($data, $person_id);
        if ($result) {
            $this->session->set_userdata(array('message' => display('successfully_updated')));
            redirect(base_url('Csettings/manage_loan'));
        } else {
            $this->session->set_userdata(array('error_message' => display('not_added')));
            redirect(base_url('Csettings/manage_loan'));
        }
    }

    // Update person loan
    public function update_loan_person($person_id) {
        $data = array(
            'person_name' => $this->input->post('name'),
            'person_phone' => $this->input->post('phone'),
            'person_address' => $this->input->post('address'),
            'status' => 1
        );
        $result = $this->Settings->update_loan_person($data, $person_id);
        if ($result) {
            $this->session->set_userdata(array('message' => display('successfully_updated')));
            redirect(base_url('Csettings/manage_person'));
        } else {
            $this->session->set_userdata(array('error_message' => display('not_added')));
            redirect(base_url('Csettings/manage_person'));
        }
    }

    //Person ledger 
    public function person_ledger($person_id) {
        $content = $this->Settings->person_ledger_data($person_id);
        $this->template->full_admin_html_view($content);
    }

    // Persona loan details
    public function person_loan_deails($person_id) {
        $content = $this->Settings->person_loan_data($person_id);
        $this->template->full_admin_html_view($content);
    }

    //Ledger search
    public function ledger_search() {
        $CI = & get_instance();
        $this->auth->check_admin_auth();
        $CI->load->library('lreport');
        $CI->load->model('Reports');
        $today = date('Y-m-d');

        $person_id = $this->input->post('person_id') ? $this->input->post('person_id') : "";
        $from_date = dbdate($this->input->post('from_date'));

        $to_date = dbdate($this->input->post('to_date')) ? dbdate($this->input->post('to_date')) : $today;

        #
        #pagination starts
        #
        $config["base_url"] = base_url('Csettings/ledger_search/');
        $config["total_rows"] = $this->Settings->ledger_search_by_date_count($person_id, $from_date, $to_date);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config["num_links"] = 5;
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        # 

        $content = $this->Settings->ledger_search_by_date($person_id, $from_date, $to_date, $links, $config["per_page"], $page);

        $this->template->full_admin_html_view($content);
    }

    //person_loan_search 
    public function person_loan_search() {
        $CI = & get_instance();
        $this->auth->check_admin_auth();
        $CI->load->library('lreport');
        $CI->load->model('Reports');
        $today = date('Y-m-d');

        $person_id = $this->input->post('person_id') ? $this->input->post('person_id') : "";
        $from_date = dbdate($this->input->post('from_date'));

        $to_date = dbdate($this->input->post('to_date')) ? dbdate($this->input->post('to_date')) : $today;

        #
        #pagination starts
        #
        $config["base_url"] = base_url('Csettings/ledger_search/');
        $config["total_rows"] = $this->Settings->person_loan_search_by_date_count($person_id, $from_date, $to_date);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config["num_links"] = 5;
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        # 

        $content = $this->Settings->person_loan_search_by_date($person_id, $from_date, $to_date, $links, $config["per_page"], $page);

        $this->template->full_admin_html_view($content);
    }

    #==============Bank list============#

    public function bank_list() {
//          $this->Settings->bank_list();
//        $data['menu_rights'] = $this->Common_model->get_menu_rights('SUPPLIER', 'admin');
//        if (empty($data['menu_rights'])) {
//            redirect('admin/Auth/Unauthorized');
//        }
//        $this->Common_model->check_menu_access('SUPPLIER', 'ADD', 'admin');


        $this->Common_model->check_menu_access('BANK', 'VIEW', 'admin');
        $rights['menu_rights'] = $this->Common_model->get_menu_rights('BANK', 'admin');
        if (empty($rights['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'BANK';

        $currency_details = $this->Web_settings->retrieve_setting_editdata();
        $data = array(
            'title' => ('bank_list'),
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
            'menu_rights' => $rights['menu_rights'],
        );

        $view = 'admin/settings/bank';
        $this->page_title = 'Bank';
        $this->load_admin_view($view, $data);
//        $this->template->full_admin_html_view($content);
    }

    public function get_banks_lists() {

        $menu_rights = $this->Common_model->get_menu_rights('BANK', 'admin');

        $data['groupMasterData'] = array();
        $masterData = $this->Settings->get_banks_lists($this->input->post());
//       

        $cnt = 0;
        $BankName = $AcName = $AcNumber = $Branch = $IFSCCode = $Balance = $SignaturePic = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;

            $BankName = '';
            $BankName .= '<a href="' . base_url("admin/Csettings/manage_bank_transaction/" . $val->bank_id) . '" >';
            $BankName .= $val->bank_name;
            $BankName .= '</a>';

            $AcName = $val->ac_name;
            $AcNumber = $val->ac_number;
            $Branch = $val->branch;
            $IFSCCode = $val->IFSC_code;

            if (isset($val->Balance) && !empty($val->Balance)) {
                if ($val->Balance > 0) {
                    $Balance = '<div class="text-success fw-500">&#8377; ' . number_format($val->Balance, 2) . '</div>';
                } elseif ($val->Balance < 0) {
                    $Balance = '<div class="text-danger fw-500">&#8377; ' . number_format($val->Balance, 2) . '</div>';
                } else {
                    $Balance = '<div class="fw-500">&#8377; ' . number_format($val->Balance, 2) . '</div>';
                }
            } else {
                $Balance = '<div class="fw-500">&#8377; ' . number_format(0, 2) . '</div>';
            }

            $SignaturePic = '';

            if (isset($val->signature_pic) && !empty($val->signature_pic)) {
                $SignaturePic .= '<img src="' . $val->signature_pic . '" class="img img-responsive center-block" height="80" width="100">';
            }

            $active_action = '';
            if ($val->active_status == 1) {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Csettings/deactivateBank/' . $val->bank_id . '\'" data-toggle="tooltip" data-placement="left" title="Active Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox" checked/>
                            <label style="border: 0px; margin: 0px;"for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> ';
            } else {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Csettings/activateBank/' . $val->bank_id . '\'" data-toggle="tooltip" data-placement="left" title="Inactive Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox"/>
                            <label style="border: 0px; margin: 0px;" for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> </a>';
            }

            $active_status = $active_action;

            $action_new = '';
            $action_new .= form_open();
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Csettings/edit_bank/' . $val->bank_id . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary" data-toggle="tooltip" title="" data-original-title="Update" data-template="<div class=\'tooltip\' role=\'tooltip\'><div class=\'tooltip-inner bg-primary-500\'></div></div>"><i class="fal fa-edit" aria-hidden="true"></i></a>';
            }
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $BankName, $AcName, $AcNumber, $Branch, $IFSCCode, $Balance, $SignaturePic, $active_status, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Settings->get_banks_lists_search_data($this->input->post()),
            "recordsFiltered" => $this->Settings->get_banks_lists_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    #=============Bank edit==============#

    public function edit_bank($bank_id) {

//        $content = $this->Settings->bank_show_by_id($bank_id);
//        $this->template->full_admin_html_view($content);


        $this->Common_model->check_menu_access('BANK', 'EDIT', 'admin');

        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'BANK';
        $bank_list = $this->Settings->bank_info($bank_id);
        $data = array(
            'title' => ('bank_edit'),
            'bank_list' => $bank_list
        );
//        $bankList = $this->parser->parse('settings/edit_bank', $data, true);
//        return $bankList;

        $view = 'admin/settings/edit_bank';
        $this->page_title = 'Bank';
        $this->load_admin_view($view, $data);
    }

    //customer active
    public function activateBank($bank_id) {
        $data = array();
        $data['active_status'] = '1';
        $this->Settings->updateStatus($data, $bank_id);
//        $this->session->set_userdata(array('message' => ('active_message')));
        $this->_show_message("Information has been activated!", "success");
        redirect('admin/Csettings/bank_list');
    }

    //Customer Inactive
    public function deactivateBank($bank_id) {
        $data = array();
        $data['active_status'] = '0';
        $this->Settings->updateStatus($data, $bank_id);
        $this->_show_message("Information has been inactivated!", "success");
        redirect('admin/Csettings/bank_list');
    }

    #============Update Bank=============#

    public function update_bank($bank_id) {
        $content = $this->Settings->bank_update_by_id($bank_id);
        $this->session->set_userdata(array('message' => display('successfully_updated')));
        redirect('Csettings/bank_list');
    }

    #==============Table list============#

    public function table_list() {
        #
        #pagination starts
        #
        $config["base_url"] = base_url('Csettings/table_list/');
        $config["total_rows"] = $this->Settings->table_list_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config["num_links"] = 5;
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #  
        $content = $this->Settings->table_list($links, $config["per_page"], $page);

        $this->template->full_admin_html_view($content);
    }

    #=============Commission==============#

    public function commission($type = '') {
        $CI = & get_instance();

        $rights['menu_rights'] = $CI->Common_model->get_menu_rights('GENERATE_COMMISSION');
        if (empty($rights['menu_rights'])) {
            redirect('Unauthorized');
        }

        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'GENERATE_COMMISSION';

        $agent_info = $this->Common_model->getAgentList();
        $currency_details = $this->Web_settings->retrieve_setting_editdata();

        $data = array(
            'title' => 'Generate Commission',
            'agent_info' => $agent_info,
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
            'menu_rights' => $rights['menu_rights'],
            'type' => $type,
        );

        $content = $this->parser->parse('commission/commission_generate', $data, true);
        $this->template->full_admin_html_view($content);
    }

    public function get_commission_list($type = '') {
        $menu_rights = $this->Common_model->get_menu_rights('GENERATE_COMMISSION');

        $data['groupMasterData'] = array();
        $masterData = $this->Settings->get_commission_list($this->input->post(), $type);
        $cnt = 0;
        $agent_name = $DateTime = $SINO = $Qty = $commission_type = $rate_type = $CommissionRate = $TotalCommission = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $agent_name = $val->agent_name;
            $DateTime = (isset($val->DateTime) && !empty($val->DateTime) ? date('d-M-Y', strtotime($val->DateTime)) : '');
            $SINO = '<a href="' . base_url() . 'Sale_invoice/saleInvoiceDetails/' . $val->SIID . '">' . $val->SINO . '</a>';
            $commission_type = $val->commission_name;
            $rate_type = (isset($val->RefCommissionRateType) && !empty($val->RefCommissionRateType) ? ($val->RefCommissionRateType == 1 ? 'FIX PER KG' : 'PERCENTAGE') : '');
            $Qty = $val->total_qty;
            $CommissionRate = $val->RefCommissionRate;
            $TotalCommission = $val->CommissionTotal;

            $data['groupMasterData'][] = array($cnt, $agent_name, $DateTime, $SINO, $Qty, $commission_type, $rate_type, $CommissionRate, $TotalCommission);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Settings->get_commission_list_search_data($this->input->post(), $type),
            "recordsFiltered" => $this->Settings->get_commission_list_search_data($this->input->post(), $type),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    #==============Retrive commission=========#

    public function retrive_product_info() {
        $customer_id = $this->input->post('customer_id');
        $product_info = $this->db->select('
								invoice_details.*,
								product_information.product_model
								')
                ->from('invoice')
                ->join('invoice_details', 'invoice_details.invoice_id = invoice.invoice_id')
                ->join('product_information', 'invoice_details.product_id = product_information.product_id')
                ->where('invoice.customer_id', $customer_id)
                ->group_by('invoice_details.product_id')
                ->get()
                ->result();

        if ($product_info) {
            echo "<select class=\"form-control\" name=\"product_model\" id=\"product_model\">";
            echo "<option>" . display('select_one') . "</option>";
            foreach ($product_info as $product) {
                echo "<option value='" . $product->product_id . "'>" . $product->product_model . "</option>";
            }
            echo "</select>";
        }
    }

    public function checkBank($id = '') {

        $model = $this->input->get('bank_name');

        if ($id != '') {
            if ($this->Settings->checkExistingBank(trim($model), $id)) {
                echo 'false';
                die;
            }
        } else if ($this->Settings->checkExistingBank(trim($model))) {
            echo 'false';
            die;
        }
        echo 'true';
        die;
    }

    //Commission generator

    public function insertCommission() {
        $commission_data = array();

        $customer_id = $this->input->post('customer_id');
        $commission_data['ref_customer_id'] = $customer_id;

        $product_id = $this->input->post('product_id');
        $commission_data['ref_product_id'] = $product_id;

        $from = !empty($this->input->post('from')) ? date('Y-m-d', strtotime($this->input->post('from'))) : date('Y/m/d');
        $commission_data['commission_from'] = $from;

        $to = !empty($this->input->post('to')) ? date('Y-m-d', strtotime($this->input->post('to'))) : date('Y/m/d');
        $commission_data['commission_to'] = $to;

        $commission_rate = $this->input->post('commission_rate');
        $commission_data['commission_rate'] = $commission_rate;

        $commission_data['InsUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        ;
        $commission_data['InsTerminal'] = $this->input->ip_address();
        $commission_data['InsDateTime'] = date('Y/m/d H:i:s');

        $this->Common_model->insertData('commission', $commission_data);

        $this->session->unset_userdata('commission_data');
        $this->session->set_userdata('commission_data', $commission_data);

        redirect(base_url() . 'Csettings/commission/' . 'generate_commission');
    }

    public function commission_generate($customer_id, $product_id, $from, $to, $commission_rate) {

        $customer_info = $this->Settings->customer_info();
        $product_info = $this->Settings->product_info($customer_id, $product_id, $from, $to);

        $subTotalCtn = 0;
        $subTotalQnty = 0;
        $subTotalComs = 0;
        if ($product_info) {
            foreach ($product_info as $k => $product) {

                $product_info[$k]['per_cartoon'] = $product_info[$k]['Qty'];

                $product_info[$k]['total_commission_rate'] = ($product_info[$k]['Qty'] * $commission_rate);

                $product_info[$k]['commission'] = $commission_rate;

                $product_info[$k]['subTotalQnty'] = $subTotalQnty + $product_info[$k]['Qty'];
                $subTotalQnty = $product_info[$k]['subTotalQnty'];

                $product_info[$k]['subTotalComs'] = $subTotalComs + $product_info[$k]['total_commission_rate'];
                $subTotalComs = $product_info[$k]['subTotalComs'];
            }
        }

        $rights['menu_rights'] = $this->Common_model->get_menu_rights('GENERATE_COMMISSION');
        if (empty($rights['menu_rights'])) {
            redirect('Unauthorized');
        }
        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'GENERATE_COMMISSION';

        $currency_details = $this->Web_settings->retrieve_setting_editdata();
        $data = array(
            'title' => display('commission'),
            'customer_info' => $customer_info,
            'commission_data' => $product_info,
            'subTotalCtn' => number_format($subTotalCtn, 6),
            'subTotalQnty' => number_format($subTotalQnty, 6),
            'subTotalComs' => number_format($subTotalComs, 6),
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
            'menu_rights' => $rights['menu_rights'],
        );

        $content = $this->parser->parse('commission/commission_generate', $data, true);
        $this->template->full_admin_html_view($content);
    }

    //This function is used to Generate Key
    public function generator($lenth) {
        $number = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "n", "m", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

        for ($i = 0; $i < $lenth; $i++) {
            $rand_value = rand(0, 34);
            $rand_number = $number["$rand_value"];

            if (empty($con)) {
                $con = $rand_number;
            } else {
                $con = "$con" . "$rand_number";
            }
        }
        return $con;
    }
}
