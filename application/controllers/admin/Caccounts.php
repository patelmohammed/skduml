<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Caccounts extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Settings');
        $this->load->model('Common_model');
        $this->load->model('Accounts');
        $this->load->model('Web_settings');

        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'TAX';
    }

    public function index() {
        $data = array('title' => display('accounts_inflow_form'));
        $data["accounts"] = $this->Accounts->accounts_name_finder(2);
        $data["bank"] = $this->Settings->get_bank_list();
        $content = $this->parser->parse('accounts/inflow', $data, true);
        $this->template->full_admin_html_view($content);
    }

    #===============Outflow accounts========#    

    public function outflow() {
        $data = array('title' => display('accounts_outflow_form'));
        $data["accounts"] = $this->Accounts->accounts_name_finder(1);
        $data["bank"] = $this->Settings->get_bank_list();
        $content = $this->parser->parse('accounts/outflow', $data, true);
        $this->template->full_admin_html_view($content);
    }

    #===============Add TAX================#

    public function add_tax() {

        $data['menu_rights'] = $this->Common_model->get_menu_rights('TAX', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('TAX', 'ADD', 'admin');

        $data = array();
        $view = 'admin/accounts/add_tax';
        $this->page_title = 'Add Tax';
        $this->load_admin_view($view, $data);
    }

    #==============TAX Entry==============#

    public function tax_entry() {

        $tax['tax'] = $this->input->post('enter_tax');
        $tax['tax_name'] = $this->input->post('tax_name');
        $tax['status'] = 1;
        $result = $this->Accounts->tax_entry($tax);
        if ($result == true) {
            $this->_show_message("Successfully Inserted Information", "success");
            redirect('admin/Caccounts/manage_tax');
        } else {
            $this->_show_message("already_exists", 'error_message');

            redirect('admin/Caccounts/manage_tax');
        }
    }

    #==============Manage TAX==============#

    public function manage_tax() {
        $this->Common_model->check_menu_access('TAX', 'VIEW', 'admin');
        $rights['menu_rights'] = $this->Common_model->get_menu_rights('TAX', 'admin');
        if (empty($rights['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }

        $data = array(
            'menu_rights' => $rights['menu_rights']
        );

        $view = 'admin/accounts/manage_tax';
        $this->page_title = 'Tax';
        $this->load_admin_view($view, $data);
    }

    public function get_tax_list() {
        $menu_rights = $this->Common_model->get_menu_rights('TAX', 'admin');

        $data['groupMasterData'] = array();
        $masterData = $this->Accounts->get_tax_list($this->input->post());
        $cnt = 0;
        $TaxName = $TaxPer = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $TaxName = $val->tax_name;
            $TaxPer = $val->TaxPer;

            $active_action = '';
            if ($val->active_status == 1) {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Caccounts/deactivateTax/' . $val->tax_id . '\'" data-toggle="tooltip" data-placement="left" title="Active Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox" checked/>
                            <label style="border: 0px; margin: 0px;"for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> ';
            } else {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Caccounts/activateTax/' . $val->tax_id . '\'" data-toggle="tooltip" data-placement="left" title="Inactive Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox"/>
                            <label style="border: 0px; margin: 0px;" for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> </a>';
            }

            $active_status = $active_action;

            $action_new = '';
            $action_new .= form_open();
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Caccounts/tax_edit/' . $val->tax_id . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" title="Edit" data-toggle="tooltip" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>\'><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="deleteTax btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2" name="' . $val->tax_id . '" data-toggle="tooltip" title="Delete Record" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>\'><i class="fal fa-times"></i></a>';
            }
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $TaxName, $TaxPer, $active_status, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Accounts->get_tax_list_search_data($this->input->post()),
            "recordsFiltered" => $this->Accounts->get_tax_list_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    #==============TAX Edit==============#

    public function tax_edit($tax_id) {
        $data['menu_rights'] = $this->Common_model->get_menu_rights('TAX', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }

        $this->Common_model->check_menu_access('TAX', 'EDIT', 'admin');

        $tax_info = $this->db->select('*')
                ->from('tax_information')
                ->where('tax_id', $tax_id)
                ->get()
                ->result_array();

        $data = array(
            'tax_id' => $tax_info[0]['tax_id'],
            'tax' => $tax_info[0]['tax'],
            'tax_name' => $tax_info[0]['tax_name']
        );

        $view = 'admin/accounts/tax_edit';
        $this->page_title = 'Tax Edit';
        $this->load_admin_view($view, $data);
    }

    #==============TAX Update==============#

    public function update_tax() {

        $id = $this->input->post('tax_id');
        $this->Common_model->check_menu_access('TAX', 'EDIT', 'admin');
        $rights['menu_rights'] = $this->Common_model->get_menu_rights('TAX', 'admin');
        if (empty($rights['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }

//        $tax_list = $this->db->select('*')
//                ->from('tax_information')
//                ->get()
//                ->result();

        $data = array(
            'tax' => $this->input->post('enter_tax') ? $this->input->post('enter_tax') : null,
            'tax_name' => $this->input->post('tax_name') ? $this->input->post('tax_name') : null
        );

        $this->Accounts->update_tax_data($data, $id);
        
        $this->_show_message("Information has been updated successfully!", "success");
        redirect(base_url('admin/Caccounts/manage_tax'));
        exit;
    }

    //customer active
    public function activateTax($tax_id) {
        $data = array();
        $data['active_status'] = '1';

        $this->Accounts->updateStatus($data, $tax_id);
        $this->_show_message("Activated Tax", "success");
        redirect('admin/Caccounts/manage_tax');
    }

    //Customer Inactive
    public function deactivateTax($tax_id) {
        $data = array();
        $data['active_status'] = '0';
        $this->load->model('Accounts');
        $this->Accounts->updateStatus($data, $tax_id);
        $this->_show_message("InActivated Tax", "success");
        redirect('admin/Caccounts/manage_tax');
    }

    #==============TAX Update==============#

    public function tax_delete() {
        $chk_for_delete = 0;

        $this->Common_model->check_menu_access('TAX', 'DELETE', 'admin');

        $tax_id = $this->input->post('id');
        
        $chk_for_delete += $this->Common_model->check_for_delete_master('tax_product', 'tax_id', $tax_id, 1);

        if ($chk_for_delete == 0) {
            $this->Accounts->delete_tax($tax_id);
            $data['result'] = true;
            $data['msg'] = "successfully deleted payment.";
        } else {
            $data['result'] = false;
            $data['msg'] = "You can't delete this Category! Because This used in product.";
        }
        echo json_encode($data);
        die;
    }

    #==============Closing reports==========#

    public function closing() {
        $data = array('title' => display('closing_account'));
        $data = $this->Accounts->accounts_closing_data();
        $content = $this->parser->parse('accounts/closing_form', $data, true);
        $this->template->full_admin_html_view($content);
    }

    #===============Accounts summary==========#

    public function summary() {

        $currency_details = $this->Web_settings->retrieve_setting_editdata();
        $data = array(
            'title' => display('accounts_summary_data'),
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
        );

        $data['table_inflow'] = $this->Accounts->table_name(2);
        $data['table_outflow'] = $this->Accounts->table_name(1);

        $data['inflow'] = $this->Accounts->accounts_summary(2);
        $data['total_inflow'] = number_format($this->Accounts->sub_total, 2, '.', ',');

        $data['outflow'] = $this->Accounts->accounts_summary(1);
        $data['total_outflow'] = number_format($this->Accounts->sub_total, 2, '.', ',');

        $content = $this->parser->parse('accounts/summary', $data, true);
        $this->template->full_admin_html_view($content);
    }

    #================Summary single===========#

    public function summary_single($start, $end, $account) {
        $data = array('title' => display('accounts_details_data'));

        //Getting all tables name.   
        $data['table_inflow'] = $this->Accounts->table_name(2);
        $data['table_outflow'] = $this->Accounts->table_name(1);

        $data['accounts'] = $this->Accounts->accounts_summary_details($start, $end, $account);
        //$data['total_inflow']=$this->accounts->sub_total;

        $content = $this->parser->parse('accounts/summary_single', $data, true);
        $this->template->full_admin_html_view($content);
    }

    #==============Summary report date  wise========#

    public function summary_datewise() {
        $start = $this->input->post('from_date');
        $end = $this->input->post('to_date');
        $account = $this->input->post('accounts');

        if ($account != "All") {
            $url = "Caccounts/summary_single/$start/$end/$account";
            redirect(base_url($url));
            exit;
        }

        $currency_details = $this->Web_settings->retrieve_setting_editdata();

        $data = array(
            'title' => display('datewise_summary_data'),
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
        );

        //Getting all tables name.   
        $data['table_inflow'] = $this->Accounts->table_name(2);
        $data['table_outflow'] = $this->Accounts->table_name(1);

        $data['inflow'] = $this->Accounts->accounts_summary_datewise($start, $end, "2");
        $data['total_inflow'] = $this->Accounts->sub_total;

        $data['outflow'] = $this->Accounts->accounts_summary_datewise($start, $end, "1");
        $data['total_outflow'] = $this->Accounts->sub_total;

        $content = $this->parser->parse('accounts/summary', $data, true);
        $this->template->full_admin_html_view($content);
    }

    #============ Cheque Manager ==============#

    public function cheque_manager() {
        $CI = & get_instance();
        $CI->auth->check_admin_auth();

        $CI->Common_model->check_menu_access('CHEQUE_MANAGER', 'VIEW');
        $data['menu_rights'] = $CI->Common_model->get_menu_rights('CHEQUE_MANAGER');
        if (empty($data['menu_rights'])) {
            redirect('Unauthorized');
        }

        $this->page_id = 'AUTHORIZATION';
        $this->menu_id = 'CHEQUE_MANAGER';

        $currency_details = $this->Web_settings->retrieve_setting_editdata();

        $data = array(
            'title' => display('accounts_cheque_manager'),
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
        );

        $content = $this->parser->parse('accounts/cheque_manager', $data, true);
        $this->template->full_admin_html_view($content);
    }

    public function get_cheque_payment_list() {
        $menu_rights = $this->Common_model->get_menu_rights('CHEQUE_MANAGER');

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);

        $data['groupMasterData'] = array();
        $masterData = $this->Accounts->get_cheque_payment_list($this->input->post());
        $cnt = 0;
        $DateTime = $ChequeNo = $PayeeName = $BankName = $TransferAmt = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $DateTime = isset($val->chq_given_date) && !empty($val->chq_given_date) ? date('d-m-Y', strtotime($val->chq_given_date)) : null;
            $ChequeNo = $val->cheque_no;
            $PayeeName = isset($val->transaction_category) && !empty($val->transaction_category) && $val->transaction_category == '1' ? $val->supplier_name : (isset($val->transection_type) && !empty($val->transection_type) && $val->transection_type == '2' ? $val->customer_name : null);
            $BankName = $val->bank_name;
            $TransferAmt = $val->amount;

            $action_new = '';
            $action_new .= form_open();
            $action_new .= '<div style="display:flex; gap: 5px;">';

            if ($menu_rights['edit_right'] == 1) {
                if ($val->cheque_status == 0) {
                    $action_new .= '<a href="javascript:void(0);" class="btn btn-success btn-sm approveCheque" name="' . $val->cheque_id . '" data-transaction_id="' . $val->transaction_id . '" data-transaction_category="' . $val->transaction_category . '" is_approve="' . $val->cheque_status . '" data-toggle="tooltip" data-placement="bottom" title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a>';
                    $action_new .= '<a href="javascript:void(0);" class="rejectCheque btn btn-danger btn-sm" name="' . $val->cheque_id . '" data-transaction_id="' . $val->transaction_id . '" data-transaction_category="' . $val->transaction_category . '" is_approve="' . $val->cheque_status . '" data-toggle="tooltip" data-placement="right" title="" data-original-title="Reject"><i class="fa fa-times" aria-hidden="true"></i></a>';
                }
            }

            $action_new .= '</div>';
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $DateTime, $ChequeNo, $PayeeName, $BankName, $TransferAmt, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Accounts->get_cheque_payment_search_data($this->input->post()),
            "recordsFiltered" => $this->Accounts->get_cheque_payment_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    #============ Cheque Manager edit ==============#

    public function cheque_manager_edit($action) {

        $transection_id = $this->input->post('transaction_id');
        $transaction_category = $this->input->post('transaction_category');

        if (isset($transection_id) && !empty($transection_id)) {
            $this->Accounts->data_update(array('cheque_status' => $action), "cheque_manger", array('transaction_id' => $transection_id));
            if ($transaction_category == 1) {
                $this->Accounts->data_update(array('status' => $action), "supplier_ledger", array('transaction_id' => $transection_id));
            }
            if ($transaction_category == 2) {
                $this->Accounts->data_update(array('status' => $action), "customer_ledger", array('transaction_id' => $transection_id));
            }

            $data['status'] = true;
        } else {
            $data['status'] = false;
            $data['msg'] = 'No transaction found, Try again!';
        }

        echo json_encode($data);
        die();
    }

    // Add daily closing 
    public function add_daily_closing() {
        $todays_date = date("Y-m-d H:i:s");

        $data = array(
            'last_day_closing' => str_replace(',', '', $this->input->post('last_day_closing')),
            'cash_in' => str_replace(',', '', $this->input->post('cash_in')),
            'cash_out' => str_replace(',', '', $this->input->post('cash_out')),
            'date' => $todays_date,
            'amount' => str_replace(',', '', $this->input->post('cash_in_hand')),
            'status' => 1
        );

        $closing_id = $this->Accounts->daily_closing_entry($data);

        $note = array(
            'closing_id' => $closing_id,
            'cash_date' => date('Y-m-d H:i:s'),
            '2000n' => ($this->input->post('thousands') ? $this->input->post('thousands') : null),
            '500n' => ($this->input->post('fivehnd') ? $this->input->post('fivehnd') : null),
            '200n' => ($this->input->post('two_hundrad') ? $this->input->post('two_hundrad') : null),
            '100n' => ($this->input->post('hundrad') ? $this->input->post('hundrad') : null),
            '50n' => ($this->input->post('fifty') ? $this->input->post('fifty') : null),
            '20n' => ($this->input->post('twenty') ? $this->input->post('twenty') : null),
            '10n' => ($this->input->post('ten') ? $this->input->post('ten') : null),
            '5n' => ($this->input->post('five') ? $this->input->post('five') : null),
            '2n' => ($this->input->post('two') ? $this->input->post('two') : null),
            '1n' => ($this->input->post('one') ? $this->input->post('one') : null),
            'grandtotal' => ($this->input->post('grndtotal') ? $this->input->post('grndtotal') : 0)
        );

        $this->db->insert('notes', $note);

        $this->session->set_userdata(array('message' => display('successfully_added')));
        redirect(base_url('Caccounts/closing_report'));
        exit;
    }

    // Add drawing entry
    public function add_drawing_entry() {
        $CI = & get_instance();
        $this->auth->check_admin_auth();
        $CI->load->model('Closings');

        $todays_date = date("m-d-Y");

        $data = array(
            'drawing_id' => $this->generator(15),
            'date' => $todays_date,
            'drawing_title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'amount' => $this->input->post('amount'),
            'status' => 1
        );

        $invoice_id = $CI->Closings->drawing_entry($data);
        $this->session->set_userdata(array('message' => display('successfully_draw_added')));
        redirect(base_url('cclosing'));
        exit;
    }

    // Add expance entry
    public function add_expence_entry() {
        $CI = & get_instance();
        $this->auth->check_admin_auth();
        $CI->load->model('Closings');

        $todays_date = date("m-d-Y");

        $data = array(
            'expence_id' => $this->generator(15),
            'date' => $todays_date,
            'expence_title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'amount' => $this->input->post('amount'),
            'status' => 1
        );

        $invoice_id = $CI->Closings->expence_entry($data);
        $this->session->set_userdata(array('message' => display('successfully_added')));
        redirect(base_url('cclosing'));
    }

    // Add bank entry
    public function add_banking_entry() {
        $CI = & get_instance();
        $this->auth->check_admin_auth();
        $CI->load->model('Closings');

        $todays_date = date("m-d-Y");

        $data = array(
            'banking_id' => $this->generator(15),
            'date' => $todays_date,
            'bank_id' => $this->input->post('bank_id'),
            'deposit_type' => $this->input->post('deposit_name'),
            'transaction_type' => $this->input->post('transaction_type'),
            'description' => $this->input->post('description'),
            'amount' => $this->input->post('amount'),
            'status' => 1
        );

        $invoice_id = $CI->Closings->banking_data_entry($data);
        $this->session->set_userdata(array('message' => display('successfully_added')));
        redirect(base_url('cclosing'));
        exit;
    }

    //Closing report
    public function closing_report() {
        $content = $this->laccounts->daily_closing_list();
        $this->template->full_admin_html_view($content);
    }

    // Date wise closing reports 
    public function date_wise_closing_reports() {

        $this->Common_model->check_menu_access('CLOSING_REPORT', 'VIEW');
        $rights['menu_rights'] = $this->Common_model->get_menu_rights('CLOSING_REPORT');
        if (empty($rights['menu_rights'])) {
            redirect('Unauthorized');
        }
        $this->page_id = 'REPORT';
        $this->menu_id = 'ACCOUNT';
        $this->sub_menu_id = 'CLOSING_REPORT';

        $from_date = $this->input->get('from_date');
        if (isset($from_date) && !empty($from_date)) {
            $from_date = date('Y-m-d', strtotime($from_date));
        }
        $to_date = $this->input->get('to_date');
        if (isset($to_date) && !empty($to_date)) {
            $to_date = date('Y-m-d', strtotime($to_date));
        }

        #
        #pagination starts
        #
        $config["base_url"] = base_url('Caccounts/date_wise_closing_reports/');
        $config["total_rows"] = $this->Accounts->get_date_wise_closing_report_count($from_date, $to_date);
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
        $config["num_links"] = 5;
        $config['suffix'] = '?' . http_build_query($_GET, '', '&');
        $config['first_url'] = $config["base_url"] . $config['suffix'];

        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();

        #
        #pagination ends
        # 

        $content = $this->laccounts->get_date_wise_closing_reports($links, $config["per_page"], $page, $from_date, $to_date);

        $this->template->full_admin_html_view($content);
    }

    // Random Id generator
    public function generator($lenth) {
        $number = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "N", "M", "O", "P", "Q", "R", "S", "U", "V", "T", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

        for ($i = 0; $i < $lenth; $i++) {
            $rand_value = rand(0, 61);
            $rand_number = $number["$rand_value"];

            if (empty($con)) {
                $con = $rand_number;
            } else {
                $con = "$con" . "$rand_number";
            }
        }
        return $con;
    }

    public function checkTax($id = '') {

        $tax_name = $this->input->get('tax_name');

        if ($id != '') {
            if ($this->Accounts->checkExistingTaxName(trim($tax_name), $id)) {
                echo 'false';
                die;
            }
        } else if ($this->Accounts->checkExistingTaxName(trim($tax_name))) {
            echo 'false';
            die;
        }
        echo 'true';
        die;
    }
}
