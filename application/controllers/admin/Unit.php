<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Unit extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->load->model('Units');
        $this->load->model('Common_model');
        $this->page_id = 'MASTERS';
        $this->menu_id = 'PRODUCT_UNIT';
    }

    // ================ by default create unit page load. =============
    public function index() {
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT_UNIT', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PRODUCT_UNIT', 'ADD', 'admin');

        $view = 'admin/units/add_unit_form';
        $this->page_title = 'PRODUCT UNIT';
        $this->load_admin_view($view, $data);
    }

//    ========== close index method ============
//    =========== unit add is start ====================
    public function insert_unit() {
        
        $data = array(
            'unit_name' => $this->input->post('unit_name'),
            'status' => 1
        );
        $result = $this->Units->insert_unit($data);

        if ($result == TRUE) {
            $this->_show_message("Information has been added successfully!", "success");
            if (isset($_POST['add-unit'])) {
                redirect(base_url('admin/Unit/manage_unit'));
            } elseif (isset($_POST['add-unit-another'])) {
                redirect(base_url('admin/Unit'));
            }
        } else {
            $this->_show_message("Information already inserted", "error");
            redirect(base_url('admin/Unit'));
        }
    }

//    =========== unit add is close ====================
//    =========== its for all unit record show start====================
    public function manage_unit() {
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT_UNIT', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        
        $this->Common_model->check_menu_access('PRODUCT_UNIT', 'VIEW', 'admin');
        
        $data['sabhasad_data'] = $this->Common_model->geAlldata('tbl_sabhasad');
        $view = 'admin/units/manage_unit';
        $this->page_title = 'PRODUCT UNIT';
        $this->load_admin_view($view, $data);
    }

    public function get_product_unit_list() {
        $menu_rights = $this->Common_model->get_menu_rights('PRODUCT_UNIT', 'admin');

        $data['groupMasterData'] = array();
        $masterData = $this->Units->get_product_unit_list($this->input->post());
        $cnt = 0;
        $UnitName = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $UnitName = $val->unit_name;
            
            $active_action = '';
            if ($val->active_status == 1) {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Unit/deactivateUnit/' . $val->unit_id . '\'" data-toggle="tooltip" data-placement="left" title="Active Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox" checked/>
                            <label style="border: 0px; margin: 0px;"for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> ';
            } else {
                $active_action .= '<div class="material-switch" onclick="window.location.href=\'' . base_url() . 'admin/Unit/activateUnit/' . $val->unit_id . '\'" data-toggle="tooltip" data-placement="left" title="Inactive Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox"/>
                            <label style="border: 0px; margin: 0px;" for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> </a>';
            }

            $active_status = $active_action;

            $action_new = '';
            $action_new .= form_open();
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Unit/unit_update_form/' . $val->unit_id . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" title="Edit" data-toggle="tooltip" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>\'><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="DeleteUnit btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2" name="' . $val->unit_id . '" data-toggle="tooltip" title="Delete Record" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>\'><i class="fal fa-times"></i></a>';
            }
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $UnitName, $active_status, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Units->get_product_unit_list_search_data($this->input->post()),
            "recordsFiltered" => $this->Units->get_product_unit_list_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

//    ========== its for all unit record show close ================
//    =========== its for unit edit form start ===============
    public function unit_update_form($unit_id) {
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT_UNIT', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PRODUCT_UNIT', 'EDIT', 'admin');
        
        $data['unit_details'] = $this->Units->retrieve_unit_editdata($unit_id);

        $view = 'admin/units/edit_unit_form';
        $this->page_title = 'PRODUCT UNIT';
        $this->load_admin_view($view, $data);
    }
    
    
    //customer active
    public function activateUnit($unit_id) {
        $this->Common_model->check_menu_access('PRODUCT_UNIT', 'EDIT', 'admin');
        $data = array();
        $data['active_status'] = '1';
        $this->Units->updateStatus($data, $unit_id);
        $this->_show_message("Information has been activated!", "success");
        redirect(base_url('admin/Unit/manage_unit'));
    }

    //Customer Inactive
    public function deactivateUnit($unit_id) {
        $this->Common_model->check_menu_access('PRODUCT_UNIT', 'EDIT', 'admin');
        $data = array();
        $data['active_status'] = '0';
        $this->Units->updateStatus($data, $unit_id);
        $this->_show_message("Information has been inactivated!", "success");
        redirect(base_url('admin/Unit/manage_unit'));
    }

//    =========== its for unit edit form close ===============
//    =========== its for unit update start  ===============
    public function unit_update() {
        $this->Common_model->check_menu_access('PRODUCT_UNIT', 'EDIT', 'admin');
        $unit_id = $this->input->post('unit_id');
        $data = array(
            'unit_name' => $this->input->post('unit_name'),
        );
        $this->Units->unit_update($data, $unit_id);
        $this->_show_message("Information has been updated successfully!", "success");
        redirect(base_url('admin/Unit/manage_unit'));
    }

//    =========== its for unit update close ===============
//    =========== its for unit delete start ===============
    public function unit_delete($unit_id) {
        $this->Common_model->check_menu_access('PRODUCT_UNIT', 'DELETE', 'admin');
//        $unit_id = $this->input->post('unit_id');
        $countCommission = 0; //$this->Units->getCollectionCountByStorage($unit_id);

        $data = [];
        
        if ($countCommission > 0) {
            $this->_show_message("Unit already used!", "warning");
            $data['result'] = false;
        } else {
            $this->Units->unit_delete($unit_id);
            $this->_show_message("Information has been deleted successfully!", "success");
            $data['result'] = true;
        }
        echo json_encode($data);
        die;
    }

    public function checkProductUnit($id = '') {

        $unit_name = $this->input->get('unit_name');

        if ($id != '') {
            if ($this->Units->checkExistingUnit(trim($unit_name), $id)) {
                echo 'false';
                die;
            }
        } else if ($this->Units->checkExistingUnit(trim($unit_name))) {
            echo 'false';
            die;
        }
        echo 'true';
        die;
    }

}
