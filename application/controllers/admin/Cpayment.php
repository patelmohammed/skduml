<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cpayment extends MY_Controller {

    public $menu;

    function __construct() {
        parent::__construct();
        $this->load->model('Payment');
        $this->load->model('Settings');
        $this->load->model('Reports');
        $this->load->model('Suppliers');
        $this->load->model('Accounts');
        $this->load->model('Customers');
        $this->load->model('Common_model');
        $this->load->model('Web_settings');
    }

    public function index() {
        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'PAYMENT';
        $this->page_title = 'Add payment';

        $this->Common_model->check_menu_access('PAYMENT', 'ADD', 'admin');

        $bank = $this->Common_model->getBankList();
        $customer = $this->Common_model->getCustomerList();
        $supplier = $this->Suppliers->supplier_list();
        $payment_mode_list = $this->Common_model->getPaymentModeList();

        $data = array(
            'supplier' => $supplier,
            'customer' => $customer,
            'bank' => $bank,
            'payment_mode_list' => $payment_mode_list
        );

        $view = 'admin/payment/payment';
        $this->load_admin_view($view, $data);
    }

    public function receipt_transaction() {
        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'RECEIPT';
        $this->page_title = 'Add Receipt';

        $this->Common_model->check_menu_access('RECEIPT', 'ADD', 'admin');

        $bank = $this->Common_model->getBankList();
        $customers = $this->Common_model->getCustomerList();
        $supplier = $this->Suppliers->supplier_list("110", "0");
        $payment_mode_list = $this->Common_model->getPaymentModeList();

        $data = array(
            'supplier' => $supplier,
            'customer' => $customers,
            'bank' => $bank,
            'payment_mode_list' => $payment_mode_list
        );

        $view = 'admin/payment/receipt_form';
        $this->load_admin_view($view, $data);
    }

    /* transaction method add start */

    public function transaction_entry($is_edit = '') {
        if ($is_edit != '') {
            $transaction_id = $is_edit;
            if (!$this->Payment->delete_transaction($transaction_id)) {
                $this->session->set_userdata(array('error' => 'Seomething Went Wrong, Please Contact Administrator'));
                redirect(base_url('admin/Cpayment/manage_payment'));
            }
        }

        $transaction_date = $this->input->post('date') ? date('Y-m-d H:i:s', strtotime($this->input->post('date'))) : date('Y-m-d H:i:s');
        $transaction_type = $this->input->post('transaction_type');
        $transaction_category = $this->input->post('transaction_category');
        $description = $this->input->post('description');
        $supplier_id = $this->input->post('supplier_id');
        $customer_id = $this->input->post('customer_id');
        $loan_id = $this->input->post('loan_id');
        $account_id = $this->input->post('office');
        $payment_type = $this->input->post('payment_type');
        $pay_amount = $this->input->post('pay_amount') ? $this->input->post('pay_amount') : 0;
        $receipt_amount = $this->input->post('amount') ? $this->input->post('amount') : 0;
        $cheque_no = $this->input->post('cheque_no');
        $cheque_given_date = dbdate($this->input->post('cheque_mature_date'));
        $bank_id = $this->input->post('bank_name');
        $relation_id = '';
        $insert_status = TRUE;

        if ($transaction_category == 1) {
            $relation_id = $this->input->post('supplier_id');
        } elseif ($transaction_category == 2) {
            $relation_id = $this->input->post('customer_id');
        } elseif ($transaction_category == 3) {
            $relation_id = $this->input->post('office'); //account_id
        } else if ($transaction_category == 4) {
            $relation_id = $this->input->post('loan_id');
        }

        $transaction_data = array(
            'date_of_transaction' => $transaction_date,
            'transaction_type' => $transaction_type,
            'transaction_category' => $transaction_category,
            'receipt_amount' => $receipt_amount,
            'transaction_mode' => $payment_type,
            'pay_amount' => $pay_amount,
            'description' => $description,
            'relation_id' => $relation_id,
            'is_transaction' => 1,
        );

        $transaction_id = $this->Payment->insert_transaction($transaction_data);

        $receipt_no = generator(10, 'customer_ledger', 'receipt_no');

        $cheque_manager_data = array(
            'transaction_id' => $transaction_id,
            'bank_id' => $bank_id,
            'cheque_no' => $cheque_no,
            'chq_given_date' => $cheque_given_date,
            'transection_type' => $transaction_category,
            'cheque_status' => 1,
            'status' => '1'
        );

        if (isset($customer_id) && !empty($customer_id)) {
            //Data ready for customer ledger
            $data = array(
                'transaction_id' => $transaction_id,
                'customer_id' => $customer_id,
                'invoice_no' => NULL,
                'receipt_no' => $receipt_no,
                'amount' => $transaction_type == '1' ? $pay_amount : $receipt_amount,
                'description' => $description,
                'payment_type' => $payment_type,
                'cheque_no' => $cheque_no,
                'date' => $transaction_date,
                'receipt_from' => ($transaction_type == 2 ? 'receipt' : ''),
                'status' => 1,
                'd_c' => ($transaction_type == 2 ? 'd' : 'c'),
            );

            $this->db->trans_begin();
            $this->Accounts->customer_ledger($data);
            if ($payment_type == '2' || $payment_type == '3') {
                $cheque_manager_data['amount'] = $transaction_type == '1' ? $pay_amount : $receipt_amount;
                $this->Accounts->insertChequePayorderData($cheque_manager_data);
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $insert_status = FALSE;
            } else {
                $this->db->trans_commit();
            }
        }

        if (isset($loan_id) && $loan_id != '') {
            $loan_data = array(
                'transaction_id' => $transaction_id,
                'person_id' => $relation_id,
                $transaction_type == '1' ? 'credit' : 'debit' => ($transaction_type == '1' ? $pay_amount : $receipt_amount),
                'date' => $transaction_date,
                'details' => $description,
                'status' => $transaction_type == '1' ? 2 : 1
            );

            $this->db->trans_begin();
            $this->Settings->submit_payment($loan_data);

            if ($payment_type == '2' || $payment_type == '3') {
                $cheque_manager_data['amount'] = $transaction_type == '1' ? $pay_amount : $receipt_amount;
                $this->Accounts->insertChequePayorderData($cheque_manager_data);
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $insert_status = FALSE;
            } else {
                $this->db->trans_commit();
            }
        }

        if (isset($supplier_id) && $supplier_id != '') {
            $supplier_data = array(
                'transaction_id' => $transaction_id,
                'supplier_id' => $supplier_id,
                'chalan_no' => NULL,
                'deposit_no' => generator(10, 'supplier_ledger', 'deposit_no'),
                'amount' => $transaction_type == '1' ? $pay_amount : $receipt_amount,
                'description' => $description,
                'payment_type' => $payment_type,
                'cheque_no' => $cheque_no,
                'date' => $transaction_date,
                'status' => '1',
                'd_c' => $transaction_type == '1' ? 'c' : 'd'
            );

            $this->db->trans_begin();
            $this->Accounts->supplier_ledger($supplier_data);

            if ($payment_type == '2' || $payment_type == '3') {
                $cheque_manager_data['amount'] = $supplier_data['amount'];
                $this->Accounts->insertChequePayorderData($cheque_manager_data);
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $insert_status = FALSE;
            } else {
                $this->db->trans_commit();
            }
        }

        if (isset($account_id) && $account_id != '') {

            $this->db->trans_begin();

            if ($payment_type == '2' || $payment_type == '3') {
                $cheque_manager_data['amount'] = $transaction_type == '1' ? $pay_amount : $receipt_amount;
                $this->Accounts->insertChequePayorderData($cheque_manager_data);
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $insert_status = FALSE;
            } else {
                $this->db->trans_commit();
            }
        }

        if ($insert_status) {
            $this->session->set_userdata(array('message' => ('successfully_saved')));
        } else {
            $this->session->set_userdata(array('error' => 'Transaction not completed, Check the Transaction Data'));
        }

        if ($transaction_type == 1) {
            redirect(base_url('admin/Cpayment/manage_payment'));
        } else {
            redirect(base_url('admin/Cpayment/manage_payment'));
            exit;
        }
    }

    /* transaction method add End */

    public function manage_payment() {
        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'TRANSACTION';
        $this->page_title = 'Trascations';

        $this->Common_model->check_menu_access('TRANSACTION', 'VIEW', 'admin');
        $rights['menu_rights'] = $this->Common_model->get_menu_rights('TRANSACTION', 'admin');
        if (empty($rights['menu_rights'])) {
            redirect('Unauthorized');
        }

        $category = $this->Payment->tran_cat_list();

        $currency_details = $this->Web_settings->retrieve_setting_editdata();
        $company_info = $this->Reports->retrieve_company();

        $data = array(
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
            'category' => $category,
            'company_info' => $company_info,
            'menu_rights' => $rights['menu_rights'],
        );

        $view = 'admin/payment/Payment1';
        $this->load_admin_view($view, $data);
    }

    public function get_payment_list() {
        $menu_rights = $this->Common_model->get_menu_rights('PAYMENT', 'admin');

        $data['groupMasterData'] = array();
        $masterData = $this->Payment->get_payment_list($this->input->post());
        $cnt = 0;
        $TransactionDate = $Name = $AccountType = $Description = $TransactionMode = $ReceiptAmount = $PayAmount = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $TransactionDate = $val->TransactionDate;
            $Name = $val->Name;
            $AccountType = $val->AccountType;
            $Description = $val->description;
            $TransactionMode = $val->TransactionMode;
            $ReceiptAmount = (isset($val->receipt_amount) && !empty($val->receipt_amount) && $val->receipt_amount != 0 ? $val->receipt_amount : '');
            $PayAmount = (isset($val->pay_amount) && !empty($val->pay_amount) && $val->pay_amount != 0 ? $val->pay_amount : '');

            $action_new = '';
            $action_new .= form_open();
            if ($menu_rights['edit_right'] == 1) {
                if ($val->is_transaction == 0) {
                    $url = 'javascript:void(0);';
                    $title = 'Please Edit From Invoice';
                } else {
                    $title = 'Update';
                    if ($val->transaction_type == 2) {
                        $url = base_url() . 'admin/Cpayment/receipt_update_form/' . $val->transaction_id;
                    } else {
                        $url = base_url() . 'admin/Cpayment/payment_update_form/' . $val->transaction_id;
                    }
                }
                $action_new .= '<a href="' . $url . '" title="' . $title . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" title="Edit" data-toggle="tooltip" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>\'><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="deletePayment btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2" data-id="' . $val->transaction_id . '" data-toggle="tooltip" title="Delete Record" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>\'><i class="fal fa-times"></i></a>';
            }
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $TransactionDate, $Name, $AccountType, $Description, $TransactionMode, $ReceiptAmount, $PayAmount, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Payment->get_payment_list_search_data($this->input->post()),
            "recordsFiltered" => $this->Payment->get_payment_list_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    public function payment_delete() {
        $chk_for_delete = 0;
        $transaction_id = $this->input->post('id');

        if ($chk_for_delete == 0) {
            $this->Payment->delete_transaction($transaction_id);
            $data['result'] = true;
            $data['msg'] = "successfully deleted transaction";
        } else {
            $data['result'] = false;
            $data['msg'] = "You can't delete this Transaction!";
        }

        echo json_encode($data);
        die;
    }

    public function payment_update_form($trans) {
        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'TRANSACTION';
        $this->page_title = 'Edit Transaction';
        
        $bank = $this->Common_model->getBankList();
        $customer = $this->Common_model->getCustomerList();
        $supplier = $this->Suppliers->supplier_list();
        $payment = $this->Payment->payment_updata($trans);
        $category = $this->Payment->tran_cat_list();
        $payment_mode_list = $this->Payment->getPaymentModeList();

        $data = array(
            'transaction_id' => $payment['transaction_id'],
            'date_of_transaction' => ($payment['date_of_transaction']),
            'relation_id' => $payment['relation_id'],
            'transaction_category' => $payment['transaction_category'],
            'transaction_type' => $payment['transaction_type'],
            'transaction_mode' => $payment['transaction_mode'],
            'description' => $payment['description'],
            'pay_amount' => $payment['pay_amount'],
            'bank_id' => $payment['bank_id'],
            'cheque_no' => $payment['cheque_no'],
            'chq_given_date' => $payment['chq_given_date'],
            'category_list' => $category,
            'supplier' => $supplier,
            'customer' => $customer,
            'bank' => $bank,
            'payment_mode_list' => $payment_mode_list
        );
        
//        preprint($data);

        $view = 'admin/payment/update_payment';
        $this->load_admin_view($view, $data);
    }
    
    public function receipt_update_form($trans) {
        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'TRANSACTION';
        $this->page_title = 'Edit Transaction';
        
        $bank = $this->Common_model->getBankList();
        $customer = $this->Common_model->getCustomerList();
        $supplier = $this->Suppliers->supplier_list();

        $payment = $this->Payment->payment_updata($trans);
        $category = $this->Payment->tran_cat_list();
        $payment_mode_list = $this->Payment->getPaymentModeList();

        $data = array(
            'transaction_id' => $payment['transaction_id'],
            'date_of_transaction' => ($payment['date_of_transaction']),
            'relation_id' => $payment['relation_id'],
            'transaction_category' => $payment['transaction_category'],
            'transaction_type' => $payment['transaction_type'],
            'transaction_mode' => $payment['transaction_mode'],
            'description' => $payment['description'],
            'receipt_amount' => $payment['receipt_amount'],
            'bank_id' => $payment['bank_id'],
            'cheque_no' => $payment['cheque_no'],
            'chq_given_date' => $payment['chq_given_date'],
            'category_list' => $category,
            'supplier' => $supplier,
            'customer' => $customer,
            'bank' => $bank,
            'payment_mode_list' => $payment_mode_list
        );
        
//        preprint($data);

        $view = 'admin/payment/update_receipt';
        $this->load_admin_view($view, $data);
    }

    // trans report details
    public function trans_details() {
        $content = $this->lpayment->transaction_report_details();
        $this->template->full_admin_html_view($content);
    }

    public function tran_det_id($id) {
        $content = $this->lpayment->transaction_data($id);
        $this->template->full_admin_html_view($content);
    }

    #==============Closing form==========#

    public function closing() {
        $CI = & get_instance();

        $CI->Common_model->check_menu_access('CLOSING', 'VIEW');
        $rights['menu_rights'] = $CI->Common_model->get_menu_rights('CLOSING');
        if (empty($rights['menu_rights'])) {
            redirect('Unauthorized');
        }
        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'CLOSING';

        $data = array('title' => "Accounts | Daily Closing");
        $data = $this->Accounts->accounts_closing_data();
        $content = $this->parser->parse('accounts/closing_form', $data, true);
        $this->template->full_admin_html_view($content);
    }

    #====== Cash closing report ============#

    public function closing_report() {
        $CI = & get_instance();
        $CI->Common_model->check_menu_access('CLOSING_REPORT', 'VIEW');
        $rights['menu_rights'] = $CI->Common_model->get_menu_rights('CLOSING_REPORT');
        if (empty($rights['menu_rights'])) {
            redirect('Unauthorized');
        }
        $this->page_id = 'REPORT';
        $this->menu_id = 'ACCOUNT';
        $this->sub_menu_id = 'CLOSING_REPORT';

        $content = $this->laccounts->daily_closing_list();
        $this->template->full_admin_html_view($content);
    }

    public function summary_single($start, $end, $account) {
        $data = array('title' => display('accounts_details_data'));

        //Getting all tables name.   
        $data['table_inflow'] = $this->Payment->table_name(2);
        $data['table_outflow'] = $this->Payment->table_name(1);

        $data['accounts'] = $this->Payment->trans_summary_details($start, $end, $account);

        $content = $this->parser->parse('payment/summary_single', $data, true);
        $this->template->full_admin_html_view($content);
    }

    // transaction report

    public function summaryy() {
        $this->Common_model->check_menu_access('DAILY_SUMMARY', 'VIEW');
        $rights['menu_rights'] = $this->Common_model->get_menu_rights('DAILY_SUMMARY');
        if (empty($rights['menu_rights'])) {
            redirect('Unauthorized');
        }
        $this->page_id = 'REPORT';
        $this->menu_id = 'ACCOUNT';
        $this->sub_menu_id = 'DAILY_SUMMARY';

        $content = $this->lpayment->trans_data();
        $this->template->full_admin_html_view($content);
    }

    public function getreport_details() {

        $transaction_category = $this->input->post('transection_category');
        $data['marks'] = $this->Payment->get_details($transaction_category);
        $data['Account_Type'] = $transaction_category == 2 ? 'Customer' : 'Supplier';
        $this->load->view("payment/modal", $data);
    }

    public function today_details() {
        $transaction_category = $_POST['transaction_category'];
        $data['marks'] = $this->Payment->today_details($transaction_category);
        $this->load->view("payment/modal", $data);
    }

//date wise report details
    public function date_summary() {

        $this->Common_model->check_menu_access('DAILY_SUMMARY', 'VIEW');
        $rights['menu_rights'] = $this->Common_model->get_menu_rights('DAILY_SUMMARY');
        if (empty($rights['menu_rights'])) {
            redirect('Unauthorized');
        }
        $this->page_id = 'REPORT';
        $this->menu_id = 'ACCOUNT';
        $this->sub_menu_id = 'DAILY_CASH_FLOW';

        $content = $this->lpayment->trans_datewise_data();
        $this->template->full_admin_html_view($content);
    }

    // search date between controller 
    public function search_datewise() {

        $this->Common_model->check_menu_access('DAILY_SUMMARY', 'VIEW');
        $rights['menu_rights'] = $this->Common_model->get_menu_rights('DAILY_SUMMARY');
        if (empty($rights['menu_rights'])) {
            redirect('Unauthorized');
        }
        $this->page_id = 'REPORT';
        $this->menu_id = 'ACCOUNT';
        $this->sub_menu_id = 'DAILY_CASH_FLOW';

        $start = dbdate($this->input->post('from_date'));
        if (isset($start)) {
            $start = date('Y-m-d', strtotime($start));
        }
        $end = dbdate($this->input->post('to_date'));
        if (isset($end)) {
            $end = date('Y-m-d', strtotime($end));
        }

        $content = $this->lpayment->result_datewise_data($start, $end);
        $this->template->full_admin_html_view($content);
    }

    //custom report transaction

    public function custom_report() {
        $config["base_url"] = base_url('Cpayment/custom_report/');
        $config["total_rows"] = $this->Payment->count_transaction();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config["num_links"] = 5;
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #  
        $content = $this->lpayment->trans_custom_report_data($links, $config["per_page"], $page);
        $this->template->full_admin_html_view($content);
    }

// custom report search

    public function custom_search_datewise() {
        $start = dbdate($this->input->post('from_date'));
        $end = dbdate($this->input->post('to_date'));
        $account = $this->input->post('accounts');
        if ($account == "All") {
            $url = "Cpayment/custom_report";
            redirect(base_url($url));
            exit;
        }
        $content = $this->lpayment->custom_result_datewise_data($start, $end, $account);
        $this->template->full_admin_html_view($content);
    }

    public function generator($lenth) {
        $number = array("1", "2", "3", "4", "5", "6", "7", "8", "9");

        for ($i = 0; $i < $lenth; $i++) {
            $rand_value = rand(0, 8);
            $rand_number = $number["$rand_value"];

            if (empty($con)) {
                $con = $rand_number;
            } else {
                $con = "$con" . "$rand_number";
            }
        }
        return $con;
    }

    public function transaction_date_to_date() {
        $start_date = dbdate($this->input->get('from_date'));
        $end_date = dbdate($this->input->get('to_date'));

        $content = $this->lpayment->payment_date_date_info($start_date, $end_date); //, $links, $config["per_page"], $page);
        $this->template->full_admin_html_view($content);
    }

    public function get_supplier_amount() {
        $supplier_id = $this->input->post('supp_id');

        $data['payable_amount'] = $this->Payment->getPaymentAmountBySupplier($supplier_id);

        if (isset($data['payable_amount']) && !empty($data['payable_amount'])) {
            $data['status'] = true;
        } else {
            $data['status'] = false;
            $data['msg'] = 'No payable amount found!';
        }

        echo json_encode($data);
        die();
    }

    public function get_customer_amount() {
        $customer_id = $this->input->post('cust_id');

        $data['payable_amount'] = $this->Payment->getPaymentAmountByCustomer($customer_id);

        if (isset($data['payable_amount']) && !empty($data['payable_amount'])) {
            $data['status'] = true;
        } else {
            $data['status'] = false;
            $data['msg'] = 'No payable amount found!';
        }

        echo json_encode($data);
        die();
    }
}
