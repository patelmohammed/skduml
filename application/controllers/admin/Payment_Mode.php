<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_Mode extends MY_Controller {

    public $menu;

    function __construct() {
        parent::__construct();

        $this->load->model('Payment_mode_model');
        $this->load->model('Common_model');

        $this->page_id = 'ACCOUNTS';
        $this->menu_id = 'PAYMENT_MODE';
    }

    //Default loading for Category system.
    public function index() {

        $data['menu_rights'] = $this->Common_model->get_menu_rights('PAYMENT_MODE', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }


        $this->Common_model->check_menu_access('PAYMENT_MODE', 'ADD', 'admin');

        $view = 'admin/payment_mode/add_payment_mode';
        $this->page_title = 'New Payment Mode';
        $this->load_admin_view($view, $data);
    }

    //Manage payment_mode form
    public function manage_payment_mode() {




        $this->Common_model->check_menu_access('PAYMENT_MODE', 'VIEW', 'admin');
        $rights['menu_rights'] = $this->Common_model->get_menu_rights('PAYMENT_MODE', 'admin');
        if (empty($rights['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }

        $data = array(
            'menu_rights' => $rights['menu_rights']
        );

        $view = 'admin/payment_mode/payment_mode';
        $this->page_title = 'Payment Mode';
        $this->load_admin_view($view, $data);
    }

    public function get_payment_mode_list() {
        $menu_rights = $this->Common_model->get_menu_rights('PAYMENT_MODE', 'admin');

        $data['groupMasterData'] = array();
        $masterData = $this->Payment_mode_model->get_payment_mode_list($this->input->post());
        $cnt = 0;
        $Payment_modeName = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $Payment_modeName = $val->payment_mode_name;

            $active_action = '';
            if ($val->active_status == 1) {
                $active_action .= '<div class="material-switch" style="text-align:center;" onclick="window.location.href=\'' . base_url() . 'admin/Payment_Mode/deactivatePayment_mode/' . $val->payment_mode_id . '\'" data-toggle="tooltip" data-placement="left" title="Active Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox" checked/>
                            <label style="border: 0px; margin: 0px;"for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> ';
            } else {
                $active_action .= '<div class="material-switch" style="text-align:center;" onclick="window.location.href=\'' . base_url() . 'admin/Payment_Mode/activatePayment_mode/' . $val->payment_mode_id . '\'" data-toggle="tooltip" data-placement="left" title="Inactive Status">
                            <input id="someSwitchOptionSuccess' . $key . '" name="someSwitchOption001" type="checkbox"/>
                            <label style="border: 0px; margin: 0px;" for="someSwitchOptionSuccess" class="label-success"></label>
                        </div> </a>';
            }

            $active_status = $active_action;

            $action_new = '';
            $action_new .= form_open();
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Payment_Mode/payment_mode_update_form/' . $val->payment_mode_id . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" title="Edit" data-toggle="tooltip" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>\'><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                if ($val->payment_mode_id != CASH_PAYMENT_METHOD_ID && $val->payment_mode_id != CHEQUE_PAYMENT_METHOD_ID && $val->payment_mode_id != PO_PAYMENT_METHOD_ID) {
                    $action_new .= '<a href="javascript:void(0);" class="DeletePayment_mode btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2" name="' . $val->payment_mode_id . '" data-toggle="tooltip" title="Delete Record" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>\'><i class="fal fa-times"></i></a>';
                }
            }
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $Payment_modeName, $active_status, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Payment_mode_model->get_payment_mode_list_search_data($this->input->post()),
            "recordsFiltered" => $this->Payment_mode_model->get_payment_mode_list_search_data($this->input->post()),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    //Insert payment_mode and upload
    public function insert_payment_mode() {

        $data = array(
            'payment_mode_name' => $this->input->post('payment_mode_name'),
            'status' => 1
        );
// echo '<pre>';
//        print_r($this->input->post());
//        echo '</pre>';
//
//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';
//        die();
        $result = $this->Payment_mode_model->payment_mode_entry($data);

        if ($result == TRUE) {
            $this->_show_message("Information has been inserted successfully!", "success");

            if (isset($_POST['add-paymentmode'])) {
                redirect(base_url('admin/Payment_Mode/manage_payment_mode'));
            } elseif (isset($_POST['add-paymentmode-another'])) {
                redirect(base_url('admin/Payment_Mode'));
            }
        } else {
            $this->_show_message("already_exists", "error");
            redirect(base_url('admin/Payment_Mode'));
        }
    }

    //Category Update Form
    public function payment_mode_update_form($payment_mode_id) {

        $data['menu_rights'] = $this->Common_model->get_menu_rights('PAYMENT_MODE', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }

        $this->Common_model->check_menu_access('PAYMENT_MODE', 'EDIT', 'admin');

        $payment_mode_detail = $this->Payment_mode_model->retrieve_payment_mode_editdata($payment_mode_id);
//  echo '<pre>';
//  print_r($payment_mode_detail);
//  die();
        $data['payment_mode_detail'] = array(
            'payment_mode_id' => $payment_mode_detail[0]['payment_mode_id'],
            'payment_mode_name' => $payment_mode_detail[0]['payment_mode_name'],
            'active_status' => $payment_mode_detail[0]['active_status'],
            'status' => $payment_mode_detail[0]['status']
        );

        $view = 'admin/payment_mode/edit_payment_mode';
        $this->page_title = 'Payment Mode Edit';
        $this->load_admin_view($view, $data);
    }

    // Category Update
    public function payment_mode_update() {

        $payment_mode_id = $this->input->post('payment_mode_id');
        $data = array(
            'payment_mode_name' => $this->input->post('payment_mode_name'),
        );

        $this->Payment_mode_model->update_payment_mode($data, $payment_mode_id);
        $this->_show_message("Information has been updated successfully!", "success");
        redirect(base_url('admin/Payment_Mode/manage_payment_mode'));
    }

    //customer active
    public function activatePayment_mode($payment_mode_id) {
        $data = array();
        $data['active_status'] = '1';
        $this->Payment_mode_model->updateStatus($data, $payment_mode_id);
        $this->_show_message("activated Payment Mode", "success");
        redirect(base_url('admin/Payment_Mode/manage_payment_mode'));
    }

    //Customer Inactive
    public function deactivatePayment_mode($payment_mode_id) {
        $data = array();
        $data['active_status'] = '0';
        $this->Payment_mode_model->updateStatus($data, $payment_mode_id);
        $this->_show_message("Inactivated Payment Mode", "success");
        redirect(base_url('admin/Payment_Mode/manage_payment_mode'));
    }

    // Category delete
    public function payment_mode_delete($id = null) {
        $chk_for_delete = 0;
        $this->Common_model->check_menu_access('PAYMENT_MODE', 'DELETE', 'admin');

        $payment_mode_id = $this->input->post('id');

        // $chk_for_delete = $this->Common_model->check_for_delete_master('payment_mode_list', 'payment_mode_id', $payment_mode_id, 1);

        if ($chk_for_delete == 0) {
            $this->Payment_mode_model->delete_payment_mode($payment_mode_id);
            $this->_show_message("Successfully Deleted Payment Mode", "success");
            $data['result'] = true;
        } else {
            $this->_show_message("You can't delete this Category! Because This used in product.", 'error_message');
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function checkPaymentmode($id = '') {

        $model = $this->input->get('payment_mode_name');

        if ($id != '') {
            if ($this->Payment_mode_model->checkExistingPaymentmode(trim($model), $id)) {
                echo 'false';
                die;
            }
        } else if ($this->Payment_mode_model->checkExistingPaymentmode(trim($model))) {
            echo 'false';
            die;
        }
        echo 'true';
        die;
    }
}
