<?php

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }

        $this->load->model('Products');
        $this->load->model('Common_model');
        $this->load->model('Suppliers');
        $this->load->model('Categories');
        $this->load->model('Units');

        $this->page_id = 'MASTERS';
        $this->menu_id = 'PRODUCT';
    }

    //Index page load
    public function index() {
        $this->Common_model->check_menu_access('PRODUCT', 'ADD', 'admin');

        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }

        $data['supplier'] = $this->Suppliers->supplier_list();
        $data['category_list'] = $this->Categories->category_list_product();
        $data['unit_list'] = $this->Units->unit_list();
        $data['tax_list'] = $this->Common_model->getTaxList();

        $this->page_title = 'Product';
        $view = 'admin/product/add_product_form';
        $this->load_admin_view($view, $data);
    }

    //Insert Product and uload
    public function insert_product() {
//        preprint($this->input->post());
        $this->Common_model->check_menu_access('PRODUCT', 'ADD', 'admin');

        $sup_price = $this->input->post('supplier_price');
        $s_id = $this->input->post('supplier_id');
        $price = $this->input->post('price');
        $taxData = $this->input->post('tax');
        $LandedCost = $this->input->post('LandedCost'); //Supplier Wise
        $OpeningStock = $this->input->post('OpeningStock'); //Supplier Wise

        $image_url = '';

        if ($_FILES['image']['name']) {
            $config['upload_path'] = 'assets/admin/uploads/products';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
            $config['max_size'] = "*";
            $config['max_width'] = "*";
            $config['max_height'] = "*";
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
                $this->_show_message($this->upload->display_errors(), "error");
                redirect(base_url('admin/Product'));
            } else {
                $image = $this->upload->data();
                $image_url = 'assets/admin/uploads/products/' . $image['file_name'];
            }
        }

        $temp_data = array();
        if (!empty($s_id) && $s_id != '') {
            foreach ($s_id as $key => $val) {
                $temp_data['supplier_id'] = $val;
                $temp_data['supplier_price'] = $sup_price[$key];
                $temp_data['LandedCost'] = $LandedCost[$key];
                $temp_data['opening_stock'] = $OpeningStock[$key];
                $supplierData[] = $temp_data;
            }
        } else {
            $supplierData = '';
        }

        $landed_cost = $this->input->post('landed_cost') != '' ? $this->input->post('landed_cost') : '0.000000';
        $opening_stock = $this->input->post('opening_stock') != '' ? $this->input->post('opening_stock') : '0.000000';
        $data = array(
            'category_id' => $this->input->post('category_id'),
            'product_name' => $this->input->post('product_name'),
            'price' => $this->input->post('sell_price'),
            'LandedCost' => $landed_cost,
            'unit' => $this->input->post('unit'),
            'opening_stock' => $opening_stock,
            'serial_no' => $this->input->post('serial_no'),
            'Product_Barcode' => $this->input->post('product_barcode'),
            'product_details' => $this->input->post('description') ? $this->input->post('description') : null,
            'image' => (isset($image_url) && !empty($image_url) ? $image_url : 'assets/admin/img/product.png'),
            'InsDateTime' => date('Y-m-d H:i:s'),
            'status' => 1,
        );
//        preprint($data);
        $result = $this->Products->product_entry($data, $taxData, $supplierData);

        if ($result == 1) {

            $this->_show_message("Information has been added successfully!", "success");
            if (isset($_POST['add-product'])) {
                redirect(base_url('admin/Product/manage_product'));
                exit;
            } elseif (isset($_POST['add-product-another'])) {
                redirect(base_url('admin/Product'));
                exit;
            }
        }
    }

    //Product Update Form
    public function product_update_form($product_id) {
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT', 'admin');

        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PRODUCT', 'EDIT', 'admin');

        $this->load->model('Suppliers');
        $this->load->model('Categories');
        $this->load->model('Units');

        $data['supplier_list'] = $this->Suppliers->supplier_list();
        $data['category_list'] = $this->Categories->category_list_product();
        $data['unit_list'] = $this->Units->unit_list();
        $data['tax_list'] = $this->Common_model->getTaxList();

        $product_detail = $this->Products->retrieve_product_editdata($product_id);
        $data['product_detail'] = $product_detail[0];
        $data['tax_select'] = $this->db->select('*')
                ->from('tax_product')
                ->where('product_id', $product_id)
                ->get()
                ->result();
        $data['supplier_product_data'] = $this->Products->supplier_product_editdata($product_id);

        $view = 'admin/product/edit_product_form';
        $this->page_title = 'PRODUCT';
        $this->load_admin_view($view, $data);
    }

    // Product Update
    public function product_update() {

        $this->Common_model->check_menu_access('PRODUCT', 'EDIT', 'admin');

        // configure for upload 
        $config = array(
            'upload_path' => "assets/admin/uploads/products",
            'allowed_types' => "png|jpg|jpeg|gif|bmp|tiff",
            'overwrite' => TRUE,
            'max_size' => '0',
        );
        $image_data = array();
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('image')) {
            $image_data = $this->upload->data();
            $image_name = "assets/admin/uploads/products/" . $image_data['file_name'];
            $config['image_library'] = 'gd2';
            $config['source_image'] = $image_data['full_path']; //get original image
            $config['maintain_ratio'] = TRUE;
            $config['height'] = '*';
            $config['width'] = '*';
            $this->load->library('image_lib', $config);
            $this->image_lib->clear();
            $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()) {

                $this->_show_message($this->upload->display_errors(), "error");
                redirect(base_url('admin/Product'));
            }
        } else {
            $image_name = $this->input->post('old_image');
        }

        $product_id = $this->input->post('product_id');
        $sup_price = $this->input->post('supplier_price');
        $s_id = $this->input->post('supplier_id');
        $taxData = $this->input->post('tax');
        $LandedCost = $this->input->post('LandedCost'); //Supplier Wise
        $OpeningStock = $this->input->post('OpeningStock'); //Supplier Wise

        $temp_data = array();
        if (!empty($s_id) && $s_id != '') {
            foreach ($s_id as $key => $val) {
                $temp_data['supplier_id'] = $val;
                $temp_data['supplier_price'] = $sup_price[$key];
                $temp_data['LandedCost'] = $LandedCost[$key];
                $temp_data['opening_stock'] = $OpeningStock[$key];
                $supplierData[] = $temp_data;
            }
        } else {
            $supplierData = '';
        }

        $landed_cost = $this->input->post('landed_cost') != '' ? $this->input->post('landed_cost') : '0';
        $opening_stock = $this->input->post('opening_stock') != '' ? $this->input->post('opening_stock') : '0';

        $data = array(
            'product_name' => $this->input->post('product_name'),
            'category_id' => $this->input->post('category_id'),
            'price' => $this->input->post('sell_price'),
            'serial_no' => $this->input->post('serial_no'),
            'LandedCost' => $landed_cost,
            'opening_stock' => $opening_stock,
            'product_details' => $this->input->post('description'),
            'unit' => $this->input->post('unit'),
            'image' => $image_name,
            'Product_Barcode' => $this->input->post('product_barcode')
        );

        $result = $this->Products->update_product($data, $product_id, $supplierData, $taxData);

        if ($result == true) {
            $this->_show_message("Information has been updated successfully!", "success");
            redirect(base_url('admin/Product/manage_product'));
        } else {
            redirect(base_url('admin/Product/manage_product'));
        }
    }

    //Manage Product
    public function manage_product() {

        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PRODUCT', 'VIEW', 'admin');

        $view = 'admin/product/product';
        $this->page_title = 'PRODUCT';
        $this->load_admin_view($view, $data);
    }

    public function get_product_list($product_id = '') {
        $menu_rights = $this->Common_model->get_menu_rights('PRODUCT', 'admin');

        $data['groupMasterData'] = array();
        $masterData = $this->Products->get_product_list($this->input->post(), $product_id);
        $cnt = 0;
        $ProductName = $CategoryName = $ProductGroupName = $SupplierName = $Price = $SLandedCost = $SupplierPrice = $SOpeningStock = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $ProductName = $val->product_name;
            $CategoryName = $val->category_name;
            $SupplierName = $val->supplier_name;
            $Price = $val->price;
            $SLandedCost = $val->s_LandedCost;
            $SupplierPrice = $val->supplier_price;
            $SOpeningStock = $val->s_opening_stock;

            $action_new = '';
            $action_new .= form_open();
            $action_new .= '<div style="display:flex;flex-wrap:nowrap;gap:5px;justify-content:center;">';

            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Product/product_update_form/' . $val->id . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" title="Edit" data-toggle="tooltip" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>\'><i class="fal fa-edit"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="deleteProduct btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2" name="' . $val->id . '" data-toggle="tooltip" title="Delete Record" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>\'><i class="fal fa-times"></i></a>';
            }

//            $action_new .= '<a href="javascript:void(0);" class="generateBarcode btn btn-icon btn-sm hover-effect-dot btn-outline-warning mr-2" name="' . $val->id . '" data-toggle="tooltip" title="Generate Barcode" data-template=\'<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-warning-500"></div></div>\'><i class="fal fa-barcode"></i></a>';
            $action_new .= '</div>';
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $ProductName, $CategoryName, $SupplierName, $Price, $SLandedCost, $SupplierPrice, $SOpeningStock, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Products->get_product_list_search_data($this->input->post(), $product_id),
            "recordsFiltered" => $this->Products->get_product_list_search_data($this->input->post(), $product_id),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    // product_delete
    public function product_delete() {
        $this->Common_model->check_menu_access('PRODUCT', 'DELETE', 'admin');

        $product_id = $this->input->post('id');
        $chk_for_delete = 0;
        $chk_for_delete += $this->Common_model->check_for_delete_master('purchase_invoice_item', 'refProductID', $product_id, 1);
        $chk_for_delete += $this->Common_model->check_for_delete_master('sale_invoice_item', 'refProductID', $product_id, 1);
        
        $data = [];
        if ($chk_for_delete == 0) {
            $result = $this->Products->delete_product($product_id);
            $data['result'] = true;
            $data['msg'] = "Information has been deleted successfully!";
        } else {
            $data['result'] = false;
            $data['msg'] = "You can't delete this product! Because This used in purchase or sale.";
        }
        echo json_encode($data);
        die;
    }

    //Retrieve Single Item  By Search
    public function product_by_search() {
//        $CI = & get_instance();
//        $this->auth->check_admin_auth();
//        $CI->load->library('lproduct');
//        $product_id = $this->input->post('product_id');
//
//        $content = $CI->lproduct->product_search_list($product_id);
//        $this->template->full_admin_html_view($content);
    }

    //Retrieve Single Item  By Search
    public function product_details($product_id) {
//        $this->product_id = $product_id;
//        $CI = & get_instance();
//        $this->auth->check_admin_auth();
//        $CI->load->library('lproduct');
//        $content = $CI->lproduct->product_details($product_id);
//        $this->template->full_admin_html_view($content);
    }

    //Retrieve Single Item  By Search
    public function product_sales_supplier_rate($product_id = null, $startdate = null, $enddate = null) {
//        if ($startdate == null) {
//            $startdate = date('Y-m-d', strtotime('-30 days'));
//        }
//        if ($enddate == null) {
//            $enddate = date('Y-m-d');
//        }
//        $product_id_input = $this->input->post('product_id');
//        if (!empty($product_id_input)) {
//            $product_id = $this->input->post('product_id');
//            $startdate = $this->input->post('from_date');
//            $enddate = $this->input->post('to_date');
//        }
//
//        $this->product_id = $product_id;
//
//        $CI = & get_instance();
//        $this->auth->check_admin_auth();
//        $CI->load->library('lproduct');
//        $content = $CI->lproduct->product_sales_supplier_rate($product_id, $startdate, $enddate);
//        $this->template->full_admin_html_view($content);
    }

    public function generateBarcode() {
        if ($this->input->get()) {
            $product_id = $this->input->get('pid');
            $user_qty = $this->input->get('qty');
            $colum_cnt = $this->input->get('per_row');

            if ($colum_cnt == 4) {
                $perpage = 12;
                $per_row = 4;
                $top_margin = 2;
                $side_margin = -4;
                $label_h = 11;
                $label_w = 30;
                $vertical_pitch = 36;
                $horizontal_pitch = 66;
                $v_label_gap = 14;
                $h_label_gap = 22;
            } else {
                $perpage = 8;
                $per_row = 3;
                $top_margin = 3;
                $side_margin = 6;
                $label_h = 25;
                $label_w = 55;
                $vertical_pitch = 36;
                $horizontal_pitch = 66;
                $v_label_gap = 13;
                $h_label_gap = 15;
            }

            $html2pdf = new Html2Pdf('P', 'A4', 'en');
            $html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->pdf->SetCreator('Weborative');
            $html2pdf->pdf->SetAuthor('Weborative');
            $html2pdf->pdf->SetTitle('Bar Code Labels');
            $html2pdf->pdf->SetSubject('');
            $html2pdf->pdf->SetKeywords('');
            $html2pdf->pdf->setPrintHeader(false);
            $html2pdf->pdf->setPrintFooter(false);
            $html2pdf->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            // set margins
            $html2pdf->pdf->setTopMargin($top_margin);
            $html2pdf->pdf->SetLeftMargin($side_margin);
            $html2pdf->pdf->SetRightMargin($side_margin);
            $html2pdf->pdf->setHeaderMargin($top_margin);
            $html2pdf->pdf->SetFooterMargin($top_margin); //1mm
            // set auto page breaks
            $html2pdf->pdf->SetAutoPageBreak(FALSE, $top_margin);
            // set image scale factor
            $html2pdf->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            // set font
            $html2pdf->pdf->SetFont('helvetica', '', 11);
            // add a page
            $html2pdf->pdf->AddPage('P', 'A4');
            $html2pdf->pdf->SetFont('helvetica', '', 10);

            // define barcode style
            $style = array(
                'position' => '',
                'align' => 'C',
                'stretch' => false,
                'fitwidth' => false,
                'cellfitalign' => '',
                'border' => FALSE,
                'hpadding' => 'auto',
                'vpadding' => 'auto',
                'fgcolor' => array(0, 0, 0),
                'bgcolor' => false, //array(255,255,255),
                'text' => true,
                'font' => 'helvetica',
                'fontsize' => 8,
                'stretchtext' => 0
            );

            //20 mm label width
            //48 mm label height
            //2 mm gap between

            $product_data = $this->Products->getProductById($product_id);
            $item = $product_data->Product_Barcode; //$this->input->post('fix_text');
            $maxLen = 8; //strlen($this->input->post('start_no'));
            $start = 1; //$this->input->post('start_no');
            $end_no = $user_qty; //$this->input->post('end_no');
            //$qty = $perpage * $per_row;
            $qty = (int) $end_no - (int) $start;
            //preprint($qty);

            $counter = 1;
            $total_label_in_sheet = $perpage * $per_row;
            $label_counter = 1;

            for ($i = 0; $i <= $qty; $i++) {
                $x = $html2pdf->pdf->GetX();
                $y = $html2pdf->pdf->GetY();
                $html2pdf->pdf->setCellMargins(0, 0, $h_label_gap, 0);
                // The width is set to the the same as the cell containing the name.

                $html2pdf->pdf->write1DBarcode($item, 'C128', $x, $y, 63.5, 14, 0.4, $style, 'L');

                $html2pdf->pdf->SetXY($x + 10, $y + 5);
                $html2pdf->pdf->Cell(63.5, 25, $product_data->serial_no, 0, 0, 'L', FALSE, '', 0, FALSE, 'C', 'B');

//                $html2pdf->pdf->SetXY($x + 10, $y + 5);
//                $html2pdf->pdf->Cell(63.5, 33, '00' . (int) $product_data->price . '00', 0, 0, 'L', FALSE, '', 0, FALSE, 'C', 'B');
                //Reset X,Y so wrapping cell wraps around the barcode's cell.
                $html2pdf->pdf->SetXY($x, $y);
                $html2pdf->pdf->Cell($label_w, 0, '', 0, 0, 'L', FALSE, '', 0, FALSE, 'C', 'B');

                if ($counter == $per_row) {
                    $counter = 1;
                    if ($total_label_in_sheet == $label_counter) {
//                    preprint($counter);
                        if (($i + 1) < $qty) {
                            $html2pdf->pdf->AddPage('P', 'A4'); //$this->input->post('page_size'));
                            $html2pdf->pdf->SetFont('helvetica', '', 10);
                            $label_counter = 1;
                        }
                    } else {
                        $html2pdf->pdf->Ln($label_h + $v_label_gap);
                        $label_counter++;
                    }
                } else {
                    $counter++;
                    $label_counter++;
                }
            }
            $date = date("Y_m_d H_i_s");
            $pdfName = 'pdf/barcodeprinting_' . str_replace(' ', '', $date) . '.pdf';
            ob_end_clean();
            $html2pdf->Output($pdfName, 'I');
            $this->output_json(["res" => "success", "msg" => base_url($pdfName)]);
        } else {
            $this->output_json(["res" => "error", "msg" => "Wrong Method"]);
        }
    }

    public function checkProduct($id = '') {

        $model = $this->input->get('product_name');

        if ($id != '') {
            if ($this->Products->checkExistingProduct(trim($model), $id)) {
                echo 'false';
                die;
            }
        } else if ($this->Products->checkExistingProduct(trim($model))) {
            echo 'false';
            die;
        }
        echo 'true';
        die;
    }

    public function checkProductBarcode($id = '') {

        $model = $this->input->get('product_barcode');

        if ($id != '') {
            if ($this->Products->checkExistingProductBarcode(trim($model), $id)) {
                echo 'false';
                die;
            }
        } else if ($this->Products->checkExistingProductBarcode(trim($model))) {
            echo 'false';
            die;
        }
        echo 'true';
        die;
    }
}
