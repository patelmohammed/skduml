<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Sale_invoice extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Web_settings');
        $this->load->model('Sale_invoice_model');
        $this->load->model('Common_model');
        $this->page_id = 'SALES';
        $this->menu_id = 'SALE_INVOICE';
    }

    public function index() {
        $this->page_title = 'GST Sale Invoice';
        $data = array();

        $this->Common_model->check_menu_access('SALE_INVOICE', 'VIEW', 'admin');
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SALE_INVOICE', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('Unauthorized');
        }

        $data['employee_list'] = $this->Common_model->get_all_users();

        $code = $this->Common_model->getCompanyCode();

        $data['company_code'] = strtoupper($code);

        $data['last_inserted_sale_invoice_id'] = $this->session->flashdata('last_inserted_sale_invoice_id');

        $view = 'admin/sale_invoice/sale_invoice_detail';
        $this->load_admin_view($view, $data);
    }

    public function get_sale_invoice_list() {
        $menu_rights = $this->Common_model->get_menu_rights('SALE_INVOICE', 'admin');

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);

        $data['groupMasterData'] = array();
        $masterData = $this->Sale_invoice_model->get_sale_invoice_list($this->input->post(), 1);
        $cnt = 0;
        $DateTime = $SINO = $CustomerName = $ProductCount = $GrandTotal = $CreatedBy = $UpdatedBy = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $DateTime = $val->DateTime;
            $SINO = $val->SINO;
            $CustomerName = $val->company__name;
            $ProductCount = $val->ProductCount;
            $GrandTotal = $val->GrandTotal;
            $CreatedBy = $val->created_by;
            $UpdatedBy = $val->updated_by;

            $action_new = '';
            $action_new .= form_open();
            $action_new .= '<div style="display: flex; gap: 5px; flex-wrap: nowrap; justify-content: center;">';
            $action_new .= '<a href="' . base_url() . 'admin/Sale_invoice/saleInvoiceDetails/' . $val->SIID . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-info mr-2" data-toggle="tooltip" data-template="<div class=&quot;tooltip&quot; role=&quot;tooltip&quot;><div class=&quot;tooltip-inner bg-info-500&quot;></div></div>" title="View"><i class="fal fa-eye" aria-hidden="true"></i></a>';
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Sale_invoice/saleInvoiceUpdateForm/' . $val->SIID . '" class="btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2" data-toggle="tooltip" data-template="<div class=&quot;tooltip&quot; role=&quot;tooltip&quot;><div class=&quot;tooltip-inner bg-primary-500&quot;></div></div>" title="Update"><i class="fal fa-edit" aria-hidden="true"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="deleteSaleInvoice btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2" name="' . $val->SIID . '" data-toggle="tooltip" data-template="<div class=&quot;tooltip&quot; role=&quot;tooltip&quot;><div class=&quot;tooltip-inner bg-danger-500&quot;></div></div>" title="" data-original-title="Delete"><i class="fal fa-times" aria-hidden="true"></i></a>';
            }
            $action_new .= '<a href="' . base_url() . 'admin/Sale_invoice/printPdfSaleInvoice/' . $val->SIID . '" target="_blank" class="btn btn-icon btn-sm hover-effect-dot btn-outline-warning" data-toggle="tooltip" data-template="<div class=&quot;tooltip&quot; role=&quot;tooltip&quot;><div class=&quot;tooltip-inner bg-warning-500&quot;></div></div>" title="Print"><i class="fal fa-barcode" aria-hidden="true"></i></a>';
            $action_new .= '</div>';
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $DateTime, $SINO, $CustomerName, $ProductCount, $GrandTotal, $CreatedBy, $UpdatedBy, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Sale_invoice_model->get_sale_invoice_list_search_data($this->input->post(), 1),
            "recordsFiltered" => $this->Sale_invoice_model->get_sale_invoice_list_search_data($this->input->post(), 1),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    public function addSaleInvoice() {
        $this->page_title = 'Add GST Sale Invoice';
        $data = array();

        $this->Common_model->check_menu_access('SALE_INVOICE', 'ADD', 'admin');

        $ref_no = $this->Sale_invoice_model->generateSaleInvoiceNo();
        $SINO = COMPANY_SHORT_NAME . '-1';

        if (isset($ref_no) && !empty($ref_no)) {
            $tmp_no = substr($ref_no, (strpos($ref_no, '-') + 1));
            $SINO = COMPANY_SHORT_NAME . '-' . sprintf('%d', $tmp_no + 1);
        }

        $data['sale_invoice_no'] = $SINO;
        $data['customer_list'] = $this->Sale_invoice_model->get_customer_list();
        $data['product_list'] = $this->Sale_invoice_model->get_product_list_with_so();
        $data['payment_mode_list'] = $this->Common_model->getPaymentModeList();
        $data['bank_list'] = $this->Common_model->getBankList();

        $code = $this->Common_model->getCompanyCode();
        $data['company_code'] = strtoupper($code);

        $view = 'admin/sale_invoice/add_sale_invoice';
        $this->load_admin_view($view, $data);
    }

    public function insertSaleInvoice() {
        $invoice_data = $transaction_data = $cheque_manager_data = $customer_ledger = array();

//        preprint($this->input->post());

        $SINO = $this->input->post('sale_invoice_no');
        $invoice_data['SINO'] = $SINO;
        $invoice_data['InvoiceType'] = 1;

        $payment_status = $this->input->post('payment_status') ? $this->input->post('payment_status') : null;
        $invoice_data['is_paid'] = ($payment_status == 'yes' ? 1 : 0);

        $DateTime = $this->input->post('invoice_date') ? date("Y-m-d", strtotime($this->input->post('invoice_date'))) . " " . date('H:i:s') : date("Y-m-d H:i:s");
        $invoice_data['DateTime'] = $DateTime;

        $RefCustomerID = $this->input->post('customer_id');
        $invoice_data['RefCustomerID'] = $RefCustomerID;
        $invoice_data['RefCustomerAddress'] = $this->input->post('hidden_customer_address') ? $this->input->post('hidden_customer_address') : null;
        $invoice_data['RefCustomerPhone'] = $this->input->post('hidden_customer_phonenumber') ? $this->input->post('hidden_customer_phonenumber') : null;
        $invoice_data['RefCustomerEmail'] = $this->input->post('hidden_customer_email') ? $this->input->post('hidden_customer_email') : null;

        $unit_qua = $this->input->post('BasicTotal');
        if (isset($unit_qua) && !empty($unit_qua)) {
            $BasicTotal = 0;
            foreach ($unit_qua as $key => $total) {
                $BasicTotal += $total;
            }
            $invoice_data['BasicTotal'] = $BasicTotal;
        }

        $discount = $this->input->post('discount_ind');
        if (isset($discount) && !empty($discount)) {
            $Discount = 0;
            foreach ($discount as $key => $disc) {
                $disc = isset($disc) && !empty($disc) ? $disc : 0;
                $Discount += $disc;
            }
            $invoice_data['DiscountTotal'] = $Discount;
        } else {
            $invoice_data['DiscountTotal'] = '';
        }

        $tax = $this->input->post('total_tax');
        if (isset($tax) && !empty($tax)) {
            $Taxtotal = 0;
            foreach ($tax as $key => $ta) {
                $Taxtotal += $ta;
            }
            $invoice_data['Taxtotal'] = $Taxtotal;
        } else {
            $invoice_data['Taxtotal'] = '';
        }

        $invoice_data['FrightCharges'] = $this->input->post('fright_charges') ? $this->input->post('fright_charges') : 0;
        $invoice_data['RoundUpAmt'] = $this->input->post('round_up_amt') ? $this->input->post('round_up_amt') : 0;

        $GrandTotal = $this->input->post('g_total');
        $invoice_data['GrandTotal'] = $GrandTotal;

        $remarks = $this->input->post('sale_invoice_remark') ? $this->input->post('sale_invoice_remark') : null;
        $invoice_data['Remarks'] = $remarks;

        $invoice_data['InsUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        $invoice_data['InsTerminal'] = $this->input->ip_address();
        $invoice_data['InsDateTime'] = date('Y-m-d H:i:s');

        $chk_uniq_invoice = $this->Common_model->chkUniqueCode('sale_invoice', 'SINO', $SINO, 1, '', '', 'DateTime', $DateTime); //('table_name',column_name for chk,'column_data',status)

        if ($chk_uniq_invoice == true) {
            $SIID = $this->Common_model->insertData('sale_invoice', $invoice_data);

            $customer_ledger['transaction_id'] = null;
            $customer_ledger['customer_id'] = $RefCustomerID;
            $customer_ledger['invoice_no'] = $SIID;
            $customer_ledger['amount'] = $GrandTotal;
            $customer_ledger['description'] = 'GST Invoice - (' . $SINO . ')';
            $customer_ledger['date'] = $DateTime;
            $customer_ledger['status'] = 1;
            $customer_ledger['d_c'] = 'c';
            $customer_ledger['ref_table_name'] = 'sale_invoice';

            $this->Common_model->insertData('customer_ledger', $customer_ledger);

            if (isset($payment_status) && !empty($payment_status) && $payment_status == 'yes') {
                $transaction_data['date_of_transaction'] = date("Y-m-d H:i:s");
                $transaction_data['invoice_no'] = $SIID;
                $transaction_data['transaction_type'] = 2;                               //1 = payment, 2 = Receipt
                $transaction_data['transaction_category'] = 2;                           //1 = Supplier, 2 = Customer
                $transaction_data['transaction_mode'] = CASH_PAYMENT_METHOD_ID;
                $transaction_data['receipt_amount'] = $GrandTotal;
                $transaction_data['description'] = $remarks;
                $transaction_data['relation_id'] = $RefCustomerID;
                $transaction_data['is_transaction'] = 1;

                $transaction_id = $this->Common_model->insertData('transaction', $transaction_data);

                $customer_ledger = [];
                $customer_ledger['transaction_id'] = isset($transaction_id) && !empty($transaction_id) ? $transaction_id : null;
                $customer_ledger['customer_id'] = $RefCustomerID;
                $customer_ledger['receipt_no'] = generator(10, 'customer_ledger', 'receipt_no');
                $customer_ledger['amount'] = $GrandTotal;
                $customer_ledger['description'] = getPaymentModeNameById(CASH_PAYMENT_METHOD_ID) . ' Payment';
                $customer_ledger['payment_type'] = CASH_PAYMENT_METHOD_ID;
                $customer_ledger['cheque_no'] = null;
                $customer_ledger['date'] = $DateTime;
                $customer_ledger['status'] = 1;
                $customer_ledger['d_c'] = 'd';

                $this->Common_model->insertData('customer_ledger', $customer_ledger);
            }

            $product = $this->input->post('product_id');
            $result = $this->Sale_invoice_model->insert_product($product, $SIID);
        } else {
            $this->session->set_userdata(array('error_message' => "This Sale Invoice already exist"));
            redirect(base_url('admin/Sale_invoice'));
        }

        if ($result == 1) {
            $this->session->set_userdata(array('message' => 'Sale invoice created successfully.'));
            if (isset($_POST['add-sale-invoice'])) {
                $this->session->set_flashdata('last_inserted_sale_invoice_id', $SIID);
                redirect(base_url('admin/Sale_invoice'));
                exit;
            } elseif (isset($_POST['add-sale-invoice-another'])) {
                redirect(base_url('admin/Sale_invoice/addSaleInvoice'));
                exit;
            } else {
                $this->session->set_flashdata('last_inserted_sale_invoice_id', $SIID);
                redirect(base_url('admin/Sale_invoice'));
                exit;
            }
        } else {
            $this->session->set_userdata(array('error_message' => "This Sale Invoice already exist"));
            redirect(base_url('admin/Sale_invoice'));
        }
    }

    public function saleInvoiceUpdateForm($SIID) {
        $this->page_title = 'Update GST Sale Invoice';
        $data = array();

        $this->Common_model->check_menu_access('SALE_INVOICE', 'EDIT', 'admin');
        $code = $this->Common_model->getCompanyCode();

        $invoice_sale_list = $this->Sale_invoice_model->invoice_purchase_list($SIID);
        $payment_data = $this->Sale_invoice_model->getSalesInvoicePaymentDataById($SIID);

        $data = array(
            'title' => 'Edit Sale Invoice',
            'SIID' => $SIID,
            'sale_invoice' => $this->Sale_invoice_model->product_edit_data($SIID),
            'invoice_sale_list' => $invoice_sale_list,
            'payment_data' => $payment_data,
            'supplier' => $this->Sale_invoice_model->get_customer_list(),
            'product_list' => $this->Sale_invoice_model->get_product_list_with_so(),
            'customer_list' => $this->Sale_invoice_model->get_customer_list(),
            'payment_mode_list' => $this->Common_model->getPaymentModeList(),
            'bank_list' => $this->Common_model->getBankList(),
            'company_code' => strtoupper($code)
        );

        $view = 'admin/sale_invoice/edit_sale_invoice';
        $this->load_admin_view($view, $data);
    }

    public function updateSaleInvoice($SIID) {
        $invoice_data = $transaction_data = $cheque_manager_data = $customer_ledger = array();

//        preprint($this->input->post());

        $SINO = $this->input->post('sale_invoice_no');
        $invoice_data['SINO'] = $SINO;
        $invoice_data['InvoiceType'] = 1;

        $payment_status = $this->input->post('payment_status') ? $this->input->post('payment_status') : null;

        $DateTime = $this->input->post('invoice_date') ? date("Y-m-d", strtotime($this->input->post('invoice_date'))) . " " . date('H:i:s') : date("Y-m-d H:i:s");
        $invoice_data['DateTime'] = $DateTime;

        $RefCustomerID = $this->input->post('customer_id');
        $invoice_data['RefCustomerID'] = $RefCustomerID;
        $invoice_data['RefCustomerAddress'] = $this->input->post('hidden_customer_address') ? $this->input->post('hidden_customer_address') : null;
        $invoice_data['RefCustomerPhone'] = $this->input->post('hidden_customer_phonenumber') ? $this->input->post('hidden_customer_phonenumber') : null;
        $invoice_data['RefCustomerEmail'] = $this->input->post('hidden_customer_email') ? $this->input->post('hidden_customer_email') : null;

        $unit_qua = $this->input->post('BasicTotal');
        if (isset($unit_qua) && !empty($unit_qua)) {
            $BasicTotal = 0;
            foreach ($unit_qua as $key => $total) {
                $BasicTotal += $total;
            }
            $invoice_data['BasicTotal'] = $BasicTotal;
        }

        $discount = $this->input->post('discount_ind');
        if (isset($discount) && !empty($discount)) {
            $Discount = 0;
            foreach ($discount as $key => $disc) {
                $disc = isset($disc) && !empty($disc) ? $disc : 0;
                $Discount += $disc;
            }
            $invoice_data['DiscountTotal'] = $Discount;
        } else {
            $invoice_data['DiscountTotal'] = '';
        }

        $tax = $this->input->post('total_tax');
        if (isset($tax) && !empty($tax)) {
            $Taxtotal = 0;
            foreach ($tax as $key => $ta) {
                $Taxtotal += $ta;
            }
            $invoice_data['Taxtotal'] = $Taxtotal;
        } else {
            $invoice_data['Taxtotal'] = '';
        }

        $invoice_data['FrightCharges'] = $this->input->post('fright_charges') ? $this->input->post('fright_charges') : 0;
        $invoice_data['RoundUpAmt'] = $this->input->post('round_up_amt') ? $this->input->post('round_up_amt') : 0;

        $GrandTotal = $this->input->post('g_total');
        $invoice_data['GrandTotal'] = $GrandTotal;

        $remarks = $this->input->post('sale_invoice_remark') ? $this->input->post('sale_invoice_remark') : null;
        $invoice_data['Remarks'] = $remarks;

        $invoice_data['UpdUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        ;
        $invoice_data['UpdTerminal'] = $this->input->ip_address();
        $invoice_data['UpdDateTime'] = date('Y-m-d H:i:s');

        if (isset($SIID) && !empty($SIID)) {
            $this->Common_model->updateData('sale_invoice', 'SIID', $SIID, $invoice_data);

            $customer_ledger['customer_id'] = $RefCustomerID;
            $customer_ledger['amount'] = $GrandTotal;
            $customer_ledger['date'] = $DateTime;
            $this->Common_model->updateData2('customer_ledger', 'invoice_no', $SIID, 'ref_table_name', 'sale_invoice', $customer_ledger);

            if (isset($payment_status) && !empty($payment_status) && $payment_status == 1) {
                $transaction_id = $this->input->post('transaction_id');

                $transaction_data['date_of_transaction'] = date("Y-m-d H:i:s");
                $transaction_data['receipt_amount'] = $GrandTotal;
                $transaction_data['description'] = $remarks;
                $transaction_data['relation_id'] = $RefCustomerID;

                $this->Common_model->updateData('transaction', 'transaction_id', $transaction_id, $transaction_data);

                $customer_ledger = [];
                $customer_ledger['customer_id'] = $RefCustomerID;
                $customer_ledger['amount'] = $GrandTotal;
                $customer_ledger['date'] = $DateTime;

                $this->Common_model->updateData2('customer_ledger', 'transaction_id', $transaction_id, '', '', $customer_ledger);
            }

            $this->Common_model->deleteData('sale_invoice_item', 'refSIID', $SIID);
            $this->Common_model->deleteData('sale_invoice_itemtaxdet', 'refSIID', $SIID);

            $product = $this->input->post('product_id');
            $result = $this->Sale_invoice_model->insert_product($product, $SIID);
        } else {
            $this->session->set_userdata(array('error_message' => "Sale invoice data not found!"));
            redirect(base_url('admin/Sale_invoice'));
        }

        if ($result == 1) {
            $this->session->set_userdata(array('message' => 'Sale invoice updated successfully.'));
            if (isset($_POST['add-sale-invoice'])) {
                redirect(base_url('admin/Sale_invoice'));
                exit;
            } elseif (isset($_POST['add-sale-invoice-another'])) {
                redirect(base_url('admin/Sale_invoice/addSaleInvoice'));
                exit;
            } else {
                redirect(base_url('admin/Sale_invoice'));
                exit;
            }
        } else {
            $this->session->set_userdata(array('error_message' => 'Something went wrong, Try again!'));
            redirect(base_url('admin/Sale_invoice'));
        }
    }

    public function saleInvoiceDelete() {
        $this->page_title = 'Delete GST Sale Invoice';
        $this->Common_model->check_menu_access('SALE_INVOICE', 'DELETE', 'admin');

        $sale_invoice_id = $_POST['sale_invoice_id'];

        if (isset($sale_invoice_id) && !empty($sale_invoice_id)) {
            if ($this->Sale_invoice_model->delete_purchase_invoice($sale_invoice_id)) {
                if ($this->Common_model->deleteUpdate3('customer_ledger', 'Deleted', 'invoice_no', $sale_invoice_id, 'ref_table_name', 'sale_invoice')) {
                    $this->session->set_userdata(array('message' => 'Sale invoice deleted successfully.'));
                    $response['status'] = true;
                } else {
                    $this->session->set_userdata(array('error_message' => 'Something went wrong, Try again!'));
                    $response['status'] = false;
                }
            } else {
                $this->session->set_userdata(array('error_message' => 'Data not found, Try again!'));
                $response['status'] = false;
            }
        } else {
            $this->session->set_userdata(array('error_message' => 'Something went wrong, Try again!'));
            $response['status'] = false;
        }
        return TRUE;
    }

    public function printSaleInvoicePdf() {
        $data = array();

        $SIID = $this->input->post('sale_invoice_id');
        $data['sale_invoice_data'] = $this->Sale_invoice_model->product_edit_data($SIID);
        $data['sale_invoice_item_data'] = $this->Sale_invoice_model->purchase_invoice_list_for_print($SIID);
        $data['company_detail'] = get_company_detail();
        $data['tax_detail'] = $this->Sale_invoice_model->invoice_tax_detail_for_print($SIID);
        $data['customer_detail'] = $this->Common_model->getCustomerDataByid($data['sale_invoice_data']['RefCustomerID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['company_code'] = $company_code;
        $data['CurrentUser'] = $this->db->where("user_id", $data['sale_invoice_data']['InsUser'])->get('users')->row_array();
//        $data['bank_data'] = $this->Common_model->getBankDetails();

        $content = $this->parser->parse('sale_invoice/ajax_pdf', $data, true);
        echo $content;
        die;
    }

    public function getProductInformationByProductId() {
        if ($this->input->post()) {
            $product_id = $this->input->post('product_id');
            $data['product_information'] = $this->Sale_invoice_model->getProductInformationByProductId($product_id);

            $tax_data = $this->Common_model->getTaxList();
            $tax_opt = '<option value="" selected="" disabled="">-- Select Tax --</option>';
            if (!empty($tax_data)) {
                foreach ($tax_data as $tk => $tv) {
                    $tax_opt .= '<option value="' . $tv->tax_id . '">' . $tv->tax_name . ' - (' . $tv->tax . '%)</option>';
                }
            }
            $data['tax_options'] = $tax_opt;

            $data['result'] = true;

            echo json_encode($data);
            die;
        } else {
            $data['result'] = false;
            echo json_encode($data);
            die;
        }
    }

    public function printPdfSaleInvoice($SIID) {
        $data = array();

        $data['sale_invoice_data'] = $this->Sale_invoice_model->product_edit_data($SIID);
        $data['sale_invoice_item_data'] = $this->Sale_invoice_model->purchase_invoice_list_for_print($SIID);
        $data['customer_detail'] = $this->Common_model->getCustomerDataByid($data['sale_invoice_data']['RefCustomerID']);
        $data['company_detail'] = get_company_detail();

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['company_code'] = $company_code;
        $data['CurrentUser'] = $this->db->where("id", $data['sale_invoice_data']['InsUser'])->get('user_information')->row_array();

        if (!empty($data)) {
            try {
                $pdf_data = '';
                $pdfhtml = $this->parser->parse('admin/pdf/Sale_invoice', $data, true);
                $pdf_data .= str_replace('%%File_type%%', 'ORIGINAL', $pdfhtml);
                $pdf_data .= str_replace('%%File_type%%', 'DUPLICATE', $pdfhtml);
                $pdfName = cleanString($data['sale_invoice_data']['SINO']) . '.pdf';
                $html2pdf = new Html2Pdf('L', 'A5', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdf_data);
                $html2pdf->output($pdfName, 'I');
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
    }

    public function getSaleOutwardDataBySOID() {
        $product_id = $this->input->post('product_id');
        $customer_id = $this->input->post('customer_id');
        $SOID = $this->input->post('sale_outward_no');

        $data['sale_outward_detail'] = $this->Sale_invoice_model->getSaleOutwardDataBySOID($SOID, $product_id, $customer_id);
        if (isset($data['sale_outward_detail']) && !empty($data['sale_outward_detail'])) {
            $data['result'] = true;
            echo json_encode($data);
            die;
        } else {
            $data['result'] = false;
            echo json_encode($data);
            die;
        }
    }

    public function mailSaleInvoiceDetail() {
        $data = $attachment = $mail_data = array();
        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $SIID = $this->input->post('SIID');
        $mail_to = $this->input->post('mail_to');
        $mail_subject = $this->input->post('mail_subject');
        $mail_body = $this->input->post('mail_body');
        $send_me = $this->input->post('send_me');

        $data['subject'] = !empty($mail_subject) ? $mail_subject : 'Sale Invoice Detail';
        $data['body'] = !empty($mail_body) ? $mail_body : 'Sale Invoice Data';

        $mail_to_new = implode(', ', $mail_to);
        $mail_data['email_id'] = $mail_to_new;
        if ($send_me == 'true') {
            $mail_to_new .= ', ' . $this->session->userdata('user_email');
            $mail_data['send_me'] = $this->session->userdata('user_email');
        }

        $data['sale_invoice'] = $this->Sale_invoice_model->product_edit_data($SIID);
        $data['sale_invoice_list'] = $this->Sale_invoice_model->purchase_invoice_list_for_print($SIID);
        $data['company_detail'] = get_company_detail();
        $data['tax_detail'] = $this->Sale_invoice_model->invoice_tax_detail_for_print($SIID);
        $data['customer_detail'] = $this->Common_model->getCustomerDataByid($data['sale_invoice']['RefCustomerID']);

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);
        $data['sale_invoice']['SINO'] = 'SI-' . $company_code . "-" . sprintf('%06d', $data['sale_invoice']['SINO']);
        $data['company_code'] = $company_code;

        if (!empty($data)) {
            try {
                $new_path = 'my-assets/documents/pdf/SaleInvoice/';
                if (!is_dir($new_path)) {
                    if (!mkdir($new_path, 0777, true)) {
                        die('Not Created');
                    }
                }
                $pdfhtml = $this->parser->parse('pdf/Sale_invoice', $data, true);
                $pdfName = $new_path . cleanString($data['sale_invoice']['SINO']) . '_' . date('H_i_s') . '.pdf';
                $attachment = array(getcwd() . '/' . $pdfName);
                $html2pdf = new Html2Pdf('P', 'A4', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($pdfhtml);
                $html2pdf->output(getcwd() . '/' . $pdfName, 'F');
                $attachment[] = getcwd() . '/' . $pdfName;
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }

        $data['login_url'] = 'google.com';
        $data['company'] = get_company_detail();
        $company_name = $data['company']['company_name'];

        if (!empty($data)) {
            try {
                $from_email = "inquiry@foodmohalla.in";
                $this->email->from($from_email, $company_name);
                $this->email->subject($data['subject']);
                $this->email->message($data['body']);
                $this->email->attach($pdfName);
                if (!empty($mail_to_new)) {
                    $this->email->to($mail_to_new);
                    if ($this->email->send()) {
                        $mail_data['refTableName'] = 'sale_invoice';
                        $mail_data['refTableId'] = $SIID;
                        $mail_data['attachments'] = $pdfName;
                        $mail_data['mailSubject'] = $data['subject'];
                        $mail_data['mailBody'] = $data['body'];
                        $mail_data['InsUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
                        ;
                        $mail_data['InsTerminal'] = $this->input->ip_address();
                        $mail_data['InsDateTime'] = date('Y/m/d H:i:s');
                        $this->Common_model->insertData('mailhistory', $mail_data);

                        $data['result'] = true;
                    }
                }
            } catch (Html2PdfException $e) {
                $data = [];
                $data['result'] = '<h1>Error In Attachment Generation, Try Again</h1>';
                echo json_encode($data);
                die;
            }
        }
        echo json_encode($data);
        die;
    }

    public function checkSaleInvoiceNo() {
        $sale_invoice_id = $this->input->post('sale_invoice_id');
        $sale_invoice_no = $this->input->post('sale_invoice_no');

        if (isset($sale_invoice_id) && !empty($sale_invoice_id)) {
            $current_invoice_no = $this->Sale_invoice_model->getSaleInvoiceNo($sale_invoice_id);
            $response = $this->Sale_invoice_model->checkSaleInvoiceNo($sale_invoice_no, $current_invoice_no);
        } else {
            $response = $this->Sale_invoice_model->checkSaleInvoiceNo($sale_invoice_no);
        }

        if (!$response) {
            echo "false";
        } else {
            echo "true";
        }
        die();
    }

    public function manageNonGSTSaleInvoice() {
        $this->page_title = 'Non GST Sale Invoice';
        $data = array();

        $this->menu_id = 'NON_GST_SALE_INVOICE';

        $this->Common_model->check_menu_access('NON_GST_SALE_INVOICE', 'VIEW', 'admin');
        $data['menu_rights'] = $this->Common_model->get_menu_rights('NON_GST_SALE_INVOICE', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('Unauthorized');
        }

        $data['employee_list'] = $this->Common_model->get_all_users();

        $code = $this->Common_model->getCompanyCode();
        $data['company_code'] = strtoupper($code);

        $sale_invoice = $this->parser->parse('sale_invoice/non_gst_sale_invoice_detail', $data, true);
        $this->template->full_admin_html_view($sale_invoice);
    }

    public function get_non_gst_sale_invoice_list() {
        $menu_rights = $this->Common_model->get_menu_rights('SALE_INVOICE', 'admin');

        $code = $this->Common_model->getCompanyCode();
        $company_code = strtoupper($code);

        $data['groupMasterData'] = array();
        $masterData = $this->Sale_invoice_model->get_sale_invoice_list($this->input->post(), 2);
        $cnt = 0;
        $DateTime = $SINO = $CustomerName = $ProductCount = $GrandTotal = $action = '';
        foreach ($masterData as $key => $val) {
            $cnt = $this->input->post('start') + $key + 1;
            $DateTime = $val->DateTime;
            $SINO = $val->SINO;
            $CustomerName = $val->company__name;
            $ProductCount = $val->ProductCount;
            $GrandTotal = $val->GrandTotal;

            $action_new = '';
            $action_new .= form_open();
            $action_new .= '<div style="display: flex; gap: 5px; flex-wrap: nowrap; justify-content: center;">';
            $action_new .= '<a href="' . base_url() . 'admin/Sale_invoice/saleInvoiceDetails/' . $val->SIID . '" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            if ($menu_rights['edit_right'] == 1) {
                $action_new .= '<a href="' . base_url() . 'admin/Sale_invoice/nonGSTSaleInvoiceUpdateForm/' . $val->SIID . '" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            }
            if ($menu_rights['delete_right'] == 1) {
                $action_new .= '<a href="javascript:void(0);" class="deleteSaleInvoice btn btn-danger btn-sm" name="' . $val->SIID . '" data-toggle="tooltip" data-placement="left" title="" data-original-title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            }
            $action_new .= '<a href="' . base_url() . 'admin/Sale_invoice/printPdfSaleInvoice/' . $val->SIID . '" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="right" title="Print"><i class="fa fa-barcode" aria-hidden="true"></i></a>';
            $action_new .= '</div>';
            $action_new .= form_close();

            $action = $action_new;
            $data['groupMasterData'][] = array($cnt, $DateTime, $SINO, $CustomerName, $ProductCount, $GrandTotal, $action);
        }

        $group_output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->Sale_invoice_model->get_sale_invoice_list_search_data($this->input->post(), 2),
            "recordsFiltered" => $this->Sale_invoice_model->get_sale_invoice_list_search_data($this->input->post(), 2),
            "data" => $data['groupMasterData'],
        );
        echo json_encode($group_output);
    }

    public function addNonGSTSaleInvoice() {
        $data = array();

        $this->menu_id = 'NON_GST_SALE_INVOICE';
        $this->page_title = 'Add Non GST Sale Invoice';

        $this->Common_model->check_menu_access('NON_GST_SALE_INVOICE', 'ADD', 'admin');

        $ref_no = $this->Sale_invoice_model->generateNonGSTSaleInvoiceNo();
        $SINO = NON_GST_INVOICE_SHORT_CODE . '-1';

        if (isset($ref_no) && !empty($ref_no)) {
            $tmp_no = substr($ref_no, (strpos($ref_no, '-') + 1));
            $SINO = NON_GST_INVOICE_SHORT_CODE . '-' . sprintf('%d', $tmp_no + 1);
        }

        $data['sale_invoice_no'] = $SINO;
        $data['customer_list'] = $this->Sale_invoice_model->get_customer_list();
        $data['product_list'] = $this->Sale_invoice_model->get_product_list_with_so();
        $data['agent_list'] = $this->Common_model->getAgentList();
        $data['commision_rate_list'] = $this->Sale_invoice_model->getCommisionRateList();
        $data['vehicle_list'] = $this->Sale_invoice_model->getVehicleList();

        $code = $this->Common_model->getCompanyCode();
        $data['company_code'] = strtoupper($code);
        $sale_invoice = $this->parser->parse('sale_invoice/add_non_gst_sale_invoice', $data, true);
        $this->template->full_admin_html_view($sale_invoice);
    }

    public function insertNonGSTSaleInvoice() {
        $invoice_data = array();

        $SINO = $this->input->post('sale_invoice_no');

        $invoice_data['SINO'] = $SINO;
        $invoice_data['InvoiceType'] = 2;
        $invoice_data['EwayBillNo'] = $this->input->post('eway_bill_no') ? $this->input->post('eway_bill_no') : null;
        $invoice_data['ReceiverName'] = $this->input->post('invoice_receievd_name') ? $this->input->post('invoice_receievd_name') : null;

        $DateTime = $this->input->post('invoice_date') ? date("Y-m-d", strtotime($this->input->post('invoice_date'))) . " " . date('H:i:s') : date("Y-m-d H:i:s");
        $invoice_data['DateTime'] = $DateTime;
        $invoice_data['DueDate'] = $this->input->post('due_date') ? date("Y-m-d", strtotime($this->input->post('due_date'))) : null;
        $invoice_data['DeliveryDate'] = $this->input->post('delivery_date') ? date("Y-m-d", strtotime($this->input->post('delivery_date'))) : null;
        $invoice_data['RefVehicleId'] = $this->input->post('vehicle_id') ? $this->input->post('vehicle_id') : null;

        $RefCustomerID = $this->input->post('customer_id');
        $invoice_data['RefCustomerID'] = $RefCustomerID;
        $invoice_data['RefCustomerAddress'] = $this->input->post('hidden_customer_address') ? $this->input->post('hidden_customer_address') : null;
        $invoice_data['RefCustomerPhone'] = $this->input->post('hidden_customer_phonenumber') ? $this->input->post('hidden_customer_phonenumber') : null;
        $invoice_data['RefCustomerEmail'] = $this->input->post('hidden_customer_email') ? $this->input->post('hidden_customer_email') : null;

        $RefAgentId = $this->input->post('agent_id');
        $invoice_data['RefAgentId'] = isset($RefAgentId) && !empty($RefAgentId) ? $RefAgentId : null;
        $invoice_data['RefAgentAddress'] = $this->input->post('hidden_agent_address') ? $this->input->post('hidden_agent_address') : null;
        $invoice_data['RefAgentPhone'] = $this->input->post('hidden_agent_phonenumber') ? $this->input->post('hidden_agent_phonenumber') : null;
        $invoice_data['RefAgentEmail'] = $this->input->post('hidden_agent_email') ? $this->input->post('hidden_agent_email') : null;

        $invoice_data['RefCommissionRateId'] = $this->input->post('commission_rate_id') ? $this->input->post('commission_rate_id') : null;

        $commission_rate_type = $this->input->post('commission_rate_type') ? $this->input->post('commission_rate_type') : null;
        $invoice_data['RefCommissionRateType'] = $commission_rate_type;

        $commision_rate = $this->input->post('commission_rate') ? $this->input->post('commission_rate') : 0;
        $invoice_data['RefCommissionRate'] = $commision_rate;

        $commisionTotal = 0;

        $qunatity = $this->input->post('quantity_amount');
        if (isset($qunatity) && !empty($qunatity)) {
            $quantityTotal = 0;
            foreach ($qunatity as $key => $qty) {
                $quantityTotal += $qty;
            }
        }

        $unit_qua = $this->input->post('BasicTotal');
        if (isset($unit_qua) && !empty($unit_qua)) {
            $BasicTotal = 0;
            foreach ($unit_qua as $key => $total) {
                $BasicTotal += $total;
            }
            $invoice_data['BasicTotal'] = $BasicTotal;
        }

        $discount = $this->input->post('discount_ind');
        if (isset($discount) && !empty($discount)) {
            $Discount = 0;
            foreach ($discount as $key => $disc) {
                $disc = isset($disc) && !empty($disc) ? $disc : 0;
                $Discount += $disc;
            }
            $invoice_data['DiscountTotal'] = $Discount;
        } else {
            $invoice_data['DiscountTotal'] = '';
        }

        $invoice_data['Taxtotal'] = 0;

        $invoice_data['FrightCharges'] = $this->input->post('fright_charges') ? $this->input->post('fright_charges') : 0;
        $invoice_data['RoundUpAmt'] = $this->input->post('round_up_amt') ? $this->input->post('round_up_amt') : 0;

        $GrandTotal = $this->input->post('g_total');
        $invoice_data['GrandTotal'] = $GrandTotal;

        if (isset($commission_rate_type) && !empty($commission_rate_type)) {
            if ($commission_rate_type == 1) {
                $commisionTotal = $quantityTotal * $commision_rate;
            } else {
                $commisionTotal = ($GrandTotal * $commision_rate) / 100;
            }
        }

        $invoice_data['CommissionTotal'] = $commisionTotal;

        $remarks = $this->input->post('sale_invoice_remark') ? $this->input->post('sale_invoice_remark') : null;
        $invoice_data['Remarks'] = $remarks;
        $invoice_data['TermsAndConditions'] = $this->input->post('sale_invoice_tc') ? $this->input->post('sale_invoice_tc') : null;

        $invoice_data['InsUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        ;
        $invoice_data['InsTerminal'] = $this->input->ip_address();
        $invoice_data['InsDateTime'] = date('Y-m-d H:i:s');

        $chk_uniq_invoice = $this->Common_model->chkUniqueCode('sale_invoice', 'SINO', $SINO, 1, '', '', 'DateTime', $DateTime); //('table_name',column_name for chk,'column_data',status)

        if ($chk_uniq_invoice == true) {
            $SIID = $this->Common_model->insertData('sale_invoice', $invoice_data);

            $customer_ledger['transaction_id'] = null;
            $customer_ledger['customer_id'] = $RefCustomerID;
            $customer_ledger['invoice_no'] = $SIID;
            $customer_ledger['date'] = $DateTime;
            $customer_ledger['amount'] = $GrandTotal;
            $customer_ledger['description'] = 'Non GST Invoice - (' . $SINO . ')';
            $customer_ledger['status'] = 1;
            $customer_ledger['d_c'] = 'c';
            $customer_ledger['ref_table_name'] = 'sale_invoice';

            $this->Common_model->insertData('customer_ledger', $customer_ledger);

            $product = $this->input->post('product_id');
            $result = $this->Sale_invoice_model->insert_non_gst_product($product, $SIID);
        } else {
            $this->session->set_userdata(array('error_message' => "This Sale Invoice already exist"));
            redirect(base_url('Sale_invoice/manageNonGSTSaleInvoice'));
        }

        if ($result == 1) {
            $this->session->set_userdata(array('message' => display('successfully_added')));
            if (isset($_POST['add-sale-invoice'])) {
                redirect(base_url('Sale_invoice/manageNonGSTSaleInvoice'));
                exit;
            } elseif (isset($_POST['add-sale-invoice-another'])) {
                redirect(base_url('Sale_invoice/addNonGSTSaleInvoice'));
                exit;
            }
        } else {
            $this->session->set_userdata(array('error_message' => "This Sale Invoice already exist"));
            redirect(base_url('Sale_invoice/manageNonGSTSaleInvoice'));
        }
    }

    public function nonGSTSaleInvoiceUpdateForm($SIID) {
        $data = array();

        $this->Common_model->check_menu_access('SALE_INVOICE', 'EDIT', 'admin');
        $code = $this->Common_model->getCompanyCode();

        $invoice_sale_list = $this->Sale_invoice_model->invoice_purchase_list($SIID);

        if (isset($invoice_sale_list) && !empty($invoice_sale_list)) {
            foreach ($invoice_sale_list as $key => $value) {
                $invoice_sale_list[$key]->supplier_list = $this->Sale_invoice_model->getProductSupplierByStock($value->refProductID);
            }
        }

        $data = array(
            'title' => 'Edit Sale Invoice',
            'SIID' => $SIID,
            'sale_invoice' => $this->Sale_invoice_model->product_edit_data($SIID),
            'invoice_sale_list' => $invoice_sale_list,
            'supplier' => $this->Sale_invoice_model->get_customer_list(),
            'product_list' => $this->Sale_invoice_model->get_product_list_with_so(),
            'customer_list' => $this->Sale_invoice_model->get_customer_list(),
            'agent_list' => $this->Common_model->getAgentList(),
            'commision_rate_list' => $this->Sale_invoice_model->getCommisionRateList(),
            'vehicle_list' => $this->Sale_invoice_model->getVehicleList(),
            'company_code' => strtoupper($code)
        );

        $content = $this->parser->parse('sale_invoice/edit_non_gst_sale_invoice', $data, true);
        $this->template->full_admin_html_view($content);
    }

    public function updateNonGSTSaleInvoice($SIID) {
        $invoice_data = array();

        $SINO = $this->input->post('sale_invoice_no');
        $invoice_data['SINO'] = $SINO;
        $invoice_data['InvoiceType'] = 2;
        $invoice_data['EwayBillNo'] = $this->input->post('eway_bill_no') ? $this->input->post('eway_bill_no') : null;
        $invoice_data['ReceiverName'] = $this->input->post('invoice_receievd_name') ? $this->input->post('invoice_receievd_name') : null;

        $DateTime = $this->input->post('invoice_date') ? date("Y-m-d", strtotime($this->input->post('invoice_date'))) . " " . date('H:i:s') : date("Y-m-d H:i:s");
        $invoice_data['DateTime'] = $DateTime;
        $invoice_data['DueDate'] = $this->input->post('due_date') ? date("Y-m-d", strtotime($this->input->post('due_date'))) : null;
        $invoice_data['DeliveryDate'] = $this->input->post('delivery_date') ? date("Y-m-d", strtotime($this->input->post('delivery_date'))) : null;
        $invoice_data['RefVehicleId'] = $this->input->post('vehicle_id') ? $this->input->post('vehicle_id') : null;

        $RefCustomerID = $this->input->post('customer_id');
        $invoice_data['RefCustomerID'] = $RefCustomerID;
        $invoice_data['RefCustomerAddress'] = $this->input->post('hidden_customer_address') ? $this->input->post('hidden_customer_address') : null;
        $invoice_data['RefCustomerPhone'] = $this->input->post('hidden_customer_phonenumber') ? $this->input->post('hidden_customer_phonenumber') : null;
        $invoice_data['RefCustomerEmail'] = $this->input->post('hidden_customer_email') ? $this->input->post('hidden_customer_email') : null;

        $RefAgentId = $this->input->post('agent_id');
        $invoice_data['RefAgentId'] = isset($RefAgentId) && !empty($RefAgentId) ? $RefAgentId : null;
        $invoice_data['RefAgentAddress'] = $this->input->post('hidden_agent_address') ? $this->input->post('hidden_agent_address') : null;
        $invoice_data['RefAgentPhone'] = $this->input->post('hidden_agent_phonenumber') ? $this->input->post('hidden_agent_phonenumber') : null;
        $invoice_data['RefAgentEmail'] = $this->input->post('hidden_agent_email') ? $this->input->post('hidden_agent_email') : null;

        $invoice_data['RefCommissionRateId'] = $this->input->post('commission_rate_id') ? $this->input->post('commission_rate_id') : null;

        $commission_rate_type = $this->input->post('commission_rate_type') ? $this->input->post('commission_rate_type') : null;
        $invoice_data['RefCommissionRateType'] = $commission_rate_type;

        $commision_rate = $this->input->post('commission_rate') ? $this->input->post('commission_rate') : 0;
        $invoice_data['RefCommissionRate'] = $commision_rate;

        $commisionTotal = 0;

        $qunatity = $this->input->post('quantity_amount');
        if (isset($qunatity) && !empty($qunatity)) {
            $quantityTotal = 0;
            foreach ($qunatity as $key => $qty) {
                $quantityTotal += $qty;
            }
        }

        $unit_qua = $this->input->post('BasicTotal');
        if (isset($unit_qua) && !empty($unit_qua)) {
            $BasicTotal = 0;
            foreach ($unit_qua as $key => $total) {
                $BasicTotal += $total;
            }
            $invoice_data['BasicTotal'] = $BasicTotal;
        }

        $discount = $this->input->post('discount_ind');
        if (isset($discount) && !empty($discount)) {
            $Discount = 0;
            foreach ($discount as $key => $disc) {
                $disc = isset($disc) && !empty($disc) ? $disc : 0;
                $Discount += $disc;
            }
            $invoice_data['DiscountTotal'] = $Discount;
        } else {
            $invoice_data['DiscountTotal'] = '';
        }

        $invoice_data['Taxtotal'] = 0;

        $invoice_data['FrightCharges'] = $this->input->post('fright_charges') ? $this->input->post('fright_charges') : 0;
        $invoice_data['RoundUpAmt'] = $this->input->post('round_up_amt') ? $this->input->post('round_up_amt') : 0;

        $GrandTotal = $this->input->post('g_total');
        $invoice_data['GrandTotal'] = $GrandTotal;

        if (isset($commission_rate_type) && !empty($commission_rate_type)) {
            if ($commission_rate_type == 1) {
                $commisionTotal = $quantityTotal * $commision_rate;
            } else {
                $commisionTotal = ($GrandTotal * $commision_rate) / 100;
            }
        }

        $invoice_data['CommissionTotal'] = $commisionTotal;

        $remarks = $this->input->post('sale_invoice_remark') ? $this->input->post('sale_invoice_remark') : null;
        $invoice_data['Remarks'] = $remarks;
        $invoice_data['TermsAndConditions'] = $this->input->post('sale_invoice_tc') ? $this->input->post('sale_invoice_tc') : null;

        $invoice_data['UpdUser'] = $this->session->userdata(SITE_NAME . '_admin')['user_id'];
        ;
        $invoice_data['UpdTerminal'] = $this->input->ip_address();
        $invoice_data['UpdDateTime'] = date('Y-m-d H:i:s');

        $customer_ledger['amount'] = $GrandTotal;
        $customer_ledger['description'] = 'Non GST Invoice - (' . $SINO . ')';
        $customer_ledger['date'] = $DateTime;
        $customer_ledger['ref_table_name'] = 'sale_invoice';
        $customer_ledger['d_c'] = 'c';

        if ($SIID != '') {
            $this->Common_model->deleteData('sale_invoice_item', 'refSIID', $SIID);
            $this->Common_model->deleteData('sale_invoice_itemtaxdet', 'refSIID', $SIID);

            $this->Common_model->updateData('sale_invoice', 'SIID', $SIID, $invoice_data);
            $this->Common_model->updateData2('customer_ledger', 'invoice_no', $SIID, 'ref_table_name', 'sale_invoice', $customer_ledger);
            $product = $this->input->post('product_id');
            $result = $this->Sale_invoice_model->insert_non_gst_product($product, $SIID);
        } else {
            $this->session->set_userdata(array('error_message' => "This Sale Invoice already exist"));
            redirect(base_url('Sale_invoice/manageNonGSTSaleInvoice'));
        }

        if ($result == 1) {
            $this->session->set_userdata(array('message' => display('successfully_updated')));
            if (isset($_POST['add-sale-invoice'])) {
                redirect(base_url('Sale_invoice/manageNonGSTSaleInvoice'));
                exit;
            } elseif (isset($_POST['add-sale-invoice-another'])) {
                redirect(base_url('Sale_invoice/addNonGSTSaleInvoice'));
                exit;
            }
        } else {
            $this->session->set_userdata(array('error_message' => display('Sale Invoice alredy exist.')));
            redirect(base_url('Sale_invoice/manageNonGSTSaleInvoice'));
        }
    }

    public function getAgentCommisionRate() {
        $data = [];

        $commision_id = $this->input->post('commision_id');
        $rate_type = $this->input->post('rate_type');

        $commision_data = $this->Sale_invoice_model->getCommisionDataById($commision_id);

        if (isset($commision_data) && !empty($commision_data)) {
            if ($rate_type == 1) {
                $data['status'] = true;
                $data['rate'] = $commision_data->commission_fix_kg;
            } elseif ($rate_type == 2) {
                $data['status'] = true;
                $data['rate'] = $commision_data->commission_percentage;
            } else {
                $data['status'] = false;
                $data['message'] = "Something went wrong, Try again!";
            }
        } else {
            $data['status'] = false;
            $data['message'] = "No data found, Try again!";
        }

        echo json_encode($data);
        die();
    }

    public function saleInvoiceDetails($SIID) {
        $this->Common_model->check_menu_access('SALE_INVOICE', 'VIEW', 'admin');

        $sale_invoice_data = $this->Sale_invoice_model->product_edit_data($SIID);
        $sale_invoice_details_data = $this->Sale_invoice_model->purchase_invoice_list_for_print($SIID);

        $subtotal = $sale_invoice_data['BasicTotal'] - $sale_invoice_data['DiscountTotal'] + $sale_invoice_data['Taxtotal'];

        $currency_details = $this->Web_settings->retrieve_setting_editdata();
        $company_info = get_company_detail();
        $code = $this->Common_model->getCompanyCode();

        $data = array(
            'title' => 'Sale Invoice Details',
            'sale_invoive_id' => $SIID,
            'sale_invoice_data' => $sale_invoice_data,
            'sale_invoice_details_data' => $sale_invoice_details_data,
            'subtotal' => $subtotal,
            'company_info' => $company_info,
            'currency' => $currency_details[0]['currency'],
            'position' => $currency_details[0]['currency_position'],
            'discount_type' => $currency_details[0]['discount_type'],
            'company_code' => strtoupper($code),
        );

        $view = 'admin/sale_invoice/invoice_detail';
        $this->load_admin_view($view, $data);
    }

    public function checkUniqueCustomerPhoneNo() {
        $phone_no = $this->input->post('cust_modal_phone_no');

        $result = $this->Sale_invoice_model->checkUniqueCustomerPhoneNo($phone_no);

        if (!empty($result)) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    public function addCustomer() {
        $data = [];

        if ($this->input->post()) {
            $customer_data = [];

            $customer_data['company__name'] = $this->input->post('cust_modal_cust_name') ? $this->input->post('cust_modal_cust_name') : null;
            $customer_data['customer_name'] = $this->input->post('cust_modal_cust_name');
            $customer_data['customer_mobile'] = $this->input->post('cust_modal_phone_no');

            if (isset($customer_data) && !empty($customer_data)) {
                $customer_id = $this->Sale_invoice_model->insertCustomer($customer_data);
                if (isset($customer_id) && !empty($customer_id)) {
                    $data['status'] = true;
                    $data['customer_data'] = $customer_data;
                    $data['customer_id'] = $customer_id;
                    $data['msg'] = "Customer added successfully.";
                } else {
                    $data['status'] = false;
                    $data['msg'] = "Something went wrong, Try again!";
                }
            } else {
                $data['status'] = false;
                $data['msg'] = "No data found, Try again!";
            }
        } else {
            $data['status'] = false;
            $data['msg'] = "Something went wrong, Try again!";
        }

        echo json_encode($data);
        die();
    }

    public function getProductDataByBarcode() {
        $barcode = $this->input->post('product_barcode');
//        log_message('ERROR', 'Product Barcode:' . $barcode);

        $product_info = $this->Sale_invoice_model->getProductDataByBarcode($barcode);

        if (isset($product_info) && !empty($product_info)) {
            $data['status'] = true;
            $data['product_info'] = $product_info;
        } else {
            $data['status'] = false;
            $data['msg'] = 'Please enter valid product barcode!';
        }

        echo json_encode($data);
        die();
    }
}
