<?php

class Report extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'REPORT';
        $this->load->model('Common_model');
        $this->load->model('Report_model');
    }

    public function index() {
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SALE_REPORT_BY_DATE', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SALE_REPORT_BY_DATE', 'VIEW', 'admin');
        $this->mnu_id = 'SALE_REPORT_BY_DATE';
        $data = array();
        if ($this->input->post()) {
            $start_date = $this->input->post($this->security->xss_clean('startDate')) != '' ? date('Y-m-d', strtotime($this->input->post($this->security->xss_clean('startDate')))) : '';
            $end_date = $this->input->post($this->security->xss_clean('endDate')) != '' ? date('Y-m-d', strtotime($this->input->post($this->security->xss_clean('endDate')))) : '';
            $data['start_date'] = $this->input->post($this->security->xss_clean('startDate')) != '' ? date('d-m-Y', strtotime($start_date)) : '';
            $data['end_date'] = $this->input->post($this->security->xss_clean('endDate')) != '' ? date('d-m-Y', strtotime($end_date)) : '';
            $data['saleReportByDate'] = $this->Report_model->saleReportByDate($start_date, $end_date);
        } else {
            $data['start_date'] = '';
            $data['end_date'] = '';
            $data['saleReportByDate'] = $this->Report_model->saleReportByDate();
        }
        $view = 'admin/report/saleReportByDate';
        $this->page_title = 'SABHASAD';
        $this->load_admin_view($view, $data);
    }

    public function saleReportByDate() {
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SALE_REPORT_BY_DATE', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SALE_REPORT_BY_DATE', 'VIEW', 'admin');
        $this->menu_id = 'SALE_REPORT_BY_DATE';
        $data = array();
        if ($this->input->post()) {
            $start_date = $this->input->post($this->security->xss_clean('startDate')) != '' ? date('Y-m-d', strtotime($this->input->post($this->security->xss_clean('startDate')))) : '';
            $end_date = $this->input->post($this->security->xss_clean('endDate')) != '' ? date('Y-m-d', strtotime($this->input->post($this->security->xss_clean('endDate')))) : '';
            $data['start_date'] = $this->input->post($this->security->xss_clean('startDate')) != '' ? date('d-m-Y', strtotime($start_date)) : '';
            $data['end_date'] = $this->input->post($this->security->xss_clean('endDate')) != '' ? date('d-m-Y', strtotime($end_date)) : '';
            $data['saleReportByDate'] = $this->Report_model->saleReportByDate($start_date, $end_date);
        } else {
            $data['start_date'] = '';
            $data['end_date'] = '';
            $data['saleReportByDate'] = $this->Report_model->saleReportByDate();
        }
        $view = 'admin/report/saleReportByDate';
        $this->page_title = 'SABHASAD';
        $this->load_admin_view($view, $data);
    }

    public function saleReportBySabhasad() {
        $this->menu_id = 'SALE_REPORT_BY_SABHASAD';
        $data = array();
        $data['menu_rights'] = $this->Common_model->get_menu_rights($this->menu_id, 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access($this->menu_id, 'VIEW', 'admin');
        if ($this->input->post()) {
            $start_date = $this->input->post($this->security->xss_clean('startDate')) != '' ? date('Y-m-d', strtotime($this->input->post($this->security->xss_clean('startDate')))) : '';
            $end_date = $this->input->post($this->security->xss_clean('endDate')) != '' ? date('Y-m-d', strtotime($this->input->post($this->security->xss_clean('endDate')))) : '';
            $sabhasad_id = $this->input->post('sabhasad_id');
            $data['start_date'] = $this->input->post($this->security->xss_clean('startDate')) != '' ? date('d-m-Y', strtotime($start_date)) : '';
            $data['end_date'] = $this->input->post($this->security->xss_clean('endDate')) != '' ? date('d-m-Y', strtotime($end_date)) : '';
            $data['sabhasad_id'] = $sabhasad_id;
            $data['saleReportBySabhasad'] = $this->Report_model->saleReportBySabhasad($start_date, $end_date, $sabhasad_id);
        } else {
            $data['start_date'] = '';
            $data['end_date'] = '';
            $data['saleReportBySabhasad'] = $this->Report_model->saleReportBySabhasad();
            $data['sabhasad_id'] = array();
        }
        $data['sabhasad_data'] = $this->Common_model->geAlldata('tbl_sabhasad');
//        preprint($data['saleReportBySabhasad']);
        $view = 'admin/report/saleReportBySabhasad';
        $this->page_title = 'SABHASAD';
        $this->load_admin_view($view, $data);
    }

    public function detailedSaleReport() {
        $this->menu_id = 'DETAILED_SALE_REPORT';
        $data = array();
        $data['menu_rights'] = $this->Common_model->get_menu_rights($this->menu_id, 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access($this->menu_id, 'VIEW', 'admin');
        if ($this->input->post()) {
            $start_date = $this->input->post($this->security->xss_clean('startDate')) != '' ? date('Y-m-d', strtotime($this->input->post($this->security->xss_clean('startDate')))) : '';
            $end_date = $this->input->post($this->security->xss_clean('endDate')) != '' ? date('Y-m-d', strtotime($this->input->post($this->security->xss_clean('endDate')))) : '';
            $data['start_date'] = $this->input->post($this->security->xss_clean('startDate')) != '' ? date('d-m-Y', strtotime($start_date)) : '';
            $data['end_date'] = $this->input->post($this->security->xss_clean('endDate')) != '' ? date('d-m-Y', strtotime($end_date)) : '';
            $data['detailedSaleReport'] = $this->Report_model->detailedSaleReport($start_date, $end_date);
        } else {
            $data['start_date'] = '';
            $data['end_date'] = '';
            $data['detailedSaleReport'] = $this->Report_model->detailedSaleReport();
        }
//        preprint($data['detailedSaleReport']);
        $view = 'admin/report/detailedSaleReport';
        $this->page_title = 'SABHASAD';
        $this->load_admin_view($view, $data);
    }

}
