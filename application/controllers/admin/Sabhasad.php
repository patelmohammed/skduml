<?php

class Sabhasad extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login('admin')) {
            redirect('admin');
        }
        $this->page_id = 'DASHBOARD';
        $this->load->model('Sabhasad_model');
        $this->load->model('Skduml_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->page_id = 'SABHASAD';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SABHASAD', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SABHASAD', 'VIEW', 'admin');
        $data['sabhasad_data'] = $this->Common_model->geAlldata('tbl_sabhasad');
        $view = 'admin/sabhasad/sabhasad';
        $this->page_title = 'SABHASAD';
        $this->load_admin_view($view, $data);
    }

    public function addEditSabhasad($encrypted_id = "") {
        $this->page_id = 'SABHASAD';
        $id = $encrypted_id;
        $user_menu_access_arr = [];
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['sabhasad_name'] = $this->input->post('sabhasad_name');
            $insert_data['sabhasad_name_en'] = $this->input->post('sabhasad_name_en');
            $insert_data['sabhasad_code'] = $this->input->post('sabhasad_code');
            $insert_data['sabhasad_contact'] = $this->input->post('sabhasad_contact');
            $insert_data['sabhasad_address'] = $this->input->post('sabhasad_address');
            $password = $this->input->post('sabhasad_password');
            $confirm_password = $this->input->post('sabhasad_confirm_password');
            if ((isset($password) && !empty($password)) && (isset($confirm_password) && !empty($confirm_password))) {
                if ($password == $confirm_password) {
                    $insert_data['sabhasad_password'] = md5($password);
                    $insert_data['raw'] = $password;
                } else {
                    $this->_show_message("Password And Confirm Password not matched", "error");
                    if ($id == "" || $id == '' || $id == NULL) {
                        redirect('admin/Sabhasad/addEditSabhasad');
                    } else {
                        redirect('admin/Sabhasad/addEditSabhasad/' . $encrypted_id);
                    }
                }
            }

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->admin_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $user_id = $this->Common_model->insertInformation($insert_data, 'tbl_sabhasad');
            } else {
                $insert_data['UpdUser'] = $this->admin_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'sabhasad_id', $id, 'tbl_sabhasad');
            }
            redirect('admin/Sabhasad');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('SABHASAD', 'ADD', 'admin');
                $data = [];
                $view = 'admin/sabhasad/addEditSabhasad';
                $this->page_title = 'SABHASAD';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('SABHASAD', 'EDIT', 'admin');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['sabhasad_data'] = $this->Common_model->getDataById2('tbl_sabhasad', 'sabhasad_id', $id, 'Live');
                $view = 'admin/sabhasad/addEditSabhasad';
                $this->page_title = 'SABHASAD';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkSabhasadName($sabhasad_id = '') {
        if ($sabhasad_id != '') {
            $SabhasadName = $this->Sabhasad_model->getSabhasadName($sabhasad_id);
            $respone = $this->Sabhasad_model->checkSabhasadName($this->input->get('sabhasad_name'), $SabhasadName);
            echo $respone;
            die;
        } else {
            $response = $this->Sabhasad_model->checkSabhasadName($this->input->get('sabhasad_name'));
            echo $response;
            die;
        }
    }

    public function checkSabhasadNameEnglish($sabhasad_id = '') {
        if ($sabhasad_id != '') {
            $SabhasadName = $this->Sabhasad_model->getSabhasadNameEnglish($sabhasad_id);
            $respone = $this->Sabhasad_model->checkSabhasadNameEnglish($this->input->get('sabhasad_name_en'), $SabhasadName);
            echo $respone;
            die;
        } else {
            $response = $this->Sabhasad_model->checkSabhasadNameEnglish($this->input->get('sabhasad_name_en'));
            echo $response;
            die;
        }
    }

    public function checkSabhasadCode($sabhasad_id = '') {
        if ($sabhasad_id != '') {
            $SabhasadCode = $this->Sabhasad_model->getSabhasadCode($sabhasad_id);
            $respone = $this->Sabhasad_model->checkSabhasadName($this->input->get('sabhasad_code'), $SabhasadCode);
            echo $respone;
            die;
        } else {
            $response = $this->Sabhasad_model->checkSabhasadCode($this->input->get('sabhasad_code'));
            echo $response;
            die;
        }
    }

    public function deleteSabhasad() {
        $this->Common_model->check_menu_access('SABHASAD', 'DELETE', 'admin');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $insert_data = array();
            $insert_data['del_status'] = "Delete";
            $insert_data['DelUser'] = $this->admin_id;
            $insert_data['DelTerminal'] = $this->input->ip_address();
            $insert_data['DelDateTime'] = date('Y-m-d H:i:s');

            if ($this->Common_model->updateInformation2($insert_data, 'sabhasad_id', $id, 'tbl_sabhasad')) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $this->_show_message("Something went wrong try again.", "error");
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function payment() {
        $data = [];
        $this->page_id = 'WALLET';
        $this->menu_id = 'WALLET_DEBIT';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('WALLET_DEBIT', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('WALLET_DEBIT', 'VIEW', 'admin');
        if ($this->input->post()) {
            $data['startDate'] = $startDate = $this->input->post('startDate');
            $data['endDate'] = $endDate = $this->input->post('endDate');
        } else {
            $data['startDate'] = $startDate = date('d-m-Y');
            $data['endDate'] = $endDate = date('d-m-Y');
        }
        $data['sabhasad_payment_data'] = $this->Sabhasad_model->getSabhasadDebit($startDate, $endDate);
        $view = 'admin/wallet/debit/debit';
        $this->page_title = 'SABHASAD PAYMENT';
        $this->load_admin_view($view, $data);
    }

    public function addEditPayment($encrypted_id = "") {
        $this->page_id = 'WALLET';
        $this->menu_id = 'WALLET_DEBIT';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['ref_sabhasad_id'] = $this->input->post('ref_sabhasad_id');
            $payment_date = $this->input->post('payment_date');
            $insert_data['payment_date'] = (isset($payment_date) && !empty($payment_date) ? date('Y-m-d H:i', strtotime($payment_date)) : '');
            $insert_data['milk_quantity'] = $this->input->post('milk_quantity');
            $insert_data['milk_price'] = $this->input->post('milk_price');
            $insert_data['total_payment'] = $this->input->post('total_payment');
            $insert_data['ref_milk_id'] = 1;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->admin_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->insertInformation($insert_data, 'tbl_sabhasad_payment_debit');
            } else {
                $insert_data['UpdUser'] = $this->admin_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'payment_id', $id, 'tbl_sabhasad_payment_debit   ');
            }
            redirect('admin/Sabhasad/payment');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('WALLET_DEBIT', 'ADD', 'admin');
                $data = [];
                $data['sabhasad_data'] = $this->Common_model->geAlldata('tbl_sabhasad');
                $data['milk_price_data'] = $this->Common_model->geAlldata('tbl_milk_price', ' ORDER BY milk_price_date DESC ');
                $view = 'admin/wallet/debit/addEditPayment';
                $this->page_title = 'SABHASAD PAYMENT';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('WALLET_DEBIT', 'EDIT', 'admin');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['sabhasad_data'] = $this->Common_model->geAlldata('tbl_sabhasad');
                $data['milk_price_data'] = $this->Common_model->geAlldata('tbl_milk_price', ' ORDER BY milk_price_date DESC ');
                $payment_data = $this->Common_model->getDataById2('tbl_sabhasad_payment_debit', 'payment_id', $id, 'Live');
                $data['payment_data'] = $payment_data;
                $payment = $this->Skduml_model->getSabhasadWalletBalance($payment_data->ref_sabhasad_id);
                $data['total_balance'] = number_format(round(($payment->total_credit - $payment->total_debit + $payment_data->total_payment), 2), 2, ".", "");
                $view = 'admin/wallet/debit/addEditPayment';
                $this->page_title = 'SABHASAD';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deletePayment() {
        $this->Common_model->check_menu_access('WALLET_DEBIT', 'DELETE', 'admin');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $insert_data = array();
            $insert_data['del_status'] = "Delete";
            $insert_data['DelUser'] = $this->admin_id;
            $insert_data['DelTerminal'] = $this->input->ip_address();
            $insert_data['DelDateTime'] = date('Y-m-d H:i:s');

            if ($this->Common_model->updateInformation2($insert_data, 'payment_id ', $id, 'tbl_sabhasad_payment_debit')) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $this->_show_message("Something went wrong try again.", "error");
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function paymentCredit() {
        $data = [];
        $this->page_id = 'WALLET';
        $this->menu_id = 'WALLET_CREDIT';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('WALLET_CREDIT', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('WALLET_CREDIT', 'VIEW', 'admin');
        if ($this->input->post()) {
            $data['startDate'] = $startDate = $this->input->post('startDate');
            $data['endDate'] = $endDate = $this->input->post('endDate');
        } else {
            $data['startDate'] = $startDate = date('d-m-Y');
            $data['endDate'] = $endDate = date('d-m-Y');
        }
        $data['sabhasad_payment_data'] = $this->Sabhasad_model->getSabhasadCredit($startDate, $endDate);
        $view = 'admin/wallet/credit/credit';
        $this->page_title = 'SABHASAD PAYMENT';
        $this->load_admin_view($view, $data);
    }

    public function addEditPaymentCredit($encrypted_id = "") {
        $this->page_id = 'WALLET';
        $this->menu_id = 'WALLET_CREDIT';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['ref_sabhasad_id'] = $this->input->post('ref_sabhasad_id');
            $credit_date = $this->input->post('credit_date');
            $insert_data['credit_date'] = (isset($credit_date) && !empty($credit_date) ? date('Y-m-d H:i', strtotime($credit_date)) : '');
            $insert_data['amount'] = $this->input->post('amount');
            $insert_data['remark'] = $this->input->post('remark');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->admin_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->insertInformation($insert_data, 'tbl_sabhasad_payment_credit');
            } else {
                $insert_data['UpdUser'] = $this->admin_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'credit_id', $id, 'tbl_sabhasad_payment_credit   ');
            }
            redirect('admin/Sabhasad/paymentCredit');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('WALLET_CREDIT', 'ADD', 'admin');
                $data = [];
                $data['sabhasad_data'] = $this->Common_model->geAlldata('tbl_sabhasad');
                $view = 'admin/wallet/credit/addEditCredit';
                $this->page_title = 'SABHASAD PAYMENT';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('WALLET_CREDIT', 'EDIT', 'admin');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['sabhasad_data'] = $this->Common_model->geAlldata('tbl_sabhasad');
                $data['payment_data'] = $this->Common_model->getDataById2('tbl_sabhasad_payment_credit', 'credit_id', $id, 'Live');
                $view = 'admin/wallet/credit/addEditCredit';
                $this->page_title = 'SABHASAD';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deletePaymentCredit() {
        $this->Common_model->check_menu_access('WALLET_CREDIT', 'DELETE', 'admin');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $insert_data = array();
            $insert_data['del_status'] = "Delete";
            $insert_data['DelUser'] = $this->admin_id;
            $insert_data['DelTerminal'] = $this->input->ip_address();
            $insert_data['DelDateTime'] = date('Y-m-d H:i:s');

            if ($this->Common_model->updateInformation2($insert_data, 'credit_id', $id, 'tbl_sabhasad_payment_credit')) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $this->_show_message("Something went wrong try again.", "error");
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function getSabhasadWalletBalance() {
        $data = array();
        $sabhasad_id = $this->input->post('sabhasad_id');
        if (isset($sabhasad_id) && !empty($sabhasad_id)) {
            $payment = $this->Sabhasad_model->getSabhasadWalletBalance($sabhasad_id);
            $data['total_balance'] = number_format(round(($payment->total_credit - $payment->total_debit), 2), 2, ".", "");
            if (isset($data['total_balance']) && !empty($data['total_balance'])) {
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

}
