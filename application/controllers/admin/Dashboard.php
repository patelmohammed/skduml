<?php

class Dashboard extends My_Controller {

    public function __construct() {
        parent::__construct();
//        if (!$this->is_login('admin')) {
//            redirect('admin');
//        }
        $this->page_id = 'DASHBOARD';
        $this->load->model('Dashboard_model');
        $this->load->model('Common_model');
        $this->load->model('Auth_model');
    }

    public function index() {
        $data = [];
        $view = 'admin/home/index';
        $this->Common_model->check_menu_access('DASHBOARD', 'VIEW', 'admin');
        $data['count_data'] = new stdClass();
        $data['count_data']->sabhasad_count = $this->Dashboard_model->getSabhsadCount();
        $data['count_data']->milk_sold_count = $this->Dashboard_model->getMilkSoldCount();
        $data['count_data']->total_credit_amount = $this->Dashboard_model->getTotalCreditAmount();
        $data['count_data']->total_debit_amount = $this->Dashboard_model->getTotalDebitAmount();
        $data['count_data']->milk_price = $this->Dashboard_model->getMilkPrice();
        $data['less_balance_sabhasad'] = $this->Dashboard_model->getLessBalanceSabhasadList();
        $this->page_title = 'DASHBOARD';
        $this->load_admin_view($view, $data);
    }

    public function addEditProfile() {
        $this->menu_id = 'PROFILE';
        $id = $this->admin_id;
        if ($this->input->post()) {
            $user_info = $this->Common_model->getDataById('user_information', $id);
            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('new_password');
            $confirm_new_password = $this->input->post('confirm_new_password');
            if ((isset($old_password) && !empty($old_password)) && (md5($old_password) == $user_info->password)) {
                if ((isset($new_password) && !empty($new_password)) && (isset($confirm_new_password) && !empty($confirm_new_password))) {
                    if ($new_password == $confirm_new_password) {
                        $insert_data['password'] = md5($new_password);
                        $insert_data['raw'] = $new_password;
                    } else {
                        $this->_show_message("New Password And Confirm Password not matched", "error");
                        redirect('admin/Dashboard/addEditProfile');
                    }
                }
            } else {
                $this->_show_message("Old Password not matched", "error");
                redirect('admin/Dashboard/addEditProfile');
            }
            $insert_data['user_name'] = $this->input->post('user_name');

            $insert_data['UpdUser'] = $this->admin_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->_show_message("Your password changed successfully", "success");
                $this->Common_model->updateInformation2($insert_data, 'id', $id, 'user_information');
            }
            redirect('admin/Dashboard');
        } else {
            if (isset($id) && !empty($id)) {
                $data = [];
                $data['user_data'] = $this->Common_model->getDataById2('user_information', 'id', $id, 'Live');
                $view = 'admin/home/editProfile';
                $this->page_title = 'PROFILE';
                $this->load_admin_view($view, $data);
            } else {
                $this->_show_message("You cant insert new profile", "error");
                redirect('admin/Dashboard');
            }
        }
    }

    public function user() {
        $data = [];
        $this->page_id = 'USER';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('USER', 'admin');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('USER', 'VIEW', 'admin');
        $data['user_data'] = $this->Common_model->geAlldata('user_information');
        $view = 'admin/user/user';
        $this->page_title = 'USER';
        $this->load_admin_view($view, $data);
    }

    public function addEditUser($encrypted_id = "") {
        $this->page_id = 'USER';
        $id = $encrypted_id;
        $user_menu_access_arr = [];
        if ($this->input->post()) {
            $user_info = $user_menu_data = array();
            $insert_data['user_name'] = $this->input->post('user_name');
            $insert_data['user_email'] = $this->input->post('user_email');
            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');
            if ((isset($password) && !empty($password)) && (isset($confirm_password) && !empty($confirm_password))) {
                if ($password == $confirm_password) {
                    $insert_data['password'] = md5($password);
                    $insert_data['raw'] = $password;
                } else {
                    $this->_show_message("Password And Confirm Password not matched", "error");
                    if ($id == "" || $id == '' || $id == NULL) {
                        redirect('admin/Dashboard/addEditUser');
                    } else {
                        redirect('admin/Dashboard/addEditUser/' . $encrypted_id);
                    }
                }
            }

            $admin_pin = $this->input->post('admin_pin');
            $confirm_admin_pin = $this->input->post('confirm_admin_pin');
            if ((isset($admin_pin) && !empty($admin_pin)) && (isset($confirm_admin_pin) && !empty($confirm_admin_pin))) {
                if ($admin_pin == $confirm_admin_pin) {
                    $insert_data['admin_pin'] = $admin_pin;
                } else {
                    $this->_show_message("Admin Pin And Confirm Admin Pin not matched", "error");
                    if ($id == "" || $id == '' || $id == NULL) {
                        redirect('admin/Dashboard/addEditUser');
                    } else {
                        redirect('admin/Dashboard/addEditUser/' . $encrypted_id);
                    }
                }
            }
            $insert_data['admin_code'] = $this->input->post('admin_code');

            $insert_data['address'] = $this->input->post('address');
            $insert_data['mobile'] = $this->input->post('mobile');

            $full_access = $this->input->post('full_access');
            $view = $this->input->post('view');
            $add = $this->input->post('add');
            $edit = $this->input->post('edit');
            $delete = $this->input->post('delete');

            $ins_menu = [];
            if (!empty($full_access)) {
                foreach ($full_access as $key => $value) {
                    $ins_menu[$key]['full_access'] = $value == 'on' ? 1 : 0;
                }
            }
            if (!empty($view)) {
                foreach ($view as $vi_key => $vi_value) {
                    $ins_menu[$vi_key]['view_right'] = $vi_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($add)) {
                foreach ($add as $ad_key => $ad_value) {
                    $ins_menu[$ad_key]['add_right'] = $ad_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($edit)) {
                foreach ($edit as $ed_key => $ed_value) {
                    $ins_menu[$ed_key]['edit_right'] = $ed_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($delete)) {
                foreach ($delete as $del_key => $del_value) {
                    $ins_menu[$del_key]['delete_right'] = $del_value == 'on' ? 1 : 0;
                }
            }

            if (!empty($ins_menu)) {
                foreach ($ins_menu as $key => $value) {
                    $ins_menu[$key]['ref_menu_id'] = $key;
                    $user_menu_data[] = $ins_menu[$key];
                }
            }

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->admin_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $user_id = $this->Common_model->insertInformation($insert_data, 'user_information');
                $this->Auth_model->insertSideMenuAccess($user_menu_data, $user_id);
            } else {
                $insert_data['UpdUser'] = $this->admin_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'id', $id, 'user_information');
                $this->Auth_model->updateSideMenuAccess($user_menu_data, $id);
            }
            redirect('admin/Dashboard/user');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('USER', 'ADD', 'admin');
                $data = [];
                $view = 'admin/user/addUser';
                $data['side_menu'] = $this->Auth_model->getMenuMSTData();
                $this->page_title = 'USER';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('USER', 'EDIT', 'admin');
                $data = [];
                $data['side_menu'] = $this->Auth_model->getMenuAccessById($encrypted_id);
                $data['encrypted_id'] = $encrypted_id;
                $data['user_data'] = $this->Common_model->getDataById2('user_information', 'id', $id, 'Live');
                $view = 'admin/user/editUser';
                $this->page_title = 'USER';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkUserName($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Dashboard_model->getUserName($category_id);
            $respone = $this->Dashboard_model->checkUserName($this->input->get('user_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Dashboard_model->checkUserName($this->input->get('user_name'));
            echo $response;
            die;
        }
    }

    public function checkUserEmail($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Dashboard_model->getUserEmail($category_id);
            $respone = $this->Dashboard_model->checkUserEmail($this->input->get('user_email'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Dashboard_model->checkUserEmail($this->input->get('user_email'));
            echo $response;
            die;
        }
    }

    public function deleteUser() {
        $this->Common_model->check_menu_access('USER', 'DELETE', 'admin');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('id', $id);
            $this->db->update('user_information');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    function sale_report_ajax_get() {
        $selectedMonth = $_GET['months'];
        $finalOutput = array();
        $max = [];
        for ($i = $selectedMonth - 1; $i >= 0; $i--) {
            $dateCalculate = $i > 0 ? '-' . $i : $i;
            $sqlStartDate = date('Y-m-01', strtotime($dateCalculate . ' month'));
            $sqlEndDate = date('Y-m-31', strtotime($dateCalculate . ' month'));
            $saleAmount = $this->Dashboard_model->sale_report_ajax_get($sqlStartDate, $sqlEndDate);
            $max[] = date('M-y', strtotime($dateCalculate . ' month'));
            $finalOutput[] = array(date('M-y', strtotime($dateCalculate . ' month')), (isset($saleAmount) && !empty($saleAmount) ? (float) $saleAmount->total_amount : 0.0));
        }
        $this->output_json(['data' => $finalOutput, 'xlabel' => $max]);
    }

}
