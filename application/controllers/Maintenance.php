<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if ($this->is_login('sabhasad')) {
            $this->session->sess_destroy();
        }
        $data = [];
        $this->page_id = 'login_pg';
        $this->page_title = 'Maintenance';
        $view = 'frontend/maintenance';
        $this->load_view($view, $data);
    }

}
