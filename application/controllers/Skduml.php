<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Skduml extends MY_Controller {

    public function __construct() {
        parent::__construct();
//        if ($this->input->ip_address() != '103.238.110.115') {
//            redirect(base_url('maintenance'));
//        }
        $this->load->model('Auth_model');
        $this->load->model('Skduml_model');
        $this->load->model('Report_model');
        $this->data['page_id'] = '';
    }

    public function index() {
        if ($this->is_login('sabhasad')) {
            redirect(base_url('dashboard'));
        }
        $data = [];
        if ($this->input->post()) {
            $sabhasad_code = $this->input->post('sabhasad_code');
            $sabhasad_password = $this->input->post('sabhasad_password');
            $sabhasad_data = $this->Common_model->getDataById2('tbl_sabhasad', 'sabhasad_code', $sabhasad_code, 'Live');
            if (isset($sabhasad_data) && !empty($sabhasad_data)) {
                if ($this->check_pwd($sabhasad_password, $sabhasad_data->sabhasad_password)) {
                    $session_data = array();
                    $session_data['sabhasad_id'] = $sabhasad_data->sabhasad_id;
                    $session_data['sabhasad_name'] = $sabhasad_data->sabhasad_name;
                    $session_data['sabhasad_name_en'] = $sabhasad_data->sabhasad_name_en;
                    $session_data['sabhasad_code'] = $sabhasad_data->sabhasad_code;
                    $session_data['login_type'] = 'sabhasad';
                    $session_data['profile_photo'] = 'assets/admin/img/demo/avatars/avatar-admin-new.png';
                    $session_data['date_format'] = 'd-m-Y';

                    $menu_data = $this->Auth_model->get_assigned_menu($sabhasad_data->sabhasad_id, '', 'sabhasad');
                    $session_data['side_menu'] = $menu_data;

                    $this->session->set_userdata(SITE_NAME . '_sabhasad', $session_data);
                    redirect(base_url('dashboard'));
                } else {
                    $this->session->sess_destroy();
                    $this->_show_message("wrong password", "error");
                }
            } else {
                $this->session->sess_destroy();
                $this->_show_message("wrong username", "error");
            }
        }
        $this->page_id = 'login_pg';
        $this->page_title = 'Login';
        $view = 'frontend/login';
        $this->load_view($view, $data);
    }

    function check_pwd($password, $db) {
        return md5($password) == $db;
    }

    public function logout() {
        unset($_SESSION[SITE_NAME . '_sabhasad']);
//        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function Unauthorized() {
        $data['title'] = 'Access Denied';
        $this->load_view("frontend/access_denied", $data);
    }

    public function changePin() {
        if (!$this->is_login('sabhasad')) {
            redirect(base_url('login'));
        }
        $data = array();
        $sabhasad_data = $this->Common_model->getDataById2('tbl_sabhasad', 'sabhasad_id', $this->sabhasad_id, 'Live');
        if ($this->input->post()) {
            if (isset($sabhasad_data) && !empty($sabhasad_data) && isset($sabhasad_data->sabhasad_contact) && !empty($sabhasad_data->sabhasad_contact)) {
                $contact_no = '91' . substr($sabhasad_data->sabhasad_contact, -10);
                $otp_response = $this->Common_model->send_otp_message($contact_no);
//                $session_data['contact_number'] = $contact_no;
//                $session_data['sabhasad_code'] = $sabhasad_code;
//                $session_data['sabhasad_id'] = $sabhasad_data->sabhasad_id;
//                $this->session->set_userdata(SITE_NAME . '_reset_pin', $session_data);
                redirect(base_url('verify_otp'));
            } else {
                $this->_show_message("Something went wrong. First logout then try again.", "error");
            }
        }
        $data['sabhasad_data'] = $sabhasad_data;
        $this->page_id = 'change_pin';
        $this->page_title = 'Change Pin';
        $view = 'frontend/change_pin';
        $this->load_view($view, $data);
    }

    public function verifyOtp() {
        if (!$this->is_login('sabhasad')) {
            redirect(base_url('login'));
        }
        $data = array();
        $sabhasad_data = $this->Common_model->getDataById2('tbl_sabhasad', 'sabhasad_id', $this->sabhasad_id, 'Live');
        $contact_number = isset($sabhasad_data->sabhasad_contact) && !empty($sabhasad_data->sabhasad_contact) ? '91' . substr($sabhasad_data->sabhasad_contact, -10) : '';
        $sabhasad_id = isset($sabhasad_data->sabhasad_id) && !empty($sabhasad_data->sabhasad_id) ? $sabhasad_data->sabhasad_id : '';
        if (isset($contact_number) && !empty($contact_number) && isset($sabhasad_id) && !empty($sabhasad_id)) {
            if ($this->input->post()) {
                $otp = $this->input->post('otp');

                $verify_response = $this->Common_model->otp_verify($contact_number, $otp);
                $verify_response = json_decode($verify_response);
                if (isset($verify_response->type) && !empty($verify_response->type) && $verify_response->type == 'success') {
                    $session_data['is_otp_verified'] = TRUE;
                    $this->session->set_userdata(SITE_NAME . '_reset_pin', $session_data);
                    redirect(base_url('reset_pin'));
                } else {
                    $this->_show_message("Invalid OTP! Please try again.", "warning");
                }
            }
            $this->page_id = 'verify_otp';
            $this->page_title = 'Verify OTP';
            $view = 'frontend/verify_otp';
            $this->load_view($view, $data);
        } else {
            $this->_show_message("Something went wrong", "error");
            redirect(base_url('dashboard'));
        }
    }

    public function resendOtp() {
        $data = array();
        $sabhasad_data = $this->Common_model->getDataById2('tbl_sabhasad', 'sabhasad_id', $this->sabhasad_id, 'Live');
        $contact_number = isset($sabhasad_data->sabhasad_contact) && !empty($sabhasad_data->sabhasad_contact) ? '91' . substr($sabhasad_data->sabhasad_contact, -10) : '';
        if (isset($contact_number) && !empty($contact_number)) {
            $resend_response = $this->Common_model->resend_otp($contact_number);
            $resend_response = json_decode($resend_response);
            if (isset($resend_response->type) && !empty($resend_response->type) && $resend_response->type == 'success') {
                $data['message'] = $resend_response->message;
                $data['result'] = true;
            } else {
                $data['message'] = 'We are facing technical error!';
                $data['result'] = false;
            }
        } else {
            $data['message'] = 'We are facing technical error!';
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function resetPin() {
        if (!$this->is_login('sabhasad')) {
            redirect(base_url('login'));
        }
        $data = [];
        $is_otp_verified = $this->session->userdata(SITE_NAME . '_reset_pin')['is_otp_verified'];
        if (isset($is_otp_verified) && !empty($is_otp_verified)) {
            if ($this->input->post()) {
                $new_pin = $this->input->post('new_pin');
                $confirm_pin = $this->input->post('confirm_pin');
                if (isset($new_pin) && !empty($new_pin) && isset($confirm_pin) && !empty($confirm_pin)) {
                    if ($new_pin == $confirm_pin) {
                        $insert_data = array();
                        $insert_data['sabhasad_password'] = md5($new_pin);
                        $insert_data['raw'] = $new_pin;
                        $this->Common_model->updateInformation2($insert_data, 'sabhasad_id', $this->sabhasad_id, 'tbl_sabhasad');
                        $session_data['is_otp_verified'] = FALSE;
                        $this->session->set_userdata(SITE_NAME . '_reset_pin', $session_data);
                        $this->_show_message("Successfully reset you PIN.", "success");
                        redirect(base_url('dashboard'));
                    } else {
                        $this->_show_message("New PIN and Confirm PIN did not match.", "error");
                        redirect(base_url('reset_pin'));
                    }
                } else {
                    $this->_show_message("Something went wrong", "error");
                    redirect(base_url('reset_pin'));
                }
            }
            $this->page_id = 'reset_pin';
            $this->page_title = 'Reset Pin';
            $view = 'frontend/reset_pin';
            $this->load_view($view, $data);
        } else {
            $this->_show_message("First verify you OTP.", "error");
            redirect(base_url('verify_otp'));
        }
    }

    public function dashboard() {
        if (!$this->is_login('sabhasad')) {
            redirect(base_url('login'));
        }
        $data = array();
        $this->page_id = 'dashboard_pg';
        $this->page_title = 'Dashboad';
        $view = 'frontend/dashboard';
        $this->load_view($view, $data);
    }

    public function booking() {
        if (!$this->is_login('sabhasad')) {
            redirect(base_url('login'));
        }
        $isPrint = $this->Common_model->checkPrintPending($this->sabhasad_id);
        if (isset($isPrint) && !empty($isPrint) && $isPrint > 0) {
            $this->_show_message("You receipt is pending.", "error");
            redirect('dashboard');
        } else {
            $data = [];
            if ($this->input->post()) {
                $insert_data = array();
                $insert_data['ref_sabhasad_id'] = $this->sabhasad_id;
                $insert_data['payment_date'] = date('Y-m-d H:i:s');
                $insert_data['milk_quantity'] = $this->input->post('hidden_milk_quantity');
                $insert_data['milk_price'] = $this->input->post('hidden_milk_price');
                $insert_data['total_payment'] = $this->input->post('hidden_total_payment');
                $insert_data['ref_milk_id'] = 1;
                $insert_data['IsPrint'] = 0;
                $insert_data['InsUser'] = $this->sabhasad_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $debit_id = $this->Common_model->insertInformation($insert_data, 'tbl_sabhasad_payment_debit');
                if (isset($debit_id) && !empty($debit_id)) {
                    $this->_show_message("Something went wrong", "success");
                    redirect('logout');
                } else {
                    $this->_show_message("Something went wrong", "error");
                    redirect('dashboard');
                }
            } else {
                $view = 'frontend/booking';
//        $this->Common_model->check_menu_access('DASHBOARD', 'VIEW', 'sabhasad');
                $this->page_title = 'BOOKING';
                $payment = $this->Skduml_model->getSabhasadWalletBalance($this->sabhasad_id);
                $data['total_balance'] = number_format(round(($payment->total_credit - $payment->total_debit), 2), 2, ".", "");
                $data['sabhasad_data'] = $this->Common_model->getDataById2('tbl_sabhasad', 'sabhasad_id', $this->sabhasad_id, 'Live');
                //$data['milk_data'] = $this->Common_model->getDataById2('tbl_milk_price', 'del_status', 'Live', 'Live');
                $data['milk_data'] = $this->Common_model->geAlldata('tbl_milk_price', ' ORDER BY milk_price_date DESC LIMIT 1');
                $this->load_view($view, $data);
            }
        }
    }

    public function transaction() {
        if (!$this->is_login('sabhasad')) {
            redirect(base_url('login'));
        }
        $data = [];
        $view = 'frontend/transaction';
        $this->page_title = 'BOOKING';
        $this->load_view($view, $data);
    }

    public function getTransactionHistory() {
        $data = array();
        $row = '';
        $start_date = $this->input->post($this->security->xss_clean('startDate'));
        $end_date = $this->input->post($this->security->xss_clean('endDate'));
        if (isset($start_date) && !empty($start_date)) {
            $start_date = date('Y-m-d', strtotime($start_date));
        } else {
            $start_date = date('Y-m-01');
        }
        if (isset($end_date) && !empty($end_date)) {
            $end_date = date('Y-m-d', strtotime($end_date));
        } else {
            $end_date = date('Y-m-t');
        }
        $transactionHistory = $this->Report_model->saleReportBySabhasad($start_date, $end_date, $this->sabhasad_id);
        $totalBalance = $totalCredit = $totalDebit = $totalMilkQuantity = 0;
        if (isset($transactionHistory) && !empty($transactionHistory)) {
            foreach ($transactionHistory as $key => $value) {
                if ($value->payment_type == 'credit') {
                    $totalBalance += $value->credit_amount;
                    $totalCredit += $value->credit_amount;
                } else if ($value->payment_type == 'debit') {
                    $totalBalance -= $value->debit_amount;
                    $totalDebit += $value->debit_amount;
                }
                $totalMilkQuantity += (isset($value->milk_quantity) && !empty($value->milk_quantity) ? $value->milk_quantity : 0);
                $key++;
                $row .= '<tr>';
                $row .= '<td class="text-center" style="vertical-align: middle;width: 25%;">' . date("d-m-Y", strtotime($value->paymentDate)) . '</br>' . date('h:i a', strtotime($value->paymentDate)) . '</td>';
                $row .= '<td class="text-center" style="vertical-align: middle;width: 25%;">' . $value->milk_quantity . (isset($value->milk_price) && !empty($value->milk_price) ? " * " . number_format($value->milk_price, 2) : "") . '</td>';
                $row .= '<td class="text-center" style="vertical-align: middle;width: 25%;">';
                if (isset($value->credit_amount) && !empty($value->credit_amount)) {
                    $row .= '<span style="color: green;">+ ' . $value->credit_amount . '</span>';
                }
                if (isset($value->debit_amount) && !empty($value->debit_amount)) {
                    $row .= '<span style="color: red;">- ' . $value->debit_amount . '</span>';
                }
                $row .= '</td>';
                $row .= '<td class="text-center" style="vertical-align: middle;width: 25%;">' . number_format($totalBalance, 2) . '</td>';
                $row .= '</tr>';
            }
            $data['result'] = true;
        } else {
            $row .= '<tr><td colspan="4" class="text-center" style="color: red;">No Data Available</td></tr>';
            $data['result'] = false;
        }
        $data['row_data'] = $row;
        echo json_encode($data);
        die;
    }

}
