<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model('Skduml_model');
    }

    public function sabhasad_get() {
        $this->list();
    }

     public function sabhasad_post() {
        $sabhasad_data = $this->post();
        if(!empty($sabhasad_data)){
            log_message("error",print_r($sabhasad_data, true));
            $this->Common_model->insertBatchInformation($sabhasad_data,'tbl_sabhasad_payment_debit');
            $this->list();
        }else{
            $this->response(['success' => false, 'error' => true, 'data' => 'Data Parsing Error Or No Data Found'], 200);
        }
     }

     public function list(){
        $select = "sabhasad_id, sabhasad_name, sabhasad_name_en, sabhasad_code, sabhasad_contact, sabhasad_address, sabhasad_password, IsAllow";
        $sabhasad_data = $this->Common_model->getAlldataAPI('tbl_sabhasad',$select);
        if (isset($sabhasad_data) && !empty($sabhasad_data)) {
            foreach ($sabhasad_data as $key => $value) {
                $payment = $this->Skduml_model->getSabhasadWalletBalance($value->sabhasad_id);
                $sabhasad_data[$key]->total_balance = number_format(round(($payment->total_credit - $payment->total_debit), 2), 2, ".", "");

                $milk_data = $this->Common_model->geAlldata('tbl_milk_price', ' ORDER BY milk_price_date DESC LIMIT 1');
                $sabhasad_data[$key]->milk_id = (isset($milk_data[0]->ref_milk_id) && !empty($milk_data[0]->ref_milk_id) ? $milk_data[0]->ref_milk_id : 0);
                $sabhasad_data[$key]->milk_price = (isset($milk_data[0]->milk_price) && !empty($milk_data[0]->milk_price) ? $milk_data[0]->milk_price : 0);
            }
            $this->response(['success' => true, 'error' => false, 'data' => $sabhasad_data], 200);
        } else {
            $this->response(['success' => false, 'error' => true, 'data' => 'No sabhasad found'], 200);
        }
     }
}
