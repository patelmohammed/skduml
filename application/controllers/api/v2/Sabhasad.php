<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Sabhasad extends RestController {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model('Skduml_model');
    }

    public function list_get() {
        $code = $this->get('code');
        if ($code === null || $code == '') {
            $this->response(['success' => false, 'error' => true, 'data' => 'No sabhasad CODE found'], 200);
        } else {
            $select = "sabhasad_id, sabhasad_name, sabhasad_name_en, sabhasad_code, sabhasad_contact, sabhasad_address, sabhasad_password, IsAllow";
            $sabhasad_data = $this->Common_model->getDataAPI('tbl_sabhasad', " AND sabhasad_code = '" . $code . "' ", $select);
            if (!empty($sabhasad_data)) {
                $payment = $this->Skduml_model->getSabhasadWalletBalance($sabhasad_data->sabhasad_id);
                $sabhasad_data->total_balance = number_format(round(($payment->total_credit - $payment->total_debit), 2), 2, ".", "");

                $milk_data = $this->Common_model->geAlldata('tbl_milk_price', ' ORDER BY milk_price_date DESC LIMIT 1');
                $sabhasad_data->milk_id = (isset($milk_data[0]->ref_milk_id) && !empty($milk_data[0]->ref_milk_id) ? $milk_data[0]->ref_milk_id : 0);
                $sabhasad_data->milk_price = (isset($milk_data[0]->milk_price) && !empty($milk_data[0]->milk_price) ? $milk_data[0]->milk_price : 0);

                $print_select = "payment_id, ref_sabhasad_id, bill_no, payment_date, ref_milk_id, milk_quantity, milk_price, total_payment, IsPrint, InsUserType ";
                $print_data = $this->Common_model->getDataAPI('tbl_sabhasad_payment_debit', " AND IsPrint = 0 AND ref_sabhasad_id = " . $sabhasad_data->sabhasad_id . " ", $print_select);
                $sabhasad_data->is_print = !empty($print_data) ? $print_data : NULL;

                $this->response(['success' => true, 'error' => false, 'data' => $sabhasad_data], 200);
            } else {
                $this->response(['success' => false, 'error' => true, 'data' => 'No sabhasad found'], 200);
            }
        }
    }

    public function receipt_post() {
        $sabhasad_data = $this->post();
        if (!empty($sabhasad_data)) {
            $sql = "SELECT * 
                    FROM tbl_sabhasad_payment_debit spd 
                    WHERE spd.ref_sabhasad_id = " . $sabhasad_data['ref_sabhasad_id'] . " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d %H:%i') = '" . $sabhasad_data['payment_date'] . "'";
            $database_data = $this->db->query($sql)->row();
            if (isset($database_data) && !empty($database_data)) {
                $sabhasad_data['bill_no'] = isset($database_data->bill_no) && !empty($database_data->bill_no) ? $database_data->bill_no : '';
                $sabhasad_data['InsUserType'] = "User";
                $sabhasad_data['IsPrint'] = 1;
                $this->response(['success' => true, 'error' => false, 'data' => $sabhasad_data], 200);
            } else {
                $check = '';
                foreach ($sabhasad_data as $key => $value) {
                    if ($value == '') {
                        $check .= $key . ", ";
                    }
                }
                if ($check != '') {
                    $this->response(['success' => false, 'error' => true, 'data' => 'Following Fields Are Empty :: ' . trim($check, ", ")], 200);
                } else {
                    $last_receipt_no = $this->Common_model->getBillNumber();
                    $sabhasad_data['bill_no'] = !empty($last_receipt_no) ? ($last_receipt_no) : 1;
                    $sabhasad_data['InsUserType'] = "User";
                    $sabhasad_data['IsPrint'] = 1;
                    $id = $this->Common_model->insertInformation($sabhasad_data, 'tbl_sabhasad_payment_debit');
                    log_message("error", print_r($sabhasad_data, true));
                    if ($id > 0) {
                        $this->response(['success' => true, 'error' => false, 'data' => $sabhasad_data], 200);
                    } else {
                        $this->response(['success' => false, 'error' => true, 'data' => 'Something Went Wrong With Database'], 200);
                    }
                }
            }
        } else {
            $this->response(['success' => false, 'error' => true, 'data' => 'Data Parsing Error Or No Data Found'], 200);
        }
    }

    public function printer_post() {
        $print_data = $this->post("print");
        $payment_id = $this->post("payment_id");
        if (!empty($print_data) && $print_data == "OkDone" && !empty($payment_id) && $payment_id > 0) {
            $data = [];
            $data['IsPrint'] = 1;
            $data['UpdUserType'] = "User";
            $data['UpdDateTime'] = date("Y-m-d H:i:s");
            if ($this->Common_model->updateInformation2($data, 'payment_id', $payment_id, 'tbl_sabhasad_payment_debit')) {
                $this->response(['success' => true, 'error' => false, 'data' => "Print Updated"], 200);
            } else {
                $this->response(['success' => false, 'error' => true, 'data' => 'Something Went Wrong With Database'], 200);
            }
        } else {
            $this->response(['success' => false, 'error' => true, 'data' => 'Data Parsing Error Or No Data Found'], 200);
        }
    }

    public function history_post() {
        $code = $this->post("scode");
        $start_dt = $this->post("start_dt");
        $end_dt = $this->post("end_dt");
        $data = $this->Common_model->report_sabhasad($start_dt, $end_dt, $code);
        if (!empty($data)) {
            $this->response(['success' => true, 'error' => false, 'data' => $data], 200);
        } else {
            $this->response(['success' => false, 'error' => true, 'data' => 'Something Went Wrong With Database'], 200);
        }
    }

    public function user_post() {
        $sabhasad_data = $this->post();
        if (!empty($sabhasad_data)) {
            $sql = "SELECT * 
                    FROM tbl_sabhasad_payment_debit spd 
                    WHERE spd.ref_sabhasad_id = " . $sabhasad_data['ref_sabhasad_id'] . " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d %H:%i') = '" . $sabhasad_data['payment_date'] . "'";
            $database_data = $this->db->query($sql)->row();
            if (isset($database_data) && !empty($database_data)) {
                $sabhasad_data['bill_no'] = (isset($database_data->bill_no) && !empty($database_data->bill_no) ? $database_data->bill_no : '');
                $sabhasad_data['InsUserType'] = "User";
                $sabhasad_data['IsPrint'] = 0;
                $this->response(['success' => true, 'error' => false, 'data' => $sabhasad_data], 200);
            } else {
                $check = '';
                foreach ($sabhasad_data as $key => $value) {
                    if ($value == '') {
                        $check .= $key . ", ";
                    }
                }
                if ($check != '') {
                    $this->response(['success' => false, 'error' => true, 'data' => 'Following Fields Are Empty :: ' . trim($check, ", ")], 200);
                } else {
                    $last_receipt_no = $this->Common_model->getBillNumber();
                    $sabhasad_data['bill_no'] = !empty($last_receipt_no) ? ($last_receipt_no) : 1;
                    $sabhasad_data['InsUserType'] = "User";
                    $sabhasad_data['IsPrint'] = 0;
                    $id = $this->Common_model->insertInformation($sabhasad_data, 'tbl_sabhasad_payment_debit');
                    log_message("error", print_r($sabhasad_data, true));
                    if ($id > 0) {
                        $this->response(['success' => true, 'error' => false, 'data' => $sabhasad_data], 200);
                    } else {
                        $this->response(['success' => false, 'error' => true, 'data' => 'Something Went Wrong With Database'], 200);
                    }
                }
            }
        } else {
            $this->response(['success' => false, 'error' => true, 'data' => 'Data Parsing Error Or No Data Found'], 200);
        }
    }

    public function userdata_post() {
        $sabhasad_id = $this->post("id");
        $sabhasad_password = $this->post("pwd");
        $raw = $this->post("raw");
        if (!empty($sabhasad_id) && !empty($sabhasad_password) && !empty($raw)) {
            $data = [];
            $data['sabhasad_password'] = $sabhasad_password;
            $data['raw'] = $raw;
            if ($this->Common_model->updateInformation2($data, 'sabhasad_id', $sabhasad_id, 'tbl_sabhasad')) {
                $this->response(['success' => true, 'error' => false, 'data' => "Password Updated"], 200);
            } else {
                $this->response(['success' => false, 'error' => true, 'data' => 'Something Went Wrong With Database'], 200);
            }
        } else {
            $this->response(['success' => false, 'error' => true, 'data' => 'Data Parsing Error Or No Data Found'], 200);
        }
    }

}
