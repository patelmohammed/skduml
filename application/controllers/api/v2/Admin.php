<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Admin extends RestController {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model('Skduml_model');
    }

    public function checkcode_get() {
        $code = $this->get('code');
        if ($code === null || $code == '') {
            $this->response(['success' => false, 'error' => true, 'data' => 'No Admin CODE found'], 200);
        } else {
            $admin_data = $this->Common_model->getDataAPI('user_information', " AND admin_code = '" . $code . "' ");
            if (!empty($admin_data)) {
                $this->response(['success' => true, 'error' => false, 'data' => 'Admin Code Found'], 200);
            } else {
                $this->response(['success' => false, 'error' => true, 'data' => 'No Admin found'], 200);
            }
        }
    }

    public function login_post() {
        log_message("error", print_r("---------------", true));
        log_message("error", print_r($this->post(), true));
        log_message("error", print_r("---------------", true));
        $code = $this->post('code');
        $authpin = $this->post('authpin');
        if ($code === null || $code == '') {
            $this->response(['success' => false, 'error' => true, 'data' => 'No Admin CODE found'], 200);
        } else if ($authpin === null || $authpin == '') {
            $this->response(['success' => false, 'error' => true, 'data' => 'No Admin PIN found'], 200);
        } else {
            $admin_data = $this->Common_model->getDataAPI('user_information', " AND admin_code = '" . $code . "' AND admin_pin = '" . $authpin . "' ", "id, user_name, admin_code, mobile, rest_key");
            if (!empty($admin_data)) {
                $rest_key = base64_encode(date("Y-m-dH:i:s") . "w3b0s" . $code . $authpin);
                $data = [];
                $admin_data->rest_key = $data['rest_key'] = $rest_key;
                if ($this->Common_model->updateInformation2($data, 'id', $admin_data->id, 'user_information')) {
                    $this->response(['success' => true, 'error' => false, 'data' => $admin_data], 200);
                } else {
                    $this->response(['success' => false, 'error' => true, 'data' => 'Something Went Wrong With Database'], 200);
                }
            } else {
                $this->response(['success' => false, 'error' => true, 'data' => 'No Admin found'], 200);
            }
        }
    }

    public function list_get() {
        $id = $this->checkauth();
        $code = $this->get('code');
        if ($code === null || $code == '') {
            $this->response(['success' => false, 'error' => true, 'data' => 'No sabhasad CODE found'], 200);
        } else {
            $select = "sabhasad_id, sabhasad_name, sabhasad_name_en, sabhasad_code, sabhasad_contact, sabhasad_address, sabhasad_password, IsAllow";
            $sabhasad_data = $this->Common_model->getDataAPI('tbl_sabhasad', " AND sabhasad_code = '" . $code . "' ", $select);
            if (!empty($sabhasad_data)) {
                $payment = $this->Skduml_model->getSabhasadWalletBalance($sabhasad_data->sabhasad_id);
                $sabhasad_data->total_balance = number_format(round(($payment->total_credit - $payment->total_debit), 2), 2, ".", "");

                $milk_data = $this->Common_model->geAlldata('tbl_milk_price', ' ORDER BY milk_price_date DESC LIMIT 1');
                $sabhasad_data->milk_id = (isset($milk_data[0]->ref_milk_id) && !empty($milk_data[0]->ref_milk_id) ? $milk_data[0]->ref_milk_id : 0);
                $sabhasad_data->milk_price = (isset($milk_data[0]->milk_price) && !empty($milk_data[0]->milk_price) ? $milk_data[0]->milk_price : 0);

                $print_select = "payment_id, ref_sabhasad_id, bill_no, payment_date, ref_milk_id, milk_quantity, milk_price, total_payment, IsPrint, InsUserType ";
                $print_data = $this->Common_model->getDataAPI('tbl_sabhasad_payment_debit', " AND IsPrint = 0 AND ref_sabhasad_id = " . $sabhasad_data->sabhasad_id . " ", $print_select);
                $sabhasad_data->is_print = !empty($print_data) ? $print_data : NULL;

                $this->response(['success' => true, 'error' => false, 'data' => $sabhasad_data], 200);
            } else {
                $this->response(['success' => false, 'error' => true, 'data' => 'No sabhasad found'], 200);
            }
        }
    }

    public function receipt_post() {
        $id = $this->checkauth();
        $sabhasad_data = $this->post();
        if (!empty($sabhasad_data)) {
            $sql = "SELECT * 
                    FROM tbl_sabhasad_payment_debit spd 
                    WHERE spd.ref_sabhasad_id = " . $sabhasad_data['ref_sabhasad_id'] . " AND DATE_FORMAT(spd.payment_date, '%Y-%m-%d %H:%i') = '" . $sabhasad_data['payment_date'] . "'";
            $database_data = $this->db->query($sql)->row();

            if (isset($database_data) && !empty($database_data)) {
                $sabhasad_data['bill_no'] = isset($database_data->bill_no) && !empty($database_data->bill_no) ? ($database_data->bill_no) : 1;
                $sabhasad_data['InsUserType'] = "Admin";
                $sabhasad_data['InsUser'] = $id;
                $sabhasad_data['IsPrint'] = 1;
                $this->response(['success' => true, 'error' => false, 'data' => $sabhasad_data], 200);
            } else {
                $check = '';
                foreach ($sabhasad_data as $key => $value) {
                    if ($value == '') {
                        $check .= $key . ", ";
                    }
                }
                if ($check != '') {
                    $this->response(['success' => false, 'error' => true, 'data' => 'Following Fields Are Empty :: ' . trim($check, ", ")], 200);
                } else {
                    $last_receipt_no = $this->Common_model->getBillNumber();
                    $sabhasad_data['bill_no'] = !empty($last_receipt_no) ? ($last_receipt_no) : 1;
                    $sabhasad_data['InsUserType'] = "Admin";
                    $sabhasad_data['InsUser'] = $id;
                    $sabhasad_data['IsPrint'] = 1;
                    $id = $this->Common_model->insertInformation($sabhasad_data, 'tbl_sabhasad_payment_debit');
                    log_message("error", print_r($sabhasad_data, true));
                    log_message("error", print_r($id, true));
                    if ($id > 0) {
                        $this->response(['success' => true, 'error' => false, 'data' => $sabhasad_data], 200);
                    } else {
                        $this->response(['success' => false, 'error' => true, 'data' => 'Something Went Wrong With Database'], 200);
                    }
                }
            }
        } else {
            $this->response(['success' => false, 'error' => true, 'data' => 'Data Parsing Error Or No Data Found'], 200);
        }
    }

    public function printer_post() {
        $id = $this->checkauth();
        $print_data = $this->post("print");
        $payment_id = $this->post("payment_id");
        if (!empty($print_data) && $print_data == "OkDone" && !empty($payment_id) && $payment_id > 0) {
            $data = [];
            $data['IsPrint'] = 1;
            $data['UpdUserType'] = "Admin";
            $data['UpdDateTime'] = date("Y-m-d H:i:s");
            if ($this->Common_model->updateInformation2($data, 'payment_id', $payment_id, 'tbl_sabhasad_payment_debit')) {
                $this->response(['success' => true, 'error' => false, 'data' => "Print Updated"], 200);
            } else {
                $this->response(['success' => false, 'error' => true, 'data' => 'Something Went Wrong With Database'], 200);
            }
        } else {
            $this->response(['success' => false, 'error' => true, 'data' => 'Data Parsing Error Or No Data Found'], 200);
        }
    }

    public function checkauth() {
        $auth_e_key = $this->input->get_request_header('ApiAuth');
        $authkey = base64_decode($auth_e_key);
        if (strpos($authkey, "w3b0s") !== false) {
            $admin_data = $this->Common_model->getDataAPI('user_information', " AND rest_key = '" . $auth_e_key . "' ", "id");
            if (!empty($admin_data)) {
                return $admin_data->id;
            } else {
                $this->response(['success' => false, 'error' => true, 'data' => 'Unauthorise Access'], 203);
            }
        } else {
            $this->response(['success' => false, 'error' => true, 'data' => 'Unauthorise Access'], 203);
        }
    }

    public function userdata_post() {
        $admin_id = $this->post("id");
        //$admin_pin = $this->post("pwd");
        $admin_pin = $this->post("raw");
        if (!empty($admin_id) && !empty($admin_pin)) {
            $data = [];
            $data['admin_pin'] = $admin_pin;
            if ($this->Common_model->updateInformation2($data, 'id', $admin_id, 'user_information')) {
                $this->response(['success' => true, 'error' => false, 'data' => "Password Updated"], 200);
            } else {
                $this->response(['success' => false, 'error' => true, 'data' => 'Something Went Wrong With Database'], 200);
            }
        } else {
            $this->response(['success' => false, 'error' => true, 'data' => 'Data Parsing Error Or No Data Found'], 200);
        }
    }

    public function history_post() {
        $dt = $this->post("dt");
        $start_time = $this->post("start_time");
        $end_time = $this->post("end_time");
        $qty = $this->Common_model->report_admin_total_qty($dt, $start_time, $end_time);
        $sabhasad = $this->Common_model->report_admin_total_sabhasad($dt, $start_time, $end_time);
        $debit_data = $this->Common_model->report_admin_sabhasad_by_data($dt, $start_time, $end_time);
        $this->response(['success' => true, 'error' => false, 'data' => ['sabhasad' => $sabhasad, 'qty' => $qty, 'time_wise' => $debit_data]], 200);
    }

}
