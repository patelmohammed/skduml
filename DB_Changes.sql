/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Acer
 * Created: 24-May-2021
 */

UPDATE `tbl_side_menu` SET `menu_order_no` = '2' WHERE `tbl_side_menu`.`menu_id` = 6;
UPDATE `tbl_side_menu` SET `menu_order_no` = '1' WHERE `tbl_side_menu`.`menu_id` = 7;
ALTER TABLE `user_information`  ADD `token` TEXT NULL  AFTER `raw`;

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES (NULL, 'Report', 'REPORT', '#', NULL, 'fal fa-chart-pie', '5', 'Live'), (NULL, 'Sale Report By Date', 'SALE_REPORT_BY_DATE', 'admin/Report/saleReportByDate', '8', NULL, '1', 'Live');

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES (NULL, 'Sale Report By Sabhasad', 'SALE_REPORT_BY_SABHASAD', 'admin/Report/saleReportBySabhasad', '8', NULL, '2', 'Live');

ALTER TABLE `tbl_side_menu`  ADD `menu_for` VARCHAR(50) NOT NULL  AFTER `menu_order_no`;
UPDATE `tbl_side_menu` SET `menu_for` = 'admin';

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Dashboard', 'DASHBOARD', 'Dashboard', NULL, 'fa-info-circle', '1', 'sabhasad', 'Live');

DROP TABLE IF EXISTS `tbl_mail_template`;
CREATE TABLE IF NOT EXISTS `tbl_mail_template` (
  `mail_template_id` int NOT NULL AUTO_INCREMENT,
  `type` mediumtext NOT NULL,
  `subject` mediumtext NOT NULL,
  `message` text NOT NULL,
  `fromname` mediumtext NOT NULL,
  `fromemail` varchar(100) DEFAULT NULL,
  `active` tinyint NOT NULL DEFAULT '0',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`mail_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mail_template`
--

INSERT INTO `tbl_mail_template` (`mail_template_id`, `type`, `subject`, `message`, `fromname`, `fromemail`, `active`, `del_status`) VALUES
(1, 'sabhasad_forgot_password', 'Forgot Password', '<p><br />\r\n<a href=\"{base_url}Auth/resetPassword/{client_id}\">Reset password</a></p>\r\n', 'SKDUML', 'info@weborative.com', 1, 'Live'),
(2, 'admin_forgot_password', 'Forgot Password', '<p><br />\r\n<a href=\"{base_url}Auth/resetPassword/{admin_id}\">Reset password</a></p>\r\n', 'SKDUML', 'info@weborative.com', 1, 'Live');


ALTER TABLE `tbl_sabhasad_payment_credit` CHANGE `credit_date` `credit_date` DATETIME NOT NULL;
ALTER TABLE `tbl_sabhasad_payment_debit` CHANGE `payment_date` `payment_date` DATETIME NOT NULL;

ALTER TABLE `tbl_sabhasad`  ADD `sabhasad_name_en` VARCHAR(200) NOT NULL  AFTER `sabhasad_name`;
ALTER TABLE `tbl_sabhasad`  ADD `IsAllow` TINYINT(1) NOT NULL DEFAULT '1'  AFTER `raw`;
INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Detailed Sale Report', 'DETAILED_SALE_REPORT', 'admin/Report/detailedSaleReport', '8', NULL, '3', 'admin', 'Live');


ALTER TABLE `user_information`  ADD `admin_pin` VARCHAR(50) NOT NULL  AFTER `password`;
ALTER TABLE `user_information`  ADD `admin_code` VARCHAR(50) NOT NULL  AFTER `password`;

-- NEW DEVELOPMENT

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`,`menu_for`, `del_status`) VALUES
(13, 'Customer', 'CUSTOMERS', '#', NULL, 'fa fa-handshake-o', 9, 'admin', 'Live'),
(14, 'Customer', 'CUSTOMER', 'admin/Ccustomer/manage_customer', 13, NULL, NULL, 'admin', 'Live'),
(15, 'Customer Ledger', 'CUSTOMER_LEDGER', 'admin/Ccustomer/customerledger', 13, NULL, NULL, 'admin', 'Live'),
(16, 'Supplier', 'SUPPLIERS', '#', NULL, 'fa fa-ship', 10, 'admin', 'Live'),
(17, 'Supplier', 'SUPPLIER', 'admin/Csupplier/manage_supplier', 16, NULL, NULL, 'admin', 'Live'),
(18, 'Supplier Ledger', 'SUPPLIER_LEDGER', 'admin/Csupplier/supplier_ledger_report', 16, NULL, NULL, 'admin', 'Live'),
(19, 'Purchase', 'PURCHASE', '#', NULL, 'fa fa-cart-plus', 8, 'admin', 'Live'),
(20, 'Purchase Invoice', 'PURCHASE_INVOICE', 'admin/Purchase_invoice', 19, NULL, NULL, 'admin', 'Live'),
(21, 'Purchase Return', 'PURCHASE_RETURN', 'admin/Purchase_return/manageReturnList', 19, NULL, NULL, 'admin', 'Live'),
(22, 'Sales', 'SALES', '#', NULL, 'fa fa-line-chart', 7, 'admin', 'Live'),
(23, 'Sale Invoice (GST)', 'SALE_INVOICE', 'admin/Sale_invoice', 22, NULL, 1, 'admin', 'Live'),
(24, 'Sale Return', 'SALE_RETURN', 'admin/Sale_return/manageReturnList', 22, NULL, 3, 'admin', 'Live'),
(25, 'Sale Invoice (Non-GST)', 'NON_GST_SALE_INVOICE', 'admin/Sale_invoice/manageNonGSTSaleInvoice', 22, NULL, 2, 'admin', 'Live'),
(26, 'Masters', 'MASTERS', '#', NULL, 'fa fa-tasks', 11, 'admin', 'Live'),
(27, 'Product Category', 'PRODUCT_CATEGORY', 'admin/Ccategory/manage_category', 26, NULL, NULL, 'admin', 'Live'),
(28, 'Product Unit', 'PRODUCT_UNIT', 'admin/Cunit/manage_unit', 26, NULL, NULL, 'admin', 'Live'),
(29, 'Product', 'PRODUCT', 'admin/Cproduct/manage_product', 26, NULL, NULL, 'admin', 'Live'),
(30, 'Accounts', 'ACCOUNTS', '#', NULL, 'fa fa-money', 12, 'admin', 'Live'),
(31, 'Payment', 'PAYMENT', 'admin/Cpayment', 30, NULL, NULL, 'admin', 'Live'),
(32, 'Receipt', 'RECEIPT', 'admin/Cpayment/receipt_transaction', 30, NULL, NULL, 'admin', 'Live'),
(33, 'Transaction', 'TRANSACTION', 'admin/Cpayment/manage_payment', 30, NULL, NULL, 'admin', 'Live'),
(34, 'Tax', 'TAX', 'admin/Caccounts/manage_tax', 30, NULL, NULL, 'admin', 'Live'),
(35, 'Bank', 'BANK', 'admin/Csettings/bank_list', 30, NULL, NULL, 'admin', 'Live'),
(36, 'Payment Mode', 'PAYMENT_MODE', 'admin/Payment_Mode/manage_payment_mode', 30, NULL, NULL, 'admin', 'Live');